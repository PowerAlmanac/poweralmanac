#!/bin/bash

# Exit on 1st error
set -e

if [ "$#" -ne 1 ]; then
    echo "Please specify an environment to provision. Options: test, prod"
    echo "Example usage: bash bin/provision.sh test"
    exit 1
fi

ENV=${1}

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  BIN_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$BIN_DIRECTORY/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
BIN_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

PROVISIONING_DIRECTORY="${BIN_DIRECTORY}/../ansible"

echo "The provisioning process will set up MySQL server with new passwords for the 'root' and application users."
echo

if [ ! -d "${PROVISIONING_DIRECTORY}/credentials" ]; then
    echo "Missing 'credentials' directory. Attempting to create it..."
    mkdir "${PROVISIONING_DIRECTORY}/credentials/"
    echo
fi


for MYSQL_USER in root "pa_${ENV}_user" newrelic; do
    CREDENTIAL_FILE="${PROVISIONING_DIRECTORY}/credentials/${ENV}.mysql.${MYSQL_USER}"

    echo "Checking for '${CREDENTIAL_FILE}'..."

    if [ -e ${CREDENTIAL_FILE} ]; then
        MYSQL_USER_PASSWORD=$(head -n 1 ${CREDENTIAL_FILE})
        echo "Credential file found for '${MYSQL_USER}'. Continuing..."
        echo
    else
        echo "Credential file not found for '${MYSQL_USER}'. Please enter a password for this user."
        echo "NB: You should find credentials for this user in the KeePass database. If not, please create a new entry."

        printf "Password for '${MYSQL_USER}': "
        read -s MYSQL_USER_PASSWORD

        echo ${MYSQL_USER_PASSWORD} | tee ${CREDENTIAL_FILE}

        echo "Thank you. Continuing..."
        echo
    fi

done

echo "----------------------------------------------------------------"
echo

echo "All credential files have been accounted for. Proceeding with the provisioning process."
echo

ansible-playbook -i ${PROVISIONING_DIRECTORY}/inventories/${ENV} ${PROVISIONING_DIRECTORY}/provision.yml

exit 0
