#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "Please specify an environment to deploy to. Options: test, prod"
    echo "Example usage: bash bin/deploy.sh test"
    exit 1
fi

ENV=${1}

export DEPLOY_DATETIME=`date +%Y%m%d%H%M%S`
ansible-playbook -i ansible/inventories/${ENV} ansible/deploy.yml
