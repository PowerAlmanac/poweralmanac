<?php

use PowerAlmanac\PDb;

require_once 'bootstrap.php';
require_once realpath(__DIR__ . '/../codebase/public/inc/') . '/mandrill.php';

$dailyRegistrantsEmailTo = PowerAlmanac\Config::env('daily_registrants_email');
if (is_null($dailyRegistrantsEmailTo)) {
    die('No daily registrants email configured');
}

$dates = [
    'start' => new DateTime('yesterday midnight'),
    'end' => new DateTime('today midnight')
];

$yesterdaysRegistrants = PDb::fetch_assoc_all(
    PDb::query(
        sprintf(
            "SELECT * FROM `registeredusers` WHERE `DateTime_Registered` BETWEEN '%s' and '%s'", $dates['start']->format('Y-m-d H:i:s'), $dates['end']->format('Y-m-d H:i:s')
        )
    )
);

if (empty($yesterdaysRegistrants)) {
    die('No registrants to email about');
}

$yesterdaysRegistrantsCount = count($yesterdaysRegistrants);
$people = $yesterdaysRegistrantsCount == 1 ? 'person' : 'people';

$message = <<<MESSAGE
<html>
<body>
<h1>Hi,

{$yesterdaysRegistrantsCount} {$people} signed up yesterday</h1>

<ol>
MESSAGE;


foreach ($yesterdaysRegistrants as $registrant) {
    $emailUrlEncoded = urlencode($registrant['User_Email']);
    $message .= "<li><a href='mailto:{$emailUrlEncoded}'>{$registrant['User_FirstName']} {$registrant['User_LastName']}</a> - {$registrant['User_Email']}</li>";
}

$message .= '</ol></body></html>';

$email_data = [
    'to' => explode(',', $dailyRegistrantsEmailTo),
    'subject' => 'Yesterday\'s PowerAlmanac Signups: ' . $yesterdaysRegistrantsCount,
    'from' => array('info@poweralmanac.com' =>'Power Almanac'),
    'bcc' => '',
    'html' => $message
];

var_dump(mandrill_send($email_data));
