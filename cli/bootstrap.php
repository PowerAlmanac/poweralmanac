<?php
require_once realpath(__DIR__ . '/../') . '/codebase/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();
