Refs: https://teamwork.link

# Why
This MR is to add the 'needs more cowbell' feature, as in the teamwork link.  This pushes us closer to xxxxx, and adds better support for yyyyyy going forward

# Acceptance Criteria
* [ ] Matches all acceptance criteria in the task
* [ ] Test with a nonadmin user
* [ ] Works within test branch after merging
* [ ] If the feature utilises soft delete, make sure you delete some rows and retest
* [ ] Documentation has been updated
* [ ] KeePass vault has been updated

# How I tested
* Any prep needed (particular SQL dump, etc..)
* Figures used
* Buttons clicked
* etc..

# Pre-deployment steps
* [ ] Ensure servers are re-provisioned

# Thoughts/notes
* I considered xxxx, but decided on xxxx because xxxx
* We can't do yyyyy until yyyyy
* What are your thoughts on zzzzz?
