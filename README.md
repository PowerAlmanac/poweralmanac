## Dev set-up


1. Clone this repo
2. Run `vagrant up`

Wait for the system to provision itself using Ansible...

Then go to: https://poweralmanac.local/

* Login as `systems+pa.dev@siftware.com` / `password`

# Email

You can access any email sent from the system here: http://poweralmanac.local:8025/

# JXARH / Icube

* https://poweralmanac.local/jxarh/
* Login as siftware/siftware

# Deployment
## Staging server

Your public SSH key needs to authorised to access the test server via `ubuntu@poweralmanac.testly.uk`

1. Merge feature into `test` branch
2. Check the system works using the `test` branch on your dev vm
3. Commit and push the `test` branch to remote
4. Connect to the VM
5. `cd ~www-data/poweralmanac.com/current`
6. `./bin/deploy.sh test`
7. Test the site at https://poweralmanac.testly.uk/
