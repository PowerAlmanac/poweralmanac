# -*- mode: ruby -*-
# vi: set ft=ruby :

require File.dirname(__FILE__)+"/vagrant-dependency-manager"

#check_plugins ["vagrant-hostmanager"]

Vagrant.configure("2") do |config|
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true

  # Forward the SSH agent so that we can use ansible on the VM to run provisioning/deploy on remote servers,
  # while using the SSH keys on the host machine.
  config.ssh.forward_agent = true

  config.vm.define "poweralmanac" do |webserver|
    webserver.vm.box = "bento/ubuntu-16.04"
    webserver.vm.hostname = "poweralmanac.local"

    webserver.vm.network "private_network", ip: "192.168.51.2"
    webserver.vm.synced_folder ".", "/var/www/poweralmanac.com/releases/dev",
        group: "www-data",
        mount_options: ["dmode=775,fmode=774"]

    webserver.vm.synced_folder "./sh", "/var/www/sh/",
        group: "www-data",
        mount_options: ["dmode=775,fmode=774"]

    webserver.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
      vb.customize ["modifyvm", :id, "--cableconnected1", "on"] # https://github.com/chef/bento/issues/688
    end

    #### Add directories here to get the folders mounted into the VM
    #### This assumes you have cloned the other repo(s)

    webserver.vm.provision "shell", inline: "if ! command -v ansible; then apt-get install -y software-properties-common; apt-add-repository -y ppa:ansible/ansible; apt-get update; apt-get install -y ansible; fi"

    webserver.vm.provision "shell", inline: "if ! command -v git; then apt-get install -y git; fi"

    webserver.vm.provision "shell", inline: "bash /var/www/poweralmanac.com/releases/dev/bin/configure-known-hosts.sh"

    webserver.vm.provision "ansible_local" do |ansible|
      ansible.install = false
      ansible.groups = {
        "vagrant" => "poweralmanac"
      }
      ansible.provisioning_path = "/var/www/poweralmanac.com/releases/dev/ansible"
      ansible.galaxy_role_file = "requirements.yml"
      ansible.galaxy_roles_path = "roles"
      ansible.playbook = "provision.yml"
#      ansible.verbose = "vvv"
    end
  end
end
