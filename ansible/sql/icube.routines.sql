-- MySQL dump 10.13  Distrib 5.7.12, for osx10.11 (x86_64)
--
-- Host: poweralmanac.cnl6vllv6ior.us-east-1.rds.amazonaws.com    Database: icube_government
-- ------------------------------------------------------
-- Server version	5.6.34-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'icube_government'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "add_new_employee"; CREATE PROCEDURE "add_new_employee"(IN FiratName varchar(100),IN LastName varchar(100),IN UserID varchar(100),IN Pass varchar(100),IN AccessLevel varchar(100),IN PrimaryRole varchar(100),OUT id BIGINT)
    DETERMINISTIC
begin
insert into employees(first_name,last_name,user_id,password,access_level,primary_role) values (replace(FiratName,'\'','\''),replace(LastName,'\'','\''),replace(UserID,'\'','\''),replace(Pass,'\'','\''),replace(AccessLevel,'\'','\''),replace(PrimaryRole,'\'','\''));
SELECT LAST_INSERT_ID() into id from dual;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "all_update_government"; CREATE PROCEDURE "all_update_government"(in g_id varchar(100),in job_id bigint,in v_Government_Web_Address varchar(255),in v_Taken_from varchar(500),in v_Updated_website varchar(10),in v_Government_PhoneNumber varchar(30),in v_Address_Street_Box varchar(255),in v_City varchar(255),in v_Zip_Code varchar(5),in v_Electron_Month varchar(5),out rc boolean)
    DETERMINISTIC
begin
set @sql='select count(*) into @check1 from va_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check2 from qc_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check3 from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
if @check1>0 or @check2>0 or @check3>0 then
begin
set rc=0;
set @v_Government_Web_Address=replace(v_Government_Web_Address,'\'','\'');
set @v_Taken_from=replace(v_Taken_from,'\'','\'');
set @v_Updated_website=replace(v_Updated_website,'\'','\'');
set @v_Government_PhoneNumber=replace(v_Government_PhoneNumber,'\'','\'');
set @v_Address_Street_Box=replace(v_Address_Street_Box,'\'','\'');
set @v_City=replace(v_City,'\'','\'');
set @v_Zip_Code=replace(v_Zip_Code,'\'','\'');
set @v_Electron_Month=v_Electron_Month;
set @sql='update va_'||job_id||' set Electron_Month=@v_Electron_Month,Government_Web_Address=@v_Government_Web_Address,Taken_from=@v_Taken_from,Updated_website=@v_Updated_website,Government_PhoneNumber=@v_Government_PhoneNumber,Address_Street_Box=@v_Address_Street_Box,City=@v_City,Zip_Code=@v_Zip_Code where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='update qc_'||job_id||' set Electron_Month=@v_Electron_Month,Government_Web_Address=@v_Government_Web_Address,Taken_from=@v_Taken_from,Updated_website=@v_Updated_website,Government_PhoneNumber=@v_Government_PhoneNumber,Address_Street_Box=@v_Address_Street_Box,City=@v_City,Zip_Code=@v_Zip_Code where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='update fa_'||job_id||' set Electron_Month=@v_Electron_Month,Government_Web_Address=@v_Government_Web_Address,Taken_from=@v_Taken_from,Updated_website=@v_Updated_website,Government_PhoneNumber=@v_Government_PhoneNumber,Address_Street_Box=@v_Address_Street_Box,City=@v_City,Zip_Code=@v_Zip_Code where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
end;
else set rc=1; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "calc_prod"; CREATE PROCEDURE "calc_prod"(in job_id bigint)
    DETERMINISTIC
begin
drop table if exists prod;
CREATE TEMPORARY TABLE "prod" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT,
  "va_date" date,
  "va_time" time,
  "va_datetime" datetime,
  PRIMARY KEY ("id")
);
set @sql='insert into prod(va_date,va_time,va_datetime) select va_date,va_time,va_date||\' \'||va_time from qc_'||job_id||' order by va_date||\' \'||va_time';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
drop table if exists tmp_table;
create temporary table tmp_table as select * from prod;
alter table tmp_table add primary key(id);

select round(avg(UNIX_TIMESTAMP(b.va_datetime)-UNIX_TIMESTAMP(a.va_datetime)),2) into @diff
from tmp_table a join prod b on a.id+1=b.id;
update jobs set prod=@diff where jobs.job_id=job_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "calc_prod_for_va"; CREATE PROCEDURE "calc_prod_for_va"(in va_id bigint,start_daytime datetime,end_daytime datetime,ignore_time bigint,out different bigint)
    DETERMINISTIC
begin
drop table if exists prod;
CREATE TEMPORARY TABLE "prod" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT,
  "va_datetime" datetime,
  PRIMARY KEY ("id")
); 
insert into prod(va_datetime) select va_datetime from va_confirms_log where va_confirms_log.va_id=va_id and va_datetime between start_daytime and end_daytime;

drop table if exists tmp_table;
create temporary table tmp_table as select * from prod;
alter table tmp_table add primary key(id);

select round(avg(UNIX_TIMESTAMP(b.va_datetime)-UNIX_TIMESTAMP(a.va_datetime)),2) into different
from tmp_table a join prod b on (a.id+1=b.id and UNIX_TIMESTAMP(b.va_datetime)-UNIX_TIMESTAMP(a.va_datetime)<ignore_time);
 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "calulate_productivity"; CREATE PROCEDURE "calulate_productivity"(in user_id bigint(20),out total_prod decimal(20,2))
    DETERMINISTIC
begin
select round((sum(num_officials*prod))/(sum(num_officials)),2) into total_prod from jobs where va_id=user_id and prod!=0.00;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "DeleteEmptyJobs"; CREATE PROCEDURE "DeleteEmptyJobs"(out EmptyJobs int)
    DETERMINISTIC
begin
DECLARE i INT;
DECLARE TotalRecords BIGINT;
DECLARE counter INT;
set counter=0;
set i=0;
select count(*) into TotalRecords from jobs;
select TotalRecords;
WHILE counter < TotalRecords DO begin
	set @sql='select job_id into @job_id from jobs limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @government_id=null;
	set @sql='select distinct government_id into @government_id from va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	if @government_id is null then set @va=true; else set @va=false; end if;
	
	set @government_id=null;
	set @sql='select distinct government_id into @government_id from qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	if @government_id is null then set @qc=true; else set @qc=false; end if;
	
	set @government_id=null;
	set @sql='select distinct government_id into @government_id from fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	if @government_id is null then set @fa=true; else set @fa=false; end if;
	
	select count(*) into @done from ready where job_id=@job_id;
	if @va and @qc and @fa and @done=false then begin
	set @sql='drop table va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='drop table qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='drop table fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	delete from jobs where job_id=@job_id;
	set i=i+1; end; end if;
	end;
END WHILE; 
select i into EmptyJobs from dual;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "DeleteEmptyJobs2"; CREATE PROCEDURE "DeleteEmptyJobs2"()
    DETERMINISTIC
begin
call DeleteEmptyJobs(@i);
WHILE @i>0 DO 
call DeleteEmptyJobs(@i);
END WHILE; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "fa_confirm"; CREATE PROCEDURE "fa_confirm"(in job_id bigint,in g_id varchar(20),out rc boolean)
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
if @sign='fa' then
begin
set rc=0;
set @job_id=job_id;
set @sql='insert into ready select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,curdate(),g_timezone,o_timezone,va_date,va_time,r_state,notes,@job_id,Electron_Month from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check from qc_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check2 from va_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check3 from fa_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
if @check3=0 and @check2=0 and @check=0 then update jobs set state='done' where jobs.job_id=job_id; end if;
if @check3=0 and @check2!=0 then update jobs set state='va' where jobs.job_id=job_id; end if;
if @check3=0 and @check2=0 and @check!=0 then update jobs set state='qc' where jobs.job_id=job_id; end if;
end;
else set rc=1; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "fa_refresh"; CREATE PROCEDURE "fa_refresh"(in job_id bigint,in StartFrom int, in numRecords2Show int,OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as select d.government_id,count(*) as number_of_officials,count(if(d.Government_ID!=o.Government_ID or d.Role!=o.Role or d.Role_exists!=o.Role_exists or d.First_Name!=o.First_Name or d.Last_Name!=o.Last_Name or d.Government_Title!=o.Government_Title or d.Email_Address!=o.Email_Address or d.Part_of_Governing_Board!=o.Part_of_Governing_Board or d.Mailing_Street_Box!=o.Mailing_Street_Box or d.Mailing_Suite_Number!=o.Mailing_Suite_Number or d.Mailing_City!=o.Mailing_City or d.Mailing_State!=o.Mailing_State or d.Mailng_Zip_Code!=o.Mailng_Zip_Code or d.Phone_Number!=o.Phone_Number or d.Phone_Number_Ext!=o.Phone_Number_Ext,1,null)) as updated_officials from fa_'||job_id||' d join full_data o on o.official_id=d.official_id where d.role!=\'Top Elected Official\' group by d.government_id';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
alter table tmp_table add primary key (government_id);
drop table if exists tmp_table2;
set @sql='create temporary table tmp_table2 as select d.government_id,if(d.Government_ID!=o.Government_ID or d.Role!=o.Role or d.Role_exists!=o.Role_exists or d.First_Name!=o.First_Name or d.Last_Name!=o.Last_Name or d.Government_Title!=o.Government_Title or d.Email_Address!=o.Email_Address or d.Part_of_Governing_Board!=o.Part_of_Governing_Board or d.Mailing_Street_Box!=o.Mailing_Street_Box or d.Mailing_Suite_Number!=o.Mailing_Suite_Number or d.Mailing_City!=o.Mailing_City or d.Mailing_State!=o.Mailing_State or d.Mailng_Zip_Code!=o.Mailng_Zip_Code or d.Phone_Number!=o.Phone_Number or d.Phone_Number_Ext!=o.Phone_Number_Ext or o.Government_Web_Address!=d.Government_Web_Address or o.Taken_from!=d.Taken_from or o.Updated_website!=d.Updated_website or o.Government_PhoneNumber!=d.Government_PhoneNumber or d.Address_Street_Box!=o.Address_Street_Box or d.City!=o.City or d.Zip_Code!=o.Zip_Code or d.Electron_Month!=o.Electron_Month,1,0) as updated_officials from fa_'||job_id||' d join full_data o on o.official_id=d.official_id where d.role=\'Top Elected Official\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
alter table tmp_table2 add primary key (government_id);
drop table if exists tmp_table3;
create temporary table tmp_table3 as 
select a.government_id,a.updated_officials+b.updated_officials as updated_officials, number_of_officials+1 as number_of_officials, round((a.updated_officials+b.updated_officials)/(number_of_officials+1)*100,2) as updated_percentage
from tmp_table a join tmp_table2 b on a.government_id=b.government_id;
alter table tmp_table3 add primary key (government_id);
drop table if exists tmp_table4;
set @sql='create temporary table tmp_table4 as SELECT \'{"Government_Type":"\'||Government_Type
||\'","Form_of_Government":"\'||COALESCE(Form_of_Government,\'n/a\')
||\'","Governing_Body_Name":"\'||COALESCE(Governing_Body_Name,\'n/a\')
||\'","County_Name":"\'||COALESCE(County_Name,\'n/a\')
||\'","Population":"\'||if(Population is null or Population=0,\'n/a\',Population)
||\'","state":"\'||state
||\'","Government_ID":"\'||a.Government_ID
||\'","Government_Place_Name":"\'||COALESCE(Government_Place_Name,\'n/a\')
||\'","Government_Type_Name":"\'||COALESCE(Government_Type_Name,\'n/a\')
||\'","Government_Web_Address":"\'||COALESCE(Government_Web_Address,\'n/a\')
||\'","Taken_from":"\'||COALESCE(Taken_from,\'n/a\')
||\'","Role":"\'||Role
||\'","Role_exists":"\'||Role_exists
||\'","First_Name":"\'||First_Name
||\'","Last_Name":"\'||Last_Name
||\'","Government_Title":"\'||COALESCE(Government_Title,\'n/a\')
||\'","Email_Address":"\'||COALESCE(Email_Address,\'n/a\')
||\'","Email_Type":"\'||COALESCE(Email_Type,\'n/a\')
||\'","Part_of_Governing_Board":"\'||COALESCE(Part_of_Governing_Board,\'n/a\')
||\'","Updated_website":"\'||COALESCE(Updated_website,\'n/a\')
||\'","Mailing_Street_Box":"\'||COALESCE(Mailing_Street_Box,\'n/a\')
||\'","Mailing_Suite_Number":"\'||COALESCE(Mailing_Suite_Number,\'n/a\')
||\'","Mailing_City":"\'||COALESCE(Mailing_City,\'n/a\')
||\'","Mailing_State":"\'||COALESCE(Mailing_State,\'n/a\')
||\'","Mailng_Zip_Code":"\'||COALESCE(Mailng_Zip_Code,\'n/a\')
||\'","Phone_Number":"\'||COALESCE(Phone_Number,\'n/a\')
||\'","Phone_Number_Ext":"\'||COALESCE(Phone_Number_Ext,\'n/a\')
||\'","Address_Street_Box":"\'||COALESCE(Address_Street_Box,\'n/a\')
||\'","City":"\'||COALESCE(City,\'n/a\')
||\'","Zip_Code":"\'||COALESCE(Zip_Code,\'n/a\')
||\'","Government_PhoneNumber":"\'||COALESCE(Government_PhoneNumber,\'n/a\')
||\'","Official_ID":"\'||Official_ID
||\'","Date_Confirmed":"\'||COALESCE(Date_Confirmed,\'n/a\')
||\'","g_timezone":"\'||COALESCE(g_timezone,\'n/a\')
||\'","o_timezone":"\'||COALESCE(o_timezone,\'n/a\')
||\'","notes":"\'||COALESCE(notes,\'n/a\')
||\'","Electron_Month":"\'||COALESCE(Electron_Month,\'0\')
||\'","updated_officials":"\'||updated_officials
||\'","number_of_officials":"\'||number_of_officials
||\'","updated_percentage":"\'||updated_percentage
||\'"}\' as rec from fa_'||job_id||' a join tmp_table3 b on a.Government_ID=b.Government_ID'; 
set @sql=@sql||' order by Date_Confirmed,updated_officials,a.government_id LIMIT '||StartFrom||','||numRecords2Show;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table4;
set Result='['||Result||']';
set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "fa_send_back_qc"; CREATE PROCEDURE "fa_send_back_qc"(in job_id bigint,in g_id varchar(20), in note varchar(1000))
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
if @sign='fa' then
begin
select fa_id into @Writer_ID from jobs where job_id=jobs.job_id;
insert into notes(Government_ID,Job_ID,timestamp,txt,Writer_ID) values (g_id,job_id,now(),note,@Writer_ID);
set @note=replace(note,'\'','\'');
set @sql='update fa_'||job_id||' set notes=@note where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='insert into qc_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,va_date,va_time,r_state,notes,Electron_Month from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check from fa_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check2 from va_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
if @check=0 and @check2=0 then update jobs set state='qc' where jobs.job_id=job_id; end if;
if @check=0 and @check2!=0 then update jobs set state='va' where jobs.job_id=job_id; end if;
end; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "fa_send_back_va"; CREATE PROCEDURE "fa_send_back_va"(in job_id bigint,in g_id varchar(20), in note varchar(1000))
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
if @sign='fa' then
begin
select fa_id into @Writer_ID from jobs where job_id=jobs.job_id;
insert into notes(Government_ID,Job_ID,timestamp,txt,Writer_ID) values (g_id,job_id,now(),note,@Writer_ID);
set @note=replace(note,'\'','\'');
set @sql='update fa_'||job_id||' set notes=@note where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='insert into va_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,va_date,va_time,r_state,notes,Electron_Month from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check from fa_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
if @check=0 then update jobs set state='va' where jobs.job_id=job_id; end if;
end; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "fa_update_official"; CREATE PROCEDURE "fa_update_official"(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255))
    DETERMINISTIC
begin
set @v_Role_exists=replace(v_Role_exists,'\'','\'');
set @v_Government_Title=replace(v_Government_Title,'\'','\'');
set @v_First_Name=replace(v_First_Name,'\'','\'');
set @v_Last_Name=replace(v_Last_Name,'\'','\'');
set @v_Email_Address=replace(v_Email_Address,'\'','\'');
set @v_Part_of_Governing_Board=replace(v_Part_of_Governing_Board,'\'','\'');
set @v_Mailing_Street_Box=replace(v_Mailing_Street_Box,'\'','\'');
set @v_Mailing_Suite_Number=replace(v_Mailing_Suite_Number,'\'','\'');
set @v_Mailing_City=replace(v_Mailing_City,'\'','\'');
set @v_Mailng_Zip_Code=replace(v_Mailng_Zip_Code,'\'','\'');
set @v_Phone_Number=replace(v_Phone_Number,'\'','\'');
set @v_Phone_Number_Ext=replace(v_Phone_Number_Ext,'\'','\'');
set @sql='update fa_'||job_id||' set Role_exists=@v_Role_exists,Government_Title=@v_Government_Title,First_Name=@v_First_Name,Last_Name=@v_Last_Name,Email_Address=@v_Email_Address,Part_of_Governing_Board=@v_Part_of_Governing_Board,Mailing_Street_Box=@v_Mailing_Street_Box,Mailing_Suite_Number=@v_Mailing_Suite_Number,Mailing_City=@v_Mailing_City,Mailng_Zip_Code=@v_Mailng_Zip_Code,Phone_Number=@v_Phone_Number,Phone_Number_Ext=@v_Phone_Number_Ext where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "FindJob"; CREATE PROCEDURE "FindJob"(in gov_id varchar(14), out jobID BIGINT, out state varchar(50))
    DETERMINISTIC
BEGIN

DECLARE TotalSearches BIGINT;
DECLARE counter BIGINT;

select count(*) into TotalSearches from jobs;
set counter=0;
WHILE counter < TotalSearches DO begin
	set @sql='select job_id,state into @job_id,@state from jobs limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1; 
	
	set @sql='select count(*) into @check from va_'||@job_id||' where government_id='||gov_id;
	PREPARE statment FROM @sql;
	EXECUTE statment;
	DEALLOCATE PREPARE statment;
	if @check>0 then set jobID=@job_id; set state='va'; end if;
	
	set @sql='select count(*) into @check from qc_'||@job_id||' where government_id='||gov_id;
	PREPARE statment FROM @sql;
	EXECUTE statment;
	DEALLOCATE PREPARE statment;
	if @check>0 then set jobID=@job_id; set state='qc'; end if;
	
	set @sql='select count(*) into @check from fa_'||@job_id||' where government_id='||gov_id;
	PREPARE statment FROM @sql;
	EXECUTE statment;
	DEALLOCATE PREPARE statment;
	if @check>0 then set jobID=@job_id; set state='fa'; end if;
	
	end;
END WHILE; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "get_emplyee_info"; CREATE PROCEDURE "get_emplyee_info"(in id BIGINT, OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
set @id=id;
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as SELECT \'{"first_name":"\'||first_name
||\'","last_name":"\'||last_name
||\'","password":"\'||password
||\'","access_level":"\'||access_level
||\'","primary_role":"\'||primary_role
||\'"}\' as rec from employees where @id=employees.emp_id';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table;

set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "get_history"; CREATE PROCEDURE "get_history"(in j_id BIGINT,in gov_id BIGINT, OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
set @gov_id=gov_id;
set @j_id=j_id;
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as SELECT \'{"Government_ID":"\'||Government_ID
||\'","Job_ID":"\'||Job_ID
||\'","timestamp":"\'||timestamp
||\'","txt":"\'||txt
||\'","Writer_ID":"\'||Writer_ID
||\'"}\' as rec from notes where @j_id=notes.job_id and @gov_id=notes.Government_ID';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table;
set Result='['||Result||']';
set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "get_history_for_government"; CREATE PROCEDURE "get_history_for_government"(in gov_id BIGINT, OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
set @gov_id=gov_id;
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as SELECT \'{"Government_ID":"\'||Government_ID
||\'","Job_ID":"\'||Job_ID
||\'","timestamp":"\'||timestamp
||\'","txt":"\'||txt
||\'","Writer_ID":"\'||Writer_ID
||\'"}\' as rec from notes where @gov_id=notes.Government_ID';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table;
set Result='['||Result||']';
set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "initiator_add_gov"; CREATE PROCEDURE "initiator_add_gov"(in g_id varchar(14),in job_id bigint,out rc boolean)
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
select distinct r_state into @r_state from full_data where government_id=g_id;
if @sign='' and @r_state='unallocated' and length(g_id)=14 then
begin
set rc=0;
update full_data set r_state='allocated' where government_id=g_id and r_state='unallocated';
set @sql='insert into va_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,va_date,va_time,r_state,notes,Electron_Month from full_data where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
select count(*) into @num from full_data where government_id=g_id;
update jobs set num_officials=num_officials+@num,num_govs=num_govs+1 where jobs.job_id=job_id;
end; 
else set rc=1; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "initiator_create_job"; CREATE PROCEDURE "initiator_create_job"(IN va_id BIGINT,IN qc_id BIGINT,IN fa_id BIGINT,OUT job_id BIGINT,OUT rc boolean)
    DETERMINISTIC
begin
select count(*) into @check from employees where emp_id=va_id;
if @check=1 then
begin
set rc=0;
insert into jobs(va_id,num_officials,num_govs,state,qc_id,fa_id) values (va_id,0,0,'',qc_id,fa_id);
SELECT LAST_INSERT_ID() into job_id from dual;
set @sql='create table va_'||job_id||' as select * from full_data where 1=2';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='create table qc_'||job_id||' as select * from full_data where 1=2';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='create table fa_'||job_id||' as select * from full_data where 1=2';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='alter table qc_'||job_id||' add primary key (official_id)';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='alter table fa_'||job_id||' add primary key (official_id)';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='alter table va_'||job_id||' add primary key (official_id)';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='alter table qc_'||job_id||' add key (government_id)';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='alter table fa_'||job_id||' add key (government_id)';
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
end;
else set rc=1; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "initiator_delete_job"; CREATE PROCEDURE "initiator_delete_job"(in j_id BIGINT)
    DETERMINISTIC
begin
delete from jobs where job_id=j_id;
drop table if exists govs;
create table govs(government_id varchar(14),primary key(government_id));
set @sql='replace into govs select distinct government_id from va_'||j_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='replace into govs select distinct government_id from qc_'||j_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='replace into govs select distinct government_id from fa_'||j_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='drop table va_'||j_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='drop table qc_'||j_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='drop table fa_'||j_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
update full_data set r_state='unallocated' where government_id in (select government_id from govs);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "initiator_job_ready"; CREATE PROCEDURE "initiator_job_ready"(in id BIGINT)
    DETERMINISTIC
begin
update jobs set state='va' where job_id=id;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "initiator_refresh"; CREATE PROCEDURE "initiator_refresh"(in StartFrom int, in numRecords2Show int, OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
drop table if exists v_initiator_A;
create temporary table v_initiator_A as 
select Government_ID,count(*) as number_of_officials
from full_data
where r_state='unallocated'
group by Government_ID;
alter table v_initiator_A add primary key (government_id);
drop table if exists v_initiator_B;
create temporary table v_initiator_B as 
select Government_Type,date_confirmed,g_timezone,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Electron_Month
from full_data
where r_state='unallocated' and role='Top Elected Official';
alter table v_initiator_B add primary key (government_id);
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as SELECT \'{"Government_ID":"\'||a.Government_ID
||\'","Government_Type":"\'||Government_Type
||\'","State":"\'||State
||\'","Government_Place_Name":"\'||COALESCE(Government_Place_Name,\'n/a\')
||\'","Population":"\'||if(Population is null or Population=0,\'n/a\',Population)
||\'","County_Name":"\'||County_Name
||\'","Government_Type_Name":"\'||Government_Type_Name
||\'","Government_Web_Address":"\'||Government_Web_Address
||\'","number_of_officials":"\'||number_of_officials
||\'","g_timezone":"\'||g_timezone
||\'","date_confirmed":"\'||date_confirmed
||\'","Electron_Month":"\'||Electron_Month
||\'"}\' as rec from v_initiator_A a join v_initiator_B b on a.government_id=b.government_id'; 
set @sql=@sql||' order by date_confirmed,g_timezone,state,Government_Type LIMIT '||StartFrom||','||numRecords2Show;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table;
set Result='['||Result||']';
set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "initiator_remove_gov"; CREATE PROCEDURE "initiator_remove_gov"(in g_id varchar(14),in job_id bigint)
    DETERMINISTIC
begin
update full_data set r_state='unallocated' where government_id=g_id;
set @sql='delete from va_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from qc_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from fa_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
delete from ready where government_id=g_id;
select count(*) into @num from full_data where government_id=g_id;
update jobs set num_officials=num_officials-@num,num_govs=num_govs-1 where jobs.job_id=job_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "klaunch"; CREATE PROCEDURE "klaunch"()
    DETERMINISTIC
begin
DECLARE TotalRecords BIGINT;
DECLARE counter BIGINT;
drop table if exists m_ready;
create table m_ready as select * from ready;
truncate table ready;
select count(*) into TotalRecords from jobs where state='done';
set counter=0;
WHILE counter < TotalRecords DO begin
	set @sql='select job_id into @job_id from jobs where state=\'done\' limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @sql='drop table if exists va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='drop table if exists qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='drop table if exists fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	end;
END WHILE; 

drop table if exists govs;
create table govs(government_id varchar(14),primary key(government_id));
select count(*) into TotalRecords from jobs where state!='done';
set counter=0;
WHILE counter < TotalRecords DO begin
	set @sql='select job_id into @job_id from jobs where state!=\'done\' limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @sql='replace into govs select distinct government_id from va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='replace into govs select distinct government_id from qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='replace into govs select distinct government_id from fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	end;
END WHILE;
update full_data set r_state='allocated' where government_id in (select government_id from govs);
delete from jobs where state='done';

CALL government2.SearchSummary('NickPA','byText=&',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists);
CALL government2.SearchDetails(25,'NickPA',@Result);
UPDATE government2.database_summary SET Total_Available_Emails = @Num_Matched_Emails, Total_Gov_Officials = @Num_Matched_Officials, Total_Phone_Number = @Phone_Numbers, Total_Mailing_Street_Box = @Mailing_Addresses, Total_Governments = @Num_Matched_Governments,  JSON_GOVERNMENTS = @JSON_GOVERNMENTS, JSON_OFFICIALS = @JSON_OFFICIALS, JSON_PREVIEW = @Result;
CALL government2.Delete_Research_Tables('NickPA');
CALL government2.Delete_Research_Tables('createpa');
CALL government2.SearchSummary('createpa','byText=&top_elected=0',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists);
call government2.Bulid_Research_Tables('createpa',@exists);

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'PIPES_AS_CONCAT' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS `launch`; CREATE PROCEDURE `launch`()
    DETERMINISTIC
begin
DECLARE TotalRecords BIGINT;
DECLARE counter BIGINT;
call government.add_new_data_icube(@govs,@off_upd,@off_new);
replace into government.government_officials
select Official_ID ,Government_ID ,Role ,Role_exists ,replace(replace(First_Name,char(9),''),char(34),'') ,replace(replace(Last_Name,char(9),''),char(34),'') ,replace(replace(Government_Title,char(9),''),char(34),'') ,Elected ,Length_of_Elected_Term ,Term_Month_End_Date ,Term_Year_End_Date ,Part_of_Governing_Board ,replace(replace(Mailing_Street_Box,char(9),''),char(34),'') ,replace(replace(Mailing_Suite_Number,char(9),''),char(34),'') ,replace(replace(Mailing_City,char(9),''),char(34),'') ,replace(replace(Mailing_State,char(9),''),char(34),'') ,replace(replace(Mailng_Zip_Code,char(9),''),char(34),'') ,replace(replace(Phone_Number,char(9),''),char(34),'') ,replace(replace(Phone_Number_Ext,char(9),''),char(34),'') ,replace(replace(Email_Address,char(9),''),char(34),'') ,replace(replace(Email_Type,char(9),''),char(34),'') ,replace(replace(Special_Notes,char(9),''),char(34),'') ,Date_Confirmed ,ID ,E_mail_exists ,phone_exists ,Mailing_Street_Box_exists
from government.government_officials;
replace into government.government
select Government_ID ,Government_Place_Name ,Government_Type_Name ,replace(replace(Government_Web_Address,char(9),''),char(34),'') ,replace(replace(Government_PhoneNumber,char(9),''),char(34),'') ,Dept_Name_for_phone_number ,Government_Type ,Form_of_Government ,Governing_Body_Name ,County_Name ,Fiscal_Year_End_Date ,Original_Incorporation_Date ,Type_Code ,County_Code ,Unit_Code ,FIPS_State ,FIPS_County ,FIPS_Place ,Population ,Population_Category ,Population_2007 ,Population_2007_Category ,Taken_from ,Updated_website ,state ,replace(replace(Address_Street_Box,char(9),''),char(34),'') ,replace(replace(City,char(9),''),char(34),'') ,Zip_Code ,Gov_ID,Electron_Month
from government.government;
call government.build_all_data;


drop table if exists tmp_table;
create table tmp_table as
select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,g.Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,g.Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,COALESCE(TimeZone,50) as g_timezone,Electron_Month
from (government.government g join government.government_officials o on g.government_id=o.government_id) left join government.zip_code2 z on z.ZipCode=g.Zip_Code;

drop table if exists full_data;
create table full_data as
select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,g.Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,g.Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,COALESCE(TimeZone,50) as o_timezone,STR_TO_DATE('1970-01-01','%Y-%m-%d') as va_date,STR_TO_DATE('01:00:00','%h:%i:%s') as va_time,'unallocated' as r_state,'' as notes,Electron_Month
from tmp_table g left join government.zip_code2 z on z.ZipCode=g.Mailng_Zip_Code;

update full_data set va_date=null,va_time=null;
alter table full_data change notes notes varchar(1000);
alter table full_data add primary key (official_id);
alter table full_data add key (government_id);
drop table if exists m_ready;
create table m_ready as select * from ready;
truncate table ready;
select count(*) into TotalRecords from jobs where state='done';
set counter=0;
WHILE counter < TotalRecords DO begin
set @sql='select job_id into @job_id from jobs where state=\'done\' limit '||counter||',1';
prepare q from @sql;
execute q;
deallocate prepare q;
set counter=counter+1;
set @sql='drop table if exists va_'||@job_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='drop table if exists qc_'||@job_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='drop table if exists fa_'||@job_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
end;
END WHILE; 

drop table if exists govs;
create table govs(government_id varchar(14),primary key(government_id));
select count(*) into TotalRecords from jobs where state!='done';
set counter=0;
WHILE counter < TotalRecords DO begin
set @sql='select job_id into @job_id from jobs where state!=\'done\' limit '||counter||',1';
prepare q from @sql;
execute q;
deallocate prepare q;
set counter=counter+1;
set @sql='replace into govs select distinct government_id from va_'||@job_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='replace into govs select distinct government_id from qc_'||@job_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
set @sql='replace into govs select distinct government_id from fa_'||@job_id;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
end;
END WHILE;
update full_data set r_state='allocated' where government_id in (select government_id from govs);
delete from jobs where state='done';

CALL government.SearchSummary2('NickPA','byText=&',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists);
CALL government.SearchDetails2(25,'NickPA',@Result);
UPDATE government.database_summary SET Total_Available_Emails = @Num_Matched_Emails, Total_Gov_Officials = @Num_Matched_Officials, Total_Phone_Number = @Phone_Numbers, Total_Mailing_Street_Box = @Mailing_Addresses, Total_Governments = @Num_Matched_Governments,  JSON_GOVERNMENTS = @JSON_GOVERNMENTS, JSON_OFFICIALS = @JSON_OFFICIALS, JSON_PREVIEW = @Result;
CALL government.Delete_Research_Tables('NickPA');
CALL government.Delete_Research_Tables('createpa');
CALL government.SearchSummary2('createpa','byText=&top_elected=0',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists);
call government.Bulid_Research_Tables2('createpa',@exists);

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "Proj2"; CREATE PROCEDURE "Proj2"(out EmptyJobs int)
    DETERMINISTIC
begin
DECLARE i INT;
DECLARE TotalRecords BIGINT;
DECLARE counter INT;
drop table if exists tmp_table;
CREATE TEMPORARY TABLE tmp_table as
select table_name from information_schema.tables where TABLE_SCHEMA='icube_government' and (table_name like 'va_%' or table_name like 'qc_%' or table_name like 'fa_%') and table_name not in ('va_confirms_log','va_gov_confirms_log','valog');
set counter=0;
set i=0;
select count(*) into TotalRecords from tmp_table;
select TotalRecords;
WHILE counter < TotalRecords DO
	set @sql='select table_name into @table_name from tmp_table limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @sql='alter table icube_government2.'||@table_name||' add "Electron_Month" varchar(2) DEFAULT \'0\'';
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	deallocate prepare stmt;
	set i=i+1;
END WHILE; 
select i into EmptyJobs from dual;


end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "qc_confirm"; CREATE PROCEDURE "qc_confirm"(in job_id bigint,in g_id varchar(20),out rc boolean)
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
if @sign='qc' then
begin
set rc=0;
set @sql='insert into fa_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,va_date,va_time,r_state,notes,Electron_Month from qc_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from qc_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check from qc_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check2 from va_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
if @check=0 and @check2=0 then update jobs set state='fa' where jobs.job_id=job_id; end if;
if @check=0 and @check2!=0 then update jobs set state='va' where jobs.job_id=job_id; end if;
end;
else set rc=1; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "qc_refresh"; CREATE PROCEDURE "qc_refresh"(in job_id bigint,in StartFrom int, in numRecords2Show int,OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as select d.government_id,count(*) as number_of_officials,count(if(d.Government_ID!=o.Government_ID or d.Role!=o.Role or d.Role_exists!=o.Role_exists or d.First_Name!=o.First_Name or d.Last_Name!=o.Last_Name or d.Government_Title!=o.Government_Title or d.Email_Address!=o.Email_Address or d.Part_of_Governing_Board!=o.Part_of_Governing_Board or d.Mailing_Street_Box!=o.Mailing_Street_Box or d.Mailing_Suite_Number!=o.Mailing_Suite_Number or d.Mailing_City!=o.Mailing_City or d.Mailing_State!=o.Mailing_State or d.Mailng_Zip_Code!=o.Mailng_Zip_Code or d.Phone_Number!=o.Phone_Number or d.Phone_Number_Ext!=o.Phone_Number_Ext,1,null)) as updated_officials from qc_'||job_id||' d join full_data o on o.official_id=d.official_id where d.role!=\'Top Elected Official\' group by d.government_id';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
alter table tmp_table add primary key (government_id);
drop table if exists tmp_table2;
set @sql='create temporary table tmp_table2 as select d.government_id,if(d.Government_ID!=o.Government_ID or d.Role!=o.Role or d.Role_exists!=o.Role_exists or d.First_Name!=o.First_Name or d.Last_Name!=o.Last_Name or d.Government_Title!=o.Government_Title or d.Email_Address!=o.Email_Address or d.Part_of_Governing_Board!=o.Part_of_Governing_Board or d.Mailing_Street_Box!=o.Mailing_Street_Box or d.Mailing_Suite_Number!=o.Mailing_Suite_Number or d.Mailing_City!=o.Mailing_City or d.Mailing_State!=o.Mailing_State or d.Mailng_Zip_Code!=o.Mailng_Zip_Code or d.Phone_Number!=o.Phone_Number or d.Phone_Number_Ext!=o.Phone_Number_Ext or o.Government_Web_Address!=d.Government_Web_Address or o.Taken_from!=d.Taken_from or o.Updated_website!=d.Updated_website or o.Government_PhoneNumber!=d.Government_PhoneNumber or d.Address_Street_Box!=o.Address_Street_Box or d.City!=o.City or d.Zip_Code!=o.Zip_Code or d.Electron_Month!=o.Electron_Month,1,0) as updated_officials from qc_'||job_id||' d join full_data o on o.official_id=d.official_id where d.role=\'Top Elected Official\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
alter table tmp_table2 add primary key (government_id);
drop table if exists tmp_table3;
create temporary table tmp_table3 as 
select a.government_id,a.updated_officials+b.updated_officials as updated_officials, number_of_officials+1 as number_of_officials, round((a.updated_officials+b.updated_officials)/(number_of_officials+1)*100,2) as updated_percentage
from tmp_table a join tmp_table2 b on a.government_id=b.government_id;
alter table tmp_table3 add primary key (government_id);
drop table if exists tmp_table4;
set @sql='create temporary table tmp_table4 as SELECT \'{"Government_Type":"\'||Government_Type
||\'","Form_of_Government":"\'||COALESCE(Form_of_Government,\'n/a\')
||\'","Governing_Body_Name":"\'||COALESCE(Governing_Body_Name,\'n/a\')
||\'","County_Name":"\'||COALESCE(County_Name,\'n/a\')
||\'","Population":"\'||if(Population is null or Population=0,\'n/a\',Population)
||\'","state":"\'||state
||\'","Government_ID":"\'||a.Government_ID
||\'","Government_Place_Name":"\'||COALESCE(Government_Place_Name,\'n/a\')
||\'","Government_Type_Name":"\'||COALESCE(Government_Type_Name,\'n/a\')
||\'","Government_Web_Address":"\'||COALESCE(Government_Web_Address,\'n/a\')
||\'","Taken_from":"\'||COALESCE(Taken_from,\'n/a\')
||\'","Role":"\'||Role
||\'","Role_exists":"\'||Role_exists
||\'","First_Name":"\'||First_Name
||\'","Last_Name":"\'||Last_Name
||\'","Government_Title":"\'||COALESCE(Government_Title,\'n/a\')
||\'","Email_Address":"\'||COALESCE(Email_Address,\'n/a\')
||\'","Email_Type":"\'||COALESCE(Email_Type,\'n/a\')
||\'","Part_of_Governing_Board":"\'||COALESCE(Part_of_Governing_Board,\'n/a\')
||\'","Updated_website":"\'||COALESCE(Updated_website,\'n/a\')
||\'","Mailing_Street_Box":"\'||COALESCE(Mailing_Street_Box,\'n/a\')
||\'","Mailing_Suite_Number":"\'||COALESCE(Mailing_Suite_Number,\'n/a\')
||\'","Mailing_City":"\'||COALESCE(Mailing_City,\'n/a\')
||\'","Mailing_State":"\'||COALESCE(Mailing_State,\'n/a\')
||\'","Mailng_Zip_Code":"\'||COALESCE(Mailng_Zip_Code,\'n/a\')
||\'","Phone_Number":"\'||COALESCE(Phone_Number,\'n/a\')
||\'","Phone_Number_Ext":"\'||COALESCE(Phone_Number_Ext,\'n/a\')
||\'","Address_Street_Box":"\'||COALESCE(Address_Street_Box,\'n/a\')
||\'","City":"\'||COALESCE(City,\'n/a\')
||\'","Zip_Code":"\'||COALESCE(Zip_Code,\'n/a\')
||\'","Government_PhoneNumber":"\'||COALESCE(Government_PhoneNumber,\'n/a\')
||\'","Official_ID":"\'||Official_ID
||\'","Date_Confirmed":"\'||COALESCE(Date_Confirmed,\'n/a\')
||\'","g_timezone":"\'||COALESCE(g_timezone,\'n/a\')
||\'","o_timezone":"\'||COALESCE(o_timezone,\'n/a\')
||\'","notes":"\'||COALESCE(notes,\'n/a\')
||\'","Electron_Month":"\'||COALESCE(Electron_Month,\'0\')
||\'","updated_officials":"\'||updated_officials
||\'","number_of_officials":"\'||number_of_officials
||\'","updated_percentage":"\'||updated_percentage
||\'"}\' as rec from qc_'||job_id||' a join tmp_table3 b on a.Government_ID=b.Government_ID'; 
set @sql=@sql||' order by state,Government_Type,updated_officials,a.government_id LIMIT '||StartFrom||','||numRecords2Show;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table4;
set Result='['||Result||']';
set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "qc_send_back"; CREATE PROCEDURE "qc_send_back"(in job_id bigint,in g_id varchar(20), in note varchar(1000))
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
if @sign='qc' then
begin
select qc_id into @Writer_ID from jobs where job_id=jobs.job_id;
insert into notes(Government_ID,Job_ID,timestamp,txt,Writer_ID) values (g_id,job_id,now(),note,@Writer_ID);
set @note=replace(note,'\'','\'');
set @sql='update qc_'||job_id||' set notes=@note where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='insert into va_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,va_date,va_time,r_state,notes,Electron_Month from qc_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from qc_'||job_id||' where government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @check from qc_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;

if @check=0 then update jobs set state='va' where jobs.job_id=job_id; end if;
end; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "qc_update_official"; CREATE PROCEDURE "qc_update_official"(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255))
    DETERMINISTIC
begin
set @v_Role_exists=replace(v_Role_exists,'\'','\'');
set @v_Government_Title=replace(v_Government_Title,'\'','\'');
set @v_First_Name=replace(v_First_Name,'\'','\'');
set @v_Last_Name=replace(v_Last_Name,'\'','\'');
set @v_Email_Address=replace(v_Email_Address,'\'','\'');
set @v_Part_of_Governing_Board=replace(v_Part_of_Governing_Board,'\'','\'');
set @v_Mailing_Street_Box=replace(v_Mailing_Street_Box,'\'','\'');
set @v_Mailing_Suite_Number=replace(v_Mailing_Suite_Number,'\'','\'');
set @v_Mailing_City=replace(v_Mailing_City,'\'','\'');
set @v_Mailng_Zip_Code=replace(v_Mailng_Zip_Code,'\'','\'');
set @v_Phone_Number=replace(v_Phone_Number,'\'','\'');
set @v_Phone_Number_Ext=replace(v_Phone_Number_Ext,'\'','\'');
set @sql='update qc_'||job_id||' set Role_exists=@v_Role_exists,Government_Title=@v_Government_Title,First_Name=@v_First_Name,Last_Name=@v_Last_Name,Email_Address=@v_Email_Address,Part_of_Governing_Board=@v_Part_of_Governing_Board,Mailing_Street_Box=@v_Mailing_Street_Box,Mailing_Suite_Number=@v_Mailing_Suite_Number,Mailing_City=@v_Mailing_City,Mailng_Zip_Code=@v_Mailng_Zip_Code,Phone_Number=@v_Phone_Number,Phone_Number_Ext=@v_Phone_Number_Ext where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "remove_employee"; CREATE PROCEDURE "remove_employee"(IN EmpID BIGINT)
    DETERMINISTIC
begin
delete from employees where emp_id=EmpID;
delete from jobs where EmpID=va_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "ShowEmptyJobs"; CREATE PROCEDURE "ShowEmptyJobs"(out EmptyJobs int)
    DETERMINISTIC
begin
DECLARE i INT;
DECLARE TotalRecords BIGINT;
DECLARE counter INT;
set counter=0;
set i=0;
select count(*) into TotalRecords from jobs;
select TotalRecords;
WHILE counter < TotalRecords DO begin
	set @sql='select job_id into @job_id from jobs limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @government_id=null;
	set @sql='select distinct government_id into @government_id from va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	if @government_id is null then set @va=true; else set @va=false; end if;
	
	set @government_id=null;
	set @sql='select distinct government_id into @government_id from qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	if @government_id is null then set @qc=true; else set @qc=false; end if;
	
	set @government_id=null;
	set @sql='select distinct government_id into @government_id from fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	if @government_id is null then set @fa=true; else set @fa=false; end if;
	
	select count(*) into @done from ready where job_id=@job_id;
	if @va and @qc and @fa and @done=false then begin
	select @job_id;
	set i=i+1; end; end if;
	end;
END WHILE; 
select i into EmptyJobs from dual;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "show_state"; CREATE PROCEDURE "show_state"(in job_id bigint,out st varchar(50))
    DETERMINISTIC
begin
select state into st from jobs where jobs.job_id=job_id;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "va_add_new_official"; CREATE PROCEDURE "va_add_new_official"(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255),in v_Mailing_State varchar(80),in g_id varchar(100),out rc boolean)
    DETERMINISTIC
begin
set @sql='select count(*) into @c1 from va_'||job_id||' where role=\'Top Elected Official\' and government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='select count(*) into @c2 from qc_'||job_id||' where role=\'Top Elected Official\' and government_id=\''||g_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
if @c1=1 then
	begin
	set @sql='select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Updated_website,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Electron_Month into @Government_Type,@Form_of_Government,@Governing_Body_Name,@County_Name,@Population,@state,@Government_Place_Name,@Government_Type_Name,@Government_Web_Address,@Taken_from,@Updated_website,@Address_Street_Box,@City,@Zip_Code,@Government_PhoneNumber,@Electron_Month from va_'||job_id||' where role=\'Top Elected Official\' and government_id=\''||g_id||'\'';
	PREPARE statment FROM @sql;
	EXECUTE statment;
	DEALLOCATE PREPARE statment;
	end;
end if;
if @c2=1 then
	begin
	set @sql='select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Updated_website,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Electron_Month into @Government_Type,@Form_of_Government,@Governing_Body_Name,@County_Name,@Population,@state,@Government_Place_Name,@Government_Type_Name,@Government_Web_Address,@Taken_from,@Updated_website,@Address_Street_Box,@City,@Zip_Code,@Government_PhoneNumber,@Electron_Month from qc_'||job_id||' where role=\'Top Elected Official\' and government_id=\''||g_id||'\'';
	PREPARE statment FROM @sql;
	EXECUTE statment;
	DEALLOCATE PREPARE statment;
	end;
end if;
if @c1=1 or @c2=1 then
begin
	set rc=0; 
	set @g_id=replace(g_id,'\'','\'');
	set @v_Role_exists=replace(v_Role_exists,'\'','\'');
	set @v_First_Name=replace(v_First_Name,'\'','\'');
	set @v_Last_Name=replace(v_Last_Name,'\'','\'');
	set @v_Government_Title=replace(v_Government_Title,'\'','\'');
	set @v_Email_Address=replace(v_Email_Address,'\'','\'');
	set @v_Part_of_Governing_Board=replace(v_Part_of_Governing_Board,'\'','\'');
	set @v_Mailing_Street_Box=replace(v_Mailing_Street_Box,'\'','\'');
	set @v_Mailing_Suite_Number=replace(v_Mailing_Suite_Number,'\'','\'');
	set @v_Mailing_City=replace(v_Mailing_City,'\'','\'');
	set @v_Mailing_State=replace(v_Mailing_State,'\'','\'');
	set @v_Mailng_Zip_Code=replace(v_Mailng_Zip_Code,'\'','\'');
	set @v_Phone_Number=replace(v_Phone_Number,'\'','\'');
	set @v_Phone_Number_Ext=replace(v_Phone_Number_Ext,'\'','\'');
	set @o_id=replace(o_id,'\'','\'');
	set @sql='insert into va_'||job_id||' values (@Government_Type,@Form_of_Government,@Governing_Body_Name,@County_Name,@Population,@state,@g_id,@Government_Place_Name,@Government_Type_Name,@Government_Web_Address,@Taken_from,\'Governing Board Member\',@v_Role_exists,@v_First_Name,@v_Last_Name,@v_Government_Title,@v_Email_Address,\'\',@v_Part_of_Governing_Board,@Updated_website,@v_Mailing_Street_Box,@v_Mailing_Suite_Number,@v_Mailing_City,@v_Mailing_State,@v_Mailng_Zip_Code,@v_Phone_Number,@v_Phone_Number_Ext,@Address_Street_Box,@City,@Zip_Code,@Government_PhoneNumber,@o_id,null,null,null,null,null,\'\',\'NEW\',@Electron_Month)';
	PREPARE statment FROM @sql;
	EXECUTE statment;
	DEALLOCATE PREPARE statment;
end;
else set rc=1;
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "va_change_mind"; CREATE PROCEDURE "va_change_mind"(in o_id varchar(100),in job_id bigint)
    DETERMINISTIC
begin
set @sql='insert into va_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,va_date,va_time,r_state,notes,Electron_Month from qc_'||job_id||' where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
set @sql='delete from qc_'||job_id||' where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;

update jobs set state='va' where jobs.job_id=job_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "va_confirm"; CREATE PROCEDURE "va_confirm"(in o_id varchar(100),in job_id bigint,out rc boolean)
    DETERMINISTIC
begin
select state into @sign from jobs where jobs.job_id=job_id;
if @sign='va' then
begin
set rc=0;
set @sql='insert into qc_'||job_id||' select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,o_timezone,if(va_date is null,curdate(),va_date),if(va_time is null,curtime(),va_time),r_state,notes,Electron_Month from va_'||job_id||' where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;

set @sql='select va_date into @va_date from va_'||job_id||' where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;

if @va_date is null then
	begin
	select va_id into @va_id from jobs where jobs.job_id=job_id;
	insert into va_confirms_log(va_id,va_datetime) values (@va_id,now());
	end;
end if;

set @sql='delete from va_'||job_id||' where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;

set @sql='select count(*) into @check from va_'||job_id;
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;

if @check=0 then 
	begin
	update jobs set state='qc' where jobs.job_id=job_id; 
	select prod into @prod from jobs where jobs.job_id=job_id;
	if @prod=0.00 then 
	begin 
	call calc_prod(job_id);
	select va_id into @va_id from jobs where jobs.job_id=job_id;
	insert into va_gov_confirms_log(va_id,va_datetime) values (@va_id,now());
	end;
	end if;
	end; end if;
end; 
else set rc=1; end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "va_refresh"; CREATE PROCEDURE "va_refresh"(in job_id bigint,in StartFrom int, in numRecords2Show int,OUT Result MEDIUMTEXT)
    DETERMINISTIC
begin
drop table if exists tmp_table;
set @sql='create temporary table tmp_table as SELECT \'{"Government_Type":"\'||Government_Type
||\'","Form_of_Government":"\'||COALESCE(Form_of_Government,\'n/a\')
||\'","Governing_Body_Name":"\'||COALESCE(Governing_Body_Name,\'n/a\')
||\'","County_Name":"\'||COALESCE(County_Name,\'n/a\')
||\'","Population":"\'||if(Population is null or Population=0,\'n/a\',Population)
||\'","state":"\'||state
||\'","Government_ID":"\'||Government_ID
||\'","Government_Place_Name":"\'||COALESCE(Government_Place_Name,\'n/a\')
||\'","Government_Type_Name":"\'||COALESCE(Government_Type_Name,\'n/a\')
||\'","Government_Web_Address":"\'||COALESCE(Government_Web_Address,\'n/a\')
||\'","Taken_from":"\'||COALESCE(Taken_from,\'n/a\')
||\'","Role":"\'||Role
||\'","Role_exists":"\'||Role_exists
||\'","First_Name":"\'||First_Name
||\'","Last_Name":"\'||Last_Name
||\'","Government_Title":"\'||COALESCE(Government_Title,\'n/a\')
||\'","Email_Address":"\'||COALESCE(Email_Address,\'n/a\')
||\'","Email_Type":"\'||COALESCE(Email_Type,\'n/a\')
||\'","Part_of_Governing_Board":"\'||COALESCE(Part_of_Governing_Board,\'n/a\')
||\'","Updated_website":"\'||COALESCE(Updated_website,\'n/a\')
||\'","Mailing_Street_Box":"\'||COALESCE(Mailing_Street_Box,\'n/a\')
||\'","Mailing_Suite_Number":"\'||COALESCE(Mailing_Suite_Number,\'n/a\')
||\'","Mailing_City":"\'||COALESCE(Mailing_City,\'n/a\')
||\'","Mailing_State":"\'||COALESCE(Mailing_State,\'n/a\')
||\'","Mailng_Zip_Code":"\'||COALESCE(Mailng_Zip_Code,\'n/a\')
||\'","Phone_Number":"\'||COALESCE(Phone_Number,\'n/a\')
||\'","Phone_Number_Ext":"\'||COALESCE(Phone_Number_Ext,\'n/a\')
||\'","Address_Street_Box":"\'||COALESCE(Address_Street_Box,\'n/a\')
||\'","City":"\'||COALESCE(City,\'n/a\')
||\'","Zip_Code":"\'||COALESCE(Zip_Code,\'n/a\')
||\'","Government_PhoneNumber":"\'||COALESCE(Government_PhoneNumber,\'n/a\')
||\'","Official_ID":"\'||Official_ID
||\'","Date_Confirmed":"\'||COALESCE(Date_Confirmed,\'n/a\')
||\'","g_timezone":"\'||COALESCE(g_timezone,\'n/a\')
||\'","o_timezone":"\'||COALESCE(o_timezone,\'n/a\')
||\'","notes":"\'||COALESCE(notes,\'n/a\')
||\'","Electron_Month":"\'||COALESCE(Electron_Month,\'0\')
||\'"}\' as rec from va_'||job_id; 
set @sql=@sql||' order by g_timezone,state,Government_Type,County_Name,Government_ID LIMIT '||StartFrom||','||numRecords2Show;
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
select group_concat(Rec) into Result from tmp_table;
set Result='['||Result||']';
set Result=replace(Result,'\n','');
set Result=replace(Result,'\r','');
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "va_update_official"; CREATE PROCEDURE "va_update_official"(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255))
    DETERMINISTIC
begin
set @v_Role_exists=replace(v_Role_exists,'\'','\'');
set @v_Government_Title=replace(v_Government_Title,'\'','\'');
set @v_First_Name=replace(v_First_Name,'\'','\'');
set @v_Last_Name=replace(v_Last_Name,'\'','\'');
set @v_Email_Address=replace(v_Email_Address,'\'','\'');
set @v_Part_of_Governing_Board=replace(v_Part_of_Governing_Board,'\'','\'');
set @v_Mailing_Street_Box=replace(v_Mailing_Street_Box,'\'','\'');
set @v_Mailing_Suite_Number=replace(v_Mailing_Suite_Number,'\'','\'');
set @v_Mailing_City=replace(v_Mailing_City,'\'','\'');
set @v_Mailng_Zip_Code=replace(v_Mailng_Zip_Code,'\'','\'');
set @v_Phone_Number=replace(v_Phone_Number,'\'','\'');
set @v_Phone_Number_Ext=replace(v_Phone_Number_Ext,'\'','\'');
set @sql='update va_'||job_id||' set Role_exists=@v_Role_exists,Government_Title=@v_Government_Title,First_Name=@v_First_Name,Last_Name=@v_Last_Name,Email_Address=@v_Email_Address,Part_of_Governing_Board=@v_Part_of_Governing_Board,Mailing_Street_Box=@v_Mailing_Street_Box,Mailing_Suite_Number=@v_Mailing_Suite_Number,Mailing_City=@v_Mailing_City,Mailng_Zip_Code=@v_Mailng_Zip_Code,Phone_Number=@v_Phone_Number,Phone_Number_Ext=@v_Phone_Number_Ext where official_id=\''||o_id||'\'';
PREPARE statment FROM @sql;
EXECUTE statment;
DEALLOCATE PREPARE statment;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "WeeklyBackups"; CREATE PROCEDURE "WeeklyBackups"()
    DETERMINISTIC
begin
DECLARE TotalRecords BIGINT;
DECLARE counter BIGINT;
call government.add_new_data_icube(@govs,@off_upd,@off_new);
replace into government.government_officials
select Official_ID ,Government_ID ,Role ,Role_exists ,replace(replace(First_Name,char(9),''),char(34),'') ,replace(replace(Last_Name,char(9),''),char(34),'') ,replace(replace(Government_Title,char(9),''),char(34),'') ,Elected ,Length_of_Elected_Term ,Term_Month_End_Date ,Term_Year_End_Date ,Part_of_Governing_Board ,replace(replace(Mailing_Street_Box,char(9),''),char(34),'') ,replace(replace(Mailing_Suite_Number,char(9),''),char(34),'') ,replace(replace(Mailing_City,char(9),''),char(34),'') ,replace(replace(Mailing_State,char(9),''),char(34),'') ,replace(replace(Mailng_Zip_Code,char(9),''),char(34),'') ,replace(replace(Phone_Number,char(9),''),char(34),'') ,replace(replace(Phone_Number_Ext,char(9),''),char(34),'') ,replace(replace(Email_Address,char(9),''),char(34),'') ,replace(replace(Email_Type,char(9),''),char(34),'') ,replace(replace(Special_Notes,char(9),''),char(34),'') ,Date_Confirmed ,ID ,E_mail_exists ,phone_exists ,Mailing_Street_Box_exists
from government.government_officials;
replace into government.government
select Government_ID ,Government_Place_Name ,Government_Type_Name ,replace(replace(Government_Web_Address,char(9),''),char(34),'') ,replace(replace(Government_PhoneNumber,char(9),''),char(34),'') ,Dept_Name_for_phone_number ,Government_Type ,Form_of_Government ,Governing_Body_Name ,County_Name ,Fiscal_Year_End_Date ,Original_Incorporation_Date ,Type_Code ,County_Code ,Unit_Code ,FIPS_State ,FIPS_County ,FIPS_Place ,Population ,Population_Category ,Population_2007 ,Population_2007_Category ,Taken_from ,Updated_website ,state ,replace(replace(Address_Street_Box,char(9),''),char(34),'') ,replace(replace(City,char(9),''),char(34),'') ,Zip_Code ,Gov_ID,Electron_Month
from government.government;
call government.build_all_data;
drop table if exists tmp_table;
create temporary table tmp_table as
select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,g.Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,g.Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,COALESCE(TimeZone,50) as g_timezone,Electron_Month
from (government.government g join government.government_officials o on g.government_id=o.government_id) left join government.zip_code2 z on z.ZipCode=g.Zip_Code;

drop table if exists full_data;
create table full_data as
select Government_Type,Form_of_Government,Governing_Body_Name,County_Name,Population,state,g.Government_ID,Government_Place_Name,Government_Type_Name,Government_Web_Address,Taken_from,Role,Role_exists,First_Name,Last_Name,Government_Title,Email_Address,Email_Type,Part_of_Governing_Board,Updated_website,Mailing_Street_Box,Mailing_Suite_Number,Mailing_City,Mailing_State,Mailng_Zip_Code,Phone_Number,Phone_Number_Ext,Address_Street_Box,City,g.Zip_Code,Government_PhoneNumber,Official_ID,Date_Confirmed,g_timezone,COALESCE(TimeZone,50) as o_timezone,STR_TO_DATE('1970-01-01','%Y-%m-%d') as va_date,STR_TO_DATE('01:00:00','%h:%i:%s') as va_time,'unallocated' as r_state,'' as notes,Electron_Month
from tmp_table g left join government.zip_code2 z on z.ZipCode=g.Mailng_Zip_Code;

update full_data set va_date=null,va_time=null;
alter table full_data change notes notes varchar(1000);
alter table full_data add primary key (official_id);
alter table full_data add key (government_id);
drop table if exists m_ready;
create table m_ready as select * from ready;
truncate table ready;
select count(*) into TotalRecords from jobs where state='done';
set counter=0;
WHILE counter < TotalRecords DO begin
	set @sql='select job_id into @job_id from jobs where state=\'done\' limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @sql='drop table if exists va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='drop table if exists qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='drop table if exists fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	end;
END WHILE; 

drop table if exists govs;
create table govs(government_id varchar(14),primary key(government_id));
select count(*) into TotalRecords from jobs where state!='done';
set counter=0;
WHILE counter < TotalRecords DO begin
	set @sql='select job_id into @job_id from jobs where state!=\'done\' limit '||counter||',1';
	prepare q from @sql;
	execute q;
	deallocate prepare q;
	set counter=counter+1;
	set @sql='replace into govs select distinct government_id from va_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='replace into govs select distinct government_id from qc_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	set @sql='replace into govs select distinct government_id from fa_'||@job_id;
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
	end;
END WHILE;
update full_data set r_state='allocated' where government_id in (select government_id from govs);
delete from jobs where state='done';

CALL government.SearchSummary('NickPA','byText=&',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists);
CALL government.SearchDetails(25,'NickPA',@Result);
UPDATE government.database_summary SET Total_Available_Emails = @Num_Matched_Emails, Total_Gov_Officials = @Num_Matched_Officials, Total_Phone_Number = @Phone_Numbers, Total_Mailing_Street_Box = @Mailing_Addresses, Total_Governments = @Num_Matched_Governments,  JSON_GOVERNMENTS = @JSON_GOVERNMENTS, JSON_OFFICIALS = @JSON_OFFICIALS, JSON_PREVIEW = @Result;
CALL government.Delete_Research_Tables('NickPA');
CALL government.Delete_Research_Tables('createpa');
CALL government.SearchSummary('createpa','byText=&top_elected=0',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists);
call government.Bulid_Research_Tables('createpa',@exists);

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,ANSI' */ ;
DELIMITER ;;
DROP PROCEDURE IF EXISTS "zip_exists"; CREATE PROCEDURE "zip_exists"(IN zip_code varchar(5),OUT exist boolean)
    DETERMINISTIC
begin
select count(*) into exist from government2.zip_code2 where ZipCode=zip_code;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-20 15:21:20
