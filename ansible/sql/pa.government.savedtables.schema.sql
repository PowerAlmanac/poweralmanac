DROP TABLE IF EXISTS `savedtables`;

CREATE TABLE `savedtables` (
  `SavedTable_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Table_Name` varchar(255) DEFAULT NULL,
  `Search_Params` text,
  `Search_NumRecords` decimal(11,0) NOT NULL DEFAULT '0',
  `Table_Params` varchar(255) DEFAULT NULL,
  `DateTime_Saved` datetime DEFAULT NULL,
  `DateTime_Rerun` datetime DEFAULT NULL,
  `RegUser_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`SavedTable_ID`),
  KEY `RegUser_ID_idxfk_1` (`RegUser_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;