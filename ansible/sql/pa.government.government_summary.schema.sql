DROP TABLE IF EXISTS `database_summary`;

CREATE TABLE `database_summary` (
  `DB_Summ_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Total_No_Of_Records` decimal(22,0) NOT NULL DEFAULT '0',
  `Total_No_Of_Changed_Rec` decimal(22,0) NOT NULL DEFAULT '0',
  `Total_Gov_Officials` decimal(22,0) NOT NULL DEFAULT '0',
  `Total_Governments` decimal(22,0) NOT NULL DEFAULT '0',
  `Total_Gov_With_No_Offices` decimal(22,0) DEFAULT '0',
  `Total_Available_Emails` decimal(22,0) DEFAULT '0',
  `Total_Mailing_Street_Box` decimal(22,0) DEFAULT '0',
  `Total_Phone_Number` varchar(15) NOT NULL,
  `JSON_GOVERNMENTS` text,
  `JSON_OFFICIALS` text,
  `JSON_PREVIEW` text,
  PRIMARY KEY (`DB_Summ_ID`),
  UNIQUE KEY `DB_Summ_ID` (`DB_Summ_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;