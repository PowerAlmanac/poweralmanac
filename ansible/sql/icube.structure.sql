-- MySQL dump 10.13  Distrib 5.7.12, for osx10.11 (x86_64)
--
-- Host: poweralmanac.cnl6vllv6ior.us-east-1.rds.amazonaws.com    Database: icube_government
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `emp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `access_level` varchar(100) NOT NULL,
  `primary_role` varchar(100) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11193 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `full_data`
--

DROP TABLE IF EXISTS `full_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `full_data` (
  `Government_Type` enum('municipality','township','county') DEFAULT NULL,
  `Form_of_Government` varchar(30) NOT NULL,
  `Governing_Body_Name` varchar(70) DEFAULT NULL,
  `County_Name` varchar(255) NOT NULL,
  `Population` decimal(22,0) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `Government_ID` varchar(14) NOT NULL DEFAULT '',
  `Government_Place_Name` varchar(50) DEFAULT NULL,
  `Government_Type_Name` enum('TOWNSHIP','CITY','Town','Village','COUNTY','BOROUGH','CHARTER TOWNSHIP','PARISH','Plantation','CITY AND COUNTY','CIVIL TOWNSHIP','MUNICIPALITY','CONSOLIDATED GOVERNMENT','Unified Government','Corporation','METROPOLITAN GOVERNMENT','CITY-PARISH','CITY AND BOROUGH','Town/Village','METRO GOVERNMENT','URBAN COUNTY GOVERNMENT') DEFAULT NULL,
  `Government_Web_Address` varchar(255) NOT NULL,
  `Taken_from` varchar(500) DEFAULT NULL,
  `Role` enum('Governing Board Member','Head of Purchasing/Procurement','Clerk','Head of IT','Head of Finance/Budgeting','Head of Law Enforcement','Top Appointed Official','Top Elected Official','Head of Public Works','Head of Fire Protection Services') DEFAULT 'Governing Board Member',
  `Role_exists` varchar(3) NOT NULL,
  `First_Name` varchar(255) NOT NULL,
  `Last_Name` varchar(255) NOT NULL,
  `Government_Title` varchar(150) DEFAULT NULL,
  `Email_Address` varchar(100) NOT NULL,
  `Email_Type` varchar(80) DEFAULT NULL,
  `Part_of_Governing_Board` enum('Yes','No','') DEFAULT NULL,
  `Updated_website` varchar(10) DEFAULT NULL,
  `Mailing_Street_Box` varchar(255) NOT NULL,
  `Mailing_Suite_Number` varchar(30) DEFAULT NULL,
  `Mailing_City` varchar(100) NOT NULL,
  `Mailing_State` varchar(80) NOT NULL,
  `Mailng_Zip_Code` varchar(5) DEFAULT NULL,
  `Phone_Number` varchar(30) NOT NULL,
  `Phone_Number_Ext` varchar(10) DEFAULT NULL,
  `Address_Street_Box` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Zip_Code` varchar(5) NOT NULL,
  `Government_PhoneNumber` varchar(30) NOT NULL,
  `Official_ID` varchar(100) NOT NULL DEFAULT '',
  `Date_Confirmed` date DEFAULT NULL,
  `g_timezone` varchar(2) DEFAULT NULL,
  `o_timezone` varchar(2) DEFAULT NULL,
  `va_date` date DEFAULT NULL,
  `va_time` time DEFAULT NULL,
  `r_state` varchar(11) NOT NULL DEFAULT '',
  `notes` varchar(1000) DEFAULT NULL,
  `Electron_Month` varchar(2) DEFAULT '0',
  PRIMARY KEY (`Official_ID`),
  KEY `Government_ID` (`Government_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `govs`
--

DROP TABLE IF EXISTS `govs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `govs` (
  `government_id` varchar(14) NOT NULL DEFAULT '',
  PRIMARY KEY (`government_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fa_id` bigint(20) NOT NULL,
  `num_officials` bigint(20) DEFAULT '0',
  `num_govs` bigint(20) DEFAULT '0',
  `state` varchar(50) NOT NULL DEFAULT '',
  `prod` decimal(20,2) DEFAULT '0.00',
  `va_id` bigint(20) NOT NULL,
  `qc_id` bigint(20) NOT NULL,
  PRIMARY KEY (`job_id`),
  KEY `va_id` (`va_id`)
) ENGINE=MyISAM AUTO_INCREMENT=245403 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_ready`
--

DROP TABLE IF EXISTS `m_ready`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_ready` (
  `Government_Type` enum('municipality','township','county') DEFAULT NULL,
  `Form_of_Government` varchar(30) NOT NULL,
  `Governing_Body_Name` varchar(70) DEFAULT NULL,
  `County_Name` varchar(255) NOT NULL,
  `Population` decimal(22,0) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `Government_ID` varchar(14) NOT NULL DEFAULT '',
  `Government_Place_Name` varchar(50) DEFAULT NULL,
  `Government_Type_Name` enum('TOWNSHIP','CITY','Town','Village','COUNTY','BOROUGH','CHARTER TOWNSHIP','PARISH','Plantation','CITY AND COUNTY','CIVIL TOWNSHIP','MUNICIPALITY','CONSOLIDATED GOVERNMENT','Unified Government','Corporation','METROPOLITAN GOVERNMENT','CITY-PARISH','CITY AND BOROUGH','Town/Village','METRO GOVERNMENT','URBAN COUNTY GOVERNMENT') DEFAULT NULL,
  `Government_Web_Address` varchar(255) NOT NULL,
  `Taken_from` varchar(500) DEFAULT NULL,
  `role` enum('Governing Board Member','Head of Purchasing/Procurement','Clerk','Head of IT','Head of Finance/Budgeting','Head of Law Enforcement','Top Appointed Official','Top Elected Official','Head of Public Works','Head of Fire Protection Services') DEFAULT 'Governing Board Member',
  `Role_exists` varchar(3) NOT NULL,
  `First_Name` varchar(255) NOT NULL,
  `Last_Name` varchar(255) NOT NULL,
  `Government_Title` varchar(150) DEFAULT NULL,
  `Email_Address` varchar(100) NOT NULL,
  `Email_Type` varchar(80) DEFAULT NULL,
  `Part_of_Governing_Board` enum('Yes','No','') DEFAULT NULL,
  `Updated_website` varchar(10) DEFAULT NULL,
  `Mailing_Street_Box` varchar(255) NOT NULL,
  `Mailing_Suite_Number` varchar(30) DEFAULT NULL,
  `Mailing_City` varchar(100) NOT NULL,
  `Mailing_State` varchar(80) NOT NULL,
  `Mailng_Zip_Code` varchar(5) DEFAULT NULL,
  `Phone_Number` varchar(30) NOT NULL,
  `Phone_Number_Ext` varchar(10) DEFAULT NULL,
  `Address_Street_Box` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Zip_Code` varchar(5) NOT NULL,
  `Government_PhoneNumber` varchar(30) NOT NULL,
  `Official_ID` varchar(100) NOT NULL DEFAULT '',
  `Date_Confirmed` date DEFAULT NULL,
  `g_timezone` int(11) DEFAULT NULL,
  `o_timezone` int(11) DEFAULT NULL,
  `va_date` date DEFAULT NULL,
  `va_time` time DEFAULT NULL,
  `r_state` varchar(11) NOT NULL DEFAULT '',
  `notes` varchar(1000) DEFAULT NULL,
  `job_id` bigint(20) DEFAULT NULL,
  `Electron_Month` varchar(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Government_ID` varchar(14) NOT NULL,
  `Job_ID` bigint(20) NOT NULL,
  `Official_ID` varchar(100) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `txt` varchar(1000) DEFAULT NULL,
  `Writer_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Government_ID` (`Government_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1198542 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ready`
--

DROP TABLE IF EXISTS `ready`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ready` (
  `Government_Type` enum('municipality','township','county') DEFAULT NULL,
  `Form_of_Government` varchar(30) NOT NULL,
  `Governing_Body_Name` varchar(70) DEFAULT NULL,
  `County_Name` varchar(255) NOT NULL,
  `Population` decimal(22,0) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `Government_ID` varchar(14) NOT NULL DEFAULT '',
  `Government_Place_Name` varchar(50) DEFAULT NULL,
  `Government_Type_Name` enum('TOWNSHIP','CITY','Town','Village','COUNTY','BOROUGH','CHARTER TOWNSHIP','PARISH','Plantation','CITY AND COUNTY','CIVIL TOWNSHIP','MUNICIPALITY','CONSOLIDATED GOVERNMENT','Unified Government','Corporation','METROPOLITAN GOVERNMENT','CITY-PARISH','CITY AND BOROUGH','Town/Village','METRO GOVERNMENT','URBAN COUNTY GOVERNMENT') DEFAULT NULL,
  `Government_Web_Address` varchar(255) NOT NULL,
  `Taken_from` varchar(500) DEFAULT NULL,
  `role` enum('Governing Board Member','Head of Purchasing/Procurement','Clerk','Head of IT','Head of Finance/Budgeting','Head of Law Enforcement','Top Appointed Official','Top Elected Official','Head of Public Works','Head of Fire Protection Services') DEFAULT 'Governing Board Member',
  `Role_exists` varchar(3) NOT NULL,
  `First_Name` varchar(255) NOT NULL,
  `Last_Name` varchar(255) NOT NULL,
  `Government_Title` varchar(150) DEFAULT NULL,
  `Email_Address` varchar(100) NOT NULL,
  `Email_Type` varchar(80) DEFAULT NULL,
  `Part_of_Governing_Board` enum('Yes','No','') DEFAULT NULL,
  `Updated_website` varchar(10) DEFAULT NULL,
  `Mailing_Street_Box` varchar(255) NOT NULL,
  `Mailing_Suite_Number` varchar(30) DEFAULT NULL,
  `Mailing_City` varchar(100) NOT NULL,
  `Mailing_State` varchar(80) NOT NULL,
  `Mailng_Zip_Code` varchar(5) DEFAULT NULL,
  `Phone_Number` varchar(30) NOT NULL,
  `Phone_Number_Ext` varchar(10) DEFAULT NULL,
  `Address_Street_Box` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Zip_Code` varchar(5) NOT NULL,
  `Government_PhoneNumber` varchar(30) NOT NULL,
  `Official_ID` varchar(100) NOT NULL DEFAULT '',
  `Date_Confirmed` date DEFAULT NULL,
  `g_timezone` int(11) DEFAULT NULL,
  `o_timezone` int(11) DEFAULT NULL,
  `va_date` date DEFAULT NULL,
  `va_time` time DEFAULT NULL,
  `r_state` varchar(11) NOT NULL DEFAULT '',
  `notes` varchar(1000) DEFAULT NULL,
  `job_id` bigint(20) DEFAULT NULL,
  `Electron_Month` varchar(2) DEFAULT '0',
  PRIMARY KEY (`Official_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time`
--

DROP TABLE IF EXISTS `time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time` (
  `time` time NOT NULL DEFAULT '00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `va_confirms_log`
--

DROP TABLE IF EXISTS `va_confirms_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `va_confirms_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `va_id` bigint(20) DEFAULT NULL,
  `va_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3368976 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `va_gov_confirms_log`
--

DROP TABLE IF EXISTS `va_gov_confirms_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `va_gov_confirms_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `va_id` bigint(20) DEFAULT NULL,
  `va_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=235752 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `valog`
--

DROP TABLE IF EXISTS `valog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `VA_ID` bigint(20) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `action` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100768 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-20 15:18:56
