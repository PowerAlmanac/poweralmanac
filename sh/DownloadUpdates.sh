# Creates the Captions and content seprately and join them into 1 file and zip it. also insert new record to table SavedDownloads
#!/bin/bash
source /var/www/sh/set-mysql-credentials

#rc=`mysql -uroot --password="W4boubWgGkaPaqnmrsXmr>FY9" -N -B -hpoweralmanac.cnl6vllv6ior.us-east-1.rds.amazonaws.com -Dgovernment -e "call DownloadUpdates($1,'$2','$3');" > /var/www/sh/u_$3.tmp`
mysql --user="${MYSQL_USER}" -N -B --host="${MYSQL_HOST}" --database="${MYSQL_DATABASE}" --password="${MYSQL_PASS}" -e "select '\"'||Official_ID||'\",\"'||DATE_FORMAT(Date_Confirmed,'%m/%d/%Y')||'\",\"'||first_name||'\",\"'||last_name||'\",\"'||Government_Title||'\",\"'||Role||'\",\"'||Part_of_Governing_Board||'\",\"'||Mailing_Street_Box||'\",\"'||Mailing_Suite_Number||'\",\"'||Mailing_City||'\",\"'||Mailing_State||'\",=\"'||Mailng_Zip_Code||'\",\"'||left(Phone_Number,3)||'-'||substr(Phone_Number,4,3)||'-'||substr(Phone_Number,7,length(Phone_Number)-1)||'\",\"'||Phone_Number_Ext||'\",\"'||if(instr(Email_Address,'@')<>0,Email_Address,'')||'\",\"'||Government_Place_Name||'\",\"'||upper(mid(Government_Type_Name ,1,1))||lower(mid(Government_Type_Name ,2,length(Government_Type_Name)-1))||'\",\"'||Government_Type||'\",\"'||if(Government_Web_Address='WEBSITE NOT FOUND','',Government_Web_Address)||'\",\"'||left(Government_PhoneNumber,3)||'-'||substr(Government_PhoneNumber,4,3)||'-'||substr(Government_PhoneNumber,7,length(Government_PhoneNumber)-1)||'\",\"'||Address_Street_Box||'\",\"'||city||'\",\"'||state||'\",=\"'||Zip_Code||'\",\"'||if(County_Name='','n/a',County_Name)||'\",\"'||if(TotalPopulation is null or TotalPopulation=0,'n/a',TotalPopulation)||'\",\"'||status||'\"' as Official_ID from s_$2_$1 where id not in (select id from u_$2_$1) union all (select '\"'||Official_ID||'\",\"'||DATE_FORMAT(Date_Confirmed,'%m/%d/%Y')||'\",\"'||first_name||'\",\"'||last_name||'\",\"'||Government_Title||'\",\"'||Role||'\",\"'||Part_of_Governing_Board||'\",\"'||Mailing_Street_Box||'\",\"'||Mailing_Suite_Number||'\",\"'||Mailing_City||'\",\"'||Mailing_State||'\",=\"'||Mailng_Zip_Code||'\",\"'||left(Phone_Number,3)||'-'||substr(Phone_Number,4,3)||'-'||substr(Phone_Number,7,length(Phone_Number)-1)||'\",\"'||Phone_Number_Ext||'\",\"'||if(instr(Email_Address,'@')<>0,Email_Address,'')||'\",\"'||Government_Place_Name||'\",\"'||upper(mid(Government_Type_Name ,1,1))||lower(mid(Government_Type_Name ,2,length(Government_Type_Name)-1))||'\",\"'||Government_Type||'\",\"'||if(Government_Web_Address='WEBSITE NOT FOUND','',Government_Web_Address)||'\",\"'||left(Government_PhoneNumber,3)||'-'||substr(Government_PhoneNumber,4,3)||'-'||substr(Government_PhoneNumber,7,length(Government_PhoneNumber)-1)||'\",\"'||Address_Street_Box||'\",\"'||city||'\",\"'||state||'\",=\"'||Zip_Code||'\",\"'||if(County_Name='','n/a',County_Name)||'\",\"'||if(TotalPopulation is null or TotalPopulation=0,'n/a',TotalPopulation)||'\",\"'||status||'\"' as Official_ID from u_$2_$1) order by Official_ID;" > /var/www/sh/u_$3.tmp
mysql --user="${MYSQL_USER}" -N -B --host="${MYSQL_HOST}" --database="${MYSQL_DATABASE}" --password="${MYSQL_PASS}" -e "update saveddownloads set Downloaded_DateTime=sysdate() where RegUser_ID=$1 and Search_Name='$2';"
rc=0
if [ "$rc" -eq 0 ]; then
# Remove superfluous "NULL" line
sed -i '/^NULL$/d' /var/www/sh/u_$3.tmp

echo -e '"Record ID","Record last updated","Official\047s First Name","Official\047s Last Name","Official\047s Title","Official\047s Role","Is Top Elected Official Part of Governing Board?","Official\047s Mailing Address - Street/Box number","Official\047s Mailing Address - Suite Number","Official\047s Mailing Address - City","Official\047s Mailing Address - State","Official\047s Mailing Address -Zip Code","Official\047s Phone Number ","Official\047s Phone Number  EXTENSION","Official\047s Email Address","Government Place Name ","Government Type Name ","Government Type","Government Web Address ","Government\047s Main Phone Number ","Government\047s Physical Address - Street ","Government\047s Physical Address - City ","Government\047s Physical Address - State ","Government\047s Physical Address -Zip Code ","Name of County that Municipal or Township Government is Located in ","Population","Record Status"' > /var/www/sh/u_$3.csv
cat /var/www/sh/u_$3.tmp >> /var/www/sh/u_$3.csv
rm /var/www/sh/u_$3.tmp
echo '""' >> /var/www/sh/u_$3.csv
echo '""' >> /var/www/sh/u_$3.csv
echo '"NOTES:"' >> /var/www/sh/u_$3.csv
echo -e '"Here\047s your Power Almanac download file.   Copyright 2016 LTBL, LLC.  Information is deemed reliable but not guaranteed."' >> /var/www/sh/u_$3.csv
echo -e '"You may use the information in this file as often as you\047d like, but only while you are a Power Almanac subscriber."' >> /var/www/sh/u_$3.csv
echo -e '"You may NOT share or give this file or any of its contents with or to any person other than a \047colleague\047.   For these purposes, \047colleague\047 is defined as someone employed by the same \047organization\047 as you are, and your \047organization\047 EXCLUDES your organization\047s parent company or any other subsidiaries owned by your organization or your organization\047s parent company."' >> /var/www/sh/u_$3.csv
echo '""' >> /var/www/sh/u_$3.csv
echo -e '"EXPLANATION OF \47RECORD STATUS\47 column:"' >> /var/www/sh/u_$3.csv
echo -e '"\47New\47 = neither you or any of your account sub-users have downloaded this record in the past 12 months"' >> /var/www/sh/u_$3.csv
echo -e '"\47Previously downloaded\47 = either you or one of your account sub-users has downloaded this record within the past 12 months, and the record has NOT been changed/updated since then"' >> /var/www/sh/u_$3.csv
echo -e '"\47Updated\47 = either you or one of your account sub-users has downloaded this record within the past 12 months, and one or more fields in the record HAS been changed/updated since then"' >> /var/www/sh/u_$3.csv
cd /var/www/sh
cp u_$3.csv u_$3.txt
sed -i 's/",="/","/g' u_$3.txt
zip /var/www/sh/u_$3.zip u_$3.csv > /dev/null
zip /var/www/sh/u_$3.zip u_$3.txt > /dev/null
rm /var/www/sh/u_$3.csv
rm /var/www/sh/u_$3.txt
rm /var/www/poweralmanac.com/dl/u_$3.zip 2>/dev/null
mv u_$3.zip /var/www/poweralmanac.com/dl/
fi
echo $rc
exit $rc
