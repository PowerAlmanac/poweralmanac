# Creates the Captions and content seprately and join them into 1 file and zip it. also insert new record to table SavedDownloads
#!/bin/bash

source /var/www/sh/set-mysql-credentials

rc=$(/usr/bin/mysql --user="${MYSQL_USER}" -N -B --host="${MYSQL_HOST}" --database="${MYSQL_DATABASE}" --password="${MYSQL_PASS}" -e "call SaveCurrentDownload($1,'$2','$3',$4,'$5','$6');" > /var/www/sh/$6.tmp; echo $?)
if [ $rc -eq 0 ]; then
    # Remove superfluous "NULL" line
    sed -i '/^NULL$/d' /var/www/sh/$6.tmp

    echo $'"Record ID","Record last updated","Official\'s First Name","Official\'s Last Name","Official\'s Title","Official\'s Role","Is Top Elected Official Part of Governing Board?","Official\'s Mailing Address - Street/Box number","Official\'s Mailing Address - Suite Number","Official\'s Mailing Address - City","Official\'s Mailing Address - State","Official\'s Mailing Address -Zip Code","Official\'s Phone Number ","Official\'s Phone Number  EXTENSION","Official\'s Email Address","Government Place Name ","Government Type Name ","Government Type","Government Web Address ","Government\'s Main Phone Number ","Government\'s Physical Address - Street ","Government\'s Physical Address - City ","Government\'s Physical Address - State ","Government\'s Physical Address -Zip Code ","Name of County that Municipal or Township Government is Located in ","Population","Record Status"' > /var/www/sh/$6.csv

    cat /var/www/sh/$6.tmp >> /var/www/sh/$6.csv
    rm /var/www/sh/$6.tmp
    YEAR=$(date +%Y)

    echo $'""' >> /var/www/sh/$6.csv
    echo $'""' >> /var/www/sh/$6.csv
    echo $'"NOTES:"' >> /var/www/sh/$6.csv
    echo $"\"Here's your Power Almanac download file.   Copyright ${YEAR} LTBL, LLC.  Information is deemed reliable but not guaranteed.\"" >> /var/www/sh/$6.csv
    echo $'"You may use the information in this file as often as you\'d like, but only while you are a Power Almanac subscriber."' >> /var/www/sh/$6.csv
    echo $'"You may NOT share or give this file or any of its contents with or to any person other than a \'colleague\'.   For these purposes, \'colleague\' is defined as someone employed by the same \'organization\' as you are, and your \'organization\' EXCLUDES your organization\'s parent company or any other subsidiaries owned by your organization or your organization\'s parent company."' >> /var/www/sh/$6.csv
    echo $'""' >> /var/www/sh/$6.csv
    echo $'"EXPLANATION OF \'RECORD STATUS\' column:"' >> /var/www/sh/$6.csv
    echo $'"\'New\' = neither you or any of your account sub-users have downloaded this record in the past 12 months"' >> /var/www/sh/$6.csv
    echo $'"\'Previously downloaded\' = either you or one of your account sub-users has downloaded this record within the past 12 months, and the record has NOT been changed/updated since then"' >> /var/www/sh/$6.csv
    echo $'"\'Updated\' = either you or one of your account sub-users has downloaded this record within the past 12 months, and one or more fields in the record HAS been changed/updated since then"' >> /var/www/sh/$6.csv
    cd /var/www/sh
    cp $6.csv $6.txt
    sed -i 's/",="/","/g' $6.txt
    zip /var/www/sh/$6.zip $6.csv > /dev/null
    zip /var/www/sh/$6.zip $6.txt > /dev/null
    rm /var/www/sh/$6.csv
    rm /var/www/sh/$6.txt
    mv -f $6.zip /var/www/poweralmanac.com/dl/
    nohup /usr/bin/mysql --user="${MYSQL_USER}" -N -B --host="${MYSQL_HOST}" --database="${MYSQL_DATABASE}" --password="${MYSQL_PASS}" -e "call SaveDataForDownload('$2',$4,'$5');" &> /dev/null &
fi
echo $rc
exit $rc
