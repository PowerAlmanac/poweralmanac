# Deletes the file + the record from the database
#!/bin/bash
source /var/www/sh/set-mysql-credentials


rc=`mysql --user="${MYSQL_USER}" -N -B --host="${MYSQL_HOST}" --database="${MYSQL_DATABASE}" --password="${MYSQL_PASS}" -e "call DeleteSavedDownload('$1');"`
if [ "$rc" -eq 0 ]; then
rm /var/www/poweralmanac.com/dl/$1
rm /var/www/poweralmanac.com/dl/u_$1 2>/dev/null
rm /var/www/poweralmanac.com/dl/f_$1 2>/dev/null
fi
echo $rc
exit $rc
