<?php
include("inc/config.php");

//redirect auth'd testers to test version
if($_SESSION['user_email'] == "jon@blendimc.com" || $_SESSION['user_email'] == "tim@blendimc.com"){
  header("Location: /checkout-test.php");
  die();
}

$_SESSION['redirect'] = 'checkout.php';
include("inc/protect-register.php");
$title = 'Terms of Service | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");

$bestPricing = getSubList($dbname, $dbhost, $dbuser, $dbpass);
$User_DL_Reserves = 0;
if($_SESSION['user_subscription'] > 0){
    $User_Email = $_SESSION['user_email'];
    $pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
    $pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
    $pdoObject2 = $pdo->query("SELECT @JSON_String");
    $rsArray2 = $pdoObject2->fetchAll();
    $json2a = $rsArray2[0]['@JSON_String'];
    $jsonArray2a = json_decode($json2a, TRUE);
    $User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
    $User_ActivationCode = $jsonArray2a['User_ActivationCode'];
}

$user_subscription = $_SESSION['user_subscription'];
$newExpirationDate = false;

// Buying a particular subscription
if(isset($_GET['subscription'])) {
  //for link clicked subscription upgrades
  $newSub = $_GET['subscription'];

  //automatically assign all grandfathered-in sub types to power 20 (level six, which is the second item in our sub array)
  $select = $bestPricing[1];

  //grab the passed through subscription
  for($i = 0; $i < count($bestPricing); $i++) {
    if($bestPricing[$i]['Sub_Level'] == $newSub) {
      $select = $bestPricing[$i];
    }
  }

  //go ahead and set quantity vars to zero, these are only used for a checkout from the search page
  $adjustedQuantity = 0;
  $quantity = 0;
  $subscriptionLevel = $select['Sub_Level'];
  $leftover = $select['Sub_NumReserves'];
  $totalPrice = $select['Sub_AnnualFees'];
  $newCredits = $select['Sub_NumReserves'];
  $existingCreditsBeforePurchase = $User_DL_Reserves;
  $newExpirationDate = new DateTime('+364 days');

  //current power unlimited subscribers do not get to keep their million credits, but any lower subscription is able to keep them.
  if($user_subscription == 10){
    $User_DL_Reserves = 0;
  }
} else {
  //for search page stuff
  //let power unlimited users jump straight to download page
  if($_SESSION['user_subscription'] == 10){
      header("Location: ./PA-DownloadNow.php");
      exit;
  }

  $checkout_data = json_decode($_SESSION['checkout'], TRUE);
  $paid_records = isset($_SESSION['checkout_paid_records']) ? $_SESSION['checkout_paid_records'] : 0;

  $allQuantity = $checkout_data['Num_Matched_Officials'];
  $quantity = $allQuantity - $paid_records;

  $_SESSION['lastSearchNumMatched'] = $allQuantity;

  //get the amount of credits that must be paid for
  //if user is power + subscriber, skip this check
  if($_SESSION['user_subscription'] != 10){
      $adjustedQuantity = $quantity - $User_DL_Reserves;
  }
  else{
      $adjustedQuantity = 0;
  }

  if ($_SESSION['RegUser_ID']) {
      $user = \PowerAlmanac\PDb::query_assoc('SELECT * FROM registeredusers WHERE RegUser_ID=' . escape($_SESSION['RegUser_ID']));
      $currentSubscriptionId = \PowerAlmanac\PDb::query_assoc('SELECT * FROM subscriptions WHERE Sub_Level=' . $user['User_Subscription'] . ' ORDER BY Purchasable DESC, Active DESC LIMIT 1')['Subscriptions_ID'];
  } else {
      $currentSubscriptionId = 0;
  }

  $bestPrice = \PowerAlmanac\PricingCalculator::bestPrice($quantity, $currentSubscriptionId, $User_DL_Reserves);

  $totalPrice = $bestPrice['totalPrice'];
  $leftover = $bestPrice['leftover'];
  $select = $bestPrice['bestSubscription'];
  $newCredits = $bestPrice['newCredits'];
  $subscriptionLevel = $select['Sub_Level'];
  $costPerRecord = $bestPrice['costPerRecord'];
  $existingCreditsBeforePurchase = $bestPrice['existingCreditsBeforePurchase'];
  $newExpirationDate = new DateTime('+364 days');

  // Not upgrading their plan, tell them that in the checkout table
  if (! $bestPrice['upgradingSubscription']) {
      $select['Sub_Name'] = 'no plan upgrade';
      $newExpirationDate = null;
  }
}


// store session so user is charged- update 3-20-14: variable should be the $quantity, as the number removed is fixed regardless of how many
// credits they have or don't have.
$_SESSION['record2Charge'] = $quantity;
$_SESSION['total_price'] = $totalPrice;

// If they don't need to pay (cost fits within the credits they already have) redirect them to get downloaded
if ($totalPrice <= 0) {
    header("Location: ./PA-DownloadNow.php");
    exit;
}

if($subscriptionLevel == 10) {
    $select['Sub_NumReserves'] = "Unlimited";
    $leftover = "Unlimited";
} else {
    $select['Sub_NumReserves'] = number_format($select['Sub_NumReserves'], 0);
    $leftover = number_format($leftover);
}

// The ($subscriptionLevel == 2 ? 3 : $subscriptionLevel) is there because I had to hack this part to change level 2 subscriptions
// to level 3. The database doesn't recognize level 2, and the Power 3 values are literally set as "3" in the database, bypassing
// 2 completely. Therefore, users that registered with a very specific amount would get set to 2 (and have no account associated
// with them). This hack fixes it.
$custom = $User_ActivationCode . ',' . $_SESSION['RegUser_ID'] . ',' . ($subscriptionLevel == 2 ? 3 : $subscriptionLevel) . ',' . $newCredits;
?>

            <div class="intro">
                <div class="intro-holder">
                    <h1>Review and Checkout</h1>
                </div>
            </div>
            <div class="main-holder layout-n checkout">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame searchgov">
							<?php							
							//if($_GET['scrvr'] == $_SESSION['checkout_secure']) :
							?>
                            <div class="section">
                                <div class="checkouthalf">
                                    <h4>Cost of this Download</h4>
                                    <div class="lineitem">Total Records in this Download:<span class="right"><?php echo number_format($allQuantity); ?></span></div>
                                    <div class="lineitem">Credits required for this Download:
                                        <span data-toggle="tooltip" data-html="true"
                                              title='Download Credits ("Credits") are used to download records, with 1 Credit needed to download 1 record.  Once you have used a Credit to download a record, you will not be charged another Credit to download that record again for 1 year (or after your plan expires).  This is why the number of "Credits required"  may be less than the records in "this download."'
                                        class="btooltip ml-1 oi oi-info"></span>
                                        <span class="right"><?php echo number_format($quantity); ?></span>
                                    </div>
                                    <div class="lineitem">Your Current Balance of Download Credits:<span class="right"><?php echo number_format($existingCreditsBeforePurchase, 0); ?></span></div>
                                    <div class="lineitem">Minimum Credits You Need to Purchase:
                                        <span data-toggle="tooltip" data-html="true"
                                              title='If your current balance of credits is less than the "credits required for download," then you will need to purchase credits.  In some cases, it may cost less to upgrade your plan than to buy "extra credits" with your current plan pricing.'
                                              class="btooltip ml-1 oi oi-info"></span>
                                        <span class="right"><?php echo number_format($quantity - $existingCreditsBeforePurchase, 0); ?></span></div>
                                    <div class="lineitem total">Lowest Cost to Purchase the Needed Credits:
                                        <span data-toggle="tooltip" data-html="true"
                                              title='We have calculated the lowest cost to purchase the credits you need. In some cases that will mean upgrading your plan and buying more credits than you need for this download.  In that case, on the right you will see a positive credit balance after this purchase.'
                                              class="btooltip ml-1 oi oi-info"></span>
                                        <span class="right">$<?php echo number_format($totalPrice, 2); ?></span></div>
                                </div>
                                <div class="checkouthalf">
                                    <h4>Included in this Purchase</h4>
                                    <div class="lineitem">Subscription Plan Upgraded to:
                                        <span data-toggle="tooltip" data-html="true"
                                              title='Depending on how many Credits you need to purchase, this download may cost you less if you upgrade your subscription plan (rather than buying more Credits under your current plan pricing).   If so, we will show your newly upgraded plan here.'
                                              class="btooltip ml-1 oi oi-info"></span>
                                        <span class="right"><?php echo $select['Sub_Name']; ?></span></div>
                                    <div class="lineitem">Expiration Date Pushed Out to:
                                        <span data-toggle="tooltip" data-html="true"
                                              title='If you upgraded your subscription plan as part of this purchase, your subscription expiration date will be  reset to 1 year from your upgrade purchase date.'
                                              class="btooltip ml-1 oi oi-info"></span>
                                        <span class="right"><?php echo $newExpirationDate instanceof DateTime ? $newExpirationDate->format('M d, Y') : 'no change' ?></span></div>
                                    <div class="lineitem">Credit Balance AFTER this Purchase:
                                        <span data-toggle="tooltip" data-html="true"
                                              title='If the balance shown here is greater than, then you upgraded your subscription plan as part of this purchase (because upgrading cost less than buying "extra credits" with your current plan pricing).  Upgraded plans come with a certain number of credits, and you have some left over for the future.'
                                              class="btooltip ml-1 oi oi-info"></span>
                                        <span class="right"><?= $leftover ?></span></div>
                                </div>
                            </div>
                            <div class="section last">


                                <h1>PAY FOR YOUR DOWNLOAD</h1>
                                <div>(click on the PayPal/Check Out button below)</div>
                                <div>
                                <?php if($adjustedQuantity !== 0 || isset($newSub)): ?>
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                  <input type='hidden' name="custom" value="<?php echo $custom ?>">
                                  <input type="hidden" name="cmd" value="_xclick">
                                  <input type="hidden" name="amount" value="<?php echo number_format($totalPrice, 2, '.', ''); ?>">
                                  <input type="hidden" name="item_name" value="Power Almanac Download Credits Purchase">
                                  <input type="hidden" name="item_number" value="PADL">
                                  <input type="hidden" name="quantity" value="1">
                                  <input type="hidden" name="business" value="ron.mester@ltbl.com">
                                  <input type="hidden" name="email" value="<?php echo($User_Email); ?>">
                                  <input type="hidden" name="first_name" value="<?php echo($User_FirstName); ?>">
                                  <input type="hidden" name="last_name" value="<?php echo($User_LastName); ?>">
                                  <input type="hidden" name="address1" value="9 Elm Street">
                                  <input type="hidden" name="address2" value="Apt 5">
                                  <input type="hidden" name="city" value="Berwyn">
                                  <input type="hidden" name="state" value="PA">
                                  <input type="hidden" name="zip" value="19312">
                                  <input type="hidden" name="night_phone_a" value="610">
                                  <input type="hidden" name="night_phone_b" value="555">
                                  <input type="hidden" name="night_phone_c" value="1234">
                                  <input type="hidden" name="no_shipping" value="1">
                                  <input type="hidden" name="notify_url" value="http://www.poweralmanac.com/PA-ipn-new.php">
                                  <input type="hidden" name="return" value="<?php echo($serverURL); ?>/PA-DownloadNow.php">
                                  <input type="hidden" name="rm" value="2">
                                  <input type="hidden" name="cbt" value="Return to Power Almanac">
                                  <input type="hidden" name="cancel_return" value="<?php echo($serverURL); ?>/PA-dlauth-cancel.php">
                                  <input type="image" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                  <img alt="" border="0" src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/scr/pixel.gif" width="1" height="1">
                              </form>
                                <div>You do not need a PayPal account to use PayPal Check Out.  PayPal accepts Visa, Mastercard, AMEX, and Discover.</div>
                                <img src="images/securepayments.jpg" style="margin-top: 20px" alt="buy government contact information securely" />
                                <?php else: ?>
                                    <a href="PA-DownloadNow.php" class="btn btn-warning" style="color: white;text-decoration:none;">Download Records</a>
                                <?php endif; ?>
                          </div>
                              <!--<?php echo '<div style="float: right; margin-right: 100px; background-color: gray; text-align: center;" class="checkout-ad"><img style="margin: auto" src="'.$select['img'].'"></div>'; ?>-->
                            </div>
							
                            <?php //endif;
                            ?>

							<div class="clearfix">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>
<?php unset($_SESSION['redirect']); ?>
