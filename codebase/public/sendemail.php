<?php


$message = file_get_contents('emailtemplates/welcome.txt');
#$message = file_get_contents('emailtemplates/reminder.txt');
#$message = file_get_contents('emailtemplates/moreuseage.txt');
$message = str_replace('[NAME]', 'Stephen', $message);
$message = str_replace('[ACT_CODE]', '1234', $message);
$message = str_replace('[EMAIL]', 'stephen@blendimc.com', $message);


$email_data = array(
	'to' => 'stephen@blendimc.com',
	'subject' => '[Registration - Please Activate Account]',	
	'html' => $message
	);


include('inc/mandrill.php');
mandrill_send($email_data);