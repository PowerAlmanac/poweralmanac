<?php

include("inc/config.php");
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$_SESSION['firstTimeSearch'] = 0;
$id = $_SESSION['deltek_exists'];

$sth = $pdo->prepare("UPDATE IGNORE `deltekimports` SET active = 0 WHERE import_id = ? LIMIT 1");
$sth->execute(array($id));
$sth = $pdo->query("DROP TABLE del_officials_" . escape($id));

$_SESSION['lastSearchParams'] = '';
$_SESSION['searchRequestArrayJSON'] = FALSE;
$_SESSION['deltek_active'] = FALSE;
$_SESSION['deltek_exists'] = FALSE;
$_SESSION['deltek_session'] = FALSE;


header("Location: search-government.php?ns=1");
flush();

?>