<?php

include("inc/config.php");
//include("inc/protect-login.php");

$unique_id = $_REQUEST['id'];

// Sanitize
// $unique_id ...

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL SharedSearchLookup(%s,@Params)", escape($unique_id))); // IN sharedID BIGINT, OUT Params TEXT
$pdoObject = $pdo->query("SELECT @Params");
$rsArray = $pdoObject->fetchAll();
$searchParams = $rsArray[0]['@Params'];

//echo("DEBUG: searchParams = $searchParams"); exit;

$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;

$searchURL = "search-government.php";
header("Location: $searchURL");
flush();


?>