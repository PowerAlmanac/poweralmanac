<?php
define("TITLE", "Privacy policy");
include("inc/config.php");
$title = 'Privacy Policy | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder container">
                    <h1>Privacy Policy</h1>
                </div>
            </div>
            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box layout-n">
                    <p>Your privacy in important to us, and we have prepared this Privacy Policy to explain to you how we collect, use and share information we obtain through your use of the Power Almanac website (http://www.poweralmanac.com) (the <span class="strengthen">"Site"</span>).</p>
                    <p><strong>By using this Site, you explicitly consent to such use of your information and agree to the terms of this Privacy Policy.</strong> This Privacy Policy is effective as of June 16, 2011.</p>
                    <ol>
                        <li><h3>Our Information Sharing Practices.</h3>
                            <ol>
                                <li><h4>General. </h4>Except as otherwise described in this section of our Privacy Policy, we will not share your Usage Data (See Section 4, below) or Personal Information (See Section 2, below) (which we will refer to in this Privacy Policy collectively as "User Information") with any third party without your permission.</li>
                                <li><h4>Aggregated Data. </h4>We don't sell or rent your personal information to third parties for their marketing purposes without your explicit consent. From time to time, we may share aggregated User Information with third parties. We will not share any aggregated data, however, in a manner that would enable the recipient to personally identify you.</li>
                                <li><h4>Publicly Available Information. </h4>As part of the Services, we provide data that is lawfully collected from federal, state or local governments; widely available sources that do not prohibit onward transfer or use; and disclosures to the general public that are required to be made by federal, state or local law ("Publicly Available Information"). If you are employed by or elected to serve with a federal, state or local government, Publicly Available Information about your employment or elected office may be included in the data that users can access when they use the Services. We will not, however, treat the Personal Information that you provide to us for your user account any differently than the Personal Information of a user who is not employed by or elected to serve with a federal, state or local government.</li>
                                <li><h4>Other Disclosure Scenarios. </h4>Notwithstanding anything in this Privacy Policy to the contrary, we reserve the right, and you hereby expressly authorize us, to share any User Information:
                                    <ul>
                                        <li>in response to subpoenas, court orders, or legal process, or to establish, protect, or exercise our legal rights or defend against legal claims;</li>
                                        <li>if we believe it is necessary in order to investigate, prevent, or take action regarding illegal activities, fraud, or situations involving potential threats to the safety of any person or property;</li>
                                        <li>if we believe it is necessary to investigate, prevent, or take action regarding abuse of the Site's infrastructure or the Internet in general (such as voluminous spamming, denial of service attacks, or attempts to compromise the security of information);</li>
                                        <li>with subsidiaries, joint ventures, or other companies under common control with us (in which case we will require such entities to honor this Privacy Policy);</li>
                                        <li>if we are acquired by or merge with another company; or</li>
                                        <li>with third parties who provide services to us (e.g., data management and storage services), provided, however, that in those circumstances we disclose User Information that is necessary for such service providers to perform those services, and request that they not share your User Information in a manner inconsistent with this Privacy Policy.</li>
                                    </ul>
                                </li>
                            </ol>
                        <li><h3>Information You Choose To Submit. </h3>You can visit the Site without telling us who you are or revealing any information through which someone could personally identify you (which we will refer to in this Privacy Policy collectively as "Personal Information"). We do not consider Personal Information to include information that has been anonymized so that it does not identify a specific user. If, however, you wish to create an account and use the Site's services ("Services"), you are required to provide certain Personal Information (e.g., your name, your e-mail address), and we ask that you also provide a user name and password. We use your Personal Information to fulfill your requests for products and services, to improve our services, and to contact you from time to time about us, our Site, and our products and services. We do not collect any information regarding payment or financial information related to our Site and Services, as all payment transactions are made through PayPal and are subject to PayPal's Privacy Policy, located at www.paypal.com.</li>
                        <li><h3>Information We Automatically Collect. </h3>We automatically collect certain information to help us understand how our users use the Site (which we will refer to in this Privacy Policy collectively as "Usage Data") and in order to tailor the site to your preferences and to improve the quality of the Site. For example, each time you visit the Site we may automatically collect your IP address, browser and computer type, access time, the Web page from which you came, and the Web page(s) that you access during your visit, and your search history. We collect and store the information through a third-party web hosting service (Amazon Web Services), and we take steps to ensure that your Usage Data is treated securely and in accordance with this Privacy Policy.</li>
                        <li><h3>Security. </h3>We have implemented reasonable measures to help protect your User Information from loss, misuse, or unauthorized access or disclosure. Unfortunately, however, no data transmission over the Internet can be guaranteed to be 100% secure. As a result, while we strive to protect your User Information, we cannot guarantee its security. If you choose to provide us with Personal Information, you consent to the transfer and storage of that information on our service provider's servers. Because the Internet is global, however, information about you that we collect or that you submit may be transferred to, processed, and held in other countries, where laws regarding processing of your Personal Information may be less stringent than the laws in the United States.</li>
                        <li><h3>Children's Privacy. </h3>We are committed to protecting the privacy needs of children and we encourage parents and guardians to take an active role in their children's online activities and interests. The Site is not intended for and may not be used by children under the age of 18. We do not knowingly collect information from children under the age of 18 and we do not target the Site to children under the age of 18.</li>
                        <li><h3>Changes. </h3>We may change this Privacy Policy from time to time. If under any such update we make any material change to the way in which we treat your User Information, we will inform you of such change via e-mail. Such changes shall be effective immediately upon our posting the revised Privacy Policy on the Site. Your continued use of the Site after such modifications will constitute your acknowledgment of the modified Privacy Policy and agreement to abide by and be bound by any modified Privacy Policy.</li>
                    </ol>
                    <p><span class="strengthen">Questions.</span> If you have any questions about this Privacy Policy, please feel free to contact us:</p>
                    <ul class="no-style-type">
                        <li>Power Almanac Information Privacy Site Coordinator</li>
                        <li>205 De Anza Blvd #59</li>
                        <li>San Mateo, CA 94402</li>
                        <li>Phone: 650-539-2340</li>
                        <li>Email: <a href='mai&#108;t&#111;&#58;&#112;&#37;&#55;&#50;iv%61c&#37;7&#57;&#64;po%&#55;&#55;eral&#109;an&#97;%&#54;3&#46;c%&#54;&#70;&#109;'>privacy&#64;pow&#101;ralmanac&#46;com</a></li>
                    </ul>

                    <div class="row subfooter mt-4">
                        <div class="col-lg-9 col-md-8 col-sm-7 text-center text-sm-left">
                            <strong class="title">Sick of Reading? Try Power Almanac Now.</strong>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-5 text-center text-sm-right">
                            <a href="/search-government?ns=1" class="btn btn-secondary" role="button">Search for Officials</a>
                        </div>
                    </div>
                </div>

            </div>
<?php include("inc/footer.php"); ?>            
