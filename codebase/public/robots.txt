User-agent: *
Allow: /
Disallow: /iCube/
Disallow: /jxarh/

# implented on 7/16/2013
Noindex: /SampleDLFile.csv   # a sample download file that is gibberish-ish - don't think we want that....
Noindex: /forgot-password.php
Disallow: /PA-ShareSearch.php
Noindex: /PA-PreviewPowerSearch.php‎
Noindex: /PA-Subscribe.php
Noindex: /PA-PreviewData.php
Disallow: /PA-RegisterStep2.php
Disallow: /activation.php