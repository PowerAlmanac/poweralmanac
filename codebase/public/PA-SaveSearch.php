<?php

include("inc/config.php");
include("inc/protect-register.php");

$User_Email = $_SESSION['user_email'];
$User_FirstName = $_SESSION['user_firstname'];
$User_Subscription = $_SESSION['user_subscription'];

$title = 'Save A Search';

$user_email = $_SESSION['user_email'];

// debug
$searchParams = $_SESSION['lastSearchParams'];
$numRecords = $_SESSION['lastSearchNumMatched'];
//echo("DEBUG: [$searchParams] ($numRecords) <br>");

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
	Custom.checkAll();
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
	Custom.clear();
}
//  End -->
</script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'mysearches';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstrip2.png" valign="top">
            <br>
            <table cellpadding="5" cellspacing="5" border="0" align="center" width="95%" bgcolor="#FFFFFF">
            <tr>
                <td>
                <h3>Save A Search</h3>
                <p>
                Name your search and click 'Save It'.
                <br />
                <form action="PA-process-savesearch.php" method="post" name="savesearch">
                <table cellpadding="2" cellspacing="3" border="0" width="100%" style="border: 1px solid #FFFFFF">
                <tr>
                    <td width="35%">
                    <p><b>Saved Search Name/Label: </b></p>
                    </td>
                    <td width="65%">
                    <p>
                    <input name="savesearch_label" type="text" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;' size="50"> (do not use punctuation)
                    </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                    <input type="submit" name="submit" value="Save It!" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: bold;'>
                    &nbsp;&nbsp;
                    <input type="reset" name="reset" value="Reset Name" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;'>
                    </td>
                </tr>
                <tr>
                	<td>
                    </td>
                    <td>
                    <?php
					$savesearch_message = $_SESSION['savesearch_message'];
					if ($savesearch_message != '') {
						echo("<font color=#ff0000>$savesearch_message</font>");
					}
					?>
                    </td>
                </tr>
                </table>
                </form>
                </p>
                </td>
            </tr>
            </table>
            <br>
			</td>
		</tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstrip2.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" align="center" bgcolor="#ebebeb">
    <?php
	include("inc/oldfooter.php");
	?>
    </td>
</tr>
</table>
<br />
</body>
</html>
