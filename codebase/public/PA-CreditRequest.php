<?php

include("inc/config.php");
include("inc/protect-login.php");

$Parent_Email = $_REQUEST['e'];
$creditsNeeded = $_REQUEST['c'];

// Sanitize
$Parent_Email = Sanitize::variable($Parent_Email, "@.-+");
$creditsNeeded = Sanitize::number($creditsNeeded);

$User_Email = $_SESSION['user_email'];
$User_FirstName = $_SESSION['user_firstname'];
$User_LastName = $_SESSION['user_lastname'];
$User_Subscription = $_SESSION['user_subscription'];
$RegUser_ID = $_SESSION['RegUser_ID'];

// use Amazon SES
require_once('ses.php');
$ses = new SimpleEmailService('AKIAI5D3JYIQSM534C6A', 'Q0xFC1uu1pLOhMO68782UC6rG5rKSlsb1umf87AE');
$m = new SimpleEmailServiceMessage();
$m->addTo("$Parent_Email");
$m->setFrom('Power Almanac Support <support@poweralmanac.com>');
$m->setReturnPath('support@poweralmanac.com');
$m->setSubject('Sub-User Download Credit Request - Power Almanac');
$m->setMessageFromString("Your Sub-User $User_FirstName $User_LastName is requesting more credits for his/her Power Almanac account. S/he wants to download a file that requires $creditsNeeded additional download credits.\n\nYou can add credits by going to your 'MyAccount' page and editing this sub-user's account credit limit. Click the link below to sign in to your Power Almanac account:\n$serverURL/login.php");

$rc = $ses->sendEmail($m);

// send email
//$mail_status = mail("$Parent_Email","Sub-User Download Credit Request - Power Almanac","Your Sub-User $User_FirstName $User_LastName is requesting more credits for his/her Power Almanac account. S/he wants to download a file that requires $creditsNeeded additional download credits.\n\nYou can add credits by going to your 'MyAccount' page and editing this sub-user's account credit limit. Click the link below to sign in to your Power Almanac account:\n$serverURL/login.php","From: $User_Email ($User_FirstName via Power Almanac)");

?>
<html>
<head>
<title>Credit Request</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="popuphelp.js"></script>
<script type="text/javascript">
function tb_remove() {
 	$("#TB_imageOff").unbind("click");
	$("#TB_closeWindowButton").unbind("click");
	$("#TB_window").fadeOut("fast",function(){$('#TB_window,#TB_overlay,#TB_HideSelect').trigger("unload").unbind().remove();});
	$("#TB_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		$("body","html").css({height: "auto", width: "auto"});
		$("html").css("overflow","");
	}
	document.onkeydown = "";
	document.onkeyup = "";
	return false;
}
</script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
</head>
</head>

<body bgcolor="#ACC6E1" background="images/ACC6E1-pixel.jpg">

<table width="600" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#ACC6E1">
<tr>
	<td valign="top" bgcolor="#ACC6E1">
    <br><font style="font-size:16px; font-weight:bold">Your CREDIT REQUEST has been sent!</font> 
    <a href="#" onClick="window.parent.Shadowbox.close()"><img src="images/preview-close.jpg" alt="Close Preview" width="40" height="40" hspace="5" vspace="0" border="0" align="right"></a>
    <br>
    <p>
    Please check your <b>MyAccount</b> tab periodically to see if the Account Master user has allocated more credits to your account.
    </p>
    <p>
    <a href="#" onClick="window.parent.Shadowbox.close()">Close Window</a>
    </p>
	</td>
</tr>
</table>


</body>
</html>
