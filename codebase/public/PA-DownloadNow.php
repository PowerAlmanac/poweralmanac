<?php

include("inc/config.php");
include("inc/protect-login.php");

$User_Email = $_SESSION['user_email'];

$User_FirstName = $_SESSION['user_firstname'];
$User_Subscription = $_SESSION['user_subscription'];
$numRecords2DL = $_SESSION['lastSearchNumMatched'];
$searchParams = $_SESSION['lastSearchParams'];
$lastSearchName = $_SESSION['lastSearchName'];
if ($lastSearchName == '') {
	$lastSearchName = 'TBD-unknown';
}

// make it unique - workaround bug
$randval = mt_rand();
$lastSearchName = "DL$randval";
$_SESSION['downloadFileName'] = $lastSearchName;

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
$RegUser_ID = $jsonArray2a['RegUser_ID'];
$User_ActivationCode = $jsonArray2a['User_ActivationCode'];


$previoustableID = $_SESSION['previousTableID'];
$currentTableID = $_SESSION['currentTableID'];


$_SESSION['downloadCMD'] = "/bin/bash /var/www/sh/SaveCurrentDownload.sh \"$numRecords2DL\" \"$currentTableID\" \"$searchParams\" \"$RegUser_ID\" \"$lastSearchName\" \"$lastSearchName\" 2>/dev/null";
$output = shell_exec($_SESSION['downloadCMD']);

// download generated successfully
$record2Charge = $_SESSION['record2Charge'];
$newReserves = $User_DL_Reserves - $record2Charge;

echo "Charged Records: $record2Charge";
echo "Previous Credits: $User_DL_Reserves";

if ($User_Subscription != '10') {
	$pdo->query(sprintf("CALL UpdateDLCount('%s',%s,'%s',@rc)", escape($User_Email), escape($newReserves), escape($User_ActivationCode)));
	$pdoObject = $pdo->query("SELECT @rc");
	$rsArray = $pdoObject->fetchAll();
	$RC = $rsArray[0]['@rc'];
}

// update DL count for sub-users only
$User_Parent = $_SESSION['User_Parent'];
if ($User_Parent != 0) {
	$sql ="USE " . escape($dbname);
	$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
	if (!$dbcnx) {
		print("Unable to connect to the database server at this time.\n");
	exit();
	}
	if (!@PowerAlmanac\PDb::query($sql)){
		die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
	}
	// insert record count
	$insertRecordCount_sql = sprintf("INSERT INTO userpay (UserEmail,NumRecords)
		VALUES ('%s','%s')
	", escape($User_Email), escape($record2Charge));
	$result_insertRecordCount = @PowerAlmanac\PDb::query($insertRecordCount_sql);
	if (!$result_insertRecordCount) {
		die('Error writing to userpay database:' . PowerAlmanac\PDb::error());
	}
}
header("Location: ./dashboard?downloadID=" . urlencode($lastSearchName) . "#mydownloads");
flush();
exit;
