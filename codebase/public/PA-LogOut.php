<?php

include("inc/config.php");

$_SESSION['logged_in'] = '0';
$_SESSION['login_message'] = '';
$_SESSION['subscribe_message'] = '';
$_SESSION['subuser_message'] = '';
$_SESSION['passwordchange_message'] = '';
$_SESSION['user_email'] = "";
$_SESSION['user_actcode'] = "";
$_SESSION['user_firstname'] = "";
$_SESSION['user_lastname'] = "";
$_SESSION['user_subscription'] = "";
$_SESSION['RegUser_ID'] = '0';
$_SESSION['User_Parent'] = '0';

// search related
$_SESSION['useLastSearchParams'] = '0';
$_SESSION['lastSearchParams'] = 'bytext=&';
$_SESSION['lastSearchName'] = '';
$_SESSION['lastSearchNumMatched'] = '';

$_SESSION['useSavedSearchParams'] = '0';
$_SESSION['savedSearchParams'] = '';
$_SESSION['savedSearchName'] = '';
$_SESSION['savesearch_message'] = '';

/// defaults
$_SESSION['checkbox_GovtTypes'] = '1';
$_SESSION['checkbox_Roles'] = '0';
$_SESSION['checkbox_Population'] = '1';
$_SESSION['checkbox_FiscalYear'] = '1';
$_SESSION['checkbox_States'] = '1';
$_SESSION['checkbox_Demographics'] = '1';

// handling summary and re-run search
$_SESSION['GovtSummary_NumByGovtTypeArray'] = '';
$_SESSION['GovtSummary_NumByStateArray'] = '';

$_SESSION['GovtOffSummary_NumByRoleArray'] = '';
$_SESSION['GovtOffSummary_NumByStateArray'] = '';

// PRG Fix
$_SESSION['searchRequestArrayJSON'] = '';
$_SESSION['firstTimeSearch'] = 1;

// reset 
$_SESSION['results_GovtOffMustMatchRoles'] = '1';
$_SESSION['results_GovtOffMustHaveEmails'] = '0';

// captcha
$_SESSION['captcha_message'] = '';

// remove last tables
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$previoustableID = $_SESSION['previousTableID'];
$pdo->query(sprintf("CALL Delete_Research_Tables('%s')", escape($previoustableID)));

// affiliate ID reset
$_SESSION['affiliateID'] = 0; 
	

// Deltek REset
$_SESSION['deltek_exists'] = FALSE; 
$_SESSION['deltek_active'] = FALSE; 
$_SESSION['deltek_session'] = FALSE; 

// go to home
header("Location: search-government.php");
flush();


?>
