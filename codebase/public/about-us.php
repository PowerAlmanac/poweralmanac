<?php
define("TITLE", "History and usage ideas");
define("DESCRIPTION", "Learn why we founded our comprehensive government list and about the different uses of our powerful data.");
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder container">
                    <h1>About Us</h1>
                </div>
            </div>
            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box pl-0 pr-0">
                    <div class="row m-b-20 pl-5">
                        <div class="col-md-7">
                            <h2>Why we created Power Almanac</h2>
                            <strong class="subheading">So you can contact the people who actually make the decisions.</strong>
                        </div>
                        <div class="col-md-4 offset-md-1 pl-5">
                            <strong class="title">Try the Search</strong>
                            <form action="PA-PRG.php" method="post" class="form-search form form-inline">
                                <fieldset>
                                    <div class="form-group">
                                        <?=$officialDrop?>
                                        <input type="submit" value="Go &#187;" class="btn btn-primary" />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                    <div class="sticky-top navbar navbar-light bg-light">
                        <nav class="nav nav-pills flex-column flex-sm-row">
                            <a class="flex-sm-fill text-sm-center nav-link active" href="#overview">Overview</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="#theBigIdea">The Big Idea</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="#whoIsItFor">Who Is It For?</a>
                        </nav>
                        <a href="#" class="btn btn-secondary">Back to Top</a>
                    </div>

                    <div class="row mb-3 pl-4 pr-4">
                        <div class="col-md-12 p-0">
                            <img class="responsive w-100" src="images/about-pa-old-government.jpg" alt="Avoid confusion of finding government officials" />
                            <h2 id="overview" class="mt-3 p-3">2010: The year finding them got a whole lot easier.</h2>
                            <div class="p-3 font-weight-bold mt-2 mb-2">Local governments in the U.S. exert a lot of power and influence.</div>
                        </div>
                    </div>
                    <div class="row mb-5 pl-4 pr-4">
                        <div class="col-md-6">
                            <p>97% of our country’s elected officials are local government officials, and local governments collectively spend nearly $1.6 trillion annually and employ about 14 million people.</p>
                            <p class="mt-4 mb-4">And yet despite their collective power and influence, local governments and their elected and appointed leaders can seem almost invisible. It's difficult to impossible to get information about them.</p>
                            <p id="theBigIdea" class="font-italic">Not anymore.</p>
                        </div>
                    </div>

                    <div class="row mb-5 pl-4 pr-4 pb-2">
                        <div class="col-md-6">
                            <h2 class="mb-5">The Big Idea</h2>
                            <p>The idea behind the Power Almanac is pretty simple. Create the <b>most comprehensive and up-to-date online source of data</b> and information about local governments, and make it very easy to use.</p>
                            <p class="mt-5 mb-5">Of course, simple ideas are often challenging to implement, and that's certainly been true for the Power Almanac. To start with, there are <b>39,045 counties, municipalities, and townships</b> in the U.S. That's a lot of data to track down, especially when many local governments have out-of-date websites, or no websites at all.</p>
                            <p>The bigger challenge stems from the <b>astonishing diversity</b> of local government structures and leadership positions in the U.S. - which varies not only from state to state, but even from county to county within a state. Finding ways to organize all that diverse data so that you can quickly and easily find exactly what you need kept us up a few nights.</p>
                            <div class="pt-3 font-weight-bold mt-4 mb-3">Our Plans for Power Almanac</div>
                            <p>We're proud of the Power Almanac, and we hope you'll try it out and see why. But we're not done. In fact, we'll probably never really be done. We'll keep working hard to make the Power Almanac increasingly valuable for you by adding more data and making it even easier to use.</p>
                        </div>
                        <div class="col-md-6">
                            <img src="images/about-ben-franklin.png" alt="Ben Franklin thinking government contact" class="responsive float-right pr-3" />
                        </div>
                    </div>
<hr id="whoIsItFor" />
                    <div class="row mb-3 pl-4 pr-4 pt-5">
                        <div class="col-md-12">
                            <div class="row col-md-6">
                                <h2 class="mb-4">Who is it for?</h2>
                                <p class="d-block">Anyone can use the Power Almanac to get the information they need about local governments and local government officials. How you use it depends on your goal.</p>
                                <p class="mt-4 mb-4">For example: </p>
                            </div>
                            <table class="table mt-2">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="border-0" scope="col">USER</th>
                                        <th class="border-0" scope="col">GOAL</th>
                                        <th class="border-0" scope="col">HOW POWER ALMANAC CAN HELP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Company</td>
                                        <td>Sell your product or service to local governments</td>
                                        <td><ul><li>Analyze the large and unique local government market</li><li>Reach key decision-makers</li></ul></td>
                                    </tr>
                                    <tr>
                                        <td>Local Government</td>
                                        <td>Find better and more cost-effective ways to deliver your services</td>
                                        <td><ul><li>Benchmark your spending against other local governments</li><li>Identify your potential opportunities for improvement.</li></ul></td>
                                    </tr>
                                    <tr>
                                        <td>Advocacy Group</td>
                                        <td>Ensure that key local government officials hear your perspectives</td>
                                        <td><ul><li>Pinpoint which local governments hear your message</li><li>Reach key opinion leaders</li></ul></td>
                                    </tr>
                                    <tr>
                                        <td>Media</td>
                                        <td>Quickly find the local government officials you need for your story</td>
                                        <td><ul><li>Determine which local governments meet your criteria</li><li>Reach out to exactly the right officials in the right roles</li></ul></td>
                                    </tr>
                                    <tr>
                                        <td>Citizen</td>
                                        <td>Learn more about your own local governments</td>
                                        <td><ul><li>Find out who the leaders are</li><li>Discover how government money is being spent</li></ul></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- subfooter -->
                    <div class="row col-md-10 subfooter mt-4 pl-4 pr-4">
                        <div class="col-md-8 text-center text-sm-left">
                            <strong class="title">Need more convincing? Try Power Almanac</strong>
                        </div>
                        <div class="col-md-4 text-center text-sm-right">
                            <a href="/search-government?ns=1" class="btn btn-light" role="button">Search for Officials</a>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>            
