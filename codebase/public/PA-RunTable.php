<?php

include("inc/config.php");
include("inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$savedTableID = $_REQUEST['id'];

// Sanitize
$savedTableID = Sanitize::number($savedTableID);

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");

if(!$dbcnx)
{
	print("Unable to connect to the database server at this time.\n");
	exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$RegUser_ID = $_SESSION['RegUser_ID'];

$readTables_sql = sprintf("SELECT * FROM SavedTables
	WHERE RegUser_ID = '%s' AND SavedTable_ID = '%s'
	LIMIT 1
", escape($RegUser_ID), escape($savedTableID));
$result_readTables = @PowerAlmanac\PDb::query($readTables_sql);
if (!$result_readTables) {
	die('Error reading from SavedTables database:' . PowerAlmanac\PDb::error());
}

$row = PowerAlmanac\PDb::fetch_array($result_readTables);
$Table_Params = $row['Table_Params'];
$Search_Params = $row['Search_Params'];

$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $Search_Params;

// update date time run
$update_sql = sprintf("UPDATE SavedTables SET DateTime_Rerun = now()
	WHERE RegUser_ID = '%s' AND SavedTable_ID = '%s'
", escape($RegUser_ID), escape($savedTableID));
$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating SavedTables: '. PowerAlmanac\PDb::error().'</p>');
}

// off to analyze
$analyzeURL = "PA-Analyze.php?$Table_Params";
header("Location: $analyzeURL");
flush();


?>