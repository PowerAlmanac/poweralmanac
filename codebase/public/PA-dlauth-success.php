<?php

include("inc/config.php");
//include("inc/protect-login.php");

$User_Email = $_SESSION['user_email'];
$User_FirstName = $_SESSION['user_firstname'];
$User_Subscription = $_SESSION['user_subscription'];

$title = 'Download Authorization Success';

// include init files here for specific page
// include("inc/init-account.php");

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
	Custom.checkAll();
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
	Custom.clear();
}
//  End -->
</script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'createyourlist';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstrip2.png" valign="top">
   			<br>
            <table cellpadding="5" cellspacing="5" width="95%" align="center" border="0" bgcolor="#FFFFFF">
            <tr>
                <td>
                <h3>Purchase via PayPal Successful</h3>
                <p>
                <b>You have now purchased additional download credits.</b>
                <br /><br />
                Please check your email for a transaction receipt from PayPal. (Not there?  Be sure to check your spam/junk folders too.)
                <br /><br />
                <b>NOTE:</b><br />Your additional download credits should be reflected on your <a href="PA-MyAccount.php">"MyAccount"</a> page within the next 1-2 minutes.
                <br /><br />
                Go back to your <a href="PA-ReRunLastSearch.php">last search</a> now that you have enough download credits to download the full results.
                </p>
                </td>
            </tr>
            </table>
            <br>
			</td>
		</tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstrip2.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
    <br />
    <?php
	include("PA-inc-specificsearch.php");
	?>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
	<img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle">
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
    <?php
	include("inc/oldfooter.php");
	?>
    <br />
    </td>
</tr>
</table>
<br />
</body>
</html>
