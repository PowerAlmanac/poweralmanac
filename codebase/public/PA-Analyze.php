<script type="text/javascript">
	if(parent.location.href!=location.href) top.location.href=location.href;
</script>
<?php

include("inc/config.php");

if (isset($_SESSION['useSavedSearchParams'])) {
	if ($_SESSION['useSavedSearchParams'] == '1') {
		$searchParams = $_SESSION['savedSearchParams'];
		$_SESSION['lastSearchParams'] = $searchParams;
	} else {
		$searchParams = $_SESSION['lastSearchParams'];
		$_SESSION['savedSearchParams'] = $searchParams;
		$_SESSION['useSavedSearchParams'] = '0';
	}
} else {
	$searchParams = $_SESSION['lastSearchParams'];
	$_SESSION['savedSearchParams'] = $searchParams;
	$_SESSION['useSavedSearchParams'] = '0';
}

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

if (isset($_REQUEST['skipBuild'])) {
	$skipBuild = $_REQUEST['skipBuild'];
} else {
	$skipBuild = 0;
}

if ($_SESSION['logged_in'] != '1') { // Not logged in
	// not logged in
	$User_Email = 'unregistered_user@poweralmanac.com';
	$User_FirstName = 'Unregistered';
	$User_Subscription = 0;
	$_SESSION['user_subscription'] = '0';
	$_SESSION['RegUser_ID'] = '0';
	$RegUser_ID = 0;
	$tableID = $_SESSION['currentTableID'];
	// don't need to show search summary
	/*
	if ($skipBuild != '1') {
		$timeIs = time();
		$tableID = $RegUser_ID . '_' . substr($timeIs,-6);
		// force full table search
		$searchParams = 'byText=&';
		$pdo->query("CALL SearchSummary('$tableID',true,'$searchParams',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists)");
	} else {
		// used saved SearchSummary 
		$tableID = $_SESSION['currentTableID'];
	}
	*/
} else { // Logged in
	$User_Email = $_SESSION['user_email'];
	$User_FirstName = $_SESSION['user_firstname'];
	$User_Subscription = $_SESSION['user_subscription'];
	$RegUser_ID = $_SESSION['RegUser_ID'];
	if ($skipBuild != '1') {
		$timeIs = time();
		$tableID = $RegUser_ID . '_' . substr($timeIs,-6);
		if ($User_Subscription < 2) {
			$tableID = $_SESSION['currentTableID'];
			// don't need to show search summary
			/*
			// force full table search
			$searchParams = 'byText=&';
			$pdo->query("CALL SearchSummary('$tableID',true,'$searchParams',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists)");
			*/
		} else {
			// build dynamic tables for Power subscriber
			if ($_SESSION['results_GovtOffMustMatchRoles'] == '1') {
				$searchParams = $searchParams . "&top_elected=0&";
			} else {
				$searchParams = $searchParams . "&top_elected=1&";
			}

			set_time_limit(180);
			$pdo->setAttribute(PDO::ATTR_TIMEOUT, 180);
            $stmt = $pdo->query(sprintf("CALL SearchSummary2('%s','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($tableID), escape($searchParams)));
            $row = $stmt->closeCursor();
			$stmt2 = $pdo->query(sprintf("CALL Bulid_Research_Tables2('%s',@exists)", escape($tableID))); // This _should_ be buLid (not build)
            $row2 = $stmt2->closeCursor();
		}
	} else {
		// used saved SearchSummary 
		$tableID = $_SESSION['currentTableID'];
	}
}

if ($skipBuild != '1') {
	if (isset($_SESSION['previousTableID'])) {
		// delete previous table
		$previoustableID = $_SESSION['previousTableID'];
		$pdo->query(sprintf("CALL Delete_Research_Tables('%s')", escape($previoustableID)));
		// now
		$_SESSION['previousTableID'] = $tableID;
		$_SESSION['currentTableID'] = $tableID;
	} else {
		// first time
		$_SESSION['previousTableID'] = $tableID;
		$_SESSION['currentTableID'] = $tableID;
	}
}

//echo("DEBUG: tableID: $tableID<br>");

$title = 'Analyze Local Gov Data | Power Almanac';

if ($skipBuild != '1') {
	$pdoObject = $pdo->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag");
	$rsArray = $pdoObject->fetchAll();
	$JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
	$JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
	$JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];
	$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
	$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
	$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);
	$_SESSION['JSON_SUMMARY'] = $JSON_SUMMARY;
} else {
	$JSON_SUMMARY = $_SESSION['JSON_SUMMARY'];
	$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
}

$Num_Matched_GovernmentsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Governments']);
$Num_Matched_OfficialsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Officials']);
//$Num_Matched_RecordsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Records']);
$Num_Matched_EmailsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Emails']);
$Num_Matched_Emails = number_format($JSON_SUMMARY_Array['Num_Matched_Emails']);
$Num_Matched_Officials = $JSON_SUMMARY_Array['Num_Matched_Officials'];
$Phone_NumbersFORMATTED = number_format($JSON_SUMMARY_Array['Phone_Numbers']);
$Mailing_AddressesFORMATTED = number_format($JSON_SUMMARY_Array['Mailing_Addresses']);
// percentage calculations
$Num_Matched_Emails = $JSON_SUMMARY_Array['Num_Matched_Emails'];
$Num_Matched_Officials = $JSON_SUMMARY_Array['Num_Matched_Officials'];
$Phone_Numbers = $JSON_SUMMARY_Array['Phone_Numbers'];
$Mailing_Addresses = $JSON_SUMMARY_Array['Mailing_Addresses'];
if ($Num_Matched_Officials != 0) {
	$Percentage_Matched_Emails = sprintf("%01.0f",($Num_Matched_Emails/$Num_Matched_Officials)*100);
	$Percentage_Matched_Phone_Numbers = sprintf("%01.0f",($Phone_Numbers/$Num_Matched_Officials)*100);
	$Percentage_Matched_Mailing_Addresses = sprintf("%01.0f",($Mailing_Addresses/$Num_Matched_Officials)*100);
} else {
	$Percentage_Matched_Emails = "0.00%";
	$Percentage_Matched_Phone_Numbers = "0.00%";
	$Percentage_Matched_Mailing_Addresses = "0.00%";
}


$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// for visitors or non-Power subscribers, use createpa tables only
if ($_SESSION['logged_in'] != '1') {
	// not logged in
	$tableID = 'createpa';
} else {
	if ($User_Subscription < 2) {
		// force full table search
		$tableID = 'createpa';
	}
}

/* 

TALLY RECORDS
selectType = 1
selectColumn - Role, Population, Government Type
selectRow - States, Population, Government Type
selectCount - Count, % of Row, % of Column, % of Table

COMPARE SPENDING
selectType = 2
selectColumn - Government Type, Population, >> States, Total Budget Quartiles, Per Capita Budget Quartiles
selectRow - States, Population, >> Budget Categories
selectCount - Median, Per Capita Median

*/

if (isset($_REQUEST['count'])) {
	$selectType = $_REQUEST['type'];
	$selectCount = $_REQUEST['count'];
	$selectRow = $_REQUEST['row'];
	$selectColumn = $_REQUEST['column'];
} else {
	$selectType = '1';
	$selectCount = '1';
	$selectRow = '2'; // population ranges
	$selectColumn = '1'; // roles
}

// check status
$read_sql = sprintf("SELECT * FROM ready_table
	WHERE table_string = '%s'
	LIMIT 1
	", escape($tableID));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from government database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$light = $onerow['light'];
$shadowbox = '';
switch($selectType) {
	case '1': // tally
		$selectedTallyImage = 'images/tallytab-sel.png';
		$keyImage = 'images/key-govtoff.png';
		// check if ready
		if ($light != '1') {
			//$selectedCompareImage = 'images/comparetab-busy.png';
			$selectedCompareImage = 'images/comparetab.png';
			$shadowbox = " rel='shadowbox;height=200;width=625' ";
			$compareURL = "PA-CheckLight.php?id=$tableID";
		} else {
			$selectedCompareImage = 'images/comparetab.png';
			$compareURL = 'PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3';
		}
		break;
	case '2': // compare
		$keyImage = 'images/key-median.png';
		$selectedTallyImage = 'images/tallytab.png';
		// check if ready
		if ($light != '1') {
			//$selectedCompareImage = 'images/comparetab-busy.png';
			$selectedCompareImage = 'images/comparetab.png';
			$shadowbox = " rel='shadowbox;height=200;width=625' ";
			$compareURL = "PA-CheckLight.php?id=$tableID";
		} else {
			$selectedCompareImage = 'images/comparetab-sel.png';
			$compareURL = 'PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3';
		}
		break;
	default:
		$keyImage = 'images/key-govtoff.png';
		$selectedTallyImage = 'images/tallytab-sel.png';
		// check if ready
		if ($light != '1') {
			//$selectedCompareImage = 'images/comparetab-busy.png';
			$selectedCompareImage = 'images/comparetab.png';
			$shadowbox = " rel='shadowbox;height=200;width=625' ";
			$compareURL = "PA-CheckLight.php?id=$tableID";
		} else {
			$selectedCompareImage = 'images/comparetab.png';
			$compareURL = 'PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3';
		}
		break;
}

// init select settings
$reqCountSelect['1'] = '';
$reqCountSelect['2'] = '';
$reqCountSelect['3'] = '';
$reqCountSelect['4'] = '';

$reqRowSelect['1'] = '';
$reqRowSelect['2'] = '';
$reqRowSelect['3'] = '';
$reqRowSelect['4'] = '';

$reqColumnSelect['1'] = '';
$reqColumnSelect['2'] = '';
$reqColumnSelect['3'] = '';
$reqColumnSelect['4'] = '';
$reqColumnSelect['5'] = '';

// disable invalid combinations
$disableRowSelect['1'] = '';
$disableRowSelect['2'] = '';
$disableRowSelect['3'] = '';
$disableRowSelect['4'] = '';

$disableColumnSelect['1'] = '';
$disableColumnSelect['2'] = '';
$disableColumnSelect['3'] = '';
$disableColumnSelect['4'] = '';
$disableColumnSelect['5'] = '';

// set selections

$reqCountSelect[$selectCount] = ' selected';
$reqRowSelect[$selectRow] = ' selected';
$reqColumnSelect[$selectColumn] = ' selected';

/*

Table Mapping

Tally
A States - Roles (state_roles_#)
B GovtTypes - Roles (govtypes_roles_#)
C Population - Roles (population_roles_#)
D States - GovtTypes (state_govtypes_#)
E States - Population (state_population_categorys_#)
F Population - GovtTypes (population_categorys_govtypes_#)

Compare
G States - GovtTypes ()
H States - Population ()
I Population - GovtTypes ()
J Budget - GovtTypes
K Budget - Population
L Budget - States
M Budget - TotalBudget

N Budget - PerCapitaBudget

*/

switch($selectType) {
	case '1': // TALLY RECORDS
		$helpText = "<ul><li><b>Officials included:</b> 100% of the officials from your search results, and no others</li><li><b>Governments included:</b> 100% of the governments from your search results, and no others</li><li><b>Data in the cells:</b> You can change the data in the cells from counts to percentages.  The percent of row/column/table data are based on the counts.</li><li><b>Total row governments:</b> If you choose &quot;Roles&quot; as you column headers, then keep in mind that the &quot;row total&quot; for the number of governments will NOT equal the sum of the governments in each cell in the row.  That\'s because the same government may be represented in two or more cells in that row.</li></ul>";
		switch($selectRow) {
			case '1':
				$rowHeader = 'States';
				$rowArray = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut');
				switch($selectColumn) {
					case '1':
						$columnHeader = 'Role';
						$columnArray = array('Top Elected Official','Governing Board Members','Top Appointed Official','Head Of Finance Budget','Head Of Purchasing Procurement','Head Of Public Works','Head of Law Enforcement','Head of Fire Services','Head Clerk','Head of IT');
						$fullTableID = $tableID . "_state_roles_1"; // get raw numbers
						$read_sql = sprintf("SELECT * FROM %s
							ORDER BY state
							", escape($fullTableID));
						$fullGovtTableID = $tableID . "_state_roles_govs";	
						$readGovt_sql = sprintf("SELECT * FROM %s
							ORDER BY state
							", escape($fullGovtTableID));
						break;
					case '2':
						$columnHeader = 'Population';
						//$columnArray = array('Unknown', '999 or less','1,000 to 2,499','2,500 to 4,999','5,000 to 9,999','10,000 to 24,999','25,000 to 49,999','50,000 to 99,999','100,000 to 499,999','500,000 or more');
						$columnArray = array('500,000 or more', '100,000 to 499,999','50,000 to 99,999','25,000 to 49,999','10,000 to 24,999','5,000 to 9,999','2,500 to 4,999','1,000 to 2,499','999 or less','Unknown');
						$fullTableID = $tableID . "_state_population_categorys_1";
						$read_sql = sprintf("SELECT * FROM %s
							ORDER BY state
							", escape($fullTableID));
						$fullGovtTableID = $tableID . "_state_population_categorys_govs";	
						$readGovt_sql = sprintf("SELECT * FROM %s
							ORDER BY state
							", escape($fullGovtTableID));
						break;
					case '3':
						$columnHeader = 'Government Types';
						//$columnArray = array('Counties','Municipalities','Townships');
						$columnArray = array('Counties','Townships','Municipalities');
						$fullTableID = $tableID . "_state_govtypes_1";
						$read_sql = sprintf("SELECT * FROM %s
							ORDER BY state
							", escape($fullTableID));
						$fullGovtTableID = $tableID . "_state_govtypes_govs";	
						$readGovt_sql = sprintf("SELECT * FROM %s
							ORDER BY state
							", escape($fullGovtTableID));
						break;
					default:
						break;
				}
				break;
			case '2':
				$rowHeader = 'Population';
				$rowArray = array('Unknown', '999 or less','1,000 to 2,499','2,500 to 4,999','5,000 to 9,999','10,000 to 24,999','25,000 to 49,999','50,000 to 99,999','100,000 to 499,999','500,000 or more');
				$disableColumnSelect['2'] = ' disabled ';
				switch($selectColumn) {
					case '1':
						$columnHeader = 'Role';
						$columnArray = array('Top Elected Official','Governing Board Members','Top Appointed Official','Head Of Finance Budget','Head Of Purchasing Procurement','Head Of Public Works','Head of Law Enforcement','Head of Fire Services','Head Clerk','Head of IT');
						$fullTableID = $tableID . "_population_roles_1";
						$read_sql = sprintf("SELECT * FROM %s
							ORDER BY Population_category DESC
							", escape($fullTableID));
						$fullGovtTableID = $tableID . "_population_roles_govs";	
						$readGovt_sql = sprintf("SELECT * FROM %s
							ORDER BY Population_category DESC
							", escape($fullGovtTableID));
						break;
					case '2':
						// INVALID OPTION
						break;
					case '3':
						$columnHeader = 'Government Types';
						$columnArray = array('Counties','Townships','Municipalities');
						$fullTableID = $tableID . "_population_categorys_govtypes_1";
						$read_sql = sprintf("SELECT * FROM %s
							ORDER BY Population_Category DESC
							", escape($fullTableID));
						$fullGovtTableID = $tableID . "_population_categorys_govtypes_govs";	
						$readGovt_sql = sprintf("SELECT * FROM %s
							ORDER BY Population_Category DESC
							", escape($fullGovtTableID));
						break;
					default:
						break;
				}
				break;
			case '3':
				$rowHeader = 'Roles';
				// TEO GBM TAO HFB HPP HPW HLE HFPS CL
				$rowArray = array('Top Elected Official','Governing Board Members','Top Appointed Official','Head Of Finance Budget','Head Of Purchasing Procurement','Head Of Public Works','Head of Law Enforcement','Head of Fire Services','Head Clerk','Head of IT');
				// INVALID OPTION
				$disableColumnSelect['1'] = ' disabled ';
				$disableColumnSelect['2'] = ' disabled ';
				$disableColumnSelect['3'] = ' disabled ';
				break;
			case '4':
				$rowHeader = 'Government Types';
				$rowArray = array('Counties','Townships','Municipalities');
				$disableColumnSelect['2'] = ' disabled ';
				$disableColumnSelect['3'] = ' disabled ';
				switch($selectColumn) {
					case '1':
						$columnHeader = 'Role';
						$columnArray = array('Top Elected Official','Governing Board Members','Top Appointed Official','Head Of Finance Budget','Head Of Purchasing Procurement','Head Of Public Works','Head of Law Enforcement','Head of Fire Services','Head Clerk','Head of IT');
						$fullTableID = $tableID . "_govtypes_roles_1";
						$read_sql = sprintf("SELECT * FROM %s
							ORDER BY GBM
							", escape($fullTableID));
						$fullGovtTableID = $tableID . "_govtypes_roles_govs";	
						$readGovt_sql = sprintf("SELECT * FROM %s
							ORDER BY GBM
							", escape($fullGovtTableID));
						break;
					case '2':
						// pop ranges
						$columnHeader = 'Population';
						$columnArray = array('500,000 or more', '100,000 to 499,999','50,000 to 99,999','25,000 to 49,999','10,000 to 24,999','5,000 to 9,999','2,500 to 4,999','1,000 to 2,499','999 or less','Unknown');
						$fullTableID = $tableID . "_population_categorys_govtypes_1P";
						$read_sql = sprintf("SELECT * FROM %s", escape($fullTableID));
						$fullGovtTableID = $tableID . "_population_categorys_govtypes_govsP";	
						$readGovt_sql = sprintf("SELECT * FROM %s", escape($fullGovtTableID));
						break;
					case '3':
						// INVALID OPTION
						break;
					default:
						break;
				}
				break;
			default:		
				break;
		}
		break;
		
	case '2': // COMPARE SPENDING
		$helpText = "info about table here...";
		switch($selectRow) { // left
			case '1':
				$rowHeader = 'States';
				$rowArray = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut');
				switch($selectColumn) {
					case '1': // govt types
						$columnHeader = 'Government Types';
						$columnArray = array('Counties','Townships','Municipalities');
						switch($selectCount) {
							case '1': // median
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total expenditure, expressed in thousands ($000s), for the governments that fall within the cell\'s row and column categories.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total column median:</b> This is the median total spend for all governments included in the column.  Note that the dollar amounts are expressed in thousands ($000s).</li></ul>";
								$fullTableID = $tableID . "_state_govtypes_total_expenditure";	
								$read_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									",escape($fullTableID));
								$fullGovtTableID = $tableID . "_state_govtypes_expenditure_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									", escape($fullGovtTableID));
								break;
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita total expenditure for the governments that fall within the cell\'s row and column categories.<li><b>Total row median:</b> This is the median per capita spend for all governments included in the row.</li><li><b>Total column median:</b> This is the median per capita spend for all governments included in the column.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = $tableID . "_state_govtypes_per_capita";	
								$read_sql = sprintf("SELECT * FROM %s 
									ORDER BY state
									", escape($fullTableID));
								$fullGovtTableID = $tableID . "_state_govtypes_per_capita_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									", escape($fullGovtTableID));
								break;
							default:
								break;
						}
						break;
					case '2': // population
						$columnHeader = 'Population';
						$columnArray = array('500,000 or more', '100,000 to 499,999','50,000 to 99,999','25,000 to 49,999','10,000 to 24,999','5,000 to 9,999','2,500 to 4,999','1,000 to 2,499','999 or less','Unknown');
						switch($selectCount) {
							case '1': // median
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total expenditure, expressed in thousands ($000s), for the governments that fall within the cell\'s row and column categories.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total column median:</b> This is the median total spend for all governments included in the column.  Note that the dollar amounts are expressed in thousands ($000s).</li></ul>";
								$fullTableID = $tableID . "_state_population_categorys_total_expenditure";				
								$read_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									", escape($fullTableID));
								$fullGovtTableID = $tableID . "_state_population_categorys_expenditure_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									", escape($fullGovtTableID));
								break;
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita total expenditure for the governments that fall within the cell\'s row and column categories.<li><b>Total row median:</b> This is the median per capita spend for all governments included in the row.</li><li><b>Total column median:</b> This is the median per capita spend for all governments included in the column.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								// no P0 data
								$columnArray = array('500,000 or more', '100,000 to 499,999','50,000 to 99,999','25,000 to 49,999','10,000 to 24,999','5,000 to 9,999','2,500 to 4,999','1,000 to 2,499','999 or less','Unknown');
								$fullTableID = $tableID . "_state_population_categorys_per_capita";
								$read_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									", escape($fullTableID));
								$fullGovtTableID = $tableID . "_state_population_categorys_per_capita_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s
									ORDER BY state
									", escape($fullGovtTableID));
								break;
							default:
								break;
						}
						break;
					case '3': // states
					case '4': // total budget quartiles
					case '5': // per capita budget quartiles
						// INVALID OPTION
						break;
					default:
						break;
				}
				break;
			case '2':
				$rowHeader = 'Population';
				$rowArray = array('500,000 or more', '100,000 to 499,999','50,000 to 99,999','25,000 to 49,999','10,000 to 24,999','5,000 to 9,999','2,500 to 4,999','1,000 to 2,499','999 or less','Unknown');
				switch($selectColumn) {
					case '1': // govt types
						$columnHeader = 'Government Types';
						$columnArray = array('Counties','Townships','Municipalities');
						switch($selectCount) {
							case '1': // median
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total expenditure, expressed in thousands ($000s), for the governments that fall within the cell\'s row and column categories.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total column median:</b> This is the median total spend for all governments included in the column.  Note that the dollar amounts are expressed in thousands ($000s).</li></ul>";
								$fullTableID = $tableID . "_population_categorys_govtypes_total_expenditure";				
								$read_sql = sprintf("SELECT * FROM %s
									ORDER BY Population_Category DESC
									", escape($fullTableID));
								$fullGovtTableID = $tableID . "_population_categorys_govtypes_expenditure_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s
									ORDER BY Population_Category DESC
									", escape($fullGovtTableID));
								break;
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita total expenditure for the governments that fall within the cell\'s row and column categories.<li><b>Total row median:</b> This is the median per capita spend for all governments included in the row.</li><li><b>Total column median:</b> This is the median per capita spend for all governments included in the column.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = $tableID . "_population_categorys_govtypes_per_capita";
								$read_sql = sprintf("SELECT * FROM %s
									ORDER BY Population_Category DESC
									", escape($fullTableID));
								$fullGovtTableID = $tableID . "_population_categorys_govtypes_per_capita_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s
									ORDER BY Population_Category DESC
									", escape($fullGovtTableID));
								break;
							default:
								break;
						}
						break;
					case '2': // population
					case '3': // states
					case '4': // total budget quartiles
					case '5': // per capita budget quartiles
						// INVALID OPTION
						break;
					default:
						break;
				}
				break;
			case '3':
				$rowHeader = 'Budget Category';
				$rowArray = array('TOT','CAP','OPE','SAL','SUP','PUB1','COR','FIR1','JUC','POL','PUB2','ASS','WEL','HEA','HES','HOS','UTL','ELE','GAS','SWE','SOL','WAT','TRA','AIR','PAR1','POR','PUB3','ROA','LEI','LIB','PAR2','FIR2','FIN','INT','MIS','EDU','GEN','HOU','LIQ','NAT','OTH');
				switch($selectColumn) {
					case '1': // govt types
						$columnHeader = 'Government Types';
						$columnArray = array('Counties','Townships','Municipalities');
						switch($selectCount) {
							case '1': // median	
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total spend for that row\'s spend category for the governments in that column, expressed in thousands ($000s).  Note that the median spend in a given cell only includes the governments from your search results that fall within the column category AND that spend more than $0 in that row\'s spend category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row.  Keep in mind that the cells in every row EXCLUDE the zero-spend governments.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total row governments:</b> Keep in mind that the row excludes zero-spend governments.</li></ul>";
								$fullTableID = $tableID . "_budget_govtypes_median_total";						
								$read_sql = sprintf("SELECT * FROM %s", escape($fullTableID));
								$fullGovtTableID = $tableID . "_budget_govtypes_median_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s", escape($fullGovtTableID));
								break;
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita spend for that row\'s spend category for the governments in that column.  Note that the median per capita spend in a given cell only includes the governments from your search results that fall within the column category AND that spend more than $0 in that row\'s spend category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median per capita spend for all governments included in the row.  Keep in mind that the cells in every row EXCLUDE the zero-spend governments.</li><li><b>Total row governments:</b> Keep in mind that the row excludes zero-spend governments.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = $tableID . "_budget_govtypes_median_per_capita";
								$read_sql = sprintf("SELECT * FROM %s", escape($fullTableID));
								$fullGovtTableID = $tableID . "_budget_govtypes_median_per_capita_govs";	
								$readGovt_sql = sprintf("SELECT * FROM %s", escape($fullGovtTableID));
								break;
							default:
								break;
						}
						break;
					case '2': // population
						$columnHeader = 'Population';
						$columnArray = array('500,000 or more', '100,000 to 499,999','50,000 to 99,999','25,000 to 49,999','10,000 to 24,999','5,000 to 9,999','2,500 to 4,999','1,000 to 2,499','999 or less','Unknown');
						switch($selectCount) {
							case '1': // median	
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total spend for that row\'s spend category for the governments in that column, expressed in thousands ($000s).  Note that the median spend in a given cell only includes the governments from your search results that fall within the column category AND that spend more than $0 in that row\'s spend category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row.  Keep in mind that the cells in every row EXCLUDE the zero-spend governments.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total row governments:</b> Keep in mind that the row excludes zero-spend governments.</li></ul>";
								$fullTableID = escape($tableID . "_budget_population_median_total");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_population_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								break;
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita spend for that row\'s spend category for the governments in that column.  Note that the median per capita spend in a given cell only includes the governments from your search results that fall within the column category AND that spend more than $0 in that row\'s spend category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median per capita spend for all governments included in the row.  Keep in mind that the cells in every row EXCLUDE the zero-spend governments.</li><li><b>Total row governments:</b> Keep in mind that the row excludes zero-spend governments.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = escape($tableID . "_budget_population_median_per_capita");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_population_median_per_capita_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								break;
							default:
								break;
						}
						break;
					case '3': // states
						$columnHeader = 'States';
						$columnArray = array('AL','AK','AZ','AR','MO','CA','CO','CT','DE','MD','DC','FL','GA','HI','ID','IL','IN','IA','KS','NE','KY','LA','ME','NH','MA','MI','MN','ND','SD','MS','MT','NV','NJ','NM','NY','NC','OH','OK','OR','PA','RI','SC','TN','TX','UT','VT','VA','WA','WV','WI','WY');
						switch($selectCount) {
							case '1': // median
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total spend for that row\'s spend category for the governments in that column, expressed in thousands ($000s).  Note that the median spend in a given cell only includes the governments from your search results that fall within the column category AND that spend more than $0 in that row\'s spend category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row.  Keep in mind that the cells in every row EXCLUDE the zero-spend governments.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total row governments:</b> Keep in mind that the row excludes zero-spend governments.</li></ul>";
								$fullTableID = escape($tableID . "_budget_state_median_total");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_state_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								break;
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita spend for that row\'s spend category for the governments in that column.  Note that the median per capita spend in a given cell only includes the governments from your search results that fall within the column category AND that spend more than $0 in that row\'s spend category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median per capita spend for all governments included in the row.  Keep in mind that the cells in every row EXCLUDE the zero-spend governments.</li><li><b>Total row governments:</b> Keep in mind that the row excludes zero-spend governments.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = escape($tableID . "_budget_state_median_per_capita");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_state_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								break;
							default:
								break;
						}
						break;
					case '4': // total budget quartiles
						$columnHeader = 'High to Low Spenders (by Quartile)';
						//$columnArray = array('Zero Spend', 'Lowest Spenders','Low-Medium','High-Medium','Highest Spenders');
						$columnArray = array('Highest Spenders<br>(in the category)', 'High-Medium<br>(in the category)','Low-Medium<br>(in the category)','Lowest Spenders<br>(in the category)','Spend Zero<br>(in the category)');
						switch($selectCount) {
							case '1': // median
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total spend for that row\'s spend category for the governments in that column, expressed in thousands ($000s).  Note that the median spend in a given cell includes ALL governments from your search results that fall within the column category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row, EXCLUDING the zero-spend governments.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total row governments:</b> This count excludes the zero-spend governments shown in the row.</li><li><b>Low to high spenders:</b> For each row, governments that spend more than $0 in that spend category are divided into quartiles (from lowest to highest spenders).  Each government\'s quartile is determined by comparing that government\'s spend in the category with all other governments of its type (county, municipality, or township) in the Power Almanac database.  Quartile placement is NOT determined relative to the governments in your search results.</li></ul>";
								$fullTableID = escape($tableID . "_budget_total_quartiles_median");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_total_quartiles_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								$fullZGovtTableID = escape($tableID . "_exp_no_data_vector");
								$readZGovt_sql = "SELECT * FROM $fullZGovtTableID
									";
								break;
							case '2': //  median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita spend for that row\'s spend category for the governments in that column.  Note that the median per capita spend in a given cell includes ALL governments from your search results that fall within the column category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median per capita spend for all governments included in the row, EXCLUDING the zero-spend governments.</li><li><b>Total row governments:</b> This count excludes the zero-spend governments shown in the row.</li><li><b>Low to high spenders:</b> For each row, governments that spend more than $0 in that spend category are divided into quartiles (from lowest to highest per capita spenders).  Each government\'s quartile is determined by comparing that government\'s per capita spend in the category with all other governments of its type (county, municipality, or township) in the Power Almanac database.  Quartile placement is NOT determined relative to the governments in your search results.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = escape($tableID . "_budget_per_capita_quartiles_median");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_per_capita_quartiles_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								$fullZGovtTableID = escape($tableID . "_per_capita_no_data_vector");
								$readZGovt_sql = "SELECT * FROM $fullZGovtTableID
									";
							default:
								break;
						}
						break;
					case '5': // per capita budget quartiles
						$columnHeader = 'High to Low Spenders (by Quartile)';
						$columnArray = array('Highest Spenders<br>(in the category)', 'High-Medium<br>(in the category)','Low-Medium<br>(in the category)','Lowest Spenders<br>(in the category)','Spend Zero<br>(in the category)');
						switch($selectCount) {
							case '2': // median per capita
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have and spend data or 2007 population data (needed to calculate median per capita spend).</li><li><b>Median per capita spend:</b> Each cell displays the median per capita spend for that row\'s spend category for the governments in that column.  Note that the median per capita spend in a given cell includes ALL governments from your search results that fall within the column category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median per capita spend for all governments included in the row, EXCLUDING the zero-spend governments.</li><li><b>Total row governments:</b> This count excludes the zero-spend governments shown in the row.</li><li><b>Low to high spenders:</b> For each row, governments that spend more than $0 in that spend category are divided into quartiles (from lowest to highest per capita spenders).  Each government\'s quartile is determined by comparing that government\'s per capita spend in the category with all other governments of its type (county, municipality, or township) in the Power Almanac database.  Quartile placement is NOT determined relative to the governments in your search results.</li><li><b>Calculation of per capita spend:</b> For each government, per capita spend is calculated by taking that government\'s total spend in a category and dividing by the total population governed.  Note that both the spending and population data are from 2007.</li></ul>";
								$fullTableID = escape($tableID . "_budget_per_capita_quartiles_median");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_per_capita_quartiles_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								$fullZGovtTableID = escape($tableID . "_per_capita_no_data_vector");
								$readZGovt_sql = "SELECT * FROM $fullZGovtTableID
									";
								break;
							case '1': // median
								$helpText = "<ul><li><b>Governments included:</b> All of the governments from your search results, except for those which we do not have any spend data (needed to calculate median total spend).</li><li><b>Median total spend:</b> Each cell displays the median total spend for that row\'s spend category for the governments in that column, expressed in thousands ($000s).  Note that the median spend in a given cell includes ALL governments from your search results that fall within the column category.  Depending on your search criteria, this could mean that this median reflects spending by a mix of governments of different types, populations, locations, etc.</li><li><b>Total row median:</b> This is the median total spend for all governments included in the row, EXCLUDING the zero-spend governments.  Note that the dollar amounts are expressed in thousands ($000s).</li><li><b>Total row governments:</b> This count excludes the zero-spend governments shown in the row.</li><li><b>Low to high spenders:</b> For each row, governments that spend more than $0 in that spend category are divided into quartiles (from lowest to highest spenders).  Each government\'s quartile is determined by comparing that government\'s spend in the category with all other governments of its type (county, municipality, or township) in the Power Almanac database.  Quartile placement is NOT determined relative to the governments in your search results.</li></ul>";
								$fullTableID = escape($tableID . "_budget_total_quartiles_median");
								$read_sql = "SELECT * FROM $fullTableID
									";
								$fullGovtTableID = escape($tableID . "_budget_total_quartiles_median_govs");
								$readGovt_sql = "SELECT * FROM $fullGovtTableID
									";
								$fullZGovtTableID = escape($tableID . "_exp_no_data_vector");
								$readZGovt_sql = "SELECT * FROM $fullZGovtTableID
									";
							default:
								break;
						}
						break;
					default:
						break;
				}
				break;
			default:		
				break;
		}

		break;
	default:
		break;
}


// DATABASE TABLES TO USE
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
    flashError('Failed to analyze data.  Please try again or contact your account manager.', PowerAlmanac\PDb::error());
}

$resultGovt_read = @PowerAlmanac\PDb::query($readGovt_sql);
if (!$resultGovt_read) {
    flashError('Failed to analyze data..  Please try again or contact your account manager.', PowerAlmanac\PDb::error());
}

// INIT
$numRows = count($rowArray);
$numColumns = count($columnArray);


// MISC FUNCTIONS
function StateLookUp($stateAbbr) { 
	// if state code is used
	$state_code_used = 0;
	if ($state_code_used == 1) {
		// static lookup
		/*
		AL	ALABAMA	01
		AK	ALASKA	02
		AZ	ARIZONA	03
		AR	ARKANSAS	04
		CA	CALIFORNIA	05
		CO	COLORADO	06
		CT	CONNECTICUT	07
		DE	DELAWARE	08
		DC	DIS. OF COLUMBIA	09
		FL	FLORIDA	10
		GA	GEORGIA	11
		HI	HAWAII	12
		ID	IDAHO	13
		IL	ILLINOIS	14
		IN	INDIANA	15
		IA	IOWA	16
		KS	KANSAS	17
		KY	KENTUCKY	18
		LA	LOUISIANA	19
		ME	MAINE	20
		MD	MARYLAND	21
		MA	MASSACHUSETTS	22
		MI	MICHIGAN	23
		MN	MINNESOTA	24
		MS	MISSISSIPPI	25
		MO	MISSOURI	26
		MT	MONTANA	27
		NE	NEBRASKA	28
		NV	NEVADA	29
		NH	NEW HAMPSHIRE	30
		NJ	NEW JERSEY	31
		NM	NEW MEXICO	32
		NY	NEW YORK	33
		NC	NORTH CAROLINA	34
		ND	NORTH DAKOTA	35
		OH	OHIO	36
		OK	OKLAHOMA	37
		OR	OREGON	38
		PA	PENNSYLVANIA	39
		RI	RHODE ISLAND	40
		SC	SOUTH CAROLINA	41
		SD	SOUTH DAKOTA	42
		TN	TENNESSEE	43
		TX	TEXAS	44
		UT	UTAH	45
		VT	VERMONT	46
		VA	VIRGINIA	47
		WA	WASHINGTON	48
		WV	WEST VIRGINIA	49
		WI	WISCONSIN	50
		WY	WYOMING	51
		*/
		$stateArray = array();
		$stateArray['01'] = "AL,ALABAMA";
		$stateArray['02'] = "AK,ALASKA";
		$stateArray['03'] = "AZ,ARIZONA";
		$stateArray['04'] = "AR,ARKANSAS";
		$stateArray['05'] = "CA,CALIFORNIA";
		$stateArray['06'] = "CO,COLORADO";
		$stateArray['07'] = "CT,CONNECTICUT";
		$stateArray['08'] = "DE,DELAWARE";
		$stateArray['09'] = "DC,DIS. OF COLUMBIA";
		$stateArray['10'] = "FL,FLORIDA";
		$stateArray['11'] = "GA,GEORGIA";
		$stateArray['12'] = "HI,HAWAII";
		$stateArray['13'] = "ID,IDAHO";
		$stateArray['14'] = "IL,ILLINOIS";
		$stateArray['15'] = "IN,INDIANA";
		$stateArray['16'] = "IA,IOWA";
		$stateArray['17'] = "KS,KANSAS";
		$stateArray['18'] = "KY,KENTUCKY";
		$stateArray['19'] = "LA,LOUISIANA";
		$stateArray['20'] = "ME,MAINE";
		$stateArray['21'] = "MD,MARYLAND";
		$stateArray['22'] = "MA,MASSACHUSETTS";
		$stateArray['23'] = "MI,MICHIGAN";
		$stateArray['24'] = "MN,MINNESOTA";
		$stateArray['25'] = "MS,MISSISSIPPI";
		$stateArray['26'] = "MO,MISSOURI";
		$stateArray['27'] = "MT,MONTANA";
		$stateArray['28'] = "NE,NEBRASKA";
		$stateArray['29'] = "NV,NEVADA";
		$stateArray['30'] = "NH,NEW HAMPSHIRE";
		$stateArray['31'] = "NJ,NEW JERSEY";
		$stateArray['32'] = "NM,NEW MEXICO";
		$stateArray['33'] = "NY,NEW YORK";
		$stateArray['34'] = "NC,NORTH CAROLINA";
		$stateArray['35'] = "ND,NORTH DAKOTA";
		$stateArray['36'] = "OH,OHIO";
		$stateArray['37'] = "OK,OKLAHOMA";
		$stateArray['38'] = "OR,OREGON";
		$stateArray['39'] = "PA,PENNSYLVANIA";
		$stateArray['40'] = "RI,RHODE ISLAND";
		$stateArray['41'] = "SC,SOUTH CAROLINA";
		$stateArray['42'] = "SD,SOUTH DAKOTA";
		$stateArray['43'] = "TN,TENNESSEE";
		$stateArray['44'] = "TX,TEXAS";
		$stateArray['45'] = "UT,UTAH";
		$stateArray['46'] = "VT,VERMONT";
		$stateArray['47'] = "VA,VIRGINIA";
		$stateArray['48'] = "WA,WASHINGTON";
		$stateArray['49'] = "WV,WEST VIRGINIA";
		$stateArray['50'] = "WI,WISCONSIN";
		$stateArray['51'] = "WY,WYOMING";
		$stateCode = $stateAbbr; // actually state_code
		list($stateAbbr,$stateDesc) = explode(",",$stateArray["$stateCode"]);
		$stateName = ucwords(strtolower($stateDesc));
	} else {
		$read_sql = sprintf("SELECT * FROM codes_table
			WHERE code_code = '%s' AND table_name = 'STATES'
			LIMIT 1
		", escape($stateAbbr));
		$result_read = @PowerAlmanac\PDb::query($read_sql);
		if (!$result_read) {
			die('Error reading from codes_table database:' . PowerAlmanac\PDb::error());
		}
		$onerow = PowerAlmanac\PDb::fetch_array($result_read);
		$stateDesc = $onerow['code_desc'];
		$stateName = ucwords(strtolower($stateDesc));
	}
	return $stateName;
}

function reverseLookUp($stateName) { 
	$stateDBName = strtoupper($stateName);
	$read_sql = sprintf("SELECT * FROM codes_table
		WHERE code_desc = '%s' AND table_name = 'STATES'
		LIMIT 1
	", escape($stateDBName));
	$result_read = @PowerAlmanac\PDb::query($read_sql);
	if (!$result_read) {
		die('Error reading from codes_table database:' . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_read);
	$stateAbbr = $onerow['code_code'];
	return $stateAbbr;
}

function GovtTypesLookUp($govtTypeAbbr) { 
	switch($govtTypeAbbr) {
		case 'COU':
			$govtTypeName = 'Counties';
			break;
		case 'MUN':
			$govtTypeName = 'Municipalities';
			break;
		case 'TOW':
			$govtTypeName = 'Townships';
			break;
		default:
			$govtTypeName = $govtTypeAbbr;
			break;
	}
	return $govtTypeName;
}

function reverseGovtTypesLookUp($govtTypeAbbr) { 
	switch($govtTypeAbbr) {
		case 'Counties':
			$govtTypeName = 'COU';
			break;
		case 'Municipalities':
			$govtTypeName = 'MUN';
			break;
		case 'Townships':
			$govtTypeName = 'TOW';
			break;
		default:
			$govtTypeName = $govtTypeAbbr;
			break;
	}
	return $govtTypeName;
}

function PopulationLookUp($popAbbr) { 
	switch($popAbbr) {
		case 'P1':
			$popName = '999 or less';
			break;
		case 'P2':
			$popName = '1,000 to 2,499';
			break;
		case 'P3':
			$popName = '2,500 to 4,999';
			break;
		case 'P4':
			$popName = '5,000 to 9,999';
			break;
		case 'P5':
			$popName = '10,000 to 24,999';
			break;
		case 'P6':
			$popName = '25,000 to 49,999';
			break;
		case 'P7':
			$popName = '50,000 to 99,999';
			break;
		case 'P8':
			$popName = '100,000 to 499,999';
			break;
		case 'P9':
			$popName = '500,000 or more';
			break;
		case 'P0':
			$popName = 'Unknown';
			break;
		default:
			$popName = $popAbbr;
			break;
	}
	return $popName;
}

function reversePopulationLookUp($popAbbr) { 
	switch($popAbbr) {
		case '999 or less':
			$popName = 'P1';
			break;
		case '1,000 to 2,499':
			$popName = 'P2';
			break;
		case '2,500 to 4,999':
			$popName = 'P3';
			break;
		case '5,000 to 9,999':
			$popName = 'P4';
			break;
		case '10,000 to 24,999':
			$popName = 'P5';
			break;
		case '25,000 to 49,999':
			$popName = 'P6';
			break;
		case '50,000 to 99,999':
			$popName = 'P7';
			break;
		case '100,000 to 499,999':
			$popName = 'P8';
			break;
		case '500,000 or more':
			$popName = 'P9';
			break;
		case 'Unknown':
			$popName = 'P0';
			break;
		default:
			$popName = 0;
			break;
	}
	return $popName;
}

// budget categories
$budgetcategoryArray = array('TOT' => '<b><font color="#CC6600">TOTAL EXPENDITURE</font></b>','CAP' => '<b>CAPITAL</b>','OPE' => '<b>OPERATING</b>','SAL' => '<b>SALARIES &amp; WAGES</b>','SUP' => '<b>SUPPLIERS</b>','PUB1' => '<b>PUBLIC SAFETY</b>','COR' => 'Corrections','FIR1' => 'Fire Protection','JUC' => 'Judicial and Legal','POL' => 'Police','PUB2' => '<b>PUBLIC WELFARE</b>','ASS' => 'Assistance and Subsidies','WEL' => 'Welfare Program Administration','HEA' => '<b>HEALTH</b>','HES' => 'Health Services','HOS' => 'Hospitals','UTL' => '<b>UTILITIES</b>','ELE' => 'Electricity','GAS' => 'Gas','SWE' => 'Sewerage','SOL' => 'Solid Waste Management','WAT' => 'Water','TRA' => '<b>TRANSPORTATION</b>','AIR' => 'Airports','PAR1' => 'Parking','POR' => 'Ports and Waterways','PUB3' => 'Public Transit','ROA' => 'Roads and Highways','LEI' => '<b>LEISURE</b>','LIB' => 'Libraries','PAR2' => 'Parks and Recreation','FIR2' => '<b>FINANCE</b>','FIN' => 'Financial Administration','INT' => 'Interest on Debt','MIS' => '<b>MISCELLANEOUS</b>','EDU' => 'Education','GEN' => 'General Administration','HOU' => 'Housing and Community Development','LIQ' => 'Liquor Stores','NAT' => 'Natural Resources','OTH' => 'Other Miscellaneous');

// handle search results visibility
if (isset($_SESSION['analyze_showresults'])) {
	if ($_SESSION['analyze_showresults'] == '1') {
		// show
		$searchResultsDisplay = 'block';
		$searchResultsButton = 'images/expand-btn.png';
	} else {
		// hide
		$searchResultsDisplay = 'none';
		$searchResultsButton = 'images/collapse-btn.png';
	}
} else {
	// show
	$searchResultsDisplay = 'block';
	$searchResultsButton = 'images/expand-btn.png';
}

// handle govt visibility
if (isset($_SESSION['analyze_showgovts'])) {
	if ($_SESSION['analyze_showgovts'] == '1') {
		// show
		$govtResultsDisplay = 'inline';
		$govtResultsButton = 'images/hide-govt.png';
		$govtResultsURL = 'PA-hidegovts.php';
	} else {
		// hide
		$govtResultsDisplay = 'none';
		$govtResultsButton = 'images/show-govt.png';
		$govtResultsURL = 'PA-showgovts.php';
	}
} else {
	// hide
	$govtResultsDisplay = 'none';
	$govtResultsButton = 'images/show-govt.png';
	$govtResultsURL = 'PA-showgovts.php';
}

?>
<html>
<head>
<title><?php echo($title); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="description" content="Online directory of city and county officials for all U.S.  Easy to use, comprehensive, and up-to-date.  Analyze government markets, spending, demographics, and more." />
<meta name="Keywords" content="Cities,Counties,demographics,government spending,Government data" />

<link rel="canonical" href="/PA-Analyze.php">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

<link rel="Stylesheet" href="ui.selectmenu.css" type="text/css" /> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script> 
<link rel="Stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/ui-darkness/jquery-ui.css" type="text/css" />
<!--<link rel="Stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/redmond/jquery-ui.css" type="text/css" />-->
<script type="text/javascript" src="ui.selectmenu.js"></script> 
    
    
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
	Custom.checkAll();
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
	Custom.clear();
}
//  End -->
</script>
<script type="text/javascript">
<!--
function resettoggle() {
	var e = document.getElementById('foo');
	e.style.display = 'block';
}

function session_hide_results() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET","PA-hideresults.php",false);
	xmlhttp.send();
}

function session_show_results() {
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET","PA-showresults.php",false);
	xmlhttp.send();
}

function toggle_visibility(id) {
	var e = document.getElementById(id);
	var b = document.getElementById('expandbtn'); 
	if(e.style.display == 'none') {
		//alert('show');
		e.style.display = 'block';
		b.src = 'images/expand-btn.png';
		session_show_results();
	}
	else {
		//alert('hide');
		e.style.display = 'none';
		b.src = 'images/collapse-btn.png';
		session_hide_results();
	}
}

//-->
</script>

<script type="text/javascript">
<!--
function getElementByClass(cName,tag,p) {
	if (!cName) { return false; }
	//tag = tag �� "*" ;
	//p = p �� document;
	var i;
	var cRegEx=new RegExp("(^�\\s)"+cName+"(\\s�$)");
	var ra=[];
	var a=p.getElementsByTagName(tag);
	for(i=0;i<a.length;i++){
		if(cRegEx.test(a[i].className)){
			ra.push(a[i]);
		}
	}
	return ra;
}
//ra = getElementByClass("foo","div",document.getElementById("bar")); // only div's with the class foo in the element bar
//ra = getElementByClass("foo"); // all elements with the class bar
//alert(ra);

//-->
</script>

<script type="text/javascript">
function tallySubmitForm()
{
	var row = document.forms.analyze.row.value;
	var column = document.forms.analyze.column.value;
	// switch in JS does not work
	// col = roles, pop*, govt types
	if (row == '1') { // states
		document.analyze.submit();
	} else {
		if (row == '2') { // population
			if (column == '2') { // population
				alert('Please change either your ROW or COLUMN setting (so that they are not the same as each other).');
			} else {
				document.analyze.submit();
			}
		} else {
			if (row == '3') { // roles - NOT USED
				alert('INVALID selection combination. Please try again.');
			} else {
				if (row == '4') { // govt types
					if (column == '1') {
						document.analyze.submit();
					} else {
						if (column == '2') { // population
							document.analyze.submit();
							//alert('INVALID selection combination. Please try again.'); // make this VALID
						} else {
							alert('Please change either your ROW or COLUMN setting (so that they are not the same as each other).');
						}
					}
				}
			}
		}
	}
}
</script>

<script type="text/javascript">
function compareSubmitForm()
{
	var row = document.forms.analyze.row.value;
	var column = document.forms.analyze.column.value;
	var count = document.forms.analyze.count.value;
	// switch in JS does not work
	if (row == '3') { // budget categories
		if (column == '4') { // quartiles
			document.analyze.submit();
		} else {
			if (column == '5') { // NOT USED
				if (count == '1') {
					alert('INVALID selection combination. Please try again.');
				} else {
					document.analyze.submit();
				}
			} else {
				document.analyze.submit();
			}
		}
	} else {
		if (row == '2') { // population
			if (column == '1') { // govt types
				document.analyze.submit();
			} else {
				if (column == '2') { // population
					alert('Please change either your ROW or COLUMN setting (so that they are not the same as each other).');
				} else {
					alert('High/Low Spenders for COLUMNS cannot be combined with either STATES or POPULATION RANGES for ROWS.');
				}
			}
		} else {
			if (row == '1') { // states
				if (column == '1') { // govt types
					document.analyze.submit();
				} else {
					if (column == '2') { // population
						document.analyze.submit();
					} else {
						alert('High/Low Spenders for COLUMNS cannot be combined with either STATES or POPULATION RANGES for ROWS.');
					}
				}
			} else {
				// invalid
				alert('INVALID selection combination. Please try again.');
			}
		}
	}
}
</script>

<script type="text/javascript"> 

	$(function(){
		
		$('select#speedA').selectmenu();
		$('select#speedAa').selectmenu({maxHeight: 150});
		$('select#speedB').selectmenu({
			width: 300,
			format: addressFormatting
		});
		
		$('select#countT').selectmenu({width: 175,style:'dropdown'});
		$('select#count').selectmenu({width: 225,style:'dropdown'});
		$('select#row').selectmenu({width: 175,style:'dropdown'});
		$('select#column').selectmenu({width: 225,style:'dropdown'});
		
		
		$('select#speedD').selectmenu({
			style:'dropdown', 
			menuWidth: 400,
			format: addressFormatting
		});
		$('select#files, select#filesC').selectmenu({
			icons: [
				{find: '.script', icon: 'ui-icon-script'},
				{find: '.image', icon: 'ui-icon-image'}
			]
		});
		$('select#filesB').selectmenu({
			icons: [
				{find: '.video'},
				{find: '.podcast'},
				{find: '.rss'}
			]
		});
		
		$(function() {
		$( "button, a", ".buttonPA" ).button();
		$( "a", ".buttonPA" ).click(function() { return true; });
		});
		
		//a custom format option callback
		var addressFormatting = function(text){
			var newText = text;
			//array of find replaces
			var findreps = [
				{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
				{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
				{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
				{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
				{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
			];
			
			for(var i in findreps){
				newText = newText.replace(findreps[i].find, findreps[i].rep);
			}
			return newText;
		}
	});
	
</script> 

<script>
	$(function() {
		<?php
		require_once('Browser.php');
		$browser = new Browser();
		$detectedBrowser = $browser->getBrowser();
		if( $detectedBrowser == 'Internet Explorer') {
		?>
			$( "#dialog" ).dialog({ width: 460, height: 300, modal: true });
		<?php
		} else {
		?>
			$( "#dialog" ).dialog({ width: 460, height: 200, modal: true });
		<?php
		}
		?>
	});
</script>

<style type="text/css"> 
	/*body {font-size: 62.5%; font-family:"Verdana",sans-serif; margin: 70px 10px;}*/
	fieldset { border:0;  margin-bottom: 40px;}	
	label,select,.ui-select-menu { float: left; margin-right: 10px; }
	select { width: 200px; font-family: Arial, Helvetica, sans-serif; font-size: 14px;font-weight: bold;}
</style> 
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<?php
	$affiliateID = $_SESSION['affiliateID'];
    if ($affiliateID == '2') {
        // GovEvents
        echo('<td align="right" valign="middle" colspan="2" background="images/topheader-govevents.jpg" height="65">');
    } else {
		// default
        echo('<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">');
	}
    ?>
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>

<tr>
<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'analyze';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstrip2.png" valign="top">
            <br>
            <?php
			if (($_SESSION['logged_in'] == '1') && ($User_Subscription >= 2)) {
				// for power users only
				$aboutExtras = '';
			?>
            <table cellpadding="5" cellspacing="0" border="0" align="center" width="95%" bgcolor="#afc8e2">
            <tr>
            	<td width="25" bgcolor="#94adc9">
                <a href="#" onClick="toggle_visibility('foo');"><img id="expandbtn" src="<?php echo($searchResultsButton); ?>" width="17" height="8" hspace="0" vspace="0" border="0" align="absmiddle"></a>
                </td>
            	<td width="140" height="40">
                <a href="PA-AnalyzeModifySearch.php"><img src="images/modifysearch-btn.png" width="125" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a>
                </td>
                <td align="left">
                <p class="matchedHdr">
                YOUR SEARCH RESULTS: <font class="analyzeMatchedLarge"><?php echo($Num_Matched_OfficialsFORMATTED); ?></font>
                </p>
                </td>
                <td>&nbsp;
                
                </td>
                <td align="right">
                <a href="PA-SaveSearch.php"><img src="images/saverecords-btn.png" width="126" height="29" hspace="5" vspace="0" border="0" align="absmiddle"></a><a href="PA-Download.php"><img src="images/downloadrecords-btn-med.png" width="152" height="29" hspace="5" vspace="0" border="0" align="absmiddle"></a>
                </td>
            </tr>
            </table>
            <div id="foo" style="display:<?php echo($searchResultsDisplay); ?>">
            <table cellpadding="5" cellspacing="0" border="0" align="center" width="95%" bgcolor="#afc8e2">
            <tr bgcolor="#94adc9">
            	<td width="25" bgcolor="#94adc9">
                </td>
            	<td width="140" height="40">
                </td>
                <td align="left" width="300">
                <font class="analyzeMatchedLarge">Local Government Officials Records </font><br>
                <font class="analyzeMatchedMedium"><b><?php echo($Percentage_Matched_Emails); ?>%</b> have email addresses <br />
                <b><?php echo($Percentage_Matched_Phone_Numbers); ?>%</b> have phone numbers <br />
                <b><?php echo($Percentage_Matched_Mailing_Addresses); ?>%</b> have mailing addresses 
                </font>
                <p class="analyzeMatchedMedium">
                <b>Unique Local Governments </b>
                </p>
                </td>
                <td align="right" width="50">
                <font class="analyzeMatchedLarge"><?php echo($Num_Matched_OfficialsFORMATTED); ?></font><br>
                <font class="analyzeMatchedMedium"><b><?php echo($Num_Matched_EmailsFORMATTED); ?></b><br />
                <b><?php echo($Phone_NumbersFORMATTED); ?></b><br />
                <b><?php echo($Mailing_AddressesFORMATTED); ?></b>
                </font>
                <p class="analyzeMatchedMedium">
                <b><?php echo($Num_Matched_GovernmentsFORMATTED); ?></b>
                </p>
                </td>
                <td>
                </td>
            </tr>
            </table>
            </div>
            <br>
            <?php
			} else {
				// for visitors and non-power users only
				$aboutExtras = '(for POWER subscribers)';
			?>
            <div id="dialog" style="background-color:#FFFFFF;" title="Non-POWER Subscriber Alert">
                <p>Note that the ANALYZE feature is only available for <b>POWER</b> subscribers.</p>
                <p>Unlike for <b>POWER</b> subscribers, the <b>ANALYZE</b> table you see here is <b>NOT</b> based on your search results.   Instead, it includes data from the entire Power Almanac database.</p>
            </div>

            <table cellpadding="5" cellspacing="5" border="0" align="center" width="95%" bgcolor="#FFFFFF">
            <tr>
            	<td>
                <p>
                Welcome to the Analyze Tab!
                <br>
                <font style="font-size:16px; font-weight:bold;">
                <br>
                What can you do here?
                </font>
                <br>
                Analyze the complete set of records in the Power Almanac by using the data tables below. Go ahead and check it out.
                <br>
                <font style="font-size:16px; font-weight:bold;">
                <br>
                What can POWER subscribers do here?
                </font>
                <br>
                They can use the data tables below to analyze their search results. Each time a Power subscriber does a search, a custom set of data tables is created below (using only the records and governments from their search). This allows them to fine-tune their searches or size up and assess very specific target markets.
                </p>
                <p>
                <a href="PA-Pricing.php">Show me the POWER plans</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="PA-Pricing.php">Subscribe to a POWER plan</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="search-government.php">Take me back to my SEARCH RESULTS</a>
                </p>
                </td>
            </tr>
            <tr>
            	<td>
                
                </td>
            </tr>
            </table>
            <br>
            <?php
            }	
			?>
            <table cellpadding="5" cellspacing="0" border="0" align="center" width="95%" bgcolor="#afc8e2">
            <tr>
            	<td width="25" bgcolor="#94adc9">
                </td>
            	<td colspan="4" align="center">
                <div style="position: relative;cursor: default;width: 1050px;overflow-x:scroll;overflow-y:hidden;">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" align="center">
                <tr valign="top">
                	<td colspan="4" align="right">
                    <a href="PA-Analyze.php?<?php echo("skipBuild=1&type=1&count=1&column=1&row=2"); ?>"><img src="<?php echo($selectedTallyImage); ?>" hspace="0" vspace="0" border="0" align="left"></a><a <?php echo($shadowbox); ?> href="<?php echo($compareURL); ?>"><img src="<?php echo($selectedCompareImage); ?>" hspace="0" vspace="0" border="0" align="left"></a> <img src="images/info-btn.png" width="14" height="15" hspace="5" vspace="0" border="0" align="absmiddle"> <a rel="shadowbox;height=275;width=675" href="PA-AnalyzeInstr.php" title="">How to customize this table?</a>
                    <?php 
					if ($_SESSION['logged_in'] != '1') {
						// disable save and export
						echo('<a href="#" onMouseOver="showhint(\'You have to be a Power User subscriber to enable this feature.\', this, event, \'200px\')"><img src="images/savetable-btn.png" alt="Save Table" width="112" height="29" hspace="5" vspace="0" border="0" align="absmiddle" title="Save Table"></a> <a href="#" onMouseOver="showhint(\'You have to be a Power User subscriber to enable this feature.\', this, event, \'200px\')"><img src="images/exporttable-btn.png" alt="Export CSV" width="122" height="29" hspace="0" vspace="0" border="0" align="absmiddle" title="Export CSV"></a>');
					} else {
						if ($User_Subscription < 2) {
							// disable save and export
							echo('<a href="#" onMouseOver="showhint(\'You have to be a Power User subscriber to enable this feature.\', this, event, \'200px\')"><img src="images/savetable-btn.png" alt="Save Table" width="112" height="29" hspace="5" vspace="0" border="0" align="absmiddle" title="Save Table"></a> <a href="#" onMouseOver="showhint(\'You have to be a Power User subscriber to enable this feature.\', this, event, \'200px\')"><img src="images/exporttable-btn.png" alt="Export CSV" width="122" height="29" hspace="0" vspace="0" border="0" align="absmiddle" title="Export CSV"></a>');
						} else {
							// OK
							// disable save table for Compare Spending for now
							if ($selectType == '2') {
								echo("<a href=\"#\" onMouseOver=\"showhint('Feature coming soon!', this, event, '200px')\"><img src=\"images/savetable-btn.png\" alt=\"Save Table\" width=\"112\" height=\"29\" hspace=\"5\" vspace=\"0\" border=\"0\" align=\"absmiddle\" title=\"Save Table\"></a> <a href=\"PA-ExportTable.php\" target='_blank'><img src=\"images/exporttable-btn.png\" alt=\"Export CSV\" width=\"122\" height=\"29\" hspace=\"0\" vspace=\"0\" border=\"0\" align=\"absmiddle\" title=\"Export CSV\"></a>&nbsp;");
							} else {
								echo("<a href=\"PA-SaveTable.php?type=$selectType&count=$selectCount&row=$selectRow&column=$selectColumn\"><img src=\"images/savetable-btn.png\" alt=\"Save Table\" width=\"112\" height=\"29\" hspace=\"5\" vspace=\"0\" border=\"0\" align=\"absmiddle\" title=\"Save Table\"></a> <a href=\"PA-ExportTable.php\" target='_blank'><img src=\"images/exporttable-btn.png\" alt=\"Export CSV\" width=\"122\" height=\"29\" hspace=\"0\" vspace=\"0\" border=\"0\" align=\"absmiddle\" title=\"Export CSV\"></a>&nbsp;");
							}
						}
					}
					?>
                    </td>
                </tr>
                <tr valign="top" bgcolor="#FFFFFF">
                    <td colspan="4" align="right" valign="middle">
                    <img src="<?php echo($keyImage); ?>" height="15" hspace="0" vspace="10" border="0" align="absmiddle"> <a href="<?php echo("$govtResultsURL?skipBuild=1&type=$selectType&count=$selectCount&row=$selectRow&column=$selectColumn"); ?>" ><img id="govtbtn" src="<?php echo("$govtResultsButton"); ?>" width="45" height="20" hspace="0" vspace="10" border="0" align="absmiddle"></a> <img src="images/key-govt.png" width="170" height="15" hspace="0" vspace="10" border="0" align="absmiddle"><img src="images/space.gif" width="100" height="1" hspace="0" vspace="0" border="0"><a href="#" class="hintanchor" onMouseOver="showhint('<?php echo($helpText);?>', this, event, '500px')"><img src="images/help-blue.png" width="12" height="12" hspace="0" vspace="10" border="0" align="absmiddle" title=""></a> About this table <?php echo($aboutExtras); ?>&nbsp;&nbsp;
                    </td>
                </tr>
                <?php
				switch($selectType) {
					case '2': // COMPARE SPENDING
						$finalCSV = '';
				?>
                <tr>
                	<td colspan="4">
                    <form name="analyze" method="get">
                    <input type="hidden" name="skipBuild" value="1">
                    <input type="hidden" name="type" value="<?php echo("$selectType"); ?>">
                    <table cellpadding="3" cellspacing="1" width="100%" border="0" bgcolor="#FFFFFF">
						<?php
                        if ($govtResultsDisplay == 'inline') {
                            $skipColumns = ($numColumns + 1)*2;
                            $columnHdrSize = 2;
                        } else {
                            $skipColumns = ($numColumns + 1);
                            $columnHdrSize = 1;
                        }
                        ?>
                    	<tr>
                        	<td></td>
                            <td colspan="2" align="left">
                            <select name="count" onChange="javascript:compareSubmitForm();" id="count">
                                <option value="1" <?php echo($reqCountSelect['1']); ?> class="analyzeColumnHdr">DATA: Median Total Spend (<font style="font-size:16px; font-weight:bold;">$000s</font>)</option>
                                <option value="2" <?php echo($reqCountSelect['2']); ?> class="analyzeColumnHdr">DATA: Median Per Capita Spend</option>
                            </select>
                            </td>
                            <td align="left" colspan="<?php $hdrSkipColumns = $skipColumns - 1; echo($hdrSkipColumns); ?>">
                            <?php
							if ($selectCount == '1') { // median total spending
								echo("<img src='images/space.gif' height='1' width='175' border='0'><font color='#CC6600' style='font-size: 12px;'>Dollars in thousands ($000s)</font>&nbsp;");
							}
							?>
                            </td>
                        </tr>
                    	<tr>
                        	<td></td>
                            <td align="left" width="200">
                            <!--
                            <select name="count" onChange="javascript:compareSubmitForm();" id="count">
                                <option value="1" <?php echo($reqCountSelect['1']); ?> class="analyzeColumnHdr">DATA: Median Total Spend (<b>000s</b>)</option>
                                <option value="2" <?php echo($reqCountSelect['2']); ?> class="analyzeColumnHdr">DATA: Median Per Capita Spend</option>
                            </select>
                            -->
                            </td>
                            <td colspan="<?php echo($skipColumns); ?>" align="center" bgcolor="#9a9a9a" height="25">
                            <select name="column" onChange="javascript:compareSubmitForm();" id="column">
                                <option value="4" <?php echo($reqColumnSelect['4']); ?> class="analyzeColumnHdr">COLUMN: High/Low Spenders</option>
                                <option value="1" <?php echo($reqColumnSelect['1']); ?> class="analyzeColumnHdr">COLUMN: Government Types</option>
                                <option value="2" <?php echo($reqColumnSelect['2']); ?> class="analyzeColumnHdr">COLUMN: Population Ranges</option>
                                <!--
                                <option value="3" <?php echo($reqColumnSelect['3']); ?> class="analyzeColumnHdr">COLUMN: States</option>
                                <option value="5" <?php echo($reqColumnSelect['5']); ?> class="analyzeColumnHdr">COLUMN: Per Capita Budget Quartiles</option>
                                -->
                            </select>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                        	<td width="5">&nbsp;
                            
                            </td>
                        	<td bgcolor="#9a9a9a" width='200'>
                            <select name="row" onChange="javascript:compareSubmitForm();" id="row">
                                <option value="3" <?php echo($reqRowSelect['3']); ?> class="analyzeColumnHdr">ROW: Spend Categories</option>
                                <option value="1" <?php echo($reqRowSelect['1']); ?> class="analyzeColumnHdr">ROW: States</option>
                                <option value="2" <?php echo($reqRowSelect['2']); ?> class="analyzeColumnHdr">ROW: Population</option>
                            </select>
                            </td>
                            <?php
							// for CSV
							switch($selectRow) {
								case '1':
									$finalCSV = $finalCSV . "States,";
									break;
								case '2':
									$finalCSV = $finalCSV . "Population,";
									break;
								case '3':
									$finalCSV = $finalCSV . "Budget Category,";
									break;	
								default:
									break;
							}
							// headers
							foreach ($columnArray as $columnItem) {
								echo("<td bgcolor='#d3d3d3' colspan='$columnHdrSize' align='right' ><font class='analyzeHdr'>$columnItem</font></td>");
								// for CSV
								$finalCSV = $finalCSV . '"' . $columnItem . '","' . $columnItem . '" (G),';
							}
							?>
                            <td bgcolor="#cfd7e0" align="right" colspan="<?php echo($columnHdrSize); ?>">
                            <font class='analyzeHdr'><b>Total Row Median&nbsp;</b></font>
                            <?php
							if (($selectRow == '3') && ($selectColumn == '4')) {
								// add extra text
								echo("<br>(excluding zero spenders)");	
							}
							// for CSV
							$finalCSV = $finalCSV . "Total Row Median, Total Row Median (G)";
							$finalCSV = $finalCSV . "\n";
							?>
                            </td>
                            <td width="5">&nbsp;
                            
                            </td>
                        </tr>
                        <?php
						// up to 51 states + total + header
						$columnCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
						$columnGovtCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
						$columnCtrIndex = 1;
						// up to 50 categories
						$rowCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
						$rowGovtCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
						$rowCtrIndex = 0;
						// output HTML
						$outputHTML = '';
						// get counts
						$numberOfColumns = 0;
						$stateTableArray = array();
						
						$numberOfRows = PowerAlmanac\PDb::num_rows($result_read);

						while ($row = PowerAlmanac\PDb::fetch_array($result_read))
						{
							$numItems = count($row)/2; // because of associative array + numeric index
							if ($numberOfColumns == 0) {
								$numberOfColumns = $numItems;
							}
							switch($selectRow) {
								case '1':
									// need to lookup states
									$stateAbbr = $row[0];
									$stateName = StateLookUp($stateAbbr);
									$formattedRowCell = $stateName;
									array_push($stateTableArray,"$formattedRowCell");
									break;
								case '2':
									// need to lookup population
									$popAbbr = $row[0];
									$popName = PopulationLookUp($popAbbr);
									$formattedRowCell = $popName;
									break;
								case '3':
									// need to lookup budget category
									/*
									$budgetCategoryAbbr = $row[0];
									$budgetCategoryName = BudgetCategoryLookUp($budgetCategoryAbbr);
									$formattedRowCell = $budgetCategoryName;
									*/
									$budgetCategoryAbbr = $row[0];
									$formattedRowCell = $budgetcategoryArray[$budgetCategoryAbbr];
									//$formattedRowCell = $row[0];
									break;
								default:
									$formattedRowCell = $row[0];
									break;
							}
							// fill in 
							$tableArray[$rowCtrIndex]['0'] = $formattedRowCell;
							
							// govt
							$rowGovt = PowerAlmanac\PDb::fetch_array($resultGovt_read);
							
							// ROW NAME
							// start at index 1 since index 0 is row name
							// row totals calculated here unless it is budget categories
							
							// insert 5th percentile for Tables M and N Row = 3 (Budget); Column = 4 (Quartiles) Count = 1 and 2
							if (($selectRow == '3') && ($selectColumn == '4')) {
								/*
								$tableArray[$rowCtrIndex]['1'] = 0; // zero median for spending
								// get government counts
								$readZGovtFinal_sql = $readZGovt_sql . " WHERE c_budget = '$budgetCategoryAbbr' LIMIT 1";
								//echo("$readZGovtFinal_sql<br>");
								$resultZGovt_read = @PowerAlmanac\PDb::query($readZGovtFinal_sql);
								if (!$resultZGovt_read) {
									die("Error reading from government database: $readZGovt_sql" . PowerAlmanac\PDb::error());
								}
								$rowZGovt = PowerAlmanac\PDb::fetch_array($resultZGovt_read);
								//print_r($rowZGovt); echo("<br>");
								$cellGovt = $rowZGovt['1'];
								$tableGovtArray[$rowCtrIndex]['1'] = $cellGovt;
								*/
								for ($i = 1; $i < $numItems; $i++) {
									// officials
									$cell = $row["$i"];
									$rowCtrArray[$rowCtrIndex] = $rowCtrArray[$rowCtrIndex] + $cell;
									$columnCtrArray[$i] = $columnCtrArray[$i] + $cell;
									$cellBGColor = '#ffffff';
									$fontStyle = 'analyzeGovtOff';
									$tableArray[$rowCtrIndex][$i] = $cell;
									// govt counts
									$cellGovt = $rowGovt["$i"];
									$rowGovtCtrArray[$rowCtrIndex] = $rowGovtCtrArray[$rowCtrIndex] + $cellGovt;
									$columnGovtCtrArray[$i] = $columnGovtCtrArray[$i] + $cellGovt;
									$tableGovtArray[$rowCtrIndex][$i] = $cellGovt;
								}
								// add zero spend to last column
								$tableArray[$rowCtrIndex][$numItems] = 0; // zero median for spending
								// get government counts
								$readZGovtFinal_sql = $readZGovt_sql . " WHERE c_budget = '$budgetCategoryAbbr' LIMIT 1";
								//echo("$readZGovtFinal_sql<br>");
								$resultZGovt_read = @PowerAlmanac\PDb::query($readZGovtFinal_sql);
								if (!$resultZGovt_read) {
									die("Error reading from government database: $readZGovt_sql" . PowerAlmanac\PDb::error());
								}
								$rowZGovt = PowerAlmanac\PDb::fetch_array($resultZGovt_read);
								//print_r($rowZGovt); echo("<br>");
								$cellGovt = $rowZGovt['1'];
								$tableGovtArray[$rowCtrIndex][$numItems] = $cellGovt;
							} else {
								for ($i = 1; $i < $numItems; $i++) {
									// officials
									$cell = $row["$i"];
									$rowCtrArray[$rowCtrIndex] = $rowCtrArray[$rowCtrIndex] + $cell;
									$columnCtrArray[$i] = $columnCtrArray[$i] + $cell;
									$cellBGColor = '#ffffff';
									$fontStyle = 'analyzeGovtOff';
									$tableArray[$rowCtrIndex][$i] = $cell;
									// govt counts
									$cellGovt = $rowGovt["$i"];
									$rowGovtCtrArray[$rowCtrIndex] = $rowGovtCtrArray[$rowCtrIndex] + $cellGovt;
									$columnGovtCtrArray[$i] = $columnGovtCtrArray[$i] + $cellGovt;
									$tableGovtArray[$rowCtrIndex][$i] = $cellGovt;
								}
							}
							// replace row totals with calculated ones from tables
							if ($selectRow == '3') // budget categories
							{
								switch($selectCount) {
									case '1': // median
										$fullTotalsID = $tableID . "_total_exp_vector";	
										break;
									case '2': // median per capita
										$fullTotalsID = $tableID . "_total_per_capita_vector";
										break;
									default:
										$fullTotalsID = $tableID . "_total_exp_vector";	
										break;
								}
								$budgetread_sql = sprintf("SELECT * FROM %s
									WHERE c_budget = '%s'
									LIMIT 1
								", escape($fullTotalsID), escape($budgetCategoryAbbr));
								$result_budgetread = @PowerAlmanac\PDb::query($budgetread_sql);
								if (!$result_budgetread) {
									die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
								}
								$onerow = PowerAlmanac\PDb::fetch_array($result_budgetread);
								$value = $onerow['value'];
								$rowCtrArray[$rowCtrIndex] = $value;
							}
							if ($selectRow == '1') // states
							{
								switch($selectCount) {
									case '1': // median
										$fullTotalsID = $tableID . "_budget_state_median_total";	
										break;
									case '2': // median per capita
										$fullTotalsID = $tableID . "_budget_state_median_per_capita";
										break;
									default:
										$fullTotalsID = $tableID . "_budget_state_median_total";	
										break;
								}
								$stateread_sql = sprintf("SELECT * FROM %s
									WHERE c_budget = 'TOT'
									LIMIT 1
								", escape($fullTotalsID));
								$result_stateread = @PowerAlmanac\PDb::query($stateread_sql);
								if (!$result_stateread) {
									die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
								}
								$onerow = PowerAlmanac\PDb::fetch_array($result_stateread);
								$value = $onerow["$stateAbbr"];
								$rowCtrArray[$rowCtrIndex] = $value;
							}
							if ($selectRow == '2') // population
							{
								switch($selectCount) {
									case '1': // median
										$fullTotalsID = $tableID . "_budget_population_median_total";	
										break;
									case '2': // median per capita
										$fullTotalsID = $tableID . "_budget_population_median_per_capita";
										break;
									default:
										$fullTotalsID = $tableID . "_budget_population_median_total";	
										break;
								}
								$stateread_sql = sprintf("SELECT * FROM %s
									WHERE c_budget = 'TOT'
									LIMIT 1
								", escape($fullTotalsID));
								$result_stateread = @PowerAlmanac\PDb::query($stateread_sql);
								if (!$result_stateread) {
									die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
								}
								$onerow = PowerAlmanac\PDb::fetch_array($result_stateread);
								$value = $onerow["$popAbbr"];
								$rowCtrArray[$rowCtrIndex] = $value;
							}
							$rowCtrIndex = $rowCtrIndex + 1;
						}
						
						// Get TABLE TOTAL
						$tableTotals = 0;
						$tableGovtTotals = 0;
						for ($j = 1; $j <= $numberOfColumns; $j++) {
							$tableTotals = $tableTotals + $columnCtrArray[$j];
							$tableGovtTotals = $tableGovtTotals + $columnGovtCtrArray[$j];	
						}
						if ($selectColumn == '1') {
							// exception with Government counts for Roles
							$tableGovtTotals = 0;
							for ($j = 0; $j < $numberOfRows; $j++) {
								$tableGovtTotals = $tableGovtTotals + $rowGovtCtrArray[$j];	
							}
						}
						
						// sort states
						sort($stateTableArray);
						
						/*********************************************************** 
						                            FINAL OUTPUT
						***********************************************************/
						// Init
						$finalOutputHTML = '';
						
						// Calculate and FINAL OUTPUT
						for ($rowNN = 0; $rowNN < $numberOfRows; $rowNN++) {
							if ($selectRow == '1') {
								// states
								$stateSorted = $stateTableArray[$rowNN];
								// find row number in table array for state
								//echo("stateSorted = $stateSorted<br>");
								for ($rowX = 0; $rowX < $numberOfRows; $rowX++) {
									if ($tableArray[$rowX]['0'] == $stateSorted) {
										$rowN = $rowX;
									}
								}
								//echo("tableIndex = $rowN<br>");
								//$rowN = $rowNN;
							} else {
								// 
								$rowN = $rowNN;
							}
							$rowName = $tableArray[$rowN][0];
							// header data
							$finalOutputHTML = $finalOutputHTML . "<tr><td></td><td bgcolor='#b5c8dc' width='200'><b><font class='analyzeHdr'>$rowName</font></b></td>";
							// for CSV
							//$finalCSV = $finalCSV . '"' . str_replace("<b>","",str_replace("</b>","",$rowName)) . '"' . ",";
							$finalCSV = $finalCSV . '"' . strip_tags($rowName) . '"' . ",";
							// columns data
							if (($selectRow == '3') && ($selectColumn == '4')) {
								// inserted zero spend - 5th quartile
								for ($columnN = 1; $columnN < $numberOfColumns + 1; $columnN++) {
									$rowData = $tableArray[$rowN][$columnN];
									$rowGovtData = $tableGovtArray[$rowN][$columnN];
									switch($selectCount) {
										case '1': // count
										case '2': 
											$formattedCell = number_format($rowData);
											$formattedGovtCell = number_format($rowGovtData);
											break;
										default:
											$formattedCell = number_format($rowData);
											$formattedGovtCell = number_format($rowGovtData);
											break;
									}
									$finalOutputHTML = $finalOutputHTML . "<td align='right' bgcolor='$cellBGColor'><font class='$fontStyle'>$$formattedCell</font></td><td align='right' bgcolor='$cellBGColor' style='display:$govtResultsDisplay;'><font class='analyzeGovt'>$formattedGovtCell</font></td>";
									// for CSV
									$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell) . ",";
								}

							} else {
								for ($columnN = 1; $columnN < $numberOfColumns; $columnN++) {
									$rowData = $tableArray[$rowN][$columnN];
									$rowGovtData = $tableGovtArray[$rowN][$columnN];
									switch($selectCount) {
										case '1': // count
										case '2': 
											$formattedCell = number_format($rowData);
											$formattedGovtCell = number_format($rowGovtData);
											break;
										default:
											$formattedCell = number_format($rowData);
											$formattedGovtCell = number_format($rowGovtData);
											break;
									}
									$finalOutputHTML = $finalOutputHTML . "<td align='right' bgcolor='$cellBGColor'><font class='$fontStyle'>$$formattedCell</font></td><td align='right' bgcolor='$cellBGColor' style='display:$govtResultsDisplay;'><font class='analyzeGovt'>$formattedGovtCell</font></td>";
									// for CSV
									$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell) . ",";
								}
							}
							// row total data
							$rowTotal = $rowCtrArray[$rowN];
							$rowGovtTotal = $rowGovtCtrArray[$rowN];
							switch($selectCount) {
								case '1': 
								case '2': 
									$formattedCell = number_format($rowTotal); 
									$formattedGovtCell = number_format($rowGovtTotal);									
									break;
								default:
									$formattedCell = number_format($rowTotal); 
									$formattedGovtCell = number_format($rowGovtTotal);
									break;
							}
							$finalOutputHTML = $finalOutputHTML . "<td bgcolor='#b5c8dc' align='right' width='100'><font class='analyzeHdr'><b>$$formattedCell</b></font></td><td bgcolor='#b5c8dc' align='right' style='display:$govtResultsDisplay;'><font class='analyzeGovt'><font color='#000000'>$formattedGovtCell</font></font></td>";
							// for CSV
							$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell);
							$finalCSV = $finalCSV . "\n";	
						}
						if ($selectRow != '3') // skip column totals if budget categories
						{
							// COLUMN TOTALS
							$finalOutputHTML = $finalOutputHTML . "<tr><td></td><td bgcolor='#d6d6d6'><font class='analyzeHdr'><b>Column Median</b></font></td>";
							// for CSV
							$finalCSV = $finalCSV . "Column Median,";
							for ($columnN = 1; $columnN < $numberOfColumns; $columnN++) {
								$rowData = $columnCtrArray[$columnN];
								$rowGovtData = $columnGovtCtrArray[$columnN];
								switch($selectCount) {
									case '1':
									case '2': 
										$formattedCell = number_format($rowData);
										$formattedGovtCell = number_format($rowGovtData);
										break;
									default:
										$formattedCell = number_format($rowData);
										$formattedGovtCell = number_format($rowGovtData);
										break;
								}
								// replace with calculated numbers from budget tables
								if ($selectColumn == '1') { // government types
									switch($selectCount) {
										case '1': // median
											$fullTotalsID = $tableID . "_budget_govtypes_median_total";	
											break;
										case '2': // median per capita
											$fullTotalsID = $tableID . "_budget_govtypes_median_per_capita";
											break;
										default:
											$fullTotalsID = $tableID . "_budget_govtypes_median_total";	
											break;
									}
									$govttyperead_sql = sprintf("SELECT * FROM %s
										WHERE c_budget = 'TOT'
										LIMIT 1
									", escape($fullTotalsID));
									$result_govttyperead = @PowerAlmanac\PDb::query($govttyperead_sql);
									if (!$result_govttyperead) {
										die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
									}
									$onerow = PowerAlmanac\PDb::fetch_array($result_govttyperead);
									switch($columnN) {
										case '1':
											$value = $onerow['COU'];
											break;
										case '2':
											$value = $onerow['MUN'];
											break;
										case '3':
											$value = $onerow['TOW'];
											break;
										default;
											$value = $onerow['COU'];
											break;
									}
									$formattedCell = number_format($value);
								}
								if ($selectColumn == '2') { // population
									switch($selectCount) {
										case '1': // median
											$fullTotalsID = escape($tableID . "_budget_population_median_total");
											$populationread_sql = "SELECT * FROM $fullTotalsID
												WHERE c_budget = 'TOT'
												LIMIT 1
											";
											$result_populationread = @PowerAlmanac\PDb::query($populationread_sql);
											if (!$result_populationread) {
												die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
											}
											$onerow = PowerAlmanac\PDb::fetch_array($result_populationread);
											switch($columnN) {
												case '1':
													$value = $onerow['P0'];
													break;
												case '2':
													$value = $onerow['P1'];
													break;
												case '3':
													$value = $onerow['P2'];
													break;
												case '4':
													$value = $onerow['P3'];
													break;
												case '5':
													$value = $onerow['P4'];
													break;
												case '6':
													$value = $onerow['P5'];
													break;
												case '7':
													$value = $onerow['P6'];
													break;
												case '8':
													$value = $onerow['P7'];
													break;
												case '9':
													$value = $onerow['P8'];
													break;
												case '10':
													$value = $onerow['P9'];
													break;
												default;
													$value = $onerow['P0'];
													break;
											}
											break;
										case '2': // median per capita
											$fullTotalsID = escape($tableID . "_budget_population_median_per_capita");
											$populationread_sql = "SELECT * FROM $fullTotalsID
												WHERE c_budget = 'TOT'
												LIMIT 1
											";
											$result_populationread = @PowerAlmanac\PDb::query($populationread_sql);
											if (!$result_populationread) {
												die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
											}
											$onerow = PowerAlmanac\PDb::fetch_array($result_populationread);
											// no P0 on per capita
											// skip P0
											switch($columnN) {
												case '1':
													$value = $onerow['P1'];
													break;
												case '2':
													$value = $onerow['P2'];
													break;
												case '3':
													$value = $onerow['P3'];
													break;
												case '4':
													$value = $onerow['P4'];
													break;
												case '5':
													$value = $onerow['P5'];
													break;
												case '6':
													$value = $onerow['P6'];
													break;
												case '7':
													$value = $onerow['P7'];
													break;
												case '8':
													$value = $onerow['P8'];
													break;
												case '9':
													$value = $onerow['P9'];
													break;
												default;
													$value = $onerow['P1'];
													break;
											}
											break;
										default:
											$fullTotalsID = escape($tableID . "_budget_population_median_total");
											$populationread_sql = "SELECT * FROM $fullTotalsID
												WHERE c_budget = 'TOT'
												LIMIT 1
											";
											$result_populationread = @PowerAlmanac\PDb::query($populationread_sql);
											if (!$result_populationread) {
												die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
											}
											$onerow = PowerAlmanac\PDb::fetch_array($result_populationread);
											switch($columnN) {
												case '1':
													$value = $onerow['P0'];
													break;
												case '2':
													$value = $onerow['P1'];
													break;
												case '3':
													$value = $onerow['P2'];
													break;
												case '4':
													$value = $onerow['P3'];
													break;
												case '5':
													$value = $onerow['P4'];
													break;
												case '6':
													$value = $onerow['P5'];
													break;
												case '7':
													$value = $onerow['P6'];
													break;
												case '8':
													$value = $onerow['P7'];
													break;
												case '9':
													$value = $onerow['P8'];
													break;
												case '10':
													$value = $onerow['P9'];
													break;
												default;
													$value = $onerow['P0'];
													break;
											}
											break;
									}
									$formattedCell = number_format($value);
								}
								$finalOutputHTML = $finalOutputHTML . "<td bgcolor='#d6d6d6' align='right'><font class='analyzeGovtOff'><b>$$formattedCell</b></font></td><td bgcolor='#d6d6d6' align='right' style='display:$govtResultsDisplay;'><font class='analyzeGovt'><b>$formattedGovtCell</b></font></td>";
								// for CSV
								$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell) . ",";
							}
							
							// FULL TOTALS in numColumns + 1
							
							switch($selectCount) {
								// only used for Tables G, H, I: States-GovtTypes, States-Population, Population-GovtTypes
								case '1':
									// median - get from 
									$fullTotalsID = escape($tableID . "_total_exp_vector");
									$tableTotalread_sql = "SELECT * FROM $fullTotalsID
										WHERE c_budget = 'TOT'
										LIMIT 1
									";
									$result_tableTotalread = @PowerAlmanac\PDb::query($tableTotalread_sql);
									if (!$result_tableTotalread) {
										die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
									}
									$onerow = PowerAlmanac\PDb::fetch_array($result_tableTotalread);
									$tableTotals = $onerow['value'];
									$formattedCell = number_format($tableTotals);
									$formattedGovtCell = number_format($tableGovtTotals);
									break;
								case '2':
									// median per capita - get from 
									$fullTotalsID = escape($tableID . "_total_per_capita_vector");
									$tableTotalread_sql = "SELECT * FROM $fullTotalsID
										WHERE c_budget = 'TOT'
										LIMIT 1
									";
									$result_tableTotalread = @PowerAlmanac\PDb::query($tableTotalread_sql);
									if (!$result_tableTotalread) {
										die('Error reading from $fullTotalsID database:' . PowerAlmanac\PDb::error());
									}
									$onerow = PowerAlmanac\PDb::fetch_array($result_tableTotalread);
									$tableTotals = $onerow['value'];
									$formattedCell = number_format($tableTotals);
									$formattedGovtCell = number_format($tableGovtTotals);
									break;
								default:
									$tableTotals = '0';
									$formattedCell = number_format($tableTotals);
									$formattedGovtCell = number_format($tableGovtTotals);
									break;
							}
							$finalOutputHTML = $finalOutputHTML . "<td bgcolor='#44576a' align='right'><font class='analyzeGovtOff'><font color='#FFFFFF'><b>$$formattedCell</b></font></td><td bgcolor='#44576a' align='right' style='display:$govtResultsDisplay;'><font class='analyzeGovt'><font color='#FFFFFF'><b>$formattedGovtCell</b></font></font></td>";
							$finalOutputHTML = $finalOutputHTML . "<td></td></tr>";
							// for CSV
							$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell);
							$finalCSV = $finalCSV . "\n";	
						} else {
							if (($selectRow == '3') && ($selectColumn == '4')) {
								// inserted 5th quartile
								$numberOfColumns = $numberOfColumns + 1;
							}
							if ($govtResultsDisplay == 'inline') {
								$numberFill = ($numberOfColumns - 1)*2;
							} else {
								$numberFill = $numberOfColumns - 1;
							}
							$finalOutputHTML = $finalOutputHTML . "<tr><td colspan='2'>&nbsp;</td><td colspan='$numberFill' bgcolor='#d3d3d3'>&nbsp;</td></tr>";
						}
						echo($finalOutputHTML);
						// save it in session variable
						$_SESSION['analyzeCSV'] = $finalCSV;
                        ?>
                        <tr>
                        	<td colspan="20" bgcolor="#FFFFFF">&nbsp;
                            <?php
							$debug = 0;
							if ($debug) {
								echo("<br>DEBUG:<br>");
								echo("read_sql = $read_sql<br>");
								echo("readGovt_sql = $readGovt_sql<br>");
								/*
								echo("<br>numberOfColumns = $numberOfColumns<br>");
								echo("<br>numberOfRows = $numberOfRows<br>");
								echo("<br>tableTotals = $tableTotals<br>");
								echo("<br>columnCtrArray<br>");
								print_r($columnCtrArray);
								echo("<br>rowCtrArray<br>");
								print_r($rowCtrArray);
								*/
								//echo("<br>tableArray<br>");
								//print_r($tableArray);
								/*
								echo("<br>tableGovtTotals = $tableGovtTotals");
								echo("<br>columnGovtCtrArray");
								print_r($columnGovtCtrArray);
								echo("<br>rowGovtCtrArray");
								print_r($rowGovtCtrArray);
								echo("<br>tableGovtArray");
								print_r($tableGovtArray);
								echo("<br>Excel CSV:<br>");
								echo("<pre>$finalCSV</pre>");
								*/
							}
							?>
                            </td>
                        </tr>
                        </table>
                        <br>
                        </form>
                        <br>
                        <i>
                        NOTE:  Financial data comes from the U.S. Census Bureau's 2007 complete survey of all state and local government finances.   The Power Almanac calculates the per capita spend" by using 2007 financial data and 2007 population data.   The medians in each category are for governments that spend more than $0 in that category.  The summary spending categories are created and defined by the Power Almanac. Information is deemed reliable but not guaranteed.
                        </i>
                        <br><br>
                    </td>
                </tr>
                <?php
						break;
					case '1': // ANALYZE TAB: TALLY RECORDS
						$finalCSV = '';
				?>
                <tr>
                	<td colspan="4">
                    <form name="analyze" method="get">
                    <input type="hidden" name="skipBuild" value="1">
                    <input type="hidden" name="type" value="<?php echo("$selectType"); ?>">
                    <table cellpadding="3" cellspacing="1" width="100%" border="0" bgcolor="#FFFFFF">
                    	<tr>
                        	<td></td>
                            <td align="left" width="200">
                            <select name="count" onChange="javascript:tallySubmitForm();" id="countT">
                                <option value="1" <?php echo($reqCountSelect['1']); ?> class="analyzeColumnHdr">DATA: Count</option>
                                <option value="2" <?php echo($reqCountSelect['2']); ?> class="analyzeColumnHdr">DATA: % of Row</option>
                                <option value="3" <?php echo($reqCountSelect['3']); ?> class="analyzeColumnHdr">DATA: % of Column</option>
                                <option value="4" <?php echo($reqCountSelect['4']); ?> class="analyzeColumnHdr">DATA: % of Table</option>
                            </select>
                            </td>
                            <?php
							if ($govtResultsDisplay == 'inline') {
								$skipColumns = ($numColumns + 1)*2;
								$columnHdrSize = 2;
							} else {
								$skipColumns = ($numColumns + 1);
								$columnHdrSize = 1;
							}
							?>
                            <td colspan="<?php echo($skipColumns); ?>" align="center" bgcolor="#9a9a9a" height="25">
                            <select name="column" onChange="javascript:tallySubmitForm();" id="column">
                                <option value="3" <?php echo($reqColumnSelect['3']); ?> class="analyzeColumnHdr">COLUMN: Government Types</option>
                                <option value="1" <?php echo($reqColumnSelect['1']); ?> class="analyzeColumnHdr">COLUMN: Roles</option>
                                <option value="2" <?php echo($reqColumnSelect['2']); ?> class="analyzeColumnHdr">COLUMN: Population Ranges</option>
                            </select>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                        	<td width="5">&nbsp;
                            
                            </td>
                        	<td bgcolor="#9a9a9a" width='200'>
                            <select name="row" onChange="javascript:tallySubmitForm();" id="row">
                                <option value="4" <?php echo($reqRowSelect['4']); ?> class="analyzeColumnHdr">ROW: Government Types</option>
                                <option value="1" <?php echo($reqRowSelect['1']); ?> class="analyzeColumnHdr">ROW: States</option>
                                <option value="2" <?php echo($reqRowSelect['2']); ?> class="analyzeColumnHdr">ROW: Population Ranges</option>
                                <!--<option value="3" <?php echo($reqRowSelect['3']); ?> class="analyzeColumnHdr">Role</option>-->
                            </select>
                            </td>
                            <?php
							// for CSV
							switch($selectRow) {
								case '1':
									$finalCSV = $finalCSV . "States,";
									break;
								case '2':
									$finalCSV = $finalCSV . "Population,";
									break;
								case '3':
									$finalCSV = $finalCSV . "Government Type,";
									break;	
								default:
									break;
							}
							// headers
							foreach ($columnArray as $columnItem) {
								echo("<td bgcolor='#d3d3d3' align='right' colspan='$columnHdrSize'><font class='analyzeHdr'>$columnItem</font></td>");
								// for CSV
								$finalCSV = $finalCSV . '"' . $columnItem . '","' . $columnItem . '" (G),';
							}
							?>
                            <td bgcolor="#cfd7e0" align="right" colspan="<?php echo($columnHdrSize); ?>">
                            <font class='analyzeHdr'><b>Row Total&nbsp;</b></font>
                            <?php
							// for CSV
							$finalCSV = $finalCSV . "Row Total, Row Total (G)";
							$finalCSV = $finalCSV . "\n";
							?>
                            </td>
                            <td width="5">&nbsp;
                            
                            </td>
                        </tr>
                        <?php
						// up to 11 roles - added Head of IT
						$columnCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0);
						$columnGovtCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0);
						$columnCtrIndex = 1;
						// up to 51 states
						$rowCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
						$rowGovtCtrArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
						$rowCtrIndex = 0;
						// output HTML
						$outputHTML = '';
						// get counts
						$numberOfColumns = 0;
						$numberOfRows = PowerAlmanac\PDb::num_rows($result_read);
						// table
						$stateTableArray = array();

						while ($row = PowerAlmanac\PDb::fetch_array($result_read))
						{
							$numItems = count($row)/2; // because of associative array + numeric index
							if ($numberOfColumns == 0) {
								$numberOfColumns = $numItems;
							}
							switch($selectRow) {
								case '1':
									// need to lookup STATES
									$stateAbbr = $row[0];
									$stateName = StateLookUp($stateAbbr);
									$formattedRowCell = $stateName;
									array_push($stateTableArray,"$formattedRowCell");
									break;
								case '2':
									// need to lookup POPULATION
									$popAbbr = $row[0];
									$popName = PopulationLookUp($popAbbr);
									$formattedRowCell = $popName;
									break;
								case '4':
									// need to lookup GOVERNMENT TYPES
									$govtTypeAbbr = $row[0];
									$govtName = GovtTypesLookUp($govtTypeAbbr);
									$formattedRowCell = $govtName;
									break;
								default: // RAW
									$formattedRowCell = $row[0];
									break;
							}
							// fill in 
							$tableArray[$rowCtrIndex]['0'] = $formattedRowCell;
							
							// govt
							$rowGovt = PowerAlmanac\PDb::fetch_array($resultGovt_read);
							//echo("<br>$readGovt_sql<br>");
							//print_r($rowGovt);
							//exit;
							
							// ROW NAME
							// start at index 1 since index 0 is row name
							for ($i = 1; $i < $numItems; $i++) {
								$cell = $row["$i"];
								$rowCtrArray[$rowCtrIndex] = $rowCtrArray[$rowCtrIndex] + $cell;
								$columnCtrArray[$i] = $columnCtrArray[$i] + $cell;
								$cellBGColor = '#ffffff';
								$fontStyle = 'analyzeGovtOff';
								$tableArray[$rowCtrIndex][$i] = $cell;
								// govt
								$cellGovt = $rowGovt["$i"];
								$rowGovtCtrArray[$rowCtrIndex] = $rowGovtCtrArray[$rowCtrIndex] + $cellGovt;
								$columnGovtCtrArray[$i] = $columnGovtCtrArray[$i] + $cellGovt;
								$tableGovtArray[$rowCtrIndex][$i] = $cellGovt;
								//echo("rowGovtCtrArray[$rowCtrIndex] = $rowGovtCtrArray[$rowCtrIndex]<br>");
							}
							/***************************************
							 Goverment Counts EXCEPTION - ROW TOTAL
							****************************************/
							if ($selectColumn == '1') {
								// exception with Government counts for Roles
								switch($selectRow) {
									case '1': // states
										$stateName = $tableArray[$rowCtrIndex]['0'];
										$stateAbbr = reverseLookup($stateName);	
										$fullGovtTableID = $tableID . "_state_govtypes_govs";
										$readGovtTotal_sql = sprintf("SELECT * FROM %s
											WHERE state = '%s'
											LIMIT 1
											", escape($fullGovtTableID), escape($stateAbbr));
										$resultGovtTotal_read = @PowerAlmanac\PDb::query($readGovtTotal_sql);
										if (!$resultGovtTotal_read) {
											die("Error reading from government database: " . PowerAlmanac\PDb::error());
										}
										$onerow = PowerAlmanac\PDb::fetch_array($resultGovtTotal_read);
										$rowGovtCtrArray[$rowCtrIndex] = $onerow['COU'] + $onerow['MUN'] + $onerow['TOW'];
										break;
									case '2': // population
										$popAbbr = $tableArray[$rowCtrIndex]['0'];
										//echo("<p>popAbbr = $popAbbr</p>");
										$populationType = reversePopulationLookUp($popAbbr);
										//echo("<p>populationType = $populationType</p>");
										$fullGovtTableID = $tableID . "_population_categorys_govtypes_govs";	
										$readGovtTotal_sql = sprintf("SELECT * FROM %s
											WHERE Population_Category = '%s'
											LIMIT 1
											",escape($fullGovtTableID), escape($populationType));
										$resultGovtTotal_read = @PowerAlmanac\PDb::query($readGovtTotal_sql);
										if (!$resultGovtTotal_read) {
											die("Error reading from government database: " . PowerAlmanac\PDb::error());
										}
										$onerow = PowerAlmanac\PDb::fetch_array($resultGovtTotal_read);
										//print_r($onerow);
										$rowGovtCtrArray[$rowCtrIndex] = $onerow['COU'] + $onerow['MUN'] + $onerow['TOW'];	
										break;
									case '4': // government types
										// use top elected official count only
										$govtAbbr = $tableArray[$rowCtrIndex]['0'];
										//echo("<p>govtAbbr = $govtAbbr</p>");
										$govtType = reverseGovtTypesLookUp($govtAbbr);
										//echo("<p>govtType = $govtType</p>");
										$fullGovtTableID = $tableID . "_govtypes_roles_govs";	
										$readGovtTotal_sql = sprintf("SELECT * FROM %s
											WHERE GovTypes_Roles = '%s'
											LIMIT 1
											", escape($fullGovtTableID), escape($govtType));
										$resultGovtTotal_read = @PowerAlmanac\PDb::query($readGovtTotal_sql);
										if (!$resultGovtTotal_read) {
											die("Error reading from government database: " . PowerAlmanac\PDb::error());
										}
										$onerow = PowerAlmanac\PDb::fetch_array($resultGovtTotal_read);
										//print_r($onerow);
										$rowGovtCtrArray[$rowCtrIndex] = $onerow['TEO'];	
										break;
									default:
										break;
								}
							}
							$rowCtrIndex = $rowCtrIndex + 1;
						}
						
						//echo("<br>rowGovtCtrArray");
						//print_r($rowGovtCtrArray);
						
						// Get TABLE TOTAL
						$tableTotals = 0;
						$tableGovtTotals = 0;
						for ($j = 1; $j <= $numberOfColumns; $j++) {
							$tableTotals = $tableTotals + $columnCtrArray[$j];
							$tableGovtTotals = $tableGovtTotals + $columnGovtCtrArray[$j];	
						}
						/*****************************************
						 Goverment Counts EXCEPTION - TABLE TOTAL
						******************************************/
						
						if ($selectColumn == '1') {
							// exception with Government counts for Roles
							$tableGovtTotals = 0;
							for ($j = 0; $j < $numberOfRows; $j++) {
								$tableGovtTotals = $tableGovtTotals + $rowGovtCtrArray[$j];	
							}
						}
						
						// sort states
						sort($stateTableArray);

								
						/*********************************************************** 
						                            FINAL OUTPUT
						***********************************************************/
						// Init
						$finalOutputHTML = '';
						
						// Calculate and FINAL OUTPUT
						for ($rowNN = 0; $rowNN < $numberOfRows; $rowNN++) {
							if ($selectRow == '1') {
								// states
								$stateSorted = $stateTableArray[$rowNN];
								// find row number in table array for state
								//echo("stateSorted = $stateSorted<br>");
								for ($rowX = 0; $rowX < $numberOfRows; $rowX++) {
									if ($tableArray[$rowX]['0'] == $stateSorted) {
										$rowN = $rowX;
									}
								}
								//echo("tableIndex = $rowN<br>");
								//$rowN = $rowNN;
							} else {
								// 
								$rowN = $rowNN;
							}
							$rowName = $tableArray[$rowN][0];
							// header data
							$finalOutputHTML = $finalOutputHTML . "<tr><td></td><td bgcolor='#b5c8dc' width='200'><b><font class='analyzeHdr'>$rowName</font></b></td>";
							// for CSV
							//$finalCSV = $finalCSV . '"' . str_replace("<b>","",str_replace("</b>","",$rowName)) . '"' . ",";
							$finalCSV = $finalCSV . '"' . strip_tags($rowName) . '"' . ",";
							// columns data
							for ($columnN = 1; $columnN < $numberOfColumns; $columnN++) {
								
								$rowData = $tableArray[$rowN][$columnN];
								$rowGovtData = $tableGovtArray[$rowN][$columnN];
								//echo("<p>rowGovtData[$rowN][$columnN] = $rowGovtData</p>");
								switch($selectCount) {
									case '1': // count
										$formattedCell = number_format($rowData);
										$formattedGovtCell = number_format($rowGovtData);
										break;
									case '2': // percentages by row
										if ($rowCtrArray[$rowN] != 0) {
											$caculatedRowData = ($rowData/$rowCtrArray[$rowN])*100;
										} else {
											$caculatedRowData = 0;
										}
										if ($rowGovtCtrArray[$rowN] != 0) {
											$caculatedGovtRowData = ($rowGovtData/$rowGovtCtrArray[$rowN])*100;
										} else {
											$caculatedGovtRowData = 0;
										}
										$formattedCell = sprintf("%01.0f", $caculatedRowData) . '%';
										$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowData) . '%';
										break;
									case '3':// percentages by column
										if ($columnCtrArray[$columnN] != 0) {
											$caculatedRowData = ($rowData/$columnCtrArray[$columnN])*100;
										} else {
											$caculatedRowData = 0;
										}
										if ($columnGovtCtrArray[$columnN] != 0) {
											$caculatedGovtRowData = ($rowGovtData/$columnGovtCtrArray[$columnN])*100;
										} else {
											$caculatedGovtRowData = 0;
										}
										$formattedCell = sprintf("%01.0f", $caculatedRowData) . '%';
										$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowData) . '%';
										break;
									case '4':// percentages by table
										$caculatedRowData = ($rowData/$tableTotals)*100;
										$caculatedGovtRowData = ($rowGovtData/$tableGovtTotals)*100;
										$formattedCell = sprintf("%01.0f", $caculatedRowData) . '%';
										$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowData) . '%';
										break;	
									default:
										$formattedCell = $rowData;
										$formattedGovtCell = $rowGovtData;
										break;
								}
								$finalOutputHTML = $finalOutputHTML . "<td align='right' bgcolor='$cellBGColor' width='80'><font class='$fontStyle'>$formattedCell</font></td><td align='right' style='display:$govtResultsDisplay;'><font class='analyzeGovt'>$formattedGovtCell</font></td>";
								// for CSV
								$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell) . ",";
							}
							// row total data
							$rowTotal = $rowCtrArray[$rowN];
							$rowGovtTotal = $rowGovtCtrArray[$rowN];
							switch($selectCount) {
								case '1': // count
									$formattedCell = number_format($rowTotal); 
									$formattedGovtCell = number_format($rowGovtTotal);									
									break;
								case '2': // percentages by row
									if ($rowCtrArray[$rowN] != 0) {
										//echo("rowGovtCtrArray[$rowN] = $rowGovtCtrArray[$rowN]<br>");
										$caculatedRowTotalData = ($rowTotal/$rowCtrArray[$rowN])*100;
										$caculatedGovtRowTotalData = ($rowGovtTotal/$rowGovtCtrArray[$rowN])*100;
										$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
										$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									} else {
										$caculatedRowTotalData = ($rowTotal/$rowCtrArray[$rowN])*100;
										$caculatedGovtRowTotalData = ($rowGovtTotal/$rowGovtCtrArray[$rowN])*100;
										$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
										$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									}
									break;
								case '3':// percentages by column
									$caculatedRowTotalData = ($rowTotal/$tableTotals)*100; 
									$caculatedGovtRowTotalData = ($rowGovtTotal/$tableGovtTotals)*100; 
									$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
									$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									break;
								case '4':// percentages by table
									$caculatedRowTotalData = ($rowTotal/$tableTotals)*100;
									$caculatedGovtRowTotalData = ($rowGovtTotal/$tableGovtTotals)*100;
									$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
									$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									break;	
								default:
									$formattedCell = $rowData;
									$formattedGovtCell = $rowGovtTotal;
									break;
							}
							$finalOutputHTML = $finalOutputHTML . "<td bgcolor='#b5c8dc' align='right' width='100'><font class='analyzeHdr'><b>$formattedCell</b></font></td><td bgcolor='#b5c8dc' align='right' style='display:$govtResultsDisplay;'><font class='analyzeGovt'><font color='#000000'>$formattedGovtCell</font></font></td>";
							// for CSV
							$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell);
							$finalCSV = $finalCSV . "\n";	
						}
						
						// COLUMN TOTALS
						$finalOutputHTML = $finalOutputHTML . "<tr><td></td><td bgcolor='#d6d6d6'><font class='analyzeHdr'><b>Column Total</b></font></td>";
						// for CSV
						$finalCSV = $finalCSV . "Column Total,";
						for ($columnN = 1; $columnN < $numberOfColumns; $columnN++) {
							$rowData = $columnCtrArray[$columnN];
							$rowGovtData = $columnGovtCtrArray[$columnN];
							//echo("<p>columnGovtCtrArray[$columnN] = $rowGovtData</p>");
							switch($selectCount) {
								case '1':
									$formattedCell = number_format($rowData);
									$formattedGovtCell = number_format($rowGovtData);
									break;
								case '2': // percentages by row
									$caculatedRowTotalData = ($rowData/$tableTotals)*100;
									$caculatedGovtRowTotalData = ($rowGovtData/$tableGovtTotals)*100;
									$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
									$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									break;
								case '3':// percentages by column
									$caculatedRowTotalData = 100;
									$caculatedGovtRowTotalData = 100;
									$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
									$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									break;
								case '4':// percentages by table
									$caculatedRowTotalData = ($rowData/$tableTotals)*100;
									$caculatedGovtRowTotalData = ($rowGovtData/$tableGovtTotals)*100;
									$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
									$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
									break;	
								default:
									$formattedCell = $rowData;
									$formattedGovtCell = $rowGovtData;
									break;
							}
							$finalOutputHTML = $finalOutputHTML . "<td bgcolor='#d6d6d6' align='right'><font class='analyzeGovtOff'><b>$formattedCell</b></font></td><td bgcolor='#d6d6d6' align='right' style='display:$govtResultsDisplay;'><font class='analyzeGovt'><b>$formattedGovtCell</b></font></td>";
							// for CSV
							$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell) . ",";
						}
						
						// FULL TOTALS in numColumns + 1
						
						switch($selectCount) {
							case '1':
								$formattedCell = number_format($tableTotals);
								$formattedGovtCell = number_format($tableGovtTotals);
								break;
							case '2': // percentages by row
								$caculatedRowTotalData = 100.00;
								$caculatedGovtRowTotalData = 100.00;
								$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
								$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
								break;
							case '3':// percentages by column
								$caculatedRowTotalData = 100.00;
								$caculatedGovtRowTotalData = 100.00;
								$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
								$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
								break;
							case '4':// percentages by table
								$caculatedRowTotalData = 100.00;
								$caculatedGovtRowTotalData = 100.00;
								$formattedCell = sprintf("%01.0f", $caculatedRowTotalData) . '%';
								$formattedGovtCell = sprintf("%01.0f", $caculatedGovtRowTotalData) . '%';
								break;	
							default:
								$formattedCell = $tableTotals;
								$formattedGovtCell = $tableGovtTotals;
								break;
						}
						$finalOutputHTML = $finalOutputHTML . "<td bgcolor='#44576a' align='right'><font class='analyzeGovtOff'><font color='#FFFFFF'><b>$formattedCell</b></font></font></td><td bgcolor='#44576a' align='right' style='display:$govtResultsDisplay;><font class='analyzeGovt''><font color='#FFFFFF'><b>$formattedGovtCell</b></font></font></td>";
						$finalOutputHTML = $finalOutputHTML . "<td></td></tr>";
						echo($finalOutputHTML);
						// for CSV
						$finalCSV = $finalCSV . str_replace(",",'',$formattedCell) . ", " . str_replace(",",'',$formattedGovtCell);
						$finalCSV = $finalCSV . "\n";
						// save it in session variable
						$_SESSION['analyzeCSV'] = $finalCSV;
                        ?>
                        <tr>
                        	<td colspan="20" bgcolor="#FFFFFF">&nbsp;
                            <?php
							$debug = 0;
							if ($debug) {
								echo("<br>DEBUG:<br>");
								echo("read_sql = $read_sql<br>");
								echo("readGovt_sql = $readGovt_sql<br>");
								/*
								echo("tableID = $tableID<br>");
								echo("<br>numberOfColumns = $numberOfColumns");
								echo("<br>numberOfRows = $numberOfRows");
								echo("<br>tableTotals = $tableTotals");
								echo("<br>columnCtrArray");
								print_r($columnCtrArray);
								echo("<br>rowCtrArray");
								print_r($rowCtrArray);
								echo("<br>tableArray");
								print_r($tableArray);
								echo("<br>stateTableArray");
								print_r($stateTableArray);
								echo("<br>tableGovtTotals = $tableGovtTotals<br>");
								echo("<br>columnGovtCtrArray<br>");
								print_r($columnGovtCtrArray);
								echo("<br>rowGovtCtrArray<br>");
								print_r($rowGovtCtrArray);
								echo("<br>tableGovtArray<br>");
								print_r($tableGovtArray);
								*/
								//echo("<br>Excel CSV:<br>");
								//echo("<pre>$finalCSV</pre>");	
							}
							?>
                            </td>
                        </tr>
                        </table>
                        </form>
                        <br>
                    </td>
                </tr>
                <?php	
						break;
					default:
						break;
				}
				?>
                </table>
                </div>
                </td>
            </tr>
            </table>
            <br>
			</td>
		</tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstrip2.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" align="center" bgcolor="#ebebeb">
    <?php
	include("inc/oldfooter.php");
	?>
    </td>
</tr>
</table>
<br />
</body>
</html>
