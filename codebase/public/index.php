<?php
define("TITLE", "Find contact lists for local government officials");
define("DESCRIPTION", "Constantly revised, up-to-date contact information for over 221,500 local government officials. Powerful filtering and search capabilities.");
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");

$SearchForOfficialsDropdownSectionSearch = <<<SEARCH
<div class="form-group">
    {$officialDrop}
    <input type="submit" value="Search &#187;" class="btn btn-primary" />
</div>
SEARCH;

$SearchForOfficialsDropdownSectionGo = <<<SEARCH
<div class="form-group">
    {$officialDrop}
    <input type="submit" value="Go &#187;" class="btn btn-primary" />
</div>
SEARCH;

$SubFooterSection = <<<HTML
<div class="row subfooter" id="solveProblems">
    <div class="col-lg-9 col-md-8 col-sm-7 text-center text-sm-left">
        <strong class="title">Need more convincing? Try Power Almanac</strong>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-5 text-center text-sm-right">
        <a href="/search-government?ns=1" class="btn btn-light" role="button">Search for Officials</a>
    </div>
</div>
HTML;



?>
<!-- welcome -->
            <div class="welcome">
                <div class="container">
                    <div class="row w-100">
                        <div class="col-sm-6 col-md-5 offset-md-1 col-xs-8 offset-xs-2">
                            <h1>Bringing the<br class="hid" /> Local Government<br /> Market to You.</h1>
                            <form action="PA-PRG.php" method="post" class="form-search form form-inline">
                                <fieldset>
                                    <strong class="title">Who Are You Looking For? <img src="images/ico01.png" width="29" height="30" alt="" /></strong>

                                    <?=$SearchForOfficialsDropdownSectionSearch?>

                                    <span class="pointer">Start Now</span>
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-sm-6 offset-sm-0 col-xs-8 offset-xs-2">
                            <div class="frame">
                                <!-- <img src="images/img01.png" width="497" height="280" alt="" /> -->
                                <iframe width="100%" height="280" src="https://www.youtube.com/embed/8Ecbz8nRYzI" frameborder="0" allowfullscreen></iframe>
                                <!-- <a href="#" class="btn-play">Play</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box">
                    <div class="row m-b-20">
                        <div class="col-md-6">
                            <h2>How do you easily market to the right local government officials?</h2>
                        </div>
                        <div class="col-md-4 offset-md-2">
                            <strong class="title">Try the Search</strong>
                            <form action="PA-PRG.php" method="post" class="form-search form form-inline">
                                <fieldset>
                                    <?=$SearchForOfficialsDropdownSectionGo?>
                                </fieldset>
                            </form>
                        </div>
                    </div>


                    <div class="navbar navbar-light bg-light sticky-top">
                        <nav class="nav nav-pills flex-column flex-sm-row">
                            <a class="flex-sm-fill text-sm-center nav-link active" href="#searchTarget">Search and Target</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="#contactInfo">Download Contact Info</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="#analyzeResults">Analyze Results</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="#solveProblems">Solve Your Problems</a>
                        </nav>
                        <a href="#" class="btn btn-secondary">Back to Top</a>
                    </div>


                    <!-- #searchTarget -->
                    <section class="post row" id="searchTarget">
                        <div class="col-sm-12 col-md-5 text-center">
                            <img src="images/img02.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-sm-12 col-md-7 text-center text-md-left">
                            <h3>Role-Based Search</h3>
                            <strong class="subheading">Find the right officials</strong>
                            <h4>10 Different Local Government Official Roles</h4>
                            <div class="columns-list text-left">
                                <ul>
                                    <li>City/County Manager</li>
                                    <li>Head of Purchasing</li>
                                    <li>Head of Public Works</li>
                                    <li>Head of IT</li>
                                </ul>
                                <ul>
                                    <li>Fire Chief</li>
                                    <li>Head Clerk</li>
                                    <li>Mayor</li>
                                </ul>
                                <ul>
                                    <li>Head of Finance</li>
                                    <li>Police Chief/Sheriff</li>
                                    <li>Council Member</li>
                                </ul>
                            </div>
                            <p>For example, you want to reach the person in charge of it all, but every municipality calls him something different. Is it the Mayor, Village President, Town President, Council President, Town Supervisor or the Chairperson of the Board? Who knows? We do.</p>
                            <p>We categorize <strong>each official</strong> into roles, so if you search for Mayor, you will get everyone in that role, no matter what their title may be. We also give you their <strong>exact title</strong> so you can personalize your outreach.</p>
                            <form action="PA-PRG.php" method="post" class="form-search">
                                <fieldset>
                                    <strong class="title">Try it! It’s that easy.</strong>
                                    <?= str_replace(' mr-sm-2', ' col-md-7 mr-sm-2', $SearchForOfficialsDropdownSectionGo) ?>
                                </fieldset>
                            </form>
                        </div>
                    </section>
                    <!-- /#searchTarget -->

                    <!-- #preciseTarget -->
                    <section class="post row" id="preciseTargeting">
                        <div class="col-sm-12 col-md-5 text-center">
                            <img src="images/img03.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-sm-12 col-md-7 text-center text-md-left">
                            <h3>Precise Targeting</h3>
                            <strong class="subheading">Get an exact match to your target market.</strong>
                            <h4>Zoom in. Tight.</h4>
                            <ul class="text-left">
                                <li>Narrow <strong>location</strong> by state, region,  or zipcode+distance.</li>
                                <li>Filter by Population</li>
                                <li>Choose city, county, or township. </li>
                                <li>
                                    Target specific government <strong>spending demographics</strong> like
                                    <ul>
                                        <li>Total and per capita spending,</li>

                                        <li>Last month of fiscal year, and</li>
                                        <li>Amount spent on categories like public safety, health, welfare, utilities, transportation, leisure, finance, etc...</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <!-- /#preciseTarget -->

                    <!-- #contactInfo -->
                    <section class="post row" id="contactInfo">
                        <div class="col-sm-12 col-md-5 text-center">
                            <img src="images/img04.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-sm-12 col-md-7 text-center text-md-left">
                            <h3>Download Contact Info</h3>
                            <strong class="subheading">Quickly get the results you need.</strong>
                            <h4>Phone Numbers, Emails, and Addresses</h4>
                            <p>It’s time to throw some numbers out. Our database spans <strong>21,000 unique local governments</strong>, giving you access to local government decision-makers from <strong>97%</strong> of cities, counties, and townships with a population greater than 1,000. With over <strong>220,000 officials</strong> listed, there’s probably no one you can’t find.</p>
                            <h4>Not Your Grandma’s Attic</h4>
                            <p>This is NOT a compilation of information from other, inadequate sources. We've built - from scratch - and continuously update our own comprehensive list of local government officials and their contact info.<br /> <a href="./how-it-works" class="link-more">Find Out More</a></p>
                        </div>
                    </section>
                    <!-- /#contactInfo -->

                    <!-- #analyzeResults -->
                    <section class="post last-post row" id="analyzeResults">
                        <div class="col-sm-12 col-md-5 text-center">
                            <img src="images/img05.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-sm-12 col-md-7 text-center text-md-left">
                            <h3>Make Good Decisions</h3>
                            <strong class="subheading">The intelligence you need to make good decisions.</strong>
                            <ul class="text-left">
                                <li>Ascertain <strong>exactly</strong> who is most likely to buy your product.</li>
                                <li>Customize your search to produce the results you’re looking for.</li>
                                <li>Save and Export your data to <strong>Excel instantly</strong>.</li>
                                <li>
                                    Narrow your campaigns by filtering the data by
                                    <ul>
                                        <li>Roles of the officials</li>
                                        <li>Population</li>
                                        <li>Location</li>
                                        <li>Types of local government</li>
                                        <li>Spending categories</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <!-- /#analyzeResults -->

                    <?=$SubFooterSection?>
<!--                </div>-->

                <!-- white box -->
<!--                <div class="white-box">-->
                    <div class="row mt-5 m-b-20 bar-bottom">
                        <div class="col-xxs-12 col-md-9">
                            <h2>Solve Your Marketing List Problems</h2>
                            <span class="note">Don’t waste your time. Get in contact with <em><strong>the right local government officials</strong></em> using highly targeted search criteria and accurate contact information.</span>
                        </div>
                    </div>

                    <section class="post row">
                        <div class="col-xs-4 col-sm-5 text-center">
                            <img src="images/img06.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-xs-8 col-sm-7">
                            <h3>1. I don’t want to waste time contacting the wrong people.</h3>
                            <h4>Freakishly Accurate</h4>
                            <p>Don’t worry. We <strong>triple verify all records</strong> before they go into the Power Almanac, so you can be confident that the phone numbers, email addresses, names, titles, roles, governments, websites, and more are all correct.</p>
                            <h4>Obsessively Up-to-Date</h4>
                            <p>Government officials often change because they quit, get fired, or don’t get re-elected. They may also change because of elections. We rigorously verify every record in the Power Almanac <strong>twice per year</strong>, updating and adding new records weekly.</p>
                        </div>
                    </section>

                    <section class="post row">
                        <div class="col-xs-4 col-sm-5 text-center">
                            <img src="images/img07.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-xs-8 col-sm-7">
                            <h3>2. I don’t have a million dollars.</h3>
                            <strong class="subheading">And if I did, I would just buy a Superbowl ad.</strong>
                            <h4>Priced for Everyone</h4>
                            <p>Start a free account in minutes and start downloading records for just 75¢ each. Your price drops as low as <strong>4¢ per record</strong> when you purchase the <a href="./pricing">Unlimited</a> plan. Spend the rest of your money on something you love, like improving your product.</p>
                            <h4>Free Updates for Subscribers</h4>
                            <p>When we update our records, you don’t have to pay again to download! Just <strong>pay once</strong> and have access to constantly updated information. </p>
                            <h4>Unlimited Usage - Like a boss!</h4>
                            <p>Other sources make you pay each time you use their list. Some won't even give you emails at all - you have to pay them extra to send the emails to you. Once you download a record from us, you can use the contact info as many times as you'd like for as long as you have your subscription.</p>
                        </div>
                    </section>

                    <section class="post row">
                        <div class="col-xs-4 col-sm-5 text-center">
                            <img src="images/img08.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-xs-8 col-sm-7">
                            <h3>3. I don’t have all day to<br /> search for contacts.</h3>
                            <strong class="subheading">Who do you think I am? Indiana Jones?</strong>
                            <h4>So Easy a Caveman Could Do It</h4>
                            <p>Custom search based on the types of officials you need and <strong>get an Excel spreadsheet</strong> that you can easily use to contact officials however you choose.</p>
                            <h4>Stupid Fast</h4>
                            <p>Get records instantly at the click of a button.<br /> Adjust parameters on the fly and <strong>receive results fast</strong>.</p>
                        </div>
                    </section>

                    <section class="post row last-post">
                        <div class="col-xs-4 col-sm-5 text-center">
                            <img src="images/img09.jpg" class="responsive" alt="" />
                        </div>
                        <div class="col-xs-8 col-sm-7">
                            <h3>4. I want to make my own list the way I want it!</h3>
                            <h4>DIY Customizable</h4>
                            <p>Search only for the officials you need. The Power Almanac allows you to focus your seach to <em><strong>market to your exact audience</strong></em>. You don’t pay for records you don’t need.</p>
                            <h4>Chock Full Comprehensive</h4>
                            <p>We have contact info for over <em><strong>220,000</strong></em> local government officials including the Mayor, Council Members, Public Works Director, Police Chief, Fire Chief, Head of Finance, Head of Purchasing, City/County Manager, Head Clerk &amp; Head of IT for <em><strong>97%</strong></em> of cities over 1000 people.</p>
                        </div>
                    </section>

                    <?=$SubFooterSection?>
                </div>
            </div>




<?php include("inc/footer.php"); ?>
