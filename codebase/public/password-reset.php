<?php
include("inc/config.php");
$title = 'Privacy Policy | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");

$error = "";
$success = false;

// Make sure the Link is structured properly:
if(!isset($_GET['id']) or !isset($_GET['time']) or (!isset($_GET['enc'])))
{
	$error = "This link is invalid.";
	$_GET['id'] = 0;
	$_GET['time'] = 0;
	$_GET['enc'] = 0;
}

// Connect to Database
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// Sanitize values
$id = $_GET['id'] + 0;
$time = $_GET['time'] + 0;
$enc = $_GET['enc'];

$fetch = $pdo->query(sprintf("SELECT * FROM registeredusers WHERE RegUser_ID='%s' LIMIT 1", escape($id)));
$fetch = $fetch->fetch(PDO::FETCH_ASSOC);

// Don't change this salt!! Used on forgot-password-confirm.php!
// because we've used the user id to grab the user and added the pwd hash into the hash here, once the link has
// been successfully used once, it won't work again.
$thisGeneratedHash = sha1("{$time}#$(*HFVDJSIVH(*&$#YJTI*&^YRDEIF{$id}{$fetch['User_Password']}");

// Make sure that the link matches:
if($enc != $thisGeneratedHash) {
	$error = "This link is invalid.";
}

// Make sure that the time was not past 24 hours
if($time <= time() - (3600 * 24)) {
	$error = "This link is no longer valid.";
}

// Check if there was a reset posted:
if($error == "" && isset($_POST['submit']) && isset($_POST['newpass']) && isset($_POST['confirmpass']))
{
	if($_POST['newpass'] == $_POST['confirmpass'])
	{
		// Run a direct query then:
		$fetch = $pdo->query(sprintf("SELECT RegUser_ID, User_Email, User_Password FROM registeredusers WHERE RegUser_ID='%s' LIMIT 1", escape($id)));
		$fetch = $fetch->fetchAll();
		
		if($fetch != array()) {
			// Prepare the Reset Password
			require_once("./class/Security.php");
			
			$resetPass = Security::setPassword($_POST['newpass']);
			// End Password Security
			$pdo->query(sprintf("UPDATE registeredusers SET User_Password='%s' WHERE RegUser_ID='%s' LIMIT 1", escape($resetPass), escape($id)));
			
			$success = true;
			
			//header("Location: ./password-reset.php?fail=true"); exit;
		}	
	}
}

// http://poweralmanac.test/password-reset.php?id=40&time=1371739201&enc=e37802c9dcd1ce337266e813f7eb0ee78dce6aab

?>
            <div class="intro">
                <div class="intro-holder">
                    <h1>Password Reset</h1>
                </div>
            </div>
            <div class="main-holder layout-n">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame">
						<?php
							if($success == true)
							{
								echo '
							<p><span class="strengthen">Reset Successful!</span></p>
							<p><span class="strengthen">You have successfully reset your password!</span></p>
							<ul class="no-style-type">
								<li>You may now continue to the <a href="./login">Login Page</a>!</li>
							</ul>';
							}
							elseif($error == "")
							{
							    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
							        //because success is false, and error is blank, and post, password reset had an error
                                    echo '<p><span class="strengthen">Passwords don\'t match</span></p>';
                                }
								echo '
							<p><span class="strengthen">Reset Your Password</span></p>
							<form action="./password-reset?id=' . $id . '&time=' . $time . '&enc=' . $enc . '" method="post">
							<ul class="no-style-type">
								<li>New Password: <input type="password" name="newpass" value="" /></li>
								<li>Confirm New Password: <input type="password" name="confirmpass" value="" /></li>
								<li><input type="submit" name="submit" value="Submit" /></li>
							</ul>
							</form>';
							}
							else
							{
								echo '
							<p><span class="strengthen">' . $error . '</span></p>';
							}
						?>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>            