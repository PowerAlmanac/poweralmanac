<?php

include("inc/config.php");
$title = 'Subscription Authorization Success';

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="5" cellspacing="5" width="99%" align="center" border="0">
            <tr>
                <td>
                <h3>Transaction via PayPal Successful</h3>
                <p>
                <b>Please check your email for a transaction receipt from PayPal. (Not there?  Be sure to check your spam/junk folders too.)</b>
                </p>
                <table cellpadding="5" cellspacing="1" border="0" width="95%">
                <tr bgcolor="#BFCDDB">
                	<td valign="top" width="35%">
					<p><b>If you just signed up for a <u>NEW SUBSCRIPTION</u>:</b></p>
					</td>
                    <td valign="top" width="65%">
                    <p>
					<ol>
                    <li class="textUL"><b>ACTIVATE YOUR ACCOUNT</b> - Check your email for the custom URL to activate your account. Not there?  Be sure to check your spam/junk folders too. (Didn't get the activation email?  <a href="PA-ResendActivation.php">Click here</a> to resend it.)</li>
                    <br><br>
					<li class="textUL"><b>LOG-IN</b> - Enjoy the benefits of your new Power subscription!</li>
					</ol>
                    </p>
                    </td>
                </tr>
                <tr bgcolor="#80BFFF">
                	<td valign="top">
					<p><b>If you just <u>UPGRADED</u> your subscription:</b></p>
					</td>
                    <td valign="top">
                    <p>
					<ul class="textUL">
                    Your upgraded subscription and additional download credits should be reflected on your "<b>My Account</b>" page within the next 1-2 minutes.
                    </ul>
                    </p>
					</td>
                </tr>
                </table>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
            //include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
