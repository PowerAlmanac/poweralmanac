<?php
include("inc/config.php");
$title = 'Questions about our government contact information database | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder center">
                    <h1>Welcome!</h1>
                    <?php
	                    if (isset($_SESSION['login_message'])) {
	                        $errorMessage = $_SESSION['login_message'];
	                        if ($errorMessage != '') {
	                            echo("$errorMessage");
	                        }
	                    } else {
	                        $_SESSION['login_message'] = '';
	                    }
	                ?>
                     <div class="black-holder clearfix">
	                	<form action="PA-ProcessLogIn.php" method="post" name="applyform">
	                	<label>
	                		Email
	                		<input type="email" name="eMail" class="form-control" value="<?php // set email address if cookie-saved or stored for some other reason
		                		if(isset($_SESSION['notAgreed']) && $_SESSION['notAgreed'] == '1')
		                		{
		                			echo $_SESSION['user_email'];
		                		}
		                		else if(isset($_COOKIE['userlogin']) == true)
		                		{
		                			echo $_COOKIE['userlogin'];
		                		}
		                		?>">
	                	</label>
	                	<label>
	                		Password<a href="./forgot-password">Forgot your password?</a> 
	                		<input type="password" name="passWord" class="form-control" value="<?php //restore password only for redirect back on TOS agreement thing
	                			if(isset($_SESSION['notAgreed']) && $_SESSION['notAgreed'] == '1')
	                			{
	                				echo $_SESSION['tmppwd'];
	                				unset($_SESSION['tmppwd']);
	                			}
	                		?>">
	                	</label>
	                	<label class="checkbox">
	                		<input type="checkbox" name="rememberMe" value="true" <?php if(isset($_COOKIE['userlogin']) == true){echo 'checked="checked"';}?> >Remember me
	                	</label>
	                	<div id="login-or-register">
	                		<?php if(isset($_SESSION['notAgreed']) && $_SESSION['notAgreed'] == '1'){
	                		echo("<div><p style='color: red; font-size: 12px; line-height: 16px'>You have not agreed to the Terms of Service yet. Please check the box to verify that you have read and agree to the
	                			 Terms of Service</p>
	                			 <label class='checkbox'><input type='checkbox' name='AgreeToTOS' value='1'>I agree to the <a target='_blank' style='float: none; margin-left: 0' href='terms-of-service.php'>Terms of Service</a></label></div>");
	                	}; ?>
	                		<input type="submit" value="Log In" class="btn btn-default"><span> or <a href="./register">Create a New Account</a></span>
	                	</div>
	  
	                </form>
	                </div>
                </div>
            </div>

<?php include("inc/footer.php"); ?>