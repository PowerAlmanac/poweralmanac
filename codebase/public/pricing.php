<?php
define("TITLE", "Pricing and features of Power Almanac");
include("inc/config.php");
$title = 'Local Gov Directory | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");
?>

            <div class="intro">
                <div class="intro-holder container">
                    <h1>Pricing</h1>
                </div>
            </div>
            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box">

                    <div class="row m-b-20">
                        <div class="col-md-12">
                            <h3>YES, EVERY PLAN INCLUDES<sup><small>^</small></sup></h3>
                        </div>
                    </div>

                    <div class="row m-b-10">
                        <div class="col-md-12 pricing-plans-include">
                            <ul>
                                <li>Access to the largest &amp; most accurate database of local government decision-makers</li>
                                <li>100% decision-makers, filtered based on your criteria</li>
                                <li>Unlimited use of records <small>(including permission to upload to your CRM)</small></li>
                                <li>Unlimited internal users at your company</li>
                                <li>100% of records date-stamped <small>(proof of verification w/in 6 months)</small></li>
                                <li>1-minute downloading of records into excel/CSV files</li>
                                <li>Use of email addresses <small>(in addition to names/titles/phones/postal addresses)</small></li>
                            </ul>
                        </div>
                    </div>

                    <div class="pricing-table-2019">
                        <!-- pricing box -->
                        <div class="row">
                            <div class="col-md-4 pricing-plan">
                                <div class="pricing-header rounded-top">Power 15</small></div>
                                <ul class="pricing-features">
                                    <li>
                                        15,000 records
                                    </li>
                                    <li>
                                        <strong class="blue-highlight">$375 per month*</strong>
                                    </li>
                                    <li>
                                        +30&cent; per extra record (above&nbsp;15,000)
                                    </li>
                                </ul>
                                <div class="pricing-footer">
                                    <a href="/checkout?subscription=12" class="btn btn-primary btn-lg btn-block" role="button">Sign Up for Power 15</a>
                                </div>
                            </div>

                            <div class="col-md-4 pricing-plan">
                                <div class="pricing-header rounded-top">Power 75</div>
                                <ul class="pricing-features">
                                    <li>
                                        75,000 records
                                    </li>
                                    <li>
                                        <strong class="blue-highlight">$875 per month*</strong>
                                    </li>
                                    <li>
                                        +14&cent; per extra record (above&nbsp;75,000)
                                    </li>
                                </ul>
                                <div class="pricing-footer ">
                                    <a href="/checkout?subscription=13" class="btn btn-primary btn-lg btn-block" role="button">Sign Up for Power 75</a>
                                </div>
                            </div>

                            <div class="col-md-4 pricing-plan">
                                <div class="pricing-header rounded-top">Power Max</div>
                                <ul class="pricing-features">
                                    <li>
                                        all 223,000+ records
                                    </li>
                                    <li>
                                        <strong class="blue-highlight">$1,225 per month*</strong>
                                    </li>
                                    <li>
                                        you already get all the records!
                                    </li>
                                </ul>
                                <div class="pricing-footer">
                                    <a href="/checkout?subscription=10" class="btn btn-primary btn-lg btn-block" role="button">Sign Up for Power Max</a>
                                </div>
                            </div>
                        </div>

                        <h1 class="pricing-choose">The FREE Plan</h1>

                        <div class="row">
                            <div class="col-md-12 pricing-plan">
                                <div class="pricing-header rounded-top">Power Entry</div>
                                <ul class="pricing-features">
                                    <li>
                                        Create account for free
                                    </li>
                                    <li>
                                        90&cent; per record - on demand
                                    </li>
                                    <li>
                                        get your 1st 100 records on us! (by&nbsp;calling&nbsp;us&nbsp;at&nbsp;650-539-2342<sup><small>**</small></sup>)
                                    </li>
                                </ul>
                                <div class="pricing-footer">
                                    <a href="/checkout?subscription=11" class="btn btn-primary btn-lg btn-block" role="button">Sign Up for Power Entry (it's free!)</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>^ during the 1-year subscription term</div>
                    <div>* billed annually</div>
                    <div>** After you sign up for your free Power Entry Plan, call us at 650-539-2342. We want to get to know you, and we’ll give you 100 free records in exchange for a 5-minute conversation.</div>

                    <div class="row mt-4">
                        <div class="col-md-5" style="min-height: 4rem;">
                            <a href="/contact-us" class="btn btn-primary btn-lg btn-block" role="button">Request a Demo</a>
                        </div>
                        <div class="col-md-7 text-center">
                            We'd love to talk to you.<br />
                            Call us: 650-539-2342
                        </div>
                    </div>
                </div>
            </div>      
<?php include("inc/footer.php"); ?>            
