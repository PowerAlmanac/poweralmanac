<?php

include("inc/config.php");
include("inc/protect-login.php");

$record_id = $_REQUEST['id'];

$user_email = $_SESSION['user_email'];
$RegUser_ID = $_SESSION['RegUser_ID'];

// Sanitize
$record_id = Sanitize::number($record_id);

/*
/var/www/sh/DownloadUpdates.sh "5" "bbbbb" "bbbbb"  -- "reg_id" "search name" "file name"   --> put the SAME file name and everything the SAME like SaveCurrentDownload.
u_bbbbb.zip is created
*/

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($user_email)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
$RegUser_ID = $jsonArray2a['RegUser_ID'];
$User_ActivationCode = $jsonArray2a['User_ActivationCode'];

// get file name and parse for user id and DL name
$pdo->query(sprintf("CALL GetFileName(%s,@filename)", escape($record_id))); // IN Saved_ID BIGINT, OUT FileName VARCHAR(255)
$pdoObject = $pdo->query("SELECT @filename");
$rsArray = $pdoObject->fetchAll();
$filename = $rsArray[0]['@filename'];
//echo("<p>DEBUG: filename = $filename</p>");
// strip extension
$savedname = str_replace(".zip","",$filename);
//echo("<p>DEBUG: RegUser_ID = $RegUser_ID, savedname = $savedname</p>");

$record2Charge = $_SESSION['record2Charge'];
//echo("record2Charge = $record2Charge; savedname = $savedname; RegUser_ID = $RegUser_ID"); exit;


$output = shell_exec("/bin/bash /var/www/sh/DownloadUpdates.sh \"$RegUser_ID\" \"$savedname\" \"$savedname\" ");
$output2 = shell_exec("nohup /usr/bin/mysql -uroot --host=poweralmanac.cnl6vllv6ior.us-east-1.rds.amazonaws.com --password=W4boubWgGkaPaqnmrsXmr>FY9 -N -B --database=government -e\"call SavePayDataForFullDownload2($RegUser_ID,'$savedname');\" &> /dev/null");

//echo("output = $output"); exit;

if ($output == 0) {
	// update DL count
	$record2Charge = $_SESSION['record2Charge'];
	$newReserves = $User_DL_Reserves - $record2Charge;
	
	$pdo->query(sprintf("CALL UpdateDLCount('%s',%s,'%s',@rc)", escape($user_email), escape($newReserves), escape($User_ActivationCode)));
	$pdoObject = $pdo->query("SELECT @rc");
	$rsArray = $pdoObject->fetchAll();
	$RC = $rsArray[0]['@rc'];
	
	// update DL count for sub-users only
	$User_Parent = $_SESSION['User_Parent'];
	if ($User_Parent != 0) {
		$sql ="USE " . escape($dbname);
		$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
		if (!$dbcnx) {
			print("Unable to connect to the database server at this time.\n");
		exit();
		}
		if (!@PowerAlmanac\PDb::query($sql)){
			die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
		}
		// insert record count
		$insertRecordCount_sql = sprintf("INSERT INTO userpay (UserEmail,NumRecords)
			VALUES ('%s','%s')
		", escape($user_email), escape($record2Charge));
		$result_insertRecordCount = @PowerAlmanac\PDb::query($insertRecordCount_sql);
		if (!$result_insertRecordCount) {
			die('Error writing to userpay database:' . PowerAlmanac\PDb::error());
		}
	}
	
	// download generated successfully
	header("Location: /dl/u_$savedname.zip");
	flush();
	exit;
}

header("Location: PA-MyDownloads.php");
flush();
exit;

?>
