<?php

include("inc/config.php");
$title = 'Local Government Profile';

$govtID = $_REQUEST['id'];
/*
if ($useReCaptcha == 0) {
	// skip
} else {
	require_once('recaptchalib.php');
	$resp = recaptcha_check_answer ($recaptcha_privatekey,
							$_SERVER["REMOTE_ADDR"],
							$_POST["recaptcha_challenge_field"],
							$_POST["recaptcha_response_field"]);
	
	if (!$resp->is_valid) {
		$_SESSION['captcha_message'] = "The reCAPTCHA wasn't entered correctly. Please try again.";
		header("Location: PA-RecaptchaBridgeNEW.php?id=$govtID");
		flush();
		// What happens when the CAPTCHA was entered incorrectly
		echo("<font face='Arial, Helvetica, sans-serif' size='+1'><center><p>The reCAPTCHA wasn't entered correctly. <a href='PA-RecaptchaBridge.php?id=$govtID'>Go back</a> and try it again or start another <a href='search-government.php'>search</a>.</p></center></font>");
		//die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." . "(reCAPTCHA said: " . $resp->error . ")");
		exit;
	} else {
		// Your code here to handle a successful verification
		//echo("Captcha VERIFIED!");
		$_SESSION['captcha_message'] = "";
	}
}
*/

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// get data
$pdo2 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo2->query(sprintf("CALL RecordDetails2('%s',@JSON_GOVERNMENT, @JSON_OFFICIALS)", escape($govtID)));
$pdoObject2 = $pdo2->query("SELECT @JSON_GOVERNMENT, @JSON_OFFICIALS");
$rsArray2 = $pdoObject2->fetchAll();
//print_r($rsArray2);

$json2a = $rsArray2[0]['@JSON_GOVERNMENT'];
$jsonGovernmentArray = json_decode($json2a, TRUE);
//print_r($jsonGovernmentArray);

$json2b = $rsArray2[0]['@JSON_OFFICIALS'];
// test
//$json2b = '[{"Official_ID":"201100800800000-108-5C2FF0","Government_ID":"01100800800000","Role":"Head of Fire Protection Services","Role_exists":"No","First_Name":"None","Last_Name":"None","Government_Title":"None","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"has no email","Email_Type":"No Email","Special_Notes":"n/a"},{"Official_ID":"201100800800000-109-CC9C09","Government_ID":"01100800800000","Role":"Clerk","Role_exists":"Yes","First_Name":"Ken","Last_Name":"Joiner","Government_Title":"County Administrator ","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-102-A25FF7","Government_ID":"01100800800000","Role":"Governing Board Member","Role_exists":"Yes","First_Name":"Rudy","Last_Name":"Abbott","Government_Title":"Commissioner District 5","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-102-070DF4","Government_ID":"01100800800000","Role":"Governing Board Member","Role_exists":"Yes","First_Name":"James","Last_Name":"Henderson","Government_Title":"Commissioner District 3 ","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-102-944EC4","Government_ID":"01100800800000","Role":"Governing Board Member","Role_exists":"Yes","First_Name":"JD","Last_Name":"Hess","Government_Title":"Commissioner District 4 ","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-102-D8C16D","Government_ID":"01100800800000","Role":"Governing Board Member","Role_exists":"Yes","First_Name":"Tim","Last_Name":"Hodges","Government_Title":"Commissioner District 2","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-104-CC9C09","Government_ID":"01100800800000","Role":"Head of Finance/Budgeting","Role_exists":"Yes","First_Name":"Ken","Last_Name":"Joiner","Government_Title":"County Administrator ","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-107-A7C2E8","Government_ID":"01100800800000","Role":"Head of Law Enforcement","Role_exists":"Yes","First_Name":"Larry","Last_Name":"Amberson","Government_Title":"Sheriff","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-106-95B8FC","Government_ID":"01100800800000","Role":"Head of Public Works","Role_exists":"Yes","First_Name":"Brian","Last_Name":"Rosenbalm","Government_Title":"County Engineer","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-105-CC9C09","Government_ID":"01100800800000","Role":"Head of Purchasing/Procurement","Role_exists":"Yes","First_Name":"Ken","Last_Name":"Joiner","Government_Title":"County Administrator ","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-103-CC9C09","Government_ID":"01100800800000","Role":"Top Appointed Official","Role_exists":"Yes","First_Name":"Ken","Last_Name":"Joiner","Government_Title":"County Administrator ","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"},{"Official_ID":"201100800800000-101-64E7F5","Government_ID":"01100800800000","Role":"Top Elected Official","Role_exists":"Yes","First_Name":"James","Last_Name":"Dunn","Government_Title":"Chairman, County Commission","Elected":"n/a","Length_of_Elected_Ter":"n/a","Term_Month_End_Date":"n/a","Term_Year_End_Date":"n/a","Part_of_Governing_Board":"Yes","Mailing_Street_Box":"1702 Noble Street","Mailing_Suite_Number":"","Mailing_City":"Anniston","Mailing_State":"AL","Mailng_Zip_Code":"36201","Phone_Number":"256-241-2800","Phone_Number_Ext":"","Email_Address":"ccc@calhouncounty.org","Email_Type":"Shared","Special_Notes":"n/a"}]';
//print_r($json2b);

$jsonOfficialsArray = json_decode($json2b, TRUE);
//print_r($jsonOfficialsArray);

//exit;

/*
SAN BERNARDINO
    [Government_Place_Name] => SAN BERNARDINO
    [Government_Type_Name] => COUNTY
    [Government_Web_Address] => http://www.co.san-bernardino.ca.us
    [Government_PhoneNumber] => 909-387-5418
    [Governments_Phone_Number_EXT] => 
    [Dept_Name_for_phone_number] => SAN BERNARDINO COUNTY OF COUNTY ADMINISTRATIVE OFFICE
    [Government_Type] => county
    [Form_of_Government] => 2
    [Governing_Body_Name] => 0
    [County_Name] => 
    [Fiscal_Year_End_Date] => 0630
    [Original_Incorporation_Date] => 1853
    [State_Code] => 05
    [Type_Code] => 1
    [County_Code] => 036
    [Unit_Code] => 036
    [Special_Notes] => text - paragraph
    [Best_person_to_talk] => text - name
    [Phonenoofbestpersontotalk] => phone number
    [Best_pages_URL] => URL link
    [Other_Notes] => text - paragraph
    [FIPS_State] => 6
    [FIPS_County] => 71
    [FIPS_Place] => 0
    [Population] => 2030009
    [Mailing_Address_Street_Box] => 481 4TH ST
    [Suite_Number] => 
    [City] => HOLLISTER
    [Zip_Code] => 95023
    [State] => CA
    [GEO_ID_CY1] => CO06071
	
	
	DEMOGRAPHICS DATA
	==================
	POPULATION
    [TotalPopulation] => 2030009.00
    [Density] => 101
    [PopulationChangeSince2000] => 19.0
	
	GENDER
    [MalePercent] => 50.24
    [FemalePercent] => 49.76
	
	RACIAL
    [WhitePercent] => 44.27
    [BlackPercent] => 5.02
    [HispanicPercent] => 32.74
    [AsianPercent] => 3.99
    [OtherPercent] => 13.98
	
	AGE
    [MedianAge] => 30.71
    [Under20YearsOldPercent] => 32.09
    [Above70YearsOldPercent] => 6.10
	
	EDUCATION
    [NotGraduatedFromHighSchoolPercent] => 21.98
    [CollegeDegreeOrMorePercent] => 18.19
    
	INCOME
	[PerCapitaIncome] => 22766.00
    [MedianHouseholdIncome] => 55611.00
    [MedianDisposableIncome] => 46505.00
    [ChangeInHouseholdIncomeSince2000] => 46.00
    [AverageHouseholdNetWorth] => 241752.00
    [AverageHouseholdSize] => 3.29
    [HouseholdsWithChildrenPercent] => 46.67
    [MedianYearsInResidence] => 3.45
    [AnnualResidentialTurnover] => 17.56
	
	EMPLOYMENT
    [TotalWorkers] => 1325127.00
    [WhiteCollarPercent] => 76.31
    [BlueCollarPercent] => 23.69
    [WorkersAsPercentOfTotalPopulation] => 65.28
	[UnemploymentRate] => 0.00
	
	
    [TakePublicTransitPercent] => 2.00
    [WorkAtHomePercent] => 3.00
    [MedianTravelTimeToWorK] => 22.35
	
	
    [WeatherRisK] => 11.00
    [SalesTaxRate] => 8.7500
    
	
	[DistanceToClosestMajorAirport] => 0.00
    [AirPollutionIndex] => 101.00
	

	BUDGET DATA
	============
	
    [Total] => 3321597
    [Capital] => 119325
    [Operating] => 3202272
    [Salaries_and_Wages] => 1215638
    [Suppliers] => 2805483

    [PUBLIC_SAFETY] => 698375
    [Corrections] => 227366
    [Fire_Protection] => 36818
    [Judicial_and_Legal] => 171348
    [Police] => 262843
	
    [PUBLIC_WELFARE] => 768038
    [Assistance_and_Subsidies] => 399622
    [Welfare_Program_Administration] => 368416
	
    [HEALTH] => 689775
    [Health_Services] => 281126
    [Hospitals] => 408649
	
    [UTILITIES] => 87009
    [Electricity] => 336
    [Gas] => 0
    [Sewerage] => 2917
    [Solid_Waste_Management] => 74125
    [Water] => 9631
	
    [TRANSPORTATION] => 115213
    [Airports] => 9854
    [Parking] => 0
    [Ports_and_Waterways] => 0
    [Public_Transit] => 0
    [Roads_and_Highways] => 105359
	
    [LEISURE] => 38305
    [Libraries] => 15582
    [Parks_and_Recreation] => 22723
	
    [FINANCE] => 174464
    [Financial_Administration] => 57972
    [Interest_on_Debt] => 116492
	
    [MISCELLANEOUS] => 750418
    [Education] => 396880
    [General_Administration] => 66843
    [Housing_and_Community_Development] => 7601
    [Liquor_Stores] => 0
    [Natural_Resources] => 76811
    [Other_Miscellaneous] => 202283
    [Population_2007] => 2061649


[Govt_Off_ID] => 688 
[Government_ID] => 20301004200000 
[Role] => Clerk 
[Role_exists] => Yes 
[First_Name] => Dawn 
[Last_Name] => Adams 
[Government_Title] => Town Clerk 
[Elected] => No 
[Length_of_Elected_Ter] => No Term 
[Term_Month_End_Date] => No Term 
[Term_Year_End_Date] => No Term 
[Part_of_Governing_Board] => No 
[Mailing_Street_Box] => 62 Davenport St. 
[Mailing_Suite_Number] => 
[Mailing_City] => MILFORD 
[Mailing_State] => Maine 
[Mailng_Zip_Code] => 4461 
[Phone_Number] => 207-827-2286 
[Phone_Number_Ext] => 
[Email_Address] => clerk@milfordmaine.org 
[Email_Type] => Individual 
[Special_Notes] => text-paragraph 


*/
function StateLookUp($stateAbbr) { 
	$read_sql = sprintf("SELECT * FROM codes_table
		WHERE code_code = '%s' AND table_name = 'STATES'
		LIMIT 1
	", escape($stateAbbr));
	$result_read = @PowerAlmanac\PDb::query($read_sql);
	if (!$result_read) {
		die('Error reading from codes_table database:' . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_read);
	$stateDesc = $onerow['code_desc'];
	$stateName = ucwords(strtolower($stateDesc));
	return $stateName;
}

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="0" cellspacing="0" border="0" align="center" width="99%">
            <tr>
                <td valign="top" align="center">
                <br>
                <table cellpadding="5" cellspacing="5" border="0" align="center" width="97%" bgcolor="#FFFFFF">
                <tr>
                    <td colspan="2">
                    <table cellpadding="5" cellspacing="5" border="0" width="100%" align="center" bgcolor="#FFFFFF">
                    <?php
					
                        $Government_Place_Name = ucwords(strtolower($jsonGovernmentArray['Government_Place_Name']));
                        $Government_Type_Name = ucwords(strtolower($jsonGovernmentArray['Government_Type_Name']));
                        $Government_Web_Address = $jsonGovernmentArray['Government_Web_Address'];
                        $Government_PhoneNumber = $jsonGovernmentArray['Government_PhoneNumber'];
						
						// parse phone number
						$areacode = substr($Government_PhoneNumber,0,3); 
						$phone1 = substr($Government_PhoneNumber,3,3); 
						$phone2 = substr($Government_PhoneNumber,6,4); 
						$ParsedPhoneNumber = '(' . $areacode . ') ' . $phone1 . '-' . $phone2;
						
						// unfix? 7/2
						//$ParsedPhoneNumber = $Government_PhoneNumber;
						
                        $Governments_Phone_Number_EXT = $jsonGovernmentArray['Governments_Phone_Number_EXT'];
                        $Dept_Name_for_phone_number = $jsonGovernmentArray['Dept_Name_for_phone_number'];
                        $Government_Type = ucwords($jsonGovernmentArray['Government_Type']);
                        $Form_of_Government = $jsonGovernmentArray['Form_of_Government'];
                        
                        $Governing_Body_Name = $jsonGovernmentArray['Governing_Body_Name'];
                        $County_Name = ucwords(strtolower($jsonGovernmentArray['County_Name']));
						if ($County_Name == '') {
								$County_Name = 'n.a.';
						}
                        $Fiscal_Year_End_Date = $jsonGovernmentArray['Fiscal_Year_End_Date'];
                        $Original_Incorporation_Date = $jsonGovernmentArray['Original_Incorporation_Date'];
						if ($Original_Incorporation_Date == 0) {
							$Original_Incorporation_Date = 'unavailable';
						}
						
                        $Population = number_format($jsonGovernmentArray['Population']);
                        
                        //$State_Code = $jsonGovernmentArray['State_Code'];
						$StateAbbr = $jsonGovernmentArray['State'];
						$State = StateLookUp($StateAbbr);
                        $Type_Code = $jsonGovernmentArray['Type_Code'];
                        $County_Code = $jsonGovernmentArray['County_Code'];
						
						// physical address
                        $PhysAddress1 = ucwords(strtolower($jsonGovernmentArray['Mailing_Address_Street_Box']));
						$PhysAddress2 = ucwords(strtolower($jsonGovernmentArray['Suite_Number']));
                        $PhysCity = ucwords(strtolower($jsonGovernmentArray['City']));
                        $PhysState = $jsonGovernmentArray['State'];
                        $PhysZipCode = sprintf("%05s",$jsonGovernmentArray['Zip_Code']);
						
						// spending/budget
						$Total = number_format($jsonGovernmentArray['Total']*1000);
						$PUBLIC_SAFETY = $jsonGovernmentArray['PUBLIC_SAFETY'];
						$PUBLIC_WELFARE = $jsonGovernmentArray['PUBLIC_WELFARE'];
						$HEALTH = $jsonGovernmentArray['HEALTH'];
						$UTILITIES = $jsonGovernmentArray['UTILITIES'];
						$TRANSPORTATION = $jsonGovernmentArray['TRANSPORTATION'];
						$LEISURE = $jsonGovernmentArray['LEISURE'];
						$FINANCE = $jsonGovernmentArray['FINANCE'];
						$MISCELLANEOUS = $jsonGovernmentArray['MISCELLANEOUS'];
						
						// demographics - pie charts
						// GENDER
						$MalePercent = $jsonGovernmentArray['MalePercent'];
						$FemalePercent = $jsonGovernmentArray['FemalePercent'];
						
						//RACIAL
						$WhitePercent = $jsonGovernmentArray['WhitePercent'];
						$BlackPercent = $jsonGovernmentArray['BlackPercent'];
						//$HispanicPercent = $jsonGovernmentArray['HispanicPercent'];
						$AsianPercent = $jsonGovernmentArray['AsianPercent'];
						$OtherPercent = 100.0 - ($WhitePercent + $BlackPercent + $AsianPercent);
						
						//EMPLOYMENT
						$WhiteCollarPercent = $jsonGovernmentArray['WhiteCollarPercent'];
						$BlueCollarPercent = $jsonGovernmentArray['BlueCollarPercent'];
						
						//EDUCATION
						$NotGraduatedFromHighSchoolPercent = $jsonGovernmentArray['NotGraduatedFromHighSchoolPercent'];
						$CollegeDegreeOrMorePercent = $jsonGovernmentArray['CollegeDegreeOrMorePercent'];
						$HighSchoolDegree = 100.0 - ($CollegeDegreeOrMorePercent + $NotGraduatedFromHighSchoolPercent);
						
						//AGE
						$Under20YearsOldPercent = $jsonGovernmentArray['Under20YearsOldPercent'];
						$Above70YearsOldPercent = $jsonGovernmentArray['Above70YearsOldPercent'];
						$TwentyToSixtyNine = 100.0 - ($Under20YearsOldPercent + $Above70YearsOldPercent);

						// demographics - bar graphs
						$Population = number_format($jsonGovernmentArray['Population']);
						$Density = $jsonGovernmentArray['Density'];
						$PopulationChangeSince2000 = $jsonGovernmentArray['PopulationChangeSince2000'];
						$PerCapitaIncome = number_format($jsonGovernmentArray['PerCapitaIncome']);
						$MedianAge = $jsonGovernmentArray['MedianAge'];
						$AverageHouseholdSize = $jsonGovernmentArray['AverageHouseholdSize'];
						$NotGraduatedFromHighSchoolPercent = $jsonGovernmentArray['NotGraduatedFromHighSchoolPercent'];	
						$CollegeDegreeOrMorePercent = $jsonGovernmentArray['CollegeDegreeOrMorePercent'];	
						
						// calculate per capital spend
						if ($jsonGovernmentArray['Population_2007'] != 0) {
							$PerCapitaSpend = number_format($jsonGovernmentArray['Total']*1000/$jsonGovernmentArray['Population_2007']);
						} else {
							$PerCapitaSpend = 'Not Available';
						}
										
                    ?>
                    <tr>
                        <td colspan="2" align="right">
                        <p align="center" class='profileHeaders'>
                        PROFILE: <?php echo($Government_Place_Name); ?> <?php echo($Government_Type_Name); ?> (<?php echo($State); ?>)
                        </p>
                        <p>
                        <a href='search-government.php'>Find another government</a>
                        </p>
                        </td>
                    </tr>
                    <tr>
                    	<td width="50%" valign="top">
                        <?php
                        echo('<p><img src="images/profile-govt.png" width="250" height="50" hspace="5" vspace="0" border="0" align="absmiddle" /><br />');
                        //echo("Name: $Government_Place_Name $Government_Type_Name<br />");
                        echo("Government type: $Government_Type<br />");
                        //echo("County location: $PhysState $County_Name<br />");
						echo("County location: $County_Name<br />");
                        //echo("Population: $Population<br />");
                        echo("Year Incorporated: $Original_Incorporation_Date<br />");
						echo("Total Population: $Population");
						echo("</p>");
						?>
                        </td>
                    	<td width="50%" valign="top">
                        <?php
						// removed 7/2 per Ron
						/*
                        echo('<p><img src="images/profile-contact.png" width="250" height="50" hspace="5" vspace="0" border="0" align="absmiddle" /><br />');
                        echo("Website: <a href='$Government_Web_Address' target='_blank'>$Government_Web_Address</a><br />");
                        echo("Address: $PhysAddress1 $PhysAddress2, ");
                        echo("$PhysCity, $PhysState $PhysZipCode<br />");
						echo("Phone: $ParsedPhoneNumber<br />");
						if ($Governments_Phone_Number_EXT != '') {
                        	echo("Extension: $Governments_Phone_Number_EXT<br />");
						}
						echo("</p>");
						*/
						?>
                        </td> 
                    </tr>
                    <tr>
                    	<td colspan="2">&nbsp;
                        
                        </td>
                    </tr>
                    <tr>
                    	<td width="50%" valign="top">
                        <?php
						echo('<p><img src="images/profile-charts.png" width="250" height="50" hspace="5" vspace="0" border="0" align="absmiddle" /></p>');
						echo("<p><b>Total Government Spend:</b> $$Total<br>");
						echo("<b>Per Capita Spend:</b> $$PerCapitaSpend</p>");
						echo("<img src='Spending-3DPie.php?city=$PhysCity&state=$PhysState&ps=$PUBLIC_SAFETY&pw=$PUBLIC_WELFARE&h=$HEALTH&u=$UTILITIES&t=$TRANSPORTATION&l=$LEISURE&f=$FINANCE&m=$MISCELLANEOUS' border='0' align='absmiddle' />");
                        //echo("<p>DEBUG: city=$PhysCity&state=$PhysState&ps=$PUBLIC_SAFETY&pw=$PUBLIC_WELFARE&h=$HEALTH&u=$UTILITIES&t=$TRANSPORTATION&l=$LEISURE&f=$FINANCE&m=$MISCELLANEOUS</p>");
						?>
                        </td>
                    	<td width="50%" valign="top">
                        <?php
						/*
                        echo('<p><img src="images/profile-demographics.png" width="250" height="50" hspace="5" vspace="0" border="0" align="absmiddle" /></p>');
                        echo("<p><b>Total Population:</b> $Population<br />");
                        echo("<b>Population Change since 2000:</b> $PopulationChangeSince2000%</p>");
                        echo("<img src='Demographics-3DPie.php?city=$PhysCity&state=$PhysState&g_m=$MalePercent&g_f=$FemalePercent&r_w=$WhitePercent&r_b=$BlackPercent&r_a=$AsianPercent&r_o=$OtherPercent&e_w=$WhiteCollarPercent&e_b=$BlueCollarPercent&ed_nhs=$NotGraduatedFromHighSchoolPercent&ed_hs=$HighSchoolDegree&ed_c=$CollegeDegreeOrMorePercent&a_u20=$Under20YearsOldPercent&a_o70=$Above70YearsOldPercent&a_other=$TwentyToSixtyNine' border='0' align='absmiddle' />");
                        //echo("<p>DEBUG: city=$PhysCity&state=$PhysState&g_m=$MalePercent&g_f=$FemalePercent&r_w=$WhitePercent&r_b=$BlackPercent&r_h=$HispanicPercent&r_a=$AsianPercent&r_o=$OtherPercent&e_w=$WhiteCollarPercent&e_b=$BlueCollarPercent</p>");
                        //echo("Per Capita Income: $$PerCapitaIncome<br />");
                        //echo("Median Age: $MedianAge<br />");
                        //echo("Average Household Size: $AverageHouseholdSize<br />");
                        //echo("Not Graduated From High School: $NotGraduatedFromHighSchoolPercent%<br />");
						//echo("College Degree Or More: $CollegeDegreeOrMorePercent%</p>");
						*/
						?>
                        </td> 
                    </tr>
                    <tr>
                        <td colspan="2">
                        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF" style="border: 1px solid #000000;">
                        <?php
                        
                        echo('<tr><td colspan="7" align="right"><p><img src="images/profile-officials.png" width="250" height="50" hspace="5" vspace="0" border="0" align="left" /><br><img src="images/checkmark.png" alt="Available in the Power Almanac" width="15" height="15" hspace="0" vspace="0" border="0" align="absbottom"> Available in the Power Almanac</p></td></tr>');
                        echo('<tr bgcolor="#7591AE"><td><b>Unique Record ID</b></td><td><b>Name</b></td><td><b>Role</b></td><td><b>Title</b></td><td><img src="images/mail.png" alt="email address available" width="16" height="16" hspace="0" vspace="0" border="0" align="absmiddle" /></td><td><img src="images/phone_1.png" alt="phone available" width="16" height="16" hspace="0" vspace="0" border="0" align="absmiddle" /></td><td><img src="images/home.png" alt="mailing address available" width="16" height="16" hspace="0" vspace="0" border="0" align="absmiddle" /></td></tr>');
                        
                        $arrayCount = count($jsonOfficialsArray);
                        //echo("<p>arrayCount = $arrayCount</p>");
						$governmentOfficialsArray = array();
                        if ($arrayCount != 0) {
						  $rowCtr = 0;
                          foreach ($jsonOfficialsArray as $item)
                          {
                              foreach ($item as $key => $value) {
                                  //echo "Key $key: Value $value<br />";
                                  switch($key) {
                                      case 'First_Name':
                                          $fname = ucwords($value);
                                          break;
                                      case 'Last_Name':
                                          $lname = ucwords($value);
                                          break;
                                      case 'Official_ID':
                                          $Govt_Off_ID = $value;
                                          break;
                                      case 'Role':
                                          $role = $value;
                                      case 'Government_Title':
                                          $title = str_replace(" / ","/",ucwords(strtolower(str_replace("/"," / ",$value))));
                                      case 'E_mail_exists':
									  	  if ($value == '1')
										  {
											  $haveEMail = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
										  } else {
                                              $haveEMail = '';
                                          }
                                      case 'phone_exists':
                                          if ($value == '1')
										  {
                                              $havePhone = '<img src="images/checkmark.png" alt="Phone Number Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
                                          } else {
                                              $havePhone = '';
                                          }
                                      case 'Mailing_Street_Box_exists':
                                          if ($value == '1')
										  {
                                              $haveAddress = '<img src="images/checkmark.png" alt="Mailing Address Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
                                          } else {
                                              $haveAddress = '';
                                          }
                                          break;
                                      default:
                                          break;
                                  }
                              }
							  if (($fname == 'None') && ($lname == 'None')) {
								  $fname = "Role Doesn't Exist";
								  $lname = '';
							  }
							  if ($title == 'None') {
								  $title = "Role Doesn't Exist";
								  $havePhone = '';
								  $haveAddress = '';
								  $haveEMail = '';
							  }
							  //if ($role != 'Head of IT') {
								  $rowCtr = $rowCtr + 1;
								  $GovtOffIDIndex = str_replace('-',"",$Govt_Off_ID);
								  if ($rowCtr % 2 == 0) {
									  $governmentOfficialsArray[$GovtOffIDIndex] = "<tr bgcolor='#replacecolor'><td>$Govt_Off_ID</td><td>$fname $lname</td><td>$role</td><td>$title</td><td>$haveEMail</td><td>$havePhone</td><td>$haveAddress</td></tr>";
									  //echo("<tr bgcolor='#aaaaaa'><td>$Govt_Off_ID</td><td>$fname $lname</td><td>$role</td><td>$title</td><td>$haveEMail</td><td>$havePhone</td><td>$haveAddress</td></tr>");
								  } else {
									  $governmentOfficialsArray[$GovtOffIDIndex] = "<tr bgcolor='#replacecolor'><td>$Govt_Off_ID</td><td>$fname $lname</td><td>$role</td><td>$title</td><td>$haveEMail</td><td>$havePhone</td><td>$haveAddress</td></tr>";
									  //echo("<tr bgcolor='#A1B9D5'><td>$Govt_Off_ID</td><td>$fname $lname</td><td>$role</td><td>$title</td><td>$haveEMail</td><td>$havePhone</td><td>$haveAddress</td></tr>");
								  }
							  //}
                          }
						  //print_r(array_keys($governmentOfficialsArray));
						  // sort it
						  ksort($governmentOfficialsArray);
						  //print_r(array_keys($governmentOfficialsArray));
						  // spit out sorted data
						  $rowCtr = 0;
						  foreach ($governmentOfficialsArray as $key => $val) {
							  $rowCtr = $rowCtr + 1;
							  if ($rowCtr % 2 == 0) {
								  $outputHTML = str_replace("replacecolor","aaaaaa",$val);
							  } else {
								  $outputHTML = str_replace("replacecolor","A1B9D5",$val);
							  }
							  echo "$outputHTML";
						  }
                        } else {
                            echo('<tr bgcolor="#A1B9D5"><td colspan="7"><p>No government officials listed in database</p></td></tr>');
                        }
						// rest of data
                        echo('<tr><td colspan="7"><br /><p><img src="images/clip.png" alt="reference" width="16" height="16" hspace="5" vspace="0" border="0" align="left" /><b>Reference Info</b></p>');
                        echo("<p>Power Almanac Profile ID: $govtID</p>");
                        echo("<p><a href='mailto:support@poweralmanac.com'>Contact us</a> to report changes or errors. In your email, please reference either the Power Almanac profile ID above or the Unique Record ID next to each government official.</p></td></tr>");
                        
                        ?>
                        <tr>
                        	<td colspan="7">
                            <br>
                            <p>
                            <b>Notes:</b>
                            <ol>
                            <li>Financial data comes from the U.S. Census Bureau's 2007 complete survey of all state and local government finances.   The summary spending categories are created and defined by the Power Almanac. NOTE: "per capita spend" is calculated by using 2007 financial data and 2007 population data.  Information is deemed reliable but not guaranteed.</li>
                            </ol>
                            </p>
                            </td>
                        </tr>
                        </table>
                    </td>
                </tr>
                </table>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		</table>
        <br>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
        <?php
        include("inc/oldfooter.php");
        ?>
        </td>
    </tr>
    <tr>
        <td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
        
        </td>
    </tr>
    </table>
	</td>
</tr>
</table>
<br />
</body>
</html>
