<?php
define("TITLE", "Fill out this form to download a free sample");
define("DESCRIPTION", "");
define("CUSTOMHEADER", '<link media="all" rel="stylesheet" type="text/css" href="/css/govwin-free-sample.css" />');
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder">
                    <h1>&ensp;</h1>
                </div>
            </div>
            <div class="main-holder about">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame">
                        	<div class="left-col">
                        		<h2>Deltek trusts and relies on Power Almanac's data.</h2>
                        		<h3>Test it with 1,000 free records.</h3>
                        		<p>Give your business a boost with fresh leads - contact information for 1,000 Heads of Purchasing.</p>
                        		<p>Of course, they're rarely called a "head of purchasing." Many governments call them a "purchasing agent" or "purchasing manager." In some cases, the finance manager, city clerk, or some other official is responsible for procurement. <b>Regardless of the title, they are decision makers you need to reach. And regardless of the title, we know who they are.</b></p>
                        		<h3>How is Head of Purchasing defined?</h3>
                        		<p>The Head of Purchasing oversees all or most purchases of the government. They are responsible in determining methods of procurement, (such as direct purchase or bid) and selecting vendors.</p>
                        		<h3>Why should I contact Heads of Purchasing?</h3>
                        		<p> The Head of Purchasing is often the main decision maker (or at least a critical gatekeeper) when you're trying to sell something to a local government. They are also a helpful contact when you are working on a bid, and therefore good people to build relationships with.</p>
                        	</div>
                        	<div class="right-col">
                        		<div id="wufoo-mwpd0p71fbnxy1">
									Fill out my <a href="https://blendimc.wufoo.com/forms/mwpd0p71fbnxy1">online form</a>.
								</div>
								<script type="text/javascript">var mwpd0p71fbnxy1;(function(d, t) {
								var s = d.createElement(t), options = {
								'userName':'blendimc', 
								'formHash':'mwpd0p71fbnxy1', 
								'autoResize':true,
								'height':'483',
								'async':true,
								'header':'show', 
								'ssl':true};
								s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'wufoo.com/scripts/embed/form.js';
								s.onload = s.onreadystatechange = function() {
								var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
								try { mwpd0p71fbnxy1 = new WufooForm();mwpd0p71fbnxy1.initialize(options);mwpd0p71fbnxy1.display(); } catch (e) {}};
								var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
								})(document, 'script');</script>
							</div>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>            