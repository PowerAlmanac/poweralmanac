<?php

include("inc/config.php");
$title = 'Subscribe';
$sku = $_REQUEST['sku'];

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

<script type="text/javascript">
function validateRegForm()
{
	var x=document.forms["applyform"]["eMail"].value
	if (x==null || x=="")
	  {
	  alert("You must enter your EMAIL ADDRESS.");
	  return false;
	  }
	var ptr = x.search("@");
	if (ptr == -1) {
		alert("Invalid email address entered. Please try again.");
		return false;
	}
	var y=document.forms["applyform"]["passWord"].value
	if (y==null || y=="")
	  {
	  alert("You must provide a PASSWORD.");
	  return false;
	  }
	var z=document.forms["applyform"]["passWord2"].value
	if (z==null || z=="")
	  {
	  alert("You must confirm your PASSWORD.");
	  return false;
	  }
	if (y != z) 
	  {
	  alert("PASSWORDs did NOT match.Please try entering your PASSWORDs again.");
	  return false;
	  }
	var a=document.forms["applyform"]["firstName"].value
	if (a==null || a=="")
	  {
	  alert("You must provide your FIRST NAME.");
	  return false;
	  }
	var b=document.forms["applyform"]["lastName"].value
	if (b==null || b=="")
	  {
	  alert("You must provide your LAST NAME.");
	  return false;
	  }
	var c=document.forms["applyform"]["companyName"].value
	if (c==null || c=="")
	  {
	  alert("You must provide your COMPANY or BUSINESS NAME.");
	  return false;
	  }
}
</script>
    
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
           <table cellpadding="5" cellspacing="5" width="95%" align="center" border="0">
            <tr>
                <td>
                <p class="hpHeaders">Subscribe</p>
                <p>
                <b>Step 1/2 - User Information</b>
                </p>
                <form action="subscribe-step-2" method="post" name="applyform" onSubmit="return validateRegForm()">
                <input type="hidden" name="sku" value="<?php echo($sku); ?>">
                <table cellpadding="2" cellspacing="3" border="0" align="center" width="80%" bgcolor="#DDDDDD" style="border: 1px solid #000000;">
                <tr>
                    <td align="right" width="20%">
                    *<b>EMAIL:</b>
                    </td>
                    <td align="left" width="30%">
                    <input type="text" name="eMail" id="eMail" size="40" />
                    </td>
                    <td align="left" width="50%">
                    We suggest you use your work email (with your company domain). This will allow you to share your account benefits with colleagues.
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    *<b>PASSWORD:</b>
                    </td>
                    <td align="left">
                    <input type="password" name="passWord" id="passWord" size="40" />
                    </td>
                    <td align="left">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    *<b>Confirm PASSWORD:</b>
                    </td>
                    <td align="left">
                    <input type="password" name="passWord2" id="passWord2" size="40" />
                    </td>
                    <td align="left">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    *<b>FIRST NAME:</b>
                    </td>
                    <td align="left">
                    <input type="text" name="firstName" id="firstName" size="40" />
                    </td>
                    <td align="left">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    *<b>LAST NAME:</b>
                    </td>
                    <td align="left">
                    <input type="text" name="lastName" id="lastName" size="40" />
                    </td>
                    <td align="left">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    *<b>COMPANY:</b>
                    </td>
                    <td align="left">
                    <input type="text" name="companyName" id="companyName" size="40" />
                    </td>
                    <td align="left">
                    Personal account? Just enter 'Personal'.
                    </td>
                </tr>
                <tr>
                    <td align="left">
                    * all fields required
                    </td>
                    <td colspan="2" align="center">
                    <input type="submit" name="Submit" id="Submit" value="Submit" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; font-weight: heavy;" />
                    </td>
                </tr>
                </table>
                </form>
                <?php
                if (isset($_SESSION['subscribe_message'])) {
                    $errorMessage = $_SESSION['subscribe_message'];
                    if ($errorMessage != '') {
                        echo("<b><font color='#FF0000'>$errorMessage</font></b>");
                    }
                } else {
                    $_SESSION['subscribe_message'] = '';
                }
                ?>
                <div style="width:650; padding-left:200px;">
                <u>Your Privacy is Important To Us</u>
                <br>
                We use the information above to fulfill your requests for products and services and to contact you from time to time about us, our site, and our products and services. We don't sell or rent your personal information to third parties for their marketing purposes without your explicit consent.  Read our complete <a rel="shadowbox;height=800;width=800" href="./privacy-policy">Privacy Policy</a>.
                </div>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
           // include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
