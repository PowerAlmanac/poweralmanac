<?php

include("inc/config.php");
include("inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$downloadSearchID = $_REQUEST['id'];

// Sanitize
// $downloadSearchID ...

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL SavedDownloadLookup('%s',%s,@Params)", escape($user_email), escape($downloadSearchID))); // IN user_email VARCHAR(255),IN downloadID BIGINT, OUT Params TEXT
$pdoObject = $pdo->query("SELECT @Params");
$rsArray = $pdoObject->fetchAll();
$searchParams = $rsArray[0]['@Params'];
$lastSearchName = 'TBD';
/*
$lookupParams = new PAUser_Account; 
$searchParams = $lookupParams->SavedSearchLookup($savedSearchID,$user_email);
$lastSearchName = $lookupParams->User_SavedSearchName;
*/

$_SESSION['lastSearchName'] = $lastSearchName;

$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;
$_SESSION['savedSearchName'] = $lastSearchName;

$searchURL = "search-government.php";
header("Location: $searchURL");
flush();

	
?>