<?php
session_start();
//print_r($_REQUEST); exit;

$off_email = $_REQUEST['off_email'];
$off_role = $_REQUEST['off_role'];
$searchParams = $_SESSION['lastSearchParams'];

// check which boxes and set/unset session variables
if ($off_email == '1') {
	// only govt officials with emails
	$_SESSION['results_GovtOffMustHaveEmails'] = '1';
	$searchParams = $searchParams . '&off_email=govtOff_email';
} else {
	// all govt officials
	$_SESSION['results_GovtOffMustHaveEmails'] = '0';
	// reset serachParms
	$searchParams = str_replace('&off_email=govtOff_email','',$searchParams);
}

if (isset($_REQUEST['off_role'])) {
	$_SESSION['results_noGovtOffRoles'] = '1';
	$searchParams = str_replace('&off_role=govtOff_role','',$searchParams);
} else {
	$_SESSION['results_noGovtOffRoles'] = '0';
	$searchParams = $searchParams . '&off_role=govtOff_role';
}

//print_r($searchParams); exit;

// re-run search
$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;
header("Location: search-government.php");
flush();

?>
