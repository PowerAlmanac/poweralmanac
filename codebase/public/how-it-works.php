<?php
define("TITLE", "Learn about our data and methodology");
define("DESCRIPTION", "What makes us the best? Our verification, the extent of our data, our powerful filters, and great account features.");
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder container">
                    <h1>How it Works</h1>
                </div>
            </div>
            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box">
                    <div class="row m-b-20">
                        <div class="col-md-8">
                            <h2>It’s no mystery why we’re the best.</h2>
                            <strong class="subheading">Find out why. It won’t take long.</strong>
                        </div>
                    </div>

                    <div class="navbar navbar-light bg-light" id="hoverMenu">
                        <nav class="nav nav-pills flex-row">
                            <a class="flex-fill text-center nav-link active" href="#straightFacts"><span>The Facts</span></a>
                            <a class="flex-fill text-center nav-link" href="#searchTarget"><span>Search, Target &amp; Analyze</span></a>
                            <a class="flex-fill text-center nav-link" href="#downloadResults"><span>Download Results</span></a>
                            <a class="flex-fill text-center nav-link" href="#shareLove"><span>Share the Love</span></a>
                        </nav>
                        <a href="#" class="btn btn-secondary">Back to Top</a>
                    </div>

                    <!-- tabs section -->
                    <div class="tabs-section pb-5" id="straightFacts">
                        <div class="heading">
                            <h2>What Makes Us Cool</h2>
                            <strong class="subtitle">The Straight Facts</strong>
                        </div>
                        <div class="tabs-holder row">
                            <ul class="tabset col-md-5">
                                <li>
                                    <a href="#tab11" class="active">
                                        <img src="images/ico03.png" width="27" height="27" alt="" />
                                        <strong class="count">100%</strong>
                                        Re&#8209;verified
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab12">
                                        <img src="images/ico04.png" width="26" height="26" alt="" />
                                        <strong class="count">21,000</strong>
                                        Governments
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab13">
                                        <img src="images/ico05.png" width="28" height="28" alt="" />
                                        <strong class="count">220,000</strong>
                                        Officials
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab14">
                                        <img src="images/ico06.png" width="29" height="19" alt="" />
                                        <strong class="count">186,000</strong>
                                        Emails
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <!-- <li>
                                                                       <a href="#tab15">
                                                                           <img src="images/ico07.png" width="28" height="30" alt="" />
                                                                           <strong class="count">1.3 Tril.</strong>
                                                                           Fiscal Data
                                                                           <span class="arrow"></span>
                                                                       </a>
                                                                   </li> -->
                            </ul>
                            <div class="tabs-frame col-md-7">
                                <div class="tab" id="tab11">
                                    <img src="images/img12.png" width="816" height="670" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>100% re-verified 2 times per year. Data you can count on.</h3>
                                        <p>We're committed to keeping our list of local government officials as accurate as possible.  <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab12">
                                    <img src="images/img13.jpg" width="570" height="402" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>21,000 municipal, township, and county governments.</h3>
                                        <p>The Power Almanac covers over <strong>97%</strong> of the municipalities, townships, and counties in the U.S. with populations of 1,000 or more. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab13">
                                    <img src="images/img14.png" width="521" height="617" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>220,000 local government officials. No source has more.</h3>
                                        <p>You won't find a directory like this anywhere else. This is NOT a compilation of information from other, inadequate sources. We've built - <strong>from scratch</strong> - our own comprehensive and fresh list of local government officials and their contact info. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab14">
                                    <img src="images/img15.png" width="744" height="672" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>166,000 emails. Go ahead and download them.</h3>
                                        <p>We know you want emails, and we've got 'em for <strong>75%</strong> of the local government officials in the Power Almanac. (Believe it or not, some government officials still don't have email.) <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <!-- <div class="tab" id="tab15">
                                                                            <div class="txt">
                                                                                <h3>Tab #5</h3>
                                                                            </div>
                                                                        </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- tabs section -->
                    <div class="tabs-section pb-5 arrow-left" id="searchTarget">
                        <div class="heading">
                            <h2>Search, Target &amp; Analyze</h2>
                            <strong class="subtitle">You get to choose exactly who’s on your list.</strong>
                        </div>
                        <div class="tabs-holder row">
                            <div class="tabs-frame col-md-7">
                                <div class="tab" id="tab21">
                                    <img src="images/img16.png" width="736" height="357" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Roles over titles.<br /> Find officials with the right decision-making authority.</h3>
                                        <p>Not all municipalities have a mayor. Common titles include "trustee," "supervisor," "commissioner," and many more. With Power Almanac, you can target the top elected official and other decision makers, regardless of title. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab22">
                                    <img src="images/img17.png" width="475" height="531" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>7 intelligent filters. Pinpoint your outreach.</h3>
                                        <p>With the Power Almanac's "Analyze" feature, you can easily size up and assess your current and potential markets. For example, how many small cities are there in Pennsylvania vs. Ohio? <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab23">
                                    <img src="images/img18.png" width="461" height="360" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Built -in analytical tools.<br /> Size up your markets or fine-tune your searches.</h3>
                                        <p>Just because the Power Almanac lists more local governments and officials than any other source, doesn't mean you want them all. With our intelligent filters you can <strong>create a target list - in seconds</strong> - that meets your needs exactly. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab24">
                                    <img src="images/img19.png" width="480" height="402" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Actual expenditures in 41 categories for each government.</h3>
                                        <p>Perhaps you want to know <strong>how much each local government spent</strong> on what. Or maybe you want the list of every government that spent more than $1.5 million on solid waste management, or police, or libraries. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                            </div>
                            <ul class="tabset col-md-4">
                                <li>
                                    <a href="#tab21" class="active">
                                        <img src="images/ico08.png" width="28" height="33" alt="" />
                                        Roles Over Titles
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab22">
                                        <img src="images/ico09.png" width="32" height="32" alt="" />
                                        7 Intelligent Filters
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab23">
                                        <img src="images/ico10.png" width="30" height="26" alt="" />
                                        Built-in Analytics
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab24">
                                        <img src="images/ico11.png" width="32" height="27" alt="" />
                                        Actual Expenditures
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- tabs section -->
                    <div class="tabs-section pb-5" id="downloadResults">
                        <div class="heading">
                            <h2>Download Results</h2>
                            <strong class="subtitle">The moment we’ve all been waiting for.</strong>
                        </div>
                        <div class="tabs-holder row">
                            <ul class="tabset col-md-5">
                                <li>
                                    <a href="#tab31" class="active">
                                        <img src="images/ico12.png" width="23" height="30" alt="" />
                                        Immediate Downloading
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab32">
                                        <img src="images/ico13.png" width="30" height="27" alt="" />
                                        Low Cost Per Record
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab33">
                                        <img src="images/ico14.png" width="32" height="32" alt="" />
                                        Unlimited Use
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab34">
                                        <img src="images/ico15.png" width="31" height="29" alt="" />
                                        Download Protection
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab35">
                                        <img src="images/ico16.png" width="29" height="34" alt="" />
                                        Alerts
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tabs-frame col-md-7">
                                <div class="tab" id="tab31">
                                    <img src="images/img20.png" width="816" height="670" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Immediate downloading.<br /> Get the records you want - any time, any day.</h3>
                                        <p>The Power Almanac has been designed from the start as an online application. Search instantly, analyze instantly, create your lists instantly, and download records immediately. It's easy and fast, just the way you need it.
</p>
                                    </div>
                                </div>
                                <div class="tab" id="tab32">
                                    <img src="images/img21.png" width="551" height="387" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Low cost per record. </h3>
                                        <p>
                                            Get unlimited record usage for 1 year for just 14¢ per record with the <a href="/register?subscription_level=13">Power 75</a> plan, or even lower with our <a href="/register?subscription_level=10">Power Max</a> plan.
                                        </p>
                                    </div>
                                </div>
                                <div class="tab" id="tab33">
                                    <img src="images/img22.png" width="537" height="509" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Unlimited use of records. Including emails. </h3>
                                        <p>Once you download a record from the Power Almanac, you can use the contact information as many times as you'd like for as long as you remain a Power Almanac subscriber. And that goes for the emails too. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab34">
                                    <img src="images/img23.png" width="955" height="347" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Download protection.<br /> Don’t pay for updates or downloading twice.</h3>
                                        <p>If we update a record that you've already downloaded during your subscription term, go ahead and download the record again. No charge. In fact, we'll notify you whenever any record you've downloaded has been updated. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab35">
                                    <img src="images/img24.png" width="455" height="493" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Alerts. Know about important new or updated records </h3>
                                        <p>We thought you'd want to know about changes in the database, so we'll alert you - in your "My Searches" or "My Downloads" folders. You never have to waste time re-running searches to see what's changed. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tabs section -->
                    <div class="tabs-section pb-5 arrow-left" id="shareLove" style="min-height:600px;">
                        <div class="heading pt-3 pr-3">
                            <h2>Share the Love</h2>
                            <strong class="subtitle">You get to choose exactly who’s on your list.</strong>
                        </div>
                        <div class="tabs-holder row">
                            <div class="tabs-frame col-md-7">
                                <div class="tab" id="tab41">
                                    <img src="images/img25.png" width="947" height="506" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Free enterprise account.<br /> Extend account benefits to your colleagues.</h3>
                                        <p>This means you can go to "My Account" and add an unlimited number of co-workers as users on your account, giving each of them access to the same features and same low download cost as you. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                                <div class="tab" id="tab42">
                                    <img src="images/img26.png" width="907" height="434" alt="" class="decor" />
                                    <div class="txt">
                                        <h3>Saved Searches. No need to redo your work.</h3>
                                        <p>When you've set the search criteria just the way you want, and you've created the perfect list, save it. It will be waiting for you the next time you sign in, so you can pick up where you left off. <!-- <a href="#" class="link-more">Read More ...</a> --></p>
                                    </div>
                                </div>
                            </div>
                            <ul class="tabset col-md-4">
                                <li>
                                    <a href="#tab41" class="active">
                                        <img src="images/ico08.png" width="28" height="33" alt="" />
                                        Free Enterprise Account
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab42">
                                        <img src="images/ico09.png" width="32" height="32" alt="" />
                                        Save Your Searches
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row subfooter mt-4">
                        <div class="col-lg-9 col-md-8 col-sm-7 text-center text-sm-left">
                            <strong class="title">Need more convincing? Try Power Almanac</strong>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-5 text-center text-sm-right">
                            <a href="/search-government" class="btn btn-secondary" role="button">Search for Officials</a>
                        </div>
                    </div>

                </div>
            </div>
<?php include("inc/footer.php"); ?>            
