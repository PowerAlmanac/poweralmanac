<?php   

$chartSize = 'large';
$chartCityName = 'San Bernardino';
$chartStateName = 'CA';
$chartTitle = "Government Spending for $chartCityName, $chartStateName";
$chartCopyright = 'Copyright© 2012 Power Almanac';
$chartWidth = 900;
$chartHeight = 250;
$chartBorderWidth = $chartWidth - 1;
$chartBorderHeight = $chartHeight - 1;
$chartLabelSeries = "Public Safety,Public Welfare,Health,Utilities,Transportation,Leisure,Finance,Miscellaneous";
$chartDataSeries = "698375,768038,689775,87009,115213,38305,174464,750418";

 /* CAT:Pie charts */

 /* pChart library inclusions */
 include("class/pData.class.php");
 include("class/pDraw.class.php");
 include("class/pPie.class.php");
 include("class/pImage.class.php");

 /* Create and populate the pData object */
 $MyData = new pData();   
 $MyData->addPoints(array(698375,768038,689775,87009,115213,38305,174464,750418),"ScoreA");  
 $MyData->setSerieDescription("ScoreA","Application A");

 /* Define the absissa serie */
 $MyData->addPoints(array('Public Safety','Public Welfare','Health','Utilities','Transportation','Leisure','Finance','Miscellaneous'),"Labels");
 $MyData->setAbscissa("Labels");

 /* Create the pChart object */
 $myPicture = new pImage($chartWidth,$chartHeight,$MyData,TRUE);

 /* Draw a solid background */
 $Settings = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107); 
 $myPicture->drawFilledRectangle(0,0,$chartWidth,$chartHeight,$Settings);

 /* Draw a gradient overlay */
 $Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50);
 $myPicture->drawGradientArea(0,0,$chartWidth,$chartHeight,DIRECTION_VERTICAL,$Settings);
 // black header
 $myPicture->drawGradientArea(0,0,$chartWidth,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,$chartBorderWidth,$chartBorderHeight,array("R"=>0,"G"=>0,"B"=>0));

 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>10));
 $myPicture->drawText(10,18,"$chartTitle",array("R"=>255,"G"=>255,"B"=>255));
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>8));
 $myPicture->drawText(700,18,"$chartCopyright",array("R"=>255,"G"=>255,"B"=>255));
 

 /* Set the default font properties */ 
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>9,"R"=>80,"G"=>80,"B"=>80));

 /* Create the pPie object */ 
 $PieChart = new pPie($myPicture,$MyData);

 /* Define the slice color */
 $PieChart->setSliceColor(0,array("R"=>143,"G"=>197,"B"=>0));
 $PieChart->setSliceColor(1,array("R"=>97,"G"=>77,"B"=>63));
 $PieChart->setSliceColor(2,array("R"=>97,"G"=>113,"B"=>63));

 /* Draw a simple pie chart */ 
 //$PieChart->draw3DPie(120,125,array("SecondPass"=>FALSE));

 /* Draw an AA pie chart */ 
 $PieChart->draw3DPie(280,145,array("Radius"=>155,"DrawLabels"=>TRUE,"Border"=>TRUE));

 /* Enable shadow computing */ 
 $myPicture->setShadow(TRUE,array("X"=>3,"Y"=>3,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

 /* Draw a splitted pie chart */ 
 $PieChart->draw3DPie(710,145,array("Radius"=>155,"WriteValues"=>TRUE,"DataGapAngle"=>10,"DataGapRadius"=>6,"Border"=>TRUE));

 /* Write the legend */
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>7));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>20));
 //$myPicture->drawText(120,200,"Single AA pass",array("DrawBox"=>TRUE,"BoxRounded"=>TRUE,"R"=>0,"G"=>0,"B"=>0,"Align"=>TEXT_ALIGN_TOPMIDDLE));
 //$myPicture->drawText(440,200,"Extended AA pass / Splitted",array("DrawBox"=>TRUE,"BoxRounded"=>TRUE,"R"=>0,"G"=>0,"B"=>0,"Align"=>TEXT_ALIGN_TOPMIDDLE));

 /* Write the legend box */ 
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>8,"R"=>255,"G"=>255,"B"=>255));
 $PieChart->drawPieLegend(250,235,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput("pictures/example.draw3DPie.png");
?>