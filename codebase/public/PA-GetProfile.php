<?php

include("inc/config.php");
$title = 'Matched Local Governments';

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$city = strtoupper($_REQUEST['city']);
$state = $_REQUEST['state'];

// simulate SpecificSearch

$sql ='USE government';
$dbcnx = @PowerAlmanac\PDb::connect("localhost", "root", "powerAlmanac2011");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$read_sql = sprintf("
	SELECT * FROM government
	WHERE Government_Place_Name LIKE '%%%s%%' AND State = '%s'
	LIMIT 50
	", escape($city), escape($state));

$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die('Error reading from government database:' . PowerAlmanac\PDb::error());
}
$numResults = PowerAlmanac\PDb::num_rows($result_read);

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="5" cellspacing="5" border="0" align="center" width="95%">
            <tr>
                <td valign="top" align="center">
                <table cellpadding="5" cellspacing="5" border="0" align="center" width="95%">
                <tr>
                    <td colspan="2">
                    <h3>
                    <?php echo($title); ?>
                    </h3>
                    <p>
                    The following <b><?php echo($numResults); ?></b> local government(s) matched your search criteria.
                    </p>
                    <p>
                    <?php
                    if ($numResults != 0) {
                      echo("<p>Please click on the link to get a full profile.</p><p>");
                      while ($row = PowerAlmanac\PDb::fetch_array($result_read))
                      {
                          $Government_ID = $row['Government_ID'];
                          $Government_Place_Name = $row['Government_Place_Name'];
                          $County_Name = $row['County_Name'];
                          $Government_Type_Name = $row['Government_Type_Name'];
        
                          // results
						  if ($County_Name != '') {
                         	 echo("<a href='PA-RecaptchaBridge.php?id=$Government_ID'>$Government_Place_Name</a> ($Government_Type_Name) in COUNTY: $County_Name <br>");
						  } else {
							  echo("<a href='PA-RecaptchaBridge.php?id=$Government_ID'>$Government_Place_Name</a> ($Government_Type_Name)<br>");
						  }
                      }
                    } else {
                          echo("<p>There are no matches. Please enter another town name/state and <a href='search-government.php'>try again</a>.</p>");
                    }
                    ?>
                    </p>
                    </td>
                </tr>
                </table>
                <br>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
            //include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
