<?php
chdir('../');
include("./inc/config.php");
require_once("./class/Security.php");
chdir('./process/');

$User_Email = $_SESSION['user_email'];

if($_SESSION['sub_activate'] == 1 && isset($_SESSION['user_email'])){
	$newPassWord = $_POST['passWord'];
	$confirmPassWord = $_POST['passWord2'];
	if(isset($newPassWord) && isset($confirmPassWord)){
			if(strlen($newPassWord) < 4 && $confirmPassWord == $newPassWord){
				$_SESSION['subscribe_message'] = 'Please make sure your passwords match and are at least 4 characters in length';
				header("Location: ../sub-register.php");
			}else{
				// Prepare Security Password
				
				$passWord = Security::setPassword($newPassWord);

				$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
				$pdo->query(sprintf("UPDATE `registeredusers` SET `User_Password`='%s', `User_Activated`=true WHERE `User_Email`='%s'", escape($passWord), escape($User_Email)));

				$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
				$pdoObject2 = $pdo->query("SELECT @JSON_String");
				$rsArray2 = $pdoObject2->fetchAll();
				$json2 = $rsArray2[0]['@JSON_String'];
				$jsonArray2a = json_decode($json2, TRUE);

				unset($_SESSION['sub_activate']);
				$_SESSION['subscribe_message'] = '';



				//set session variables for user
				$_SESSION['user_actcode'] = $jsonArray2a['User_ActivationCode'];
				$_SESSION['user_firstname'] = $jsonArray2a['User_FirstName'];
				$_SESSION['user_lastname'] = $jsonArray2a['User_LastName'];
				$_SESSION['user_subscription'] = $jsonArray2a['User_Subscription'];
				$_SESSION['RegUser_ID'] = $jsonArray2a['RegUser_ID'];
				$_SESSION['User_Parent'] = $jsonArray2a['User_Parent'];
				$_SESSION['logged_in'] = '1';
				header("Location: ../terms-of-service-agree.php");
				flush();
				exit;
			}
		}else{
			$_SESSION['subscribe_message'] = 'Please make sure you enter your password in both fields';
			header("Location: ../sub-register.php");
	}
}else{
	$_SESSION['subscribe_message'] = 'Something went wrong, please try again';
	header("Location: ../sub-register.php");
}
