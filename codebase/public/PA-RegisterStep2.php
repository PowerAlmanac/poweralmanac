<?php

include("inc/config.php");
$title = 'Register';

// Sanitizing Data
$eMail = Sanitize::variable($_POST['eMail'], "@.-+");
$firstName = Sanitize::word($_POST['firstName'], " ");
$lastName = Sanitize::word($_POST['lastName'], " ");
$phone = str_replace(' ', '', Sanitize::variable($_POST['phone'], "- "));
$companyName = Sanitize::variable($_POST['companyName'], " '.-+,");

$recaptchaResponse = !empty($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response'] : '';
$ipAddress = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

// Their string slashing:
$passWord = str_replace("'","\'",$_POST['passWord']);
$passWordTwo = str_replace("'","\'",$_POST['passWord2']);

$_SESSION['subscribe_message'] = '';

$stuffArray = array(
	'email' => $eMail,
	'first' => $firstName,
	'last' => $lastName
);

$_SESSION['temp_register'] = json_encode($stuffArray);

if (($eMail == '') || ($passWord == ''))
{
	$_SESSION['subscribe_message'] = '<center>All fields are required and must be filled in.<br /></center>';
	header("Location: register.php");
	flush();
	exit;
}
else if (isset($_POST['AgreeToTOS']) == false || $_POST['AgreeToTOS'] != '1' )
{
	$_SESSION['subscribe_message'] = "You must agree to the terms of service.";
	header("Location: register.php");
	flush();
	exit;
} elseif (empty($recaptchaResponse)) {
    $_SESSION['subscribe_message'] = "<center>Please confirm that you are not a robot<br /></center>";
    header("Location: register.php");
    flush();
    exit;
}

// check the recaptcha response
$recaptchaResponseValid = PowerAlmanac\Recaptcha::validate(\PowerAlmanac\Config::env('recaptcha_privatekey'), $recaptchaResponse, $ipAddress);

if (!$recaptchaResponseValid) {
    $_SESSION['subscribe_message'] = "<center>Robot validation failed<br /></center>";
    header("Location: register.php");
    flush();
    exit;
}

// check if email already used
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL CheckEmail('%s',@RC)", escape($eMail)));
$pdoObject = $pdo->query("SELECT @RC");
$rsArray = $pdoObject->fetchAll();
$RC = $rsArray[0]['@RC'];
//print_r($RC) ; exit;

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}


if ($RC == '1') {
	// clean
	//echo("DEBUG: Clean email.<br>");
	$randval = mt_rand();
	
	// affiliate tracking here
	$affiliateID = $_SESSION['affiliateID'];
	
	// get affiliate encoding from database
	$readAffiliateEncoding_sql = sprintf("SELECT * FROM affiliate_partners
		WHERE Partner_ID = '%s'
		LIMIT 1
	", escape($affiliateID));
	$result_readAffiliateEncoding = @PowerAlmanac\PDb::query($readAffiliateEncoding_sql);
	if (!$result_readAffiliateEncoding) {
		die('Error reading from affiliate_partners database:' . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_readAffiliateEncoding);
	$Partner_Encoding = $onerow['Partner_Encoding'];
	$actCode = $Partner_Encoding . $randval;
	/*
	switch($affiliateID) {
		case '2': // GovEvents
			$actCode = "GE$randval";
			break;
		case '3': // Mark Amtower
			$actCode = "MA$randval";
			break;
		default:
			$actCode = "PA$randval";
			break;
	}
	*/
	// insert to database
	//mail('nick@poweralmanac.com', "DEBUG: InsertUserInfo", "'$eMail','$passWord','$firstName','','$lastName','$companyName','$actCode'");
	
	// Prepare Security Password
	require_once("./class/Security.php");
	
	$passWord = Security::setPassword($passWord);
	// End Password Security

    $pdo->query(sprintf("CALL InsertUserInfo('%s','%s','%s','','%s','','%s',@RC,0)",
        escape($eMail), escape($passWord), escape($firstName), escape($lastName), escape($actCode)));
    $pdoObject1 = $pdo->query("SELECT @RC");
    $rsArray1 = $pdoObject1->fetchAll();
	$RC1 = $rsArray1[0]['@RC'];

	//echo("DEBUG: RC1 = $RC1");
	if ($RC1 == '0') {

		//set agreement to ToS true
		$pdo->query(sprintf("UPDATE registeredusers SET AgreeToTerms = '1' WHERE User_Email='%s'", escape($eMail)));
		$pdo->query(sprintf("UPDATE registeredusers SET User_Phone = '%s' WHERE User_Email='%s'", escape($phone), escape($eMail)));

		// If Power Entry, update User_Subscription as the default is '1'
        if(isset($_POST['register-account']) && $_POST['register-account'] == 11) {
            $pdo->query(sprintf("UPDATE registeredusers SET User_Subscription = '11' WHERE User_Email='%s'", escape($eMail)));
        }

        $_SESSION['subscribe_message'] = '';
	} else {
		// should not happen
		$_SESSION['subscribe_message'] = '<center>Error Code: [IUI] - There is a hiccup with our servers. Please <a href="PA-ContactUs.php">contact us</a> for a status update.</center>';
		header("Location: register.php");
		flush();
		exit;
	}
} else {
	// email already used
	$_SESSION['subscribe_message'] = '<center>Your eMail address is already in use. <br />You may have already registered but not activated your account. <br />Please check your email. (Not there?  Be sure to check your spam/junk folders too.)</center>';
	header("Location: register.php");
	flush();
	exit;
}


//$url = 'PA-Activate.php?actCode=' . $actCode . '&e=' . $eMail;

// If not a pay as you go subscription (1 and 11 are Pay as you go and Power Entry)
if(isset($_POST['register-account']) && $_POST['register-account'] > 1 && $_POST['register-account'] != 11)
{
	$_SESSION['register-upgrade'] = $_POST['register-account'];
}

header("Location: activation.php?actCode=$actCode&e=$eMail");
flush();
exit;
// send email to activate code
//echo("DEBUG: messageBody = $messageBody "); exit;

// use Amazon SES
// require_once('ses.php');
// $ses = new SimpleEmailService('AKIAI5D3JYIQSM534C6A', 'Q0xFC1uu1pLOhMO68782UC6rG5rKSlsb1umf87AE');
// $m = new SimpleEmailServiceMessage();
// $m->addTo("$eMail");
// $m->addBCC('Power Almanac Activation <activation@poweralmanac.com>');
// $m->setFrom('Power Almanac Registration <registration@poweralmanac.com>');
// $m->setReturnPath('registration@poweralmanac.com');
// $m->setSubject('[Registration - Please Activate Account]');
// $m->setMessageFromString("$messageBody");

// $rc = $ses->sendEmail($m);



// $message = file_get_contents('emailtemplates/welcome.txt');
// $message = str_replace('[NAME]', $firstName, $message);
// $message = str_replace('[ACT_CODE]', $actCode, $message);
// $message = str_replace('[EMAIL]', $eMail, $message);

// $email_data = array(
// 	'to' => $eMail,
// 	'subject' => '[Registration - Please Activate Account]',	
// 	'html' => $message 
// 	);



// mandrill_send($email_data);



//mail("$eMail","[Registration - Please Activate Account]","$messageBody","Sender: registration@poweralmanac.com\r\nFrom: registration@poweralmanac.com\r\nBcc: activation@poweralmanac.com\r\n");

// set session variables for tracking PayPal checkout
$_SESSION['user_email'] = $eMail;
$_SESSION['user_actcode'] = $actCode;

// set terms of agreement flag
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
	exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}
$update_sql = sprintf("UPDATE registeredusers SET AgreeToTerms = '1'
	WHERE User_Email = '%s'
", escape($eMail));
$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
}

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("PA-inc-topheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("PA-inc-mainnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("PA-inc-tabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="5" cellspacing="5" width="95%" align="center" border="0">
            <tr>
                <td>
                <p class="hpHeaders">Register</p>
                <p>
                <!--
                You have successfully registered as a FREE subscriber!
                <br>
                <font color="#FF0000">Please check your email for the custom URL to activate your account. (Not there?  Be sure to check your spam/junk folders too.)</font>
                -->
                <b>ALMOST DONE!</b>
                <ul>
                <li class="textUL">Within seconds you should receive an activation email from registration@poweralmanac.com.</li>
                <li class="textUL"><font color="#FF0000"><u><b>Click the link in the activation email</b></u></font>, and then you can access your new account.</li>
                <br>          
				Didn't receive your activation email?  Please check your spam/junk folders.  If it's not there, please contact <a href="mailto:support@poweralmanac.com">support@poweralmanac.com</a>, and tell us the email address you used to register.
				</ul>
                </p>
                <p>
                Your free (basic) subscription includes 50 download credits. Your cost to download additional records is $0.95. Would you like to upgrade to a Power plan?
                </p>
                <table cellpadding="5" cellspacing="5" border="0" width="100%" align="center">
                <tr bgcolor="#95a9c2">
                    <?php
                    $pdo->query("CALL GetPaidSubscriptionTree(@JSON_String)"); 
                    $pdoObject2 = $pdo->query("SELECT @JSON_String");
                    $rsArray2 = $pdoObject2->fetchAll();
                    $json = $rsArray2[0]['@JSON_String'];
                    $jsonArray = json_decode($json, TRUE);
                    foreach ($jsonArray as $item) {
                        $Sub_Level = $item['Sub_Level'];
                        $Sub_Name = $item['Sub_Name'];
                        $Sub_SKU = $item['Sub_SKU'];
                        $Sub_NumReserves = number_format($item['Sub_NumReserves']);
                        if ($Sub_NumReserves == '999999') $Sub_NumReserves = 'unlimited';
                        $Sub_AnnualFees = number_format($item['Sub_AnnualFees']);
                        $Sub_CostPerDL = $item['Sub_CostPerDL'];
						$RegUser_ID = $item['RegUser_ID'];
						
						// handle affiliate ID
						if ($affiliateID == $RegUser_ID) {
							if ($Sub_Level <= '5') {
								echo("<td width='25%' align='center' valign='top'><h3><a href='registerStep3.php?sku=$Sub_SKU'>$Sub_Name</a></h3><p><b>$$Sub_AnnualFees Annual Fee</b><br />Download credits included:<br /><b>$Sub_NumReserves</b><br />Cost per each additional download credit: <br /><b>$$Sub_CostPerDL</b><br /><a href='registerStep3.php?sku=$Sub_SKU'>select this</a</p></td>");
							}
						}
						/*
						switch($affiliateID) {
							case '2': // GovEvents
								if ($RegUser_ID == '2') {
									if ($Sub_Level <= '5') {
										echo("<td width='25%' align='center' valign='top'><h3><a href='registerStep3.php?sku=$Sub_SKU'>$Sub_Name</a></h3><p><b>$$Sub_AnnualFees Annual Fee</b><br />Download credits included:<br /><b>$Sub_NumReserves</b><br />Cost per each additional download credit: <br /><b>$$Sub_CostPerDL</b><br /><a href='registerStep3.php?sku=$Sub_SKU'>select this</a</p></td>");
									}
								}
								break;
							case '3': // Mark Amtower
								if ($RegUser_ID == '3') {
									if ($Sub_Level <= '5') {
										echo("<td width='25%' align='center' valign='top'><h3><a href='registerStep3.php?sku=$Sub_SKU'>$Sub_Name</a></h3><p><b>$$Sub_AnnualFees Annual Fee</b><br />Download credits included:<br /><b>$Sub_NumReserves</b><br />Cost per each additional download credit: <br /><b>$$Sub_CostPerDL</b><br /><a href='registerStep3.php?sku=$Sub_SKU'>select this</a</p></td>");
									}
								}
								break;
							default:
								if ($Sub_Level <= '5') {
									echo("<td width='25%' align='center' valign='top'><h3><a href='registerStep3.php?sku=$Sub_SKU'>$Sub_Name</a></h3><p><b>$$Sub_AnnualFees Annual Fee</b><br />Download credits included:<br /><b>$Sub_NumReserves</b><br />Cost per each additional download credit: <br /><b>$$Sub_CostPerDL</b><br /><a href='registerStep3.php?sku=$Sub_SKU'>select this</a</p></td>");
								}
								break;
						}
						*/
                    }
                    ?>
                </tr>
                <tr bgcolor="#CCFF99">
                    <td colspan="5" align="left" valign="top">
                    <center>
                    <p>
                    <a href="login.php"><b>I'll stick with my Basic (free) subscription for now</b></a>
                    </p>
                    </center>
                    <!--
                    Remember to check your email for the activation URL. Not there?  Be sure to check your spam/junk folders too. After activating, you can log in and start searching and downloading.
                    -->
                    <p>
                    Remember to <b>CLICK THE LINK IN YOUR ACTIVATION EMAIL</b> (which should be in your inbox by now).
					</p>
					Didn't receive your activation email?  Please check your spam/junk folders.  If it's not there, please contact <a href="mailto:support@poweralmanac.com">support@poweralmanac.com</a>, and tell us the email address you used to register.
                    </td>
                </tr>
                </table>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
           // include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("PA-inc-footer.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
