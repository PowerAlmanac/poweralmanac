<?php

require_once("inc/config.php");

$upgrade = $_SESSION['register-upgrade'];

$eMail = trim($_POST['eMail']);
$eMail = Sanitize::variable($eMail, "@.-+");


// Because of the auto-login registration, we're not necessarily going to pass these POST values
// Though code should be more careful with how it interprets posts anyway. Could add elsewhere.
$passWord = (isset($_POST['passWord']) ? $_POST['passWord'] : '');
$remember = (isset($_POST['rememberMe']) ? $_POST['rememberMe'] : '');


$sql ="USE " . escape($dbname);
$dbcnx = PowerAlmanac\PDb::getInstance();
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
	exit();
}

if (!PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// Prepare Security Password
require_once("./class/Security.php");

$result = $pdo->prepare("SELECT User_Password FROM registeredusers WHERE User_Email=? LIMIT 1");
$result->execute(array($eMail));
$value = $result->fetch(PDO::FETCH_ASSOC);

// This section is to allow for an auto-login after registration:
if(defined("REGISTRATION_AUTO"))
{
	$passWord = $value['User_Password'];
}
else
{
	$passWord = Security::getPassword($passWord, $value['User_Password']);
}
	
// End Password Security

$pdo->query(sprintf("CALL ValidateLogIn('%s','%s',@JSON_String)", escape($eMail), escape($passWord)));
$pdoObject = $pdo->query("SELECT @JSON_String");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@JSON_String'];
$jsonArray = json_decode($json, TRUE);
//print_r($jsonArray); exit;
$RC = $jsonArray['RC'];
$REASON = $jsonArray['REASON'];


if ($RC == '1') {
	// added email to session for re-send
	$_SESSION['user_email'] = $eMail;
	// display error message - not activated, not in database, etc.
	// clean up REASON
	switch($REASON) {
		case 'user email':
			$ReasonText = 'ERROR: Incorrect Email Address. <br />If you have not registered yet, please <a href="./register">register FREE here</a>.';
			break;
		case 'incorrect password':
			$ReasonText = 'ERROR: Incorrect Password. <br />If you forgot your password, <a href="./forgot-password">click here</a> to recover it.';
			break;
		case 'account not activated':
			$ReasonText = 'ERROR: Account is NOT activated yet. <br />To re-send the activation email, <a href="./PA-ResendActivation">click here</a>.';
			//check if subuser needs to reset password
			$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($eMail)));
			$pdoObject2 = $pdo->query("SELECT @JSON_String");
			$rsArray2 = $pdoObject2->fetchAll();
			$json2 = $rsArray2[0]['@JSON_String'];
			$jsonArray = json_decode($json2, TRUE);	
			if($jsonArray['User_Parent'] != '0'){
				$_SESSION['sub_activate'] = 1;
				$_SESSION['user_email'] = $jsonArray['User_Email'];
				$_SESSION['user_actcode'] = $jsonArray['User_ActivationCode'];
				$_SESSION['user_firstname'] = $jsonArray['User_FirstName'];
				$_SESSION['user_lastname'] = $jsonArray['User_LastName'];
				$_SESSION['user_subscription'] = $jsonArray['User_Subscription'];
				$_SESSION['user_subscription_start'] = $jsonArray['DateTime_SubscriptionStart'];
				$_SESSION['RegUser_ID'] = $jsonArray['RegUser_ID'];
				$_SESSION['User_Parent'] = $jsonArray['User_Parent'];
				header("Location: sub-register.php");
				flush();
				exit();
			}

			break;
		default:
			$ReasonText = 'Unknown error.';
			break;
	}
	$_SESSION['login_message'] = "<center><font color='#FF0000'>$ReasonText<br /></font></center>";
	header("Location: login.php");
	flush();
} else {
	// get user info
	$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($eMail)));
	//echo "CALL GetUserInfo('$eMail',@JSON_String)";
	$pdoObject2 = $pdo->query("SELECT @JSON_String");
	$rsArray2 = $pdoObject2->fetchAll();
	$json2 = $rsArray2[0]['@JSON_String'];
	$jsonArray = json_decode($json2, TRUE);	
	
	// chekc subscription expiration
	$oneDay = 24*60*60;
	$temp = $jsonArray['DateTime_SubscriptionEnd'];
	//echo("<!-- temp = $temp -->");
	$SubscriptionEnd_UNIX = strtotime($jsonArray['DateTime_SubscriptionEnd']) - $oneDay;
	$todayIs_UNIX = time();
	
	//exit;	
	if ($SubscriptionEnd_UNIX > 0 && $todayIs_UNIX > $SubscriptionEnd_UNIX) {
		$_SESSION['login_message'] = "<center><font color='#FF0000'>ERROR: Your subscription has expired.<br /> Please contact <a href='https://www.poweralmanac.com/PA-ContactUs.php'>Support here</a> for more information on renewals.<br /></font></center>";
		header("Location: login.php");
		flush();
		exit;
	}
	
	$RC = $jsonArray['RC'];
	$_SESSION['useSavedSearchParams'] = '0';
	$_SESSION['login_message'] = '';
	$_SESSION['user_email'] = $jsonArray['User_Email'];
	$_SESSION['user_actcode'] = $jsonArray['User_ActivationCode'];
	$_SESSION['user_firstname'] = $jsonArray['User_FirstName'];
	$_SESSION['user_lastname'] = $jsonArray['User_LastName'];
	$_SESSION['user_subscription'] = $jsonArray['User_Subscription'];
	$_SESSION['user_subscription_start'] = $jsonArray['DateTime_SubscriptionStart'];
	$_SESSION['RegUser_ID'] = $jsonArray['RegUser_ID'];
	$_SESSION['User_Parent'] = $jsonArray['User_Parent'];
	$_SESSION['firstTimeSearch'] = 1;
	// do we flush search params?
	$_SESSION['lastSearchParams'] = '';
	$_SESSION['lastSearchStuff'] = '';
	$_SESSION['searchRequestArrayJSON'] = '
	[{"off_role":"govtOff_role ","searchbtn_x":"51","searchbtn_y":"11","searchbtn":"search","govType_counties":"county","govType_townships":"township","govType_municipalities":"municipality","role_1":"Top Elected Official","role_2":"Top Appointed Official","role_3":"Governing Board Member","role_4":"Head Of Finance\/Budgeting","role_5":"Head of Purchasing\/Procurement","role_6":"Head of Public Works","role_7":"Head of Law Enforcement","role_8":"Head of Fire Protection Services","role_9":"Clerk","selLocation":"locationByNational","govLoc_byRegion":["Northeast","South","Midwest","West"],"govLoc_byState":["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","District of Columbia","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"],"byZipcode":"","byZipcodeDistance":"","population":["9","8","7","6","5","4","3","2","1","0"]}]';
	// reset 
	$_SESSION['results_GovtOffMustMatchRoles'] = '1';
	$_SESSION['results_GovtOffMustHaveEmails'] = '0';
	
	// default search settings
	$_SESSION['checkbox_GovtTypes'] = '1';
	$_SESSION['checkbox_Roles'] = '0';
	$_SESSION['checkbox_Population'] = '1';
	$_SESSION['checkbox_FiscalYear'] = '1';
	$_SESSION['checkbox_States'] = '1';
	$_SESSION['checkbox_Demographics'] = '1';
	
	$User_Parent = $jsonArray['User_Parent'];
	$User_Activated = $jsonArray['User_Activated'];

	//check to see if they haven't just agreed to TOS, (bypass this for admin users)
	if($_POST['AgreeToTOS'] != '1' && $_SESSION['adminloggedin'] != '1')
	{
		//force all users to accept TOS
		$read_sql = sprintf("SELECT * FROM registeredusers WHERE User_Email = '%s' LIMIT 1", escape($eMail));

		$result_read = PowerAlmanac\PDb::query($read_sql);
		if (!$result_read) {
			die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
		}
		$row = PowerAlmanac\PDb::fetch_array($result_read);
		$AgreeToTerms = $row['AgreeToTerms'];

		if ($AgreeToTerms == '0') {
			$_SESSION['notAgreed'] = '1';
			$_SESSION['tmppwd'] = $_POST['passWord'];

			header("Location: ./login.php");
			flush();
			exit;
		}
	}
	else
	{
		//update agreement in db
		$User_Email = $_SESSION['user_email'];
		
		$pdo->query(sprintf("UPDATE registeredusers SET AgreeToTerms = '1' WHERE User_Email='%s'", escape($User_Email)));
		unset($_SESSION['notAgreed']);
	}


	// validated - set server SESSION variables
	$_SESSION['logged_in'] = '1';

	//store login cookie
	if($remember == 'true'){
		if(isset($_COOKIE['userlogin']) == false){
			$number_of_days = 30 ;
			$date_of_expiry = time() + 60 * 60 * 24 * $number_of_days ;
			setcookie("userlogin", $eMail, $date_of_expiry);
		}
	}else{
		if(isset($_COOKIE['userlogin']) == true){
			setcookie("userlogin", "", time() - 3600);
		}
	}

	
	// for power user alert
	//$_SESSION['poweruser_showdialog'] = '1';
	
	// insert log in user info
	$processUserNotes_sql = sprintf("INSERT INTO userinfo (UserEmail,RecordType,RecordTime)
		VALUES ('%s','login',now())
	", escape($eMail));
	$result_processUserNotes = PowerAlmanac\PDb::query($processUserNotes_sql);
	if (!$result_processUserNotes) {
		die('Error writing to userinfo database:' . PowerAlmanac\PDb::error());
	}
	
	// affiliate handling
	$user_actcode = $_SESSION['user_actcode'];
	$affiliateCode = substr($user_actcode,0,2);
	
	// get affiliate ID from database
	$readAffiliateID_sql = sprintf("SELECT * FROM affiliate_partners
		WHERE Partner_Encoding = '%s'
		LIMIT 1
	", escape($affiliateCode));
	$result_readAffiliateID = PowerAlmanac\PDb::query($readAffiliateID_sql);
	if (!$result_readAffiliateID) {
		die('Error reading from affiliate_partners database:' . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_readAffiliateID);
	$affiliateID = $onerow['Partner_ID'];
	/*
	switch($affiliateCode) {
		case 'PA':
			$affiliateID = '0';
			break;
		case 'GE':
			$affiliateID = '2';
			break;
		case 'MA':
			$affiliateID = '3';
			break;
		default:
			$affiliateID = '0';
			break;
	}
	*/
	$_SESSION['affiliateID'] = $affiliateID; 

	//Check if There are pending Deltek Imports to Assign to User instead of Session ID.
	$user_id = $jsonArray['RegUser_ID'];		//Logged In User ID



	if(!empty($_SESSION['deltek_exists']) AND !empty($_SESSION['deltek_session'])){				
		$sth = $pdo->prepare("UPDATE IGNORE `deltekimports` set user_id = ? WHERE user_id = ?"); //Change Deltek Import Record to use Member ID
		$sth->execute(array($user_id, session_id()));			
	
			header("Location: D-search-import.php");
			flush();
			exit;		

	}
	
	//Check If User has any imports
	$stm = $pdo->prepare("SELECT import_id FROM deltekimports WHERE user_id = ? AND active = 1 ORDER BY import_id desc LIMIT 1");
    $stm->execute(array($user_id));
    $table_id = $stm->fetch();


    //add admin bypass to allow access
    if(!empty($table_id) && $_SESSION['adminloggedin'] != '1'){
    	$_SESSION['deltek_exists'] = $table_id['import_id'];    	
    	header("Location: D-search-import.php");
		flush();
		exit;	
    }

    if(!empty($_SESSION['redirect'])){
    	$redirect = $_SESSION['redirect'];
    	unset($_SESSION['redirect']);
    	header("Location: " . $redirect);
    	flush();
		exit;
    }

    //if they've upgraded their account on login, we need to redirect them to the checkout page to complete that, otherwise they keep their basic account
    if(isset($upgrade) && $upgrade > 2){
    	header("Location: checkout?subscription=$upgrade");
    	unset($_SESSION['register-upgrade']);
		flush();
		exit;
    }

	// go to dashboard
	header("Location: search-government.php");
	flush();
	exit;
}

?>
