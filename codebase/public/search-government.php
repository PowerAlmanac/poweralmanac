<?php

//use these two global variables to define the title and any other meta info
define("TITLE", "Search government officials in over 21,000 municipalities");
define("DESCRIPTION", "Search our whole database consisting of 220,000+ government officials with their contact information. Narrow your search by location, role, and many other extensive filters.");
define("CUSTOMHEADER", '<link rel="canonical" href="http://www.poweralmanac.com/search-government.php" />');

include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>

<nav class="navbar fixed-bottom navbar-dark bg-success d-block d-sm-none">
    <div id="mobileLoadingOverlay" class="text-center d-sm-none mb-2" style="display:none;">
        <h2 style="color:#FFFFFF;">Loading your search results...</h2>
        <div id="noTrespassingOuterBarG">
            <div id="noTrespassingFrontBarG" class="noTrespassingAnimationG">
                <div class="noTrespassingBarLineG"></div>
                <div class="noTrespassingBarLineG"></div>
                <div class="noTrespassingBarLineG"></div>
                <div class="noTrespassingBarLineG"></div>
                <div class="noTrespassingBarLineG"></div>
                <div class="noTrespassingBarLineG"></div>
            </div>
        </div>
    </div>

    <span class='total-officials-fill-in navbar-text' style="color:#FFFFFF;">213,000</a></span>
    <span class="navbar-text" style="color:#FFFFFF;">&nbsp;Matched Records</span>

    <div class="float-right btn-group ml-1">
        <a class="btn btn-sm btn-danger" href="./search-government?resetSearch=true">Start over..</a>
        <a class="btn btn-sm btn-warning" href="checkout">Download &raquo;</a>
    </div>
</nav>


            <div class="main-holder tabbed searchgov container">
                <!-- white box -->
                <div class="frame">
                	<div id="search-header" class="d-none d-sm-block">
                        <div id="loadingOverlayTwo">
                            <h2>Loading your search results...</h2>
                            <div id="noTrespassingOuterBarG">
                                <div id="noTrespassingFrontBarG" class="noTrespassingAnimationG">
                                    <div class="noTrespassingBarLineG"></div>
                                    <div class="noTrespassingBarLineG"></div>
                                    <div class="noTrespassingBarLineG"></div>
                                    <div class="noTrespassingBarLineG"></div>
                                    <div class="noTrespassingBarLineG"></div>
                                    <div class="noTrespassingBarLineG"></div>
                                </div>
                            </div>
                        </div>
                        <h2><span class='total-officials-fill-in' id="total-officials-fill-in">217,300+</span> Matched Records <small class="d-none d-lg-inline">(# of local govt officials)</small></h2>
                		<div id="search-fill-in" class="d-none d-sm-block">
                			<span id="total-gov" title="This is the total number of local governments with officials that matched your search">21,000+</span>
                			<span id="total-email" title="This is the matched number of officials with an email address included">160,000+</span>
                			<span id="total-phone" title="This is the matched number of officials with a phone number included">217,000+</span>
                			<span id="total-mail" title="This is the matched number of officials with a mailing address included">217,000+</span>

                            <a id="smallcheckout" class="btn btn-sm btn-warning" href="checkout">Download</a>
                			<a href="./search-government?resetSearch=true" class='btn btn-sm btn-info' id="reset">Start Over</a>
                            <a id="savesearchbutton" class="btn btn-secondary btn-sm" href="PA-SaveSearch">Save Search</a>
                		</div>
                	</div>

                    <div class="row">
                        <div class="col-md-9 col-xs-12 pl-0 pr-0 pl-sm-3 pr-sm-3">
                            <ul class="top-tabs lowertabs">
                                <li><a class="tablink gray-tab" id="btn-chooseroles" href="#chooseroles"><span class="d-none d-sm-inline">Choose Roles</span></a></li>
                                <li><a class="tablink gray-tab" id="btn-location" href="#location"><span class="d-none d-sm-inline">Location</span></a></li>
                                <li><a class="tablink gray-tab" id="btn-spending" href="#spendingdata"><span class="d-none d-sm-inline">Fiscal Data</span></a></li>
                                <li><a class="d-none d-sm-inline-block orange-tab" id="btn-analyze" href="PA-Analyze">Analyze</a></li>
                            </ul>
                            <div class="step-holder">
                                <div class="tab" id="chooseroles">
                                    <section class="inset" id="r-roles">
                                        <h3>Roles</h3>

                                        <div class="ml-3 form-row">
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input" type="checkbox" id="infotech" data-val="Head of IT" value="role_10" />
                                                <label class="form-check-label" for="infotech">Head of IT</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>The person responsible for management of the local government’s IT infrastructure.</strong></p><p><strong>How many are there?</strong> Most large local governments (those representing 25,000 or more people) have someone in this role.  But the smaller the government, the more likely it is to outsource this role to the county government or a private company.</p><p><strong>Common Titles: </strong>“IT director” and “IT manager” are the most common, although “IT” is sometimes replaced with “IS.”</p>">?</a>
                                            </div>
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input" type="checkbox" id="police" data-val="Head of Law Enforcement" value="role_7" />
                                                <label class="form-check-label" for="police">Police Chief / Sheriff</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>Manages the government’s law enforcement services.</strong></p><p><strong>How many are there?</strong> About 65% of local governments have their own police chief.  However, most townships outsource law enforcement to the county or a neighboring municipality.</p><p><strong>Common Titles: </strong>“Police chief” is used by municipalities and townships, while counties have a “sheriff.”</p>">?</a>
                                            </div>
                                        </div>

                                        <div class="ml-3 form-row">
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="purchasing" data-val="Head of Purchasing/Procurement" value="role_5" />
                                                <label class="form-check-label"  for="purchasing">Head of Purchasing</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>Oversees most or all purchases of the government and sets procurement policies.</strong></p><p><strong>How many are there?</strong> About 40% of governments have someone in this role.  But many governments have a decentralized purchasing approach and therefore nobody in this role.</p><p><strong>Common Titles: </strong>“Purchasing manager,” “purchasing agent,” or some variation.  In smaller governments this role is often handled by the Head Clerk or Head of Finance.</p>">?</a>
                                            </div><div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="clerk" data-val="Clerk" value="role_9" />
                                                <label class="form-check-label"  for="clerk">Head Clerk</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>Manages official records, publishes the government’s code and legal notices, and responds to public records requests.</strong></p><p><strong>How many are there?</strong> Virtually every local government has someone in this role. </p><p><strong>Common Titles: </strong>This person typically has “clerk” in their title, but “secretary” and “recorder” are not uncommon.</p>">?</a>
                                            </div>
                                        </div>

                                        <div class="ml-3 form-row">
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="publicworks" data-val="Head of Public Works" value="role_6" />
                                                <label class="form-check-label"  for="publicworks">Head of Public Works</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>Oversees the government’s engineering, surveying, and public construction projects, as well as maintenance and operation of equipment, facilities, and roads.</strong></p><p><strong>How many are there?</strong> About 70% of local governments have someone in this role. Townships and small governments don’t need anyone in this role, or outsource this job to a larger neighboring government or the county.</p><p><strong>Common Titles: </strong>“Director of public works,” “highway superintendent,” or some variation.</p>">?</a>
                                            </div><div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="alderman" data-val="Governing Board Member" value="role_3" />
                                                <label class="form-check-label"  for="alderman">Council Member / Equivalent</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>A member of the governing board, which sets overall policies for the local government.</strong></p><p><strong>How many are there?</strong> Every local government has at least one, but the typical range is 3-11 per government.</p><p><strong>Common Titles: </strong>Other than “council member,” common titles include “alderman,” “trustee,” “supervisor,” and “commissioner.”</p>">?</a>
                                            </div>
                                        </div>

                                        <div class="ml-3 form-row">
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="fire" data-val="Head of Fire Protection Services" value="role_8" />
                                                <label class="form-check-label"  for="fire">Fire Chief</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>Manages the government’s fire protection services.</strong></p><p><strong>How many are there?</strong> About 75% of cities have their own fire chief. However, only about 20% of counties provide fire protection services, and most townships outsource fire protection services to a neighboring government, and therefore don’t have this role.</p><p><strong>Common Titles: </strong>Almost always a “fire chief.” Not to be confused with the “fire marshal,” who checks for code violations.</p>">?</a>
                                            </div>
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="finance" data-val="Head of Finance/Budgeting" value="role_4" />
                                                <label class="form-check-label"  for="finance">Head of Finance</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>Oversees all accounting, financial management, and financial reporting for a government.</strong></p><p><strong>How many are there?</strong> Virtually every local government has someone in this role, but in smaller governments, the role may be combined with the role of Head Clerk.</p><p><strong>Common Titles: </strong>“Treasurer,” “finance director,” or some variation.</p>">?</a>
                                            </div>
                                        </div>

                                        <div class="ml-3 form-row">
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="manager" data-val="Top Appointed Official" value="role_2" />
                                                <label class="form-check-label"  for="manager">City / County Manager</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>The top appointed official for a government, responsible for day-today operations.</strong></p><p><strong>How many are there?</strong> Medium and large sized local governments are likely to have someone in this role, but smaller governments are not. For governments that do NOT have anyone in this role, it’s commonly the top elected official who runs day-to-day operations.</p><p><strong>Common Titles: </strong>“Administrator” is a common alternative to “manager.”</p>">?</a>
                                            </div>
                                            <div class="col-md-6 form-check">
                                                <input class="form-check-input"  type="checkbox" id="mayor" data-val="Top Elected Official" value="role_1" />
                                                <label class="form-check-label"  for="mayor">Mayor / Equivalent</label><span class="query-fill"></span>
                                                <a class="circle-tip" title="" data-html="true" data-original-title="<p><strong>The top elected official for every government, regardless of title.</strong></p><p><strong>How many are there? </strong>By definition, every local government has someone in this role.</p><p><strong>Common Titles: </strong>Most cities have a mayor, while townships have a wide range of titles such as “township supervisor” or “township trustee.” For counties, this is usually the chairman of the governing board. “President” is a common alternative at all levels.</p>">?</a>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="inset pb-3" style="min-height: 10rem;">
                                        <h3>Population Range <br class="d-block d-sm-none"/><small id="populationRangeResult"></small></h3>
                                        <div id="rangeslider" class="noUiSlider">
                                        </div>
                                    </section>
                                    <section class="inset" id="r-government-type">
                                        <h3>Type of Government</h3>

                                        <div class="ml-3 form-row">
                                            <div class="col-md-4 form-check">
                                                <input class="form-check-input" type="checkbox" id="cities" value="govType_municipalities" data-val="municipality" />
                                                <label class="form-check-label" for="cities">Cities / Municipalities</label><span class="query-fill"></span>
                                            </div>

                                            <div class="col-md-4 form-check">
                                                <input class="form-check-input" type="checkbox" id="towns" value="govType_townships" data-val="township" />
                                                <label class="form-check-label" for="towns">Townships</label><span class="query-fill"></span>
                                            </div>

                                            <div class="col-md-4 form-check">
                                                <input class="form-check-input" type="checkbox" id="counties" value="govType_counties" data-val="county" />
                                                <label class="form-check-label" for="counties">Counties</label><span class="query-fill"></span>
                                            </div>
                                        </div>
                                    </section>
                                    <a href="#location" class="btn btn-warning btn-lg next float-sm-right mt-4">NEXT STEP <span class="bump-up">»</span> Choose Location</a>
                                </div>

                                <div class="tab" id="location">
                                    <section class="inset" id="r-location">
                                        <h3>Search by Location</h3>
                                        <div id="search-region" class="full">
                                            <h4>By Region</h4>
                                            <div class="row">
                                                <div class="form-check col-md-3"><input class="region-checkbox" type="checkbox" id="northeast" /><label for="northeast">Northeast</label><span class="query-fill"></span></div>
                                                <div class="form-check col-md-3"><input class="region-checkbox" type="checkbox" id="south" /><label for="south">South</label><span class="query-fill"></span></div>
                                                <div class="form-check col-md-3"><input class="region-checkbox" type="checkbox" id="midwest" /><label for="midwest">Midwest</label><span class="query-fill"></span></div>
                                                <div class="form-check col-md-3"><input class="region-checkbox" type="checkbox" id="west" /><label for="west">West</label><span class="query-fill"></span></div>
                                            </div>
                                        </div>
                                        <div id="search-state" class="full cutoff">
                                            <h4>By State <small><a href="#" class="orange" id="toggle-states">toggle</a></small></h4>
                                            <input type="checkbox" id="selectall0"><label class="orange" for="selectall0">Select All / Deselect All</label>
                                            <div id="statelist" class="d-flex p-1 flex-wrap pb-2">
                                                <div><input type="checkbox" id="alabama" /><label for="alabama">Alabama</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="alaska" /><label for="alaska">Alaska</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="arizona" /><label for="arizona">Arizona</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="arkansas" /><label for="arkansas">Arkansas</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="california" /><label for="california">California</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="colorado" /><label for="colorado">Colorado</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="connecticut" /><label for="connecticut">Connecticut</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="delaware" /><label for="delaware">Delaware</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="districtofcolumbia" /><label for="districtofcolumbia">District Of Columbia</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="florida" /><label for="florida">Florida</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="georgia" /><label for="georgia">Georgia</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="hawaii" /><label for="hawaii">Hawaii</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="idaho" /><label for="idaho">Idaho</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="illinois" /><label for="illinois">Illinois</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="indiana" /><label for="indiana">Indiana</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="iowa" /><label for="iowa">Iowa</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="kansas" /><label for="kansas">Kansas</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="kentucky" /><label for="kentucky">Kentucky</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="louisiana" /><label for="louisiana">Louisiana</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="maine" /><label for="maine">Maine</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="maryland" /><label for="maryland">Maryland</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="massachusetts" /><label for="massachussets">Massachusetts</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="michigan" /><label for="michigan">Michigan</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="minnesota" /><label for="minnesota">Minnesota</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="mississippi" /><label for="mississippi">Mississippi</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="missouri" /><label for="missouri">Missouri</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="montana" /><label for="montana">Montana</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="nebraska" /><label for="nebraska">Nebraska</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="nevada" /><label for="nevada">Nevada</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="newhampshire" /><label for="newhampshire">New Hampshire</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="newjersey" /><label for="newjersey">New Jersey</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="newmexico" /><label for="newmexico">New Mexico</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="newyork" /><label for="newyork">New York</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="northcarolina" /><label for="northcarolina">North Carolina</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="northdakota" /><label for="northdakota">North Dakota</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="ohio" /><label for="ohio">Ohio</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="oklahoma" /><label for="oklahoma">Oklahoma</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="oregon" /><label for="oregon">Oregon</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="pennsylvania" /><label for="pennsylvania">Pennsylvania</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="rhodeisland" /><label for="rhodeisland">Rhode Island</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="southcarolina" /><label for="southcarolina">South Carolina</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="southdakota" /><label for="southdakota">South Dakota</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="tennessee" /><label for="tennessee">Tennessee</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="texas" /><label for="texas">Texas</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="utah" /><label for="utah">Utah</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="vermont" /><label for="vermont">Vermont</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="virginia" /><label for="virginia">Virginia</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="washington" /><label for="washington">Washington</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="westvirginia" /><label for="westvirginia">West Virginia</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="wisconsin" /><label for="wisconsin">Wisconsin</label><span class="query-fill"></span></div>
                                                <div><input type="checkbox" id="wyoming" /><label for="wyoming">Wyoming</label><span class="query-fill"></span></div>
                                            </div>
                                        </div>
                                        <div id="search-zip" class="full last">
                                            <h4>By Zip Code</h4>
                                            <div>
                                                <label>Zip Code</label>
                                                <input type="text" name="searchzip" id="searchzip" />
                                            </div>
                                            <div id="radius-select">
                                                <label>Radius</label>
                                                <select class="form-control">
                                                    <option value="10" selected="selected" value="10 mi">10 mi</option>
                                                    <option value="25" value="25 mi">25 mi</option>
                                                    <option value="50" value="50 mi">50 mi</option>
                                                    <option value="100" value="100 mi">100 mi</option>
                                                </select>
                                            </div>
                                            <div id="zipWarning"></div>
                                        </div>
                                    </section>

                                    <!-- <section class="inset" id="specific-government">
                                        <h4>Looking for one specific local government?</h4>
                                        <div>
                                            <label>City</label>
                                            <input type="text" class="input-medium" placeholder="San Francisco">
                                        </div>
                                        <div>
                                            <label>State</label>
                                            <div class="input-append">
                                              <input class="input-small" id="appendedInputButton" type="text" placeholder="California">
                                              <button class="btn btn-info" type="button">Go!</button>
                                            </div>
                                        </div>
                                        <div id="or-zip">
                                            <strong>-- OR --</strong>
                                        </div>
                                        <div>
                                            <label>Zip Code</label>
                                            <div class="input-append">
                                              <input class="input-mini" id="appendedInputButton" type="text" placeholder="94101" maxlength="5">
                                              <button class="btn btn-info" type="button">Go!</button>
                                            </div>
                                        </div>
                                    </section> -->
                                    <a href="#spendingdata" class="next btn btn-warning btn-lg expand-btn float-sm-right mt-4">NEXT STEP <span class="bump-up">»</span> Spending Data</a>
                                </div>

                                <div class="tab" id="spendingdata">
                                    <section class="inset d-none d-lg-block" id="r-spending">
                                        <h3 class="mb-0">Search only governments that spend</h3>
                                        <div class="btn-group w-100 row ml-1" role="group" id="r-spending-type">
                                            <button class="expand-btn btn btn-sm btn-secondary col-md-4 d-none" data-val="expenseBudget" data-expand="#annual-range">Annual Spending Range</button>
                                            <button class="expand-btn btn btn-sm btn-secondary col-md-4 d-none" data-val="expenseBudgetQuartile" data-expand="#quartile">Spending by Quartile</button>
                                            <button class="expand-btn btn btn-sm btn-secondary col-md-4 d-none" data-val="perCapitalBudgetQuartile" data-expand="#quartile">Per Capita by Quartile</button>
                                        </div>

                                        <form class="form-inline" id="spending-form mt-0">
                                            <div id="annual-range" class="expand col-md-12">
                                                <div class="input-group input-group-sm col-md-6">
                                                    <label class="col-md-4">More than</label>

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input class="form-control" id="more-than-spending" type="text">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">.00</span>
                                                    </div>
                                                </div>

                                                <div class="input-group input-group-sm col-md-6">
                                                    <label class="col-md-4">Less than</label>

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input class="form-control" id="less-than-spending" type="text">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">.00</span>
                                                    </div>
                                                </div>
                                                <span id="amount-error-msg"></span>
                                            </div>

                                            <div id="quartile" class="expand w-100 d-none">
                                                <div class="form-row">
                                                    <div class="col-md-3 form-check">
                                                        <input class="form-check-input" type="checkbox" id="quartile_highest_4" value="4" />
                                                        <label class="form-check-label" for="quartile_highest_4">Highest Spenders<br/>(4th Quartile)</label>
                                                    </div>
                                                    <div class="col-md-2 form-check">
                                                        <input class="form-check-input" type="checkbox" id="quartile_highest_3" value="3" />
                                                        <label class="form-check-label" for="quartile_highest_3">High-Medium<br/>(3rd Quartile)</label>
                                                    </div>

                                                    <div class="col-md-2 form-check">
                                                        <input class="form-check-input" type="checkbox" id="quartile_highest_2" value="2" />
                                                        <label class="form-check-label" for="quartile_highest_2">Low-Medium<br />(2nd Quartile)</label>
                                                    </div>
                                                    <div class="col-md-3 form-check">
                                                        <input class="form-check-input" type="checkbox" id="quartile_highest_1" value="1" />
                                                        <label class="form-check-label" for="quartile_highest_1">Lowest Spenders<br />(1st Quartile)</label>
                                                    </div>

                                                    <div class="col-md-2 form-check">
                                                        <input class="form-check-input" type="checkbox" id="quartile_highest_5" value="5" />
                                                        <label class="form-check-label" for="quartile_highest_5">Spend $0<br />&nbsp;</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="following-category" class="row w-100">
                                                <h4 class="col-md-5 d-inline">In the following category</h4>
                                                <select id="categories" class="form-control col-md-7">
                                                    <option data-val="expType" value="expType">Total Expenditure</option>
                                                    <option data-val="finPS" value="Public Safety">Public Safety</option>
                                                    <option data-val="finPW" value="Public Welfare">Public Welfare</option>
                                                    <option data-val="finH" value="Health">Health</option>
                                                    <option data-val="finU" value="Utilities">Utilities</option>
                                                    <option data-val="finT" value="Transportation">Transportation</option>
                                                    <option data-val="finL" value="Leisure">Leisure</option>
                                                    <option data-val="finF" value="Finance">Finance</option>
                                                    <option data-val="finM" value="Miscellaneous">Miscellaneous</option>
                                                    <option data-val="expType" value="Special Categories">Special Categories</option>
                                                </select>

                                                <div class="expandSelect" id="finPS">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Public Safety</option>
                                                        <option value="Corrections">Corrections</option>
                                                        <option value="Fire Protection">Fire Protection</option>
                                                        <option value="Judicial and Legal">Judicial and Legal</option>
                                                        <option value="Police">Police</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finPW">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Public Welfare</option>
                                                        <option value="Assistance and Subsidies">Assistance and Subsidies</option>
                                                        <option value="Welfare Program Administration">Welfare Program Administration</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finH">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Health</option>
                                                        <option value="Health Services">Health Services</option>
                                                        <option value="Hospitals">Hospitals</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finU">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Utilities</option>
                                                        <option value="Electricity">Electricity</option>
                                                        <option value="Gas">Gas</option>
                                                        <option value="Sewerage">Sewerage</option>
                                                        <option value="Solid Waste Management">Solid Waste Management</option>
                                                        <option value="Water">Water</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finT">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Transportation</option>
                                                        <option value="Airports">Airports</option>
                                                        <option value="Parking">Parking</option>
                                                        <option value="Ports and Waterways">Ports and Waterways</option>
                                                        <option value="Public Transit">Public Transit</option>
                                                        <option value="Roads and Highways">Roads and Highways</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finL">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Leisure</option>
                                                        <option value="Libraries">Libraries</option>
                                                        <option value="Parks and Recreation">Parks and Recreation</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finF">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Total Finance</option>
                                                        <option value="Financial Administration">Financial Administration</option>
                                                        <option value="Interest on Debt">Interest on Debt</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="finM">
                                                    <select class="form-control">
                                                        <option selected="selected" value="All">Education</option>
                                                        <option value="General Administration">General Administration</option>
                                                        <option value="Housing & Community Development">Housing & Community Development</option>
                                                        <option value="Liquor Stores">Liquor Stores</option>
                                                        <option value="Natural Resources">Natural Resources</option>
                                                    </select>
                                                </div>

                                                <div class="expandSelect" id="expType">
                                                    <select class="form-control">
                                                        <option selected="selected" value="2">Only Capital Expenditures</option>
                                                        <option value="3">Only Operating Expenditures</option>
                                                        <option value="4">Only Salaries & Wages</option>
                                                        <option value="5">Only Expenditures on Suppliers</option>
                                                    </select>
                                                </div>

                                            </div>
                                    </section>
                                    <section class="inset">
                                        <div id="endBudgetYear">
                                            <h3>Fiscal Year ending in</h3>
                                            <select id="months" class="selectpicker" data-style="btn-white" data-tick-icon="oi oi-circle-check" multiple>
                                                <option data-subtext="0" value="January">January</option>
                                                <option data-subtext="0" value="February">February</option>
                                                <option data-subtext="0" value="March">March</option>
                                                <option data-subtext="0" value="April">April</option>
                                                <option data-subtext="0" value="May">May</option>
                                                <option data-subtext="0" value="June">June</option>
                                                <option data-subtext="0" value="July">July</option>
                                                <option data-subtext="0" value="August">August</option>
                                                <option data-subtext="0" value="September">September</option>
                                                <option data-subtext="0" value="October">October</option>
                                                <option data-subtext="0" value="November">November</option>
                                                <option data-subtext="0" value="December">December</option>
                                            </select>
                                            <a class="btn btn-secondary left ml-2" style="color:#FFFFFF;" id="manipulateMonths" data-all="1">Select All</a>
                                        </div>
                                        </form>
                                        <a id="checkout-final" href="checkout.php" class="btn btn-warning btn-lg ready float-sm-right mt-4">DOWNLOAD SEARCH RESULTS <span class="bump-up">»</span></a>
                                    </section>
                                    <!--<section class="inset" id="spending-one">
                                        <img src="images/chart1.png" alt="select specific government">
                                        <h4>Looking for a spending profile of one specific government?</h4>
                                        <form class="form-inline">
                                            <input type="text" placeholder="San Francisco">
                                            <div class="input-append">
                                              <input class="col-md-2" id="appendedInputButton" type="text" placeholder="California">
                                              <button class="btn btn-info" type="button">Go!</button>
                                            </div>
                                        </form>
                                    </section> -->
                                    <!-- <a class="btn btn-warning btn-large" id="submitsearch" href="checkout">SUBMIT SEARCH <span class="bump-up">»</span></a> -->						</div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div id="priceBar" class="d-none d-sm-block">
                                <div id="loadingOverlay">
                                    <h4>Loading your search results...</h4>
                                    <div id="noTrespassingOuterBarG">
                                    <div id="noTrespassingFrontBarG" class="noTrespassingAnimationG">
                                    <div class="noTrespassingBarLineG">
                                    </div>
                                    <div class="noTrespassingBarLineG">
                                    </div>
                                    <div class="noTrespassingBarLineG">
                                    </div>
                                    <div class="noTrespassingBarLineG">
                                    </div>
                                    <div class="noTrespassingBarLineG">
                                    </div>
                                    <div class="noTrespassingBarLineG">
                                    </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="grayinset" id="pricingHeader">FILTER SETTINGS</div>
                                <div class="blackinset" id="switches">
                                    <div class="whole" id="onePerGov">
                                        <a class="tooltipLink" title="Toggle this 'On' if you to make sure that you have at least one official from each government included in your results."><img src="images/tooltip.png" /></a>
                                        <div class="switch switch-mini">
                                            <input type="checkbox">
                                        </div>
                                        <span>Include at least one official per matched government</span>
                                    </div>
                                    <div class="whole" id="includeEmails">
                                        <a class="tooltipLink" title="Toggle this 'On' if you want your results to only include officials with an email address"><img src="images/tooltip.png" /></a>
                                        <div class="switch switch-mini">
                                            <input type="checkbox">
                                        </div>
                                        <span>Only include officials with an email address</span>
                                    </div>
                                    <div class="clearfix"></div>

                                    <p class="mt-3" style="font-size: 0.7rem;">
                                        Depending on which Power Plan you subscribe to, your cost per downloaded record will range from 7&cent; to 90&cent;, which gives you the right to 1-year unlimited usage of that record.
                                    </p>
                                </div>

                                <div id="pricingHeader" class="d-none">PRICING<span id="pricingTop">N/A</span></div>
                                <div class="blackinset d-none">
                                    <div class="whole borderUnder">Quantity<span id="quantityRecords">N/A</span></div>
                                    <div class="whole borderUnder">Quantity Unpurchased<span id="quantityUnpaid">N/A</span></div>
                                    <div class="whole borderUnder">Your Credits Used<span id="usedRecords">N/A</span></div>
                                    <div class="whole borderUnder">Additional Cost / Record<span id="costPerRecord">N/A</span></div>
                                    <div class="whole grayEncapsule">Total<span id="totalPricing">N/A</span></div>
                                    <div class="whole"><a class="btn btn-secondary btn-sm" href="PA-SaveSearch">Save Search</a><span>
                                            <a id="smallcheckout" class="btn btn-sm btn-warning" href="checkout">Download</a></span></div>
                                    <div class="whole grayEncapsule" id="subusercredits"></div>
                                    <div class="whole borderUnder"><strong id="purchaseOrUpgrade">Included in Purchase</strong></div>
                                    <div class="whole borderUnder" id="subscriptionType">&nbsp;<span>N/A</span></div>
                                    <div class="whole borderUnder">Included Credits<span id="includedCredits">N/A</span></div>
                                    <div class="whole borderUnder">Remaining Credits<span id="remainingCredits">N/A</span></div>
                                    <div class="whole"><a href="./pricing" target="_blank" id="otheroptions">See Other Pricing Options</a></div>
                                    <div class="whole"><a href="./pricing" target="_blank" id="joinpowerplus"><img src="images/joinpowerplus.png" alt="join power plus government search" /></a></div>
                                </div>
                            </div>

                            <div id="popupCities">
                            </div>
                        </div>
                    </div>
                </div>
                 <?php
					// get last updated

					$lastUpdated = 'Not Available';
					
					$sql ="USE " . escape($dbname);
					$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
					if (!$dbcnx) {
						print("Unable to connect to the database server at this time.\n");
					exit();
					}
					if (!@PowerAlmanac\PDb::query($sql)){
						die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
					}

					$readdate_sql = "
						SELECT CREATE_TIME
						FROM   information_schema.tables
						WHERE  TABLE_SCHEMA = 'government'
						AND TABLE_NAME = 'all_data'
						";

					$result_readdate = @PowerAlmanac\PDb::query($readdate_sql);
					if (!$result_readdate) {
						die('Error reading from government database:' . PowerAlmanac\PDb::error());
					}
					$onerow = PowerAlmanac\PDb::fetch_array($result_readdate);
					//$lastUpdated = $onerow['UPDATE_TIME'];
					$UPDATE_TIME = $onerow['CREATE_TIME'];
					$UPDATE_TIME_Unix = strtotime($UPDATE_TIME) - (8*3600); // CREATE_TIME is the DB timezone, UTC, remove 8 h    ours to be Pacific Standard Time (PST)
					$lastUpdated = date("F j, Y, g:i a",$UPDATE_TIME_Unix) . ' PST';
					echo "<p class='text-center mx-auto pb-5 mb-4 mt-4' style='font-size: 14px;'>Records updated weekly. Most recent update on <strong>$lastUpdated</strong></p>";
				?>
            </div>


            <script src="js/jquery.colorbox-min.js"></script>
            <script src="js/bootstrapSwitch.js"></script>
            <script src="js/jquery.nouislider.min.js?v=20181105-2"></script>

    <script>
    //no log errors in IE
    if ( ! window.console ) console = { log: function(){} };
    <?php

    	//loop through subscriptions - assign javascript objects to be used in calculations
    		echo "var bestPricing = JSON.parse('" . json_encode(getSubList($dbname,$dbhost,$dbuser,$dbpass)) . "');"; 

    	if($_SESSION['logged_in'] == '1'){
    		echo "var loggedIn = true;\n";
    		echo "var subscriptionType = '".$_SESSION['user_subscription']."';\n";


    		if($_SESSION['user_subscription'] > 0){
    			$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
    			$User_Email = $_SESSION['user_email'];
    			$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
				$pdoObject2 = $pdo->query("SELECT @JSON_String");
				$rsArray2 = $pdoObject2->fetchAll();
				$json2a = $rsArray2[0]['@JSON_String'];
				$jsonArray2a = json_decode($json2a, TRUE);
				$User_Password = $jsonArray2a['User_Password'];
				$User_Parent = $jsonArray2a['User_Parent'];
				$User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
				if(!$User_DL_Reserves){
					$User_DL_Reserves = '0';
				}
				echo "var creditsRemaining = ". $User_DL_Reserves . ";\n";

				// check for subuser extra credits
				$subuser_credits_left = 0;
				if($User_Parent == "0" && $_SESSION['RegUser_ID']){
					$statement = $pdo->prepare("SELECT SUM(User_DL_Reserves) as subuserTotal FROM registeredusers WHERE User_Parent = :user_id;");
					$statement->bindParam(':user_id', $_SESSION['RegUser_ID']);
					$statement->execute();
					$amount = $statement->fetch();
					$amount = $amount['subuserTotal'];
					$subuser_credits_left = $amount ? $amount : 0;
				}
    		}
    	}
    	else{
    		echo "var loggedIn = false;\n";
    		echo "var subscriptionType = 0;\n";
    		echo "var creditsRemaining = 0;\n";
    	}
    	if(isset($_SESSION['lastSearchStuff'])){
    		$lastSearch = $_SESSION['lastSearchStuff'];
    	}
    	else{
    		$lastSearch = '';
    	}
    	echo 'var lastSearch = "' . $lastSearch . '";' . "\n";
    	$subuser_credits_left = isset($subuser_credits_left) ? $subuser_credits_left : '0';
    	echo "var subuser_credits_left = $subuser_credits_left;\n";
    ?>
    $('#account-down').dropdown();
	</script>
	<script>
		//update header initially
		$(document).ready(function(){
			$.ajax({
				dataType: 'jsonp',
				url: 'search-summary.php',
				data: {
					req: 'summary'
				}
			}).done(function(data){				
				updateHeader(data[0][0]);
			});
			$('#search-fill-in span').tooltip();
			$('#subusercredits').html('<a href="/dashboard">You have ' + subuser_credits_left.toLocaleString() + ' unused credits in subuser accounts. Click here to manage accounts.</a>');
		});

		$('.selectpicker').selectpicker();

	</script>
    <script src="/js/search-calc.js?v=20181106"></script>
    
        <!--Processing Results Modal -->
        <div class="modal fade" id="processingResultsModal" tabindex="-1" role="dialog" aria-labelledby="processingResultsLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel">Thank You for Downloading</h3>
                    </div>
                    <div class="modal-body">
                        Please give us 10-15 seconds to compile your records into a file.  (If you’re downloading more than 100,000 records, give us a few extra seconds).
                    </div>
                </div>
            </div>
        </div>

<?php include("inc/footer.php"); ?>
