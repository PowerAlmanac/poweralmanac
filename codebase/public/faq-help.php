<?php
define("TITLE", "Frequently Asked Questions");
define("DESCRIPTION", "Browse many frequently asked questions about subscription plans, your account, searching, our data, and more.");
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder container">
                    <h1>FAQ</h1>
                </div>
            </div>

            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box">
                    <ul class="nav nav-tabs" id="faqs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="subscription-tab" data-toggle="tab" aria-controls="subscription" aria-selected="true" href="#subscription">Subscription Plans</a></li>
                        <li class="nav-item"><a class="nav-link" id="account-tab" data-toggle="tab" aria-controls="account" aria-selected="false" href="#account">Account</a></li>
                        <li class="nav-item"><a class="nav-link" id="search-tab" data-toggle="tab" aria-controls="search" aria-selected="false" href="#search">Search</a></li>
                        <li class="nav-item"><a class="nav-link" id="analyze-tab" data-toggle="tab" aria-controls="analyze" aria-selected="false" href="#analyze">Analyze</a></li>
                        <li class="nav-item"><a class="nav-link" id="data-tab" data-toggle="tab" aria-controls="data" aria-selected="false" href="#data">The Data</a></li>
                        <li class="nav-item"><a class="nav-link" id="other-tab" data-toggle="tab" aria-controls="other" aria-selected="false" href="#other">Other</a></li>
                    </ul>

                    <section class="tab-content tabbed faq clearfix" id="faqsContent">
                        <div class="tab tab-pane fade p-2 show active" role="tabpanel" aria-labelledby="subscription-tab" id="subscription">
                            <ul class="bulleted spaced">
                                <li><strong>Can I subscribe online and download records right now?</strong></li><li class="answer">
                                    Heck Yeah! Go right ahead.  You’ll be subscribed in 2-3 minutes. <a href="./pricing" class="gray-btn"><span>See Pricing</span></a></li>
                                <li><strong>Is there a free subscription plan?</strong></li><li class="answer">
                                    Yes, just sign-up at no cost for the <a href="./register">Power Entry plan</a>, and then pay per record when you download them.  If you know you'll be downloading 5,000 or more records, the other plans will be more cost-effective for you.</li>
                                <li><strong>What's a "download credit"?</strong></li><li class="answer">
                                    One "download credit" allows you to download one new record. You don't need any download credits to download a record you've previously downloaded during your subscription, even if we've updated it. </li>
                                <li><strong>Is my credit card information secure?</strong></li><li class="answer">
                                    Yes. Your payment is handled entirely by PayPal, one of the most secure online payment processors. The Power Almanac never gets or sees your credit card information.</li>
                                <li><strong>Do I have to have a PayPal account to subscribe to Power Almanac?</strong></li><li class="answer">
                                    No. When you get to the PayPal site you'll see an option to use your credit card without signing up for a PayPal account. </li>
                                <li><strong>Can I pay for my subscription monthly</strong></li><li class="answer">
                                    No, the full fee is due upon sign-up. The reason is simple. When you subscribe we give you immediate access to all of your download credits. You can use them all on your first day, or throughout your subscription period.</li>
                                <li><strong>If I use up all my download credits, do I still have access to my account?</strong></li><li class="answer">
                                    Absolutely. You'll have access for the full term of your subscription, regardless of how fast you use your download credits. You will also have ongoing access to the ANALYZE function, which is especially valuable.</li>
                                <li><strong>How many times can I use the data after I download it?</strong></li><li class="answer">
                                    Unlimited, for as long as you're a Power Almanac subscriber. Of course you can't give the data to others, or sell it, or use it inappropriately. Feel free to check out our <a href="./terms-of-service">Terms of Service</a>.</li>
                                <li><strong>What if I want no limit on the number of Power Almanac records I can download?</strong></li><li class="answer">
                                    We call that our UNLIMITED plan. <a href="./contact-us">Contact us</a> if you're interested.</li>
                            </ul>
                        </div>
                        <div class="tab tab-pane fade p-2" role="tabpanel" aria-labelledby="account-tab" id="account">
                            <ul class="bulleted spaced">
                                <li><strong>Can I let colleagues in my company use my Power Almanac account?</strong></li>
                                <li class="answer">
                                    Yes. It's very easy. After you login, go to your "My Account" tab and click on "Add a new sub-user." You can assign each sub-user a certain number of your download credits, and you can edit that number at any time. Your sub-users must work for the same organization that employs you.</li>
                                <li><strong>Can I change my password?</strong></li>
                                <li class="answer">
                                    Yes. You'll find that option in your "My Account" tab.</li>
                                <li><strong>Can I change my user ID (email address)?</strong></li>
                                <li class="answer">
                                    Not on your own. If you need to do that, call our support line at 650-539-2340, ext 101.</li>
                                <li><strong>What happens to download credits that I don't use during my subscription?</strong></li>
                                <li class="answer">
                                    If you renew your subscription, those download credits roll-over to your next subscription period (plus you'll get a new set of download credits with your renewal). But once your subscription lapses, you lose any remaining download credits.</li>
                                <li><strong>If I use up the download credits that come with my account, can I buy more?</strong></li>
                                <li class="answer">
                                    Yes. When you need to purchase more download credits for a particular download file you want, you'll be instructed during the download process on how many credits you need and how to buy them. Your cost per additional download credit depends on which subscription plan you have, and you can see that cost on your "My Account" tab.</li>
                                <li><strong>How easy is it to upgrade my subscription?</strong></li>
                                <li class="answer">
                                    It's very easy; you can upgrade your subscription at any time. Just go to your "My Account" tab and click on "Upgrade now." When you upgrade, any remaining download credits will roll-over to your new "upgraded" account, plus you'll get a new set of download credits. All of your saved searches and prior download files will be preserved in your "My Account" tab. And your subscription period will restart as of the day of your upgrade.</li>
                            </ul>
                        </div>
                        <div class="tab tab-pane fade p-2" role="tabpanel" aria-labelledby="search-tab" id="search">
                            <ul class="bulleted spaced">
                                <li><strong>Can I create a customized list of local government officials before I download a file?</strong></li><li class="answer">
                                    Yes. The Power Almanac is designed to help you do just that. Click the SEARCH button at the top of the page and use the criteria there to customize your list. If you're a POWER subscriber, you'll have extra criteria (spending data and fiscal year end) plus a great ANALYZE function that will help you really fine-tune your list.</li>
                                <li><strong>Can I focus my search to local government officials from only certain states?</strong></li><li class="answer">
                                    Yes. To be clear, the Power Almanac does NOT include state government officials. But you can choose county, municipal, or township officials from only certain states. On the left side of the SEARCH tab find the LOCATION criteria and choose "states." Then select the states that have local government officials you want, and click SUBMIT to see your search results.</li>
                                <li><strong>Can I focus my search to only mayors, or only police chiefs?</strong></li><li class="answer">
                                    Yes. In fact the Power Almanac does better than that because it classifies all officials based on roles rather than titles. Titles are inaccurate. For example, many "top elected officials" for local governments do not have the title "mayor." On the left side of the SEARCH tab find the ROLES criteria and pick the role(s) you want, and click SUBMIT. You don't have to worry about exact titles.</li>
                                <li><strong>Can I focus my search on just larger cities? Or just small townships?</strong></li><li class="answer">
                                    Yes. On the left side of the SEARCH tab find the POPULATION and TYPE criteria. You can choose just counties, townships, or municipalities (cities, villages, etc.). You can also choose the population range(s) that you want. Just click SUBMIT after you've made your selections and see the search results.</li>
                                <li><strong>Can I focus my search based on how much a local government spends?</strong></li><li class="answer">
                                    Yes, POWER subscribers can use the Power Almanac's spending criteria (on the SEARCH tab), which includes spending data for each of the 21,000 local governments in the Power Almanac database. The data covers 41 spending categories. If you're not a POWER subscriber, you can see the list of spending categories by going to the "Compare Spending" tables on the ANALYZE tab.</li>
                                <li><strong>Can I include only officials with email addresses in my search results?</strong></li><li class="answer">
                                    Yes. On the SEARCH page, in the "Your Search Results" box, there is an ON/OFF switch that allows you to include only records that have email addresses.</li>
                                <li><strong>Can I search by title?</strong></li><li class="answer">
                                    No. The Power Almanac classifies officials based on their roles, not their titles. We're confident that you'll find this to be a far more powerful and precise method of finding the officials you want, because titles are inaccurate. For example, a "Supervisor" can be the head of public works, or the top elected official in a city, or a member of a county's governing board. But don't worry - your download file will include a title (along with "role" and other information) for each official.</li>
                                <li><strong>Can I get an idea of what kind of records are included in my search results?</strong></li><li class="answer">
                                    Yes. On the SEARCH tab, in the white "Your Search Results" box, just click on "Preview 25" to see 25 random records from your search results. You can see governments, titles, and the type of contact details that will be included in the download file.</li>
                            </ul>
                        </div>
                        <div class="tab tab-pane fade p-2" role="tabpanel" aria-labelledby="analyze-tab" id="analyze">
                            <ul class="bulleted spaced">
                                <li><strong>How can the ANALYZE function help me?</strong></li><li class="answer">
                                    Use it to get a detailed breakdown of what's included in your search results. Or use it to understand the size of sub segments of the local government market. Or use it to financially benchmark different types of local governments. And we think you'll probably come up with a bunch more ways to use ANALYZE.</li>
                                <li><strong>Can the Power Almanac break down my search results by state?</strong></li><li class="answer">
                                    Yes. Complete a search on the SEARCH page, then click on ANALYZE. You can see a breakdown of your search results by state, and by almost every other search criteria you use on the SEARCH page.</li>
                                <li><strong>Can I use the ANALYZE function for benchmarking?</strong></li><li class="answer">
                                    Yes. Complete a search in the SEARCH page, then click on ANALYZE. Benchmarking can be especially valuable when looking at spending data. See how much certain types of local governments spend compared to others. See how much more high-spending (highest quartile) local governments spend compared to low-spending (lowest quartile). Use absolute or per capita spending.</li>
                                <li><strong>Can I save a table that I've created for future reference?</strong></li><li class="answer">
                                    Yes. On the ANALYZE tab you can create many different tables to analyze your search results. If you create a table that you'll want to reference later, just save it.</li>
                                <li><strong>Can I export a table to excel?</strong></li><li class="answer">
                                    Yes. When you're in the ANALYZE tab just click on the "export" button, and the displayed table will be instantly exported to excel.</li>
                            </ul>
                        </div>
                        <div class="tab tab-pane fade p-2" role="tabpanel" aria-labelledby="downloads-tab" id="downloads">
                            <ul class="bulleted spaced">
                                <li><strong>Can I download any records for free?</strong></li><li class="answer">
                                    Yes. Just <a href="./register">register</a> for a free BASIC account, and you'll have 50 free download credits to use.</li>
                                <li><strong>Does downloading a file take less than 30 seconds?</strong></li><li class="answer">
                                    Yes. In almost every case the download file will be ready in 5-10 seconds. Larger files (more than 25,000 records) will take longer, but rarely (if ever) longer than 30 seconds.</li>
                                <li><strong></strong>What's the file format of the download file?</li></li><li class="answer">
                                    Each download comes in two formats: CSV (which can be easily opened with Excel) and TXT (which might be preferred if you're incorporating the data with another program).</li>
                                <li><strong>How much does it cost to download the same file again later?</strong></li><li class="answer">
                                    Nothing, as long as you're an active Power Almanac subscriber and you're downloading the file again within 12 months of the first time you downloaded it. Just go to your "My Downloads" tab, find the file you want, and download it. In fact, you can even download an updated version at no charge (with records that have been updated since the last time you downloaded it).</li>
                                <li><strong>What happens if the Power Almanac updates a record after I've downloaded it?</strong></li><li class="answer">
                                    We'll let you know. In your "My Downloads" tab we tell you how many records we've updated for each of your download files. Then you can download the file again - with the updated records - at no charge (as long as you're an active Power Almanac subscriber and it's been less than 1 year since your previous download).</li>
                                <li><strong>Are officials' email addresses included in a download file?</strong></li><li class="answer">
                                    Yes. We have emails for about 73% of the officials included in the Power Almanac. If we have the email for an official in your search results, it will be included in your download file, and you can use it (responsibly please!) as much as you'd like. Just be aware that in some cases (especially with smaller governments), officials share an email address.</li>
                                <li><strong>Can I see a sample download file?</strong></li><li class="answer">
                                    Yes. Here's a <a href="http://www.poweralmanac.com/SampleDLFile.xlsx">sample download file</a>. Just keep in mind that the records shown in this sample file are NOT real.</li>
                            </ul>
                        </div>
                        <div class="tab tab-pane fade p-2" role="tabpanel" aria-labelledby="data-tab" id="data">
                            <ul class="bulleted spaced">
                                <li><strong>Does the Power Almanac include data for every county, municipality, and township in the US?</strong></li><li class="answer">
                                    Not quite. The Power Almanac covers 21,000 local governments in the US, including 97% of the counties, municipalities, and townships with a population of 1,000 or more. We also have close to 500 local governments from communities of less than 1,000 people. We haven't focused on these smaller governments yet. Let us know if you think we should.</li>
                                <li><strong>How many different types of officials does the Power Almanac include?</strong></li><li class="answer">
                                    We researched 10 roles for every one of the 21,000 local governments in the Power Almanac. Every local government includes a "top elected official." We also looked for governing board members (typically 3-11 per government, but sometimes more or less than that) and 8 other roles that you can see listed under the ROLES criteria on the SEARCH tab. If a local government had nobody in a particular role, we captured that as "role doesn't exist." Of course roles that "don't exist" won't show up in your search results or download files, but you can see those shown on the individual government profile pages.</li>
                                <li><strong>What if an official serves in more than one role?</strong></li><li class="answer">
                                    This certainly happens, especially in smaller governments. We list an official separately for each role they serve in. For example, if a city clerk serves as the "clerk" and also as the "head of finance," then that city clerk will show up in the search results for both of those roles. In that way you won't ever miss reaching a decision-maker in the role you're looking for.</li>
                                <li><strong>How did the Power Almanac get this data?</strong></li><li class="answer">
                                    The information about officials was gathered from public resources (e.g. local government websites) and in almost every case also confirmed by phone. In a smaller number of cases we confirmed via fax or email. Confirming is critical, because many local governments don't have a website, or their site is out-of-date and/or incomplete. Raw local government financial data comes from the US Census Bureau. Demographic data is provided to us by Onboard Informatics.</li>
                                <li><strong>How will the Power Almanac maintain the quality of this data?</strong></li><li class="answer">
                                    Maintaining the high quality of the Power Almanac is our top priority. We will continue calling local governments to re-confirm (and update as needed) every record in the database approximately every 6 months. That means we'll be re-confirming every record 2 times per year. We're not relying on news alerts or website searches to do our updates. That's just not reliable or thorough enough.</li>
                                <li><strong>Is the Power Almanac data 100% accurate?</strong></li><li class="answer">
                                    No. If we said "yes" you wouldn't believe us. Every day officials somewhere in the US are changing jobs or moving offices or getting new emails. But we will strive to be highly accurate, and certainly more accurate and more comprehensive than any other source of local government information.</li>
                                <li><strong>How will I know if the Power Almanac has added or updated records that I care about?</strong></li><li class="answer">
                                    We'll tell you. In your "My Downloads" and "My Saved Searches tabs we tell you how many records we've updated for each of your download files or saved searches. In the same places you can also see how many new records meet your criteria. This way you'll always know what's going on with the records you care about.</li>
                                <li><strong>Is the mailing address for government officials typically the same as the main government mailing address?</strong></li><li class="answer">
                                    There is no "typically" when it comes to local governments and officials. For some officials it's the same. But in many cases officials have separate and distinct mailing addresses (and phone numbers). This can be true for officials of larger local governments (which might have multiple locations) and smaller local governments (where many officials use their personal contact information for government business).</li>
                            </ul>
                        </div>
                        <div class="tab tab-pane fade p-2" role="tabpanel" aria-labelledby="other-tab" id="other">
                            <ul class="bulleted spaced">
                                <li><strong>Will the Power Almanac keep my registration information private?</strong></li><li class="answer">
                                    Yes. The small amount of personal information we collect from you we use for our own business purposes only. We do NOT sell your information to others. Feel free to read our privacy policy.</li>
                                <li><strong>What if I still have more questions?</strong></li><li class="answer">
                                    Ask away. <a href="./contact-us">Reach out to us</a> by email or phone and let us know what's on your mind.</li>
                            </ul>
                        </div>
                    </section>

                    <div class="row subfooter mt-4">
                        <div class="col-lg-9 col-md-8 col-sm-7 text-center text-sm-left">
                            <strong class="title">Already Know it All? Try Power Almanac</strong>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-5 text-center text-sm-right">
                            <a href="/search-government" class="btn btn-secondary" role="button">Search for Officials</a>
                        </div>
                    </div>

                </div>
            </div>
<?php include("inc/footer.php"); ?>            
