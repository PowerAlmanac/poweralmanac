<?php

include("inc/config.php");
include_once('inc/mandrill.php');

$title = 'Activate Registration';

// Right now, the code is passing the $email in the activation process through $_GET, which prevents us from using "+" signs
// in emails, since the URL will interpret it as a space. This hack switches spaces to +'s so that it works.
$eMail = str_replace(" ", "+", $_REQUEST['e']);

$actCode = Sanitize::variable($_REQUEST['actCode']);


// Sanitize
$eMail = Sanitize::variable($eMail, "@.-+");
// Not sure what the $actCode is or how to sanitize it.

$firstTime = 0;

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
	exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$read_sql = sprintf("SELECT * FROM registeredusers
	WHERE User_Email = '%s'
	LIMIT 1
", escape($eMail));
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$row = PowerAlmanac\PDb::fetch_array($result_read);
$User_Activated = $row['User_Activated'];
$User_DL_Reserves = $row['User_DL_Reserves'];
$User_Subscription = $row['User_Subscription'];

if ($User_Activated == '0') {
	// disabled on 7/29/13 after pricing changes
	//$newReserves = $User_DL_Reserves + 50;
	$newReserves = 0;
	$firstTime = 1;
}

// add 50 credits to account if not activated yet

//$found = $newUser->Validate_User($actCode,$eMail);
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL Validate_User('%s','%s',@RC)", escape($actCode), escape($eMail)));
$pdoObject = $pdo->query("SELECT @RC");
$rsArray = $pdoObject->fetchAll();
$RC = $rsArray[0]['@RC'];

if ($RC == '1') {
	// not found
	$activationMessage = "<b>INVALID</b> activation code. Your registration may have been de-activated due to inactivity. <br /> Please try <a href='register.php'>registering again</a>.";
} else {
	// validated
	$nowIsUNIX = time();
	$laterIsUNIX = $nowIsUNIX + (60*60*24*366);
	$sixLaterIsUNIX = $nowIsUNIX + (60*60*24*183);
	$nowIs = date("Y-m-d H:i:s",$nowIsUNIX);
	$laterIs = date("Y-m-d H:i:s",$laterIsUNIX);
	$sixLaterIs = date("Y-m-d H:i:s",$sixLaterIsUNIX);
	if ($firstTime == 1) {
		$affiliateCode = substr($actCode,0,2);
		if ($affiliateCode == 'PO') {
			// private offer 2012
			$endOfYear2012 = '2013-01-01 00:00:00';
			if ($User_Subscription == '10') {
				$update_sql = sprintf("UPDATE registeredusers SET DateTime_SubscriptionStart = now(), DateTime_SubscriptionEnd = '%s'
					WHERE User_Email = '%s'
				", escape($laterIs), escape($eMail));
			} else {
				$update_sql = sprintf("UPDATE registeredusers SET DateTime_SubscriptionStart = now(), DateTime_SubscriptionEnd = '%s'
					WHERE User_Email = '%s'
				", escape($laterIs), escape($eMail));
			}
			$result_update = @PowerAlmanac\PDb::query($update_sql);
			if (!$result_update) {
				die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
			}
		} else {
			// normal - add 50 credits to account if not activated yet
			$update_sql = sprintf("UPDATE registeredusers SET User_DL_Reserves = '%s', DateTime_SubscriptionStart = now(), DateTime_SubscriptionEnd = '%s'
				WHERE User_Email = '%s'
			", escape($newReserves), escape($laterIs), escape($eMail));
			$result_update = @PowerAlmanac\PDb::query($update_sql);
			if (!$result_update) {
				die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
			}
		}
	}
	$activationMessage = "<b>SUCCESSFUL</b> activation code. Your registration has been activated. <br /> Please <a href='login.php'>log in</a> to start using your account.";
	$_SESSION['login_message'] = '';
	
	
	$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
	$result = $pdo->query(sprintf("SELECT User_FirstName FROM RegisteredUsers WHERE RegisteredUsers.User_Email='%s' LIMIT 1;", escape($eMail)));
	foreach($result as $row){
		$firstName = $row['User_FirstName'];
	}
	
	 $message = file_get_contents(__DIR__ . '/emailtemplates/welcome.html');
	 $message = str_replace('[NAME]', $firstName, $message);
	 $message = str_replace('[EMAIL]', $eMail, $message);
     $message = str_replace('[URL]', 'https://' . $_SERVER['HTTP_HOST'], $message);

	 $text = <<<TEXT
Hi {$firstName}


Congratulations on creating your Power Almanac account.  You’ll now find it easy to contact local government decision-makers! 

Please feel free to contact me by email (ron@poweralmanac.com) or phone (650.539.2342) if you have any questions or feedback.

We're very proud of the quality of our data, and look forward to hearing how it contributes to your success!

Thanks for your interest!
TEXT;

	 $email_data = array(
	 	'to' => $eMail,
	 	'from' => array('registration@poweralmanac.com' => 'Ron from Power Almanac'),
	 	'subject' => 'Welcome to Power Almanac!',	
	 	'html' => $message,
        'replyTo' => 'ron.mester@poweralmanac.com',
        'text' => $text
     );
	
	mandrill_send($email_data);
	
	/****** Automatic login needs to take place ******/
	// We unfortunately need to load PAProcessLogIn here because the login is developed poorly
	// This means we need to create false $_POST values so that PA-ProcessLogIn can evaluate it
	$_POST['eMail'] = $eMail;
	
	// Okay, $_POST['passWord'] can't be passed securely, since this page is loaded via header.
	// So instead we're going to have to create an auto-login cheater and update PA-ProcessLogIn
	// to interpret it properly for registration ONLY.
	define("REGISTRATION_AUTO", true);
	
	require_once("./PA-ProcessLogIn.php"); exit;
}


include("inc/header-search.php");
?>

            <div class="intro">
                <div class="intro-holder center login">
                    <h3><?php echo $activationMessage ?></h3>
                </div>
            </div>   
