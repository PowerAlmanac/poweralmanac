<?php
header('Content-type: application/json');
ob_start('ob_gzhandler');
include("../inc/config.php");
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));

$zip = !empty($_GET['zip']) ? $_GET['zip'] : false;

function outputResult($result = '') {
    echo $_GET['callback'] . '('.json_encode(array('result' => $result)).')';
    ob_end_flush();
    exit;
}

if ($zip === false || strlen($zip) !== 5) {
    outputResult('BAD_ZIPCODE');
}

$stmt = $pdo->prepare("SELECT ZipCode from zip_code2 WHERE ZipCode=:ZipCode");
$stmt->execute(array('ZipCode' => $_GET['zip']));
$result = $stmt->fetchColumn();

// Invalid zip code
if ($result === false) {
    outputResult('UNKNOWN_ZIPCODE');
} else {
    outputResult('KNOWN_ZIPCODE');
}

