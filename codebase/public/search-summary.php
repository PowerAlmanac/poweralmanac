<?php

	header('Content-type: application/json');
	ob_start('ob_gzhandler');
	include("inc/config.php");
	$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));

	if(isset($_GET['req'])){
		//get totals
		$pdo->query("CALL DatabaseSummary(@Result)"); 
		$pdoObject = $pdo->query("SELECT @Result");
		$rsArray = $pdoObject->fetchAll();
		$summaryString = str_replace("`",'"',$rsArray[0][0]);
		echo $_GET['callback'] . '('.json_encode($rsArray).')';
		ob_end_flush();
	}
	else{

		//if requesting a filtered search (most of this is copied from search-government.php)
		//ini_set('display_errors','On');
		//error_reporting(E_ALL);
		
		if(isset($_GET['params']))
		{
			$searchParams = $_GET['params'];
			//store parameters for checkout
			$_SESSION['lastSearchStuff'] = $searchParams;
			//store the parameters for compatibility with old pages
			$_SESSION['lastSearchParams']  = $searchParams;
		}
		else
		{
			$searchParams = '';
		}
		
		if ($_SESSION['logged_in'] != '1')
		{
			$User_Subscription = '0';
			$_SESSION['user_subscription'] = '0';
			$_SESSION['RegUser_ID'] = '0';
			$RegUser_ID = 0;
			$timeIs = time();
			$tableID = $RegUser_ID . '_' . substr($timeIs,-6);
		}
		else
		{
			$User_Subscription = $_SESSION['user_subscription'];
			$RegUser_ID = $_SESSION['RegUser_ID'];
			$timeIs = time();
			$tableID = $RegUser_ID . '_' . substr($timeIs,-6);
		}
		
		//echo("DEBUG: Results - Search Summary request ($newSearch:$useSavedSearchParams)<br>");
		$useCache = 0;
		// handling search result tables
		if (isset($_SESSION['previousTableID']))
		{
			// delete previous table
			$previoustableID = $_SESSION['previousTableID'];
			$pdo->query(sprintf("CALL Delete_Research_Tables('%s')", escape($previoustableID)));
			// now
			$_SESSION['previousTableID'] = $tableID;
			$_SESSION['currentTableID'] = $tableID;
		} else {
			// first time
			$_SESSION['previousTableID'] = $tableID;
			$_SESSION['currentTableID'] = $tableID;
		}


		$pdo->query(sprintf("CALL SearchSummary2('%s','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($tableID), escape($searchParams)));
		//echo("<p>DEBUG: CALL SearchSummary('$tableID',true,'$searchParams',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists)</p>");
		
		$pdoObject = $pdo->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists");
		$rsArray = $pdoObject->fetchAll();

		// check for records already downloaded and subtract from totals
		if($RegUser_ID)
		{
			$pdo->query(sprintf("CALL CheckPaymentForCurrentDownload('%s',%s,@paid,@unpaid)", escape($tableID), escape($RegUser_ID)));
			$pdoObject = $pdo->query("SELECT @paid,@unpaid");
			$checkPaymentArray = $pdoObject->fetchAll();
			$paid = $checkPaymentArray[0]['@paid'];
			$unpaid = $checkPaymentArray[0]['@unpaid'];
		}
		else{
			$paid = 0;
		}

		//print_r($rsArray);
		$JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
		$JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
		$JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];
		$num = json_decode($JSON_SUMMARY);
		if(isset($num)){
			$_SESSION['lastSearchNumMatched'] = $num->Num_Matched_Officials;
		}
		
		//store last search into session
		$_SESSION['checkout'] = $rsArray[0]['@JSON_SUMMARY'];
		$_SESSION['checkout_paid_records'] = $paid;
		
		//send a random number to the page as a last resort to be sure that the user has the same checkout that the session has
		$_SESSION['checkout_secure'] = rand(10000000,99999999);

		$smallerArray[0] = array('@JSON_SUMMARY' => $JSON_SUMMARY, '@JSON_OFFICIALS' => $JSON_OFFICIALS, '@scrvr' => $_SESSION['checkout_secure'], 'paid_records' => $paid);

		$SearchSummaryFLAG = $rsArray[0]['@flag'];
		$table_existsFLAG = $rsArray[0]['@table_exists'];
		echo $_GET['callback'] . '('.json_encode($smallerArray).')';
		ob_end_flush();

	}

?>
