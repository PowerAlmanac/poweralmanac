<?php

include("inc/config.php");
include("inc/protect-login.php");

$namedSearch = $_REQUEST['savesearch_label'];

$origNamedSearch = $namedSearch;

$user_email = $_SESSION['user_email'];
$searchParams = $_SESSION['lastSearchParams'];
$numRecords = $_SESSION['lastSearchNumMatched'];

$User_Email = $_SESSION['user_email'];
$RegUser_ID = $_SESSION['RegUser_ID'];

//echo($namedSearch);
//echo("$user_email<br>");
//echo("$namedSearch<br>");
//echo("$searchParams<br>");
//echo("$numRecords<br>");

$namedSearch = str_replace("'","-",str_replace("`","-",$namedSearch));

// Sanitize
$namedSearch = Sanitize::variable($namedSearch, " ,-+;.@?!");




$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// check if search name already used
$readSearches_sql = sprintf("SELECT * FROM SavedSearches
	WHERE RegUser_ID = '%s' AND Search_Name = '%s'
", escape($RegUser_ID), escape($namedSearch));
$result_readSearches = @PowerAlmanac\PDb::query($readSearches_sql);
if (!$result_readSearches) {
	die('Error reading from SavedSearches database:' . PowerAlmanac\PDb::error());
}
$num_rows = PowerAlmanac\PDb::num_rows($result_readSearches);
if ($num_rows != '0') {
	// conflict
	$_SESSION['savesearch_message'] = "You already have a saved search named '$origNamedSearch'. Please save it with another name.";
	header("Location: PA-SaveSearch.php");
	flush();
	exit;
}

$_SESSION['savesearch_message'] = '';
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL SaveCurrentSearch('%s','%s','%s',%s,@RC)", escape($user_email), escape($namedSearch), escape($searchParams), escape($numRecords)));

// update last search run date
$update_sql = sprintf("UPDATE savedsearches SET DateTime_Rerun = now()
	WHERE Search_Name = '%s' AND RegUser_ID = '%s'
", escape($namedSearch), escape($RegUser_ID));
$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating savedsearches: '. PowerAlmanac\PDb::error().'</p>');
}

header("Location: dashboard.php");
flush();


?>
