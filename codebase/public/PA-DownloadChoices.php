<?php

include("inc/config.php");
include("inc/protect-login.php");

$record_id = $_REQUEST['id'];

$User_Email = $_SESSION['user_email'];
$User_FirstName = $_SESSION['user_firstname'];
$User_Subscription = $_SESSION['user_subscription'];
$RegUser_ID = $_SESSION['RegUser_ID'];
$Parent_User_ID = $_SESSION['User_Parent'];

// Sanitize
$record_id = Sanitize::number($record_id);


$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$readDownloads_sql = sprintf("SELECT * FROM SavedDownloads
	WHERE RegUser_ID IN ('%s','%s') AND SavedDownloads_ID = '%s'
	LIMIT 1
", escape($RegUser_ID), escape($Parent_User_ID), escape($record_id));
$result_readDownloads = @PowerAlmanac\PDb::query($readDownloads_sql);
if (!$result_readDownloads) {
	die('Error reading from SavedDownloads database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readDownloads);

$Download_UpdatedRecords = $onerow['Download_UpdatedRecords'];
$Download_NewRecords = $onerow['Download_NewRecords'];
$Download_FileName = $onerow['Download_FileName'];
$downloadChoice1 = "../dl/$Download_FileName";

?>
<html>
<head>
<title>Download Choices</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="popuphelp.js"></script>
<script type="text/javascript">
function tb_remove() {
 	$("#TB_imageOff").unbind("click");
	$("#TB_closeWindowButton").unbind("click");
	$("#TB_window").fadeOut("fast",function(){$('#TB_window,#TB_overlay,#TB_HideSelect').trigger("unload").unbind().remove();});
	$("#TB_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		$("body","html").css({height: "auto", width: "auto"});
		$("html").css("overflow","");
	}
	document.onkeydown = "";
	document.onkeyup = "";
	return false;
}
</script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
</head>
</head>

<body bgcolor="#ACC6E1" background="images/ACC6E1-pixel.jpg">

<table width="800" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#ACC6E1">
<tr>
	<td colspan="3" valign="top" bgcolor="#ACC6E1">
    <br><font style="font-size:16px; font-weight:bold">Your DOWNLOAD Choices</font> 
    <a href="#" onClick="window.parent.Shadowbox.close()"><img src="images/preview-close.jpg" alt="Close Preview" width="40" height="40" hspace="5" vspace="0" border="0" align="right"></a>
	</td>
</tr>
<tr bgcolor="#CCCCCC">
	<td bgcolor="#99FFFF" width="33%">
	<font color="#7591AE" style="font-size:14px"><b><u>Choice 1: AS IS</u></b></font>
	</td>
	<td bgcolor="#FFCC66" width="33%">
	<font color="#7591AE" style="font-size:14px"><b><u>Choice 2: UPDATED</u></b></font>
	</td>
	<td bgcolor="#FFFFCC" width="33%">
	<font color="#7591AE" style="font-size:14px"><b><u>Choice 3: UPDATED + NEW</u></b></font>
	</td>
</tr>
<tr bgcolor="#7492AE" height="15">
	<td valign="top">
	<font color="#FFFFFF" style="font-size:12px">
    Download the file <b>AS IS</b> (you will NOT get the updated version of each record)
    <br><br>
    <center>
    <?php
	echo("<a href='PA-ValidateAsIs.php?id=$record_id' target='_parent'><font color='#FFFFFF'><b>Download AS IS now</b></font></a>");
	?>
    <!-- <a href="<?php echo($downloadChoice1); ?>" target="_blank"><font color="#FFFFFF"><b>Download AS IS now</b></font></a> -->
    </center>
    </font>
	</td>
	<td valign="top">
	<font color="#FFFFFF" style="font-size:12px">
    Download the file with the most UPDATED version of each record.
    <br><br>
    <center>
    <?php
	if ($Download_UpdatedRecords != 0) {
		echo("<a href='PA-ValidateUpdatesOnly.php?id=$record_id' target='_parent'><font color='#FFFFFF'><b>Download UPDATED now</b></font></a>");
	} else {
		echo("<font color='#FFFFFF'><i>No UPDATES available for this download.</i></font>");
	}
	?>
    </center>
    </font>
	</td>
	<td valign="top">
	<font color="#FFFFFF" style="font-size:12px">
    Download the file with the most UPDATED version of each record, PLUS NEW records that fit the same search criteria will also be included.
    <br><br>
    <center>
    <?php
	if ($Download_NewRecords != 0) {
		echo("<a href='PA-ValidateFullSet.php?id=$record_id' target='_parent'><font color='#FFFFFF'><b>Download UPDATED +NEW now</b></font></a>");
	} else {
		echo("<font color='#FFFFFF'><i>No NEW records available for this download.</i></font>");
	}
	?>
    </center>
    </font>
	</td>
</tr>
<tr>
	<td colspan="3" align="center" bgcolor="#FFFFFF">
    NOTE:  There is NO charge for downloading any records that you've already downloaded within the past 12 months.
    </td>
</table>


</body>
</html>
