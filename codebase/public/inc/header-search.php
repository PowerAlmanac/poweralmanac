<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=(defined("TITLE") ? TITLE . " - Power Almanac" : "Power Almanac");?></title>
	<?=(defined("DESCRIPTION") ? '<meta name="description" content="' . DESCRIPTION .'">' : "");?>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link media="all" rel="stylesheet" type="text/css" href="/css/all.css?v=20190114" />
	<link media="all" rel="stylesheet" type="text/css" href="/css/colorbox.css" />
	<link media="all" rel="stylesheet" type="text/css" href="/css/bootstrapSwitch.css" />
    <link media="all" rel="stylesheet" type="text/css" href="/css/bootstrap-select.min.css" />
    <!--[if IE 8]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"/><![endif]-->

    <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script src="/js/jquery.scrollTo.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-select.min.js"></script>


    <script src="//cdn.optimizely.com/js/942881400.js"></script>
   	<?=(defined("CUSTOMHEADER") ? CUSTOMHEADER : "");?>

<meta name="google-site-verification" content="h1oGPpIsOkOvQn8gVwqV93s_JGaRsOtc7duViMFVsbg" />

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Hotjar Tracking Code for https://www.poweralmanac.com/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1051388,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

	<script>
	    $.fn.selectpicker.Constructor.BootstrapVersion = '4';
	</script>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="20">
	<div id="wrapper" class="allwhite">
		<!-- header -->
		<div id="header" class="clearfix container">
                <nav class="navbar navbar-expand-lg navbar-light mt-2 mb-2">
                    <strong class="logo navbar-brand"><a href="/">Power Almanac</a></strong>


                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item"><a class="nav-item nav-link" href="./search-government">DATA</a></li>
                            <li class="nav-item"><a class="nav-item nav-link" href="./how-it-works">HOW IT WORKS</a></li>
                            <li class="nav-item"><a class="nav-item nav-link" href="./pricing">PRICING</a></li>
                            <li class="nav-item"><a class="nav-item nav-link" href="./faq-help">FAQ</a></li>
                        </ul>

					<?php
					if (empty($_SESSION['user_email'])) {
                        echo '<div class="btn-group login-menu" role="group">
                          <a href="/register" class="btn btn-primary">Register Free!</a>
                          <a href="/login" class="btn btn-secondary">Log In</a>
                        </div>';
					} else {
					  echo '<span class="mr-2">
					    Welcome, ' . $_SESSION['user_firstname'] . '

					  </span>
					  <div class="btn-group login-menu">
					    <a href="/dashboard.php" class="btn btn-secondary">Dashboard</a>
					    <a href="/PA-LogOut.php" class="btn btn-secondary">Sign Out</a>				    
					  </div>';
					}
					?>

                    </div>
                </nav>
		</div>
		<div id="main">
