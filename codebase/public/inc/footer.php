<script>
  window.intercomSettings = {
    app_id: "zbhjdh8a"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/zbhjdh8a';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

	<!-- Google Code for Remarketing Tag -->
	<!--------------------------------------------------
	Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 962092992;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/962092992/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>

</div>
		<!-- footer -->
		<div id="footer">
			<div class="container footer-holder">

                <div class="row">
                    <!-- search form -->
                    <div class="col-sm-12 col-md-5 col-lg-6 m-b-20">
                        <div class="footer-box">
                            <form action="PA-PRG.php" method="post" class="form-search form form-inline">
                                <fieldset class="w-64">
                                    <strong class="title">Who Are You Looking For? <img src="images/ico01.png" width="29" height="30" alt="" /></strong>
                                    <div class="form-group">
                                        <?=(isset($officialDrop) ? $officialDrop : '');?>
                                        <input type="submit" class="btn btn-primary" value="Go &#187;" />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>

                    <div class="col-8 col-md-5 col-lg-4 m-b-20"><!-- social box -->
                        <div class="footer-box social-box">
                            <strong class="title">Connect with us</strong>
                            <ul>
                                <!-- <li><a href="#" class="rss">RSS</a></li> -->
                                <li><a href="https://www.facebook.com/PowerAlmanac" class="facebook">Facebook</a></li>
                                <li><a href="http://twitter.com/PowerAlmanac" class="twitter">Twitter</a></li>
                                <li><a href="http://www.linkedin.com/company/2308456?trk=tyah" class="linkedin">Linkedin Follow</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-4 col-md-2 col-lg-2 m-b-20"><!-- back to top -->
                        <div class="footer-box back-to-top">
                            <a href="#" class="btn-to-top">Back to Top</a>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-4 m-b-20">
                        <div class="footer-box contact-box">
                            <address>
                                205 De Anza Blvd, #59
                                San Mateo, CA 94402<br />
                                650-539-2340
                                <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#080;&#111;&#119;&#101;&#114;&#065;&#108;&#109;&#097;&#110;&#097;&#099;&#046;&#099;&#111;&#109;">&#105;&#110;&#102;&#111;&#064;&#080;&#111;&#119;&#101;&#114;&#065;&#108;&#109;&#097;&#110;&#097;&#099;&#046;&#099;&#111;&#109;</a>
                            </address>
                            <p>Got any helpful tips about how we can improve?</p>
                            <div class="btn-holder">
                                <a href="./contact-us" class="btn btn-warning">Contact us</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-8 m-b-20">
                        <div class="footer-box footer-content">
                            <strong class="title"><a href="./about-us">About Us</a></strong>
                            <p>The idea behind the Power Almanac is pretty simple. Create the most comprehensive and up-to-date online source of data and information about local governments, and make it very easy to use. Of course, simple ideas are often challenging to implement, and that's certainly been  true for the Power Almanac <a href="./about-us">... read more</a></p>
                        </div>
                    </div>
                </div>

				<!-- footer navigation -->
				<ul class="footer-nav">
					<li><a href="./about-us">About Us</a></li>
					<li><a href="./contact-us">Contact Us</a></li>
					<li><a href="./terms-of-service">Terms of Service</a></li>
					<li><a href="./privacy-policy">Privacy Policy</a></li>
				</ul>
				<span class="copyright">&copy; Copyright <?php echo date("Y"); ?> LTBL, LLC. All rights reserved. POWER ALMANAC and the POWER ALMANAC logo are trademarks of LTBL, LLC.</span>
				<div class="right">
					<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
						<tr>
						<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.poweralmanac.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script><br />
						<a href="http://www.symantec.com/ssl-certificates" target="_blank"  style="color:white; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	
	<script>
	$(document).ready(function() {
		if($("#hoverMenu").length > 0 && $(window).width() > 400) {
            $("#hoverMenu").addClass("sticky-top");
		}

		var offset = 50;

		$("[href='#']").click(function(event) {
			 $.scrollTo(0,300);
		});

		$(".form-search").submit(function(){
			var sel = $("[name='official_role'] option:selected",this);
			var value = sel.attr("value");
			var name = sel.attr("data-name");
			 var input = '<input type="hidden" name="' + name + '" value="' + value + '" />';
			 $(this).append(input);
		});

		//tabs for faq page
		if($('.tabbed').length>0){
			var tabs = $('#main').find('.tab');
			// $(tabs).hide();
			if(window.location.hash){
				$(window.location.hash).show();
				$('.top-tabs').find('[href='+window.location.hash+']').addClass('selected-tab');
			}
			else{
				$(tabs[0]).show();
				$($('.top-tabs').find('.tablink')[0]).addClass('selected-tab');
			}
			
			$(tabs).find('.answer').hide();
			


			//handle click between tabs
			$('.top-tabs').find('.tablink').on('click',function(e){
				e.preventDefault();
				$('.top-tabs').find('.tablink').removeClass('selected-tab');
				$(this).addClass('selected-tab');
				var tabId = $(this).attr('href');
				window.location.hash = tabId;
				$(tabs).hide();
				$(tabId).show();
			});

			//handle open / close answers
			$('.tabbed').on('click','li:not(.answer)', function(){
				var answer = $(this).next('.answer');
				if(answer.is(':hidden')){
					answer.fadeIn(350);
					$(this).addClass('hide-up');
				}
				else{
					answer.fadeOut(350);
					$(this).removeClass('hide-up');
				}
			})
		}

		$('.btooltip').tooltip();
	});

	</script>

	<?php if(!empty($footer_js)){
		echo $footer_js;
	}
	?>
 <script type="text/javascript">

      var _gaq = _gaq || [];
      // old nick
      //_gaq.push(['_setAccount', 'UA-21974562-1']);
      _gaq.push(['_setAccount', 'UA-26302434-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script> 
    
    
    <!-- KISSmetrics tracking snippet -->
<script type="text/javascript">var _kmq = _kmq || [];
var _kmk = _kmk || '89a359f2c4e182ee91b6aa59d0b21cabfb51b95e';
function _kms(u){
  setTimeout(function(){
    var d = document, f = d.getElementsByTagName('script')[0],
    s = d.createElement('script');
    s.type = 'text/javascript'; s.async = true; s.src = u;
    f.parentNode.insertBefore(s, f);
  }, 1);
}
_kms('//i.kissmetrics.com/i.js');
_kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
</script>

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('8961-875-10-8869');/*]]>*/</script><noscript><a href="https://www.olark.com/site/8961-875-10-8869/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

<script type="text/javascript" src="//s3.amazonaws.com/scripts.hellobar.com/12dfd069615decb7d1ebb6315073dc75d286985d.js"></script>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0023/1488.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<?php if(!empty($_SESSION['user_email'])){ ?>
	<script>
	  window.intercomSettings = {
	    app_id: "zbhjdh8a"
	};
	</script>
	<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/zbhjdh8a';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<?php } ?>

<?php include __DIR__ . '/googletagmanager.php'; ?>

<script type="text/javascript">

    _linkedin_partner_id = "572428";

    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];

    window._linkedin_data_partner_ids.push(_linkedin_partner_id);

</script><script type="text/javascript">

    (function(){var s = document.getElementsByTagName("script")[0];

        var b = document.createElement("script");

        b.type = "text/javascript";b.async = true;

        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";

        s.parentNode.insertBefore(b, s);})();

</script>

<noscript>

    <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=572428&fmt=gif" />

</noscript>
</body>
</html>
