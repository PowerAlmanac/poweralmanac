<?php
require_once __DIR__ . '/../../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();

require_once(__DIR__ . "/../class/Sanitize.php");

$useReCaptcha = 1;
date_default_timezone_set('America/Chicago');

// debug - localhost
$serverURL = PowerAlmanac\Config::env('server_url');
$recaptcha_publickey = PowerAlmanac\Config::env('recaptcha_publickey');
$recaptcha_privatekey = PowerAlmanac\Config::env('recaptcha_privatekey');

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name');

$sandbox = PowerAlmanac\Config::env('sandbox');

error_reporting(E_ALL);

if (isset($_SESSION['logged_in'])) {
  if ($_SESSION['logged_in'] != '1') {
    $_SESSION['user_subscription'] = '0';
  }
} else {
  $_SESSION['logged_in'] = '0';
}

if (!isset($_SESSION['lastSearchParams']) || isset($_GET['resetSearch'])) {
  $_SESSION['lastSearchParams'] = 'byZipcode=&byZipcodeDistance=&submit=Search+Power+Almanac';
}

$selTopNav = 'findlistings';

// affiliate handling
if (!isset($_SESSION['deltek_active'])) {
  $_SESSION['deltek_active'] = FALSE; // normal PA
}
if (!isset($_SESSION['deltek_exists'])) {
  $_SESSION['deltek_active'] = FALSE; // normal PA
}


// affiliate handling
if (!isset($_SESSION['affiliateID'])) {
  $_SESSION['affiliateID'] = '0'; // normal PA
}

if (!function_exists('escape')) {
    function escape($string)
    {
        return PowerAlmanac\PDb::escape($string);
    }
}
$escapeFunc = 'escape';

//function to return array of subscription types & their pricing break point
function getSubList($dbname,$dbhost,$dbuser,$dbpass, $purchasable=false) {
  $pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
  $query = "SELECT Sub_Level, Sub_NumReserves, Sub_AnnualFees, Sub_CostPerDL, Sub_Name, Purchasable FROM  `subscriptions` WHERE  `Active`='1'";

  // Only return subscriptions that are purchasable
  if ($purchasable) {
      $query .= " AND `Purchasable`=1";
  }

  $subscriptionTypes = $pdo->query($query);
  $subscriptionTypes = $subscriptionTypes->fetchAll(PDO::FETCH_ASSOC);
  $index = 0;

  //sort arrays by annual fee
  usort($subscriptionTypes, function($a, $b) {
      return $a['Sub_AnnualFees'] - $b['Sub_AnnualFees'];
  });

  foreach($subscriptionTypes as $index => &$value){
    if($index < count($subscriptionTypes) - 1){
      //calculate the pricing breakpoint for each level and pop it on the array  y is upgrade cost, x is cost per download, i is included credits, sub0 is the lower
      //subscription and sub1 is the higher subscription

      $I0 = $value['Sub_NumReserves'];
      $X0 = $value['Sub_CostPerDL'];
      $Y0 = $value['Sub_AnnualFees'];
      $I1 = array_key_exists($index+1, $subscriptionTypes) ? $subscriptionTypes[$index + 1]['Sub_NumReserves'] : 0;
      $X1 = array_key_exists($index+1, $subscriptionTypes) ? $subscriptionTypes[$index + 1]['Sub_CostPerDL'] : 0;
      $Y1 = array_key_exists($index+1, $subscriptionTypes) ? $subscriptionTypes[$index + 1]['Sub_AnnualFees'] : 0;

      $breakPoint = intval( ($Y1 - $Y0 + ($I0 * $X0)) / $X0 );

      $subscriptionTypes[$index + 1]['pricingBreak'] = $breakPoint;
    }

    //name fields the same as they are used in js and other php
    $value['cost'] = $value['Sub_CostPerDL'];
    $value['price'] = $value['Sub_AnnualFees'];
    $value['included'] = $value['Sub_NumReserves'];
  }
  return $subscriptionTypes;
}

function flashError($message, $technicalMessage = '') {
    $_SESSION['flashError'] = $message;
    $_SESSION['flashErrorTechnical'] = $technicalMessage;
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

if (isset($_SESSION['flashError'])) {
    include 'header-search.php';

    echo "<div class='container'><div class='alert alert-danger'>" . htmlentities($_SESSION['flashError']) . "</div></div> <!-- " . htmlentities($_SESSION['flashErrorTechnical']) . " -->";

    include 'footer.php';
    unset($_SESSION['flashError']);
    exit;
}

?>
