<?php
require_once 'config.php';


$pdo->query(sprintf("CALL GetSubscriptionInfo(%s,@JSON_String)", escape($User_Subscription)));
$pdoObject3 = $pdo->query("SELECT @JSON_String");
$rsArray3 = $pdoObject3->fetchAll();
$json3 = $rsArray3[0]['@JSON_String'];
$jsonArray3 = json_decode($json3, TRUE);
//print_r($json3);
$jsonArray3 = json_decode($json3, TRUE);
/*
switch(json_last_error())
{
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded';
	break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found';
	break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON';
	break;
	case JSON_ERROR_NONE:
		echo ' - No errors';
	break;
}
*/
//print_r($jsonArray3);
$Sub_Level = $jsonArray3['Sub_Level'];
$Sub_Name = $jsonArray3['Sub_Name'];
if ($Sub_Name == '') {
	$Sub_Name = 'Flex Power';
}
$Sub_CostPerDL = $jsonArray3['Sub_CostPerDL'];
if ($Sub_CostPerDL == '') {
	$Sub_CostPerDL = 0.30;
}
$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
//print_r($jsonArray2a);
$User_Password = $jsonArray2a['User_Password'];
$User_Parent = $jsonArray2a['User_Parent'];
$User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
$User_OptInAlerts = $jsonArray2a['User_OptInAlerts'];
$User_OptInNewsletter = $jsonArray2a['User_OptInNewsletter'];
$RegUser_ID = $jsonArray2a['RegUser_ID'];
$User_FirstName = $jsonArray2a['User_FirstName'];
$User_LastName = $jsonArray2a['User_LastName'];
$User_Email = $jsonArray2a['User_Email'];

// calculate downloads used
$Account_DLUsed = 0;

// use pay tables for master account
if ($User_Parent == '0') {
	// check if table exist
	$sql ="USE " . escape($dbname);
	$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
	if (!$dbcnx) {
		print("Unable to connect to the database server at this time.\n");
	exit();
	}
	if (!@PowerAlmanac\PDb::query($sql)){
		die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
	}
	$checkSQL = sprintf("show tables from %s like 'pay_%s'", escape($dbname), escape($RegUser_ID));
	$result_checkTable = @PowerAlmanac\PDb::query($checkSQL);
	if (!$result_checkTable) {
		die('Error reading from government database:' . PowerAlmanac\PDb::error());
	}
	$numTables = number_format(PowerAlmanac\PDb::num_rows($result_checkTable));
	if ($numTables > 0) {
		// get paid from pay_$id table if exist
		$readPaid_sql = sprintf("SELECT * FROM pay_%s", escape($RegUser_ID));
		$result_readPaid = @PowerAlmanac\PDb::query($readPaid_sql);
		if (!$result_readPaid) {
			die('Error reading from pay table database:' . PowerAlmanac\PDb::error());
		}
		$Account_DLUsed = number_format(PowerAlmanac\PDb::num_rows($result_readPaid));
	} else {
		$Account_DLUsed = 0;
	}
} else {
	// check pay tables
	$sql ="USE " . escape($dbname);
	$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
	if (!$dbcnx) {
		print("Unable to connect to the database server at this time.\n");
	exit();
	}
	if (!@PowerAlmanac\PDb::query($sql)){
		die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
	}
	$readPaid_sql = sprintf("SELECT * FROM userpay WHERE UserEmail = '%s'", escape($User_Email));
	$result_readPaid = @PowerAlmanac\PDb::query($readPaid_sql);
	if (!$result_readPaid) {
		die('Error reading from userpay table database:' . PowerAlmanac\PDb::error());
	}
	$Account_DLUsed = 0;
	while ($row = PowerAlmanac\PDb::fetch_array($result_readPaid))
	{
		$NumRecords = $row['NumRecords'];
		$Account_DLUsed = $Account_DLUsed + $NumRecords;
	}
}

/*
$pdo->query("CALL SavedDownloads('$User_Email',@JSON_String)"); // IN user_email VARCHAR(255),OUT JSON_String MEDIUMTEXT
$pdoObject6 = $pdo->query("SELECT @JSON_String");
$rsArray6 = $pdoObject6->fetchAll();
$json6 = $rsArray6[0]['@JSON_String'];

if ($json6 != "") {
	$jsonArray6 = json_decode($json6, TRUE);
	foreach ($jsonArray6 as $item)
	{
		$Download_NumRecords = $item['Download_NumRecords'];
		$Account_DLUsed = $Account_DLUsed + $Download_NumRecords;
	}
}

*/

$oneDay = 24*60*60;

/*
if ($User_Parent != '0') {
	$pdo->query("CALL GetUserInfo('$User_Parent',@JSON_String)"); 
	$pdoObject4 = $pdo->query("SELECT @JSON_String");
	$rsArray4 = $pdoObject4->fetchAll();
	$json4 = $rsArray4[0]['@JSON_String'];
	$jsonArray4 = json_decode($json4, TRUE);
	//$temp = $jsonArray4['DateTime_SubscriptionStart'];
	//echo("<p>DEBUG: temp = $temp</p>");
	if ($jsonArray4['DateTime_SubscriptionStart'] != 'n/a') {
		$DateTime_SubscriptionStart = date("F j, Y",strtotime($jsonArray4['DateTime_SubscriptionStart']));
	} else {
		$DateTime_SubscriptionStart = 'N/A';
	}
	if ($jsonArray4['DateTime_SubscriptionEnd'] != 'n/a') {
		$DateTime_SubscriptionEnd = date("F j, Y",strtotime($jsonArray4['DateTime_SubscriptionEnd']) - $oneDay);
	} else {
		$DateTime_SubscriptionEnd = 'N/A';
	}
} else {
	//$temp = $jsonArray2a['DateTime_SubscriptionStart'];
	//echo("<p>DEBUG: temp = $temp</p>");
	if ($jsonArray2a['DateTime_SubscriptionStart'] != 'n/a') {
		$DateTime_SubscriptionStart = date("F j, Y",strtotime($jsonArray2a['DateTime_SubscriptionStart']));
	} else {
		$DateTime_SubscriptionStart = 'N/A';
	}
	if ($jsonArray2a['DateTime_SubscriptionEnd'] != 'n/a') {
		$DateTime_SubscriptionEnd = date("F j, Y",strtotime($jsonArray2a['DateTime_SubscriptionEnd']) - $oneDay);
	} else {
		$DateTime_SubscriptionEnd = 'N/A';
	}
}
*/

$DateTime_SubscriptionStart = date("F j, Y",strtotime($jsonArray2a['DateTime_SubscriptionStart']));
$DateTime_SubscriptionEnd = date("F j, Y",strtotime($jsonArray2a['DateTime_SubscriptionEnd']) - $oneDay);
	
$optinAlerts = '';
$optinNewsletter = '';
if ($User_OptInAlerts) $optinAlerts = 'checked';
if ($User_OptInNewsletter) $optinNewsletter = 'checked';

$pdo->query(sprintf("CALL GetTransactions('%s',@JSON_String)", escape($User_Email)));
$pdoObject5 = $pdo->query("SELECT @JSON_String");
$rsArray5 = $pdoObject5->fetchAll();
$json5 = $rsArray5[0]['@JSON_String'];
$jsonArray5 = json_decode($json5, TRUE);
$arrayCount = count($jsonArray5);

// parse sub and dl
$trans_Sub = '';
$trans_DL = '';
$DLPurchased = 0;
if ($arrayCount != 0) {
	foreach ($jsonArray5 as $itemArray) {
		$Trans_Gross = $itemArray['Trans_Gross'];
		$Trans_DLCredits = $itemArray['Trans_DLCredits'];
		$Trans_SKU = $itemArray['Trans_SKU'];
		$PayPal_TransID = $itemArray['PayPal_TransID'];
		$Trans_Type = $itemArray['Trans_Type'];
		if (($Trans_SKU == 'PASUB_POWER3') || ($Trans_SKU == 'PASUB_POWER10') || ($Trans_SKU == 'PASUB_POWER51')) {
			$trans_Sub = $trans_Sub . "Transaction ID: <b>$PayPal_TransID</b><br />Amount: <b>$$Trans_Gross</b><br />";
		}
		if ($Trans_SKU == 'PADL') {
			$trans_DL = $trans_DL . "Transaction ID: <b>$PayPal_TransID</b><br />Amount: <b>$$Trans_Gross</b><br />Credits: <b>$Trans_DLCredits</b><br />";
			$DLPurchased = $DLPurchased + intval($Trans_DLCredits);
		}
	}
}
if ($trans_Sub == '') $trans_Sub = 'None.<br />';
if ($trans_DL == '') $trans_DL = 'None.<br />';

// mask password
$numChars = strlen($User_Password);
$maskedPassword = '';
for ($i = 1; $i <= $numChars; $i++) {
    $maskedPassword = $maskedPassword . '*';
}

?>
