<?php

$officials = array(
	'City/County Manager' => array(
        'role' => 'City/County Manager',                                    
        'name' => 'role_2',
        'value' => 'Top Appointed Official'
        ),	 	
	'Head of Public Works' => array(
        'role' => 'Head of Public Works',                     
        'name' => 'role_6',
        'value' => 'Head of Public Works'
        ),
	'Head of Finance' => array(
        'role' => 'Head of Finance',                                    
        'name' => 'role_4',
        'value' => 'Head of Finance/Budgeting'
        ),
	'Head of Purchasing' => array(
        'role' => 'Head of Purchasing',                                    
        'name' => 'role_5',
        'value' => 'Head of Purchasing/Procurement'
        ),
	'Head Clerk' => array(
        'role' => 'Head Clerk',                                    
        'name' => 'role_9',
        'value' => 'Clerk'
        ),
	'Police Chief/Sheriff' => array(
        'role' => 'Police Chief/Sheriff',                                    
        'name' => 'role_7',
        'value' => 'Head of Law Enforcement'
        ),
	'Fire Chief' => array(
        'role' => 'Fire Chief',                                    
        'name' => 'role_8',
        'value' => 'Head of Fire Protection Services'
        ),
	'Mayor' => array(
        'role' => 'Mayor',                                    
        'name' => 'role_1',
        'value' => 'Top Elected Official'
        ),
    'Head of IT' => array(
        'role' => 'Head of IT',                                    
        'name' => 'role_10',
        'value' => 'Head of IT'
        )
	);

   $officialDrop = '<select name="official_role" class="form-control mr-sm-2">
	    <option>choose officials...</option>';
	    
	     foreach($officials as $name => $data){
	        $officialDrop .= '<option value="' . $data['value'] . '" data-name="' . $data['name'] . '">' . $data['role'] . '</option>';
	        }	    
	$officialDrop .= '</select>';
