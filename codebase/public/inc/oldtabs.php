<?php

$selectedCreate = '';
$selectedAnalyze = '';
$selectedMySearches = '';
$selectedMyDownloads = '';
$selectedMyAccount = '';

switch($selBTN) {
	case 'mysearches':
		$selectedMySearches = '-sel';
		break;
	case 'mydownloads':
		$selectedMyDownloads = '-sel';
		break;
	case 'myaccount':
		$selectedMyAccount = '-sel';
		break;
	case 'createyourlist':
		$selectedCreate = '-sel';
		break;
	case 'analyze':
		$selectedAnalyze = '-sel';
		break;
	default:
		break;
}
if (!isset($_SESSION['firstTimeSearch'])) {
	$firstTimeSearch = 1;
} else {
	$firstTimeSearch = $_SESSION['firstTimeSearch'];
}
if ($firstTimeSearch == '1') {
	$analyzeURL = "javascript:alert('Please SUBMIT a search before choosing DOWNLOAD, SAVE, SHARE, ANALYZE, or PREVIEW.');";
} else {
	$analyzeURL = 'PA-Analyze.php';
}
if ($_SESSION['logged_in'] != '1') {
	// not logged in
?>
<a href="search-government.php"><img src="images/createtab<?php echo($selectedCreate); ?>.png" width="138" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><a href="<?php echo($analyzeURL); ?>"><img src="images/analyzetab<?php echo($selectedAnalyze); ?>.png" width="121" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><img src="images/tabpixel40.png" width="861" height="40" hspace="0" vspace="0" border="0" align="absbottom" />
<?php
} else {
?>
<a href="search-government.php"><img src="images/createtab<?php echo($selectedCreate); ?>.png" width="138" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><a href="<?php echo($analyzeURL); ?>"><img src="images/analyzetab<?php echo($selectedAnalyze); ?>.png" width="121" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><a href="PA-MySearches.php"><img src="images/mysearchestab<?php echo($selectedMySearches); ?>.png" width="127" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><a href="PA-MyDownloads.php"><img src="images/mydownloadstab<?php echo($selectedMyDownloads); ?>.png" width="127" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><a href="PA-MyAccount.php"><img src="images/myaccounttab<?php echo($selectedMyAccount); ?>.png" width="127" height="40" hspace="0" vspace="0" border="0" align="absbottom" /></a><img src="images/tabpixel40.png" width="480" height="40" hspace="0" vspace="0" border="0" align="absbottom" />
<?php
}
?>