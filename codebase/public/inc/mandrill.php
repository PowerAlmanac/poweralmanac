<?php

date_default_timezone_set('America/Chicago');

include_once realpath(__DIR__ . '/../') . "/lib/swift_required.php";

$smtp_host = 'smtp.mandrillapp.com';
$smtp_port = '587';
$smtp_user = 'mandrill@poweralmanac.com';
$smtp_pass = '85339f54-9ca7-49a0-820d-248711418b20';



function mandrill_setup(){
	$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
	$transport->setUsername('mandrill@poweralmanac.com');
	$transport->setPassword('85339f54-9ca7-49a0-820d-248711418b20');
	$swift = Swift_Mailer::newInstance($transport);
	return $swift;
}


function mandrill_send($data){

	$defaults = array(
		'from' => array('registration@poweralmanac.com' => 'Power Almanac Registration'),
		'bcc' => array('activation@poweralmanac.com' => 'Power Almanac Activation'),
		'html' => FALSE,
		'text' => FALSE,
        'replyTo' => FALSE
	);

	$data = array_merge($defaults,$data);

	extract($data);
	$swift = mandrill_setup();
	
	$message = new Swift_Message($subject);
	$message->setFrom($from);	
	$message->setTo($to);
	if($html){
		$message->setBody($html, 'text/html');
	}
	if ($replyTo) {
	    $message->setReplyTo($replyTo);
    }
	if($text){
		$message->addPart($text, 'text/plain');	
	}	
	$headers = $message->getHeaders();
	$headers->addTextHeader('X-MC-Template', 'poweralmanac');
	//$headers->addTextHeader('X-MC-MergeVars', 'lifecycle');

	if ($recipients = $swift->send($message, $failures))
	{
	
	} else {
	 	echo "There was an error:\n";
		print_r($failures);
	}

	return $recipients;
}
