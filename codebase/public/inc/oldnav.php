<?php
// get last updated

$lastUpdated = 'Not Available';

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$readdate_sql = "
	SELECT UPDATE_TIME
	FROM   information_schema.tables
	WHERE  TABLE_SCHEMA = 'government'
	AND TABLE_NAME = 'all_data'
	";

$result_readdate = @PowerAlmanac\PDb::query($readdate_sql);
if (!$result_readdate) {
	die('Error reading from government database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readdate);
//$lastUpdated = $onerow['UPDATE_TIME'];
$UPDATE_TIME = $onerow['UPDATE_TIME'];
$UPDATE_TIME_Unix = strtotime($UPDATE_TIME);
$lastUpdated = date("F j, Y, g:i a T",$UPDATE_TIME_Unix); 
?>
	
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
    	<td align="right" valign="middle" height="25">
        <p class="topNav">
        <a href="PA-Features.php" class="topNav">Features</a><img src="images/space.gif" width="25" height="1" align="absmiddle"><a href="faq-help.php" class="topNav">FAQ</a><img src="images/space.gif" width="25" height="1" align="absmiddle"><a href="PA-Pricing.php" class="topNav">Pricing</a><img src="images/space.gif" width="25" height="1" align="absmiddle">
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
			echo('<a href="login.php" class="topNavFocus">Log In</a><img src="images/space.gif" width="25" height="1" align="absmiddle"><a href="register.php"><img src="images/registerbtn.png" width="141" height="26" border="0" align="absmiddle" /></a><img src="images/space.gif" width="50" height="1" align="absmiddle">');
        } else {
            // logged in
            $User_FirstName = $_SESSION['user_firstname'];
			echo("<font style='font-weight: normal;text-transform: none;'>Welcome $User_FirstName!</font> <img src='images/space.gif' width='10' height='1' align='absmiddle'>");
			echo('<a href="PA-LogOut.php" class="topNavFocus"><img src="images/logoutbtn.png" alt="Log Out" width="100" height="26" hspace="0" vspace="0" border="0" align="absmiddle" title="Log Out" /></a><img src="images/space.gif" width="25" height="1" align="absmiddle">');
            echo('<br>');
            echo("<center><font style=\"font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #b34c2b; text-transform: none; padding-right: 25px;\">(records updated weekly; most recently on: <b>$lastUpdated</b>)</font></center>");	 
        }
        if ($_SESSION['logged_in'] != '1') {
            echo('<br><font class="topPromoText">');
            echo("And Download 50 Records FREE");	
            echo('</font>');
            echo('<br>');
            echo("<center><font style=\"font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #b34c2b; text-transform: none; padding-right: 25px;\">(records updated weekly; most recently on: <b>$lastUpdated</b>)</font></center>");	 
        } else {
            //
        }
        ?>
        </p>
        </td>
    </tr>
    </table>