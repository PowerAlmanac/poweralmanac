<?php
if (!class_exists('PowerAlmanac\\Config')) {
    require_once __DIR__ . '/../../vendor/autoload.php';
}

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name');

if($_SERVER['SERVER_NAME'] == 'pa.localhost' OR $_SERVER['SERVER_NAME'] == 'www.pa.localhost'){
	$dbhost = '127.0.0.1';
	$dbpass = '';
	$dbname = "pa";	
}