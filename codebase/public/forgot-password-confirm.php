<?php
include("inc/config.php");
$title = 'Privacy Policy | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");

// Needs the email functionality
require_once("inc/mandrill.php");

// Connect to Database
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// Prepared statements are disabled!?!?
//$pdo->prepare("SELECT * FROM registeredusers WHERE User_Email=? LIMIT 1");
//$fetch = $pdo->execute(array('joebruiser@gmail.com'));

// Sanitize email value
$email = Sanitize::variable($_POST['eMail'], "@.-+");

// Run a direct query then:
$fetch = $pdo->query(sprintf("SELECT * FROM registeredusers WHERE User_Email='%s' LIMIT 1", escape($email)));
$fetch = $fetch->fetch(PDO::FETCH_ASSOC);

if($fetch == array())
{
	header("Location: ./forgot-password?fail=true"); exit;
}

// Time
$time = time();
// Don't change this salt!! Used on password-reset.php!
// because we've used the user id to grab the user and added the pwd hash into the hash here, once the link has
// been successfully used once, it won't work again.
$sha1 = sha1("{$time}#$(*HFVDJSIVH(*&$#YJTI*&^YRDEIF{$fetch['RegUser_ID']}{$fetch['User_Password']}");

// Prepare Email Sending
$mailDefaults = array
(
	'from' => array('no-reply@poweralmanac.com' =>'Power Almanac Government Search'),	
	'bcc' => '',
	'html' => 'Someone from this email has just requested a password reset.<br /><br />If you requested this reset, you can reset it at the <a href="http://www.poweralmanac.com/password-reset?id=' . $fetch['RegUser_ID'] . '&time=' . $time . '&enc=' . $sha1 . '">Password Validation Page</a>.<br /><br />This link will be valid for 24 hours.<br /><br />Please do not respond to this email - it is a one-way automatic mailbox.',
	'text' => FALSE,
	'to' => $email,
	'subject' => 'Password Reset Request'
);

mandrill_send($mailDefaults);

// echo 'http://www.poweralmanac.com/password-reset?id=' . $fetch['RegUser_ID'] . '&time=' . $time . '&enc=' . $sha1;

?>
            <div class="intro">
                <div class="intro-holder">
                    <h1>Password Recovery Confirmation</h1>
                </div>
            </div>
            <div class="main-holder layout-n">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame">
							<p><span class="strengthen">An email has been sent to <?=$fetch['User_Email'];?></span></p>
							<ul class="no-style-type">
								<li>Make sure you check your email settings to ensure you can get email from poweralmanac.com, and check your spam folder if you don't find it in your inbox.</li>
								<li>&nbsp;</li>
								<li>If you still don't receive an email after five minutes, resubmit the form.</li>
							</ul>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>            