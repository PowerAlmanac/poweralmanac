<?php
define("TITLE", "Bidsync orders");
define("DESCRIPTION", "");
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder">
                    <h1>Bidsync Order</h1>
                </div>
            </div>
            <div class="main-holder about">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame">
                            <div id="wufoo-m7p8p5">
                                Fill out my <a href="http://blendimc.wufoo.com/forms/m7p8p5">online form</a>.
                                </div>
                                <script type="text/javascript">var m7p8p5;(function(d, t) {
                                var s = d.createElement(t), options = {
                                'userName':'blendimc',
                                'formHash':'m7p8p5',
                                'autoResize':true,
                                'height':'1435',
                                'async':true,
                                'header':'show',
                                'ssl':true};
                                s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'wufoo.com/scripts/embed/form.js';
                                s.onload = s.onreadystatechange = function() {
                                var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
                                try { m7p8p5 = new WufooForm();m7p8p5.initialize(options);m7p8p5.display(); } catch (e) {}};
                                var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
                                })(document, 'script');</script>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>            