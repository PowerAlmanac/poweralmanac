<?php
require_once realpath(__DIR__ . '/../../') . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();
include("inc-protected.php");
include("../inc/dbconfig.php");

$id = $_REQUEST['id'];

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect($dbhost, $dbuser, $dbpass);
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$readUser_sql = sprintf("SELECT * FROM registeredusers
	WHERE RegUser_ID = '%s'
	LIMIT 1
", escape($id));
$result_readUser = @PowerAlmanac\PDb::query($readUser_sql);
if (!$result_readUser) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readUser);

$RegUser_ID = $onerow['RegUser_ID'];	
$User_Email = $onerow['User_Email'];	
$User_Password = $onerow['User_Password'];	
$User_FirstName = $onerow['User_FirstName'];	
$User_LastName = $onerow['User_LastName'];	
$User_Subscription = $onerow['User_Subscription'];	
$User_Parent = $onerow['User_Parent'];	
$User_CompanyName = $onerow['User_CompanyName'];
	
$User_DL_Reserves = $onerow['User_DL_Reserves'];	
$User_Activated = $onerow['User_Activated'];	
$User_ActivationCode = $onerow['User_ActivationCode'];	

$User_OptInAlerts = $onerow['User_OptInAlerts'];	
$User_OptInNewsletter = $onerow['User_OptInNewsletter'];

$oneDay = 24*60*60;
$DateTime_SubscriptionStart = date("F j, Y",strtotime($onerow['DateTime_SubscriptionStart']));	
$DateTime_SubscriptionEnd = date("F j, Y",strtotime($onerow['DateTime_SubscriptionEnd']) - $oneDay);	
$DateTime_Registered = date("F j, Y",strtotime($onerow['DateTime_Registered']));	

$AgreeToTerms = $onerow['AgreeToTerms'];
if ($AgreeToTerms == '1') {
	$AgreeToTerms = 'Yes';
} else {
	$AgreeToTerms = 'No';
}

// for private offer - '2013-01-01 00:00:00'

$txtSubscriptionEnd = $onerow['DateTime_SubscriptionEnd'];

echo("<!--");
print_r($onerow);
echo("-->");


// subscriptions
$readSubscriptions_sql = "SELECT * FROM subscriptions
	WHERE Sub_Level  != '0' ORDER BY Purchasable DESC, Active DESC, Subscriptions_ID DESC
";
$result_readSubscriptions = @PowerAlmanac\PDb::query($readSubscriptions_sql);
if (!$result_readSubscriptions) {
	die('Error reading from subscriptions database:' . PowerAlmanac\PDb::error());
}

$affiliateCode = substr($User_ActivationCode,0,2);

// get affiliate ID from database
$readAffiliateID_sql = sprintf("SELECT * FROM affiliate_partners
	WHERE Partner_Encoding = '%s'
	LIMIT 1
", escape($affiliateCode));
$result_readAffiliateID = @PowerAlmanac\PDb::query($readAffiliateID_sql);
if (!$result_readAffiliateID) {
	die('Error reading from affiliate_partners database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readAffiliateID);
$affiliateID = $onerow['Partner_ID'];
	
/*

switch($User_Subscription) {
	case '1':
		$selectBasic = 'selected';
		$selectPower3 = '';
		$selectPower10 = '';
		$selectPower51 = '';
		$selectPowerFlex = '';
		$selectPowerUnltd = '';
		break;
	case '3':
		$selectBasic = '';
		$selectPower3 = 'selected';
		$selectPower10 = '';
		$selectPower51 = '';
		$selectPowerFlex = '';
		$selectPowerUnltd = '';
		break;
	case '4':
		$selectBasic = '';
		$selectPower3 = '';
		$selectPower10 = 'selected';
		$selectPower51 = '';
		$selectPowerFlex = '';
		$selectPowerUnltd = '';
		break;
	case '5':
		$selectBasic = '';
		$selectPower3 = '';
		$selectPower10 = '';
		$selectPower51 = 'selected';
		$selectPowerFlex = '';
		$selectPowerUnltd = '';
		break;
	case '9':
		$selectBasic = '';
		$selectPower3 = '';
		$selectPower10 = '';
		$selectPower51 = '';
		$selectPowerFlex = 'selected';
		$selectPowerUnltd = '';
		break;
	case '10':
		$selectBasic = '';
		$selectPower3 = '';
		$selectPower10 = '';
		$selectPower51 = '';
		$selectPowerFlex = '';
		$selectPowerUnltd = 'selected';
		break;
	default:
		$selectBasic = 'selected';
		$selectPower3 = '';
		$selectPower10 = '';
		$selectPower51 = '';
		$selectPowerFlex = '';
		$selectPowerUnltd = '';
		break;
}

*/

// read user notes
$readUserNotes_sql = sprintf("SELECT * FROM userinfo
	WHERE UserEmail = '%s' AND RecordType = 'note'
	LIMIT 1
", escape($User_Email));
$result_readUserNotes = @PowerAlmanac\PDb::query($readUserNotes_sql);
if (!$result_readUserNotes) {
	die('Error reading from userinfo database:' . PowerAlmanac\PDb::error());
}
$userrow = PowerAlmanac\PDb::fetch_array($result_readUserNotes);
$RecordNotes = $userrow['RecordNotes'];
$RecordFlag = $userrow['RecordFlag'];
if ($RecordFlag == '1') {
	$flagChecked = ' checked ';
} else {
	$flagChecked = '';
}

$readUserLogin_sql = sprintf("SELECT * FROM userinfo
	WHERE UserEmail = '%s' AND RecordType = 'login'
	ORDER BY ID DESC
	LIMIT 1
", escape($User_Email));
$result_readUserLogin = @PowerAlmanac\PDb::query($readUserLogin_sql);
if (!$result_readUserLogin) {
	die('Error reading from userinfo database:' . PowerAlmanac\PDb::error());
}
$userrow2 = PowerAlmanac\PDb::fetch_array($result_readUserLogin);
$RecordTime = $userrow2['RecordTime'];
if ($RecordTime == '') {
	$RecordTime = 'N/A';
} else {
	$RecordTime = date("F j, Y g:i a",strtotime($RecordTime));
}

// get list of emails
$readEmails_sql = "SELECT User_Email FROM registeredusers
	ORDER BY User_Email ASC
";
$result_readUserMails = @PowerAlmanac\PDb::query($readEmails_sql);
if (!$result_readUserMails) {
	die('Error reading from userinfo database:' . PowerAlmanac\PDb::error());
}

?>

<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/base/jquery-ui.css" type="text/css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" type="text/javascript"></script>
            
<link rel="Stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/cupertino/jquery-ui.css" type="text/css" />

<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>

<link rel="stylesheet" href="style.css" type="text/css">

<script>
$(function() {
	$( "#dateend" ).datepicker({
		dateFormat: 'MM dd, yy',
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true
	});
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 300,
		width: 350,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});
	$( "#create-user" )
		.button()
		.click(function() {
			$( "#dialog-form" ).dialog( "open" );
	});
});
</script>

<script type="text/javascript">
Shadowbox.init();
</script>
	<style>
		label, input { display:block; }
		fieldset { padding:0; border:0; margin-top:25px; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
	</style>
<body bgcolor="#FFFFFF">
<p align="left">
<br />
<?php
include("inc-topnav.php");
?>
<br />
<center><b>USER INFORMATION</b></center>
<br />
<form name="userdetails" action="process-userdetails.php" method="post">
<input type="hidden" name="id" value="<?php echo($id); ?>">
<table cellpadding="0" cellspacing="0" border="0" width="1000" align="center" style="border: 1px solid #000000;">
<tr>
	<td>
    <b>User ID</b>
    </td>
	<td>
    <?php echo($RegUser_ID); ?>
    <br>(<font class="smallfont">cannot change</font>)
    </td>
	<td>
    <b>User Email</b>
    </td>
	<td>
    <input name="email" type="text" id="email" size="20" value="<?php echo($User_Email); ?>" />
    </td>
	<td>
    <b>Parent Account</b>
    </td>
	<td>
    <?php 
	
	if ($User_Parent == '0') {
		echo("Master Account.");
	} else {
		// look up parent
		$readMaster_sql = sprintf("SELECT * FROM registeredusers
			WHERE RegUser_ID = '%s'
			LIMIT 1
		", escape($User_Parent));
		$result_readMaster = @PowerAlmanac\PDb::query($readMaster_sql);
		if (!$result_readMaster) {
			die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
		}
		$subrow = PowerAlmanac\PDb::fetch_array($result_readMaster);
		$Master_Email = $subrow['User_Email'];
		$Master_FirstName = $subrow['User_FirstName'];
		$Master_LastName = $subrow['User_LastName'];
		echo("$Master_FirstName $Master_LastName ($Master_Email)"); 
	}
	?> 
    <br>(<font class="smallfont">cannot change</font>)
    </td>
 	<td>
    <b>Subscription Level</b>
    </td>
	<td>
    <select name="subscription" id="subscription">
    <?php
	// loop through subscriptions
    $pastPurchasable = false;
    $pastActive = false;
	while ($srow = PowerAlmanac\PDb::fetch_array($result_readSubscriptions))
	{
		$Sub_Name = $srow['Sub_Name'];
		$Subscriptions_ID = $srow['Subscriptions_ID'];
		$Sub_Level = $srow['Sub_Level'];
		$Sub_AffiliateID = $srow['RegUser_ID'];
		echo("<!-- $Sub_AffiliateID $affiliateID $Sub_Level $User_Subscription -->\n");
		$subscriptionNameAppend = '';
		if ($Sub_Level == '1') {
			// basic
			$selected = $Sub_Level == $User_Subscription ? 'selected' : '';
			switch($affiliateCode) {
				case 'PA':
					$subscriptionNameAppend = '';
					if ($User_Subscription == '10') {
						if ($txtSubscriptionEnd == '2013-01-01 00:00:00') { // private offer
							$subscriptionNameAppend = ' 2012';
							$affiliateID = '04';
						}
					}
					break;
				case 'GE':
					$subscriptionNameAppend = ' G';
					break;
				case 'MA':
					$subscriptionNameAppend = ' A';
					break;
				case 'PO':
					if ($User_Subscription == '10') {
						$subscriptionNameAppend = ' 2012';
						$affiliateID = '04';
					} else {
						$subscriptionNameAppend = '';
					}
					break;
				default:
					$subscriptionNameAppend = '';
					break;
			}
		} else {
			if (($Sub_AffiliateID == $affiliateID) && ($Sub_Level == $User_Subscription)) {
				$selected = 'selected';
			} else {
				$selected = '';
			}
		}

		if (empty($srow['Purchasable']) && !$pastPurchasable) {
		    $pastPurchasable = true;
            echo("<option disabled>------</option>");
        }

        if (empty($srow['Active']) && !$pastActive) {
            $pastActive = true;
            echo("<option disabled>------</option>");
        }

        echo("<option value=\"$Sub_Level\" $selected >$Sub_Name $subscriptionNameAppend ({$srow['Sub_SKU']})</option>");
    }
	?>
    </select>
    </td>
</tr>
<tr>
	<td colspan="8">
    <hr>
    </td>
</tr>
<tr>
 	<td>
    <b>User Password</b>
    </td>
	<td>
	<a href="auto-login.php?id=<?=$RegUser_ID;?>">Log In As User</a>
    <br>
    <a rel="shadowbox;height=150;width=400" href="resend-password.php?id=<?php echo($RegUser_ID); ?>">Resend password email</a>
    </td>
	<td>
    <b>First Name</b>
    </td>
	<td>
    <input name="firstname" type="text" id="firstname" value="<?php echo($User_FirstName); ?>" /> 
    </td>
	<td>
    <b>Last Name</b>
    </td>
	<td>
    <input name="lastname" type="text" id="lastname" value="<?php echo($User_LastName); ?>" /> 
    </td>
	<td>
    <b>Company Name</b>
    </td>
	<td>
    <input name="companyname" type="text" id="companyname" value="<?php echo($User_CompanyName); ?>" /> 
    </td>
</tr>
<tr>
	<td colspan="8">
    <hr>
    </td>
</tr>
<tr>
 	<td>
    <b>Download Credits</b>
    </td>
	<td>
    <input name="dlcredits" type="text" id="dlcredits" size="20" value="<?php echo($User_DL_Reserves); ?>" /> 
    </td>
	<td>
    <b>Account Activated</b>
    </td>
	<td>
    <input name="activated" type="text" id="activated" value="<?php echo($User_Activated); ?>" /> 
    <br>
    <a rel="shadowbox;height=150;width=400" href="resend-actemail.php?id=<?php echo($RegUser_ID); ?>">Resend activation email</a>
    </td>
	<td>
    <b>Activation Code</b>
    </td>
	<td>
    <?php echo($User_ActivationCode); ?>
    <br>(<font class="smallfont">cannot change</font>)
    </td>
	<td>
    <b>Agreed to Terms of Service</b>
    </td>
	<td>
    <input type="checkbox" name="agreed" value="1" <?php if($AgreeToTerms == "Yes"){echo "checked='checked'";}?> >
    <br>(<font class="smallfont">cannot change</font>)
    </td>
</tr>
<tr>
	<td colspan="8">
    <hr>
    </td>
</tr>
<tr>
 	<td>
    <b>Date Registered</b>
    </td>
	<td>
    <?php echo($DateTime_Registered); ?>
    <br>(<font class="smallfont">cannot change</font>)
    </td>
	<td>
    <b>Subscription Start</b>
    </td>
	<td>
    <?php echo($DateTime_SubscriptionStart); ?>
    <br>(<font class="smallfont">cannot change</font>)
    </td>
	<td>
    <b>Subscription End</b>
    </td>
	<td colspan="2">
    <input name="dateend" type="text" id="dateend" value="<?php echo($DateTime_SubscriptionEnd); ?>"  style="padding-right:5px;padding-left:5px;" />
    </td>
    <td>
    <b>Last Log In:</b>
    <br>
    <?php echo($RecordTime); ?>
    </td>
</tr>
<tr>
	<td colspan="8">
    <hr>
    </td>
</tr>
<tr>
	<td valign="top">
    <b>NOTES:</b>
    </td>
	<td colspan="5">
<textarea name="notes" cols="80" rows="10"><?php echo($RecordNotes); ?>
</textarea>
    </td>
    <td valign="top">
    <b>Flag User</b>
    <br><br>
    <button id="create-user">Show All Emails</button>
    </td>
    <td valign="top">
    <input name="flag" type="checkbox" id="flag" value="1" <?php echo($flagChecked); ?>/>
    </td>
</tr>
<tr>
	<td colspan="8">
    <hr>
    </td>
</tr>
<tr>
	<td colspan="4" align="center">
    <input type="button" text="cancel" value="Cancel" onClick="history.back()" />
    </td>
	<td colspan="4" align="center">
    <input type="submit" name="submit" id="Submit" value="Submit" />
    </td>
</tr>
</table>
</form>
<br />
<center><b>TRANSACTION INFORMATION</b></center>
<br />
<table cellpadding="0" cellspacing="0" border="0" width="1000" align="center" style="border: 1px solid #000000;">
<tr><td width="20%"><b>Main Download File</b></td><td width="20%"><b>Downloaded</b></td><td align='right' width="20%"><b># Records</b></td><td align='right' width="20%"><b>New Records</b> (if user paid)</td><td align='right' width="20%"><b>Updates</b> (if user requested)</td></tr>
<tr>
	<td colspan="5">
    <hr>
    </td>
</tr>
    <?php
		if ($User_Parent == '0') {
			echo('<tr><td colspan="5"><i>Master Account</i></td></tr>');
		}
		$readDownloads_sql = sprintf("SELECT * FROM SavedDownloads
			WHERE RegUser_ID = '%s'
			ORDER BY SavedDownloads_ID DESC
		", escape($RegUser_ID));
		$result_readDownloads = @PowerAlmanac\PDb::query($readDownloads_sql);
		if (!$result_readDownloads) {
			die('Error reading from SavedDownloads database:' . PowerAlmanac\PDb::error());
		}
		$num_rows = PowerAlmanac\PDb::num_rows($result_readDownloads);

			while ($row = PowerAlmanac\PDb::fetch_array($result_readDownloads))
			{
				$Download_ID = $row['SavedDownloads_ID'];
				$Download_FileName = $row['Download_FileName'];
				$Search_Name = $row['Search_Name'];
				$Search_Params = $row['Search_Params'];
				$Credits_Used = $row['Credits_Used'];
				$Download_NumRecords = number_format($row['Download_NumRecords']);
				$DateTime_Saved = date("m/d/y h:i A",strtotime($row['DateTime_Saved']));
				// for delete
				$Search_NameENCODED = urlencode($Search_Name);
				$Download_NewRecords = number_format($row['Download_NewRecords']);
				$Download_UpdatedRecords = number_format($row['Download_UpdatedRecords']);
				if ($Download_NewRecords != 0) {
					$DLN = "<a href='http://www.poweralmanac.com/dl/f_$Download_FileName' target='_blank'>f_$Download_FileName</a>";
				} else {
					$DLN = "no new records available";
				}
				if ($Download_UpdatedRecords != 0) {
					$DLU = "<a href='http://www.poweralmanac.com/dl/u_$Download_FileName' target='_blank'>u_$Download_FileName</a>";
				} else {
					$DLU = "no updates available";
				}
				echo("<tr><td><a href='http://www.poweralmanac.com/dl/$Download_FileName' target='_blank'>$Download_FileName</a></td><td>$DateTime_Saved</td><td align='right'> $Download_NumRecords</td><td align='right'>$DLN</td><td align='right'>$DLU</td></tr>");
			}

			if ($User_Parent == '0') {
				echo('<tr><td colspan="5"><i>Sub-Users</i></td></tr>');
				// check if sub-users exist
				$readMySubUsers_sql = sprintf("SELECT * FROM registeredusers
					WHERE User_Parent = '%s'
					ORDER BY RegUser_ID DESC
				", escape($RegUser_ID));
				$result_readMySubUsers = @PowerAlmanac\PDb::query($readMySubUsers_sql);
				if (!$result_readMySubUsers) {
					die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
				}
				// sub-users only
				while ($subrow = PowerAlmanac\PDb::fetch_array($result_readMySubUsers))
				{
					$subRegUser_ID = $subrow['RegUser_ID'];
					// get downloads
					$readSubDownloads_sql = sprintf("SELECT * FROM SavedDownloads
						WHERE RegUser_ID = '%s'
						ORDER BY SavedDownloads_ID DESC
					", escape($subRegUser_ID));
					$result_readSubDownloads = @PowerAlmanac\PDb::query($readSubDownloads_sql);
					if (!$result_readSubDownloads) {
						die('Error reading from SavedDownloads database:' . PowerAlmanac\PDb::error());
					}
					$num_rowsSub = PowerAlmanac\PDb::num_rows($result_readSubDownloads);
					if ($num_rowsSub != '0') {
						while ($srow = PowerAlmanac\PDb::fetch_array($result_readSubDownloads))
						{
							$Download_ID = $srow['SavedDownloads_ID'];
							$Download_FileName = $srow['Download_FileName'];
							$Search_Name = $srow['Search_Name'];
							$Search_Params = $srow['Search_Params'];
							$Credits_Used = $srow['Credits_Used'];
							$Download_NumRecords = number_format($srow['Download_NumRecords']);
							$DateTime_Saved = date("m/d/y h:i A",strtotime($srow['DateTime_Saved']));
							// for delete
							$Search_NameENCODED = urlencode($Search_Name);
							$Download_NewRecords = number_format($srow['Download_NewRecords']);
							$Download_UpdatedRecords = number_format($srow['Download_UpdatedRecords']);
							if ($Download_NewRecords != 0) {
								$DLN = "<a href='http://www.poweralmanac.com/dl/f_$Download_FileName' target='_blank'>f_$Download_FileName</a>";
							} else {
								$DLN = "no new records available";
							}
							if ($Download_UpdatedRecords != 0) {
								$DLU = "<a href='http://www.poweralmanac.com/dl/u_$Download_FileName' target='_blank'>u_$Download_FileName</a>";
							} else {
								$DLU = "no updates available";
							}
							echo("<tr><td><a href='http://www.poweralmanac.com/dl/$Download_FileName' target='_blank'>$Download_FileName</a></td><td>$DateTime_Saved</td><td align='right'> $Download_NumRecords</td><td align='right'>$DLN</td><td align='right'>$DLU</td></tr>");
						}
					}
				}
			}
			
			
			// ONLY for MASTER ACCOUNT HERE ONWARDS!!!
			
			if ($User_Parent == '0') {
				
			echo('<tr><td colspan="5"><hr></td></tr>');
			// check if table exist
			$checkSQL = sprintf("show tables from government like 'pay_%s'", escape($RegUser_ID));
			$result_checkTable = @PowerAlmanac\PDb::query($checkSQL);
			if (!$result_checkTable) {
				die('Error reading from government database:' . PowerAlmanac\PDb::error());
			}
			$numTables = number_format(PowerAlmanac\PDb::num_rows($result_checkTable));

			echo("<tr><td colspan='2' align='right'>&nbsp;</td><td align='right'><b>Total of Paid Records</b></td><td colspan='2'>&nbsp;</td></tr>");
			if ($numTables > 0) {
				// get paid from pay_$id table if exist
				$readPaid_sql = sprintf("SELECT * FROM pay_%s", escape($RegUser_ID));
				$result_readPaid = @PowerAlmanac\PDb::query($readPaid_sql);
				if (!$result_readPaid) {
					die('Error reading from pay table database:' . PowerAlmanac\PDb::error());
				}
				$numPaid = number_format(PowerAlmanac\PDb::num_rows($result_readPaid));
				echo("<tr><td colspan='2' align='left'>Master Account ($User_Email)</td><td align='right'><b>$numPaid</b></td><td colspan='2'>&nbsp;</td></tr>");
			} else {
				echo("<tr><td colspan='2' align='left'>Master Account ($User_Email)</td><td align='right'><b>0</b></td><td colspan='2'>&nbsp;</td></tr>");
			}
		
	?>
<tr>
	<td colspan="5">
    <hr>
    </td>
</tr>
<tr>
	<td colspan="5" align="center">
        <i>PayPal Download Credit Purchases</i>
        <hr>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
        <tr><td width="20%"><b>PayPal Transaction ID</b></td><td width="20%"><b>Transaction Date/Time</b></td><td align='right' width="20%"><b>Credits Purchased</b></td><td align='right' width="20%"><b>Transaction Amount</b></td><td width="20%">&nbsp;</td></tr>
        <?php
                $readDLPurchases_sql = sprintf("SELECT * FROM paypaltransactions
                    WHERE RegUser_ID = '%s' AND Trans_SKU = 'PADL'
                    ORDER BY PayPal_ID DESC
                ", escape($RegUser_ID));
                $result_readDLPurchases = @PowerAlmanac\PDb::query($readDLPurchases_sql);
                if (!$result_readDLPurchases) {
                    die('Error reading from paypaltransactions database:' . PowerAlmanac\PDb::error());
                }
                $num_DLrows = PowerAlmanac\PDb::num_rows($result_readDLPurchases);
                if ($num_DLrows != '0') {
                    while ($drow = PowerAlmanac\PDb::fetch_array($result_readDLPurchases))
                    {
                        $Trans_Gross = number_format($drow['Trans_Gross'],2);
                        $Trans_DLCredits = number_format($drow['Trans_DLCredits']);
                        $Trans_DateTime = date("m/d/y h:i A",strtotime($drow['Trans_DateTime']));
                        $PayPal_TransID = $drow['PayPal_TransID'];
						$PayPal_ID = $drow['PayPal_ID'];
                        echo("<tr><td><a rel='shadowbox;height=700;width=600' href='viewpaypal.php?id=$PayPal_ID'>$PayPal_TransID</a></td><td>$Trans_DateTime</td><td align='right'>$Trans_DLCredits</td><td align='right'>$$Trans_Gross</td><td>&nbsp;</td></tr>");
                    }
                } else {
                    echo("<tr><td colspan='5'>There are no download purchases.</td></tr>");
                }
        ?>
        </table>
    </td>
</tr>
<tr>
	<td colspan="5">
    <hr>
    </td>
</tr>
<tr>
	<td colspan="5" align="center">
        <i>PayPal Purchases</i>
        <hr>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
        <tr><td width="20%"><b>PayPal Transaction ID</b></td><td width="20%"><b>Transaction Date/Time</b></td><td align='right' width="20%"><b>PA SKU</b></td><td align='right' width="20%"><b>Transaction Amount</b></td><td width="20%">&nbsp;</td></tr>
        <?php
                $readPurchases_sql = sprintf("SELECT * FROM paypaltransactions
                    WHERE RegUser_ID = '%s' AND Trans_SKU != 'PADL'
                    ORDER BY PayPal_ID DESC
                ", escape($RegUser_ID));
                $result_readPurchases = @PowerAlmanac\PDb::query($readPurchases_sql);
                if (!$result_readPurchases) {
                    die('Error reading from paypaltransactions database:' . PowerAlmanac\PDb::error());
                }
                $num_Prows = PowerAlmanac\PDb::num_rows($result_readPurchases);
                if ($num_Prows != '0') {
                    while ($prow = PowerAlmanac\PDb::fetch_array($result_readPurchases))
                    {
                        $Trans_Gross = number_format($prow['Trans_Gross'],2);
                        $Trans_SKU = $prow['Trans_SKU'];
                        $Trans_DateTime = date("m/d/y h:i A",strtotime($prow['Trans_DateTime']));
                        $PayPal_TransID = $prow['PayPal_TransID'];
						$PayPal_ID = $prow['PayPal_ID'];
                        echo("<tr><td><a rel='shadowbox;height=700;width=600' href='viewpaypal.php?id=$PayPal_ID'>$PayPal_TransID</a></td><td>$Trans_DateTime</td><td align='right'>$Trans_SKU</td><td align='right'>$$Trans_Gross</td><td>&nbsp;</td></tr>");
                    }
                } else {
                    echo("<tr><td colspan='5'>There are no PayPal purchases.</td></tr>");
                }
				
						
		} // only for MASTER ACCOUNT
		
        ?>
        </table>
    </td>
</tr>
</table>
</p>
<div id="dialog-form" title="List of Emails">
<p>
<?php
while ($row = PowerAlmanac\PDb::fetch_array($result_readUserMails))
{
	$User_Email = $row['User_Email'];
	echo("$User_Email<br>");
}
?>
</p>
</div>
</body>
