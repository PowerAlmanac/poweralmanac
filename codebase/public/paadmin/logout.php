<?php
require_once realpath(__DIR__ . '/../../') . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();

$_SESSION['adminloggedin'] = '0';	
header("Location: login.php");
flush();
exit;	


?>