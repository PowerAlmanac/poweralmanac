<?php
require_once realpath(__DIR__ . '/../../') . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

//print_r($_REQUEST); exit;
/*
    [id] => 37
    [subscription] => 4
    [password] => demo
    [firstname] => Nick
    [lastname] => Pang
    [companyname] => Power Almanac
    [dlcredits] => 8750
    [activated] => 1
    [dateend] => May 24, 2012
*/

session_start();
include("inc-protected.php");

$id = $_REQUEST['id'];

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name');

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$dbid = $_REQUEST['id'];
$email = $_REQUEST['email'];
$subscription = $_REQUEST['subscription'];
$password = str_replace("'","\'",$_REQUEST['password']);
$firstname = str_replace("'","\'",$_REQUEST['firstname']);
$lastname = str_replace("'","\'",$_REQUEST['lastname']);
$companyname = str_replace("'","\'",$_REQUEST['companyname']);
$dlcredits = $_REQUEST['dlcredits'];
$activated = $_REQUEST['activated'];
$dateend = $_REQUEST['dateend'];

//add option to unset tos agreement
if(isset($_REQUEST['agreed'])){
	$agreedTOS = true;
}else{
	$agreedTOS = false;
}

// userinfo table
if (isset($_REQUEST['flag'])) {
	$flag = $_REQUEST['flag'];
} else {
	$flag = 0;
}
$notes = str_replace("'","\'",$_REQUEST['notes']);

$oneDay = 24*60*60;

// process date
$enddate = date("Y-m-d H:i:s",strtotime($dateend)+$oneDay);


// check if NEW user email and if already used
$readUser_sql = sprintf("SELECT * FROM registeredusers
	WHERE RegUser_ID = '%s'
	LIMIT 1
", escape($dbid));
$result_readUser = @PowerAlmanac\PDb::query($readUser_sql);
if (!$result_readUser) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}

$numResults = PowerAlmanac\PDb::num_rows($result_readUser);

//echo("DEBUG: email = $email, numResults = $numResults"); exit;

if ($numResults == 1) {
	// check if new email
	$onerow = PowerAlmanac\PDb::fetch_array($result_readUser);
	$User_Email = $onerow['User_Email'];
	if ($User_Email != $email) {
		// check if email already in use
		$readSubUser_sql = sprintf("SELECT * FROM registeredusers
			WHERE User_Email = '%s'
		", escape($email));
		$result_readSubUser = @PowerAlmanac\PDb::query($readSubUser_sql);
		if (!$result_readSubUser) {
			die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
		}
		$numSubResults = PowerAlmanac\PDb::num_rows($result_readSubUser);
		if ($numSubResults > 0) {
			echo("<p align='center'><b>ERROR</b>: You've tried to change the email to one already in use.<a href='index.php?#tabs-2'>Please try again.</a></p>");
			exit;
		} else {
			// new email - update
			$update_sql = sprintf("UPDATE registeredusers SET User_Email = '%s', User_Subscription = '%s', User_FirstName = '%s', User_LastName = '%s', User_CompanyName = '%s', User_DL_Reserves = '%s', User_Activated = '%s', DateTime_SubscriptionEnd = '%s', AgreeToTerms = '%s'
				WHERE RegUser_ID = '%s'
			", escape($email), escape($subscription), escape($firstname), escape($lastname), escape($companyname), escape($dlcredits), escape($activated), escape($enddate), escape($agreedTOS), escape($dbid));
		}		
	} else {
		// just update
		$update_sql = sprintf("UPDATE registeredusers SET User_Subscription = '%s', User_FirstName = '%s', User_LastName = '%s', User_CompanyName = '%s', User_DL_Reserves = '%s', User_Activated = '%s', DateTime_SubscriptionEnd = '%s', AgreeToTerms = '%s'
		WHERE RegUser_ID = '%s'
	", escape($subscription), escape($firstname), escape($lastname), escape($companyname), escape($dlcredits), escape($activated), escape($enddate), escape($agreedTOS), escape($dbid));
	}
	
} else {
	echo("<b>ERROR</b>: Should not happen! Contact Nick.");
	exit;
}

$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
}

// check if first time - then insert
$readUserNotes_sql = sprintf("SELECT * FROM userinfo
	WHERE UserEmail = '%s' AND RecordType = 'note'
	LIMIT 1
", escape($email));
$result_readUserNotes = @PowerAlmanac\PDb::query($readUserNotes_sql);
if (!$result_readUserNotes) {
	die('Error reading from userinfo database:' . PowerAlmanac\PDb::error());
}
$numNotes = PowerAlmanac\PDb::num_rows($result_readUserNotes);

if ($numNotes == '0') {
	// insert user info
	$processUserNotes_sql = sprintf("INSERT INTO userinfo (UserEmail,RecordType,RecordNotes,RecordFlag,RecordTime)
	VALUES ('%s','note','%s','%s',now())
", escape($email), escape($notes), escape($flag));
} else {
	// update
	$processUserNotes_sql = sprintf("UPDATE userinfo SET RecordNotes = '%s', RecordFlag = '%s', RecordTime = now()
		WHERE UserEmail = '%s' AND RecordType = 'note'
	", escape($notes), escape($flag), escape($email));
}
$result_processUserNotes = @PowerAlmanac\PDb::query($processUserNotes_sql);
if (!$result_processUserNotes) {
	die('Error writing to userinfo database:' . PowerAlmanac\PDb::error());
}

header("Location: index.php?#tabs-2");
flush();
exit;

?>