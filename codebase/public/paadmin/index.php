<?php
require_once realpath(__DIR__ . '/../../') . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();
include("../inc/dbconfig.php");
include("inc-protected.php");

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// Dashboard - Tabs-1 Only
if (isset($_REQUEST['sd'])) {
	$startDate = $_REQUEST['sd'];
} else {
	$startDate = 'all';
}
if (isset($_REQUEST['ed'])) {
	$endDate = $_REQUEST['ed'];
} else {
	$endDate = 'all';
}

// Registered Users - Tabs-2 Only
if (isset($_REQUEST['s'])) {
	$sortOrder = $_REQUEST['s'];
} else {
	$sortOrder = 'i_';
}

$nextEOrderURL = "index.php?s=e#tabs-2";
$nextIOrderURL = "index.php?s=i#tabs-2";
$nextNOrderURL = "index.php?s=n#tabs-2";
$nextSOrderURL = "index.php?s=s#tabs-2";

switch($sortOrder) {
	case 'e':
		$orderSQL = "ORDER BY User_Email";
		$nextEOrderURL = "index.php?s=e_#tabs-2";
		break;
	case 'e_':
		$orderSQL = "ORDER BY User_Email DESC";
		$nextEOrderURL = "index.php?s=e#tabs-2";
		break;
	case 'i':
		$orderSQL = "ORDER BY RegUser_ID";
		$nextIOrderURL = "index.php?s=i_#tabs-2";
		break;
	case 'i_':
		$orderSQL = "ORDER BY RegUser_ID DESC";
		$nextIOrderURL = "index.php?s=i#tabs-2";
		break;
	case 'n':
		$orderSQL = "ORDER BY User_FirstName";
		$nextNOrderURL = "index.php?s=n_#tabs-2";
		break;
	case 'n_':
		$orderSQL = "ORDER BY User_FirstName DESC";
		$nextNOrderURL = "index.php?s=n#tabs-2";
		break;
	case 's':
		$orderSQL = "ORDER BY User_Subscription";
		$nextSOrderURL = "index.php?s=s_#tabs-2";
		break;
	case 's_':
		$orderSQL = "ORDER BY User_Subscription DESC";
		$nextSOrderURL = "index.php?s=s#tabs-2";
		break;
	default:
		$orderSQL = "ORDER BY RegUser_ID DESC";
		$nextIOrderURL = "index.php?s=i#tabs-2";
		break;
}
// master accounts
$readRegisteredUsers_sql = sprintf("SELECT * FROM registeredusers
	WHERE User_Parent = '0'
	%s
", escape($orderSQL));
$result_readRegisteredUsers = @PowerAlmanac\PDb::query($readRegisteredUsers_sql);
if (!$result_readRegisteredUsers) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccounts = PowerAlmanac\PDb::num_rows($result_readRegisteredUsers);

// subusers
$readSubUsers_sql = "SELECT * FROM registeredusers
	WHERE User_Parent != '0'
";
$result_readSubUsers = @PowerAlmanac\PDb::query($readSubUsers_sql);
if (!$result_readSubUsers) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numSubAccounts = PowerAlmanac\PDb::num_rows($result_readSubUsers);

// basic
$readRegisteredUsersBasic_sql = "SELECT * FROM registeredusers
	WHERE User_Parent = '0' AND User_Subscription = '1'
	ORDER BY RegUser_ID DESC
";
$result_readRegisteredUsersBasic = @PowerAlmanac\PDb::query($readRegisteredUsersBasic_sql);
if (!$result_readRegisteredUsersBasic) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccountsBasic = PowerAlmanac\PDb::num_rows($result_readRegisteredUsersBasic);

// power 3
$readRegisteredUsersPower3_sql = "SELECT * FROM registeredusers
	WHERE User_Parent = '0' AND User_Subscription = '3'
	ORDER BY RegUser_ID DESC
";
$result_readRegisteredUsersPower3 = @PowerAlmanac\PDb::query($readRegisteredUsersPower3_sql);
if (!$result_readRegisteredUsersPower3) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccountsPower3 = PowerAlmanac\PDb::num_rows($result_readRegisteredUsersPower3);

// power 10
$readRegisteredUsersPower10_sql = "SELECT * FROM registeredusers
	WHERE User_Parent = '0' AND User_Subscription = '4'
	ORDER BY RegUser_ID DESC
";
$result_readRegisteredUsersPower10 = @PowerAlmanac\PDb::query($readRegisteredUsersPower10_sql);
if (!$result_readRegisteredUsersPower10) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccountsPower10 = PowerAlmanac\PDb::num_rows($result_readRegisteredUsersPower10);

// power 51
$readRegisteredUsersPower51_sql = "SELECT * FROM registeredusers
	WHERE User_Parent = '0' AND User_Subscription = '5'
	ORDER BY RegUser_ID DESC
";
$result_readRegisteredUsersPower51 = @PowerAlmanac\PDb::query($readRegisteredUsersPower51_sql);
if (!$result_readRegisteredUsersPower51) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccountsPower51 = PowerAlmanac\PDb::num_rows($result_readRegisteredUsersPower51);

// flex power
$readRegisteredUsersPowerFlex_sql = "SELECT * FROM registeredusers
	WHERE User_Parent = '0' AND User_Subscription = '9'
	ORDER BY RegUser_ID DESC
";
$result_readRegisteredUsersPowerFlex = @PowerAlmanac\PDb::query($readRegisteredUsersPowerFlex_sql);
if (!$result_readRegisteredUsersPowerFlex) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccountsPowerFlex = PowerAlmanac\PDb::num_rows($result_readRegisteredUsersPowerFlex);

// power unlimited
$readRegisteredUsersPowerUnlimited_sql = "SELECT * FROM registeredusers
	WHERE User_Parent = '0' AND User_Subscription = '10'
	ORDER BY RegUser_ID DESC
";
$result_readRegisteredUsersPowerUnlimited = @PowerAlmanac\PDb::query($readRegisteredUsersPowerUnlimited_sql);
if (!$result_readRegisteredUsersPowerUnlimited) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$numMasterAccountsPowerUnlimited = PowerAlmanac\PDb::num_rows($result_readRegisteredUsersPowerUnlimited);

$numPaidAccounts = $numMasterAccountsPower3 + $numMasterAccountsPower10 + $numMasterAccountsPower51 + $numMasterAccountsPowerFlex + $numMasterAccountsPowerUnlimited;

// build subscription tree
$readSubscription_sql = "SELECT * FROM subscriptions
	WHERE RegUser_ID = '0'
";
$result_readSubscription = @PowerAlmanac\PDb::query($readSubscription_sql);
if (!$result_readSubscription) {
	die('Error reading from subscriptions database:' . PowerAlmanac\PDb::error());
}
$subscriptionArray = array();
while ($rows = PowerAlmanac\PDb::fetch_array($result_readSubscription))
{
	$Sub_Level = $rows['Sub_Level'];
	$Sub_Name = $rows['Sub_Name'];
	$RegUser_ID = $rows['RegUser_ID'];
	$subscriptionArray["$Sub_Level"] = $Sub_Name;
}

//echo("<!-- ");
//print_r($subscriptionArray);
//echo(" -->");

// paypal 
$readPayPal_sql = "SELECT * FROM paypaltransactions
	WHERE 1
	ORDER BY PayPal_ID DESC
";
$result_readPayPal = @PowerAlmanac\PDb::query($readPayPal_sql);
if (!$result_readPayPal) {
	die('Error reading from paypaltransactions database:' . PowerAlmanac\PDb::error());
}

$rowColor1 = 'FFFF66';
$rowColor2 = '99FFFF';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/base/jquery-ui.css" type="text/css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" type="text/javascript"></script>
            
<link rel="Stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/cupertino/jquery-ui.css" type="text/css" />

<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>

<link rel="stylesheet" href="style.css" type="text/css">
<title>PA Admin Dashboard</title>
<script type="text/javascript">
	$(function(){

		// Tabs
		$('#tabs').tabs();
		
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
		
		$(function() {
		$( "button" ).button();
		});
		
	});
</script>

<script type="text/javascript">
Shadowbox.init();
</script>
</head>

<body>
<center>

            <div style="padding-left:10px; padding-right:10px; padding-top:5px; width:1200px;">
			<p align="right">
            <a href="logout.php" class="logout">Log Out</a>
            </p>
            <div id="tabs">
            
                <ul>
                    <li><a href="#tabs-1">Dashboard</a></li>
                    <li><a href="#tabs-2">Registered Accounts</a></li>
                    <li><a href="#tabs-3">PayPal Transactions</a></li>
                    <!--
                    <li><a href="#tabs-4">Searches</a></li>
                    <li><a href="#tabs-5">Downloads</a></li>
                    -->
                </ul>
                
                <div id="tabs-1">
                <p align="left">
                <table cellpadding="0" cellspacing="0" border="0" width="99%" align="center">
                <tr>
                	<td>
                    <font class="dashboard">Total # of Master Accounts: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccounts); ?></font></b>
                    </td>
                	<td>
                    <font class="dashboard">Total # of Sub-Users Accounts: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numSubAccounts); ?></font></b>
                    </td>
                	<td>&nbsp;
                    
                    </td>
                    <td>&nbsp;
                    
                    </td>
                	<td>&nbsp;
                    
                    </td>
                    <td>&nbsp;
                    
                    </td>
                </tr>
                <tr>
                	<td colspan="8">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                	<td>
                    <font class="dashboard">Total # of Basic Subscribers: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccountsBasic); ?></font></b>
                    </td>
                    <td align="right" colspan="6">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                	<td colspan="8">&nbsp;
                    
                    </td>
                </tr>
                <tr>
                	<td>
                    <font class="dashboard">Total # Power 3 Subscribers: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccountsPower3); ?></font></b>
                    </td>
                	<td>
                    <font class="dashboard">Total # Power 10 Subscribers: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccountsPower10); ?></font></b>
                    </td>
                	<td>
                    <font class="dashboard">Total # Power 51 Subscribers: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccountsPower51); ?></font></b>
                    </td>
                	<td>
                    <font class="dashboard"></font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"></font></b>
                    </td>
                </tr>
                <tr>
                	<td>
                    <font class="dashboard">Total # Flex Power Subscribers: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccountsPowerFlex); ?></font></b>
                    </td>
                	<td>
                    <font class="dashboard">Total # Unlimited Power Subscribers: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numMasterAccountsPowerUnlimited); ?></font></b>
                    </td>
                	<td>
                    <font class="dashboard"></font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"></font></b>
                    </td>
                	<td>
                    <font class="dashboard"></font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"></font></b>
                    </td>
                </tr>
                <tr>
                	<td>
                    <font class="dashboard">Total # of Paid Users: </font>
                    </td>
                    <td align="right">
                    <b><font class="dashboard"><?php echo($numPaidAccounts); ?></font></b>
                    </td>
                	<td>&nbsp;
                    
                    </td>
                    <td>&nbsp;
                    
                    </td>
                	<td>&nbsp;
                    
                    </td>
                    <td>&nbsp;
                    
                    </td>
                	<td>&nbsp;
                    
                    </td>
                    <td>&nbsp;
                    
                    </td>
                </tr>
                </table>
                </p>
                </div>
                
                <div id="tabs-2">
                <p align="left">
                <table cellpadding="0" cellspacing="0" border="0" width="99%" align="center">
                <tr bgcolor="#CCCCCC">
                	<td></td>
                	<td><b><a href="<?php echo($nextIOrderURL); ?>">ID</a></b></td>
                	<td align="left"><b><a href="<?php echo($nextEOrderURL); ?>">Email</a></b></td>
                	<td><b><a href="<?php echo($nextNOrderURL); ?>">Name</a></b></td>
                	<td><b><a href="<?php echo($nextSOrderURL); ?>">Subscription</a></b></td>
                	<td><b>Phone</b></td>
                	<!--
                    <td align="right"><b>DL Credits</b></td>
                	<td align="center"><b>Activated</b></td>
                    -->
                	<td align='right'><b>Sub Start</b></td>
                	<td align='right'><b>Sub End</b></td>
                	<td align='right'><b>Registered</b></td>
                	<td><b>Terms</b></td>
                	<td><b>Actions</b></td>
                </tr>
                <?php
				$rowCtr = 0;
				$oneDay = 24*60*60;
				$masterAccountArray = array();
				while ($row = PowerAlmanac\PDb::fetch_array($result_readRegisteredUsers))
				{
					$RegUser_ID = $row['RegUser_ID'];	 	 	 	
					$User_Email = str_replace("'","\'",$row['User_Email']);	 	 
					$User_Password = $row['User_Password'];
					$User_FirstName = $row['User_FirstName'];
					$User_LastName = $row['User_LastName'];
					//$User_Middle_Name = $row['User_Middle_Name'];
					$User_Subscription = $row['User_Subscription'];
					$User_Parent = $row['User_Parent'];
					$User_Phone = $row['User_Phone']; 	 	 
					//$User_DL_Limit = $row['User_DL_Limit'];
					$User_DL_Reserves = $row['User_DL_Reserves'];	 	 	
					$User_Activated = $row['User_Activated'];
					$User_ActivationCode = $row['User_ActivationCode'];	 	 	 
					$User_OptInAlerts = $row['User_OptInAlerts'];
					$User_OptInNewsletter = $row['User_OptInNewsletter']; 	 	
					$txtSubscriptionEnd = $row['DateTime_SubscriptionEnd'];
                    $AgreeToTerms = $row['AgreeToTerms'];
                    if($AgreeToTerms == '1'){
                        $AgreeToTerms = "Yes";
                    }else{
                        $AgreeToTerms = "No";
                    }

					// affiliate
					$affiliateCode = substr($User_ActivationCode,0,2);
					switch($affiliateCode) {
						case 'PA':
							$affiliateID = '0';
							$subscriptionNameAppend = '';
							if ($User_Subscription == '10') {
								if ($txtSubscriptionEnd == '2013-01-01 00:00:00') { // private offer
									$subscriptionNameAppend = ' 2012';
								}
							}
							break;
						case 'GE':
							$affiliateID = '2';
							$subscriptionNameAppend = ' G';
							break;
						case 'MA':
							$affiliateID = '3';
							$subscriptionNameAppend = ' A';
							break;
						case 'PO':
							$affiliateID = '04';
							if ($User_Subscription == '10') {
								$subscriptionNameAppend = ' 2012';
							} else {
								$subscriptionNameAppend = '';
							}
							break;
						default:
							$affiliateID = '0';
							$subscriptionNameAppend = '';
							break;
					}
					
					if ($row['DateTime_SubscriptionStart'] == NULL) {
						$DateTime_SubscriptionStart = '';
					} else {
						$DateTime_SubscriptionStart = date("F j, Y",strtotime($row['DateTime_SubscriptionStart']));
					}
					if ($row['DateTime_SubscriptionEnd'] == NULL) {
						$DateTime_SubscriptionEnd = '';
					} else {
						$DateTime_SubscriptionEnd = date("F j, Y",strtotime($row['DateTime_SubscriptionEnd']) - $oneDay);
					}
					$DateTime_Registered = date("F j, Y",strtotime($row['DateTime_Registered']));
					
					$masterAccountArray["$RegUser_ID"] = $User_Email;
					
					// look up subscription
					$subscriptionName = $subscriptionArray["$User_Subscription"] . $subscriptionNameAppend;
					
					// read user notes
					$readUserNotes_sql = sprintf("SELECT * FROM userinfo
						WHERE UserEmail = '%s' AND RecordType = 'note'
						LIMIT 1
					", escape($User_Email));
					$result_readUserNotes = @PowerAlmanac\PDb::query($readUserNotes_sql);
					if (!$result_readUserNotes) {
						die('Error reading from userinfo database:' . PowerAlmanac\PDb::error());
					}
					$userrow = PowerAlmanac\PDb::fetch_array($result_readUserNotes);
					$RecordFlag = $userrow['RecordFlag'];
					if ($RecordFlag == '1') {
						$flagImg = "<img src='images/redflag.png' border='0'>";
					} else {
						$flagImg = '';
					}

					$rowCtr = $rowCtr + 1;
					if ($rowCtr % 2 == 0) {
						echo("<tr bgcolor='#$rowColor1'>");
					} else {
						echo("<tr bgcolor='#$rowColor2'>");
					}
					echo("<td>$flagImg</td>");
					echo("<td>$RegUser_ID</td>");
					echo("<td align='left'><b>$User_Email</b></td>");
					echo("<td>$User_FirstName $User_LastName</td>");
					echo("<td>$subscriptionName</td>");
					echo("<td>$User_Phone</td>");
					/*
					echo("<td align='right'>$User_DL_Reserves</td>");
					if ($User_Activated == '0') {
						echo("<td align='center'><font color='#ff0000'>No</font></td>");
					} else {
						echo("<td align='center'>Yes</td>");
					}
					*/
					echo("<td align='right'>$DateTime_SubscriptionStart</td>");
					echo("<td align='right'>$DateTime_SubscriptionEnd</td>");
					echo("<td align='right'>$DateTime_Registered</td>");
					echo("<td align='center'>$AgreeToTerms</td>");
					echo("<td><a href='edit-userdetails.php?id=$RegUser_ID'>edit</a></td>");
					echo("</tr>");
					
					$readMySubUsers_sql = sprintf("SELECT * FROM registeredusers
						WHERE User_Parent = '%s'
						ORDER BY RegUser_ID DESC
					", escape($RegUser_ID));
					$result_readMySubUsers = @PowerAlmanac\PDb::query($readMySubUsers_sql);
					if (!$result_readMySubUsers) {
						die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
					}
					// sub-users only
					while ($subrow = PowerAlmanac\PDb::fetch_array($result_readMySubUsers))
					{
						$subRegUser_ID = $subrow['RegUser_ID'];	 	 	 	
						$subUser_Email = $subrow['User_Email'];	 	 
						$subUser_Password = $subrow['User_Password'];
						$subUser_FirstName = $subrow['User_FirstName'];
						$subUser_LastName = $subrow['User_LastName'];
						$subUser_DL_Reserves = $subrow['User_DL_Reserves'];
						$subDateTime_Registered = date("F j, Y",strtotime($subrow['DateTime_Registered']));
						$subAgreeToTerms = $subrow['AgreeToTerms'];
						
						// read sub-user notes
						$readSubUserNotes_sql = sprintf("SELECT * FROM userinfo
							WHERE UserEmail = '%s' AND RecordType = 'note'
							LIMIT 1
						", escape($subUser_Email));
						$result_readSubUserNotes = @PowerAlmanac\PDb::query($readSubUserNotes_sql);
						if (!$result_readSubUserNotes) {
							die('Error reading from userinfo database:' . PowerAlmanac\PDb::error());
						}
						$subuserrow = PowerAlmanac\PDb::fetch_array($result_readSubUserNotes);
						$RecordFlag = $subuserrow['RecordFlag'];
						if ($RecordFlag == '1') {
							$flagImg = "<img src='images/redflag.png' border='0'>";
						} else {
							$flagImg = '';
						}
					
						if ($rowCtr % 2 == 0) {
							echo('<tr  bgcolor="#FFFF66">');
						} else {
							echo('<tr bgcolor="#99FFFF">');
						}
						echo("<td>$flagImg</td>");
						echo("<td>$subRegUser_ID</td>");
						echo("<td align='left'><i>$subUser_Email</i></td>");
						echo("<td><i>$subUser_FirstName $subUser_LastName</i></td>");
						echo("<td><i>$subscriptionName</i></td>");
						echo("<td><i></i></td>");
						/*
						echo("<td align='right'><i>$subUser_DL_Reserves</i></td>");
						if ($User_Activated == '0') {
							echo("<td align='center'><font color='#ff0000'><i>No</i></font></td>");
						} else {
							echo("<td align='center'><i>Yes</i></td>");
						}
						*/
						echo("<td><i></i></td>");
						echo("<td><i></i></td>");
						echo("<td align='right'><i>$subDateTime_Registered</i></td>");
						if ($subAgreeToTerms == '0') {
							echo("<td align='center'><font color='#ff0000'><i>No</i></font></td>");
						} else {
							echo("<td align='center'><i>Yes</i></td>");
						}
						echo("<td><a href='edit-userdetails.php?id=$subRegUser_ID'><i>edit</i></a></td>");
						echo("</tr>");
					}
				}
				?>
                </table>
                </p>
                </div>
                
                <div id="tabs-3">
                <p align="left">
                <table cellpadding="0" cellspacing="0" border="0" width="99%" align="center">
                <tr bgcolor="#CCCCCC">
                	<td><b>ID</b></td>
                	<td><b>Trans SKU</b></td>
                	<td align='right'><b>Trans Date</b></td>
                	<td><b>Trans ID</b></td>
                	<td align="right"><b>Trans Amount</b></td>
                	<td align="right"><b>Trans Credit</b></td>
                	<td><b>User Email</b></td>
                	<td><b>Actions</b></td>
                </tr>
                <?php
				//print_r($masterAccountArray);
				$prowCtr = 0;
				while ($prow = PowerAlmanac\PDb::fetch_array($result_readPayPal))
				{
					$prowCtr = $prowCtr + 1;
					if ($prowCtr % 2 == 0) {
						echo("<tr bgcolor='#$rowColor1'>");
					} else {
						echo("<tr bgcolor='#$rowColor2'>");
					}
					$PayPal_ID = $prow['PayPal_ID'];
					$Trans_SKU = $prow['Trans_SKU'];
					$Trans_DateTime = date("F j, Y",strtotime($prow['Trans_DateTime']));
					$PayPal_TransID = $prow['PayPal_TransID'];
					$Trans_Gross = $prow['Trans_Gross'];
					$Trans_DLCredits = $prow['Trans_DLCredits'];
					$PayPalRegUser_ID = $prow['RegUser_ID'];
					if (isset($masterAccountArray["$PayPalRegUser_ID"])) {
						$PayPalUser_Email = $masterAccountArray["$PayPalRegUser_ID"];
					} else {
						$PayPalUser_Email = 'N/A';
					}
					echo("<td>$PayPal_ID</td>");
					echo("<td>$Trans_SKU</td>");
					echo("<td align='right'>$Trans_DateTime</td>");
					echo("<td>$PayPal_TransID</td>");
					echo("<td align='right'>$$Trans_Gross</td>");
					echo("<td align='right'>$Trans_DLCredits</td>");
					echo("<td>$PayPalUser_Email</td>");
					echo("<td><a rel='shadowbox;height=700;width=600' href='viewpaypal.php?id=$PayPal_ID'><i>details</i></a></td>");
					echo("</tr>");
				}
				?>
                </table>
                </p>
                </div>
               
                
                </div>
                
            </div>
            
            </div>
</center>
</body>
</html>