<?php
require_once realpath(__DIR__ . '/../../') . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();
include("inc-protected.php");

$id = $_REQUEST['id'];

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name');
$serverURL = "http://www.poweralmanac.com";

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$readUser_sql = sprintf("SELECT * FROM registeredusers
	WHERE RegUser_ID = '%s'
	LIMIT 1
", escape($id));
$result_readUser = @PowerAlmanac\PDb::query($readUser_sql);
if (!$result_readUser) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readUser);
$User_Email = $onerow['User_Email'];
$User_ActivationCode = $onerow['User_ActivationCode'];
$User_FirstName = $onerow['User_FirstName'];
$User_LastName = $onerow['User_LastName'];

$User_Email_Urlencoded = urlencode($User_Email);
$messageBody = "Please click on the following URL to activate your account:\n $serverURL/PA-Activate.php?actCode=$User_ActivationCode&e=$User_Email_Urlencoded \n";
 
// use Amazon SES
require_once('ses.php');
$ses = new SimpleEmailService('AKIAI5D3JYIQSM534C6A', 'Q0xFC1uu1pLOhMO68782UC6rG5rKSlsb1umf87AE');
$m = new SimpleEmailServiceMessage();
$m->addTo("$User_FirstName $User_LastName <$User_Email>");
$m->addBCC('Power Almanac Activation <activation@poweralmanac.com>');
$m->setFrom('Power Almanac Registration <registration@poweralmanac.com>');
$m->setReturnPath('activation@poweralmanac.com');
$m->setSubject('[Registration - Please Activate Account]');
$m->setMessageFromString("$messageBody");

$rc = $ses->sendEmail($m);

$status = 'Sent via Amazon Simple Email Service'; 

/*
$rc = mail("$User_Email","[Registration - Please Activate Account]","$messageBody","From: registration@poweralmanac.com\r\nBcc: activation@poweralmanac.com\r\n");
if ($rc) {
	$status = 'SUCCESS';
} else {
	$status = 'ERROR';
}
*/

?>

<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>

<link rel="stylesheet" href="style.css" type="text/css">
<body bgcolor="#FFFFFF">
<p align="left">
<a href="#" onClick="window.parent.Shadowbox.close()">Close</a>
<br /><br />

Activation eMail re-sent to: <?php echo($User_Email); ?>
<br>
Status: <?php echo($status); ?>
</p>
</body>