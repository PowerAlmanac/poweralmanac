<?php

// This hack is basically done because the existing login requires a redirect to /search-government, but the parent
// path doesn't stick. So if you use the login from the admin directory (which is done for the sake of accessing other
// accounts without needing their password), this will redirect a second time, but this time properly.
header("Location: ../search-government.php"); exit;