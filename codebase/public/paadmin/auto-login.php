<?php
require_once realpath(__DIR__ . '/../../') . '/vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();
include("inc-protected.php");
include("../inc/dbconfig.php");

$id = $_REQUEST['id'];

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// Gather the Email (for auto login purposes)
$data = PowerAlmanac\PDb::query("SELECT RegUser_ID, User_Email FROM registeredusers WHERE RegUser_ID='" . escape($id) . "' LIMIT 1");
$fetch = PowerAlmanac\PDb::fetch_array($data);

// var_dump($fetch);

/****** Automatic login needs to take place ******/
// We unfortunately need to load PAProcessLogIn here because the login is developed poorly
// This means we need to create false $_POST values so that PA-ProcessLogIn can evaluate it
$_POST['eMail'] = $fetch['User_Email'];

// Okay, $_POST['passWord'] can't be passed securely, since this page is loaded via header.
// So instead we're going to have to create an auto-login cheater and update PA-ProcessLogIn
// to interpret it properly for registration ONLY.
define("REGISTRATION_AUTO", true);

chdir('../');

unset($_SESSION['deltek_exists']);
unset($_SESSION['deltek_session']);
require_once("./PA-ProcessLogIn.php"); exit;
