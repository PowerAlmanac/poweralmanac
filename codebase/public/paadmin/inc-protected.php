<?php

if (!function_exists('escape')) {
    function escape($string)
    {
        return PowerAlmanac\PDb::escape($string);
    }
}

if ($_SESSION['adminloggedin'] != '1') {
	// not logged in
	header("Location: login.php");
	flush();
}

?>
