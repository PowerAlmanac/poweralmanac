<?php

include("inc/config.php");

include('inc/mandrill.php');

$title = 'Activate Registration';

$eMail = $_REQUEST['e'];
$actCode = $_REQUEST['actCode'];

$firstTime = 0;

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
	exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$read_sql = sprintf("SELECT * FROM registeredusers
	WHERE User_Email = '%s'
	LIMIT 1
", escape($eMail));

$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$row = PowerAlmanac\PDb::fetch_array($result_read);
$User_Activated = $row['User_Activated'];
$User_DL_Reserves = $row['User_DL_Reserves'];
$User_Subscription = $row['User_Subscription'];

if ($User_Activated == '0') {
	$newReserves = $User_DL_Reserves + 50;
	$firstTime = 1;
}

// add 50 credits to account if not activated yet

//$found = $newUser->Validate_User($actCode,$eMail);
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL Validate_User('%s','%s',@RC)", escape($actCode), escape($eMail)));
$pdoObject = $pdo->query("SELECT @RC");
$rsArray = $pdoObject->fetchAll();
$RC = $rsArray[0]['@RC'];

if ($RC == '1') {
	// not found
	$activationMessage = "<b>INVALID</b> activation code. Your registration may have been de-activated due to inactivity. Please try <a href='register.php'>registering again</a>.";
} else {
	// validated
	$nowIsUNIX = time();
	$laterIsUNIX = $nowIsUNIX + (60*60*24*366);
	$nowIs = date("Y-m-d H:i:s",$nowIsUNIX);
	$laterIs = date("Y-m-d H:i:s",$laterIsUNIX);
	if ($firstTime == 1) {
		$affiliateCode = substr($actCode,0,2);
		if ($affiliateCode == 'PO') {
			// private offer 2012
			$endOfYear2012 = '2013-01-01 00:00:00';
			if ($User_Subscription == '10') {
				$update_sql = sprintf("UPDATE registeredusers SET DateTime_SubscriptionStart = now(), DateTime_SubscriptionEnd = '%s'
					WHERE User_Email = '%s'
				", escape($endOfYear2012), escape($eMail));
			} else {
				$update_sql = sprintf("UPDATE registeredusers SET DateTime_SubscriptionStart = now(), DateTime_SubscriptionEnd = '%s'
					WHERE User_Email = '%s'
				", escape($laterIs), escape($eMail));
			}
			$result_update = @PowerAlmanac\PDb::query($update_sql);
			if (!$result_update) {
				die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
			}
		} else {
			// normal - add 50 credits to account if not activated yet
			$update_sql = sprintf("UPDATE registeredusers SET User_DL_Reserves = '%s', DateTime_SubscriptionStart = now(), DateTime_SubscriptionEnd = '%s'
				WHERE User_Email = '%s'
			", escape($newReserves), escape($laterIs), escape($eMail));
			$result_update = @PowerAlmanac\PDb::query($update_sql);
			if (!$result_update) {
				die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
			}
		}
	}
	$activationMessage = "<b>SUCCESSFUL</b> activation code. Your registration has been activated. Please <a href='login.php'>log in</a> to start using your account.";
	$_SESSION['login_message'] = '';


	$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
	$result = $pdo->query(sprintf("SELECT User_FirstName FROM RegisteredUsers WHERE RegisteredUsers.User_Email='%s' LIMIT 1;", escape($eMail)));
	foreach($result as $row){
		$firstName = $row['User_FirstName'];
	}
	
	 $message = file_get_contents('emailtemplates/welcome.txt');
	 $message = str_replace('[NAME]', $firstName, $message);
	 $message = str_replace('[EMAIL]', $eMail, $message);

	 $email_data = array(
	 	'to' => $eMail,
	 	'subject' => 'Welcome to Power Almanac!',	
	 	'html' => $message 
	 	);



	 mandrill_send($email_data);

}

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="5" cellspacing="5" width="99%" align="center" border="0" bgcolor="#FFFFFF">
            <tr>
                <td>
                <p class="hpHeaders">Activate Registration</p>
                <p>
                <?php echo($activationMessage); ?>
                </p>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
            //include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
