<?php
require __DIR__ . '/inc/config.php';
error_reporting(E_ALL);

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name');
		
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error());
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error());

$searchParams = 'byText=&top_elected=0'; 
$pdo2 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

echo("Cleaning up tables...<br>");
// clean up first
$pdo2->query("CALL Delete_Research_Tables('createpa')"); 

echo("Updating summary...<br>");
// update
$pdo2->query(sprintf("CALL SearchSummary('createpa','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($searchParams)));
$pdoObject = $pdo2->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists");
$rsArray = $pdoObject->fetchAll();
$JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
$JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
$JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];
$table_exists = $rsArray[0]['@table_exists'];
//print_r($rsArray);
//print_r($JSON_SUMMARY);print_r($JSON_GOVERNMENTS);print_r($JSON_OFFICIALS);print_r($flag);exit;
$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
//print_r($JSON_SUMMARY_Array);
$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
//print_r($JSON_GOVERNMENTS_Array);
$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);
//print_r($JSON_OFFICIALS_Array);

echo("Building tables...<br>");
// build research tables
$output = shell_exec("nohup /usr/bin/mysql -uroot --host=poweralmanac.cnl6vllv6ior.us-east-1.rds.amazonaws.com --password=W4boubWgGkaPaqnmrsXmr>FY9 -N -B --database=government -e\"call Bulid_Research_Tables('createpa',@exists);\" > /dev/null&");
//$pdo2->query("CALL Bulid_Research_Tables('createpa')"); 

echo("Done!<br>");

?>
