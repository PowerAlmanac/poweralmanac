<?php

include("inc/config.php");
$title = 'Subscribe';

$sku = $_REQUEST['sku'];

// create user
$eMail = $_POST['eMail'];
$passWord = $_POST['passWord'];
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$companyName = $_POST['companyName'];

$_SESSION['subscribe_message'] = '';

if (($eMail == '') || ($passWord == '') || ($firstName == '') || ($lastName == '') || ($companyName == '')) {
	$_SESSION['subscribe_message'] = '<center>All fields are required and must be filled in.<br /></center>';
	header("Location: ./subscribe");
	flush();
	exit;
}

// check if email already used
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL CheckEmail('%s',@RC)", escape($eMail)));
$pdoObject = $pdo->query("SELECT @RC");
$rsArray = $pdoObject->fetchAll();
$RC = $rsArray[0]['@RC'];
//print_r($RC) ; exit;

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

if ($RC == '1') {
	// clean
	//echo("DEBUG: Clean email.<br>");
	$randval = mt_rand();
	
	// affiliate tracking here
	$affiliateID = $_SESSION['affiliateID'];

	// get affiliate encoding from database
	$readAffiliateEncoding_sql = sprintf("SELECT * FROM affiliate_partners
		WHERE Partner_ID = '%s'
		LIMIT 1
	", escape($affiliateID));
	$result_readAffiliateEncoding = @PowerAlmanac\PDb::query($readAffiliateEncoding_sql);
	if (!$result_readAffiliateEncoding) {
		die('Error reading from affiliate_partners database:' . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_readAffiliateEncoding);
	$Partner_Encoding = $onerow['Partner_Encoding'];
	$actCode = $Partner_Encoding . $randval;
	/*
	switch($affiliateID) {
		case '2': // GovEvents
			$actCode = "GE$randval";
			break;
		case '3': // Mark Amtower
			$actCode = "MA$randval";
			break;
		default:
			$actCode = "PA$randval";
			break;
	}
	*/
	
	// Prepare Security Password
	require_once("./class/Security.php");
	
	$passWord = Security::setPassword($passWord);
	// End Password Security
	
	// insert to database
	$pdo->query(sprintf("CALL InsertUserInfo('%s','%s','%s','','%s','%s','%s',@RC,0)", escape($eMail), escape($passWord), escape($firstName), escape($lastName), escape($companyName), escape($actCode)));
	$pdoObject1 = $pdo->query("SELECT @RC");
	$rsArray1 = $pdoObject1->fetchAll();
	$RC1 = $rsArray1[0]['@RC'];
	//echo("DEBUG: RC1 = $RC1");
	$_SESSION['subscribe_message'] = '';
} else {
	// email already used
	$_SESSION['subscribe_message'] = '<center>Your eMail address is already in use. <br />You may have already registered but not activated your account. <br />Please check your email. (Not there?  Be sure to check your spam/junk folders too.)</center>';
	header("Location: ./subscribe");
	flush();
}

$eMail_Urlencoded = urlencode($eMail);
// send email to activate code
$messageBody = "Please click on the following URL to activate your account:\n $serverURL/PA-Activate.php?actCode=$actCode&e=$eMail_Urlencoded \n";
//echo("DEBUG: messageBody = $messageBody "); exit;

// use Amazon SES
require_once('ses.php');
$ses = new SimpleEmailService('AKIAI5D3JYIQSM534C6A', 'Q0xFC1uu1pLOhMO68782UC6rG5rKSlsb1umf87AE');
$m = new SimpleEmailServiceMessage();
$m->addTo("$eMail");
$m->addBCC('Power Almanac Activation <activation@poweralmanac.com>');
$m->setFrom('Power Almanac Registration <registration@poweralmanac.com>');
$m->setReturnPath('registration@poweralmanac.com');
$m->setSubject('[Registration - Please Activate Account]');
$m->setMessageFromString("$messageBody");

$rc = $ses->sendEmail($m);

//mail("$eMail","[Registration - Please Activate Account]","$messageBody","From: registration@poweralmanac.com\r\nBcc: activation@poweralmanac.com\r\n");

// set terms of agreement flag
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
	exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}
$update_sql = sprintf("UPDATE registeredusers SET AgreeToTerms = '1'
	WHERE User_Email = '%s'
", escape($eMail));
$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
}

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL GetSubscriptionInfo2('%s',@JSON_String)", escape($sku)));
$pdoObject = $pdo->query("SELECT @JSON_String");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@JSON_String'];
$jsonArray = json_decode($json, TRUE);
$Sub_Name = $jsonArray['Sub_Name'];
$Sub_Level = $jsonArray['Sub_Level'];
$Sub_AnnualFees = $jsonArray['Sub_AnnualFees'];
$Sub_AnnualFeesFORMATTED = number_format($Sub_AnnualFees);
$Sub_PayPalCode_Sandbox = $jsonArray['Sub_PayPalCode_Sandbox'];
$Sub_PayPalCode_Production = $jsonArray['Sub_PayPalCode_Production'];
$PP_SKU = $jsonArray['Sub_SKU'];

// for tracking PayPal checkout
$_SESSION['user_email'] = $eMail;
$_SESSION['user_actcode'] = $actCode;

eval ("\$Sub_PayPalCode_Sandbox = \"$Sub_PayPalCode_Sandbox\";");
eval ("\$Sub_PayPalCode_Production = \"$Sub_PayPalCode_Production\";");

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

<script type="text/javascript">
function validateTermsOfUse()
{
	var w=document.forms["applyform"]["agreement"].checked
	if (w == false) 
	  {
	  alert("You must agree to the TERMS OF SERVICE.");
	  return false;
	  }
}
</script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="5" cellspacing="5" width="95%" align="center" border="0">
            <tr>
                <td>
                <p class="hpHeaders">Subscribe</p>
                <p>
                <b>Step 2/2 - Payment</b>
                </p>
                <p>
                Please click on the button below to pay for your subscription: <b><?php echo("$$Sub_AnnualFeesFORMATTED"); ?></b> (total due for <?php echo("$Sub_Name"); ?> subscription)
                </p>
                <?php
                // add hiddden form var custom=activationcode-email
                echo("$Sub_PayPalCode_Production"); 
                ?>
                <p>
                <b>Please Note:</b>
                <br>
                <ul>
                <li>Your payment will be processed by PayPal. The Power Almanac website will not see or get a copy of your financial information.</li>
                <li>You do NOT need a PayPal account to pay. Just a credit card.</li>
                <li>After you pay through PayPal, click on "<b>Return to Power Almanac</b>".  You'll be brought back to this site, and you'll have confirmation of your new subscription.</li>
                </ul>
                </p>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
            //include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
