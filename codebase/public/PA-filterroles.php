<?php
session_start();
//print_r($_REQUEST); exit;

$off_role = $_REQUEST['off_role'];
$searchParams = $_SESSION['lastSearchParams'];

// check which boxes and set/unset session variables
if ($off_role == '1') {
	// only govt officials with matching roles
	$_SESSION['results_GovtOffMustMatchRoles'] = '1';
	$searchParams = $searchParams . '&off_role=govtOff_role';
} else {
	// all govt officials
	$_SESSION['results_GovtOffMustMatchRoles'] = '0';
	// reset serachParms
	$searchParams = str_replace('&off_role=govtOff_role','',$searchParams);
}

//print_r($searchParams); exit;

// re-run search
$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;
header("Location: search-government.php");
flush();

?>
