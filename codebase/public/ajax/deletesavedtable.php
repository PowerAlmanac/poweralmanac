<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$record_id = $_POST['id'];

// Sanitize
$record_id = Sanitize::number($record_id);

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// get reg user id
$read_sql = sprintf("SELECT * FROM registeredusers
	WHERE User_Email = '%s'
	LIMIT 1
", escape($user_email));
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$row = PowerAlmanac\PDb::fetch_array($result_read);
$RegUser_ID = $row['RegUser_ID'];

// delete entry from table
$delete_sql = sprintf("DELETE FROM SavedTables
			  WHERE RegUser_ID = '%s' AND SavedTable_ID = '%s'
			", $RegUser_ID, $record_id);
$result_delete = @PowerAlmanac\PDb::query($delete_sql);
if (!$result_delete) {
	die('Error deleting to SavedTables database:' . PowerAlmanac\PDb::error());
}


?>