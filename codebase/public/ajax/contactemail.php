<?php
include('mandrill.php');

//recieve contact message to send to poweralmanac
if(isset($_POST['contact'])){

	if($_POST['emailContact'] == '1'){
		$toEmail = 'support@poweralmanac.com';
	}
	else if($_POST['emailContact'] == '2'){
		$toEmail = 'sales@poweralmanac.com';
	}
	else{
		$toEmail = 'info@poweralmanac.com';
	}

	$contactMessage = 'Email: ' . $_POST['userEmail'] . "\r\n\r\n" . $_POST['contact'];

	$email_data = array(
 	'to' => $toEmail,
 	'subject' => 'Contact Form Submission',
 	'from' => array('info@poweralmanac.com' =>'Power Almanac'),	
	'bcc' => '',
 	'text' => $contactMessage
 	);

 	mandrill_send($email_data);

}