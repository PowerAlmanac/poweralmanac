<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

chdir('../');
include("inc/config.php");
include("inc/protect-login.php");
chdir('ajax/');


$User_FirstName = $_SESSION['user_firstname'];
$User_LastName = $_SESSION['user_lastname'];
$parentEmail = $_SESSION['user_email'];
$User_Subscription = $_SESSION['user_subscription'];

$eMail = $_POST['eMail'];
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$dlLimits = $_POST['dlLimits'];

//echo("[$eMail]"); exit;

$_SESSION['subuser_message'] = '';
	
if ($firstName == '' || $lastName == '' || $eMail == '' || ($User_Subscription != '10' && $dlLimits == '') ) {
	http_response_code(406);
	echo 'Something\'s missing - please check your fields and try again';
	exit;
}
// check domain name
preg_match('/@.*/', $parentEmail, $domain);
$domain = $domain[0];
preg_match('/@.*/', $eMail, $sub_domain);
$sub_domain = $sub_domain[0];

if(strtolower($domain) != strtolower($sub_domain))
{
	http_response_code(406);
	echo "Your domain email addresses don't match - please try again";
	exit;
}


$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL CheckEmail('%s',@RC)", escape($eMail)));
$pdoObject = $pdo->query("SELECT @RC");
$rsArray = $pdoObject->fetchAll();
$RC = $rsArray[0]['@RC'];

if ($RC == '1') {
	// check DL limit of parent	
	$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($parentEmail)));
	$pdoObject2 = $pdo->query("SELECT @JSON_String");
	$rsArray2 = $pdoObject2->fetchAll();
	$json2a = $rsArray2[0]['@JSON_String'];
	$jsonArray2a = json_decode($json2a, TRUE);
	$parent_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
	$parent_CompanyName = $jsonArray2a['User_CompanyName'];
	$parent_ID = $jsonArray2a['RegUser_ID'];
	$parent_SubscriptionStart = $jsonArray2a['DateTime_SubscriptionStart'];
	$parent_SubscriptionEnd = $jsonArray2a['DateTime_SubscriptionEnd'];
	$parent_Subscription = $jsonArray2a['User_Subscription'];
	
	//echo("DEBUG: $dlLimits $parent_DL_Reserves"); exit;
	//allow unlimited users to give more credits
	if ($dlLimits <= $parent_DL_Reserves || $User_Subscription == 10) {
	// Prepare Security Password
	require_once("../class/Security.php");

	//set password for user
	$newpassWord = substr(md5(rand()), 0, 7);
	
	$passWord = Security::setPassword($newpassWord);

	//use security class on other posted vars
	$eMail = Sanitize::variable($eMail, "@.-+");
	$firstName = Sanitize::variable($firstName, " ,_.");
	$lastName = Sanitize::variable($lastName, " ,_.");

	// End Password Security
	
		// insert to database
		$randval = mt_rand();
		$actCode = "PA$randval";

		//give subusers unlimited if the parent is unlimited
		if($User_Subscription == 10)
		{
			$dlLimits = 1000000;
		}

		$pdo->query(sprintf("CALL CreateSubUser('%s','%s','%s','','%s','%s','%s',%s,@RC,0,%s)", escape($eMail), escape($passWord), escape($firstName), escape($lastName), escape($parent_CompanyName), escape($actCode), escape($dlLimits), escape($parent_ID)));
		// IN user_email VARCHAR(255),IN passWord VARCHAR(255),IN firstName VARCHAR(255),IN middleName VARCHAR(255),IN lastName VARCHAR(255),IN companyName VARCHAR(255),IN newUserCode VARCHAR(50),IN dlCredits NUMERIC(11,0),OUT RC BOOLEAN, IN Time_Zone INT, IN Parent BIGINT
		$_SESSION['subuser_message'] = '';
		
		// adjust parent DL
		if($User_Subscription != 10)
		{
			$new_DL_Reserves = $parent_DL_Reserves - $dlLimits;
			$pdo->query(sprintf("CALL UpdateUserDLCredits('%s',%s,@rc)", escape($parentEmail), escape($new_DL_Reserves)));
		}

		//hack to set activated to false - it's actually in the CreateSubUser procedure
		$pdo->query(sprintf("UPDATE `registeredusers` SET `User_Activated`=false WHERE `User_Email`='%s'", escape($eMail)));
		
		// use mandrill
		require_once('mandrill.php');

		$message = file_get_contents('../emailtemplates/subuser.txt');
		$message = str_replace('[Parent_Name]', $User_FirstName . " " . $User_LastName, $message);
		$message = str_replace('[NAME]', $firstName, $message);
		$message = str_replace('[Email]', $eMail, $message);
		$message = str_replace('[Password]', $newpassWord, $message);

		$mailDefaults = array(
			'from' => array('no-reply@poweralmanac.com' =>'Power Almanac Government Search'),	
			'bcc' => '',
			'html' => $message,
			'text' => FALSE,
			'to' => $eMail,
			'subject' => 'Your Subuser Account Created'
		);

		mandrill_send($mailDefaults);

		//mail("$eMail","[Sub-User Account Created for you by $parentEmail]","$messageBody","From: registration@poweralmanac.com");
		
		// copy start and end date plus subscription level
		$sql ="USE " . escape($dbname);
		$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
		if (!$dbcnx) {
			print("Unable to connect to the database server at this time.\n");
		exit();
		}
		if (!@PowerAlmanac\PDb::query($sql)){
			die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
		}
		$update_sql = sprintf("UPDATE registeredusers SET DateTime_SubscriptionStart = '%s', DateTime_SubscriptionEnd = '%s', User_Subscription = '%s'
			WHERE User_Email = '%s'
		", escape($parent_SubscriptionStart), escape($parent_SubscriptionEnd), escape($parent_Subscription), escape($eMail));
		$result_update = @PowerAlmanac\PDb::query($update_sql);
		if (!$result_update) {
			die('<p>Error updating registeredusers: '. PowerAlmanac\PDb::error().'</p>');
		}
		//echo($update_sql); exit;
		// back

	} else {
		http_response_code(406);
		echo 'You\'ve allocated too many download credits to this user. Please check your credits and try again';

	}
} else {
	http_response_code(406);
	echo 'Your email address is already in use. <br />You may have already registered but not activated your account. <br />Please check your email. (Not there?  Be sure to check your spam/junk folders too.)';

}



?>
