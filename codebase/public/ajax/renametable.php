<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$tableID = $_REQUEST['id'];
$namedTable = $_REQUEST['new_label'];

// Sanitize
$tableID = Sanitize::number($tableID);
$namedTable = Sanitize::variable($namedTable, " .-,?");

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// get reg user id
$read_sql = sprintf("SELECT * FROM registeredusers
	WHERE User_Email = '%s'
	LIMIT 1
", escape($user_email));
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
}
$row = PowerAlmanac\PDb::fetch_array($result_read);
$RegUser_ID = $row['RegUser_ID'];

$modify_sql =sprintf("UPDATE SavedTables SET Table_Name = '%s'
			  WHERE SavedTable_ID = '%s' AND RegUser_ID = '%s'
			", escape($namedTable), escape($tableID), escape($RegUser_ID));
$result_modify = @PowerAlmanac\PDb::query($modify_sql);
if (!$result_modify) {
	die('Error modifying SavedTables database:' . PowerAlmanac\PDb::error());
}

?>