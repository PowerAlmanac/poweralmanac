<?php
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', '1');
	chdir('../');
	require_once 'inc/mandrill.php';

	// Create the message
	if($_GET['emailone']){
		$emailone = $_GET['emailone'];
		if($_GET['emailtwo']){
			$emailone = array($emailone,$_GET['emailtwo']);
		}
		else if($_GET['emailthree']){
			array_push($emailone,$_GET['emailthree']);
		}
		if($_GET['message']){
			$message = $_GET['message'];
		}
		else{
			$message = 'I just found comprehensive contact information for local government officials. Enjoy!';
		}


		$defaults = array(
			'from' => array('info@poweralmanac.com' =>'Power Almanac Government Search'),	
			'bcc' => '',
			'html' => 'I just found comprehensive contact information for local government officials. Enjoy! (copy and paste into your browser) <br /><br /><a href="http://www.poweralmanac.com/search-government.php?'. $_GET['link'] .'&restoreSaved=true">Load Government Search</a>',
			'text' => FALSE,
			'to' => $emailone,
			'subject' => 'I\'ve shared government contact info with you'
		);

		
		mandrill_send($defaults);
	}
?>