<?php

//we just take the ajaxed new password, check to see if the old password matches, and also check to make sure the confirm password works
chdir('../');
include("inc/config.php");
chdir('ajax/');

//allow subusers to change their password
include("../inc/protect-login.php");

require_once("../class/Security.php");

$User_Email = $_SESSION['user_email'];

$oldPassWord = $_POST['oldPassWord'];
$newPassWord = $_POST['newPassWord'];
$confirmPassWord = $_POST['confirmPassWord'];

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$User_Password = $jsonArray2a['User_Password'];

$oldPassWord = Security::getPassword($oldPassWord, $User_Password);

if(isset($newPassWord) && isset($confirmPassWord) && isset($oldPassWord)){
	if ($User_Password != $oldPassWord) {
		// display error message
		http_response_code(406);
		echo 'invalid old password';
	} else {
		if($confirmPassWord != $newPassWord && strlen($newPassWord) < 4){
			http_response_code(405);
			echo 'new passwords unmatched';
		}else{
			// Prepare Security Password
			
			$passWord = Security::setPassword($newPassWord);
			$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
			$pdoObject2 = $pdo->query("SELECT @JSON_String");
			$rsArray2 = $pdoObject2->fetchAll();
			$json2a = $rsArray2[0]['@JSON_String'];
			$jsonArray2a = json_decode($json2a, TRUE);
			$RegUser_ID = $jsonArray2a['RegUser_ID'];

			$pdo->query(sprintf("UPDATE registeredusers SET User_Password='%s' WHERE RegUser_ID='%s' LIMIT 1", escape($passWord), escape($RegUser_ID)));
			echo 'success';
		}
	}
}else{
	echo 'please make sure all forms are filled out.';
}


?>