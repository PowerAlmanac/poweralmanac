<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$record_id = $_POST['id'];
$namedSearch = $_POST['new_label'];

// Sanitize
$record_id = Sanitize::variable($record_id, "-.,_@+ :="); // Not sure what the record contains, so added some symbols that are common to record storage.
// $namedSearch = Sanitize::safeword($namedSearch, " "); // Can't sanitize this safely without knowing what it is.

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL RenameSavedSearch('%s','%s',%s,@RC)", escape($user_email), escape($namedSearch), escape($record_id)));


?>