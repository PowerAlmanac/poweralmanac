<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$email = Sanitize::variable($_POST['email'], "@.-+");
$User_Email = $_SESSION['user_email'];

if ($_POST['alerts'] == "true") {
	$alerts = 1;
} else {
	$alerts = 0;
}
if ($_POST['newsletter'] == "true") {
	$newsletter = 1;
} else {
	$newsletter = 0;
}

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL UpdateCommSettings('%s',%s,%s,@RC)", escape($User_Email), escape($newsletter), escape($alerts))); // IN user_email VARCHAR(255),IN news TINYINT,IN alerts TINYINT, OUT RC BOOLEAN
?>
