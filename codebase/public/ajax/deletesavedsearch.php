<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$record_id = $_POST['id'];

// Sanitize
$record_id = Sanitize::number($record_id);

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL DeleteSavedSearch('%s',%s,@RC)", escape($user_email), escape($record_id)));

?>