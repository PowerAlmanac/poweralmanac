<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$parentEmail = $_SESSION['user_email'];
$subUserEmail = $_POST['e'];

// Sanitize
$subUserEmail = Sanitize::variable($subUserEmail, "@.-+");

// get sub user DL reserves
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($subUserEmail)));
$pdoObject3 = $pdo->query("SELECT @JSON_String");
$rsArray3 = $pdoObject3->fetchAll();
$json3a = $rsArray3[0]['@JSON_String'];
$jsonArray3a = json_decode($json3a, TRUE);
$subUserDL_Reserves = $jsonArray3a['User_DL_Reserves'];

//database procedure DeleteSubUser requires that activation must be true before subuser can be dropped, so we're going to set it temporarily to true
$pdo->query(sprintf("UPDATE `registeredusers` SET `User_Activated`=true WHERE `User_Email`='%s'", escape($subUserEmail)));

// remove sub user
$pdo->query(sprintf("CALL DeleteSubUser('%s',@RC)", escape($subUserEmail))); // IN user_email VARCHAR(255),OUT RC BOOLEAN

// reclaim DLL credits
$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($parentEmail)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$parent_DL_Reserves = $jsonArray2a['User_DL_Reserves'];

$new_DL_Reserves = $parent_DL_Reserves + $subUserDL_Reserves;
$pdo->query(sprintf("CALL UpdateUserDLCredits('%s',%s,@rc)", escape($parentEmail), escape($new_DL_Reserves)));

?>
