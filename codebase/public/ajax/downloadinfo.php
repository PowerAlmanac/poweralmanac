<?php
ini_set('display_errors',1); 
error_reporting(E_ALL);

chdir('../');
include_once('inc/config.php');
chdir('./ajax');

//we textualize a search based on the parameters given and output it back to help users remember what their search was about

$statesAbbrev = array(
		'Alabama' => 'AL',
		'Alaska' => 'AK', 
		'Arizona' => 'AZ', 
		'Arkansas' => 'AR', 
		'California' => 'CA', 
		'Colorado' => 'CO', 
		'Connecticut' => 'CT', 
		'Delaware' => 'DE', 
		'District of Columbia' => 'DC', 
		'Florida' => 'FL', 
		'Georgia' => 'GA', 
		'Hawaii' => 'HI', 
		'Idaho' => 'ID', 
		'Illinois' => 'IL', 
		'Indiana' => 'IN', 
		'Iowa' => 'IA', 
		'Kansas' => 'KS', 
		'Kentucky' => 'KY', 
		'Louisiana' => 'LA', 
		'Maine' => 'ME', 
		'Maryland' => 'MD', 
		'Massachusetts' => 'MA', 
		'Michigan' => 'MI', 
		'Minnesota' => 'MN', 
		'Mississippi' => 'MS', 
		'Missouri' => 'MO', 
		'Montana' => 'MT',
		'Nebraska' => 'NE',
		'Nevada' => 'NV',
		'New Hampshire' => 'NH',
		'New Jersey' => 'NJ',
		'New Mexico' => 'NM',
		'New York' => 'NY',
		'North Carolina' => 'NC',
		'North Dakota' => 'ND',
		'Ohio' => 'OH', 
		'Oklahoma' => 'OK', 
		'Oregon' => 'OR', 
		'Pennsylvania' => 'PA', 
		'Rhode Island' => 'RI', 
		'South Carolina' => 'SC', 
		'South Dakota' => 'SD',
		'Tennessee' => 'TN', 
		'Texas' => 'TX', 
		'Utah' => 'UT', 
		'Vermont' => 'VT', 
		'Virginia' => 'VA', 
		'Washington' => 'WA', 
		'West Virginia' => 'WV', 
		'Wisconsin' => 'WI', 
		'Wyoming' => 'WY'
);

if(isset($_POST['downloadName']) == false){
	die('No download file sent');
}

$downloadName = $_POST['downloadName'];

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$downloadItem = $pdo->query(sprintf("SELECT Search_Params FROM SavedDownloads WHERE Download_FileName = '%s'", escape($downloadName)));
$results = $downloadItem->fetchAll(PDO::FETCH_ASSOC);

if(isset($results[0]['Search_Params']) == false){
	die('No matching downloads');
}

//convert to array
parse_str($results[0]['Search_Params'], $queries);

//deltek
if(isset($queries['deltek']))
{
	echo('<p>This list was built in GovWin IQ</p>');
}
else
{

	//collect an array of roles
	$roles = array();
	for($i = 1; $i <= 10; $i++){
		if( isset($queries['role_' . $i]) ){$roles[] = $queries['role_' . $i];}
	}

	if(count($roles) == 10 || count($roles) == 0){
		echo('<p>Selected Roles: All');
	}else{
		echo('<p>Selected Roles: ' . implode(", ",$roles) . '</p>');
	}

	//collect population range
	if(isset($queries['populationMoreThan']) && $queries['populationMoreThan'] > 0){
		$popMore = $queries['populationMoreThan'];
	}

	if(isset($queries['populationLessThan']) && $queries['populationMoreThan'] != 100000000){
		$popLess = $queries['populationLessThan'];
	}

	if(isset($popMore) || isset($popLess)){
		echo("<p>Population Range: $popMore - $popLess</p>");
	}

	//collect government types

	$govType = array();
	if(isset($queries['govType_municipalities']) && $queries['govType_municipalities'] == 'municipality'){
		$govType[] = 'Municipalities';
	}
	if(isset($queries['govType_counties']) && $queries['govType_counties'] == 'county'){
		$govType[] = 'Counties';
	}

	if(isset($queries['govType_townships']) && $queries['govType_townships'] == 'township'){
		$govType[] = 'Townships';
	}

	if(count($govType) > 2 || count($govType) == 0){
		echo('<p>Government Types: All</p>');
	}
	else{
		echo('<p>Government Types: ' . implode(", ",$govType) . '</p>');
	}

	//location info
	if(isset($queries['byZipcode']))
	{
		$zip = $queries['byZipcode'];
		$distance = $queries['byZipcodeDistance'];
		echo("<p>Location: Zipcode of $zip with a  {$distance} mile radius</p>");
	}
	elseif(isset($queries['govLoc_byState%5B%5D']) || isset($queries['govLoc_byState']))
	{
		$states = isset($queries['govLoc_byState%5B%5D']) ? $queries['govLoc_byState%5B%5D'] : $queries['govLoc_byState'];
		$newstates = array();
		for($i = 0; $i < count($states); $i++){
			$newstates[] = $statesAbbrev[$states[$i]];
		}
		$newstates = implode(', ',$newstates);
		echo("<p>Location: $newstates</p>");
	}else
	{
		echo('<p>Location: All US</p>');
	}


	//grab expense stuff
	$filterExpense = false;
	if(isset($queries['criteria'])){
		if($queries['criteria'] == 'expenseBudget' && ( $queries['expenseBudgetLessThan'] > 0 ))
		{
			echo('<p>Expense by Annual Spending Range</p>');
			$moreThan = '$'.number_format($queries['expenseBudgetMoreThan'] * 1000);
			$lessThan = '$'.number_format($queries['expenseBudgetLessThan'] * 1000);
			echo("<p>Expense Range: More than $moreThan and less than $lessThan</p>");
			$filterExpense = true;
		}
		elseif($queries['criteria'] == 'expenseBudgetQuartile' && isset($queries['expenseBudgetQuartileSel']))
		{
			$lexicount = array('Lowest', 'Low-Medium', 'High-Medium', 'Highest','Spend $0');
			echo('<p>Spending by Quartile: ');
			$quarters = array();
			foreach($queries['expenseBudgetQuartileSel'] as $i)
			{
				if(count($queries['expenseBudgetQuartileSel']) > 2 && $i == 1){
					$lexicount[$i-1] = 'and ' . $lexicount[$i-1];
				}

				$quarters[] = $lexicount[$i-1];
			}
			if(count($queries['expenseBudgetQuartileSel']) == 2)
			{
				echo(implode(' and ', $quarters));
			}
			else{
				echo(implode(', ', $quarters));
			}
			echo('</p>');
			$filterExpense = true;
		}
		elseif($queries['criteria'] == 'perCapitalBudgetQuartile' && isset($queries['perCapitalBudgetQuartileSel']))
		{
			$lexicount = array('Lowest', 'Low-Medium', 'High-Medium', 'Highest','Spend $0');
			echo('<p>Spending per capita by quartile: ');
			$quarters = array();
			foreach($queries['expenseBudgetQuartileSel'] as $i)
			{
				if(count($queries['expenseBudgetQuartileSel']) > 2 && $i == 1){
					$lexicount[$i-1] = 'and ' . $lexicount[$i-1];
				}

				$quarters[] = $lexicount[$i-1];
			}
			if(count($queries['expenseBudgetQuartileSel']) == 2)
			{
				echo(implode(' and ', $quarters));
			}
			else{
				echo(implode(', ', $quarters));
			}
			echo('</p>');
			$filterExpense = true;
		}

		//expense categories, only matters if they filter it via spending range or quartiles
		if($filterExpense)
		{
			echo('<p>');
			if(isset($queries['finPS']))
			{
				if($queries['finPS'] == 'All')
				{
					echo('Filtering by Total Public Safety Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finPS']) . "  spending in Public Safety");
				}
			}
			elseif(isset($queries['finPW']))
			{
				if($queries['finPW'] == 'All')
				{
					echo('Filtering by Total Public Welfare Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finPW']) . "  spending in Public Welfare");
				}
			}
			elseif(isset($queries['finH']))
			{
				if($queries['finH'] == 'All')
				{
					echo('Filtering by Total Health Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finH']) . "  spending in Health");
				}
			}
			elseif(isset($queries['finU']))
			{
				if($queries['finU'] == 'All')
				{
					echo('Filtering by Total Utility Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finU']) . "  spending in Utilities");
				}
			}
			elseif(isset($queries['finT']))
			{
				if($queries['finT'] == 'All')
				{
					echo('Filtering by Total Transportation Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finT']) . "  spending in Transportation");
				}
			}
			elseif(isset($queries['finF']))
			{
				if($queries['finF'] == 'All')
				{
					echo('Filtering by Total Finance Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finF']) . "  spending in Finance");
				}
			}
			elseif(isset($queries['finL']))
			{
				if($queries['finL'] == 'All')
				{
					echo('Filtering by Total Lesiure Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finL']) . "  spending in Leisure");
				}
			}
			elseif(isset($queries['finM']))
			{
				if($queries['finM'] == 'All')
				{
					echo('Filtering by Total Misc Expense');
				}
				else{
					echo("Filtering by " . getValue($queries['finM']) . "  spending");
				}
			}
			elseif(isset($queries['expType']) && $queries['expType'] == 2){
				echo("Filtering by Capital Expenditures");
			}
			elseif(isset($queries['expType']) && $queries['expType'] == 3){
				echo("Filtering by Operating Expenditures");
			}
			elseif(isset($queries['expType']) && $queries['expType'] == 4){
				echo("Filtering by Salaries &amp; Wages");
			}
			elseif(isset($queries['expType']) && $queries['expType'] == 5){
				echo("Filtering by Expenditures on Suppliers");
			}
			else{
				echo('Filtering by Total Expense');
			}
			echo('</p>');
		}

		
	}

	//fiscal year end
	if(isset($queries['fiscalYearEnd_Month'])){
		echo('<p>Fiscal year ends in ');
		if(count($queries['fiscalYearEnd_Month']) > 2){
			$queries['fiscalYearEnd_Month'][count($queries['fiscalYearEnd_Month']) - 1] = 'and ' . $queries['fiscalYearEnd_Month'][count($queries['fiscalYearEnd_Month']) - 1];
			echo(implode(", ", $queries['fiscalYearEnd_Month']));
		}
		else{
			echo(implode(" and ", $queries['fiscalYearEnd_Month']));
		}
		echo('</p>');
	}

	//show if set to make sure there is at least one official from each government
	if(isset($queries['top_elected']))
	{
		echo('<p>Include at least one official per matched government</p>');
	}

	if(isset($queries['off_email']) && $queries['off_email'] == 'off_email')
	{
		echo('<p>Only include officials with an email address</p>');
	}
}

//print_r($queries);


//grabs either the first index of an item if it's an array or just grabs the item if it's not
function getValue($item){
	if(is_array($item)){
		$return_var = $item[0];
	}
	else{
		$return_var = $item;
	}
	return $return_var;
}