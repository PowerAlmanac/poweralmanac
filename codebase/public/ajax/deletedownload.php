<?php
chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$record_id = $_POST['id'];

// Sanitize
$record_id = Sanitize::number($record_id);

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// get file name and parse for user id and DL name
$pdo->query(sprintf("CALL GetFileName(%s,@filename)", escape($record_id))); // IN Saved_ID BIGINT, OUT FileName VARCHAR(255)
$pdoObject = $pdo->query("SELECT @filename");
$rsArray = $pdoObject->fetchAll();
$filename = $rsArray[0]['@filename'];
//echo("<p>DEBUG: filename = $filename</p>");
//list($userid, $dlcode) = explode("_",$filename);
// strip extension
$savedname = $filename; 
//echo("<p>DEBUG: userid = $userid, savedname = $savedname</p>");

$output = shell_exec("/bin/bash /var/www/sh/DeleteSavedDownload.sh \"$savedname\"");
//echo "<p>DEBUG: RC = $output for DeleteSavedDownload.sh</p>";
?>
