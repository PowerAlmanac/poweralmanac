<?php

chdir('../');
include("inc/config.php");
chdir('ajax/');
include("../inc/protect-login.php");

$parentEmail = $_SESSION['user_email'];
$subUserEmail = $_POST['eMail'];
$newDLLimits = $_POST['newDLLimits'];

// Sanitize
$subUserEmail = Sanitize::variable($subUserEmail, "@.-+");
$newDLLimits = Sanitize::number($newDLLimits);

// get current DL rerserves
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($parentEmail)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$parent_DL_Reserves = $jsonArray2a['User_DL_Reserves'];

$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($subUserEmail)));
$pdoObject3 = $pdo->query("SELECT @JSON_String");
$rsArray3 = $pdoObject3->fetchAll();
$json3a = $rsArray3[0]['@JSON_String'];
$jsonArray3a = json_decode($json3a, TRUE);
$subuser_DL_Reserves = $jsonArray3a['User_DL_Reserves'];

// check if total available
if ($newDLLimits <= ($subuser_DL_Reserves + $parent_DL_Reserves)) {
	// update sub-user DL
	$pdo->query(sprintf("UPDATE registeredusers SET User_DL_Reserves = %s WHERE User_Email = '%s';", escape($newDLLimits), escape($subUserEmail)));
	
	// adjust parent DL
	$new_DL_Reserves = $parent_DL_Reserves + $subuser_DL_Reserves - $newDLLimits;
	$pdo->query(sprintf("CALL UpdateUserDLCredits('%s',%s,@rc)", escape($parentEmail), escape($new_DL_Reserves)));

	echo 'true';
} else {
	// not enough DL credits
	http_response_code(406);
	echo 'You have allocated more credits than you have left.';
}
?>
