<?php

include("inc/config.php");
$title = 'Resend Activation Code';

$User_FirstName = $_SESSION['user_firstname'];
$User_Email = $_SESSION['user_email'];

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL GetActivationCode('%s',@actcode)", escape($User_Email)));
$pdoObject = $pdo->query("SELECT @actcode");
$rsArray = $pdoObject->fetchAll();
$actCode = $rsArray[0]['@actcode'];

$User_Email_Urlencoded = urlencode($User_Email);
// re-send email to activate code
$messageBody = "Please click on the following URL to activate your account:\n $serverURL/PA-Activate.php?actCode=$actCode&e=$User_Email_Urlencoded \n";

// use Amazon SES
require_once('ses.php');
$ses = new SimpleEmailService('AKIAI5D3JYIQSM534C6A', 'Q0xFC1uu1pLOhMO68782UC6rG5rKSlsb1umf87AE');
$m = new SimpleEmailServiceMessage();
$m->addTo("$User_Email");
$m->addBCC('Power Almanac Activation <activation@poweralmanac.com>');
$m->setFrom('Power Almanac Registration <registration@poweralmanac.com>');
$m->setReturnPath('registration@poweralmanac.com');
$m->setSubject('[Registration - Please Activate Account]');
$m->setMessageFromString("$messageBody");

$rc = $ses->sendEmail($m);

//mail("$User_Email","[Registration - Please Activate Account]","$messageBody","From: registration@poweralmanac.com\r\nBcc: activation@poweralmanac.com\r\n");

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>

<tr>
    <td align="left" colspan="2">
    <div style="padding-left:20px; padding-right:20px;">
    <br>
    <table cellpadding="5" cellspacing="5" width="99%" align="center" border="0" bgcolor="#FFFFFF">
    <tr>
    	<td>
        <h3>Resend Activation Code</h3>
        <p>
        An email has been sent to: <?php echo($User_Email); ?>
        <br /><br />
        Please check your email and click on the activation URL to enable your account. (Not there?  Be sure to check your spam/junk folders too.)
        <br />
        <br />
        After you activate your account, you can <a href="login.php">log in here</a>
        </p>
        </td>
    </tr>
    </table>
    <br>
    </div>
    </td>
</tr>
<?php
if ($_SESSION['logged_in'] != '1') {
	// not logged in
	echo('<tr><td colspan="2" align="center"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
	echo('<tr><td colspan="2">');
	//include("PA-inc-whypa.php");
	echo('</td></tr>');
}
?>
<tr>
	<td colspan="2" align="center">
    <?php
	include("inc/oldfooter.php");
	?>
    <br />
    </td>
</tr>
</table>
<br />
</body>
</html>
