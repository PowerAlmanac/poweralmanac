<?php
define("TITLE", "Your account dashboard");
include("inc/config.php");
include("inc/protect-login.php");
$title = 'Your account dashboard | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");

// get variables from account
$User_Email = $_SESSION['user_email'];
$User_FirstName = $_SESSION['user_firstname'];
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$User_Subscription = $jsonArray2a['User_Subscription'];
$_SESSION['user_subscription'] = $User_Subscription;

include("inc/init-account.php");
?>

            <div class="intro">
                <div class="intro-holder container">
                    <h1>My Account</h1>
                </div>
            </div>
            <div class="main-holder dashboard container">
                <!-- white box -->
                <div class="white-box  pl-0 pr-0">
                    <div class="row m-b-20 pl-5">
                        <div class="col-md-7">
                            <h2>Dashboard</h2>
                            <strong class="subheading">Manage Accounts, Searches, and Downloads</strong>
                        </div>
                        <div class="col-md-4 offset-md-1 pl-5">
                            <strong class="title">Try the Search</strong>
                            <form action="PA-PRG.php" method="post" class="form-search form form-inline">
                                <fieldset>
                                    <div class="form-group">
                                        <?=$officialDrop?>
                                        <input type="submit" value="Go &#187;" class="btn btn-primary" />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">

                          <div id="hoverMenu" class="navbar navbar-light bg-light sticky-top">
                              <nav class="nav nav-pills flex-row">
                                  <a href="#myaccounts" class="flex-fill text-center nav-link active"><span>My Accounts</span></a>
                                  <a href="#mysearches" class="flex-fill text-center nav-link"><span>My Searches</span></a>
                                  <a href="#mydownloads" class="flex-fill text-center nav-link"><span>My Downloads</span></a>
                              </nav>
                              <a href="#" class="btn btn-secondary">Back to Top</a>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 pl-5 pr-5">
                            <section class="sec1 alpha" id="myaccounts">

                            <?php
                             // first get parent user id, get total account records
                                $pdoUserId = $pdo->prepare("SELECT RegUser_ID from registeredusers WHERE User_Email =:email");
                                $pdoUserId->bindParam(':email', $User_Email); $pdoUserId->execute();
                                $parentId = $pdoUserId->fetch(); $parentId = $parentId['RegUser_ID'];
                                $totalAccountRecords = (int)$User_DL_Reserves;
                                if($parentId){
                                    $pdoObjectSubUsers = $pdo->prepare("SELECT User_Email, User_DL_Limit, User_Subscription, User_FirstName, User_LastName, User_DL_Reserves FROM registeredusers WHERE User_Parent = :parentId");
                                    $pdoObjectSubUsers->bindParam(':parentId', $parentId);
                                    $pdoObjectSubUsers->execute();
                                    $subUsers = $pdoObjectSubUsers->fetchAll();
                                    
                                    if($subUsers){

                                        // get total account records first by adding subusers records to primary's
                                        
                                        foreach ($subUsers as $itemArray){
                                            $totalAccountRecords = $totalAccountRecords + intval($itemArray['User_DL_Reserves']);
                                        }
                                        $totalAccountRecords = ($totalAccountRecords >= 0) ? $totalAccountRecords : 0;
                                        // can't remember adding this - it is most likely redundant with below, so am removing
                                        //echo('<strong>Total Account Records: ' . number_format($totalAccountRecords) . '</strong>');
                                    }
                                }

                                $totalAccountRecords = ($totalAccountRecords >= 0) ? $totalAccountRecords : 0;
                            ?>

                                <h2>My Accounts</h2>
                                <div class="dash-box">
                                    <h3>SUBSCRIPTION SUMMARY</h3>
                                    <strong>Current Plan:</strong><span class="fill-in"><?php echo($Sub_Name); ?></span>
                                    <strong>Start Date:</strong><span class="fill-in"><?php echo $DateTime_SubscriptionStart ?></span>
                                    <strong>End Date:</strong>
                                    <span class="fill-in"><?php echo $DateTime_SubscriptionEnd ?> &nbsp;&nbsp;
                                        <?php if($User_Subscription > 1 && $User_Subscription != 11): ?>
                                            <a href="./checkout?subscription=<?php echo $_SESSION['user_subscription'] ?>" class="btn btn-xs btn-info" style="color: white; text-decoration: none">RENEW NOW</a>
                                        <?php endif; ?>
                                    </span>
                                    <strong>Unused Records Remaining (total account):</strong><span class="fill-in">
                                        <?php if($User_Subscription == '10'){echo 'Unlimited';}else{echo number_format($totalAccountRecords);}; ?>
                                    </span>
                                    <strong>Cost per Additional Record:</strong><span class="fill-in"><?php echo("$$Sub_CostPerDL"); ?></span>
                                    <p><?php if($Sub_Level != 10){ ?><a href="pricing.php">Upgrade your plan now »</a><?php } ?></p>
                                </div>
                                <div class="dash-box omega" id="subusers">
                                    <h3>ACTIVE ACCOUNT USERS</h3>
                                    <?php echo "<p>$User_FirstName $User_LastName (PRIMARY USER)</p>"; ?>
                                    <p><?php echo($User_Email);?></p>
                                    <p>Records Downloaded: <span><?php echo($Account_DLUsed); ?></span></p>
                                    <p>Unused Records Remaining: </strong>
                                        <span id="availableCredits">
                                        <?php if($User_Subscription == '10'){
                                            echo('Unlimited');
                                        }else{
                                            $userDownloadAmount = ($User_DL_Reserves >= 0) ? $User_DL_Reserves : 0;
                                            echo(number_format(intval($userDownloadAmount)));
                                        }?>
                                        </span>
                                    </p>

                                    <?php //loop over subusers
                                         if ($User_Parent == '0' && $subUsers){
                                            $Account_DLUsedCLEANED = str_replace(",","",$Account_DLUsed);
                                            $Account_DLUsed = number_format($Account_DLUsedCLEANED);

                                                echo('<strong style="margin-top: 15px;">Subusers:</strong>');
                                                foreach ($subUsers as $itemArray){
                                                    echo('<div class="subuser">');
                                                    $SubUser_Email = $itemArray['User_Email'];
                                                    $SubUser_FirstName = $itemArray['User_FirstName'];
                                                    $SubUser_LastName = $itemArray['User_LastName'];
                                                    $SubUser_DL_Limit = number_format(intval($itemArray['User_DL_Limit']));
                                                    $SubUser_DL_Reserves = number_format($itemArray['User_DL_Reserves']);
                                                    $SubUser_Subscription = $itemArray['User_Subscription'];

                                                    // get downloads used via account snapshot
                                                    $numDownloads = 0;
                                                    $readPaid_sql = sprintf("SELECT * FROM userpay WHERE UserEmail = '%s'", escape($SubUser_Email));
                                                    $result_readPaid = @PowerAlmanac\PDb::query($readPaid_sql);
                                                    if (!$result_readPaid) {
                                                        die('Error reading from userpay table database:' . PowerAlmanac\PDb::error());
                                                    }

                                                    while ($row = PowerAlmanac\PDb::fetch_array($result_readPaid))
                                                    {
                                                        $NumRecords = $row['NumRecords'];
                                                        $numDownloads = $numDownloads + $NumRecords;
                                                    }
                                                    
                                                    $numDownloads = number_format($numDownloads);
                                                    
                                                    //echo first and last name with links to edit the amount of credits as well
                                                    echo("<p>$SubUser_FirstName $SubUser_LastName (SUB-USER)<a class='editsubuser' data-val='$SubUser_Email'><img src='images/edit.png' alt='edit credits' title='edit credit' border='0'></a> <a class='deletesubuser' href='' data-val='$SubUser_Email'><img src='images/delete.png' alt='delete' title='delete' border='0'></a></p>");
                                                    echo("<p>$SubUser_Email</p>");
                                                    echo("<p>Download credits used: <span>$numDownloads</span></p>");
                                                    if($SubUser_Subscription == '10'){
                                                        echo '<p>Unlimited download credits.</p>';
                                                    }else{
                                                        $userDownloadAmount = ($SubUser_DL_Reserves >= 0) ? $SubUser_DL_Reserves : 0;
                                                        echo("<p>Available download credits: <span class='availableCredits'>$userDownloadAmount</span></p>");
                                                    }
                                                    echo("</div>");
                                                }
                                            }
                                            else{
                                                echo('<p class="hinted" id="nosub">You don\'t have any subusers associated with your account.</p>');
                                            }
                                        ?>

                                        <div class="subuser" id="addsubbefore">
                                        <?php
                                            if ($User_Parent == '0')
                                            {
                                                // if free email account, disable CREATE SUB USER and display MESSAGE
                                                $freeEmailArray = array(0 => 'gmail.com', 1 => 'yahoo.com', 2 => 'aim.com', 3 => 'aol.com');
                                                $pos = strpos($User_Email, '@');
                                                if ($pos !== false){
                                                    $extractStart = $pos + 1;
                                                    $domainName = substr($User_Email, $extractStart);
                                                }

                                                $key = array_search($domainName, $freeEmailArray); 
                                                if ($key === false) {
                                                    echo("<p><a class='addSubuser'>+ Add a new sub-user</a></p>");
                                                    echo("<p class='hinted'>*Sub-users must work in the same organization as you, with an email address that shares the same domain name (e.g. @poweralmanac.com).</p>");
                                                } else {
                                                    echo("<p class='hinted'>You cannot create sub-users because the email you used for creating your Power Almanac account does not include your organization's domain name (@YourOrgDomain.com).</p>");
                                                }
                                            }
                                            else
                                            {
                                                echo("<p class='hinted'>Only parent account holders can now create subusers. For help, please <a href='http://www.poweralmanac.com/contact-us'>contact us</a> </p>");
                                            }
                                        ?>
                                    </div>
                                </div>

                                <div class="dash-box">
                                    <h3>LOG IN INFO</h3>
                                    <strong>Email Address: </strong><span class="fill-in"><?php echo($User_Email);?></span>
                                    <strong>Password: </strong><span class="fill-in"><a id="changePass" href="">Change Password</a></span>
                                </div>
                                 <div class="dash-box omega">
                                    <h3>COMMUNICATION SETTINGS</h3>
                                    <input type="hidden" name="email" value="<?php echo($User_Email); ?>">
                                    <label><input id="getalerts" name="alerts" type="checkbox" value="" <?php echo($optinAlerts); ?>>Email Weekly Alerts</label>
                                    <label><input id="getnews" name="newsletter" type="checkbox" value="" <?php echo($optinNewsletter); ?>>Email Monthly Newsletter</label>
                                    <button id="updateCom">Save Changes</button>
                                </div>
                            </section>

                            <?php
                                // include init files here for specific page
                                include("inc/init-searches.php");
                            ?>

                            <section class="sec1" id="mysearches">
                                <h2>My Saved Searches</h2>
                                <table id="mysearchestable">
                                    <thead>
                                        <td>NAME OF SEARCH</td>
                                        <td>TIME CREATED (GMT)</td>
                                        <td>LAST RUN (GMT)</td>
                                        <td>RECORDS</td>
                                        <td>ACTIONS</td>
                                    </thead>

                                    <?php
                                    //loop through saved searches and output rows
                                    $readSearches_sql = sprintf("SELECT * FROM SavedSearches WHERE RegUser_ID = '%s' ORDER BY SavedSer_ID DESC", escape($RegUser_ID));
                                    $result_readSearches = @PowerAlmanac\PDb::query($readSearches_sql);
                                    if (!$result_readSearches) {
                                        die('Error reading from SavedSearches database:' . PowerAlmanac\PDb::error());
                                    }
                                    $num_rows = PowerAlmanac\PDb::num_rows($result_readSearches);
                                    if ($num_rows != '0') {
                                        while ($row = PowerAlmanac\PDb::fetch_array($result_readSearches))
                                        {
                                            // ID missing
                                            $Search_ID = $row['SavedSer_ID'];
                                            $Search_Name = $row['Search_Name'];
                                            $Search_Params = $row['Search_Params'];
                                            $Search_NumRecords = number_format($row['Search_NumRecords']);
                                            $Search_NewRecords = number_format($row['Search_NewRecords']);
                                            $Search_UpdatedRecords = number_format($row['Search_UpdatedRecords']);
                                            $DateTime_Saved = date("m/d/y h:i A",strtotime($row['DateTime_Saved']));
                                            if ($row['DateTime_Rerun'] == '') {
                                                $DateTime_Rerun = 'never';
                                            } else {
                                                $DateTime_Rerun = date("m/d/y h:i A",strtotime($row['DateTime_Rerun']));
                                            }

                                            //bolden up wording, add class if new records are available
                                            $boldclass = "";
                                            if($Search_NewRecords > 0){
                                                $boldclass = "strong";
                                            }
                                            //echo("<td align='right'><p>$Search_NumRecords</p></td><td align='right'><p>$Search_NewRecords</p></td><td align='right'><p>$Search_UpdatedRecords</p></td><td align='right'><p><a href='PA-RenameSearch.php?id=$Search_ID'><img src='images/edit.png' alt='rename search' title='rename search' border='0'></a> <a href='search-government.php?".$Search_Params."&restoreSaved=true'><img src='images/refresh.png' alt='re-run search' title='re-run search' border='0'></a> <a href='PA-DeleteSearch.php?id=$Search_ID'><img src='images/delete.png' alt='delete' title='delete' border='0'></a> <a href='PA-ShareSavedSearch.php?id=$Search_ID'><img src='images/share.png' alt='share' title='share' border='0'></a> </p></td></tr>");
                                            ?>
                                            <tr>
                                                <td><?php echo("<a href='search-government.php?$Search_Params&restoreSaved=true' class='restoreLink'>$Search_Name</a>"); ?></td>
                                                <td><?php echo("<p>$DateTime_Saved</p>"); ?></td>
                                                <td><?php echo("<p>$DateTime_Rerun</p>"); ?></td>
                                                <?php /*<td><?php echo("<p>$Search_NumRecords <span class='$boldclass'>($Search_NewRecords new)</span></p>"); ?></td>*/ ?>
                                                <td><?php echo("<p>$Search_NumRecords</p>"); ?></td>
                                                <td><?php echo("<a href='' data-val='$Search_ID' class='edit'><img src='images/edit.png' alt='Rename' title='Rename'></a> <a class='delete' href='' data-val='$Search_ID'><img src='images/delete.png' alt='delete' title='delete'></a> <a href='' class='share'><img src='images/share.png' alt='share' title='share' border='0'></a></td>"); ?>
                                            </tr>
                                        <?php }
                                    } else {
                                        echo("<tr><td colspan='5'><p>You have no saved searches.</p></td></tr>");
                                    }
                                    ?>
                                    <tr>
                                    </tr>
                                </table>

                                <h2>My Saved Tables</h2>
                                <table id="mysavedtable">
                                    <thead>
                                        <td>NAME OF SEARCH</td>
                                        <td>TIME CREATED (GMT)</td>
                                        <td>LAST RUN (GMT)</td>
                                        <td>RECORDS</td>
                                        <td>ACTIONS</td>
                                    </thead>

                                    <?php
                                    //loop through saved tables and output rows
                                    $readTables_sql = sprintf("SELECT * FROM SavedTables
                                        WHERE RegUser_ID = '%s'
                                        ORDER BY SavedTable_ID DESC
                                    ", escape($RegUser_ID));
                                    $result_readTables = @PowerAlmanac\PDb::query($readTables_sql);
                                    if (!$result_readTables) {
                                        die('Error reading from SavedTables database:' . PowerAlmanac\PDb::error());
                                    }
                                    $num_rows = PowerAlmanac\PDb::num_rows($result_readTables);
                                    if ($num_rows != '0') {
                                        while ($row = PowerAlmanac\PDb::fetch_array($result_readTables)){
                                            $Table_Name = $row['Table_Name'];
                                            $SavedTable_ID = $row['SavedTable_ID'];
                                            $Search_NumRecords = number_format($row['Search_NumRecords']);
                                            $DateTime_Saved = date("m/d/y h:i A",strtotime($row['DateTime_Saved']));
                                            $DateTime_Rerun = date("m/d/y h:i A",strtotime($row['DateTime_Rerun']));
                                            //echo("<td align='right'><p>$Search_NumRecords</p></td><td align='right'><p>$Search_NewRecords</p></td><td align='right'><p>$Search_UpdatedRecords</p></td><td align='right'><p><a href='PA-RenameSearch.php?id=$Search_ID'><img src='images/edit.png' alt='rename search' title='rename search' border='0'></a> <a href='search-government.php?".$Search_Params."&restoreSaved=true'><img src='images/refresh.png' alt='re-run search' title='re-run search' border='0'></a> <a href='PA-DeleteSearch.php?id=$Search_ID'><img src='images/delete.png' alt='delete' title='delete' border='0'></a> <a href='PA-ShareSavedSearch.php?id=$Search_ID'><img src='images/share.png' alt='share' title='share' border='0'></a> </p></td></tr>");
                                            ?>
                                            <tr>
                                                <td><?php echo("<a class='restoreLink' href='PA-RunTable.php?id=$SavedTable_ID'>$Table_Name</a>"); ?></td>
                                                <td><?php echo("<p>$DateTime_Saved</p>"); ?></td>
                                                <td><?php echo("<p>$DateTime_Rerun</p>"); ?></td>
                                                <td></td>
                                                <td><?php echo("<a class='edit' href='' data-val='$SavedTable_ID'><img src='images/edit.png' alt='rename table' title='rename table'></a> <a class='delete' data-val='$SavedTable_ID'><img src='images/delete.png' alt='delete' title='delete table'></a> <a class='share' href='PA-ShareSavedSearch.php?id=$SavedTable_ID'><img src='images/share.png' alt='share' title='share'></a></td>"); ?>
                                            </tr>
                                        <?php }
                                    } else {
                                        echo("<tr><td colspan='5'><p>You have no saved tables</p></td></tr>");
                                    }
                                    ?>
                                    <tr>
                                    </tr>
                                </table>
                            </section>


                            <section class="sec1" id="mydownloads">
                                <h2>My Downloads</h2>
                                <table id="mydownloadstable">
                                    <thead>
                                        <td>FILE NAME</td>
                                        <td>DOWNLOADED (GMT)</td>
                                        <td>SEARCH</td>
                                        <?php //<td>CREDITS USED</td> ?>
                                        <td>RECORDS</td>
                                        <td>UPDATED RECORDS</td>
                                        <td>ACTIONS</td>
                                    </thead>
                                <?php
                                    $readDownloads_sql = sprintf("SELECT * FROM SavedDownloads
                                        WHERE RegUser_ID = '%s'
                                        ORDER BY SavedDownloads_ID DESC
                                    ", escape($RegUser_ID));
                                    $result_readDownloads = @PowerAlmanac\PDb::query($readDownloads_sql);
                                    if (!$result_readDownloads) {
                                        die('Error reading from SavedDownloads database:' . PowerAlmanac\PDb::error());
                                    }
                                    $num_rows = PowerAlmanac\PDb::num_rows($result_readDownloads);
                                    if ($num_rows != '0') {
                                        while ($row = PowerAlmanac\PDb::fetch_array($result_readDownloads))
                                        {
                                            $Download_ID = $row['SavedDownloads_ID'];
                                            $Download_FileName = $row['Download_FileName'];
                                            $Download_AccID = substr($Download_FileName, 0, -4);
                                            $Search_Name = $row['Search_Name'];
                                            $Search_Params = $row['Search_Params'];
                                            $Credits_Used = $row['Credits_Used'];
                                            $Download_NumRecords = number_format($row['Download_NumRecords']);
                                            $DateTime_Saved = date("m/d/y h:i A",strtotime($row['DateTime_Saved']));
                                            // for delete
                                            $Search_NameENCODED = urlencode($Search_Name);
                                            $Download_NewRecords = number_format($row['Download_NewRecords']);
                                            $Download_UpdatedRecords = number_format($row['Download_UpdatedRecords']);
                                            ?>
                                                <tr>
                                                    <td><?php echo("<a id='$Download_AccID' data-placement='bottom' href='../dl/$Download_FileName' target='_blank'>$Download_FileName</a>") ?></td>
                                                    <td><?php echo("<p>$DateTime_Saved</p>") ?></td>
                                                    <td><?php echo("<p>$Search_Name</p>"); ?></td>
                                                    <?php /*<td><?php echo("<p>$Credits_Used</p>") ?></td>
                                                    <td><?php echo("<p>$Download_NumRecords ($Download_NewRecords new)</p>"); ?></td>*/?>
                                                    <td><?php echo("<p>$Download_NumRecords</p>"); ?></td>
                                                    <td><?php echo("$Download_UpdatedRecords"); ?></td>
                                                    <td><?php echo("<div class='download-info' data-val='$Download_AccID' data-search='$Search_Params'><img src='images/info-btn.png'></div>&nbsp;&nbsp;&nbsp;<a rel='shadowbox;height=250;width=825' href='PA-DownloadChoices.php?id=$Download_ID' title=''><img src='images/download-choices.png' alt='download choices' title='download choices'></a>&nbsp;&nbsp;<a class='delete' data-val='$Download_ID'><img src='images/delete.png' alt='delete' title='delete'></a>") ?></td>
                                                </tr>
                                            <?php
                                            //echo("<tr></td><td align='right'><p>$Download_NumRecords</p></td><td align='right'><p>$Credits_Used</p></td><td align='right'><p>$Download_NewRecords</p></td><td align='right'><p>$Download_UpdatedRecords</p></td><td align='right'><p><a href='PA-RunSearch4sd.php?id=$Download_ID'><img src='images/refresh.png' alt='re-run search' title='re-run search' border='0'></a> <a rel='shadowbox;height=250;width=825' href='PA-DownloadChoices.php?id=$Download_ID' title=''><img src='images/download-choices.png' alt='download choices' title='download choices' border='0'></a> <a href='PA-DeleteDownload.php?id=$Download_ID'><img src='images/delete.png' alt='delete' title='delete' border='0'></a></p></td></tr>");
                                        }
                                    } else {
                                        echo("<tr><td colspan='9'><p>You have no saved downloads.</p></td></tr>");
                                    }
                                    //show parent's downloads if subuser
                                    if($User_Parent != '0' && 1==2){
                                        $readDownloads_sql = sprintf("SELECT * FROM SavedDownloads
                                            WHERE RegUser_ID = '%s'
                                            ORDER BY SavedDownloads_ID DESC
                                            ", escape($User_Parent));
                                        $result_readDownloads = @PowerAlmanac\PDb::query($readDownloads_sql);
                                        if (!$result_readDownloads) {
                                            die('Error reading from SavedDownloads database:' . PowerAlmanac\PDb::error());
                                        }
                                        $num_rows = PowerAlmanac\PDb::num_rows($result_readDownloads);
                                        if ($num_rows != '0') {
                                            while ($row = PowerAlmanac\PDb::fetch_array($result_readDownloads))
                                            {
                                                $Download_ID = $row['SavedDownloads_ID'];
                                                $Download_FileName = $row['Download_FileName'];
                                                $Download_AccID = substr($Download_FileName, 0, -4);
                                                $Search_Name = $row['Search_Name'];
                                                $Search_Params = $row['Search_Params'];
                                                $Credits_Used = $row['Credits_Used'];
                                                $Download_NumRecords = number_format($row['Download_NumRecords']);
                                                $DateTime_Saved = date("m/d/y h:i A",strtotime($row['DateTime_Saved']));
                                                // for delete
                                                $Search_NameENCODED = urlencode($Search_Name);
                                                $Download_NewRecords = number_format($row['Download_NewRecords']);
                                                $Download_UpdatedRecords = number_format($row['Download_UpdatedRecords']);
                                                ?>
                                                <tr>
                                                    <td><?php echo("<a id='$Download_AccID' data-placement='bottom' href='../dl/$Download_FileName' target='_blank'>$Download_FileName</a>") ?></td>
                                                    <td><?php echo("<p>$DateTime_Saved</p>") ?></td>
                                                    <td><?php echo("<p>$Search_Name</p>"); ?></td>
                                                    <td><?php echo("<p>$Credits_Used</p>") ?></td>
                                                    <td><?php echo("<p>$Download_NumRecords ($Download_NewRecords new)</p>"); ?></td>
                                                    <td><?php echo("$Download_UpdatedRecords"); ?></td>
                                                    <td><?php echo("<div class='download-info' data-val='$Download_AccID' data-search='$Search_Params'><img src='images/info-btn.png'></div>&nbsp;&nbsp;&nbsp;<a rel='shadowbox;height=250;width=825' href='PA-DownloadChoices.php?id=$Download_ID' title=''><img src='images/download-choices.png' alt='download choices' title='download choices'></a>") ?></td>
                                                </tr>
                                                <?php
                                                //echo("<tr></td><td align='right'><p>$Download_NumRecords</p></td><td align='right'><p>$Credits_Used</p></td><td align='right'><p>$Download_NewRecords</p></td><td align='right'><p>$Download_UpdatedRecords</p></td><td align='right'><p><a href='PA-RunSearch4sd.php?id=$Download_ID'><img src='images/refresh.png' alt='re-run search' title='re-run search' border='0'></a> <a rel='shadowbox;height=250;width=825' href='PA-DownloadChoices.php?id=$Download_ID' title=''><img src='images/download-choices.png' alt='download choices' title='download choices' border='0'></a> <a href='PA-DeleteDownload.php?id=$Download_ID'><img src='images/delete.png' alt='delete' title='delete' border='0'></a></p></td></tr>");
                                            }
                                        }
                                    }


                                ?>
                                </table>
                            </section>
                        </div>
                    </div>
                    <!-- subfooter -->
                    <div class="row col-md-10 subfooter mt-4 pl-4 pr-4">
                        <div class="col-md-8 text-center text-sm-left">
                            <strong class="title">Need more convincing? Try Power Almanac</strong>
                        </div>
                        <div class="col-md-4 text-center text-sm-right">
                            <a href="/search-government?ns=1" class="btn btn-light" role="button">Search for Officials</a>
                        </div>
                    </div>
                </div>

                <?php //Change Password Modal ?>
                <div id="passModal" class="modal hide fade half-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Change Your Password</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="passShareForm">
                                <div class="modal-body">
                                    <p id="passerror" class="hinted" style="color: red"></p>

                                    <div class="form-group">
                                        <label for="oldpass">Old Password</label>
                                        <input type="password" class="form-control" id="oldpass" required="required" />
                                    </div>

                                    <div class="form-group">
                                        <label for="newpass">New Password</label>
                                        <input type="password" class="form-control" id="newpass" required="required" />
                                    </div>

                                    <div class="form-group">
                                        <label for="confirmpass">Confirm Password</label>
                                        <input type="password" class="form-control" id="confirmpass" required="required" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" id="ajaxpasssave">Create User</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php //Share Search Modal ?>
                <div id="shareModal" class="modal hide fade half-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Share</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="shareForm">
                                <div class="modal-body">
                                    <p id="passerror" class="hinted" style="color: red"></p>

                                    <div class="form-group">
                                        <label for="shareName">Your Name</label>
                                        <input type="text" class="form-control" id="shareName" required="required" value="<?php echo $User_FirstName ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="shareEmail">Your Email</label>
                                        <input type="email" class="form-control" id="shareEmail" required="required" value="<?php echo $User_Email ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="recipientEmail">Recipient Email</label>
                                        <input type="email" class="form-control" id="recipientEmail" required="required" />
                                    </div>

                                    <div class="form-group">
                                        <label for="shareMessage">Message</label>
                                        <textarea class="form-control" id="shareMessage" rows="6">
Hi,
I thought you'd want to see this Power Almanac Search of local government officials.

Regards,
<?php echo $User_FirstName ?>
                                        </textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" id="ajaxshare">Share!</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php //Share Table Modal ?>
                <div id="shareTableModal" class="modal hide fade half-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Share</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="shareTableForm">
                                <div class="modal-body">
                                    <p id="passerror" class="hinted" style="color: red"></p>

                                    <div class="form-group">
                                        <label for="shareTableName">Your Name</label>
                                        <input type="text" class="form-control" id="shareTableName" required="required" value="<?php echo $User_FirstName ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="shareTableEmail">Your Email</label>
                                        <input type="email" class="form-control" id="shareTableEmail" required="required" value="<?php echo $User_Email ?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="recipientTableEmail">Recipient Email</label>
                                        <input type="email" class="form-control" id="recipientTableEmail" required="required" />
                                    </div>

                                    <div class="form-group">
                                        <label for="shareTableMessage">Message</label>
                                        <textarea class="form-control" id="shareTableMessage" rows="6">
Hi,
I thought you'd want to see this Power Almanac Table of local government officials.

Regards,
<?php echo $User_FirstName ?>
                                        </textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" id="ajaxtableshare">Share!</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <?php //Create SubUser Modal ?>
                <div id="subUserModal" class="modal hide fade half-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Create Subuser</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="subUserForm">
                                <div class="modal-body">
                                    <p id="subUserError" class="hinted" style="color: red"></p>
                                    <div class="form-group">
                                        <label for="subuserEmail">Subuser's Email</label>
                                        <input type="email" class="form-control" id="subuserEmail" aria-describedby="emailHelp" required="required" placeholder="Email Address" />
                                        <small id="emailHelp" class="form-text text-muted">Must be the same domain as your email</small>
                                    </div>

                                    <div class="form-group">
                                        <label for="subFirst">Subuser's First Name</label>
                                        <input type="text" class="form-control" id="subFirst" required="required" placeholder="First Name" />
                                    </div>

                                    <div class="form-group">
                                        <label for="subLast">Subuser's Last Name</label>
                                        <input type="text" class="form-control" id="subLast" required="required" placeholder="First Name" />
                                    </div>

                                    <?php /* don't show this to unlimited subscribers */
                                    if ($User_Subscription != "10") { ?>
                                        <div class="form-group">
                                            <label for="creditLimit">Sub-users Download Credit Limit</label>
                                            <input id="creditLimit" class="form-control" aria-describedby="creditLimitHelp"  type="number" min="0" max="<?php echo str_replace(",", "", $User_DL_Reserves) ?>">
                                            <small id="creditLimitHelp" class="form-text text-muted">This subuser won't be able to use more than this number of download credits. You can edit this number later.</small>
                                        </div>

                                        <div class="form-group">
                                            <label for="credits">Max Download Credit Limit</label>
                                            <input id="credits" class="form-control" aria-describedby="creditsHelp" type="text" disabled="disabled">
                                            <small id="creditsHelp" class="form-text text-muted">This is the total amount of your account's remaining download credits.</small>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" id="savesub">Create User</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <script>
                var userCredits = <?php echo str_replace(",", "", $User_DL_Reserves) ?>;
                var userEmail = '<?= $User_Email ?>';
                var userLevel = '<?= $User_Subscription ?>'
            </script>
            <script src="js/dashboard.js"></script>
<?php include("inc/footer.php"); ?>            
