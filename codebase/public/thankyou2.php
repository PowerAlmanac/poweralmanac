<?php
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		$to      = 'tim@blendimc.com';
		$subject = 'Landing Page - Credits Requested';
		$message = $_POST['email'] . ' has filled out the form requesting credits.';
		$headers = 'From: info@poweralmanac.com' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);
	}
?>
<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/animate.css"></link>
<script src="js/wow.min.js"></script>
<script>
new WOW().init();
</script>
<meta charset="utf-8">
<title>Thank you</title>

<style type="text/css">
body,td,th {
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
	font-size: 36px;
	width: 100%;
	min-width: 500px;
	margin:0px;
}
body {
	/* background-image: url(Email); */
	font-size: 14px;
	text-align: center;
	float: none;
	margin:0px auto;
}
#wrapper {
background: #e3e3e3;
}

.content {
margin: 0 auto;
text-align:center;
width: 85%;
border: 1px solid #c8c7c7;
background: white;
max-width: 1000px;
}
.main-image img{
width:100%;
}

.h3 {
font-size: 32px;
font-decoration: underline;
font-weight:bold;
font-color: black;
font-family: 'Arial';
}

.h3:visited {

}

.orange-button {
font-size: 32px;
font-weight:bold;
padding: 15px 45px 15px 45px;
margin-bottom: 15px;
background-color: #ec8b00;
box-shadow: 0px 3px 2px 0px #616161;
color:white;
border-radius: 10px;
}
.orange-button:hover {
background-color: #d57d00;
cursor:pointer;
}
.input { 
padding: 15px 15px 10px 15px;
margin-bottom: 15px;
margin-top: 15px;
font-size: 28px;
text-align: center;
}

.title {
font-family: "Segoe UI bold";
font-size: 24px;
color:dark gray;
width: 70%;
margin: 0 auto;
margin-top: 50px;
max-width: 1210px;
}

.text {
font-family: "Segoe UI";
font-size: 24px;
width: 70%;
margin: 0 auto;
margin-top: 50px;
max-width: 1210px;
}

.text2 {
font-family: "Segoe UI";
font-size: 16px;
width: 70%;
margin: 0 auto;
margin-top: 20px;
max-width: 1210px;
}

.asterisk-text {
font-family: "Segoe UI";
font-size: 16px;
width: 70%;
margin: 0 auto;
margin-top: 40px;
max-width: 1210px;
}

.play-button {
margin-left:auto;
margin-right: auto;
text-align: center;
background-image: url(images\thankyou.png);
background-position:0 -163px;
background-image: url('images/playbuttonsprite.png');
width: 133px; 
height: 133px;
margin-top: 25px;
}

</style>
</head>

<body>

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('8961-875-10-8869');/*]]>*/</script><noscript><a href="https://www.olark.com/site/8961-875-10-8869/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

<div id="wrapper">
	<div class="content">
	<div class="main-image"><img src="images/thankyou.png" ></div>

<p class="title"><b>Why do I have to wait?</b> 

<p class="text2">We have our sales staff working to get your credits prepared for you! Once they are, we’ll give you an email letting you know.</p>

<p> </p>

<p class="title"><b>Do I get to choose my records?</b>
<p class="text2">Absolutely! Once we put the records in your account we’ll send you an email detailing how to choose your 1000 records. Watch the video below for a walk-through.</p>         
<br>

</br>
<br>

<iframe width="700" height="400" src="//www.youtube.com/embed/ZhcBT-BlCww?rel=0" frameborder="0" allowfullscreen></iframe>


</div></div>
</div>
</body>
</html>
