<?php

include("inc/config.php");
include("inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$savedSearchID = $_REQUEST['id'];

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL SavedSearchLookup('%s',%s,@Params)", escape($user_email), escape($savedSearchID)));
$pdoObject = $pdo->query("SELECT @Params");
$rsArray = $pdoObject->fetchAll();
$searchParams = $rsArray[0]['@Params'];
$lastSearchName = 'TBD';
/*
$lookupParams = new PAUser_Account; 
$searchParams = $lookupParams->SavedSearchLookup($savedSearchID,$user_email);
$lastSearchName = $lookupParams->User_SavedSearchName;
*/

$_SESSION['lastSearchName'] = $lastSearchName;

$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;
$_SESSION['savedSearchName'] = $lastSearchName;
$_SESSION['lastSearchParams'] = $searchParams;

$searchURL = "PA-ShareSearch.php";
header("Location: $searchURL");
flush();


?>