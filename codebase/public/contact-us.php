<?php
define("TITLE", "Contact & Support - Power Almanac");
define("DESCRIPTION", "Contact information - let us know how we can help you.");
include("inc/config.php");
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder container">
                    <h1>Contact Us</h1>
                </div>
            </div>
            <div class="container main-holder">
                <!-- white box -->
                <div class="white-box">
                    <h1 class="display-4">Email Us</h1>

                    <section class="pt-3 row border-row">
                        <div class="col-8">
                            <form id="contactForm">

                                <div class="form-group">
                                    <label for="userEmail"></label>
                                    <input type="email" class="form-control" id="userEmail" required="required" placeholder="Enter your email">
                                </div>

                                <div class="form-group">
                                    <label for="question">My question / comment is about</label>
                                    <select id="question" class="form-control" required="required">
                                        <option value="1">support</option>
                                        <option value="2" selected="selected">sales</option>
                                        <option value="3">other</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <textarea id="contact-data" class="form-control">Enter your message here..</textarea>
                                </div>

                                <div class="form-group text-right">
                                    <button id="contactBtn" class="btn btn-primary">Contact Us</button>
                                </div>
                            </form>
                            <div id="contactFeedback" style="display: none;">
                                Thank you for your message.<hr/>Our team has received it and will reply shortly.
                            </div>
                    </section>

                    <h1 class="display-4 mb-3 mt-4">Or call us at <br class="d-block d-sm-none"/><a style='font-size: 44px;' href="tel:650-539-2340">650-539-2340</a></h1>
                    <section class="row border-row pb-4">
                        <dl class="row">
                            <dt class="col-sm-4">For SALES (dial extension 201):</dt>
                            <dd class="col-sm-8"> ask questions or sign-up for a POWER plan</dd>

                            <dt class="col-sm-4">For SUPPORT (dial extension 101):</dt>
                            <dd class="col-sm-8"> help with your data, adding users to your account, payment, etc.</dd>

                            <dt class="col-sm-4">For ANY REASON (dial extension 916):</dt>
                            <dd class="col-sm-8"> talk to Ron Mester, the founder and CEO</dd>
                        </dl>
                    </section>

                    <h1 class="display-4 mb-3 mt-4">Or send us a note</h1>
                    <section class="row mb-2 pb-4">
                        <div class="col-12">
                            <strong class="subtitle">Power Almanac</strong>
                            <p>205 De Anza Blvd #59<br>
                                San Mateo, CA &nbsp;94402</p>

                            <hr />
                            <dl class="row">
                                <dt class="col-sm-4">Ron Mester, Founder and CEO:</dt><dd class="col-sm-8">ron.mester@poweralmanac.com</dd>
                            </dl>

                        </div>
                    </section>

                    <div class="row subfooter mt-4">
                        <div class="col-lg-9 col-md-8 col-sm-7 text-center text-sm-left">
                            <strong class="title">Don't want to talk? Try Power Almanac</strong>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-5 text-center text-sm-right">
                            <a href="/search-government" class="btn btn-secondary" role="button">Search for Officials</a>
                        </div>
                    </div>

                </div>
            </div>
            <script>
                $(document).ready(function(){
                    $('.selectpicker').selectpicker();

                    $('#contact-data').one('mouseup',function(){
                        $(this).select();
                    });

                    $('#contactForm').submit(function(e){
                        e.preventDefault();
                        if($(this).hasClass('sent')){
                            //do nothing
                        }
                        else{
                            $('#contactBtn').text('Sending...').addClass('disabled');
                            $(this).addClass('sent');
                            console.log($('#question option:selected').val());
                            $.ajax({
                                type: 'post',
                                data: {
                                    contact: $('#contact-data').val(),
                                    emailContact: $('#question option:selected').val(),
                                    userEmail: $('#userEmail').val()
                                },
                                url: 'ajax/contactemail.php',
                                success: function(){
                                    $('#contactBtn').text('Complete');
                                    $('#contact-data').attr('disabled','');
                                    $('#contactForm').hide();
                                    $('#contactFeedback').show();
                                    $('#contact-data').text('Thank you for your comments - we sincerely appreciate them.');
                                }
                            })
                        }
                    })
                });
            </script>
<?php include("inc/footer.php"); ?>            
