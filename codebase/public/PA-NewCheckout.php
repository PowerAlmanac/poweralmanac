<?php
include("inc/config.php");
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);


        $searchParams = $_SESSION['lastSearchParams'];

        if ($_SESSION['logged_in'] != '1') {
            $User_Subscription = '0';
            $_SESSION['user_subscription'] = '0';
            $_SESSION['RegUser_ID'] = '0';
            $RegUser_ID = 0;
            $timeIs = time();
            $tableID = $RegUser_ID . '_' . substr($timeIs,-6);
        } else {
            $User_Subscription = $_SESSION['user_subscription'];
            $RegUser_ID = $_SESSION['RegUser_ID'];
            $timeIs = time();
            $tableID = $RegUser_ID . '_' . substr($timeIs,-6);
        }

        

        //echo("DEBUG: Results - Search Summary request ($newSearch:$useSavedSearchParams)<br>");
        $useCache = 0;
        // handling search result tables
        if (isset($_SESSION['previousTableID'])) {
            // delete previous table
            $previoustableID = $_SESSION['previousTableID'];
            $pdo->query(sprintf("CALL Delete_Research_Tables('%s')", escape($previoustableID)));
            // now
            $_SESSION['previousTableID'] = $tableID;
            $_SESSION['currentTableID'] = $tableID;
        } else {
            // first time
            $_SESSION['previousTableID'] = $tableID;
            $_SESSION['currentTableID'] = $tableID;
        }
        
        $pdo->query(sprintf("CALL SearchSummary2('%s','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($tableID), escape($searchParams)));
        //echo("<p>DEBUG: CALL SearchSummary('$tableID',true,'$searchParams',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists)</p>");

        $pdoObject = $pdo->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists");
        $rsArray = $pdoObject->fetchAll();        
        //print_r($rsArray);
        $JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
        $JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
        $JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];

        //store last search into session
        $_SESSION['checkout'] = $rsArray[0]['@JSON_SUMMARY'];
        //send a random number to the page as a last resort to be sure that the user has the same checkout that the session has
        $_SESSION['checkout_secure'] = rand(10000000,99999999);
        $smallerArray[0] = array('@JSON_SUMMARY' => $JSON_SUMMARY, '@JSON_OFFICIALS' => $JSON_OFFICIALS, '@scrvr' => $_SESSION['checkout_secure']);

        header("Location: /checkout.php");
        flush();


?>