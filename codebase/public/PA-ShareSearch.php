<?php

include("inc/config.php");
$title = 'Share A Search';

$User_FirstName = $_SESSION['user_firstname'];
$user_email = $_SESSION['user_email'];
$searchParams = $_SESSION['lastSearchParams'];
$User_Email = $_SESSION['user_email'];

if ($user_email == '') {
	// not loogged in or registered
	$user_email = 'unregistered_user@poweralmanac.com';
}
if (isset($_REQUEST['ssCode'])) {
	$ssCode = $_REQUEST['ssCode'];
	if ($ssCode == '') {
		// new
		$randval = mt_rand();
		$ssCode = "SS$randval";	
		$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
		$pdo->query(sprintf("CALL ShareCurrentSearch('%s',@RC,@SSID)", escape($searchParams)));
		$pdoObject = $pdo->query("SELECT @RC,@SSID");
		$rsArray = $pdoObject->fetchAll();
		//print_r($rsArray);
		$ssCode = $rsArray[0]['@SSID'];
	} else {
		// retry
		$ssCode = $_REQUEST['ssCode'];
	}
	//echo("<p>DEBUG 1. $ssCode</p>");
} else {
	// new
	$randval = mt_rand();
	$ssCode = "SS$randval";	
	$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
	$pdo->query(sprintf("CALL ShareCurrentSearch('%s',@RC,@SSID)", escape($searchParams)));
	$pdoObject = $pdo->query("SELECT @RC,@SSID");
	$rsArray = $pdoObject->fetchAll();
	//print_r($rsArray);
	$ssCode = $rsArray[0]['@SSID'];
	//echo("<p>DEBUG 2. $ssCode</p>");
}


$sharedFullURL = "http://www.poweralmanac.com/PA-RunSearch4shared.php?id=$ssCode";

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>
<SCRIPT LANGUAGE="JavaScript">
function validatShareForm()
{
	var x=document.forms["shareasearch"]["sender_Name"].value
	if (x==null || x=="")
	  {
	  alert("You must enter your FIRST NAME.");
	  return false;
	  }
	var y=document.forms["shareasearch"]["sender_Email"].value
	if (y==null || y=="")
	  {
	  alert("You must provide your EMAIL ADDRESS.");
	  return false;
	  }
	/*
	var a=document.forms["shareasearch"]["recipient_Name"].value
	if (a==null || a=="")
	  {
	  alert("You must enter the RECIPIENT NAME.");
	  return false;
	  }
	*/
	var b=document.forms["shareasearch"]["recipient_Email"].value
	if (b==null || b=="")
	  {
	  alert("You must provide the RECIPIENT EMAIL ADDRESS.");
	  return false;
	  }
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
<table cellpadding="5" cellspacing="5" border="0" align="center" width="95%">
                <tr>
					<td>
                    <h3>Share Your Search</h3>
                    <p>
                    <b>Email this search to a friend</b>
                    <br />
                    <form action="PA-process-shareasearch.php" method="post" name="shareasearch" onSubmit="return validatShareForm()">
                    <table cellpadding="2" cellspacing="3" border="0" width="600" style="border: 1px solid #000000">
                    <input type="hidden" name="ssCode" value="<?php echo($ssCode); ?>">
                    <tr>
                        <td>
                        <b>Your Name: </b>
                        </td>
                        <td>
                        <input name="sender_Name" type="text" value="<?php echo($User_FirstName); ?>" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;' size="50">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <b>Your Email: </b>
                        </td>
                        <td>
                        <input name="sender_Email" type="text" value="<?php echo($User_Email); ?>" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;' size="50">
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td>
                        <b>Recipient Name: </b>
                        </td>
                        <td>
                        <input name="recipient_Name" type="text" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;' size="50">
                        </td>
                    </tr>
                    -->
                    <tr>
                        <td>
                        <b>Recipient Email: </b>
                        </td>
                        <td>
                        <input name="recipient_Email" type="text" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;' size="50">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                        <b>Message or Note: </b>
                        </td>
                        <td>
                        <textarea cols="70" rows="20" name="message" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: normal;'>
Hi,

I thought you'd want to see this Power Almanac search of local government officials.
 
<?php echo($sharedFullURL); ?>


Regards,
<?php echo($User_FirstName); ?>
                        </textarea>
						<?php
						if ($_SESSION['logged_in'] != '1') {
							require_once('recaptchalib.php');
							echo recaptcha_get_html($recaptcha_publickey);
						}
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                        <input type="submit" name="submit" value="Share It!" style='font-family:  Arial, Helvetica, sans-serif;font-size: 10pt;color: #000000;font-weight: bold;'>
                        </td>
                    </tr>
                    </table>
                    </form>
                    </p>
                    </td>
                </tr>
                </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
            //include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
