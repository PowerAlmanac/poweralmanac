<?php

include("inc/config.php");

?>
<html>
<head>
<title>Analyze Instructions</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="popuphelp.js"></script>
<script type="text/javascript">
function tb_remove() {
 	$("#TB_imageOff").unbind("click");
	$("#TB_closeWindowButton").unbind("click");
	$("#TB_window").fadeOut("fast",function(){$('#TB_window,#TB_overlay,#TB_HideSelect').trigger("unload").unbind().remove();});
	$("#TB_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		$("body","html").css({height: "auto", width: "auto"});
		$("html").css("overflow","");
	}
	document.onkeydown = "";
	document.onkeyup = "";
	return false;
}
</script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
</head>
</head>

<body bgcolor="#FFFFFF">

<table width="675" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF">
<tr>
	<td>
	<a href="#" onClick="window.parent.Shadowbox.close()"><img src="images/popup-instructions2.jpg" width="650" height="224" hspace="5" vspace="5" border="0" align="absmiddle"></a>
	</td>
</tr>
</table>

</body>
</html>
