<?php
include("inc/config.php");
$title = 'Create Your Password | Power Almanac';
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder center login">
                    <h3>Create your password to access your account:</a>
                    <?php
	                    if (isset($_SESSION['subscribe_message'])) {
	                       echo '<p class="error-warning">' . $_SESSION['subscribe_message'] . '</p>';
	                      
	                    } else {
	                        $_SESSION['subscribe_message'] = '';
	                    }
                    ?>
                     <div class="black-holder clearfix">
	                	<form action="process/sub-pass.php" method="post" name="applyform">
	                	<div class="divider">
		                	<label class="halfwidth">
		                		New Password
		                		<input type="password" name="passWord" required>
		                	</label>
		                	<label class="halfwidth">
		                		Confirm New Password
		                		<input type="password" name="passWord2" required>
		                	</label>
		                </div>
	                	<div id="login-or-register" style="text-align: right">
	                		<input type="submit" value="Set Password" style="float: none" >
	                	</div>
	                	</form>
	                </div>
                </div>
            </div> 

<?php include("inc/footer.php"); ?>