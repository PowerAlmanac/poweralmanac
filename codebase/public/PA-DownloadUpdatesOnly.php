<?php

include("inc/config.php");
include("inc/protect-register.php");

$record_id = $_REQUEST['id'];

$user_email = $_SESSION['user_email'];
$RegUser_ID = $_SESSION['RegUser_ID'];

// Sanitize
$record_id = Sanitize::number($record_id);

/*
/var/www/sh/DownloadUpdates.sh "5" "bbbbb" "bbbbb"  -- "reg_id" "search name" "file name"   --> put the SAME file name and everything the SAME like SaveCurrentDownload.
u_bbbbb.zip is created
*/

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
// get file name and parse for user id and DL name
$pdo->query(sprintf("CALL GetFileName(%s,@filename)", escape($record_id))); // IN Saved_ID BIGINT, OUT FileName VARCHAR(255)
$pdoObject = $pdo->query("SELECT @filename");
$rsArray = $pdoObject->fetchAll();
$filename = $rsArray[0]['@filename'];
//echo("<p>DEBUG: filename = $filename</p>");
// strip extension
$savedname = str_replace(".zip","",$filename);
//echo("<p>DEBUG: RegUser_ID = $RegUser_ID, savedname = $savedname</p>");

$output = shell_exec("/bin/bash /var/www/sh/DownloadUpdates.sh \"$RegUser_ID\" \"$savedname\" \"$savedname\" ");

//echo("output = $output"); exit;

if ($output == 0) {
	// download generated successfully
	header("Location: /dl/u_$savedname.zip");
	flush();
}

?>