<?php   

$chartCityName = ucwords(strtolower($_REQUEST['city']));
$chartStateName = $_REQUEST['state'];
// gender
$MalePercent = $_REQUEST['g_m'];
$FemalePercent = $_REQUEST['g_f'];
// racial
$WhitePercent = $_REQUEST['r_w'];
$BlackPercent = $_REQUEST['r_b'];
//$HispanicPercent = $_REQUEST['r_h'];
$AsianPercent = $_REQUEST['r_a'];
$OtherPercent = $_REQUEST['r_o'];
// employment
//$WhiteCollarPercent = $_REQUEST['e_w'];
//$BlueCollarPercent = $_REQUEST['e_b'];

// education
$EducationNoHighSchool = $_REQUEST['ed_nhs'];
$EducationHighSchool = $_REQUEST['ed_hs'];
$EducationCollege = $_REQUEST['ed_c'];

// age
$AgeUnder20 = $_REQUEST['a_u20'];
$AgeOver70 = $_REQUEST['a_o70'];
$Age20to69 = $_REQUEST['a_other'];

// test
/*
$chartCityName = 'San Bernadino';
$chartStateName = 'CA';
$MalePercent = '52.24';
$FemalePercent = '47.76';
$WhitePercent = '44.27';
$BlackPercent = '5.02';
$HispanicPercent = '32.74';
$AsianPercent = '3.99';
$OtherPercent = '13.98';
$WhiteCollarPercent = '76.31';
$BlueCollarPercent = '23.69';
*/

$chartTitle = "Demographics Data for $chartCityName, $chartStateName";
$chartCopyright = 'Copyright© 2011 Power Almanac';
$chartWidth = 525;
$chartHeight = 350;
$chartBorderWidth = $chartWidth - 1;
$chartBorderHeight = $chartHeight - 1;

// fill data
$chartGender_LabelSeries = explode(",","Male,Female");
$chartGender_DataSeries = explode(",","$MalePercent,$FemalePercent");
$chartRace_LabelSeries = explode(",","White,Black,Asian,Other");
$chartRace_DataSeries = explode(",","$WhitePercent,$BlackPercent,$AsianPercent,$OtherPercent");
//$chartEmp_LabelSeries = explode(",","White Collar,Blue Collar");
//$chartEmp_DataSeries = explode(",","$WhiteCollarPercent,$BlueCollarPercent");
$chartEd_LabelSeries = explode(",","No High School Degree,High School Degree,College Degree");
$chartEd_DataSeries = explode(",","$EducationNoHighSchool,$EducationHighSchool,$EducationCollege");
$chartAge_LabelSeries = explode(",","Under 20,20 to 69,70+");
$chartAge_DataSeries = explode(",","$AgeUnder20,$Age20to69,$AgeOver70");
 /* CAT:Pie charts */

 /* pChart library inclusions */
 include("class/pData.class.php");
 include("class/pDraw.class.php");
 include("class/pPie.class.php");
 include("class/pImage.class.php");

 /* Create and populate the pData object */
 $MyData = new pData();   
 $MyData->addPoints($chartGender_DataSeries,"ScoreA");  
 $MyData->setSerieDescription("ScoreA","Application A");
 $MyData->addPoints($chartGender_LabelSeries,"Labels");
 $MyData->setAbscissa("Labels");

 $MyData2 = new pData();   
 $MyData2->addPoints($chartRace_DataSeries,"ScoreA");  
 $MyData2->setSerieDescription("ScoreA","Application A");
 $MyData2->addPoints($chartRace_LabelSeries,"Labels");
 $MyData2->setAbscissa("Labels");
 
 $MyData3 = new pData();   
 $MyData3->addPoints($chartEd_DataSeries,"ScoreA");  
 $MyData3->setSerieDescription("ScoreA","Application A");
 $MyData3->addPoints($chartEd_LabelSeries,"Labels");
 $MyData3->setAbscissa("Labels");
 
 $MyData4 = new pData();   
 $MyData4->addPoints($chartAge_DataSeries,"ScoreA");  
 $MyData4->setSerieDescription("ScoreA","Application A");
 $MyData4->addPoints($chartAge_LabelSeries,"Labels");
 $MyData4->setAbscissa("Labels");
 
 /* Create the pChart object */
 $myPicture = new pImage($chartWidth,$chartHeight,$MyData,TRUE);

 /* Draw a solid background */
 $Settings = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107); 
 $myPicture->drawFilledRectangle(0,0,$chartWidth,$chartHeight,$Settings);

 /* Draw a gradient overlay */
 $Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50);
 $myPicture->drawGradientArea(0,0,$chartWidth,$chartHeight,DIRECTION_VERTICAL,$Settings);
 // black header
 //$myPicture->drawGradientArea(0,0,$chartWidth,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,$chartBorderWidth,$chartBorderHeight,array("R"=>0,"G"=>0,"B"=>0));

 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>10));
 //$myPicture->drawText(10,18,"$chartTitle",array("R"=>255,"G"=>255,"B"=>255));
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>8));
 //$myPicture->drawText(335,345,"$chartCopyright",array("R"=>255,"G"=>255,"B"=>255));
 

 /* Set the default font properties */ 
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>8,"R"=>80,"G"=>80,"B"=>80));

 // GENDER
 /* Create the pPie object */ 
 $PieChart = new pPie($myPicture,$MyData);
 /* Define the slice color */
 $PieChart->setSliceColor(0,array("R"=>143,"G"=>197,"B"=>0));
 $PieChart->setSliceColor(1,array("R"=>97,"G"=>77,"B"=>63));
 $PieChart->setSliceColor(2,array("R"=>97,"G"=>113,"B"=>63));
 /* Draw pie chart */ 
 $PieChart->draw3DPie(145,70,array("Radius"=>60,"WriteValues"=>TRUE,"DrawLabels"=>TRUE,"DataGapAngle"=>10,"DataGapRadius"=>6,"Border"=>TRUE));
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>9));
 $myPicture->drawText(125,120,"GENDER",array("R"=>255,"G"=>255,"B"=>255));

 // RACE
 /* Create the pPie object */ 
 $PieChart2 = new pPie($myPicture,$MyData2);
 /* Define the slice color */
 $PieChart2->setSliceColor(0,array("R"=>143,"G"=>197,"B"=>0));
 $PieChart2->setSliceColor(1,array("R"=>97,"G"=>77,"B"=>63));
 $PieChart2->setSliceColor(2,array("R"=>97,"G"=>113,"B"=>63));
 /* Draw pie chart */ 
 $PieChart2->draw3DPie(375,100,array("Radius"=>70,"WriteValues"=>TRUE,"DrawLabels"=>TRUE,"DataGapAngle"=>10,"DataGapRadius"=>6,"Border"=>TRUE));
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>9));
 $myPicture->drawText(360,155,"RACE",array("R"=>255,"G"=>255,"B"=>255));

 // EDUCATION
 /* Create the pPie object */ 
 $PieChart3 = new pPie($myPicture,$MyData3);
 /* Define the slice color */
 $PieChart3->setSliceColor(0,array("R"=>143,"G"=>197,"B"=>0));
 $PieChart3->setSliceColor(1,array("R"=>97,"G"=>77,"B"=>63));
 $PieChart3->setSliceColor(2,array("R"=>97,"G"=>113,"B"=>63));
 /* Draw pie chart */ 
 $PieChart3->draw3DPie(180,230,array("Radius"=>60,"WriteValues"=>TRUE,"DrawLabels"=>TRUE,"DataGapAngle"=>10,"DataGapRadius"=>6,"Border"=>TRUE));
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>9));
 $myPicture->drawText(150,280,"EDUCATION",array("R"=>255,"G"=>255,"B"=>255));

 // AGE
 /* Create the pPie object */ 
 $PieChart4 = new pPie($myPicture,$MyData4);
 /* Define the slice color */
 $PieChart4->setSliceColor(0,array("R"=>143,"G"=>197,"B"=>0));
 $PieChart4->setSliceColor(1,array("R"=>97,"G"=>77,"B"=>63));
 $PieChart4->setSliceColor(2,array("R"=>97,"G"=>113,"B"=>63));
 /* Draw pie chart */ 
 $PieChart4->draw3DPie(375,290,array("Radius"=>60,"WriteValues"=>TRUE,"DrawLabels"=>TRUE,"DataGapAngle"=>10,"DataGapRadius"=>6,"Border"=>TRUE));
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>9));
 $myPicture->drawText(360,335,"AGE",array("R"=>255,"G"=>255,"B"=>255));
 
 /* Write the legend */
 $myPicture->setFontProperties(array("FontName"=>"fonts/verdana.ttf","FontSize"=>7));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>20));

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput("pictures/example.draw3DPie.png");
?>