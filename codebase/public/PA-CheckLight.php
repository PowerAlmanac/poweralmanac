<?php

include("inc/config.php");

$tableID = escape($_REQUEST['id']);

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

// check status
$read_sql = sprintf("SELECT * FROM ready_table
	WHERE table_string = '%s'
	LIMIT 1
	", escape($tableID));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from government database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$light = $onerow['light'];

if ($light == '1') {
	header("Location: PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3");
	flush();
	exit;
}

?>
<html>
<head>
<title>Check Build Status</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="popuphelp.js"></script>
<script type="text/javascript">
function tb_remove() {
 	$("#TB_imageOff").unbind("click");
	$("#TB_closeWindowButton").unbind("click");
	$("#TB_window").fadeOut("fast",function(){$('#TB_window,#TB_overlay,#TB_HideSelect').trigger("unload").unbind().remove();});
	$("#TB_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		$("body","html").css({height: "auto", width: "auto"});
		$("html").css("overflow","");
	}
	document.onkeydown = "";
	document.onkeyup = "";
	return false;
}
</script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
</head>
</head>

<body bgcolor="#ACC6E1" background="images/ACC6E1-pixel.jpg">

<table width="600" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#ACC6E1">
<tr>
	<td colspan="3" valign="top" bgcolor="#ACC6E1">
    <br><font style="font-size:16px; font-weight:bold">Checking Build Status...</font> 
    <a href="#" onClick="window.parent.Shadowbox.close()"><img src="images/preview-close.jpg" alt="Close Preview" width="40" height="40" hspace="5" vspace="0" border="0" align="right"></a>
	</td>
</tr>
<tr>
	<td colspan="3">
	<font color="#FFFFFF">
    <?php
	if ($light != '1') {
		echo("<p>Your COMPARE SPENDING tables are NOT available yet. Try again in about 15 seconds. Why the delay? We're dynamically generating these tables to match your search results.</p>");
	} else {
		//echo("<p>The Spending tables are NOW available! <a href='PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3' target='_parent'>Click here to continue.</a></p>");
		// redirect to tab
		// javascript:window.top.document.location.href='PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3'
		header("Location: PA-Analyze.php?skipBuild=1&type=2&count=2&column=4&row=3");
		flush();
	}
	?>
    </font>
	
	</td>
</tr>
</table>


</body>
</html>
