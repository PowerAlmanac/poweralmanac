<?php

include("inc/config.php");
include("inc/protect-login.php");

$user_email = $_SESSION['user_email'];
$savedSearchID = $_REQUEST['id'];

// Sanitize
$savedSearchID = Sanitize::number($savedSearchID);


$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL SavedSearchLookup('%s',%s,@Params)", escape($user_email), escape($savedSearchID)));
$pdoObject = $pdo->query("SELECT @Params");
$rsArray = $pdoObject->fetchAll();
$searchParams = $rsArray[0]['@Params'];
$lastSearchName = 'TBD';
/*
$lookupParams = new PAUser_Account; 
$searchParams = $lookupParams->SavedSearchLookup($savedSearchID,$user_email);
$lastSearchName = $lookupParams->User_SavedSearchName;
*/

$_SESSION['lastSearchName'] = $lastSearchName;

$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;
$_SESSION['savedSearchName'] = $lastSearchName;
$RegUser_ID = $_SESSION['RegUser_ID'];

// update datadase
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$update_sql = sprintf("UPDATE SavedSearches SET DateTime_Rerun = now()
	WHERE RegUser_ID = '%s' AND SavedSer_ID = '%s'
", escape($RegUser_ID), escape($savedSearchID));
$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating SavedSearches: '. PowerAlmanac\PDb::error().'</p>');
}

$searchURL = "search-government.php";
header("Location: $searchURL");
flush();


?>