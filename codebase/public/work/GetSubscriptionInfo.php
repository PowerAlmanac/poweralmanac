<?php
error_reporting(E_ALL);

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');
		
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error()); 
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error()); 

echo("<h3>Get Subscription Routines</h3>");

echo("<br /><b>Calling GetSubscriptionInfo</b><br />"); // IN level NUMERIC(11,0),OUT JSON_String VARCHAR(9999)
$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo3->query("CALL GetSubscriptionInfo(3,@JSON_String)"); 
$pdoObject = $pdo3->query("SELECT @JSON_String");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@JSON_String'];
//print_r($json); exit;
$jsonArray = json_decode($json, TRUE);
//print_r($jsonArray); 
$Sub_Name = $jsonArray['Sub_Name'];
$Sub_CostPerDL = $jsonArray['Sub_CostPerDL'];
echo("<p>$Sub_Name,$Sub_CostPerDL</p>");

echo("<br /><b>Calling GetSubscriptionInfo2</b><br />"); // IN SKU VARCHAR(25),OUT JSON_String VARCHAR(9999)
$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo3->query("CALL GetSubscriptionInfo2('PASUB_UNLIMITED2012',@JSON_String)"); 
$pdoObject = $pdo3->query("SELECT @JSON_String");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@JSON_String'];
//print_r($json); exit;
$jsonArray = json_decode($json, TRUE);
print_r($jsonArray); 
$Sub_Name = $jsonArray['Sub_Name'];
$Sub_CostPerDL = $jsonArray['Sub_CostPerDL'];
$Sub_NumReserves = $jsonArray['Sub_NumReserves'];
echo("<p>$Sub_Name,$Sub_CostPerDL,$Sub_NumReserves</p>");

echo("<br /><b>Calling GetSubscriptionInfo</b><br />"); // IN level NUMERIC(11,0),OUT JSON_String VARCHAR(9999)
$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo3->query("CALL GetSubscriptionInfo(4,@JSON_String)"); 
$pdoObject = $pdo3->query("SELECT @JSON_String");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@JSON_String'];
//print_r($json); exit;
$jsonArray = json_decode($json, TRUE);
//print_r($jsonArray); 
$Sub_Name = $jsonArray['Sub_Name'];
$Sub_CostPerDL = $jsonArray['Sub_CostPerDL'];
echo("<p>$Sub_Name,$Sub_CostPerDL</p>");

echo("<br /><b>Calling GetSubscriptionInfo2</b><br />"); // IN SKU VARCHAR(25),OUT JSON_String VARCHAR(9999)
$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo3->query("CALL GetSubscriptionInfo2('PASUB_POWER3A',@JSON_String)"); 
$pdoObject = $pdo3->query("SELECT @JSON_String");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@JSON_String'];
//print_r($json); exit;
$jsonArray = json_decode($json, TRUE);
//print_r($jsonArray); 
$Sub_Name = $jsonArray['Sub_Name'];
$Sub_CostPerDL = $jsonArray['Sub_CostPerDL'];
$Sub_NumReserves = $jsonArray['Sub_NumReserves'];
echo("<p>$Sub_Name,$Sub_CostPerDL,$Sub_NumReserves</p>");

exit;

?>