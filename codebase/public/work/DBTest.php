<?php
error_reporting(E_ALL);

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');

$govtID = '01101701700000';

if ($govtID == '') {
	$govtID = '01101701700000';
}

// DEBUG
//$govtID = '05103503500000';

// get data
$pdo2 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$read_sql = "SELECT * FROM government
	ORDER BY Government_ID
	";

$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from government database: 1. $read_sql" . PowerAlmanac\PDb::error());
}
$numResults = PowerAlmanac\PDb::num_rows($result_read);

echo("Processing $numResults results...<br>");
$ctr = 0;
while ($row = PowerAlmanac\PDb::fetch_array($result_read))
{
	$govtID = $row['Government_ID'];
    $pdo2->query(sprintf("CALL RecordDetails('%s',@JSON_GOVERNMENT, @JSON_OFFICIALS)", escape($govtID)));
    $pdoObject2 = $pdo2->query("SELECT @JSON_GOVERNMENT, @JSON_OFFICIALS");
    $rsArray2 = $pdoObject2->fetchAll();
	
	$json2a = $rsArray2[0]['@JSON_GOVERNMENT'];
	//print_r($json2a);
	
	$jsonGovernmentArray = json_decode($json2a, TRUE);
	switch(json_last_error())
	{
		case JSON_ERROR_DEPTH:
			echo ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_CTRL_CHAR:
			echo ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			echo ' - Syntax error, malformed JSON_GOVERNMENT for ' . $govtID;
		break;
		case JSON_ERROR_NONE:
			//echo ' - No errors';
		break;
	}
	
	//print_r($jsonGovernmentArray);
	
	$json2b = $rsArray2[0]['@JSON_OFFICIALS'];
	//echo("<p>json2b</p>");
	//print_r($json2b);
	
	$jsonOfficialsArray = json_decode($json2b, TRUE);
	switch(json_last_error())
	{
		case JSON_ERROR_DEPTH:
			echo ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_CTRL_CHAR:
			echo ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			echo ' - Syntax error, malformed JSON_OFFICIALS for ' . $govtID;
		break;
		case JSON_ERROR_NONE:
			//echo ' - No errors';
		break;
	}
	$ctr = $ctr + 1;
	if ($ctr % 1000 == 0) {
		echo("Processed $ctr records...<br>");
	}
	//echo("<p>jsonOfficialsArray</p>");
	//print_r($jsonOfficialsArray);

}
						
exit;

?>