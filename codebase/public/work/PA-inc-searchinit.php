<?php

// initialize results COUNT summary arrays

$statesArray = array (
	"AL" => 0,
	"AK" => 0,
	"AZ" => 0,
	"AR" => 0,
	"CA" => 0,
	"CO" => 0,
	"CT" => 0,
	"DE" => 0,
	"DC" => 0,
	"FL" => 0,
	"GA" => 0,
	"HI" => 0,
	"ID" => 0,
	"IL" => 0,
	"IN" => 0,
	"IA" => 0,
	"KS" => 0,
	"KY" => 0,
	"LA" => 0,
	"ME" => 0,
	"MD" => 0,
	"MA" => 0,
	"MI" => 0,
	"MN" => 0,
	"MS" => 0,
	"MO" => 0,
	"MT" => 0,
	"NE" => 0,
	"NV" => 0,
	"NH" => 0,
	"NJ" => 0,
	"NM" => 0,
	"NY" => 0,
	"NC" => 0,
	"ND" => 0,
	"OH" => 0,
	"OK" => 0,
	"OR" => 0,
	"PA" => 0,
	"RI" => 0,
	"SC" => 0,
	"SD" => 0,
	"TN" => 0,
	"TX" => 0,
	"UT" => 0,
	"VT" => 0,
	"VA" => 0,
	"WA" => 0,
	"WV" => 0,
	"WI" => 0,
	"WY" => 0
);

$regionsArray = array (
  "Northeast" => 0,
  "South" => 0,
  "Midwest" => 0,
  "West" => 0
);

$govtTypesArray = array (
  "county" => 0,
  "township" => 0,
  "municipality" => 0
);

$govtRolesArray = array (
  "Top Elected Official" => 0,
  "Top Appointed Official" => 0,
  "Governing Board Member" => 0,
  "Head Of Finance/Budgeting" => 0,
  "Head of Purchasing/Procurement" => 0,
  "Head of Public Works" => 0,
  "Head of Law Enforcement" => 0,
  "Head of Fire Protection Services" => 0,
  "Clerk" => 0
);

/*
$populationArray = array (
  "Population0-999" => 0,
  "Population1000-2499" => 0,
  "Population2500-4999" => 0,
  "Population5000-9999" => 0,
  "Population10000-24999" => 0,
  "Population25000-49999" => 0,
  "Population50000-99999" => 0,
  "Population100000-499999" => 0,
  "Population500000+" => 0
);
*/

$populationArray = array (
  "P0" => 0,
  "P1" => 0,
  "P2" => 0,
  "P3" => 0,
  "P4" => 0,
  "P5" => 0,
  "P6" => 0,
  "P7" => 0,
  "P8" => 0,
  "P9" => 0
);

$govtAgeArray = array (
  "GovAge0-49" => 0,
  "GovAge50-99" => 0,
  "GovAge100-149" => 0,
  "GovAge150-199" => 0,
  "GovAge200+" => 0,
  "GovAgeUnknown" => 0
);
	
$fiscalYearArray = array (
  "Fiscal_January" => 0,
  "Fiscal_February" => 0,
  "Fiscal_March" => 0,
  "Fiscal_April" => 0,
  "Fiscal_May" => 0,
  "Fiscal_June" => 0,
  "Fiscal_July" => 0,
  "Fiscal_August" => 0,
  "Fiscal_September" => 0,
  "Fiscal_October" => 0,
  "Fiscal_November" => 0,
  "Fiscal_December" => 0,
  "Fiscal_Not_Known" => 0
);

// not used?

$expTypeArray = array (
  "1" => 0,
  "2" => 0,
  "3" => 0,
  "4" => 0,
  "5" => 0
);

// catch all

$unmatchedArray = array (
  "DUMMY" => 0
);

// top level category counts

$expGovtFunctionArray = array (
  "Total Expenditure" => 0,
  "Special Categories" => 0,
  "Public Safety" => 0,
  "Public Welfare" => 0,
  "Health" => 0,
  "Utilities" => 0,
  "Transportation" => 0,
  "Leisure" => 0,
  "Finance" => 0,
  "Miscellaneous" => 0
);

// top level category counts

$demoFunctionArray = array (
  "Population" => 0,
  "Gender" => 0,
  "Race" => 0,
  "Age" => 0,
  "Education" => 0,
  "Income/Wealth" => 0,
  "Households" => 0,
  "Employment" => 0,
  "Commute" => 0,
  "Quality of Life" => 0
);
	
$govAgeArray = array (
  "GovAge0-49" => 0,
  "GovAge50-99" => 0,
  "GovAge100-149" => 0,
  "GovAge150-199" => 0,
  "GovAge200+" => 0,
  "GovAgeUnknown" => 0
);

// initialize search criteria used - default is USA, All Govt Types, All Govt Off Roles, All Population, All Ages
// parse $searchParams

$statesSelectArray = array (
  "Alabama" => '',
  "Alaska" => '',
  "Arizona" => '',
  "Arkansas" => '',
  "California" => '',
  "Colorado" => '',
  "Connecticut" => '',
  "Delaware" => '',
  "District+of+Columbia" => '',
  "Florida" => '',
  "Georgia" => '',
  "Hawaii" => '',
  "Idaho" => '',
  "Illinois" => '',
  "Indiana" => '',
  "Iowa" => '',
  "Kansas" => '',
  "Kentucky" => '',
  "Louisiana" => '',
  "Maine" => '',
  "Maryland" => '',
  "Massachusetts" => '',
  "Michigan" => '',
  "Minnesota" => '',
  "Mississippi" => '',
  "Missouri" => '',
  "Montana" => '',
  "Nebraska" => '',
  "Nevada" => '',
  "New+Hampshire" => '',
  "New+Jersey" => '',
  "New+Mexico" => '',
  "New+York" => '',
  "North+Carolina" => '',
  "North+Dakota" => '',
  "Ohio" => '',
  "Oklahoma" => '',
  "Oregon" => '',
  "Pennsylvania" => '',
  "Rhode+Island" => '',
  "South+Carolina" => '',
  "South+Dakota" => '',
  "Tennessee" => '',
  "Texas" => '',
  "Utah" => '',
  "Vermont" => '',
  "Virginia" => '',
  "Washington" => '',
  "West+Virginia" => '',
  "Wisconsin" => '',
  "Wyoming" => ''
);

$govtTypesSelectArray = array (
  "county" => '',
  "township" => '',
  "municipality" => ''
);

$regionsSelectArray = array (
  "Northeast" => '',
  "South" => '',
  "Midwest" => '',
  "West" => ''
);

$govtRolesSelectArray = array (
  "role_1" => '',
  "role_2" => '',
  "role_3" => '',
  "role_4" => '',
  "role_5" => '',
  "role_6" => '',
  "role_7" => '',
  "role_8" => '',
  "role_9" => ''
);

$populationSelectArray = array (
  "0" => '',
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => ''
);

$govtAgeSelectArray = array (
  "GovAge0-49" => '',
  "GovAge50-99" => '',
  "GovAge100-149" => '',
  "GovAge150-199" => '',
  "GovAge200+" => '',
  "GovAgeUnknown" => ''
);

$fiscalYearSelectArray = array (
  "January" => '',
  "February" => '',
  "March" => '',
  "April" => '',
  "May" => '',
  "June" => '',
  "July" => '',
  "August" => '',
  "September" => '',
  "October" => '',
  "November" => '',
  "December" => '',
  "ot+Known" => ''
);

// not used?

$expTypeSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => ''
);

// FINANCIALS

$finTotalSelectArray = array (
  "1" => ''
);

$finSpecialSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => ''
);

$finPSSelectArray = array (
  "Corrections" => '',
  "Fire Protection" => '',
  "Judicial and Legal" => '',
  "Police" => '',
  "All" => ''
);

$finPWSelectArray = array (
  "Assistance and Subsidies" => '',
  "Welfare Program Administration" => '',
  "All" => ''
);

$finHSelectArray = array (
  "Health Services" => '',
  "Hospitals" => '',
  "All" => ''
);

$finUSelectArray = array (
  "Electricity" => '',
  "Gas" => '',
  "Sewerage" => '',
  "Solid Waste Management" => '',
  "Water" => '',
  "All" => ''
);

$finTSelectArray = array (
  "Airports" => '',
  "Parking" => '',
  "Ports and Waterways" => '',
  "Public Transit" => '',
  "Roads and Highways" => '',
  "All" => ''
);

$finLSelectArray = array (
  "Libraries" => '',
  "Parks and Recreation" => '',
  "All" => ''
);

$finFSelectArray = array (
  "Financial Administration" => '',
  "Interest on Debt" => '',
  "All" => ''
);

$finMSelectArray = array (
  "Education" => '',
  "General Administration" => '',
  "Housing and Community Development" => '',
  "Liquor Stores" => '',
  "Natural Resources" => '',
  "Other Miscellaneous" => '',
  "All" => ''
);

// expense budget

$eBQSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => ''
);

// expense per capita

$pCBQSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => ''
);

$finCategorySelectArray = array (
  "byTotal" => '',
  "bySpecial" => '',
  "byPS" => '',
  "byPW" => '',
  "byH" => '',
  "byU" => '',
  "byT" => '',
  "byL" => '',
  "byF" => '',
  "byM" => ''
);

$finCategorySelectDisplayArray = array (
  "byTotal" => 'none',
  "bySpecial" => 'none',
  "byPS" => 'none',
  "byPW" => 'none',
  "byH" => 'none',
  "byU" => 'none',
  "byT" => 'none',
  "byL" => 'none',
  "byF" => 'none',
  "byM" => 'none'
);

// default search settings
if (!isset($_SESSION['checkbox_GovtTypes'])) {
	$_SESSION['checkbox_GovtTypes'] = '1';
	$checkbox_GovtTypes = '1';
} else {
	$checkbox_GovtTypes = $_SESSION['checkbox_GovtTypes'];
}
switch($checkbox_GovtTypes) {
	case '0':
		$checkbox_GovtTypesImg = 'images/uncheckedAll.png';
		break;
	case '1':
		$checkbox_GovtTypesImg = 'images/checkedAll.png';
		break;
	default:
		$checkbox_GovtTypesImg = 'images/uncheckedAll.png';
		break;
}

if (!isset($_SESSION['checkbox_Roles'])) {
	$_SESSION['checkbox_Roles'] = '0';
	$checkbox_Roles = '0';
} else {
	$checkbox_Roles = $_SESSION['checkbox_Roles'];
}
switch($checkbox_Roles) {
	case '0':
		$checkbox_RolesImg = 'images/uncheckedAll.png';
		break;
	case '1':
		$checkbox_RolesImg = 'images/checkedAll.png';
		break;
	default:
		$checkbox_RolesImg = 'images/uncheckedAll.png';
		break;
}

if (!isset($_SESSION['checkbox_Population'])) {
	$_SESSION['checkbox_Population'] = '1';
	$checkbox_Population = '1';
} else {
	$checkbox_Population = $_SESSION['checkbox_Population'];
}
switch($checkbox_Population) {
	case '0':
		$checkbox_PopulationImg = 'images/uncheckedAll.png';
		break;
	case '1':
		$checkbox_PopulationImg = 'images/checkedAll.png';
		break;
	default:
		$checkbox_PopulationImg = 'images/uncheckedAll.png';
		break;
}

if (!isset($_SESSION['checkbox_FiscalYear'])) {
	$_SESSION['checkbox_FiscalYear'] = '1';
	$checkbox_FiscalYear = '1';
} else {
	$checkbox_FiscalYear = $_SESSION['checkbox_FiscalYear'];
}
switch($checkbox_FiscalYear) {
	case '0':
		$checkbox_FiscalYearImg = 'images/uncheckedAll.png';
		break;
	case '1':
		$checkbox_FiscalYearImg = 'images/checkedAll.png';
		break;
	default:
		$checkbox_FiscalYearImg = 'images/uncheckedAll.png';
		break;
}


if (!isset($_SESSION['checkbox_States'])) {
	$_SESSION['checkbox_States'] = '1';
	$checkbox_States = '1';
} else {
	$checkbox_States = $_SESSION['checkbox_States'];
}
switch($checkbox_States) {
	case '0':
		$checkbox_StatesImg = 'images/uncheckedAll.png';
		break;
	case '1':
		$checkbox_StatesImg = 'images/checkedAll.png';
		break;
	default:
		$checkbox_StatesImg = 'images/uncheckedAll.png';
		break;
}

if (!isset($_SESSION['checkbox_Demographics'])) {
	$_SESSION['checkbox_Demographics'] = '1';
	$checkbox_Demographics = '1';
} else {
	$checkbox_Demographics = $_SESSION['checkbox_Demographics'];
}
switch($checkbox_Demographics) {
	case '0':
		$checkbox_DemographicsImg = 'images/uncheckedAll.png';
		break;
	case '1':
		$checkbox_DemographicsImg = 'images/checkedAll.png';
		break;
	default:
		$checkbox_DemographicsImg = 'images/uncheckedAll.png';
		break;
}
// DEMOGRAPHICS

// visibility settings

if (isset($_SESSION['demoGenderDisplay'])) {
	$demoGenderDisplay = $_SESSION['demoGenderDisplay'];
} else {
	$demoGenderDisplay = 'none';
}

if (isset($_SESSION['demoRaceDisplay'])) {
	$demoRaceDisplay = $_SESSION['demoRaceDisplay'];
} else {
	$demoRaceDisplay = 'none';
}

if (isset($_SESSION['demoAgeDisplay'])) {
	$demoAgeDisplay = $_SESSION['demoAgeDisplay'];
} else {
	$demoAgeDisplay = 'none';
}

if (isset($_SESSION['demoEducationDisplay'])) {
	$demoEducationDisplay = $_SESSION['demoEducationDisplay'];
} else {
	$demoEducationDisplay = 'none';
}

if (isset($_SESSION['demoIncomeDisplay'])) {
	$demoIncomeDisplay = $_SESSION['demoIncomeDisplay'];
} else {
	$demoIncomeDisplay = 'none';
}

if (isset($_SESSION['demoEmploymentDisplay'])) {
	$demoEmploymentDisplay = $_SESSION['demoEmploymentDisplay'];
} else {
	$demoEmploymentDisplay = 'none';
}

if (isset($_SESSION['demoHouseholdsDisplay'])) {
	$demoHouseholdsDisplay = $_SESSION['demoHouseholdsDisplay'];
} else {
	$demoHouseholdsDisplay = 'none';
}

if (isset($_SESSION['demoCommuteDisplay'])) {
	$demoCommuteDisplay = $_SESSION['demoCommuteDisplay'];
} else {
	$demoCommuteDisplay = 'none';
}

if (isset($_SESSION['demoQualityLifeDisplay'])) {
	$demoQualityLifeDisplay = $_SESSION['demoQualityLifeDisplay'];
} else {
	$demoQualityLifeDisplay = 'none';
}

if (isset($_SESSION['demoOtherPopulationDisplay'])) {
	$demoOtherPopulationDisplay = $_SESSION['demoOtherPopulationDisplay'];
} else {
	$demoOtherPopulationDisplay = 'none';
}

// for images
if (isset($_SESSION['demoGenderDisplayImg'])) {
	$demoGenderDisplayImg = $_SESSION['demoGenderDisplayImg'];
} else {
	$demoGenderDisplayImg = '';
}

if (isset($_SESSION['demoRaceDisplayImg'])) {
	$demoRaceDisplayImg = $_SESSION['demoRaceDisplayImg'];
} else {
	$demoRaceDisplayImg = '';
}

if (isset($_SESSION['demoAgeDisplayImg'])) {
	$demoAgeDisplayImg = $_SESSION['demoAgeDisplayImg'];
} else {
	$demoAgeDisplayImg = '';
}

if (isset($_SESSION['demoEducationDisplayImg'])) {
	$demoEducationDisplayImg = $_SESSION['demoEducationDisplayImg'];
} else {
	$demoEducationDisplayImg = '';
}

if (isset($_SESSION['demoIncomeDisplayImg'])) {
	$demoIncomeDisplayImg = $_SESSION['demoIncomeDisplayImg'];
} else {
	$demoIncomeDisplayImg = '';
}

if (isset($_SESSION['demoEmploymentDisplayImg'])) {
	$demoEmploymentDisplayImg = $_SESSION['demoEmploymentDisplayImg'];
} else {
	$demoEmploymentDisplayImg = '';
}

if (isset($_SESSION['demoHouseholdsDisplayImg'])) {
	$demoHouseholdsDisplayImg = $_SESSION['demoHouseholdsDisplayImg'];
} else {
	$demoHouseholdsDisplayImg = '';
}

if (isset($_SESSION['demoCommuteDisplayImg'])) {
	$demoCommuteDisplayImg = $_SESSION['demoCommuteDisplayImg'];
} else {
	$demoCommuteDisplayImg = '';
}

if (isset($_SESSION['demoQualityLifeDisplayImg'])) {
	$demoQualityLifeDisplayImg = $_SESSION['demoQualityLifeDisplayImg'];
} else {
	$demoQualityLifeDisplayImg = '';
}

if (isset($_SESSION['demoOtherPopulationDisplayImg'])) {
	$demoOtherPopulationDisplayImg = $_SESSION['demoOtherPopulationDisplayImg'];
} else {
	$demoOtherPopulationDisplayImg = '';
}

// checkbox settings
$demoPSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => ''
);

$demoGSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => ''
);

$demoRSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => '',
  "16" => '',
  "17" => '',
  "18" => '',
  "19" => '',
  "20" => '',
  "21" => '',
  "22" => '',
  "23" => '',
  "24" => '',
  "25" => ''
);

$demoASelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => ''
);

$demoEdSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => ''
);

$demoISelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => '',
  "16" => '',
  "17" => '',
  "18" => '',
  "19" => '',
  "20" => '',
  "21" => '',
  "22" => '',
  "23" => '',
  "24" => '',
  "25" => ''
);

$demoHSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => '',
  "16" => '',
  "17" => '',
  "18" => '',
  "19" => '',
  "20" => ''
);

$demoEmSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => '',
  "16" => '',
  "17" => '',
  "18" => '',
  "19" => '',
  "20" => ''
);

$demoCSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => ''
);

$demoQSelectArray = array (
  "1" => '',
  "2" => '',
  "3" => '',
  "4" => '',
  "5" => '',
  "6" => '',
  "7" => '',
  "8" => '',
  "9" => '',
  "10" => '',
  "11" => '',
  "12" => '',
  "13" => '',
  "14" => '',
  "15" => '',
  "16" => '',
  "17" => '',
  "18" => '',
  "19" => '',
  "20" => ''
);

$locationSelectArray = array (
  "locationByNational" => '',
  "locationByStates" => '',
  "locationByZipCode" => ''
);

$locationSelectDisplayArray = array (
  "locationByNational" => 'none',
  "locationByRegions" => 'none',
  "locationByStates" => 'none',
  "locationByZipCode" => 'none'
);

$governmentSpendingSelectArray = array (
  "gsTotal" => '',
  "gsPS" => '',
  "gsPW" => '',
  "gsH" => '',
  "gsU" => '',
  "gsT" => '',
  "gsL" => '',
  "gsF" => '',
  "gsM" => '',
  "gsSC" => ''
);

$governmentSpendingSelectDisplayArray = array (
  "gsTotal" => 'none',
  "gsPS" => 'none',
  "gsPW" => 'none',
  "gsH" => 'none',
  "gsU" => 'none',
  "gsT" => 'none',
  "gsL" => 'none',
  "gsF" => 'none',
  "gsM" => 'none',
  "gsSC" => 'none'
);

$governmentSpendingScreenSelectArray = array (
  "DefinedRange" => '',
  "SpendingBuckets" => '',
  "PerCapitaSpending" => ''
);

$governmentSpendingScreenSelectDisplayArray = array (
  "DefinedRange" => 'none',
  "SpendingBuckets" => 'none',
  "PerCapitaSpending" => 'none'
);

$locationZipCode = '';
$locationZipCodeDistance = '';

$statesDefaultFlag = 0;
$regionsDefaultFlag = 0;
$govtTypesDefaultFlag = 0;
$govtRolesDefaultFlag = 0;
$populationDefaultFlag = 0;
$govtAgeDefaultFlag = 0;
$fiscalYearDefaultFlag = 0;
$expTypeDefaultFlag = 0;

$finTotalDefaultFlag = 0;
$finSpecialDefaultFlag = 0;
$finPSDefaultFlag = 0;
$finPWDefaultFlag = 0;
$finHDefaultFlag = 0;
$finUDefaultFlag = 0;
$finTDefaultFlag = 0;
$finLDefaultFlag = 0;
$finFDefaultFlag = 0;
$finMDefaultFlag = 0;
$eBQDefaultFlag = 0;
$pCQDefaultFlag = 0;

$demoPDefaultFlag = 0;
$demoGDefaultFlag = 0;
$demoRDefaultFlag = 0;
$demoADefaultFlag = 0;
$demoEdDefaultFlag = 0;
$demoIDefaultFlag = 0;
$demoHDefaultFlag = 0;
$demoEmDefaultFlag = 0;
$demoCDefaultFlag = 0;
$demoQDefaultFlag = 0;
$demoOPDefaultFlag = 0;

$expenseBudgetMoreThan = '';
$expenseBudgetLessThan = '';
$populationMoreThan = '';
$populationLessThan = '';

// from latest search

$paramsArray = explode("&", $searchParams);
//print_r($paramsArray);
foreach ($paramsArray as $item) {
	if ($item != "") {
		list($key,$value) = explode("=",$item);
		//echo("<p>$key = $value</p>");
		switch($key) {
			// basic search
			case 'selLocation':
				$locationSelectArray["$value"] = 'checked="checked"';
				$locationSelectDisplayArray["$value"] = 'block';
				break;
			case 'govLoc_byState%5B%5D':
				$statesSelectArray["$value"] = ' checked';
				$statesDefaultFlag = 1;
				$regionsDefaultFlag = 1;
				break;
			case 'govLoc_byRegion%5B%5D':
				$regionsSelectArray["$value"] = ' checked';
				$regionsDefaultFlag = 1;
				$statesDefaultFlag = 1;
				break;
			case 'byZipcode':
				$locationZipCode = "$value";
				break;
			case 'byZipcodeDistance':
				$locationZipCodeDistance = "$value";
				break;
			case 'govType_municipalities':
			case 'govType_townships':
			case 'govType_counties':
				$govtTypesSelectArray["$value"] = ' checked';
				$govtTypesDefaultFlag = 1;
				break;
			case 'population%5B%5D':
				$populationSelectArray["$value"] = ' checked';
				$populationDefaultFlag = 1;
				break;
			case 'role_1':
			case 'role_2':
			case 'role_3':
			case 'role_4':
			case 'role_5':
			case 'role_6':
			case 'role_7':
			case 'role_8':
			case 'role_9':
				$govtRolesSelectArray["$key"] = ' checked';
				$govtRolesDefaultFlag = 1;
				break;
			// age to be added later
			
			// power search
			
			// government spending
			case 'finPS%5B%5D':
				$index = urldecode($value);
				$finPSSelectArray["$index"] = 'checked="checked"';
				$finPSDefaultFlag = 1;
				break;
			case 'finPW%5B%5D':
				$index = urldecode($value);
				$finPWSelectArray["$index"] = 'checked="checked"';
				$finPWDefaultFlag = 1;
				break;
			case 'finH%5B%5D':
				$index = urldecode($value);
				$finHSelectArray["$index"] = 'checked="checked"';
				$finHDefaultFlag = 1;
				break;
			case 'finU%5B%5D':
				$index = urldecode($value);
				$finUSelectArray["$index"] = 'checked="checked"';
				$finUDefaultFlag = 1;
			case 'finT%5B%5D':
				$index = urldecode($value);
				$finTSelectArray["$index"] = 'checked="checked"';
				$finTDefaultFlag = 1;
				break;
				break;
			case 'finL%5B%5D':
				$index = urldecode($value);
				$finLSelectArray["$index"] = 'checked="checked"';
				$finLDefaultFlag = 1;
				break;
			case 'finF%5B%5D':
				$index = urldecode($value);
				$finFSelectArray["$index"] = 'checked="checked"';
				$finFDefaultFlag = 1;
				break;
			case 'finM%5B%5D':
				$index = urldecode($value);
				$finMSelectArray["$index"] = 'checked="checked"';
				$finMDefaultFlag = 1;
				break;
			case 'expType%5B%5D':
				$index = urldecode($value);
				$expTypeSelectArray["$index"] = 'checked="checked"';
				$expTypeDefaultFlag = 1;
				break;
			case 'criteria':
				$criteria = "$value";
				switch($criteria) {
					case 'expenseBudget':
						$selGovtExpScreen = 'DefinedRange';
						break;
					case 'expenseBudgetQuartile':
						$selGovtExpScreen = 'SpendingBuckets';
						break;
					case 'perCapitalBudgetQuartile':
						$selGovtExpScreen = 'PerCapitaSpending';
						break;
					default:
						break;
				}
				break;
			case 'selGovtExpScreen':
				$selGovtExpScreen = "$value";
				break;
			case 'expenseBudgetMoreThan':
				$expenseBudgetMoreThan = "$value";
				break;
			case 'expenseBudgetLessThan':
				$expenseBudgetLessThan = "$value";
				break;
			case 'expenseBudgetQuartileSel%5B%5D':
				//echo("selGovtExpScreen = $selGovtExpScreen<br>");
				if ($selGovtExpScreen == 'SpendingBuckets') {
					$index = urldecode($value);
					$eBQSelectArray["$index"] = 'checked="checked"';
					$eBQDefaultFlag = 1;
				}
				break;
			case 'perCapitalBudgetQuartileSel%5B%5D':
				if ($selGovtExpScreen == 'PerCapitaSpending') {
					$index = urldecode($value);
					$pCBQSelectArray["$index"] = 'checked="checked"';
					$pCQDefaultFlag = 1;
				}
				break;

			// demographics
			case 'populationMoreThan':
				$populationMoreThan = "$value";
				break;
			case 'populationLessThan':
				$populationLessThan = "$value";
				break;
			case 'demoGQ%5B%5D':
				$index = urldecode($value);
				$demoGSelectArray["$index"] = 'checked="checked"';
				$demoGDefaultFlag = 1;
				break;
			case 'demoRQ%5B%5D':
				$index = urldecode($value);
				$demoRSelectArray["$index"] = 'checked="checked"';
				$demoRDefaultFlag = 1;
				break;	
			case 'demoAQ%5B%5D':
				$index = urldecode($value);
				$demoASelectArray["$index"] = 'checked="checked"';
				$demoADefaultFlag = 1;
				break;
			case 'demoEdQ%5B%5D':
				$index = urldecode($value);
				$demoEdSelectArray["$index"] = 'checked="checked"';
				$demoEdDefaultFlag = 1;
				break;
			case 'demoIQ%5B%5D':
				$index = urldecode($value);
				$demoISelectArray["$index"] = 'checked="checked"';
				$demoIDefaultFlag = 1;
				break;
			case 'demoEmQ%5B%5D':
				$index = urldecode($value);
				$demoEmSelectArray["$index"] = 'checked="checked"';
				$demoEmDefaultFlag = 1;
				break;
			case 'demoHQ%5B%5D':
				$index = urldecode($value);
				$demoHSelectArray["$index"] = 'checked="checked"';
				$demoHDefaultFlag = 1;
				break;
			case 'demoCQ%5B%5D':
				$index = urldecode($value);
				$demoCSelectArray["$index"] = 'checked="checked"';
				$demoCDefaultFlag = 1;
				break;
			case 'demoQQ%5B%5D':
				$index = urldecode($value);
				$demoQSelectArray["$index"] = 'checked="checked"';
				$demoQDefaultFlag = 1;
				break;
			case 'demoPQ%5B%5D':
				$index = urldecode($value);
				$demoPSelectArray["$index"] = 'checked="checked"';
				$demoPDefaultFlag = 1;
				break;
			//
			case 'fiscalYearEnd_Month%5B%5D':
				$fiscalYearSelectArray["$value"] = ' checked';
				$fiscalYearDefaultFlag = 1;
				break;
			
			default:
				break;
		}
	}
}


if (isset($selGovtExp)) {
	$governmentSpendingSelectArray["$selGovtExp"] = 'checked="checked"';
	$governmentSpendingSelectDisplayArray["$selGovtExp"] = 'block';
}

if (isset($selGovtExpScreen)) {
	$governmentSpendingScreenSelectArray["$selGovtExpScreen"] = 'checked="checked"';
	$governmentSpendingScreenSelectDisplayArray["$selGovtExpScreen"] = 'block';
}

// check if default selected all to be checked?

if ($statesDefaultFlag == 0) {
	foreach ($statesSelectArray as $k => $v) 
	{
    	$statesSelectArray[$k] = ' checked';
	}
}

if ($regionsDefaultFlag == 0) {
	foreach ($regionsSelectArray as $k => $v) 
	{
    	$regionsSelectArray[$k] = ' checked';
	}
}

// default unchecked
if ($govtTypesDefaultFlag == 0) {
	foreach ($govtTypesSelectArray as $k => $v) 
	{
    	$govtTypesSelectArray[$k] = ' checked';
		//$govtTypesSelectArray[$k] = '';
	}
}

// default unchecked
if ($govtRolesDefaultFlag == 0) {
	foreach ($govtRolesSelectArray as $k => $v) 
	{
    	//$govtRolesSelectArray[$k] = ' checked';
		$govtRolesSelectArray[$k] = '';
	}
}

// default unchecked
if ($populationDefaultFlag == 0) {
	foreach ($populationSelectArray as $k => $v) 
	{
    	$populationSelectArray[$k] = ' checked';
		//$populationSelectArray[$k] = '';
	}
}

if ($fiscalYearDefaultFlag == 0) {
	foreach ($fiscalYearSelectArray as $k => $v) 
	{
    	$fiscalYearSelectArray[$k] = ' checked';
	}
}

if ($expTypeDefaultFlag == 0) {
	foreach ($expTypeSelectArray as $k => $v) 
	{
    	$expTypeSelectArray[$k] = ' selected';
	}
}

if ($finPSDefaultFlag == 0) {
	foreach ($finPSSelectArray as $k => $v) 
	{
    	$finPSSelectArray[$k] = ' selected';
	}
}

if ($finPWDefaultFlag == 0) {
	foreach ($finPWSelectArray as $k => $v) 
	{
    	$finPWSelectArray[$k] = ' selected';
	}
}

if ($finHDefaultFlag == 0) {
	foreach ($finHSelectArray as $k => $v) 
	{
    	$finHSelectArray[$k] = ' selected';
	}
}

if ($finUDefaultFlag == 0) {
	foreach ($finUSelectArray as $k => $v) 
	{
    	$finUSelectArray[$k] = ' selected';
	}
}

if ($finTDefaultFlag == 0) {
	foreach ($finTSelectArray as $k => $v) 
	{
    	$finTSelectArray[$k] = ' selected';
	}
}

if ($finLDefaultFlag == 0) {
	foreach ($finLSelectArray as $k => $v) 
	{
    	$finLSelectArray[$k] = ' selected';
	}
}

if ($finFDefaultFlag == 0) {
	foreach ($finFSelectArray as $k => $v) 
	{
    	$finFSelectArray[$k] = ' selected';
	}
}

if ($finMDefaultFlag == 0) {
	foreach ($finMSelectArray as $k => $v) 
	{
    	$finMSelectArray[$k] = ' selected';
	}
}

// 6/14/2011 defautt checked but don't display
if ($eBQDefaultFlag == 0) {
	foreach ($eBQSelectArray as $k => $v) 
	{
    	//$eBQSelectArray[$k] = ' checked';
	}
}

// 6/14/2011 defautt checked but don't display
if ($pCQDefaultFlag == 0) {
	foreach ($pCBQSelectArray as $k => $v) 
	{
    	//$pCBQSelectArray[$k] = ' checked';
	}
}

/*
if ($finSpecialDefaultFlag == 0) {
	foreach ($expTypeSelectArray as $k => $v) 
	{
    	$expTypeSelectArray[$k] = ' selected';
	}
}
*/

// leave unchecked on display
// 6/14/2011 defautt checked but don't display
if ($demoPDefaultFlag == 0) {
	foreach ($demoPSelectArray as $k => $v) 
	{
    	//$demoPSelectArray[$k] = ' checked';
	}
}

if ($demoGDefaultFlag == 0) {
	foreach ($demoGSelectArray as $k => $v) 
	{
    	//$demoGSelectArray[$k] = ' checked';
	}
}

if ($demoRDefaultFlag == 0) {
	foreach ($demoRSelectArray as $k => $v) 
	{
    	//$demoRSelectArray[$k] = ' checked';
	}
}

if ($demoADefaultFlag == 0) {
	foreach ($demoASelectArray as $k => $v) 
	{
    	//$demoASelectArray[$k] = ' checked';
	}
}

if ($demoEdDefaultFlag == 0) {
	foreach ($demoEdSelectArray as $k => $v) 
	{
    	//$demoEdSelectArray[$k] = ' checked';
	}
}

if ($demoIDefaultFlag == 0) {
	foreach ($demoISelectArray as $k => $v) 
	{
    	//$demoISelectArray[$k] = ' checked';
	}
}

if ($demoHDefaultFlag == 0) {
	foreach ($demoHSelectArray as $k => $v) 
	{
    	//$demoHSelectArray[$k] = ' checked';
	}
}

if ($demoEmDefaultFlag == 0) {
	foreach ($demoEmSelectArray as $k => $v) 
	{
    	//$demoEmSelectArray[$k] = ' checked';
	}
}

if ($demoCDefaultFlag == 0) {
	foreach ($demoCSelectArray as $k => $v) 
	{
    	//$demoCSelectArray[$k] = ' checked';
	}
}

if ($demoQDefaultFlag == 0) {
	foreach ($demoQSelectArray as $k => $v) 
	{
    	//$demoQSelectArray[$k] = ' checked';
	}
}

// set National if nothing selected
if (($locationSelectArray['locationByStates'] == '') && ($locationSelectArray['locationByZipCode'] == '')) {
	$locationSelectArray['locationByNational'] = 'checked="checked"';
}

// set Total Expenditure if nothing selected
if (($governmentSpendingSelectArray['gsPS'] == '') && ($governmentSpendingSelectArray['gsPW'] == '') && ($governmentSpendingSelectArray['gsH'] == '') && ($governmentSpendingSelectArray['gsU'] == '') && ($governmentSpendingSelectArray['gsT'] == '') && ($governmentSpendingSelectArray['gsL'] == '') && ($governmentSpendingSelectArray['gsF'] == '') && ($governmentSpendingSelectArray['gsM'] == '') && ($governmentSpendingSelectArray['gsSC'] == '')) {
	$governmentSpendingSelectArray['gsTotal'] = 'checked="checked"';
}

if (($governmentSpendingScreenSelectArray['DefinedRange'] == '') && ($governmentSpendingScreenSelectArray['PerCapitaSpending'] == '')) {
	$governmentSpendingScreenSelectArray['SpendingBuckets'] = 'checked="checked"';
	$governmentSpendingScreenSelectDisplayArray["SpendingBuckets"] = 'block';
}
  
// check size of results array

$govtCount = count($JSON_GOVERNMENTS_Array); // not used
$govtOfficialsCount = count($JSON_OFFICIALS_Array); 

/*
if ($govtOfficialsCount > 0) {
	foreach ($JSON_OFFICIALS_Array as $key => $value) {
		$govtRolesArray["$key"] = $value;
	}
}
*/
if ($govtOfficialsCount > 0) {
	foreach ($JSON_OFFICIALS_Array as $key => $value) {
		//echo "<br>JSON_OFFICIALS: Key: $key, Value: $value";
		switch ($key) {
			//case '0':
			case 'P0':
				$populationArray["P0"] = $value;
				break;
			//case '1':
			case 'P1':
				$populationArray["P1"] = $value;
				break;
			//case '2':
			case 'P2':
				$populationArray["P2"] = $value;
				break;
			//case '3':
			case 'P3':
				$populationArray["P3"] = $value;
				break;
			//case '4':
			case 'P4':
				$populationArray["P4"] = $value;
				break;
			//case '5':
			case 'P5':
				$populationArray["P5"] = $value;
				break;
			//case '6':
			case 'P6':
				$populationArray["P6"] = $value;
				break;
			//case '7':
			case 'P7':
				$populationArray["P7"] = $value;
				break;
			//case '8':
			case 'P8':
				$populationArray["P8"] = $value;
				break;
			//case '9':
			case 'P9':
				$populationArray["P9"] = $value;
				break;
			//case 'q':
			case 'Clerk':
				$govtRolesArray["Clerk"] = $value;
				break;
			//case 'w':
			case 'Governing Board Member':
				$govtRolesArray["Governing Board Member"] = $value;
				break;
			//case 'e':
			case 'Head of Finance/Budgeting':
				$govtRolesArray["Head Of Finance/Budgeting"] = $value;
				break;
			//case 'a':
			case 'Head of Fire Protection Services':
				$govtRolesArray["Head of Fire Protection Services"] = $value;
				break;
			//case 'r':
			case 'Head of Law Enforcement':
				$govtRolesArray["Head of Law Enforcement"] = $value;
				break;
			//case 'y':
			case 'Head of Public Works':
				$govtRolesArray["Head of Public Works"] = $value;
				break;
			//case 'u':
			case 'Head of Purchasing/Procurement':
				$govtRolesArray["Head of Purchasing/Procurement"] = $value;
				break;
			//case 'i':
			case 'Top Appointed Official':
				$govtRolesArray["Top Appointed Official"] = $value;
				break;
			//case 'o':
			case 'Top Elected Official':
				$govtRolesArray["Top Elected Official"] = $value;
				break;
			//case 'c':
			case 'county':
				$govtTypesArray["county"] = $value;
				break;
			//case 't':
			case 'township':
				$govtTypesArray["township"] = $value;
				break;
			//case 'm':
			case 'municipality':
				$govtTypesArray["municipality"] = $value;
				break;
			case 'GovAge0-49':
			case 'GovAge50-99':
			case 'GovAge100-149':
			case 'GovAge150-199':
			case 'GovAge200+':
			case 'GovAgeUnknown':
				$govtAgeArray["$key"] = $value;
				break;
			case 'AL':
			case 'AK':
			case 'AZ':
			case 'AR':
			case 'CA':
			case 'CO':
			case 'CT':
			case 'DE':
			case 'DC':
			case 'FL':
			case 'GA':
			case 'HI':
			case 'ID':
			case 'IL':
			case 'IN':
			case 'IA':
			case 'KS':
			case 'KY':
			case 'LA':
			case 'ME':
			case 'MD':
			case 'MA':
			case 'MI':
			case 'MN':
			case 'MS':
			case 'MO':
			case 'MT':
			case 'NE':
			case 'NV':
			case 'NH':
			case 'NJ':
			case 'NM':
			case 'NY':
			case 'NC':
			case 'ND':
			case 'OH':
			case 'OK':
			case 'OR':
			case 'PA':
			case 'RI':
			case 'SC':
			case 'SD':
			case 'TN':
			case 'TX':
			case 'UT':
			case 'VT':
			case 'VA':
			case 'WA':
			case 'WV':
			case 'WI':
			case 'WY':
				$statesArray["$key"] = $value;
				// count regions
				switch ($key) {
					case 'CT':
					case 'ME':
					case 'MA':
					case 'NH':
					case 'RI':	
					case 'VT':
					case 'NJ':
					case 'NY':
					case 'PA':
						$regionsArray['Northeast'] = $regionsArray['Northeast'] + $value;
						break;
					case 'DE':
					case 'DC':
					case 'FL':
					case 'GA':
					case 'MD':
					case 'NC':
					case 'SC':
					case 'VA':
					case 'WV':
					case 'AL':
					case 'KY':
					case 'MS':
					case 'TN':
					case 'AR':
					case 'LA':
					case 'OK':
					case 'TX':
						$regionsArray['South'] = $regionsArray['South'] + $value;
						break;
					case 'IN':
					case 'IL':
					case 'MI':
					case 'OH':
					case 'WI':
					case 'IA':
					case 'KS':
					case 'MN':
					case 'MO':
					case 'NE':
					case 'SD':
					case 'ND':
						$regionsArray['Midwest'] = $regionsArray['Midwest'] + $value;
						break;
					case 'AZ':
					case 'CO':
					case 'ID':
					case 'NM':
					case 'MT':
					case 'UT':
					case 'NV':
					case 'WY':
					case 'AK':
					case 'CA':
					case 'HI':
					case 'OR':
					case 'WA':
						$regionsArray['West'] = $regionsArray['West'] + $value;
						break;
					default:
						break;
				}
				break;
			// unused
			case 'GovAge0-49':
			case 'GovAge50-99':
			case 'GovAge100-149':
			case 'GovAge150-199':
			case 'GovAge200+':
			case 'GovAgeUnknown':
				$govAgeArray["$key"] = $value;
				break;
				
			//case 'z':
			case 'Fiscal_January':
				$fiscalYearArray["Fiscal_January"] = $value;
				break;
			//case 'x':
			case 'Fiscal_February':
				$fiscalYearArray["Fiscal_February"] = $value;
				break;
			//case 'v':
			case 'Fiscal_March':
				$fiscalYearArray["Fiscal_March"] = $value;
				break;
			//case 'b':
			case 'Fiscal_April':
				$fiscalYearArray["Fiscal_April"] = $value;
				break;
			//case 'n':
			case 'Fiscal_May':
				$fiscalYearArray["Fiscal_May"] = $value;
				break;
			//case 'l':
			case 'Fiscal_June':
				$fiscalYearArray["Fiscal_June"] = $value;
				break;
			//case 'k':
			case 'Fiscal_July':
				$fiscalYearArray["Fiscal_July"] = $value;
				break;
			//case 'j':
			case 'Fiscal_August':
				$fiscalYearArray["Fiscal_August"] = $value;
				break;
			//case 'h':
			case 'Fiscal_September':
				$fiscalYearArray["Fiscal_September"] = $value;
				break;
			//case 'g':
			case 'Fiscal_October':
				$fiscalYearArray["Fiscal_October"] = $value;
				break;
			//case 'f':
			case 'Fiscal_November':
				$fiscalYearArray["Fiscal_November"] = $value;
				break;
			//case 'd':
			case 'Fiscal_December':
				$fiscalYearArray["Fiscal_December"] = $value;
				break;
			//case 's':
			case 'Fiscal_Not_Known':
				$fiscalYearArray["Fiscal_Not_Known"] = $value;
				break;

			default: // catch all
				$unmatchedArray["$key"] = $value;
				break;
		}
		//print_r($govtRolesArray);
		//print_r($populationArray);
	}
}

//print_r($govtRolesArray);
//print_r($populationArray);
//print_r($unmatchedArray);
					
?>
<script type="text/javascript">
<!--
function toggle_visibility_ps(id,idImg) {
	var e = document.getElementById(id);
	var eImg = document.getElementById(idImg);
	
	//alert(e);
	//alert(eImg);
	
	if(e.style.display == 'none')
	{
	   e.style.display = 'block';
	   eImg.src = 'images/' + id + '-sel.jpg';
	}
	else {	
	   e.style.display = 'none';
	   eImg.src = 'images/' + id + '.jpg';
	}
}
//-->
</script>

<script type="text/javascript">
<!--
function toggle_visibility(id) {
var e = document.getElementById(id);
var img_e = id + 'img';
var eImg = document.getElementById(img_e);
//alert(e);
//alert(img_e);
if(e.style.display == 'none')
{
   e.style.display = 'block';
   eImg.src = 'images/' + id + '-sel.jpg';
}
else {	
   e.style.display = 'none';
   eImg.src = 'images/' + id + '.jpg';
   }
}
//alert(eImg);
//-->
</script>

<script type="text/javascript">
<!--
function toggle_visibilityDemo(id) {
	
	var e = document.getElementById(id);
	var idDisplay = '';
	
	//alert(id);
	
	var img_e = id + 'img';
	var eImg = document.getElementById(img_e);
	
	if(e.style.display == 'none')
	{
	   e.style.display = 'block';
	   eImg.src = 'images/' + id + '-sel.jpg';
	   idDisplay = 'block';
	}
	else {	
	   e.style.display = 'none';
	   eImg.src = 'images/' + id + '.jpg';
	   idDisplay = 'none';
	}
	
	//alert(idDisplay);
	
	// set session variables
	if (window.XMLHttpRequest)
	{
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else {
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	var fullURL = "PA-setDemoDisplay.php?id=" + id + "&stat=" + idDisplay;
	
	//alert(fullURL);
	
	xmlhttp.open("GET",fullURL,true);
	xmlhttp.send();
}
function showall() {
	
	var a = document.getElementById('demoG');
	var aImg = document.getElementById('demoGimg');
	a.style.display = 'block';
	aImg.src = 'images/demoG-sel.jpg';
	
	var b = document.getElementById('demoR');
	var bImg = document.getElementById('demoRimg');
	b.style.display = 'block';
	bImg.src = 'images/demoR-sel.jpg';
	
	var c = document.getElementById('demoA');
	var cImg = document.getElementById('demoAimg');
	c.style.display = 'block';
	cImg.src = 'images/demoA-sel.jpg';
	
	var d = document.getElementById('demoEd');
	var dImg = document.getElementById('demoEdimg');
	d.style.display = 'block';
	dImg.src = 'images/demoEd-sel.jpg';
	
	var e = document.getElementById('demoI');
	var eImg = document.getElementById('demoIimg');
	e.style.display = 'block';
	eImg.src = 'images/demoI-sel.jpg';
	
	var f = document.getElementById('demoEm');
	var fImg = document.getElementById('demoEmimg');
	f.style.display = 'block';
	fImg.src = 'images/demoEm-sel.jpg';
	
	var g = document.getElementById('demoH');
	var gImg = document.getElementById('demoHimg');
	g.style.display = 'block';
	gImg.src = 'images/demoH-sel.jpg';
	
	var h = document.getElementById('demoC');
	var hImg = document.getElementById('demoCimg');
	h.style.display = 'block';
	hImg.src = 'images/demoC-sel.jpg';
	
	var i = document.getElementById('demoQ');
	var iImg = document.getElementById('demoQimg');
	i.style.display = 'block';
	iImg.src = 'images/demoQ-sel.jpg';
	
	var j = document.getElementById('demoPO');
	var jImg = document.getElementById('demoPOimg');
	j.style.display = 'block';
	jImg.src = 'images/demoPO-sel.jpg';
	
	// set session variables
	if (window.XMLHttpRequest)
	{
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else {
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	//var fullURL = "PA-setDemoDisplay.php?id=demoG&stat=block";
	
	xmlhttp.open("GET","PA-setDemoDisplayAll.php",true);
	xmlhttp.send();
}
function hideall() {
	
	var a = document.getElementById('demoG');
	var aImg = document.getElementById('demoGimg');
	a.style.display = 'none';
	aImg.src = 'images/demoG.jpg';
	
	var b = document.getElementById('demoR');
	var bImg = document.getElementById('demoRimg');
	b.style.display = 'none';
	bImg.src = 'images/demoR.jpg';
	
	var c = document.getElementById('demoA');
	var cImg = document.getElementById('demoAimg');
	c.style.display = 'none';
	cImg.src = 'images/demoA.jpg';
	
	var d = document.getElementById('demoEd');
	var dImg = document.getElementById('demoEdimg');
	d.style.display = 'none';
	dImg.src = 'images/demoEd.jpg';
	
	var e = document.getElementById('demoI');
	var eImg = document.getElementById('demoIimg');
	e.style.display = 'none';
	eImg.src = 'images/demoI.jpg';
	
	var f = document.getElementById('demoEm');
	var fImg = document.getElementById('demoEmimg');
	f.style.display = 'none';
	fImg.src = 'images/demoEm.jpg';
	
	var g = document.getElementById('demoH');
	var gImg = document.getElementById('demoHimg');
	g.style.display = 'none';
	gImg.src = 'images/demoH.jpg';
	
	var h = document.getElementById('demoC');
	var hImg = document.getElementById('demoCimg');
	h.style.display = 'none';
	hImg.src = 'images/demoC.jpg';
	
	var i = document.getElementById('demoQ');
	var iImg = document.getElementById('demoQimg');
	i.style.display = 'none';
	iImg.src = 'images/demoQ.jpg';
	
	var j = document.getElementById('demoPO');
	var jImg = document.getElementById('demoPOimg');
	j.style.display = 'none';
	jImg.src = 'images/demoPO.jpg';
	
	// set session variables
	if (window.XMLHttpRequest)
	{
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else {
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	//var fullURL = "PA-setDemoDisplay.php?id=demoG&stat=none";
	
	xmlhttp.open("GET","PA-resetDemoDisplayAll.php",true);
	xmlhttp.send();
}
//-->
</script>

<?php
// determine width for states
$userAgent = $_SERVER['HTTP_USER_AGENT'];
$pos = strpos($userAgent, 'MSIE');
if ($pos === false) {
	$selWidth = '172';
	$expFuncWidth = '132';
	$demoFuncWidth = '690';
	$demoFuncHeight = '100';
	$positioning = 'absolute';
} else {
	// MSIE
	$selWidth = '194';
	$expFuncWidth = '152';
	$demoFuncWidth = '690';
	$demoFuncHeight = '100';
	$positioning = 'relative';
}

// bg colors
$demoBG = '#5d7c9c'; // default
//$demoBG = '#6D8BA9'; // lighter

?>