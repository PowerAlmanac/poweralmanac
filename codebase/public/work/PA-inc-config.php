<?php
session_start();

// debug - localhost
error_reporting(E_ALL);
$serverURL = "http://localhost/PA-AWSEC2-Server/version1.0";
$recaptcha_publickey = "6LcQWMESAAAAAOtaenI-YdN3Wpyw0NnJgpbwWiaW";
$recaptcha_privatekey = "6LcQWMESAAAAAG-xM5qy3WYx79SlccTYnf_MuC0m";

$useReCaptcha = 1;

// production 1
error_reporting(0);
$serverURL = "http://www.poweralmanac.com";
$recaptcha_publickey = "6Le328ESAAAAAOtIcADpepAZiT59yT3qFIm2_DZj";
$recaptcha_privatekey = "6Le328ESAAAAABqpWmImCN4_WyO2IovH0GhC-Ekr";

// production 2
//error_reporting(0);
//$serverURL = "http://poweralmanac.com";
//$recaptcha_publickey = "6LcPWMESAAAAAMhB49aJLkZ3olKAuD6RRHyA0W-E";
//$recaptcha_privatekey = "6LcPWMESAAAAAKYK7yY_esC7yjIEEAv_PF7cdFan";

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');

if (isset($_SESSION['logged_in'])) {
	if ($_SESSION['logged_in'] != '1') {
		$_SESSION['user_subscription'] = '0';
	}
} else {
	//
	$_SESSION['logged_in'] = '0';
}

if (!isset($_SESSION['lastSearchParams'])) {
	$_SESSION['lastSearchParams'] = 'byZipcode=&byZipcodeDistance=&submit=Search+Power+Almanac';
}

$selTopNav = 'findlistings';

?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21974562-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
