<?php
error_reporting(E_ALL);

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');
		
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error()); 
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error()); 

/*
$searchParams = 'byText=&byZipcode=&byZipcodeDistance=&submit=Search+Power+Almanac';

echo("<h3>Search Details Routines</h3>");

echo("<br /><b>Calling SearchDetails</b><br />"); 
$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo3->query("CALL SearchDetails(25,'$searchParams',@Result)"); // IN numRecords2Show INT, IN input TEXT, OUT Result MEDIUMTEXT
$pdoObject = $pdo3->query("SELECT @Result");
$rsArray = $pdoObject->fetchAll();
//print_r($rsArray); exit;
$json = $rsArray[0]['@Result'];
//print_r($json); exit;
$jsonArray = json_decode($json, TRUE);
print_r($jsonArray);
*/

//$searchParams = 'byText=&byZipcode=&byZipcodeDistance=&SAFETY_service1=Fire+Protection&submit=Search+Power+Almanac';
//echo("<br /><b>Calling SearchDetailsPower - Services : Fire Protection</b><br />"); 

//$searchParams = 'byText=&govLoc_byState%5B%5D=Washington&role_2=Top+Appointed+Official&HEALTH_service6=Ambulances&submit=Search+Power+Almanac';
//$searchParams = 'byText=&govLoc_byState%5B%5D=Washington&role_1=Top+Elected+Official&HEALTH_service6=Ambulances&submit=Search+Power+Almanac';
//$searchParams = 'byText=&govLoc_byState%5B%5D=Washington&role_9=Clerk&HEALTH_service6=Ambulances&submit=Search+Power+Almanac';
//$searchParams = 'byText=&govLoc_byState%5B%5D=Washington&HEALTH_service6=Ambulances&submit=Search+Power+Almanac';
$searchParams = 'byText=&';
$pdo2 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo2->query("CALL Delete_Research_Tables('nicktest')"); 
$pdo2->query(sprintf("CALL SearchSummary('nicktest','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($searchParams)));
$pdo2->query("CALL SearchDetails(1000,'nicktest',@Result)");
$pdoObject = $pdo2->query("SELECT @Result, @top_elected_flag");
$rsArray = $pdoObject->fetchAll();
$json2 = $rsArray[0]['@Result'];
print_r($json2); exit;
$jsonArray2 = json_decode($json2, TRUE);
print_r($jsonArray2);

?>
<html>
<head>
<title>Search Results Preview</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript" src="http://use.typekit.com/tvv6urc.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="popuphelp.js"></script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
</head>

<body bgcolor="#ACC6E1" background="images/ACC6E1-pixel.jpg">
<p>
<b>Calling SearchDetails: <?php echo($searchParams); ?></b>
</p>
<table width="910" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#A1B9D5">
<tr>
	<td colspan="10" align="center">
    <?php
	echo("<b>top_elected_flag = $top_elected_flag</b>");
	?>
    </td>
</tr>
<tr bgcolor="#ACC6E1">
	<td>&nbsp;
	
	</td>
	<td colspan="4">
	<font color="#7591AE"><b>Local Government</b></font>
	</td>
	<td colspan="3">
	<font color="#7591AE"><b>Official</b></font>
	</td>
	<td colspan="3">
	<font color="#7591AE"><b>Contact Information</b></font>
	</td>
</tr>
<tr bgcolor="#7492AE" height="15">
	<td>
	<font color="#FFFFFF">Record#</font>
	</td>
	<td>
	<font color="#FFFFFF">Name</font>
	</td>
	<td>
	<font color="#FFFFFF">Type</font>
	</td>
	<td>
	<font color="#FFFFFF">State<br />Location</font>
	</td>
	<td>
	<font color="#FFFFFF">Community<br />Population</font>
	</td>
	<td>
	<font color="#FFFFFF">Name</font>
	</td>
	<td>
	<font color="#FFFFFF">Role</font>
	</td>
	<td>
	<font color="#FFFFFF">Title</font>
	</td>
	<td>
	<font color="#FFFFFF">Address</font>
	</td>
	<td>
	<font color="#FFFFFF">Phone</font>
	</td>
	<td>
	<font color="#FFFFFF">Email</font>
	</td>
</tr>
<?php
/*
            [Govt_Off_ID] => 1996
            [Government_ID] => 48203901200000
            [Government_Type] => municipality
            [Form_of_Government] => 4
            [Governing_Body_Name] => 0
            [County_Name] => YAKIMA
            [Role] => Governing Board Member
            [First_Name] => Rita 
            [Last_Name] => Zuniga
            [Government_Title] => Councilwoman
            [Term_Month_End_Date] => n/a
            [Term_Year_End_Date] => n/a
            [Government_Place_Name] => WAPATO
            [Government_Type_Name] => CITY
            [Population_2006] => 4608
            [Mailing_State] => Washington 
            [Email_Address] => No Email
            [Phone_Number] => 509-877-2334
            [Mailing_Street_Box] => 205 South Simcoe Avenue
*/
	foreach ($jsonArray2 as $item) {
		$haveAddress = '';
		$havePhone = '';
		$haveEMail = '';
		$Govt_Off_ID = $item['Govt_Off_ID'];
		$Government_ID = $item['Government_ID'];
		$Government_Type = $item['Government_Type'];
		$Government_Place_Name = $item['Government_Place_Name'];
		$Population_2006 = $item['Population_2006'];
		$Form_of_Government = $item['Form_of_Government'];
		$County_Name = $item['County_Name'];
		$Mailing_State = $item['Mailing_State'];
		$County_Name = $item['County_Name'];
		$Role = $item['Role'];
		$First_Name = $item['First_Name'];
		$Last_Name = $item['Last_Name'];
		$Government_Title = $item['Government_Title'];
		// check for existence of these
		$Email_Address = $item['Email_Address'];
		$Phone_Number = $item['Phone_Number'];
		$Mailing_Street_Box = $item['Mailing_Street_Box'];
		if ($Email_Address != 'No Email') {
			$haveEMail = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		if ($Phone_Number != 'n/a') {
			$havePhone = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		if ($Mailing_Street_Box != 'n/a') {
			$haveAddress = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		// clean up
		$Government_Place_NameCLEANED = ucwords(strtolower($Government_Place_Name));
		$Government_TypeCLEANED = ucwords($Government_Type);
		$PopulationCLEANED = number_format($Population_2006);
		echo("<tr><td>$Govt_Off_ID</td><td>$Government_Place_NameCLEANED</td><td>$Government_TypeCLEANED</td><td>$Mailing_State</td><td>$PopulationCLEANED</td><td>$First_Name $Last_Name</td><td>$Role</td><td>$Government_Title</td><td>$haveAddress</td><td>$havePhone</td><td>$haveEMail</td></tr>");
	}
?>
</table>


</body>
</html>