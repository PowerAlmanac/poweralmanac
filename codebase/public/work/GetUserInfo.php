<?php
error_reporting(E_ALL);

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');

function DisplayUserInfo($json) { 
	// display a HTML user info table
	$jsonArray = json_decode($json, TRUE);
	$User_Email = $jsonArray['User_Email'];
	$User_Password = $jsonArray['User_Password'];
	$User_FirstName = $jsonArray['User_FirstName'];
	$User_LastName = $jsonArray['User_LastName'];
	$User_Middle_Name = $jsonArray['User_Middle_Name'];
	$User_Subscription = $jsonArray['User_Subscription'];
	$User_Parent = $jsonArray['User_Parent'];
	$User_CompanyName = $jsonArray['User_CompanyName'];
	$User_DL_Limit = $jsonArray['User_DL_Limit'];
	$User_DL_Reserves = $jsonArray['User_DL_Reserves'];
	$User_Activated = $jsonArray['User_Activated'];
	$User_ActivationCode = $jsonArray['User_ActivationCode'];
	$User_OptInAlerts = $jsonArray['User_OptInAlerts'];
	$User_OptInNewsletter = $jsonArray['User_OptInNewsletter'];
	$DateTime_SubscriptionStart = $jsonArray['DateTime_SubscriptionStart'];
	$DateTime_SubscriptionEnd = $jsonArray['DateTime_SubscriptionEnd'] ;
	$DateTime_Registered = $jsonArray['DateTime_Registered'];
	$TimeZone = $jsonArray['TimeZone'];
	echo('<table cellpadding="2" cellspacing="3" border="0" align="center" width="800">');
	echo('<tr>');
	echo('<td>');
	echo("$User_FirstName $User_Middle_Name $User_LastName ($User_Email $User_Password) $User_CompanyName<br />");
	echo("SUB: $User_Subscription ($User_Parent) $User_DL_Limit ($User_DL_Reserves)<br />");
	echo("ACT: $User_Activated ($User_ActivationCode) [$User_OptInAlerts $User_OptInNewsletter]<br />");
	echo("DATE: $DateTime_SubscriptionStart to $DateTime_SubscriptionEnd ($DateTime_Registered) $TimeZone<br />");
	echo('</td>');
	echo('</tr>');
	echo('</table>');
	return;
}
	
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error()); 
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error()); 

echo("<br /><b>Calling GetUserInfo</b><br />"); // IN eMail VARCHAR(255),OUT JSON_String TEXT
$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo3->query("CALL GetUserInfo('abc@poweralmanac.com',@JSON_String)"); 

$pdoObject2 = $pdo3->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
//print_r($rsArray2);exit;

$json2a = $rsArray2[0]['@JSON_String'];
//print_r($json2a);exit;

DisplayUserInfo($json2a);


$pdo3->query("CALL GetUserInfo('nick@poweralmanac.com',@JSON_String)"); 

$pdoObject2 = $pdo3->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
//print_r($rsArray2);exit;

$json2a = $rsArray2[0]['@JSON_String'];
//print_r($json2a);exit;

DisplayUserInfo($json2a);

exit;

$jsonArray2a = json_decode($json2a, TRUE);
print_r($jsonArray2a);

?>



