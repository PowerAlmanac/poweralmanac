<?php
error_reporting(E_ALL);

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');
		
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error()); 
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error()); 

//$searchParams = 'byText=&selLocation=locationByStates&govLoc_byState%5B%5D=Michigan&';
//$searchParams = 'byText=&govType_counties=county&byZipcode=&byZipcodeDistance=&submit=Search+Power+Almanac';
//$searchParams = 'byZipcode=98951&byZipcodeDistance=&submit=Search+Power+Almanac';
//$searchParams = 'byText=&govType_counties=county&govType_townships=township&govType_municipalities=municipality&role_3=Governing+Board+Member&selLocation=locationByNational&expType%5B%5D=1&criteria=expenseBudgetQuartile&'; 
//$searchParams = 'byText=&govType_counties=county&govType_townships=township&govType_municipalities=municipality&role_5=Head+of+Purchasing%2FProcurement&selLocation=locationByNational&';

$searchParams = 'byText=&';

$pdo2 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo2->query("CALL Delete_Research_Tables('nicktest')"); 
$pdo2->query(sprintf("CALL SearchSummary('nicktest',true,'%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists)", escape($searchParams)));
$pdoObject = $pdo2->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists");
$rsArray = $pdoObject->fetchAll();
$JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
$JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
$JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];
$flag = $rsArray[0]['@flag'];
$table_exists = $rsArray[0]['@table_exists'];
//print_r($rsArray);exit;
//print_r($JSON_SUMMARY);print_r($JSON_GOVERNMENTS);print_r($JSON_OFFICIALS);print_r($flag);exit;
print_r($JSON_OFFICIALS);

$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
//print_r($JSON_SUMMARY_Array);

$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
//print_r($JSON_GOVERNMENTS_Array);

$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);
print_r($JSON_OFFICIALS_Array);

exit;

$Num_Matched_Governments = number_format($JSON_SUMMARY_Array['Num_Matched_Governments']);
$Num_Matched_Officials = number_format($JSON_SUMMARY_Array['Num_Matched_Officials']);
$Num_Matched_Records = number_format($JSON_SUMMARY_Array['Num_Matched_Records']);
$Num_Matched_Emails = number_format($JSON_SUMMARY_Array['Num_Matched_Emails']);

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query("CALL DatabaseSummary(@Result)"); 
$pdoObject = $pdo->query("SELECT @Result");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@Result'];
$jsonArray = json_decode($json, TRUE);

print_r($jsonArray);

$Total_No_Of_Records = number_format($jsonArray['Total_No_Of_Records']);
$Total_No_Of_Changed_Rec = number_format($jsonArray['Total_No_Of_Changed_Rec']);
$Total_Gov_Officials = number_format($jsonArray['Total_Gov_Officials']);
$Total_Governments = number_format($jsonArray['Total_Governments']);
$Total_Gov_With_No_Offices = number_format( $jsonArray['Total_Gov_With_No_Offices']);
$Total_Available_Emails = number_format($jsonArray['Total_Available_Emails']);

?>
<html>
<head>
<title>Search Summary</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript" src="http://use.typekit.com/tvv6urc.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="popuphelp.js"></script>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<body bgcolor="#999999">

				<table cellpadding="0" cellspacing="5" border="0" align="center" width="95%">
                <tr>
                	<td colspan="2">
                    <p class="matchedRecords">
                    Records Found: <?php echo($Num_Matched_Officials); ?> 
                    </p>
                    <p class="matchedLarge">
                    <?php echo($Num_Matched_Officials); ?> Local Government Officials
                    <br />
                    <font class="PALarge">out of <?php echo($Total_Gov_Officials); ?> in the Power Almanac</font>
                    </p>
                    </td>
                </tr>
				<tr>
					<td width="274" valign="top">
                    <p class="matchedMedium">
                    <?php echo($Num_Matched_Emails); ?> with Email Addresses<br />
					NNN with Phone Numbers<br />
					NNN with Mailing Addresses<br />
                    </p>		
					</td>
					<td valign="top">	                    
					<p class="PAMedium">
					<?php echo($Total_Available_Emails); ?> with Email Addresses<br />
					234,567 with Phone Numbers<br />
					123,456 with Mailing Addresses<br />
                    in the Power Almanac
					</p>
					</td>
				</tr>
                <tr>
                	<td colspan="2">
                    <p class="matchedLarge">
                    <?php echo($Num_Matched_Governments); ?> Governments
                    <br />
                    <font class="PALarge">out of <?php echo($Total_Governments); ?> in the Power Almanac</font>
                    </p>
                    </td>
                </tr>
				</table>


</body>
</html>