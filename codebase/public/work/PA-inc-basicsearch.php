<?php
$searchParams = 'byText=&';
$JSON_GOVERNMENTS_Array = array();
$JSON_OFFICIALS_Array = array();
include("PA-inc-searchinit.php");
?>
<html>
<head>
<title>Search for Local Gov Officials | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="description" content="Online directory of city and county officials for all U.S.  Easy to use, comprehensive, and up-to-date.  Download names, titles, emails, phone numbers, and more." />
<meta name="Keywords" content="Cities,Counties,Government officials,Government emails,Government phone numbers" /> 

<link rel="stylesheet" href="popup.css" type="text/css">
<script type="text/javascript" src="custom-form-elements.js"></script>

                <table cellpadding="0" cellspacing="0" width="261" align="center" border="0">
                <tr>
                	<td colspan="2" valign="top" bgcolor="#99b8d8">
                    <div id="selSTATES">
					<p class="cb">
                    <a href="javascript: void(0)" onclick="toggleAllLocationButton('locationCheckBoxIMG');" id="locationCheckBoxHREF"><img id="locationCheckBoxIMG" src="<?php echo($checkbox_StatesImg); ?>" width="15" height="20" hspace="2" vspace="0" border="0" align="bottom" /></a>
                    <b>SELECT ALL STATES</b><br />
                    <input name="govLoc_byState[]" type="checkbox" class="styled" id="location" value="Alabama" <?php echo($statesSelectArray['Alabama']); ?> />Alabama (<font class="searchCounts"><?php echo(number_format($statesArray['AL'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Alaska" class="styled" <?php echo($statesSelectArray['Alaska']); ?> />Alaska (<font class="searchCounts"><?php echo(number_format($statesArray['AK'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Arizona" class="styled" <?php echo($statesSelectArray['Arizona']); ?> />Arizona (<font class="searchCounts"><?php echo(number_format($statesArray['AZ'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Arkansas" class="styled" <?php echo($statesSelectArray['Arkansas']); ?> />Arkansas (<font class="searchCounts"><?php echo(number_format($statesArray['AR'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="California" class="styled" <?php echo($statesSelectArray['California']); ?> />California (<font class="searchCounts"><?php echo(number_format($statesArray['CA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Colorado" class="styled" <?php echo($statesSelectArray['Colorado']); ?> />Colorado (<font class="searchCounts"><?php echo(number_format($statesArray['CO'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" name="Connecticut" value="Connecticut" class="styled" <?php echo($statesSelectArray['Connecticut']); ?> />Connecticut (<font class="searchCounts"><?php echo(number_format($statesArray['CT'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Delaware" class="styled" <?php echo($statesSelectArray['Delaware']); ?> />Delaware (<font class="searchCounts"><?php echo(number_format($statesArray['DE'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="District of Columbia" class="styled" <?php echo($statesSelectArray['District+of+Columbia']); ?> />District of Columbia (<font class="searchCounts"><?php echo(number_format($statesArray['DC'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Florida" class="styled" <?php echo($statesSelectArray['Florida']); ?> />Florida (<font class="searchCounts"><?php echo(number_format($statesArray['FL'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Georgia" class="styled" <?php echo($statesSelectArray['Georgia']); ?> />Georgia (<font class="searchCounts"><?php echo(number_format($statesArray['GA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Hawaii" class="styled" <?php echo($statesSelectArray['Hawaii']); ?> />Hawaii (<font class="searchCounts"><?php echo(number_format($statesArray['HI'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Idaho" class="styled" <?php echo($statesSelectArray['Idaho']); ?> />Idaho (<font class="searchCounts"><?php echo(number_format($statesArray['ID'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Illinois" class="styled" <?php echo($statesSelectArray['Illinois']); ?> />Illinois (<font class="searchCounts"><?php echo(number_format($statesArray['IL'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Indiana" class="styled" <?php echo($statesSelectArray['Indiana']); ?> />Indiana (<font class="searchCounts"><?php echo(number_format($statesArray['IN'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Iowa" class="styled" <?php echo($statesSelectArray['Iowa']); ?> />Iowa (<font class="searchCounts"><?php echo(number_format($statesArray['IA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Kansas" class="styled" <?php echo($statesSelectArray['Kansas']); ?> />Kansas (<font class="searchCounts"><?php echo(number_format($statesArray['KS'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Kentucky" class="styled" <?php echo($statesSelectArray['Kentucky']); ?> />Kentucky (<font class="searchCounts"><?php echo(number_format($statesArray['KY'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Louisiana" class="styled" <?php echo($statesSelectArray['Louisiana']); ?> />Louisiana (<font class="searchCounts"><?php echo(number_format($statesArray['LA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Maine" class="styled" <?php echo($statesSelectArray['Maine']); ?> />Maine (<font class="searchCounts"><?php echo(number_format($statesArray['ME'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Maryland" class="styled" <?php echo($statesSelectArray['Maryland']); ?> />Maryland (<font class="searchCounts"><?php echo(number_format($statesArray['MD'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Massachusetts" class="styled" <?php echo($statesSelectArray['Massachusetts']); ?> />Massachusetts (<font class="searchCounts"><?php echo(number_format($statesArray['MA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Michigan" class="styled" <?php echo($statesSelectArray['Michigan']); ?> />Michigan (<font class="searchCounts"><?php echo(number_format($statesArray['MI'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Minnesota" class="styled" <?php echo($statesSelectArray['Minnesota']); ?> />Minnesota (<font class="searchCounts"><?php echo(number_format($statesArray['MN'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Mississippi" class="styled" <?php echo($statesSelectArray['Mississippi']); ?> />Mississippi (<font class="searchCounts"><?php echo(number_format($statesArray['MS'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Missouri" class="styled" <?php echo($statesSelectArray['Missouri']); ?> />Missouri (<font class="searchCounts"><?php echo(number_format($statesArray['MO'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Montana" class="styled" <?php echo($statesSelectArray['Montana']); ?> />Montana (<font class="searchCounts"><?php echo(number_format($statesArray['MT'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Nebraska" class="styled" <?php echo($statesSelectArray['Nebraska']); ?> />Nebraska (<font class="searchCounts"><?php echo(number_format($statesArray['NE'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Nevada" class="styled" <?php echo($statesSelectArray['Nevada']); ?> />Nevada (<font class="searchCounts"><?php echo(number_format($statesArray['NV'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="New Hampshire" class="styled" <?php echo($statesSelectArray['New+Hampshire']); ?> />New Hampshire (<font class="searchCounts"><?php echo(number_format($statesArray['NH'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="New Jersey" class="styled" <?php echo($statesSelectArray['New+Jersey']); ?> />New Jersey (<font class="searchCounts"><?php echo(number_format($statesArray['NJ'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="New Mexico" class="styled" <?php echo($statesSelectArray['New+Mexico']); ?> />New Mexico (<font class="searchCounts"><?php echo(number_format($statesArray['NM'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="New York" class="styled" <?php echo($statesSelectArray['New+York']); ?> />New York (<font class="searchCounts"><?php echo(number_format($statesArray['NY'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="North Carolina" class="styled" <?php echo($statesSelectArray['North+Carolina']); ?> />North Carolina (<font class="searchCounts"><?php echo(number_format($statesArray['NC'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="North Dakota" class="styled" <?php echo($statesSelectArray['North+Dakota']); ?> />North Dakota (<font class="searchCounts"><?php echo(number_format($statesArray['ND'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Ohio" class="styled" <?php echo($statesSelectArray['Ohio']); ?> />Ohio (<font class="searchCounts"><?php echo(number_format($statesArray['OH'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Oklahoma" class="styled" <?php echo($statesSelectArray['Oklahoma']); ?> />Oklahoma (<font class="searchCounts"><?php echo(number_format($statesArray['OK'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Oregon" class="styled" <?php echo($statesSelectArray['Oregon']); ?> />Oregon (<font class="searchCounts"><?php echo(number_format($statesArray['OR'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Pennsylvania" class="styled" <?php echo($statesSelectArray['Pennsylvania']); ?> />Pennsylvania (<font class="searchCounts"><?php echo(number_format($statesArray['PA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Rhode Island" class="styled" <?php echo($statesSelectArray['Rhode+Island']); ?> />Rhode Island (<font class="searchCounts"><?php echo(number_format($statesArray['RI'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="South Carolina" class="styled" <?php echo($statesSelectArray['South+Carolina']); ?> />South Carolina (<font class="searchCounts"><?php echo(number_format($statesArray['SC'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="South Dakota" class="styled" <?php echo($statesSelectArray['South+Dakota']); ?> />South Dakota (<font class="searchCounts"><?php echo(number_format($statesArray['SD'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Tennessee" class="styled" <?php echo($statesSelectArray['Tennessee']); ?> />Tennessee (<font class="searchCounts"><?php echo(number_format($statesArray['TN'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Texas" class="styled" <?php echo($statesSelectArray['Texas']); ?> />Texas (<font class="searchCounts"><?php echo(number_format($statesArray['TX'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Utah" class="styled" <?php echo($statesSelectArray['Utah']); ?> />Utah (<font class="searchCounts"><?php echo(number_format($statesArray['UT'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Vermont" class="styled" <?php echo($statesSelectArray['Vermont']); ?> />Vermont (<font class="searchCounts"><?php echo(number_format($statesArray['VT'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Virginia" class="styled" <?php echo($statesSelectArray['Virginia']); ?> />Virginia (<font class="searchCounts"><?php echo(number_format($statesArray['VA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Washington" class="styled" <?php echo($statesSelectArray['Washington']); ?> />Washington (<font class="searchCounts"><?php echo(number_format($statesArray['WA'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="West Virginia" class="styled" <?php echo($statesSelectArray['West+Virginia']); ?> />West Virginia (<font class="searchCounts"><?php echo(number_format($statesArray['WV'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Wisconsin" class="styled" <?php echo($statesSelectArray['Wisconsin']); ?> />Wisconsin (<font class="searchCounts"><?php echo(number_format($statesArray['WI'])); ?></font>)<br />
                    <input type="checkbox" name="govLoc_byState[]" id="location" value="Wyoming" class="styled" <?php echo($statesSelectArray['Wyoming']); ?> />Wyoming (<font class="searchCounts"><?php echo(number_format($statesArray['WY'])); ?></font>)<br />

                    </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" bgcolor="#99b8d8">
                    <img src="images/space.gif" width="1" height="8" align="absmiddle">
                    </td>
                </tr>

                <tr>
                	<td colspan="2" valign="top">
                    <img src="images/space.gif" width="1" height="5" align="absmiddle">
                    </td>
                </tr>
                </table>