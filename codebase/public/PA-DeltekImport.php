<?php
include("inc/config.php");
$title = 'Best Local Gov Info | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

//Import Contacts
$importCount = 0;
$importCount = (!empty($_GET['PowerAlmanacCount']) ? $_GET['PowerAlmanacCount'] : $_POST['PowerAlmanacCount']);
$contact_list = (!empty($_GET['PowerAlmanacContactIDList']) ? $_GET['PowerAlmanacContactIDList'] : $_POST['PowerAlmanacContactIDList']);
$contacts = explode(',',$contact_list);

// Sanitize
$importCount = Sanitize::number($importCount);
// $contact_list ..

//Save Contact List into Database
if(empty($_SESSION['RegUser_ID'])){
    $id = session_id();     
    $_SESSION['deltek_session'] = TRUE;
}else{
    $id = $_SESSION['RegUser_ID'];    
}


    $pdo->query(
            sprintf("
            INSERT IGNORE INTO `deltekimports` (`user_id`, `contact_list`,`import_count`,`import_date`) VALUES ('%s', '%s','%s',NOW())",
                escape($id),
                escape($contact_list),
                escape($importCount)
            ));
    // $stm = $pdo->prepare("SELECT import_id FROM deltekimports WHERE user_id = ? LIMIT 1");
    // $stm->execute(array($id));
    // $table_id = $stm->fetch();
    $import_id = $pdo->lastInsertId();

foreach ($contacts as $govtOfficalsID) {
    //Insert into table
    $pdo->query(sprintf("CALL SaveIDForDeltekTable('%s','officials_%s')", escape($govtOfficalsID), escape($import_id)));
}

$table = "officials_" . $import_id;
$searchParams = 'byText=&deltek=' . $table;

$_SESSION['deltek_exists'] = $import_id;
$_SESSION['firstTimeSearch'] = 0;
$_SESSION['lastSearchParams'] = $searchParams;
$_SESSION['searchRequestArrayJSON'] = '{"deltek":"'. $table .'"}';
$_SESSION['deltek_active'] = TRUE;


//Save User Info to Database
    //Import ID, Session_ID/USER_ID, Contact List

?>
<script>
<?php
    if($_SESSION['logged_in'] == '1'){
        echo "var loggedIn = true;\n";
        echo "var subscriptionType = ".$_SESSION['user_subscription'].";\n";
        if($_SESSION['user_subscription'] > 0){
            $User_Email = $_SESSION['user_email'];
            $pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
            $pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
            $pdoObject2 = $pdo->query("SELECT @JSON_String");
            $rsArray2 = $pdoObject2->fetchAll();
            $json2a = $rsArray2[0]['@JSON_String'];
            $jsonArray2a = json_decode($json2a, TRUE);
            $User_Password = $jsonArray2a['User_Password'];
            $User_Parent = $jsonArray2a['User_Parent'];
            $User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
            echo 'var creditsRemaining ='. $User_DL_Reserves;
        }
    }
    else{
        echo "var loggedIn = false;\n";
        echo "var subscriptionType = 0;\n";
        echo "var creditsRemaining = 0;\n";
    }
?>
</script>
            <div class="intro deltek-import">
                <div class="intro-holder">
                    <h1><span class="importCount"><?php echo $importCount; ?></span> <span class="small-title">Records received from</span> <img src="/images/GovWin.jpg" /></h1>
                </div>
            </div>
            <div class="main-holder">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame">
                            <div class="heading">
                                <div class="half gov_results">
                                    <h2>Results from your <br /><img src="/images/blue_gov_win.png" /> Search</h2>
                                    <div id="priceBar">
                                        <div id="pricingHeader">PRICING<span id="pricingTop">N/A</span></div>
                                        <div class="blackinset">
                                            <div class="whole borderUnder">Quantity<span id="quantityRecords">N/A</span></div>
                                            <div class="whole borderUnder">Cost / Record<span id="costPerRecord">N/A</span></div>
                                            <div class="whole grayEncapsule">Total<span id="totalPricing">N/A</span></div>
                                            <div class="whole">
                                                <!-- <a class="btn">Save Search</a> -->
                                                <span><a class="btn btn-warning" href="/PA-Download.php">Checkout</a></span></div>
                                            <div class="whole borderUnder"><strong id="purchaseOrUpgrade">Included in Purchase</strong></div>
                                            <div class="whole borderUnder" id="subscriptionType">&nbsp;<span>N/A</span></div>
                                            <div class="whole borderUnder">Included Credits<span id="includedCredits">N/A</span></div>
                                            <div class="whole borderUnder">Remaining Credits<span id="remainingCredits">N/A</span></div>
                                            <div class="whole"><a href="/PA-Pricing.php" id="otheroptions" target="_blank">See Other Pricing Options</a></div>                                            
                                        </div>                                       
                                    </div>
                                </div>
                                <div class="half maximize_search">                                    
                                    <a href="/D-search-reset.php" class="btn btn-warning btn-lg">Start Searching Now &rarr;</a>
                                </div>
                            </div>
                            <!-- subfooter -->
                            <div class="subfooter">
                                <strong class="title">Is this your first visit to Power Almanac?</strong>
                                <a href="/search-government.php?ns=1" class="btn-search btn-gray"><span>Find out more &rarr;</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


     <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.colorbox-min.js"></script>
            <script src="js/bootstrapSwitch.js"></script>
            <script src="js/jquery.nouislider.min.js"></script>











    <script>
    <?php
        if($_SESSION['logged_in'] == '1'){
            echo "var loggedIn = true;\n";
            echo "var subscriptionType = ".$_SESSION['user_subscription'].";\n";
            if($_SESSION['user_subscription'] > 0){
                $User_Email = $_SESSION['user_email'];
                $pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
                $pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
                $pdoObject2 = $pdo->query("SELECT @JSON_String");
                $rsArray2 = $pdoObject2->fetchAll();
                $json2a = $rsArray2[0]['@JSON_String'];
                $jsonArray2a = json_decode($json2a, TRUE);
                $User_Password = $jsonArray2a['User_Password'];
                $User_Parent = $jsonArray2a['User_Parent'];
                $User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
                echo 'var creditsRemaining ='. $User_DL_Reserves;
            }
        }
        else{
            echo "var loggedIn = false;\n";
            echo "var subscriptionType = 0;\n";
            echo "var creditsRemaining = 0;\n";
        }
    ?>
    </script>
    
    <?php
    $footer_js = '<script src="js/jquery.colorbox-min.js"></script>
    <script src="js/bootstrapSwitch.js"></script>
    <script src="js/jquery.nouislider.min.js"></script>
    <script src="/js/search-calc.js"></script>

    <script>
    var priceData = {
        "Num_Matched_Officials": "' . $importCount . '",         
    };
    updatePrice(priceData);
    $(".importCount").text(addCommas($(".importCount").text()));
    </script>';?>

<?php include("inc/footer.php"); ?>            