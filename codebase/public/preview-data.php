<?php

include("inc/config.php");
		
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error()); 
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error()); 

$useCache = $_REQUEST['uc'];

$searchParams = $_SESSION['lastSearchParams'];
$currentTableID = $_SESSION['currentTableID'];
//echo("DEBUG: searchParams = $searchParams<br>");
//echo("DEBUG: currentTableID = $currentTableID<br>");

//$searchParams = 'byZipcode=&byZipcodeDistance=&submit=Search+Power+Almanac';
if ($useCache != 1) {
	$pdo3 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
	$pdo3->query(sprintf("CALL SearchDetails(25,'%s',@Result)", escape($currentTableID)));
	$pdoObject = $pdo3->query("SELECT @Result");
	$rsArray = $pdoObject->fetchAll();
	$json = $rsArray[0]['@Result'];
	
	//print_r($json);
} else {
	$sql ="USE " . escape($dbname);
	$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
	if (!$dbcnx) {
		print("Unable to connect to the database server at this time.\n");
		exit();
	}
	if (!@PowerAlmanac\PDb::query($sql)){
		die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
	}
	$read_sql = "SELECT * FROM database_summary
		LIMIT 1
		";
	$result_read = @PowerAlmanac\PDb::query($read_sql);
	if (!$result_read) {
		die("Error reading from database_summary database: $read_sql" . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_read);
	$json = $onerow['JSON_PREVIEW'];
	
	//print_r($json);
}

// First, replace UTF-8 characters.
$json = str_replace(
 array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6", "\xc3\x82"),
 array("'", "'", '"', '"', '-', '--', '...', ''),
 $json);
// Next, replace their Windows-1252 equivalents.
$json = str_replace(
 array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)),
 array("'", "'", '"', '"', '-', '--', '...'),
 $json);

//print_r($json);

//$json = str_replace('Â','',$json);

$jsonArray = json_decode($json, TRUE);
//echo("jsonArray<br>");
//print_r($jsonArray);

//print_r($json_last_error());

switch(json_last_error())
{
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded';
	break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found';
	break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON';
	break;
	case JSON_ERROR_NONE:
		//echo ' - No errors';
	break;
}

//print_r($jsonArray); exit;

$stateArray['AL'] = "ALABAMA";
$stateArray['AK'] = "ALASKA";
$stateArray['AZ'] = "ARIZONA";
$stateArray['AR'] = "ARKANSAS";
$stateArray['CA'] = "CALIFORNIA";
$stateArray['CO'] = "COLORADO";
$stateArray['CT'] = "CONNECTICUT";
$stateArray['DE'] = "DELAWARE";
$stateArray['DC'] = "DISTRICT OF COLUMBIA";
$stateArray['FL'] = "FLORIDA";
$stateArray['GA'] = "GEORGIA";
$stateArray['HI'] = "HAWAII";
$stateArray['ID'] = "IDAHO";
$stateArray['IL'] = "ILLINOIS";
$stateArray['IN'] = "INDIANA";
$stateArray['IA'] = "IOWA";
$stateArray['KS'] = "KANSAS";
$stateArray['KY'] = "KENTUCKY";
$stateArray['LA'] = "LOUISIANA";
$stateArray['ME'] = "MAINE";
$stateArray['MD'] = "MARYLAND";
$stateArray['MA'] = "MASSACHUSETTS";
$stateArray['MI'] = "MICHIGAN";
$stateArray['MN'] = "MINNESOTA";
$stateArray['MS'] = "MISSISSIPPI";
$stateArray['MO'] = "MISSOURI";
$stateArray['MT'] = "MONTANA";
$stateArray['NE'] = "NEBRASKA";
$stateArray['NV'] = "NEVADA";
$stateArray['NH'] = "NEW HAMPSHIRE";
$stateArray['NJ'] = "NEW JERSEY";
$stateArray['NM'] = "NEW MEXICO";
$stateArray['NY'] = "NEW YORK";
$stateArray['NC'] = "NORTH CAROLINA";
$stateArray['ND'] = "NORTH DAKOTA";
$stateArray['OH'] = "OHIO";
$stateArray['OK'] = "OKLAHOMA";
$stateArray['OR'] = "OREGON";
$stateArray['PA'] = "PENNSYLVANIA";
$stateArray['RI'] = "RHODE ISLAND";
$stateArray['SC'] = "SOUTH CAROLINA";
$stateArray['SD'] = "SOUTH DAKOTA";
$stateArray['TN'] = "TENNESSEE";
$stateArray['TX'] = "TEXAS";
$stateArray['UT'] = "UTAH";
$stateArray['VT'] = "VERMONT";
$stateArray['VA'] = "VIRGINIA";
$stateArray['WA'] = "WASHINGTON";
$stateArray['WV'] = "WEST VIRGINIA";
$stateArray['WI'] = "WISCONSIN";
$stateArray['WY'] = "WYOMING";

?>
<html>
<head>
<title>Search Results Data Preview</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="popup.css" type="text/css">
<!-- <script type="text/javascript">try{Typekit.load();}catch(e){}</script> -->
<!-- <script type="text/javascript" src="popuphelp.js"></script> -->
<style>
	table tr td{
		border-right: 1px solid #ddd;
		border-bottom: 1px solid #ddd;
		padding: 2px;
	}
</style>
</head>

<body style="background-color: white;">

<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" >
<tr>
	<td colspan="12" style="padding: 0px;">
    <h3 style="color: black; margin: 8px 0px">PREVIEW of 25 randomly selected records from YOUR SEARCH RESULTS</h3>
    <!-- <a href="#" onClick="window.parent.Shadowbox.close()"><img src="images/preview-close.jpg" alt="Close Preview" width="40" height="40" hspace="5" vspace="0" border="0" align="right"></a>
    <br>
    <a href="#" onClick="window.parent.Shadowbox.close()">Close Preview</a> -->
	</td>
</tr>
<!--
<tr>
	<td colspan="12" align="center">
    <?php
	/*
	if ($top_elected_flag == '1') {
		echo("<b>Results include additional 'Top Elected Official' for Local Governments</b>");
	}
	//echo("<b>top_elected_flag = $top_elected_flag</b>");
	*/
	?>
    </td>
</tr>
-->
<tr bgcolor="#CCCCCC">
	<td colspan="4" style="background-color: #1998ca; color: black; padding: 8px;">
	<b>Local Government</b>
	</td>
	<td colspan="3" style="background-color: #FFCC00; color: black; padding: 8px;">
	<b>Official</b>
	</td>
	<td colspan="3" style="background-color: #FFF; color: black; padding: 8px;">
	<b>Contact Information</b>
    <br>
    <img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle"> <font class="previewhdrSmall">= included in download</font>
	</td>
</tr>
<tr height="15" style="background: #45484d; /* Old browsers */
	background: -moz-linear-gradient(top, #45484d 0%, #000000 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#45484d), color-stop(100%,#000000)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #45484d 0%,#000000 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #45484d 0%,#000000 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #45484d 0%,#000000 100%); /* IE10+ */
	background: linear-gradient(to bottom, #45484d 0%,#000000 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 ); /* IE6-9 */">
	<td>
	<font color="#FFFFFF">Name</font>
	</td>
	<td>
	<font color="#FFFFFF">Type</font>
	</td>
	<td>
	<font color="#FFFFFF">State<br />Location</font>
	</td>
	<td>
	<font color="#FFFFFF">Community<br />Population</font>
	</td>
	<td>
	<font color="#FFFFFF">Name</font>
	</td>
	<td>
	<font color="#FFFFFF">Role</font>
	</td>
	<td>
	<font color="#FFFFFF">Title</font>
	</td>
	<td>
	<font color="#FFFFFF">Address</font>
	</td>
	<td>
	<font color="#FFFFFF">Phone</font>
	</td>
	<td>
	<font color="#FFFFFF">Email</font>
	</td>
</tr>
<?php
	foreach ($jsonArray as $item) {
		$haveAddress = '';
		$havePhone = '';
		$haveEMail = '';
		//$Govt_Off_ID = $item['Govt_Off_ID'];
		$Government_ID = $item['Government_ID'];
		$Government_Type = $item['Government_Type'];
		$Government_Place_Name = $item['Government_Place_Name'];
		$Population = $item['Population'];
		$Form_of_Government = $item['Form_of_Government'];
		$County_Name = $item['County_Name'];
		//$Mailing_State = $item['Mailing_State'];
		//$State = ucwords(strtolower($stateArray[$Mailing_State]));
		$StateAbbr = $item['State'];
		$State = ucwords(strtolower($stateArray[$StateAbbr]));
		$County_Name = $item['County_Name'];
		$Role = $item['Role'];
		/*
		$First_Name_length = $item['First_Name_length'];
		$Last_Name_length = $item['Last_Name_length'];
		$maskedFName = '';
		$maskedLName = '';
		for ($i = 1; $i <= $First_Name_length; $i++) {
			$maskedFName = $maskedFName . '*';
		}
		for ($i = 1; $i <= $Last_Name_length; $i++) {
			$maskedLName = $maskedLName . '*';
		}
		*/
		$First_Name = $item['First_Name'];
		$Last_Name = $item['Last_Name'];
		$Fullname = $First_Name . ' ' . $Last_Name;
		// mask name
		$numCharsFirst = strlen($First_Name);
		$maskedFName = '';
		for ($i = 1; $i <= $numCharsFirst; $i++) {
			$maskedFName = $maskedFName . '*';
		}
		$numCharsLast = strlen($Last_Name);
		$maskedLName = '';
		for ($i = 1; $i <= $numCharsLast; $i++) {
			$maskedLName = $maskedLName . '*';
		}
		$maskedFullname = $maskedFName . ' ' . $maskedLName;
		$Government_Title = str_replace(" / ","/",ucwords(strtolower(str_replace("/"," / ",$item['Government_Title']))));
		// check for existence of these
		/*
		$Email_Address = $item['Email_Address'];
		$Phone_Number = $item['Phone_Number'];
		$Mailing_Street_Box = $item['Mailing_Street_Box'];
		$posEmail = strpos($Email_Address,'@');
		if ($posEmail !== false) 
		{
			$haveEMail = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		if ($Phone_Number != 'n/a') {
			$havePhone = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		if ($Mailing_Street_Box != 'n/a') {
			$haveAddress = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		*/
		$Mailing_Street_Box_exists = $item['Mailing_Street_Box_exists'];
		$Phone_Number_exists = $item['Phone_Number_exists'];
		$Email_Address_exists = $item['Email_Address_exists'];
		if ($Mailing_Street_Box_exists == 1) {
			$haveAddress = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		if ($Phone_Number_exists == 1) {
			$havePhone = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		if ($Email_Address_exists == 1) {
			$haveEMail = '<img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle">';
		}
		// clean up
		$Government_Place_NameCLEANED = ucwords(strtolower($Government_Place_Name));
		$Government_TypeCLEANED = ucwords($Government_Type);
		$PopulationCLEANED = number_format($Population);
		echo("<tr><td>$Government_Place_NameCLEANED</td><td>$Government_TypeCLEANED</td><td>$State</td><td>$PopulationCLEANED</td><td>$maskedFullname</td><td>$Role</td><td>$Government_Title</td><td>$haveAddress</td><td>$havePhone</td><td>$haveEMail</td></tr>");
	}
?>
<!--
<tr>
	<td colspan="12" >
    <p>
    <b>KEY:</b><br>
    <img src="images/checkmark.png" alt="Available" width="15" height="15" hspace="0" vspace="0" border="0" align="absmiddle"> = included in download
    </p>
    </td>
</tr>
-->
<tr>
	<td colspan="12" >
    <br>
	</td>
</tr>
</table>


</body>
</html>
