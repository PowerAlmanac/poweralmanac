<?php

include("inc/config.php");
$title = 'Matched Local Government';

$govtID = $_REQUEST['id'];
$state = $_REQUEST['state'];
$city = $_REQUEST['city'];

//print_r($_REQUEST); echo("govtID=$govtID");exit;

if (($govtID == 'undefined') || ($govtID == '')){
	header("Location: PA-GetProfile.php?city=$city&state=$state");
	flush();
}

if ($useReCaptcha == 0) {
	header("Location: PA-GovernmentProfileNEW.php?id=$govtID");
	flush();
}

$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

$read_sql = sprintf("SELECT DISTINCT Government_Place_Name,Government_ID,Government_Type_Name,State,County_Name FROM all_data
	WHERE government_id = '%s'
	LIMIT 1
", escape($govtID));
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die('Error reading from codes_table database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$Government_Place_Name = $onerow['Government_Place_Name'];
$Government_Type_Name = $onerow['Government_Type_Name'];
$State = $onerow['State'];
$County_Name = $onerow['County_Name'];

if ($County_Name != '') {
	$header = "$Government_Place_Name ($Government_Type_Name) in COUNTY: $County_Name";
} else {
	$header = "$Government_Place_Name ($Government_Type_Name)";
}
	
	
?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
	<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'none';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstripstatic.png" valign="top">
            <table cellpadding="5" cellspacing="5" border="0" align="center" width="95%">
            <tr>
                <td colspan="2">
                <h3>
                Local Government Matched: <?php echo("$header"); ?>
                </h3>
                <p>
                To go to your profile, please enter the two-word captcha to verify that you are a human!
                </p>
                <form method="post" action="PA-GovernmentProfileNEW.php?id=<?php echo($govtID); ?>">
                <?php
                require_once('recaptchalib.php');
                echo recaptcha_get_html($recaptcha_publickey);
                ?>
                <input type="submit" />
                </form>
                </td>
            </tr>
            <tr>
            	<td colspan="2">
                <font color="#FF0000">
                <?php
				if ($_SESSION['captcha_message'] != '') {
					echo($_SESSION['captcha_message']);
				}
				?>
                </font>
                </td>
            </tr>
            </table>
			</td>
		</tr>
		<?php
        if ($_SESSION['logged_in'] != '1') {
            // not logged in
            echo('<tr><td colspan="2" align="center" background="images/canvasbgstripstatic.png"><img src="images/divider.jpg" width="1024" height="15" vspace="15" align="absmiddle"></td></tr>');
            echo('<tr><td colspan="2" background="images/canvasbgstripstatic.png">');
            //include("PA-inc-whypa.php");
            echo('</td></tr>');
        }
        ?>
        <tr>
            <td colspan="2" align="center" background="images/canvasbgstripstatic.png">
            <?php
            include("inc/oldfooter.php");
            ?>
            </td>
        </tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstripstatic.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
<br />
</body>
</html>
