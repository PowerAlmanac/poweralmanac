<?php
include("inc/config.php");
$title = 'Sign up for a free account | Power Almanac';
include("inc/header-search.php");
?>
            <div class="intro">
                <div class="intro-holder center login">
                    <h1 class="display-4">Sign up for an account</h1><a href="./pricing" class="d-none d-sm-inline">See Other Plans</a>
                    <?php
	                    if (isset($_SESSION['subscribe_message'])) {
	                       echo '<p class="error-warning">' . $_SESSION['subscribe_message'] . '</p>';
	                      
	                    } else {
	                        $_SESSION['subscribe_message'] = '';
	                    }
                    ?>
                     <div class="black-holder clearfix">
	                	<form action="PA-RegisterStep2.php" method="post" name="applyform">
	                	<div class="divider d-none d-sm-block">
		                	<label class="halfwidth">
		                		First Name
		                		<input type="text" class="form-control" name="firstName">
		                	</label>
		                	<label class="halfwidth">
		                		Last Name
		                		<input type="text" class="form-control" name="lastName">
		                	</label>
		                </div>
		                <label class="d-none d-sm-block">
	                		Phone
	                		<input type="text" class="form-control" name="phone" value="">
	                	</label>
	                	<label>
	                		Email
	                		<input type="email" class="form-control" name="eMail" value="" required>
	                	</label>
	                	<div class="divider">
		                	<label>
		                		Password
		                		<input type="password" class="form-control" name="passWord" required>
		                	</label>
		                	<label class="d-none d-sm-block">
		                		Confirm Password
		                		<input type="password" class="form-control" name="passWord2">
		                	</label>
		                </div>
	                	<div id="login-or-register">
	                		<label class="d-inline">Account Type: &nbsp;&nbsp;
		                		<select id="subpicker" name="register-account" class="form-control d-inline">
		                			<option value="11" selected>Power Entry - Free!</option>
		                			<option value="12">Power 15 - 15,000 records - $4,500</option>
                                    <option value="13">Power 75 - 75,000 records - $10,500</option>
		                			<option value="10">Power Max - 223,000+ records - $14,700</option>
		                		</select>
	                		</label>

	                		<label class='checkbox'><input type='checkbox' name='AgreeToTOS' value='1'> I agree to the <a target='_blank' style='float: none; margin-left: 0' href='terms-of-service.php'>Terms of Service</a></label>
                            <div class="g-recaptcha" data-sitekey="<?php echo $recaptcha_publickey ?>"></div>
	                		<input style='font-size: 18px;' type="submit" value="Create Account" class="btn btn-primary btn-lg btn-block mt-1"><span> or <a href="./login">Login with existing account</a></span>
	                	</div>
	                	</form>
	                </div>
                    <script>

                    	var dataArray = <?php
                    		if(isset($_SESSION["temp_register"]))
                    		{
                    			$sess_register = $_SESSION['temp_register'];
                    			echo("JSON.parse('$sess_register');");
                    		}else{
                    			echo("'';");
                    		}
                    	?>

                        <?php if(isset($_SESSION['register-upgrade']))
                        {
                            $subtype = $_SESSION['register-upgrade'];
                            echo 'var subType = "' . $subtype . '";';
                        }
                            ?>

                        <?php if(isset($_GET['subscription_level']))
                        {
                            $subtype = preg_replace('/[^0-9]/', '', $_GET['subscription_level']);
                            echo 'var subType = "' . $subtype . '";';
                        }
                            ?>

                    	$(function(){
                    		// $('input[name="firstName"]').val(dataArray['first']);
                    		// $('input[name="lastName"]').val(dataArray['last']);
                    		$('input[name="eMail"]').val(dataArray['email']);

                    		if(typeof subType !== 'undefined'){
                    			$('#subpicker option').each(function(){
                    				if($(this).val() == subType)
                    				{
                    					$(this).prop('selected', true);
                    				}
                    			})
                    		}
                    	});
                    </script>
                </div>
            </div> 

<?php include("inc/footer.php"); ?>
