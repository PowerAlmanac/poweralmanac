<?php
session_start();
//print_r($_REQUEST); exit;

$off_email = $_REQUEST['off_email'];
$searchParams = $_SESSION['lastSearchParams'];

// check which boxes and set/unset session variables
if ($off_email == '1') {
	// only govt officials with emails
	$_SESSION['results_GovtOffMustHaveEmails'] = '1';
	$searchParams = $searchParams . '&off_email=govtOff_email';
} else {
	// all govt officials
	$_SESSION['results_GovtOffMustHaveEmails'] = '0';
	// reset searchParms
	$searchParams = str_replace('&off_email=govtOff_email','',$searchParams);
}

//print_r($searchParams); exit;

// re-run search
$_SESSION['useSavedSearchParams'] = '1';
$_SESSION['savedSearchParams'] = $searchParams;
header("Location: search-government.php");
flush();

?>
