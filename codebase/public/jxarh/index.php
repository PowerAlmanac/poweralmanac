<?php
include("inc-config.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Power Almanac Database Management System</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>

<script language="javascript" type="text/javascript" src="niceforms.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="niceforms-default.css" />

<meta name="robots" content="noindex">
</head>

<body>
<center>
<div id="users-contain" class="ui-widget">

<h1>Power Almanac Database Management System</h1>

<?php
include("inc-help.php");
?>
    
</div>
<br />
</center>
<div id="container">
<form action="login.php" method="post" class="niceform">
	<fieldset>
    	<legend>Log In</legend>
        <dl>
        	<dt><label for="userid">User Name:</label></dt>
            <dd><input type="text" name="userid" id="userid" size="40" maxlength="128" value="" /></dd>
        </dl>
        <dl>
        	<dt><label for="password">Password:</label></dt>
            <dd><input type="password" name="password" id="password" size="40" maxlength="128" value="" /></dd>
        </dl>
    </fieldset>
    <fieldset class="action">
    	<input type="submit" name="submit" id="submit" value="Submit" />
    </fieldset>
</form>
<center>

<?php
$errorMessage = $_SESSION['error_message'];
echo("<font color='#FF0000'>$errorMessage</font>");
?>

</center>

</div>

</body>
</html>