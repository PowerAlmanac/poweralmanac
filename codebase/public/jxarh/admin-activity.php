<?php
include("inc-config.php");
include("inc-protected-admin.php");

// dates
$show = $_REQUEST['show'];
$date_start = $_REQUEST['datestart'];
$date_end = $_REQUEST['dateend'];
if ($show == '') $show = 1;
// get all users
$read_sql = "SELECT * FROM employees
		WHERE access_level != 'Administrator'
	";
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Access - Activity</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<meta name="robots" content="noindex">

<script>
$(function() {
	$( "#datestart" ).datepicker({
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "img/calendar.gif",
		buttonImageOnly: true
	});
	$( "#dateend" ).datepicker({
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "img/calendar.gif",
		buttonImageOnly: true
	});
});
</script>

</head>

<body>

<div class="demo">

<center>
<?php
// navigation
include("nav-admin.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Activity</h1>
    <?php
    $today = strtotime(date('M j, Y'));
	$yesterday = $today - 86400;
	$yesterdayFMTD = date("Y-m-d",$yesterday);
    ?>
	<table id="users" class="ui-widget ui-widget-content">
		<thead>
        <?php
		//$today = strtotime(date('M j, Y'));
		//$yesterday = $today - 86400;
	
		$yesterdayStart = date("Y-m-d",$yesterday) . ' 00:00:00';
		$yesterdayEnd = date("Y-m-d",$yesterday) . ' 23:59:59';
			
		$todayStart = date("Y-m-d") . ' 00:00:00';
		$todayEnd = date("Y-m-d") . ' 23:59:59';
		
		if ($show == '1') {
			$dateStart = $yesterdayStart;
			$dateEnd = $yesterdayEnd;
			$dateStart_display = date("Y-m-d",$yesterday);
			$dateEnd_display = date("Y-m-d",$yesterday);
		} else {
			$dateStart = $date_start . ' 00:00:00';
			$dateEnd = $date_end . ' 23:59:59';
			$dateStart_display = $date_start;
			$dateEnd_display = $date_end;
		}
		?>
			<tr>
				<th colspan="2" bgcolor='#42D0FF'><a href="?show=1">Yesterday</a> (<?php echo($yesterdayFMTD); ?>)</th>
                <form action="admin-activity.php" method="get">
                <input type="hidden" name="show" value="2" />
				<th colspan="2">Start Date: <input name="datestart" type="text" id="datestart" value="<?php echo($dateStart_display); ?>"  style="padding-right:5px;padding-left:5px;" /></th><th colspan="2">End Date: <input name="dateend" type="text" id="dateend" value="<?php echo($dateEnd_display); ?>"  style="padding-right:5px;padding-left:5px;" /></th><th><input type="submit" value="Refresh Activity" /></th>
                </form>
			</tr>
			<tr class="ui-widget-header">
				<th width="100">Employee ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>User ID</th>
				<th>Primary Role</th>
                <th># Log Ins</th>
                <th># Hours Logged ON</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			$emp_id = $row['emp_id'];
			$first_name = $row['first_name'];
			$last_name = $row['last_name'];
			$user_id = $row['user_id'];
			$primary_role = str_replace('QualityChecker','Quality Checker',str_replace('FinalApprover','Final Approver',str_replace('VoiceAgent','Voice Agent',$row['primary_role'])));
			

			
			$readUser_sql = sprintf("SELECT * FROM valog
					WHERE VA_ID = '%s' AND timestamp >= '%s' AND timestamp <= '%s'
				", $emp_id, $dateStart, $dateEnd);
			$result_readUser = @PowerAlmanac\PDb::query($readUser_sql);
			if (!$result_readUser) {
				die("Error reading from $dbname database: $readUser_sql" . PowerAlmanac\PDb::error());
			}
			// debug
			//echo("<tr><td colspan='9'>$readUser_sql</td></tr>");
			
			$numLogIn = 0;
			$startSession = 0;
			$endSession = 0;
			$totalSession = 0;
			while ($logrow = PowerAlmanac\PDb::fetch_array($result_readUser))
			{
				$timestamp = $logrow['timestamp'];
				$action = $logrow['action'];
				switch($action) {
					case 'login':
						$numLogIn = $numLogIn + 1;
						$startSession = strtotime($timestamp);
						break;
					case 'logout':
						$endSession = strtotime($timestamp);
						//echo("<tr><td colspan='9'>$totalSession, $endSession, $startSession</td></tr>");
						if ($startSession != 0) {
							$totalSession = $totalSession + ($endSession - $startSession);
						}
						break;
					default:
						break;
				}
				//echo("<tr><td><b>$emp_id</b></td><td><b>$first_name</b></td><td><b>$last_name</b></td><td><b>$user_id</b></td><td>$primary_role</td><td>$timestamp</td><td>$action</td></tr>");
			}
			$totalSessionHrs = number_format($totalSession/3600,2);
			echo("<tr><td><b>$emp_id</b></td><td><b>$first_name</b></td><td><b>$last_name</b></td><td><b>$user_id</b></td><td>$primary_role</td><td>$numLogIn</td><td>$totalSessionHrs</td></tr>");
		}
		?>
		</tbody>
	</table>
</div>

</center>

</div>

</body>
</html>