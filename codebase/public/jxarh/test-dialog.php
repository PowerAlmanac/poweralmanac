<?php

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Voice Agent - Edit Government</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<script>
	$(function() {
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
	$( "input:submit" ).button();
		
	$( "#open-qcNote1" )
	  .button({
            icons: {
                primary: "ui-icon-plus"
            }
        })
	  .click(function() {
		  $( "#dialog-form1" ).dialog( "open" );
	  });
	  
	var gid1 = $( "#gid" ),
		jid1 = $( "#jid" ),
		note1 = $( "#note" );
			
	$( "#dialog-form1" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Add Disposition": function() {
				//alert(gid.val());
				//alert(jid.val());
				//alert(note.val());
				
				$.ajax({
					type: "POST",
					url: "voiceagent-add-notes.php",
					data: "gid=" + gid1.val() + "&jid=" + jid1.val() + "&note=" + note1.val(),
					success: function(msg){
						window.location = 'voiceagent-editgovt.php?gid='  + gid1.val() + '&jid=' + jid1.val();
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});
	
</script>

<meta name="robots" content="noindex">
</head>

<body>

	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var gid1 = $( "#gid" ),
			jid1 = $( "#jid" ),
			note1 = $( "#note" );

		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				"Add Disposition": function() {
					//alert(gid.val());
					//alert(jid.val());
					//alert(note.val());
					
					$.ajax({
						type: "POST",
						url: "voiceagent-add-notes.php",
						data: "gid=" + gid1.val() + "&jid=" + jid1.val() + "&note=" + note1.val(),
						success: function(msg){
							window.location = 'voiceagent-editgovt.php?gid='  + gid1.val() + '&jid=' + jid1.val();
						}
					});
			},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#add-disposition" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});
	});
	</script>



<div class="demo2">

<div id="dialog-form" title="Add Disposition">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to this Government</label>
        <textarea name="note" id="note" cols="30" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

<button id="add-disposition">Add Disposition</button>

</div><!-- End demo -->

</body>
</html>