<?php
include("inc-config.php");
include("inc-protected-supervisor.php");

$gid = $_REQUEST['gid'];
$jid = $_REQUEST['jid'];

$empid = $_SESSION['emp_id'];

// get job detail
$read_sql = sprintf("SELECT * FROM jobs
		WHERE job_id = '%s'
		LIMIT 1
	", escape($jid));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$job_id = $onerow['job_id'];
$va_id = $onerow['va_id'];
$num_officials = $onerow['num_officials'];
$num_govs = $onerow['num_govs'];
$job_state = $onerow['state'];

// check if job state is same as primary role
$primary_role = $_SESSION['primary_role'];

if (($job_state == 'qc') && ($primary_role != 'QualityChecker')) {
	// error
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

if (($job_state == 'fa') && ($primary_role != 'FinalApprover')) {
	// error
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

$job_table = $job_state . '_' . $job_id;

//echo("DEBUG: job_table = $job_table");

// check if govt id is done
$readgovtoff_sql = sprintf("SELECT * FROM %s WHERE Government_ID = '%s'
	", escape($job_table), escape($gid));
$result_readgovtoff = @PowerAlmanac\PDb::query($readgovtoff_sql);
if (!$result_readgovtoff) {
	die("Error reading from job_table database: $readgovtoff_sql" . PowerAlmanac\PDb::error());
}
$num_officials = PowerAlmanac\PDb::num_rows($result_readgovtoff);

if ($num_officials == '0') {
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

$checkSQL = sprintf("show tables from %s like '%s'", escape($dbname), escape($job_table));
$result_checkTable = @PowerAlmanac\PDb::query($checkSQL);
if (!$result_checkTable) {
	die('Error reading from $dbname database:' . PowerAlmanac\PDb::error());
}
$numTables = number_format(PowerAlmanac\PDb::num_rows($result_checkTable));
if ($numTables > 0) {
	// found
	$errorMsg = '';
	
	// govt info
	$readgovtInfo_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		LIMIT 1
		", escape($job_table), escape($gid));
	$result_readgovtInfo = @PowerAlmanac\PDb::query($readgovtInfo_sql);
	if (!$result_readgovtInfo) {
		die("Error reading from $job_table database: $read_sql" . PowerAlmanac\PDb::error());
	}
	$govtinforow = PowerAlmanac\PDb::fetch_array($result_readgovtInfo);
	$Government_Type = $govtinforow['Government_Type'];
	$Government_ID = $govtinforow['Government_ID'];
	$Government_Place_Name = $govtinforow['Government_Place_Name'];
	$Government_Type_Name = $govtinforow['Government_Type_Name'];
	$County_Name = $govtinforow['County_Name'];
	$state = $govtinforow['state'];
	$Population = $govtinforow['Population'];
	$Government_PhoneNumber = $govtinforow['Government_PhoneNumber'];
	$Government_PhoneNumber_FMTD = substr($Government_PhoneNumber,0,3) . '-' .  substr($Government_PhoneNumber,3,3) .  '-' . substr($Government_PhoneNumber,6,4);
	$Government_Web_Address = $govtinforow['Government_Web_Address'];
	$Address_Street_Box = $govtinforow['Address_Street_Box'];
	$City = $govtinforow['City'];
	$Zip_Code = $govtinforow['Zip_Code'];
	$Electron_Month = $govtinforow['Electron_Month'];
	$Taken_from = $govtinforow['Taken_from'];
	$Updated_website = $govtinforow['Updated_website'];
			
	// govt officials
	$readgovtOfficials_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		ORDER BY Official_ID
		", escape($job_table), escape($gid));
	$result_readgovtOfficials = @PowerAlmanac\PDb::query($readgovtOfficials_sql);
	if (!$result_readgovtOfficials) {
		die("Error reading from $job_table database: $read_sql" . PowerAlmanac\PDb::error());
	}
} else {
	// does not exist - must have moved to DONE
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

// text
switch($job_state) {
	case 'va':
		$job_state_description = 'Voice Agent';
		break;
	case 'qc':
		$job_state_description = 'Quality Check';
		break;
	case 'fa':
		$job_state_description = 'Final Approval';
		break;
	case 'dome':
		$job_state_description = 'Ready To Upload';
		break;
	default:
		$job_state_description = 'Unknown';
		break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supervisor - Edit Government</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />
<script>
	$(function() {
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( ".ready a" ).button({
            icons: {
                primary: "ui-icon-plus"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-minus"
			}
        }).next().button({
            icons: {
                primary: "ui-icon-circle-arrow-s"
			}
        });
		
	$( "input:submit" ).button();

	$( ".hidebtn a" ).button({
            icons: {
                primary: "ui-icon-locked"
            },
            text: false
        });
	
	var gid1 = $( "#gid" ),
		jid1 = $( "#jid" ),
		note1 = $( "#note" );
			
	$( "#dialog-form1" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Send Note": function() {
				//alert(gid.val());
				//alert(jid.val());
				//alert(note1.val());
				var encodedNote = encodeURIComponent(note1.val());
				//var allparams = "gid=" + gid1.val() + "&jid=" + jid1.val() + "&note=" + encodedNote;
				//alert(allparams);
				
				// url: "supervisor-qc-sendback2va.php",
				// url: "test-note.php",
				$.ajax({
					type: "POST",
					url: "supervisor-qc-sendback2va.php",
					data: "gid=" + gid1.val() + "&jid=" + jid1.val() + "&note=" + encodedNote,
					success: function(msg){
						window.location = 'dashboard-supervisor.php';
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});

	var gid2 = $( "#gid" ),
		jid2 = $( "#jid" ),
		note2 = $( "#note" );
		
	$( "#dialog-form2" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Send Note": function() {
				//alert(gid.val());
				//alert(jid.val());
				//alert(note.val());
				var encodedNote = encodeURIComponent(note2.val());
				$.ajax({
					type: "POST",
					url: "supervisor-fa-sendback2va.php",
					data: "gid=" + gid2.val() + "&jid=" + jid2.val() + "&note=" + encodedNote,
					success: function(msg){
						//indow.location = 'supervisor-editgovt.php?gid=' + gid2.val() + '&jid=' + jid2.val();
						window.location = 'dashboard-supervisor.php';
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});

	var gid3 = $( "#gid" ),
		jid3 = $( "#jid" ),
		note3 = $( "#note" );
		
	$( "#dialog-form3" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Send Note": function() {
				//alert(gid3.val());
				//alert(jid3.val());
				//alert(note3.val());
				var encodedNote = encodeURIComponent(note3.val());
				$.ajax({
					type: "POST",
					url: "supervisor-fa-sendback2qc.php",
					data: "gid=" + gid3.val() + "&jid=" + jid3.val() + "&note=" + encodedNote,
					success: function(msg){
						//window.location = 'supervisor-editgovt.php?gid=' + gid3.val() + '&jid=' + jid3.val();
						window.location = 'dashboard-supervisor.php';
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});
	
	$( "#open-qcNote1" )
	  .button({
            icons: {
                primary: "ui-icon-minus"
            }
        })
	  .click(function() {
		  $( "#dialog-form1" ).dialog( "open" );
	  });
	  
	$( "#open-faNote1" )
	  .button({
            icons: {
                primary: "ui-icon-minus"
            }
        })
	  .click(function() {
		  $( "#dialog-form2" ).dialog( "open" );
	  });
	  
	$( "#open-faNote2" )
	  .button({
            icons: {
                primary: "ui-icon-minus"
            }
        })
	  .click(function() {
		  $( "#dialog-form3" ).dialog( "open" );
	  });
	  
	});
	  
	$( "#dialog-zipmessage" ).dialog({
		modal: true,
		autoOpen: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});
		
</script>
	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var gidN = $( "#gidN" ),
			empidN = $( "#empidN" ),
			jidN = $( "#jidN" ),
			noteN = $( "#noteN" );

		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				"Add Disposition": function() {
					//alert(gid.val());
					//alert(jid.val());
					//alert(note.val());
					//alert(empid.val());
					$.ajax({
						type: "POST",
						url: "voiceagent-add-disposition.php",
						data: "gid=" + gidN.val() + "&jid=" + jidN.val() + "&note=" + noteN.val() + "&emp_id=" + empidN.val(),
						success: function(msg){
							window.location = 'supervisor-editgovt.php?gid='  + gidN.val() + '&jid=' + jidN.val();
						}
					});
			},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#add-disposition" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});
			
		var gidN2 = $( "#gidN" ),
			empidN2 = $( "#empidN" ),
			jidN2 = $( "#jidN" ),
			noteN2 = $( "#noteN" );

		$( "#dialog-formnotes" ).dialog({
			autoOpen: false,
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				"Add Notes": function() {
					//alert(gid.val());
					//alert(jid.val());
					//alert(note.val());
					//alert(empid.val());
					var encodedNote = encodeURIComponent(noteN2.val());
					$.ajax({
						type: "POST",
						url: "voiceagent-add-notes.php",
						data: "gid=" + gidN2.val() + "&jid=" + jidN2.val() + "&note=" + encodedNote + "&emp_id=" + empidN2.val(),
						success: function(msg){
							window.location = 'supervisor-editgovt.php?gid='  + gidN2.val() + '&jid=' + jidN2.val();
						}
					});
			},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#add-notes" )
			.button()
			.click(function() {
				$( "#dialog-formnotes" ).dialog( "open" );
			});
			
	});
	</script>
<script type="text/javascript">
function showEdit(id) {
	//alert('showEdit start');
	var e = document.getElementById(id);
	e.style.display = 'block';
	//alert('showEdit end');
}
function closeEdit(id) {
	var e = document.getElementById(id);
	e.style.display = 'none';
}
function validateForm(id) {
	//alert('validating form...');
	var mail_zip = document.forms[id]["mail_zip"].value;
	if(mail_zip.length != 5) {
		alert('Invalid Zip Code entered. Please try again.');
		return false;
	}
}
function validateGovtForm() {
	//alert('validating form...');
	var zip = document.forms["editgovt"]["zip"].value;
	if(zip.length != 5) {
		alert('Invalid Zip Code entered. Please try again.');
		return false;
	}
}

function validateZip(inputString) {
	if(inputString.length == 5) {
	$('#foo').addClass('load');
		$.post("validate-zipcode.php", {queryString: ""+inputString}, function(data){
			if(data.length >0) {
				//alert(data);
				if (data == '0') {
					//$( "#dialog-zipmessage" ).dialog( "open" );
					alert('Invalid Zip Code entered. Please try again.');
				}
			}
		});
	}
	if(inputString.length > 5) {
		alert('Invalid Zip Code entered. Please try again.');
	}
}
		
</script>

<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">

<center>
<?php
// navigation
include("nav-supervisor.php");
include("userinfo.php");
?>
</center>

<center>
<div id="wide-contain" class="ui-widget">
	<h1>EDITING: Government #<?php echo($gid); ?> Task #<?php echo($jid); ?></h1>
    Government ID Status: <button><?php echo($job_state_description); ?></button>
	<?php
	$googleArgs = "$Government_Place_Name $Government_Type_Name, $City, $County_Name, $state";
	?>
	<a href="http://www.google.com/search?q=<?php echo($googleArgs); ?>" target="_blank"><img src="img/google.png" alt="google search" width="57" height="20" hspace="5" vspace="0" align="absmiddle" title="google search" /></a>
    <?php
	if (($job_state == 'qc')) {
		// add ready submission
		$confirmURL = "supervisor-qc-confirm.php?gid=$Government_ID&jid=$job_id";
		$sendback2vaURL = "supervisor-qc-sendback2va.php?gid=$Government_ID&jid=$job_id";
		$downloadCSVURL = "qc-generate-csv.php?gid=$Government_ID&jid=$job_id";
		echo("<div class='ready'>");
		echo("<a href='$confirmURL'>Confirm Government</a> ");
		echo('<button id="open-qcNote1">Send Back To VA</button> ');
		echo("<a href='$downloadCSVURL' title='Download CSV for $Government_Place_Name' target='_blank'>Download CSV</a>");
		echo("</div>");
	}
	if (($job_state == 'fa')) {
		// add ready submission
		$confirmURL = "supervisor-fa-confirm.php?gid=$Government_ID&jid=$job_id";
		$sendback2vaURL = "supervisor-fa-sendback2va.php?gid=$Government_ID&jid=$job_id";
		$sendback2qcURL = "supervisor-fa-sendback2qc.php?gid=$Government_ID&jid=$job_id";
		echo("<div class='ready'>");
		echo("<a href='$confirmURL'>Confirm Government</a> ");
		echo('<button id="open-faNote1">Send Back To VA</button> ');
		echo('<button id="open-faNote2">Send Back To QC</button>');
		echo("</div>");
	}
	?>   
    <br />
	<b>Government Information</b>
	<table id="govtinfo" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
           		<th>Action</th>
                <th>Government Type</th>
				<th>Government ID</th>
                <th>Government Place Name</th>
                <th>Government Type Name</th>
				<th>County Name</th>
				<th>State</th>
				<th>Population</th> 
                
				<th>Government Phone Number</th>
                <th>Government Web Address</th>
                <th>Updated</th>
				<th>Street Box</th>
				<th>City</th>
                <th>Zip Code</th>
                <th>Taken From</th>
                <th>Election Month</th>
			</tr>
		</thead>
		<tbody>
        <?php
		// list government info - 8 fields
		// get govt notes
		$readGovtNotes_sql = sprintf("SELECT * FROM notes WHERE Government_ID = '%s' AND Official_ID = '%s'
			ORDER BY Job_ID DESC
			LIMIT 1
			", escape($gid), escape($gid));
		$result_readGovtNotes = @PowerAlmanac\PDb::query($readGovtNotes_sql);
		if (!$result_readGovtNotes) {
			die("Error reading from notes database: $readGovtNotes_sql" . PowerAlmanac\PDb::error());
		}
		$numGovtNotes = PowerAlmanac\PDb::num_rows($result_readGovtNotes);
		if ($numGovtNotes == 0) { // first time
			$notesGovtArray = array(0,0,0,0,0,0,0);
		} else {
			$onerow = PowerAlmanac\PDb::fetch_array($result_readGovtNotes);
			$notes = $onerow['txt'];
			$notesGovtArray = explode(",", $notes);
		}
		// get ORIGINAL data
		$readORIGINALgovtInfo_sql = sprintf("SELECT * FROM full_data
			WHERE Government_ID = '%s'
			LIMIT 1
			", escape($gid));
		$result_readORIGINALgovtInfo = @PowerAlmanac\PDb::query($readORIGINALgovtInfo_sql);
		if (!$result_readORIGINALgovtInfo) {
			die("Error reading from $VAjob_table database: $readORIGINALgovtInfo_sql" . PowerAlmanac\PDb::error());
		}
		$ORIGINALgovtinforow = PowerAlmanac\PDb::fetch_array($result_readORIGINALgovtInfo);
		$o_Government_PhoneNumber = $ORIGINALgovtinforow['Government_PhoneNumber'];
		$o_Government_PhoneNumber_FMTD = substr($o_Government_PhoneNumber,0,3) . '-' .  substr($o_Government_PhoneNumber,3,3) .  '-' . substr($o_Government_PhoneNumber,6,4);
		$o_Government_Web_Address = $ORIGINALgovtinforow['Government_Web_Address'];
		$o_Address_Street_Box = $ORIGINALgovtinforow['Address_Street_Box'];
		$o_City = $ORIGINALgovtinforow['City'];
		$o_Zip_Code = $ORIGINALgovtinforow['Zip_Code'];
		$o_Electron_Month = $ORIGINALgovtinforow['Electron_Month'];
		$o_Taken_from = $ORIGINALgovtinforow['Taken_from'];
		$o_Updated_website = $ORIGINALgovtinforow['Updated_website'];
		
		// tag
		echo("<tr><td><a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Government_ID');\">Edit</a></td><td>$Government_Type</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name</td><td>$County_Name</td><td>$state</td><td>$Population</td><td class='color$notesGovtArray[0]'>$o_Government_PhoneNumber_FMTD</td><td class='color$notesGovtArray[1]'><a href='$o_Government_Web_Address' target='_blank'>$o_Government_Web_Address</a></td><td class='color$notesGovtArray[2]'>$o_Updated_website</td><td class='color$notesGovtArray[3]'>$o_Address_Street_Box</td><td class='color$notesGovtArray[4]'>$o_City</td><td class='color$notesGovtArray[5]'>$o_Zip_Code</td><td class='color$notesGovtArray[6]'>$o_Taken_from</td><td class='color$notesGovtArray[7]'>$o_Electron_Month</td></tr>");
		//echo("<tr><td><a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Government_ID');\">Edit</a></td><td>$Government_Type</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name</td><td>$County_Name</td><td>$state</td><td>$Population</td><td>$Government_PhoneNumber</td><td<a href='$Government_Web_Address' target='_blank'>$Government_Web_Address</a></td><td>$Updated_website</td><td>$Address_Street_Box</td><td>$City</td><td>$Zip_Code</td><td>$Taken_from</td></tr>");
		switch($Updated_website) {
			case 'Yes':
				$updated_yes = ' selected';
				$updated_no = ''; 
				$updated_notsure = ''; 
				break;
			case 'No':
				$updated_yes = ''; 
				$updated_no = ' selected'; 
				$updated_notsure = ''; 
				break;
			default:
				$updated_yes = ''; 
				$updated_no = ''; 
				$updated_notsure = ' selected'; 
				break;
		}
		// initialize
		for ($i = 0; $i <= 12; $i++) {
    			$electionMonthSelect[$i] = '';
		}
		$electionMonthSelect[$Electron_Month] = ' selected';
		//echo("<tr bgcolor='#ffffff'><td colspan='20' align='left'><div id='$Government_ID' style='display:none;'>&nbsp;&nbsp;<a href='JavaScript:void(0);' onclick=\"javascript:closeEdit('$Government_ID');\" style='color:#ff0000;'>Hide/Cancel</a><form action='supervisor-government-edit.php' name='edit' method='post' onSubmit=\"return validateForm()\"><input type='hidden' name='gid' value='$gid' /><input type='hidden' name='jid' value='$jid' /><table id='$Government_ID' width='75%' align='center'><thead><tr class='ui-edit-header '><th>Govt Phone Number</th><th>Government Web Address</th><th>Updated</th><th>Street Box</th><th>City</th><th>Zip Code</th><th>Taken From</th></tr></thead><tbody><tr><td><input type='text' name='phone' value='$Government_PhoneNumber'></td><td><input type='text' name='webaddress' value='$Government_Web_Address' size='30'></td><td><select name='updatedweb'><option value='Yes' $updated_yes>Yes</option><option value='No' $updated_no>No</option></select></td><td><input type='text' name='address' value=\"$Address_Street_Box\"></td><td><input type='text' name='city' value='$City' size='30'></td><td><input type='text' name='zip' value='$Zip_Code' onkeyup='validateZip(this.value);'></td><td><input type='text' name='takenfrom' value=\"$Taken_from\"></td></tr></tbody><tr><td colspan='7' align='center'><input type='submit' value='submit updates'></td></tr></table></form></div></td></tr>");
		echo("<tr bgcolor='#ffffff'><td colspan='20' align='left'><div id='$Government_ID' style='display:none;'>&nbsp;&nbsp;<a href='JavaScript:void(0);' onclick=\"javascript:closeEdit('$Government_ID');\" style='color:#ff0000;'>Hide/Cancel</a><form action='supervisor-government-edit.php' name='editgovt' method='post' onSubmit=\"return validateGovtForm()\"><input type='hidden' name='gid' value='$gid' /><input type='hidden' name='jid' value='$jid' />");
		// previous values
		echo("<input type='hidden' name='p_phone' value='$Government_PhoneNumber_FMTD'>");
		echo("<input type='hidden' name='p_webaddress' value='$Government_Web_Address'>");
		echo("<input type='hidden' name='p_updatedweb' value='$Updated_website'>");
		echo("<input type='hidden' name='p_address' value='$Address_Street_Box'>");
		echo("<input type='hidden' name='p_city' value='$City'>");
		echo("<input type='hidden' name='p_zip' value='$Zip_Code'>");
		echo("<input type='hidden' name='p_takenfrom' value='$Taken_from'>");
		echo("<input type='hidden' name='p_electionmonth' value='$Electron_Month'>");
		// updates
		echo("<table id='$Government_ID' width='75%' align='center'><thead><tr class='ui-edit-header '><th>Govt Phone Number</th><th>Government Web Address</th><th>Updated</th><th>Street Box</th><th>City</th><th>Zip Code</th><th>Taken From</th></tr></thead><tbody><tr><td><input type='text' name='phone' value='$Government_PhoneNumber_FMTD'></td><td><input type='text' name='webaddress' value='$Government_Web_Address' size='30'></td><td><select name='updatedweb'><option value='Yes' $updated_yes>Yes</option><option value='No' $updated_no>No</option><option value='Not Sure' $updated_notsure>Not Sure</option></select></td><td><input type='text' name='address' value=\"$Address_Street_Box\"></td><td><input type='text' name='city' value=\"$City\" size='30'></td><td><input type='text' name='zip' value='$Zip_Code' onkeyup='validateZip(this.value);'></td><td><input type='text' name='takenfrom' value=\"$Taken_from\"></td><td><select name='electionmonth'><option value='0' $electionMonthSelect[0]>Unknown</option><option value='1' $electionMonthSelect[1]>January</option><option value='2' $electionMonthSelect[2]>February</option><option value='3' $electionMonthSelect[3]>March</option><option value='4' $$electionMonthSelect[4]>April</option><option value='5' $electionMonthSelect[5]>May</option><option value='6' $electionMonthSelect[6]>June</option><option value='7' $electionMonthSelect[7]>July</option><option value='8' $electionMonthSelect[8]>August</option><option value='9' $electionMonthSelect[9]>September</option><option value='10' $electionMonthSelect[10]>October</option><option value='11' $electionMonthSelect[11]>November</option><option value='12' $electionMonthSelect[12]>December</option></select></td></tr></tbody><tr><td colspan='8' align='center'><input type='submit' value='submit updates'></td></tr></table></form></div></td></tr>");
		?>
		</tbody>
	</table>
    <br />
    <b>Government Officials</b>
	<table id="govtoff" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
           		<th>Actions</th>
				<th>Role</th>
                <th>Role Exist</th>
                <th>Government Title</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th> 
				<th>Phone Number</th>
                <th>Phone Ext</th>
				<th>Mailing Street Box</th>
				<th>Mailing Suite Number</th>
				<th>Mailing City</th>
                <th>Mailing State</th>
                <th>Mailing Zip Code</th>
                <th>Governing Board?</th>
                <th>Official ID</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_readgovtOfficials))
		{
			// list officials - 12 to 13
			//echo("<!--"); print_r($row); echo("-->");
			$Role = str_replace("/"," / ",$row['Role']);
			//$Role = str_replace("/"," / ",$row['role']);
			$Role_exists = $row['Role_exists'];
			$Government_Title = $row['Government_Title'];
			$First_Name = $row['First_Name'];
			$Last_Name = $row['Last_Name'];
			$Email_Address = $row['Email_Address'];
			$Phone_Number = $row['Phone_Number'];
			// format
			$Phone_Number_FMTD = substr($Phone_Number,0,3) . '-' .  substr($Phone_Number,3,3) .  '-' . substr($Phone_Number,6,4);
			$Phone_Number_Ext = $row['Phone_Number_Ext'];
			$Mailing_Street_Box = $row['Mailing_Street_Box'];
			$Mailing_Suite_Number = $row['Mailing_Suite_Number'];
			$Mailing_City = $row['Mailing_City'];
			$Mailing_State = $row['Mailing_State'];
			$Mailng_Zip_Code = $row['Mailng_Zip_Code'];
			$Part_of_Governing_Board = $row['Part_of_Governing_Board'];
			$Official_ID = $row['Official_ID'];
			
			// list info - 13 fields
			$numOfficialFields = 13;
			// get govt notes
			$readOfficialNotes_sql = sprintf("SELECT * FROM notes WHERE Government_ID = '%s' AND Official_ID = '%s'
				ORDER BY Job_ID DESC
				LIMIT 1
				", escape($gid), escape($Official_ID));
			$result_readOfficialNotes = @PowerAlmanac\PDb::query($readOfficialNotes_sql);
			if (!$result_readOfficialNotes) {
				die("Error reading from notes database: $readOfficialNotes_sql" . PowerAlmanac\PDb::error());
			}
			$numOfficialNotes = PowerAlmanac\PDb::num_rows($result_readOfficialNotes);
			if ($numOfficialNotes == 0) { // first time
				$notesOfficialArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
			} else {
				$onerow = PowerAlmanac\PDb::fetch_array($result_readOfficialNotes);
				$notes = $onerow['txt'];
				$notesOfficialArray = explode(",", $notes);
				// debug
				//echo('<!--'); echo($notes); print_r($notesOfficialArray); echo('-->');
			}
			
			// qc or fa confirm all officials in government, NOT individual officials
			
			//echo("<tr><td><a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Official_ID');\">Edit</a>  | Delete</td><td>$Role</td><td>$Role_exists</td><td>$Government_Title</td><td>$First_Name</td><td>$Last_Name</td><td>$Email_Address</td><td>$Phone_Number</td><td>$Phone_Number_Ext</td><td>$Mailing_Street_Box</td><td>$Mailing_Suite_Number</td><td>$Mailing_City</td><td>$Mailing_State</td><td>$Mailng_Zip_Code</td><td>$Part_of_Governing_Board</td><td>$Official_ID</td></tr>");
			// 

			// get ORIGINAL content
			$readORIGINALGovtOfficial_sql = sprintf("SELECT * FROM full_data
					WHERE Official_ID = '%s'
					LIMIT 1
				", escape($Official_ID));
			$result_readORIGINALGovtOfficial = @PowerAlmanac\PDb::query($readORIGINALGovtOfficial_sql);
			if (!$result_readORIGINALGovtOfficial) {
				die("Error reading from $dbname database: $readORIGINALGovtOfficial_sql" . PowerAlmanac\PDb::error());
			}
			// if does not exisit, it's a new addition
			$numOldMatch = PowerAlmanac\PDb::num_rows($result_readORIGINALGovtOfficial);
			$onerowORIGINALGovtOfficial = PowerAlmanac\PDb::fetch_array($result_readORIGINALGovtOfficial);
			//$o_Role = $onerowORIGINALGovtOfficial['role'];
			$o_Role = $onerowORIGINALGovtOfficial['Role'];
			if ($numOldMatch == 0) {
				$o_Role = '<font color="#FF0000">NEWLY ADDED OFFICIAL</font>';
			}
			$o_Role_exists = $onerowORIGINALGovtOfficial['Role_exists'];
			$o_Government_Title = $onerowORIGINALGovtOfficial['Government_Title'];
			$o_First_Name = $onerowORIGINALGovtOfficial['First_Name'];
			$o_Last_Name = $onerowORIGINALGovtOfficial['Last_Name'];
			$o_Email_Address = $onerowORIGINALGovtOfficial['Email_Address'];
			$o_Phone_Number = $onerowORIGINALGovtOfficial['Phone_Number'];
			$o_Phone_Number_FMTD = substr($o_Phone_Number,0,3) . '-' .  substr($o_Phone_Number,3,3) .  '-' . substr($o_Phone_Number,6,4);
			$o_Phone_Number_Ext = $onerowORIGINALGovtOfficial['Phone_Number_Ext'];
			$o_Mailing_Street_Box = $onerowORIGINALGovtOfficial['Mailing_Street_Box'];
			$o_Mailing_Suite_Number = $onerowORIGINALGovtOfficial['Mailing_Suite_Number'];
			$o_Mailing_City = $onerowORIGINALGovtOfficial['Mailing_City'];
			$o_Mailing_State = $onerowORIGINALGovtOfficial['Mailing_State'];
			$o_Mailng_Zip_Code = $onerowORIGINALGovtOfficial['Mailng_Zip_Code'];
			$o_Part_of_Governing_Board = $onerowORIGINALGovtOfficial['Part_of_Governing_Board'];
		
			// tag changes but display ORIGINAL content from FULL_DATA
			echo("<tr><td><a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Official_ID');\">Edit</a></td><td>$o_Role</td><td class='color$notesOfficialArray[0]'>$o_Role_exists</td><td class='color$notesOfficialArray[1]'>$o_Government_Title</td><td class='color$notesOfficialArray[2]'>$o_First_Name</td><td class='color$notesOfficialArray[3]'>$o_Last_Name</td><td class='color$notesOfficialArray[4]'>$o_Email_Address</td><td class='color$notesOfficialArray[5]'>$o_Phone_Number_FMTD</td><td class='color$notesOfficialArray[6]'>$o_Phone_Number_Ext</td><td class='color$notesOfficialArray[7]'>$o_Mailing_Street_Box</td><td class='color$notesOfficialArray[8]'>$o_Mailing_Suite_Number</td><td class='color$notesOfficialArray[9]'>$o_Mailing_City</td><td class='color$notesOfficialArray[10]'>$o_Mailing_State</td><td class='color$notesOfficialArray[11]'>$o_Mailng_Zip_Code</td><td class='color$notesOfficialArray[12]'>$o_Part_of_Governing_Board</td><td>$Official_ID</td></tr>");

			switch($Role_exists) {
				case 'Yes':
					$selected_role_yes = ' selected';
					$selected_role_no = ''; 
					break;
				case 'No':
					$selected_role_yes = ''; 
					$selected_role_no = ' selected'; 
					break;
				default:
					$selected_role_yes = ''; 
					$selected_role_no = ''; 
					break;
			}
			switch($Part_of_Governing_Board) {
				case 'Yes':
					$selected_board_yes = ' selected';
					$selected_board_no = ''; 
					break;
				case 'No':
					$selected_board_yes = ''; 
					$selected_board_no = ' selected'; 
					break;
				default:
					$selected_board_yes = ''; 
					$selected_board_no = ' selected'; 
					break;
			}
			if ($Role == 'Top Elected Official') {
				$editRole = "<select name='partofboard'><option value='Yes' $selected_board_yes>Yes</option><option value='' $selected_board_no>No</option></select>";
				$editRoleExist = "<input type='text' name='roleexist' value='Yes' readonly='readonly'>";
			} elseif (in_array($Role, array('Head of Communications', 'Head Building Official', 'Head of HR'))) {
				$editRole = "<input type='text' name='partofboard' value='$Part_of_Governing_Board' readonly='readonly'>";
				$editRoleExist = "<select name='roleexist'><option value='No' selected>No</option></select>";
			} else {
				//if ($Role == 'Head of IT') {
					//$editRole = "<input type='text' name='partofboard' value='$Part_of_Governing_Board' readonly='readonly'>";
					//$editRoleExist = "<input type='text' name='roleexist' value='No' readonly='readonly'>";
				//} else {
					$editRole = "<input type='text' name='partofboard' value='$Part_of_Governing_Board' readonly='readonly'>";
					$editRoleExist = "<select name='roleexist'><option value='Yes' $selected_role_yes>Yes</option><option value='No' $selected_role_no>No</option></select>";
				//}
			}
			
			// show latest UPDATED content
			echo("<tr bgcolor='#ffffff'><td colspan='20' align='left'><div id='$Official_ID' style='display:none;'>&nbsp;&nbsp;<a href='JavaScript:void(0);' onclick=\"javascript:closeEdit('$Official_ID');\" style='color:#ff0000;'>Hide/Cancel</a><form action='supervisor-$job_state-edit.php' name='$Official_ID' method='post' onSubmit=\"return validateForm('$Official_ID')\"><input type='hidden' name='gid' value='$gid' /><input type='hidden' name='jid' value='$jid' /><input type='hidden' name='oid' value='$Official_ID' />");
			// previous values
			echo("<input type='hidden' name='role' value='$Role'>");
			echo("<input type='hidden' name='p_roleexist' value='$Role_exists'>");
			echo("<input type='hidden' name='p_title' value='$Government_Title'>");
			echo("<input type='hidden' name='p_fname' value='$First_Name'>");
			echo("<input type='hidden' name='p_lname' value='$Last_Name'>");
			echo("<input type='hidden' name='p_email' value='$Email_Address'>");
			echo("<input type='hidden' name='p_phone' value='$Phone_Number_FMTD'>");
			echo("<input type='hidden' name='p_phoneext' value='$Phone_Number_Ext'>");
			echo("<input type='hidden' name='p_mail_box' value='$Mailing_Street_Box'>");
			echo("<input type='hidden' name='p_mail_suite' value='$Mailing_Suite_Number'>");
			echo("<input type='hidden' name='p_mail_city' value='$Mailing_City'>");
			echo("<input type='hidden' name='p_mail_state' value='$Mailing_State'>");
			echo("<input type='hidden' name='p_mail_zip' value='$Mailng_Zip_Code'>");
			echo("<input type='hidden' name='p_partofboard' value='$Part_of_Governing_Board'>");
			// updates
			echo("<table id='$Official_ID' width='75%' align='center'><thead><tr class='ui-edit-header '><th>Role Exist</th><th>Government Title</th><th>First Name</th><th>Last Name</th><th>Email</th> <th>Phone Number</th><th>Phone Ext</th></tr></thead><tbody><tr><td>$editRoleExist</td><td><input type='text' name='title' value='$Government_Title' size='30'></td><td><input type='text' name='fname' value=\"$First_Name\"></td><td><input type='text' name='lname' value=\"$Last_Name\"></td><td><input type='text' name='email' value='$Email_Address' size='30'></td><td><input type='text' name='phone' value='$Phone_Number'></td><td><input type='text' name='phoneext' value='$Phone_Number_Ext'></td></tr></tbody><thead><tr class='ui-edit-header '><th>Mailing Street Box</th><th>Mailing Suite Number</th><th>Mailing City</th><th>Mailing State</th><th>Mailing Zip Code</th><th>Governing Board?</th></tr></thead><tbody><tr><td><input type='text' name='mail_box' value=\"$Mailing_Street_Box\" size='30'></td><td><input type='text' name='mail_suite' value=\"$Mailing_Suite_Number\"></td><td><input type='text' name='mail_city' value=\"$Mailing_City\"></td><td><input type='text' name='mail_state' value='$Mailing_State' readonly='readonly'></td><td><input type='text' name='mail_zip' value='$Mailng_Zip_Code' onkeyup='validateZip(this.value);'></td><td>$editRole</td></tr></tbody><tr><td colspan='7' align='center'><input type='submit' value='submit updates'></td></tr></table></form></div></td></tr>");
		}
		?>
		</tbody>
	</table>
    <br />
    <?php
	include("inc-government-history.php");
    ?>
    <div id="dialog-form" title="Add Disposition">
        <form>
        <input type="hidden" name="gidN" id="gidN" value="<?php echo($gid); ?>" />
        <input type="hidden" name="jidN" id="jidN" value="<?php echo($jid); ?>" />
        <input type="hidden" name="empidN" id="empidN" value="<?php echo($empid); ?>" />
        <fieldset>
            <label for="noteN">Add Disposition to this Government</label>
            <select id="noteN">
            <option value="[Can't Connect] -> [No Answer]">Can't Connect->No Answer</option>
            <option value="[Can't Connect] -> [Answering Machine]">Can't Connect->Answering Machine</option>
            <option value="[Can't Connect] -> [Refuse To Participate]">Can't Connect->Refuse To Participate</option>
            <option value="[In Progress] -> [Left Voicemail]">In Progress->Left Voicemail</option>
            <option value="[In Progress] -> [Sent eMail]">In Progress->Sent eMail</option>
            <option value="[In Progress] -> [Sent Fax]">In Progress->Sent Fax</option>
            <option value="[In Progress] -> [Partially Completed Call]">In Progress->Partially Completed Call</option>
            <option value="[Almost Done] -> [Holding For Final Review]">Almost Done->Holding For Final Review</option>
            </select>
        </fieldset>
        </form>
    </div>

    <div id="dialog-formnotes" title="Add General Notes">
        <form>
        <input type="hidden" name="gidN" id="gidN" value="<?php echo($gid); ?>" />
        <input type="hidden" name="jidN" id="jidN" value="<?php echo($jid); ?>" />
        <input type="hidden" name="empidN" id="empidN" value="<?php echo($empid); ?>" />
        <fieldset>
            <label for="noteN">Add Notes to this Government</label>
            <textarea name="noteN" id="noteN" cols="50" rows="10">
            </textarea>
        </fieldset>
        </form>
    </div>
    
	<div class="readyNotes">    
		<button id="add-disposition">Add Disposition</button>
        <button id="add-notes">Add Notes</button>
    </div>
    
</div>

<div id="dialog-form1" title="Add Notes">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to Voice Agent</label>
        <textarea name="note" id="note" cols="50" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

<div id="dialog-form2" title="Add Notes">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to Voice Agent</label>
        <textarea name="note" id="note" cols="50" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

<div id="dialog-form3" title="Add Notes">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to Quality Checker</label>
        <textarea name="note" id="note" cols="50" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

</center>

</div>


</body>
</html>