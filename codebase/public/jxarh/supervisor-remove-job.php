<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');


$jid = $_REQUEST['jid'];

// initiator_delete_job(in j_id BIGINT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL initiator_delete_job(%s)", escape($jid)));

header("Location: dashboard-supervisor.php");
flush();
exit;

?>
