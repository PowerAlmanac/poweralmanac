<?php

$primary_role = $_SESSION['primary_role'];
$name = $_SESSION['name'];
$access_level = $_SESSION['access_level'];

switch($primary_role) {
	case 'VoiceAgent':
		$primaryRole = 'Voice Agent';
		break;
	case 'FinalApprover':
		$primaryRole = 'Final Approver';
		break;
	case 'QualityChecker':
		$primaryRole = 'Quality Checker';
		break;
	case 'ProcessInitiator':
		$primaryRole = 'Process Initiator';
		break;
	case 'DataUploader':
		$primaryRole = 'Admin/Data Uploader';
		break;
	default:
		break;
}

?>
<br>
<div id="usersinfo" class="ui-widget">
<table id='userinfo' width='500' align='center' style='border: 1px solid #000000;'>
<thead>
<tr class='ui-edit-header '>
    <th>Logged In As</th>
    <th>Access Level</th>
    <th>Primary Role</th>
</tr>
</thead>
<tbody>
<tr>
    <td align="center"><?php echo($name); ?></td>
    <td align="center"><?php echo($access_level); ?></td>
    <td align="center"><?php echo($primaryRole); ?></td>
</tr>
</tbody>
</table>
</div>
