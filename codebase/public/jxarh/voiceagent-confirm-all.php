<?php
include("inc-config.php");

$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// loop through all unconfirmed
$VAjob_table = 'va_' . $jid;
$readgovtOfficialsVA_sql = sprintf("SELECT * FROM %s
	WHERE Government_ID = '%s'
	", escape($VAjob_table), escape($gid));
$result_readgovtOfficialsVA = @PowerAlmanac\PDb::query($readgovtOfficialsVA_sql);
if (!$result_readgovtOfficialsVA) {
	die("Error reading from $VAjob_table database: $readgovtOfficialsVA_sql" . PowerAlmanac\PDb::error());
}

// va_confirm(in o_id varchar(100),in job_id bigint,out rc boolean)

while ($row = PowerAlmanac\PDb::fetch_array($result_readgovtOfficialsVA))
{
	$oid = $row['Official_ID'];
	//echo("$oid<br>");

	$pdo->query(sprintf("CALL va_confirm('%s',%s,@rc)", escape($oid), escape($jid)));
	$pdoObject = $pdo->query("SELECT @rc");
	$rsArray = $pdoObject->fetchAll();
	$rc = $rsArray[0]['@rc'];

}

header("Location: voiceagent-editgovt.php?gid=$gid&jid=$jid");
flush();
exit;

?>