<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$govtsArray = $_REQUEST['govts']; // array
$jid = $_REQUEST['jid'];

// initiator_add_gov(in g_id varchar(14),in job_id bigint,out rc boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// loop thru govts
foreach ($govtsArray as $gid) 
{
	//echo("Adding $gid to job $jid<br>");
	$pdo->query(sprintf("CALL initiator_add_gov('%s',%s,@rc3)", escape($gid), escape($jid)));
}
header("Location: supervisor-editjob.php?jid=$jid");
flush();
exit;

?>