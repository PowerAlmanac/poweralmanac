<?php
include("inc-config.php");

$userid = $_POST['userid'];
$password = $_POST['password'];

// read employees database
$read_sql = sprintf("SELECT * FROM employees
		WHERE user_id = '%s' AND password = '%s'
		LIMIT 1
	", escape($userid), escape($password));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from database: $read_sql" . PowerAlmanac\PDb::error());
}

// see if match
$numMatch = PowerAlmanac\PDb::num_rows($result_read);

if ($numMatch == '1') {
	// valid log in - get access level
	$onerow = PowerAlmanac\PDb::fetch_array($result_read);
	$access_level = $onerow['access_level'];
	$primary_role = $onerow['primary_role'];
	$emp_id = $onerow['emp_id'];
	$first_name = $onerow['first_name'];
	$last_name = $onerow['last_name'];
	
	$_SESSION['access_level'] = $access_level;
	$_SESSION['emp_id'] = $emp_id;
	$_SESSION['name'] = "$first_name $last_name";
	$_SESSION['error_message'] = '';
	$_SESSION['primary_role'] = $primary_role;
	 

	switch($access_level) {
		case 'Administrator':
			header("Location: dashboard-admin.php");
			break;
		case 'Supervisor':
			header("Location: dashboard-supervisor.php");
			// log it
			$insertLog_sql = sprintf("INSERT INTO valog (VA_ID,timestamp,action)
				VALUES ('%s',now(),'login')
			", escape($emp_id));
			$result_insertLog = @PowerAlmanac\PDb::query($insertLog_sql);
			if (!$result_insertLog) {
				die('Error writing to valog database:' . PowerAlmanac\PDb::error());
			}
			break;
		case 'VoiceAgent':
			header("Location: dashboard-voiceagent.php");
			// log it
			$insertLog_sql = sprintf("INSERT INTO valog (VA_ID,timestamp,action)
				VALUES ('%s',now(),'login')
			", escape($emp_id));
			$result_insertLog = @PowerAlmanac\PDb::query($insertLog_sql);
			if (!$result_insertLog) {
				die('Error writing to valog database:' . PowerAlmanac\PDb::error());
			}
			break;
		default:
			header("Location: dashboard-voiceagent.php");
			break;
	}
	// go to dashboard
	flush();
	exit;	
} else {
	// mismatch
	$_SESSION['error_message'] = 'Incorrect User Name and/or Password. Please try again.';
	header("Location: index.php");
	flush();
	exit;	
}

?>