<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$gid = $_REQUEST['gid'];
$jid = $_REQUEST['jid'];

$empid = $_SESSION['emp_id'];

// get job detail
$read_sql = sprintf("SELECT * FROM jobs
		WHERE job_id = '%s'
		LIMIT 1
	", escape($jid));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$job_id = $onerow['job_id'];
$va_id = $onerow['va_id'];
$num_officials = $onerow['num_officials'];
$num_govs = $onerow['num_govs'];
$job_state = $onerow['state'];

// check if job state is same as primary role
$primary_role = $_SESSION['primary_role'];

if (($job_state == 'qc') && ($primary_role != 'QualityChecker')) {
	// error
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

if (($job_state == 'fa') && ($primary_role != 'FinalApprover')) {
	// error
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

$job_table = $job_state . '_' . $job_id;

//echo("DEBUG: job_table = $job_table");

// check if govt id is done
$readgovtoff_sql = sprintf("SELECT * FROM %s WHERE Government_ID = '%s'
	", escape($job_table), escape($gid));
$result_readgovtoff = @PowerAlmanac\PDb::query($readgovtoff_sql);
if (!$result_readgovtoff) {
	die("Error reading from job_table database: $readgovtoff_sql" . PowerAlmanac\PDb::error());
}
$num_officials = PowerAlmanac\PDb::num_rows($result_readgovtoff);

if ($num_officials == '0') {
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

$checkSQL = sprintf("show tables from %s like '%s'", escape($dbname), escape($job_table));
$result_checkTable = @PowerAlmanac\PDb::query($checkSQL);
if (!$result_checkTable) {
	die('Error reading from $dbname database:' . PowerAlmanac\PDb::error());
}
$numTables = number_format(PowerAlmanac\PDb::num_rows($result_checkTable));
if ($numTables > 0) {
	// found
	$errorMsg = '';
	
	// govt info
	$readgovtInfo_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		LIMIT 1
		", escape($job_table), escape($gid));
	$result_readgovtInfo = @PowerAlmanac\PDb::query($readgovtInfo_sql);
	if (!$result_readgovtInfo) {
		die("Error reading from $job_table database: $read_sql" . PowerAlmanac\PDb::error());
	}
	$govtinforow = PowerAlmanac\PDb::fetch_array($result_readgovtInfo);
	// LATEST UPDATED DATA
	$Government_Type = $govtinforow['Government_Type'];
	$Government_ID = $govtinforow['Government_ID'];
	$Government_Place_Name = $govtinforow['Government_Place_Name'];
	$Government_Type_Name = $govtinforow['Government_Type_Name'];
	$Government_County_Name = $govtinforow['County_Name'];
	$Government_state = $govtinforow['state'];
	$Government_Population = $govtinforow['Population'];
	$Government_PhoneNumber = $govtinforow['Government_PhoneNumber'];
	$Government_PhoneNumber_FMTD = substr($Government_PhoneNumber,0,3) . '-' .  substr($Government_PhoneNumber,3,3) .  '-' . substr($Government_PhoneNumber,6,4);
	$Government_Web_Address = $govtinforow['Government_Web_Address'];
	$Government_Address_Street_Box = $govtinforow['Address_Street_Box'];
	$Government_City = $govtinforow['City'];
	$Government_Zip_Code = $govtinforow['Zip_Code'];
	$Electron_Month = $govtinforow['Electron_Month'];
	$Taken_from = $govtinforow['Taken_from'];
	$Updated_website = $govtinforow['Updated_website'];
			
	// govt officials
	$readgovtOfficials_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		ORDER BY Official_ID
		", escape($job_table), escape($gid));
	$result_readgovtOfficials = @PowerAlmanac\PDb::query($readgovtOfficials_sql);
	if (!$result_readgovtOfficials) {
		die("Error reading from $job_table database: $read_sql" . PowerAlmanac\PDb::error());
	}
} else {
	// does not exist - must have moved to DONE
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

// text
switch($job_state) {
	case 'va':
		$job_state_description = 'Voice Agent';
		break;
	case 'qc':
		$job_state_description = 'Quality Check';
		break;
	case 'fa':
		$job_state_description = 'Final Approval';
		break;
	case 'dome':
		$job_state_description = 'Ready To Upload';
		break;
	default:
		$job_state_description = 'Unknown';
		break;
}


// fix spaces on place names
$Government_Place_NameFMT = str_replace(" ","-",$Government_Place_Name);

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header("Content-Disposition: attachment; filename=$Government_ID-$Government_Place_NameFMT.csv");

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array("Record ID","Record last updated","Official's First Name","Official's Last Name","Official's Title","Official's Role","Is Top Elected Official Part of Governing Board?","Official's Mailing Address - Street/Box number","Official's Mailing Address - Suite Number","Official's Mailing Address - City","Official's Mailing Address - State","Official's Mailing Address -Zip Code","Official's Phone Number ","Official's Phone Number  EXTENSION","Official's Email Address","Government Place Name ","Government Type Name ","Government Type","Government Web Address ","Government's Main Phone Number ","Government's Physical Address - Street ","Government's Physical Address - City ","Government's Physical Address - State ","Government's Physical Address -Zip Code ","Name of County that Municipal or Township Government is Located in ","Population","Role Exist?"));

/*
Col: A to B
Record ID
Record last updated

Col: C to O
Official's First Name	
Official's Last Name	
Official's Title	
Official's Role	Is Top Elected 
Official Part of Governing Board?	
Official's Mailing Address - Street/Box number	
Official's Mailing Address - Suite Number	
Official's Mailing Address - City	
Official's Mailing Address - State	
Official's Mailing Address -Zip Code	
Official's Phone Number 	
Official's Phone Number  EXTENSION	
Official's Email Address

Col: P to Z
Government Place Name
Government Type Name 	
Government Type	
Government Web Address 	
Government's Main Phone Number 	
Government's Physical Address - Street 	
Government's Physical Address - City 	
Government's Physical Address - State 	
Government's Physical Address -Zip Code 	
Name of County that Municipal or Township Government is Located in 	
Population

Col: AA
Role Exist?

*/

// loop over the rows, outputting them

while ($row = PowerAlmanac\PDb::fetch_array($result_readgovtOfficials))
{
	// list officials - 12 to 13
	$Role = str_replace("/"," / ",$row['Role']);
	//$Role = str_replace("/"," / ",$row['role']);
	$Role_exists = $row['Role_exists'];
	$Government_Title = $row['Government_Title'];
	$First_Name = $row['First_Name'];
	$Last_Name = $row['Last_Name'];
	$Email_Address = $row['Email_Address'];
	$Phone_Number = $row['Phone_Number'];
	// format
	$Phone_Number_FMTD = substr($Phone_Number,0,3) . '-' .  substr($Phone_Number,3,3) .  '-' . substr($Phone_Number,6,4);
	$Phone_Number_Ext = $row['Phone_Number_Ext'];
	$Mailing_Street_Box = $row['Mailing_Street_Box'];
	$Mailing_Suite_Number = $row['Mailing_Suite_Number'];
	$Mailing_City = $row['Mailing_City'];
	$Mailing_State = $row['Mailing_State'];
	$Mailng_Zip_Code = $row['Mailng_Zip_Code'];
	$Part_of_Governing_Board = $row['Part_of_Governing_Board'];
	$Official_ID = $row['Official_ID'];
	$Date_Confirmed = $row['Date_Confirmed'];
	//
	fputcsv($output, array("$Official_ID","$Date_Confirmed","$First_Name","$Last_Name","$Government_Title","$Role","$Part_of_Governing_Board","$Mailing_Street_Box","$Mailing_Suite_Number","$Mailing_City","$Mailing_State","$Mailng_Zip_Code","$Phone_Number_FMTD","$Phone_Number_Ext","$Email_Address","$Government_Place_Name","$Government_Type_Name","$Government_Type","$Government_Web_Address","$Government_PhoneNumber_FMTD","$Government_Address_Street_Box","$Government_City","$Government_state","$Government_Zip_Code","$Government_County_Name","$Government_Population","$Role_exists"));
}

// close file
fclose($output);

?>