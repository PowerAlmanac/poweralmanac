<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$oid = $_REQUEST['oid'];
$jid = $_REQUEST['jid'];
$notes = $_REQUEST['notes'];

// qc_send_back(in job_id bigint,in g_id varchar(20), in note varchar(1000))

//$notes = 'please recheck Top Elected Official record';

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL qc_send_back('%s',%s,'%s')", escape($jid), escape($oid), escape($notes)));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QC Send Back</title>
<meta name="robots" content="noindex">
</head>

<body>
<?php
	echo("<a href='qc-refresh.php'>continue</a>");
?>
</body>
</html>
