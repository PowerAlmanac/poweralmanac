<?php
include("inc-config.php");
include("inc-protected-admin.php");

$read_sql = "SELECT DISTINCT Government_ID,Government_Place_Name,Government_Type_Name,Mailing_State,Date_Confirmed FROM full_data
		WHERE r_state = 'allocated'
		ORDER BY Date_Confirmed ASC
	";
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$num_rows = PowerAlmanac\PDb::num_rows($result_read);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Access - Allocated Government IDs</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<meta name="robots" content="noindex">

</head>

<body>

<div class="demo">

<center>
<?php
// navigation
include("nav-admin.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Allocated Government IDs</h1>
    Total = <b><?php echo($num_rows); ?></b>
	<table id="govtids" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header">
				<th>Government ID</th>
				<th>Government Place Name</th>
				<th>Government Type Name</th>
				<th>State</th>
				<th>Date Confirmed</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			// Government_ID  Government_Place_Name  Government_Type_Name  Mailing_State  
			$Government_ID = $row['Government_ID'];
			$Government_Place_Name = $row['Government_Place_Name'];
			$Government_Type_Name = $row['Government_Type_Name'];
			$Mailing_State = $row['Mailing_State'];
			$Date_Confirmed = $row['Date_Confirmed'];
			echo("<tr><td><b>$Government_ID</b></td><td>$Government_Place_Name</td><td>$Government_Type_Name</td><td><b>$Mailing_State</b></td><td>$Date_Confirmed</td></tr>");
		}
		?>
		</tbody>
	</table>
</div>

</center>

</div>

</body>
</html>