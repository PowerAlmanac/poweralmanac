<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$gid = $_REQUEST['gid'];
$jid = $_REQUEST['jid'];
$note = $_REQUEST['note'];

// fa_send_back_qc(in job_id bigint,in g_id varchar(20), in note varchar(1000))

//$notes = 'please recheck Top Elected Official record';

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL fa_send_back_qc(%s,'%s','%s')", escape($jid), escape($gid), escape($note)));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FA Send Back QC</title>
<meta name="robots" content="noindex">
</head>

<body>
<?php
	echo("<a href='fa-refresh.php'>continue</a>");
?>
</body>
</html>
