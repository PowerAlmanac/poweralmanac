<?php
include("inc-config.php");

include("inc-tag-changes.php");

// va_update_official(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255))

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL va_update_official('%s',%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
    escape($oid), escape($jid), escape($roleexist), escape($title), escape($fname), escape($lname), escape($email), escape($partofboard), escape($mail_box), escape($mail_suite), escape($mail_city), escape($mail_zip), escape($phone), escape($phoneext)));

header("Location: voiceagent-editgovt.php?gid=$gid&jid=$jid");
flush();
exit;

?>