<?php
include("inc-config.php");
include("inc-protected-voiceagent.php");

$emp_id = $_SESSION['emp_id'];

$readVAStatus_sql = sprintf("SELECT * FROM jobs
		WHERE va_id = '%s' AND state = 'va'
		ORDER BY job_id
	", escape($emp_id));
$result_readVAStatus = @PowerAlmanac\PDb::query($readVAStatus_sql);
if (!$result_readVAStatus) {
	die("Error reading from $dbname database: $readVAStatus_sql" . PowerAlmanac\PDb::error());
}
$numVAProgress = PowerAlmanac\PDb::num_rows($result_readVAStatus);

$readCompletedStatus_sql = sprintf("SELECT * FROM jobs
		WHERE va_id = '%s' AND state != 'va' AND state != ''
		ORDER BY job_id DESC
	", escape($emp_id));
$result_readCompletedStatus = @PowerAlmanac\PDb::query($readCompletedStatus_sql);
if (!$result_readCompletedStatus) {
	die("Error reading from $dbname database: $readCompletedStatus_sql" . PowerAlmanac\PDb::error());
}
$numVAComplete = PowerAlmanac\PDb::num_rows($result_readCompletedStatus);

function decodeTimeZone($tz) {
	/*
	4	Atlantic (GMT -04:00)
	5	Eastern (GMT -05:00)
	6	Central (GMT -06:00)
	7	Mountain (GMT -07:00)
	8	Pacific (GMT -08:00)
	9	Alaska (GMT -09:00)
	10	Hawaii-Aleutian Islands (GMT -10:00)
	11	American Samoa (GMT -11:00)
	14	Guam (GMT +10:00)
	15	Palau (GMT +9:00)
	*/
	switch($tz) {
		case '4':
			$timezone = 'Atlantic';
			break;
		case '5':
			$timezone = 'Eastern';
			break;
		case '6':
			$timezone = 'Central';
			break;
		case '7':
			$timezone = 'Mountain';
			break;
		case '8':
			$timezone = 'Pacific';
			break;
		case '9':
			$timezone = 'Alaska';
			break;
		case '10':
			$timezone = 'Hawaii-Aleutian Islands';
			break;
		case '11':
			$timezone = 'American Samoa';
			break;
		case '14':
			$timezone = 'Guam';
			break;
		case '15':
			$timezone = 'Palau';
			break;
		default: // unknown
			$timezone = $tz;
			break;
	}
	return $timezone;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Voice Agent</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<script type="text/javascript">

function selectAll()
{
	//alert('selectAll');
	checkAll(document.vaform.govts);
}

function unselectAll()
{
	//alert('unselectAll');
	uncheckAll(document.vaform.govts);
}

function checkAll(field)
{
	for (i = 0; i < field.length; i++)
		//alert(field[i].checked);
		field[i].checked = true ;
		//field[i].className = "NFCheck NFh";
}

function uncheckAll(field)
{
	for (i = 0; i < field.length; i++)
		field[i].checked = false ;
}

</script>   

<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">


<center>
<?php
// navigation
include("nav-voiceagent.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Voice Agent Dashboard</h1>
    <h2>Tasks In Progress</h2>
	<form action="voiceagent-multipleconfirm.php" method="post" name="vaform" id="vaform" >
    <center>
    <input type="submit" name="submit" id="submit" value="Confirm SELECTED Tasks" />
	</center>
    <table id="inprogressjobs" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Actions</th>
				<th>Task#</th>
				<th>Govt ID</th>
				<th>Govt Name</th>
				<th>Govt Type</th>
				<th>Govt Location (county,state)</th>
				<th>Time Zone</th>
				<th># Ofcls</th>
				<th># Edits (%)</th>
                <th>Confirmed</th>
                <th>Disposition</th>
                <th>&nbsp;Select<br /><a href="javascript: void(0)" onclick="selectAll();">ALL</a><br /><a href="javascript: void(0)" onclick="unselectAll();">NONE</a>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
        <?php
		if ($numVAProgress != 0) {
			while ($row = PowerAlmanac\PDb::fetch_array($result_readVAStatus))
			{
				$job_id = $row['job_id'];
				$va_id = $row['va_id'];
				$qc_id = $row['qc_id'];
				$fa_id = $row['fa_id'];
				$num_officials = $row['num_officials'];
				$num_govs = $row['num_govs'];
				$job_state = $row['state'];
			
				// look up Govt Info
				$readgovt_sql = sprintf("SELECT DISTINCT Government_ID,state,Government_Place_Name,Government_Type_Name,Government_Type,County_Name,g_timezone,Date_Confirmed  FROM va_%s", escape($job_id));
				$result_readgovt = @PowerAlmanac\PDb::query($readgovt_sql);
				if (!$result_readgovt) {
					die("Error reading from va_$job_id database: $readgovt_sql" . PowerAlmanac\PDb::error());
				}
				$onerow = PowerAlmanac\PDb::fetch_array($result_readgovt);
				$Government_ID = $onerow['Government_ID'];
				$state = $onerow['state'];
				$Government_Place_Name = $onerow['Government_Place_Name'];
				$Government_Type_Name = $onerow['Government_Type_Name'];
				$Government_Type = $onerow['Government_Type'];
				$County_Name = $onerow['County_Name'];
				$Date_Confirmed = $onerow['Date_Confirmed'];
				$confirmed = date("F",strtotime($Date_Confirmed));
				$g_timezone = $onerow['g_timezone'];
				$TZ = decodeTimeZone($g_timezone);
				if ($County_Name != '') {
					$location = "$County_Name, $state";
				} else {
					$location = "$state";
				}
				// rework
				$readRework_sql = sprintf("SELECT * FROM notes
						WHERE Government_ID = '%s' AND Official_ID = 'REWORK'
						ORDER BY id DESC
						LIMIT 1
					", escape($Government_ID));
				$result_readRework = @PowerAlmanac\PDb::query($readRework_sql);
				if (!$result_readRework) {
					die("Error reading from $dbname database: $readRework_sql" . PowerAlmanac\PDb::error());
				}
				$numRework = PowerAlmanac\PDb::num_rows($result_readRework);
				if ($numRework != 0) {
					$rework = "<img src='img/redflag.png' width='10' height='10' alt='rework' title='rework'>";
				} else {
					$rework = '';
				}
				
				
				// get dispostion
				/*
				[Can't Connect]->[No Answer]">Can't Connect->No Answer</option>
				[Can't Connect]->[Answering Machine]">Can't Connect->Answering Machine</option>
				[Can't Connect]->[Refuse To Participate]">Can't Connect->Refuse To Participate</option>
				[In Progress]->[Left Voicemail]">In Progress->Left Voicemail</option>
				[In Progress]->[Sent Voicemail]">In Progress->Sent Voicemail</option>
				[In Progress]->[Sent Fax]">In Progress->Sent Fax</option>
				[In Progress]->[Partially Completed Call]">In Progress->Partially Completed Call</option>
				[Almost Done]->[Holding For Final Review]">Almost Done->Holding For Final Review</option>
				*/
				$readDisposition_sql = sprintf("SELECT * FROM notes
						WHERE Government_ID = '%s' AND Official_ID = 'DISPOSITION'
						ORDER BY id DESC
						LIMIT 1
					", escape($Government_ID));
				$result_readDisposition = @PowerAlmanac\PDb::query($readDisposition_sql);
				if (!$result_readDisposition) {
					die("Error reading from $dbname database: $readDisposition_sql" . PowerAlmanac\PDb::error());
				}
				$numDisposition = PowerAlmanac\PDb::num_rows($result_readDisposition);
				if ($numDisposition != 0) {
					$donerow = PowerAlmanac\PDb::fetch_array($result_readDisposition);
					$disposition = $donerow['txt'];
				} else {
					$disposition = 'NONE';
				}
					
				// calculate changes
				/*
				all = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0'
				government = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = '$Government_ID'
				officials = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  != '$Government_ID' AND Official_ID  != ''
				disposition = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND Official_ID  = 'DISPOSITION'
				notes = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` != '0'
				rework = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND Official_ID  = 'REWORK'
				*/
				$readOfficialsEdits_sql = sprintf("SELECT * FROM notes
						WHERE Government_ID = '%s' AND Writer_ID = '0' AND Official_ID  != '%s' AND Official_ID  != ''
					", escape($Government_ID), escape($Government_ID));
				$result_readOfficialEdits = @PowerAlmanac\PDb::query($readOfficialsEdits_sql);
				if (!$result_readOfficialEdits) {
					die("Error reading from $dbname database: $readOfficialsEdits_sql" . PowerAlmanac\PDb::error());
				}
				$numOfficialEdits = PowerAlmanac\PDb::num_rows($result_readOfficialEdits);
				$numOfficialEditsPCT = number_format(($numOfficialEdits/$num_officials)*100,2);
				
				echo("<tr><td><a href='voiceagent-editgovt.php?gid=$Government_ID&jid=$job_id'>Edit</a> $rework</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$TZ</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$confirmed</td><td><b>$disposition</b></td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID,$job_id' /></td></tr>");	
			}
		} else {
			echo("<tr><td colspan='9'>You have no tasks</td></tr>");
		}
		?>
		</tbody>
	</table>
    </form>
    <h2>Recently Completed Tasks</h2>
	<table id="completedjobs" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Actions</th>
				<th>Task#</th>
				<th>Govt ID</th>
				<th>Govt Name</th>
				<th>Govt Type</th>
				<th>Govt Loc</th>
				<th>Time Zone</th>
				<th># Ofcls</th>
				<th># Edits (%)</th>
                <th>Disposition</th>
			</tr>
		</thead>
		<tbody>
        <?php
		if ($numVAComplete != 0) {
			while ($row = PowerAlmanac\PDb::fetch_array($result_readCompletedStatus))
			{
				
				$job_id = $row['job_id'];
				$va_id = $row['va_id'];
				$qc_id = $row['qc_id'];
				$fa_id = $row['fa_id'];
				$num_officials = $row['num_officials'];
				$num_govs = $row['num_govs'];
				$job_state = $row['state'];
				
				if ($job_state != 'done') {
					$tableID = $job_state . '_' . $job_id;
	
					// look up Govt Info
					$readgovt_sql = sprintf("SELECT DISTINCT Government_ID,state,Government_Place_Name,Government_Type_Name,Government_Type,County_Name,g_timezone  FROM %s", escape($tableID));
					$result_readgovt = @PowerAlmanac\PDb::query($readgovt_sql);
					if (!$result_readgovt) {
						die("Error reading from $tableID database: $readgovt_sql" . PowerAlmanac\PDb::error());
					}
					$onerow = PowerAlmanac\PDb::fetch_array($result_readgovt);
					$Government_ID = $onerow['Government_ID'];
					$state = $onerow['state'];
					$Government_Place_Name = $onerow['Government_Place_Name'];
					$Government_Type_Name = $onerow['Government_Type_Name'];
					$Government_Type = $onerow['Government_Type'];
					$County_Name = $onerow['County_Name'];
					$g_timezone = $onerow['g_timezone'];
					$TZ = decodeTimeZone($g_timezone);
					if ($County_Name != '') {
						$location = "$County_Name, $state";
					} else {
						$location = "$state";
					}
					// get dispostion
					/*
					[Can't Connect]->[No Answer]">Can't Connect->No Answer</option>
					[Can't Connect]->[Answering Machine]">Can't Connect->Answering Machine</option>
					[Can't Connect]->[Refuse To Participate]">Can't Connect->Refuse To Participate</option>
					[In Progress]->[Left Voicemail]">In Progress->Left Voicemail</option>
					[In Progress]->[Sent Voicemail]">In Progress->Sent Voicemail</option>
					[In Progress]->[Sent Fax]">In Progress->Sent Fax</option>
					[In Progress]->[Partially Completed Call]">In Progress->Partially Completed Call</option>
					[Almost Done]->[Holding For Final Review]">Almost Done->Holding For Final Review</option>
					*/
					$readDisposition_sql = sprintf("SELECT * FROM notes
							WHERE Government_ID = '%s' AND Official_ID = 'DISPOSITION'
							ORDER BY id DESC
							LIMIT 1
						", escape($Government_ID));
					$result_readDisposition = @PowerAlmanac\PDb::query($readDisposition_sql);
					if (!$result_readDisposition) {
						die("Error reading from $dbname database: $readDisposition_sql" . PowerAlmanac\PDb::error());
					}
					$numDisposition = PowerAlmanac\PDb::num_rows($result_readDisposition);
					if ($numDisposition != 0) {
						$donerow = PowerAlmanac\PDb::fetch_array($result_readDisposition);
						$disposition = $donerow['txt'];
					} else {
						$disposition = 'NONE';
					}
					
					// calculate changes
					/*
					all = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0'
					government = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = '$Government_ID'
					officials = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  != '$Government_ID' AND Official_ID  != ''
					disposition = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = 'DISPOSITION'
					notes = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` != '0'
					*/
					$readOfficialsEdits_sql = sprintf("SELECT * FROM notes
							WHERE Government_ID = '%s' AND Writer_ID = '0' AND Official_ID  != '%s' AND Official_ID  != ''
						", escape($Government_ID), escape($Government_ID));
					$result_readOfficialEdits = @PowerAlmanac\PDb::query($readOfficialsEdits_sql);
					if (!$result_readOfficialEdits) {
						die("Error reading from $dbname database: $readOfficialsEdits_sql" . PowerAlmanac\PDb::error());
					}
					$numOfficialEdits = PowerAlmanac\PDb::num_rows($result_readOfficialEdits);
					$numOfficialEditsPCT = number_format(($numOfficialEdits/$num_officials)*100,2);
					echo("<tr><td>Confirmed</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$TZ</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td><b>$disposition</b></td></tr>");
				} else {
					echo("<tr><td>Completed</td><td>$job_id</td><td colspan='3'>Information Not Available Yet - Task is in Data Uploader</td><!--<td></td><td></td>--><td></td><td></td><td>$num_officials</td><td>TBD</td><td>NA</td></td></tr>");
				}
			}
		} else {
			echo("<tr><td colspan='9'>You have no tasks</td></tr>");
		}
		?>
		</tbody>
	</table>
</div>


</center>

</div>

</body>
</html>