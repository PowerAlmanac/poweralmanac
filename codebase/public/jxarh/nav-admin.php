<?php
// admin only 
?>

	<script>
	$(function() {
		$( ".admin_nav a:first" ).button({
            icons: {
                primary: "ui-icon-person"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-folder-collapsed"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-image"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-signal-diag"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-calculator"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-info"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-power"
            }
        });
	});
	</script>
    
<div class="admin_nav">

<a href="admin-users.php">Manage Active Users</a>
<a href="admin-jobs.php">Manage Active Tasks</a>
<a href="admin-allocated.php">Show Allocated Govt IDs</a>
<a href="admin-productivity.php">Show Productivity</a>
<a href="admin-activity.php">Show Activity</a>
<a href="help.php" target="_blank">Help</a>
<a href="logout.php">Log Out</a>

</div>