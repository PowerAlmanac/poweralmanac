<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$jid = $_REQUEST['jid'];

if (isset($_REQUEST['p'])) {
	$p = $_REQUEST['p'];
} else {
	$p = 0;
}

$numRecords2Show = 25;

$pageOffset = $p * $numRecords2Show;

// initiator_refresh(in StartFrom int, in numRecords2Show int, OUT Result MEDIUMTEXT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL initiator_refresh(%s,%s,@Result1)", escape($pageOffset), escape($numRecords2Show)));
$pdoObject = $pdo->query("SELECT @Result1");
$rsArray = $pdoObject->fetchAll();
$Result1 = $rsArray[0]['@Result1'];

$Result_Array = json_decode($Result1, TRUE);
switch(json_last_error())
{
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded';
	break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found';
	break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON_SUMMARY';
		print_r($JSON_SUMMARY);
	break;
	case JSON_ERROR_NONE:
		//echo ' - No errors';
	break;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Governments</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>

<script language="javascript" type="text/javascript" src="niceforms.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="niceforms-default.css" />
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />
<script type="text/javascript">
<!-- Begin
function selectAll()
{
	//alert('selectAll');
	checkAll(document.govtform.govtid);
}

function unselectAll()
{
	//alert('unselectAll');
	uncheckAll(document.govtform.govtid);
}

function checkAll(field)
{
	for (i = 0; i < field.length; i++)
		field[i].checked = true ;
}

function uncheckAll(field)
{
	for (i = 0; i < field.length; i++)
		field[i].checked = false ;
}

//  End -->
</script>
	<script>
	$(function() {
		$( "input:submit" ).button();
	});
	</script>
<meta name="robots" content="noindex">
</head>

<body>

<center>
<?php
// navigation
include("nav-supervisor.php");
include("userinfo.php");
?>
</center>
<center>
<div id="users-contain" class="ui-widget">
    	<h1>Add Governments To Job #<?php echo($jid); ?></h1>
        <form action="supervisor-add-govts.php" method="post" name="govtform" id="govtform">
        <input type="hidden" name="jid" value="<?php echo($jid); ?>" />
        <table id="govts" class="ui-widget ui-widget-content">
            <thead>
                <tr class="ui-widget-header ">
                	<th>&nbsp;Government ID&nbsp;</th>
                    <th>&nbsp;Government Name&nbsp;</th>
                    <th>&nbsp;State&nbsp;</th>
                    <th>&nbsp;Government Type&nbsp;</th>
                    <th>&nbsp;Population&nbsp;</th>
                    <th>&nbsp;# Officials&nbsp;</th>
                    <th>&nbsp;<a href="javascript: void(0)" onclick="selectAll();">Set All</a> | <a href="javascript: void(0)" onclick="unselectAll();">UnSet All</a>&nbsp;</th> 
                </tr>
            </thead>
            <tbody>
			<?php
            foreach ($Result_Array as $resultItem) {
                //print_r($resultItem);
                $Government_ID = $resultItem['Government_ID'];
                $Government_Place_Name = $resultItem['Government_Place_Name'];
                $Government_Type = $resultItem['Government_Type'];
                $State = $resultItem['State'];
                $Population = $resultItem['Population'];
                $number_of_officials = $resultItem['number_of_officials'];
                /*
                [Government_ID] => 07300501800000
                [Government_Type] => township
                [State] => CT
                [Government_Place_Name] => WALLINGFORD
                [Population] => 44942
                [County_Name] => NEW HAVEN
                [Government_Type_Name] => TOWN
                [Government_Web_Address] => http://www.town.wallingford.ct.us/
                [number_of_officials] => 17
                */
				echo("<tr><td>$Government_ID</td><td>$Government_Place_Name</td><td>$State</td><td>$Government_Type</td><td>$Population</td><td>$number_of_officials</td><td align='center'><input type='checkbox' name='govts[]' id='govtid' value='$Government_ID' /></td></tr>");
            }
            ?>
            </tbody>
        </table>
    	<input type="submit" name="submit" id="submit" value="Add Selected Governments" />
</form>
<?php
// paging
$nextPage = $p + 1;
if ($p != 0) {
	$prevPage = $p - 1;
	echo("&lt; <a href='?p=$prevPage&jid=$jid'>Prev $numRecords2Show</a> | ");
	echo("<a href='?p=$nextPage&jid=$jid'>Next $numRecords2Show</a> &gt;");
} else {
	echo("<a href='?p=$nextPage&jid=$jid'>Next $numRecords2Show</a> &gt;");
}
?>
</div>

</center>
</body>
</html>