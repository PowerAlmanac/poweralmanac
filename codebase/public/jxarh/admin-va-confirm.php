<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$oid = $_REQUEST['oid'];
$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

// va_confirm(in o_id varchar(100),in job_id bigint,out rc boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL va_confirm('%s',%s,@rc)", escape($oid), escape($jid)));
$pdoObject = $pdo->query("SELECT @rc");
$rsArray = $pdoObject->fetchAll();
$rc = $rsArray[0]['@rc'];

header("Location: admin-editgovt.php?gid=$gid&jid=$jid");
flush();
exit;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VA Confirm</title>
<meta name="robots" content="noindex">
</head>

<body>
<?php
	echo("rc = $rc<br>");
	echo("<a href='va-refresh.php'>continue</a>");
?>
</body>
</html>
