<?php
include("inc-config.php");
include("inc-protected-supervisor.php");

// get all users
$read_sql = "SELECT * FROM employees
		WHERE access_level = 'VoiceAgent'
	";
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from government database: $read_sql" . PowerAlmanac\PDb::error());
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supervisor Access - Users</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
		body { font-size: 62.5%; }
		label, input { display:block; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.5em; margin: .6em 0; }
		div#users-contain { width: 800px; margin: 20px 0;}
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>
	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var f_name = $( "#f_name" ),
			l_name = $( "#l_name" ),
			access_level = $( "#access_level" ),
			primary_role = $( "#primary_role" ),
			userid = $( "#userid" ),
			password = $( "#password" );

		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be between " +
					min + " and " + max + "." );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}
		
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 400,
			width: 400,
			modal: true,
			buttons: {
				"Create an account": function() {
					$.ajax({
						type: "POST",
						url: "add-new-va.php",
						data: "f_name=" + f_name.val() + "&l_name=" + l_name.val() + "&userid=" + userid.val() + "&password=" + password.val() + "&access_level=" + access_level.val() + "&primary_role=" + primary_role.val(),
						//data: "f_name=" + f_name.val() + "&userid=" + userid.val(),
						success: function(msg){
							//$('#dialog-form').html("<a href='admin-users.php'>success</a>");
							//alert('ajax completed' + msg);
							//$( "#dialog-form" ).dialog( "close" );
							// refresh?
							//top.location.href=location.href;
							//location.reload(true);
							window.location = 'supervisor-users.php';
						}
					});
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});
		
		$( "#create-user" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});

	});
	</script>

<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">


<center>
<?php
// navigation
include("nav-supervisor.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>View Voice Agents</h1>
	<table id="users" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
				<th>Employee ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>User ID</th> 
				<th>Password</th>
				<th>Access Level</th>
				<th>Primary Role</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			$emp_id = $row['emp_id'];
			$first_name = $row['first_name'];
			$last_name = $row['last_name'];
			$user_id = $row['user_id'];
			$password = $row['password'];
			//$password = '****';
			$access_level = $row['access_level'];
			$primary_role = $row['primary_role'];
			echo("<tr><td>$emp_id</td><td>$first_name</td><td>$last_name</td><td>$user_id</td><td>$password</td><td>$access_level</td><td>$primary_role</td></tr>");
		}
		?>
		</tbody>
	</table>
</div>
<button id="create-user">Create new Voice Agents</button>

<div id="dialog-form" title="Create new Voice Agent">
	<p class="validateTips">All form fields are required.</p>

	<form>
	<fieldset>
		<label for="first_name">First Name</label>
		<input type="text" name="f_name" id="f_name" class="text ui-widget-content ui-corner-all" />
		<label for="last_name">Last Name</label>
		<input type="text" name="l_name" id="l_name" class="text ui-widget-content ui-corner-all" />
		<label for="userid">User ID</label>
		<input type="text" name="userid" id="userid" value="" class="text ui-widget-content ui-corner-all" />
		<label for="password">Password</label>
		<input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />
        <input type="hidden" name="access_level" id="access_level" value="VoiceAgent" />
        <input type="hidden" name="primary_role" id="primary_role" value="VoiceAgent" />
	</fieldset>
	</form>
</div>

</center>

</div>

</body>
</html>