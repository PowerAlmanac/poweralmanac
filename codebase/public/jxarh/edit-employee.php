<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$employeeID = $_REQUEST['id'];

// init db
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connectIcube("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die("Unable to connect to $dbname database: " . PowerAlmanac\PDb::error());
}

// get user info
$read_sql = sprintf("SELECT * FROM employees
		WHERE emp_id = '%s'
		LIMIT 1
	", escape($employeeID));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);

$emp_id = $onerow['emp_id'];
$first_name = $onerow['first_name'];
$last_name = $onerow['last_name'];
$user_id = $onerow['user_id'];
$password = $onerow['password'];
$access_level = $onerow['access_level'];
$primary_role = $onerow['primary_role'];
$access_levelSelectArray["$access_level"] = 'selected';
$primary_roleSelectArray["$primary_role"] = 'selected';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Employee</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>

<script language="javascript" type="text/javascript" src="niceforms.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="niceforms-default.css" />

<meta name="robots" content="noindex">
</head>

<body>

<center>
<?php
// navigation
include("nav-admin.php");
?>
</center>
<br />

<div id="container">
<form action="update-employee.php" method="post" class="niceform">
<input type="hidden" name="emp_id" value="<?php echo($emp_id); ?>" />
	<fieldset>
    	<legend>Edit User</legend>
        <dl>
        	<dt><label for="f_name">First Name:</label></dt>
            <dd><input type="text" name="f_name" id="f_name" size="40" maxlength="128" value="<?php echo($first_name); ?>" /></dd>
        </dl>
        <dl>
        	<dt><label for="l_name">Last Name:</label></dt>
            <dd><input type="text" name="l_name" id="l_name" size="40" maxlength="128" value="<?php echo($last_name); ?>" /></dd>
        </dl>
        <dl>
        	<dt><label for="userid">User ID:</label></dt>
            <dd><input type="text" name="userid" id="userid" size="40" maxlength="128" value="<?php echo($user_id); ?>" /></dd>
        </dl>
        <dl>
        	<dt><label for="password">Password:</label></dt>
            <dd><input type="password" name="password" id="password" size="40" maxlength="128" value="<?php echo($password); ?>" /></dd>
        </dl> 
        <dl>
        	<dt><label for="access_level">Access Level:</label></dt>
            <dd>
            	<select size="1" name="access_level" id="access_level">
                    <option value="VoiceAgent" <?php echo($access_levelSelectArray['VoiceAgent']); ?>>Voice Agent</option>
                    <option value="Supervisor" <?php echo($access_levelSelectArray['Supervisor']); ?>>Supervisor</option>
                    <option value="Administrator" <?php echo($access_levelSelectArray['Administrator']); ?>>Administrator</option>
            	</select>
            </dd>
        </dl>
        <dl>
        	<dt><label for="primary_role">Primary Role:</label></dt>
            <dd>
            	<select size="1" name="primary_role" id="primary_role">
                    <option value="VoiceAgent" <?php echo($primary_roleSelectArray['VoiceAgent']); ?>>Voice Agent</option>
                    <option value="ProcessInitiator" <?php echo($primary_roleSelectArray['ProcessInitiator']); ?>>Process Initiator</option>
                    <option value="QualityChecker" <?php echo($primary_roleSelectArray['QualityChecker']); ?>>Quality Checker</option>
                    <option value="FinalApprover" <?php echo($primary_roleSelectArray['FinalApprover']); ?>>Final Approver</option>
                    <option value="DataUploader" <?php echo($primary_roleSelectArray['DataUploader']); ?>>Data Uploader</option>
            	</select>
            </dd>
        </dl>
    </fieldset>
    <fieldset class="action">
    	<input type="submit" name="submit" id="submit" value="Submit" />
    </fieldset>
</form>
</div>
</body>
</html>
