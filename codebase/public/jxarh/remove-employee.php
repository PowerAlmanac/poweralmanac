<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$employeeID = $_REQUEST['id'];

// remove_employee(IN EmpID BIGINT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL remove_employee(%s)", escape($employeeID)));

// back to admin-users.php

header("Location: admin-users.php");
flush();
exit;

echo("Removed $employeeID");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Remove Employee</title>
<meta name="robots" content="noindex">
</head>

<body>

</body>
</html>
