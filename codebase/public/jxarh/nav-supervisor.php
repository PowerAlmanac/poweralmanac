<?php

?>

	<script>
	$(function() {
		$( ".supervisor_nav a:first" ).button({
            icons: {
                primary: "ui-icon-folder-collapsed"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-person"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-folder-collapsed"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-info"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-power"
            }
        });
	});
	</script>
    
<div class="supervisor_nav">

<a href="dashboard-supervisor.php">My Active Tasks</a>
<a href="supervisor-users.php">View Voice Agents</a>
<a href="supervisor-productivity.php">View Productivity</a>
<a href="help.php" target="_blank">Help</a>
<a href="logout.php">Log Out</a>

</div>
