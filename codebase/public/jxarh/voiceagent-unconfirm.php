<?php
include("inc-config.php");

$oid = $_REQUEST['oid'];
$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

// va_confirm(in o_id varchar(100),in job_id bigint,out rc boolean)
// va_change_mind(in o_id varchar(100),in job_id bigint)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL va_change_mind('%s',%s)", escape($oid), escape($jid)));

header("Location: voiceagent-editgovt.php?gid=$gid&jid=$jid");
flush();
exit;

?>