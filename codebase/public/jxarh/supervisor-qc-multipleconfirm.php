<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$govtsArray = $_REQUEST['govts']; // array

if (!isset($_REQUEST['govts'])) {
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

//print_r($_REQUEST); exit;

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// loop thru govts
foreach ($govtsArray as $gid_jid) 
{
	// explode
	list($gid, $jid) = explode(",", $gid_jid);
	$pdo->query(sprintf("CALL qc_confirm(%s,'%s',@rc)", escape($jid), escape($gid)));
	$pdoObject = $pdo->query("SELECT @rc");
	$rsArray = $pdoObject->fetchAll();
	$rc = $rsArray[0]['@rc'];
}

header("Location: dashboard-supervisor.php");
flush();
exit;

?>