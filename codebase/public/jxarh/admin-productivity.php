<?php
include("inc-config.php");
include("inc-protected-admin.php");

// dates
$show = $_REQUEST['show'];
$date_start = $_REQUEST['datestart'];
$date_end = $_REQUEST['dateend'];
if ($show == '') $show = 1;
// get all users
$read_sql = "SELECT * FROM employees
		WHERE access_level = 'VoiceAgent'
	";
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Access - Productivity</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<meta name="robots" content="noindex">

<script>
$(function() {
	$( "#datestart" ).datepicker({
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "img/calendar.gif",
		buttonImageOnly: true
	});
	$( "#dateend" ).datepicker({
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "img/calendar.gif",
		buttonImageOnly: true
	});
});
</script>

</head>

<body>

<div class="demo">

<center>
<?php
// navigation
include("nav-admin.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Voice Agents Productivity</h1>
	<table id="users" class="ui-widget ui-widget-content">
        <?php
		//$today = strtotime(date('M j, Y'));
		//$yesterday = $today - 86400;
	
		$yesterdayStart = date("Y-m-d",$yesterday) . ' 00:00:00';
		$yesterdayEnd = date("Y-m-d",$yesterday) . ' 23:59:59';
		$todayIs = date("Y-m-d");
		$todayStart = date("Y-m-d") . ' 00:00:00';
		$todayEnd = date("Y-m-d") . ' 23:59:59';
			
		if ($show == '1') {
			$dateStart = $todayStart;
			$dateEnd = $todayEnd;
			$dateStart_display = $todayIs;
			$dateEnd_display = $todayIs;
		} else {
			$dateStart = $date_start . ' 00:00:00';
			$dateEnd = $date_end . ' 23:59:59';
			$dateStart_display = $date_start;
			$dateEnd_display = $date_end;
		}
		?>
		<thead>
        <tr>
            <th bgcolor='#42D0FF'><a href="?show=1">Today</a> (<?php echo($todayIs); ?>)</th>
            <form action="admin-productivity.php" method="get">
            <input type="hidden" name="show" value="2" />
            <th>Start Date: <input name="datestart" type="text" id="datestart" value="<?php echo($dateStart_display); ?>"  style="padding-right:5px;padding-left:5px;" /></th><th>End Date: <input name="dateend" type="text" id="dateend" value="<?php echo($dateEnd_display); ?>"  style="padding-right:5px;padding-left:5px;" /></th><th><input type="submit" value="Refresh Date Range" /></th><th>&nbsp;</th>
            </form>
			</tr>
			<tr class="ui-widget-header ">
				<th>Employee ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>User ID</th>
                <th>#Govts Confirmed</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$totalGovtsConfirmed = 0;
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			$emp_id = $row['emp_id'];
			$first_name = $row['first_name'];
			$last_name = $row['last_name'];
			$user_id = $row['user_id'];

			$readUser_sql = sprintf("SELECT * FROM va_gov_confirms_log
					WHERE va_id = '%s' AND va_datetime  >= '%s' AND va_datetime  <= '%s'
				", escape($emp_id), escape($dateStart), escape($dateEnd));
			$result_readUser = @PowerAlmanac\PDb::query($readUser_sql);
			if (!$result_readUser) {
				die("Error reading from $dbname database: $readUser_sql" . PowerAlmanac\PDb::error());
			}
			
			$numGovtsCOnfirmed = PowerAlmanac\PDb::num_rows($result_readUser);
			$totalGovtsConfirmed = $totalGovtsConfirmed + $numGovtsCOnfirmed;
			echo("<tr><td><b>$emp_id</b></td><td><b>$first_name</b></td><td><b>$last_name</b></td><td><b>$user_id</b></td><td><b>$numGovtsCOnfirmed</b></td></tr>");
		}
		echo("<tr><td><b></b></td><td><b></b></td><td><b></b></td><td><b>TOTAL</b></td><td><b>$totalGovtsConfirmed</b></td></tr>");
		?>
		</tbody>
	</table>
</div>

</center>

</div>

</body>
</html>