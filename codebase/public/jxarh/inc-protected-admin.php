<?php

if ($_SESSION['access_level'] != 'Administrator') {
	$_SESSION['error_message'] = 'Inadequate Access Level. Please log in with the correct userid/password.';	
	header("Location: index.php");
	flush();
}

?>