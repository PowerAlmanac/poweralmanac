<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$gid = $_REQUEST['gid'];
$jid = $_REQUEST['jid'];
// initiator_remove_gov(in g_id varchar(14),in job_id bigint)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL initiator_remove_gov('%s',%s)", escape($gid), escape($jid)));

header("Location: supervisor-editjob.php?jid=$jid");
flush();
exit;

?>
