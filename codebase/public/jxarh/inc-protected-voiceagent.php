<?php

$access_level = $_SESSION['access_level'];
if (($access_level != 'VoiceAgent')){
	//echo("Inadequate Access Level; [$access_level]");
	$_SESSION['error_message'] = 'Inadequate Access Level. Please log in with the correct userid/password.';	
	header("Location: index.php");
	flush();
}

?>