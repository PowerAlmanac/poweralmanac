<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$voiceagent = $_REQUEST['voiceagent'];
$qualitychecker = $_REQUEST['qualitychecker'];
$finalapprover = $_REQUEST['finalapprover'];

// initiator_create_job(IN id BIGINT,OUT job_id BIGINT,out rc boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL initiator_create_job(%s,%s,%s,@job_id,@rc)", escape($voiceagent), escape($qualitychecker), escape($finalapprover)));
$pdoObject = $pdo->query("SELECT @job_id,@rc");
$rsArray = $pdoObject->fetchAll();
//print_r($rsArray);
$job_id = $rsArray[0]['@job_id'];
$rc = $rsArray[0]['@rc'];
if ($rc == 1) {
	echo("Invalid User ID");
}

header("Location: admin-jobs.php");
flush();
exit;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Create Job</title>
<meta name="robots" content="noindex">
</head>

<body>

</body>
</html>
