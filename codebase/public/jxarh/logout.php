<?php
session_start();
include("inc-config.php");

$emp_id = $_SESSION['emp_id'];
$access_level = $_SESSION['access_level'];

$_SESSION['access_level'] = '';
$_SESSION['emp_id'] = '';
$_SESSION['name'] = '';
$_SESSION['error_message'] = '';
$_SESSION['primary_role'] = '';

// log it if VA
if (($access_level == 'VoiceAgent') || ($access_level == 'Supervisor')) {
	$insertLog_sql = sprintf("INSERT INTO valog (VA_ID,timestamp,action)
		VALUES ('%s',now(),'logout')
	", escape($emp_id));
	$result_insertLog = @PowerAlmanac\PDb::query($insertLog_sql);
	if (!$result_insertLog) {
		die('Error writing to valog database:' . PowerAlmanac\PDb::error());
	}
}
header("Location: index.php");
flush();
exit;	

?>