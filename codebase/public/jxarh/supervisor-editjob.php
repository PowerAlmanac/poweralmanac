<?php
include("inc-config.php");
include("inc-protected-supervisor.php");

$jid = $_REQUEST['jid'];

// get job detail
$read_sql = sprintf("SELECT * FROM jobs
		WHERE job_id = '%s'
		LIMIT 1
	", escape($jid));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$job_id = $onerow['job_id'];
$va_id = $onerow['va_id'];
$num_officials = $onerow['num_officials'];
$num_govs = $onerow['num_govs'];
$job_state = $onerow['state'];

// look up voice agent
$readVA_sql = sprintf("SELECT * FROM employees
	WHERE emp_id = '%s'
	LIMIT 1
	", escape($va_id));
$result_readVA = @PowerAlmanac\PDb::query($readVA_sql);
if (!$result_readVA) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readVA);
$first_name = $onerow['first_name'];
$last_name = $onerow['last_name'];
if ($first_name == '') $first_name = 'Not';
if ($last_name == '') $last_name = 'Available';
if ($job_state == '') $job_state = '-';
			
$primary_role = $_SESSION['primary_role'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supervisor - Edit Job</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<script>
	$(function() {
		$( ".zoom a" ).button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
		$( ".ready a" ).button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
		//$( "a", ".zoom" ).click(function() { return false; });
	});
</script>
<meta name="robots" content="noindex">
</head>

<body>

<center>
<?php
// navigation
include("nav-supervisor.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Edit Active Job #<?php echo($job_id); ?></h1>
	<table id="job" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
				<th>Job ID</th>
				<th>Voice Agent ID</th>
				<th>VA Name</th>
				<th>Number of Officials</th> 
				<th>Number of Governments</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody>
        <?php
		echo("<tr><td>$job_id</td><td>$va_id</td><td>$first_name $last_name</td><td>$num_officials</td><td>$num_govs</td><td>$job_state</td></tr>");
		?>
		</tbody>
	</table>
    <?php
	// check if at least one goverment (how?) added and state in NULL
	if (($job_state == '-')) {
		// check governments in VA table
		$checkGovtSQL =sprintf( "SELECT * FROM va_%s", escape($job_id));
		$result_checkGovt = @PowerAlmanac\PDb::query($checkGovtSQL);
		if (!$result_checkGovt) {
			die('Error reading from va_$job_id database:' . PowerAlmanac\PDb::error());
		}
		$numGovt = number_format(PowerAlmanac\PDb::num_rows($result_checkGovt));
		//echo("DEBUG: numGovt = $numGovt<br>");
		// add ready submission
		if ($numGovt != '0') {
			echo("<div class='ready'><a href='supervisor-job-ready.php?jid=$job_id'>Make job available</a></div>");
		}
	}
	//for each government, list officials, profile info
	// get all govts
	$checkSQL = sprintf("show tables from %s like '___%s'", escape($dbname), escape($job_id));
	$result_checkTable = @PowerAlmanac\PDb::query($checkSQL);
	if (!$result_checkTable) {
		die('Error reading from $dbname database:' . PowerAlmanac\PDb::error());
	}
	$numTables = number_format(PowerAlmanac\PDb::num_rows($result_checkTable));
	?>
	<table id="details" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
				<th>Actions</th>
				<th>Government Summary</th>
				<th>Number of Officials</th> 
                <th>Records State</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($grow = PowerAlmanac\PDb::fetch_array($result_checkTable))
		{
			$tableID = $grow[0];
			// get record state
			$recordState = substr($tableID,0,2);
			if ($recordState == 'qc') {
				$recordState = 'CONFIRMED (QC)';
			}
			if ($recordState == 'va') {
				$recordState = 'PENDING';
			}
			if ($recordState == 'fa') {
				$recordState = 'CONFIRMED (FA)';
			}
			if ($recordState == 'done') {
				$recordState = 'CONFIRMED (DONE)';
			}
			$readgovt_sql = sprintf("SELECT DISTINCT Government_ID FROM %s", escape($tableID));
			$result_readgovt = @PowerAlmanac\PDb::query($readgovt_sql);
			if (!$result_readgovt) {
				die("Error reading from $tableID database: $readgovt_sql" . PowerAlmanac\PDb::error());
			}
			while ($row = PowerAlmanac\PDb::fetch_array($result_readgovt))
			{
				$Government_ID = $row['Government_ID'];
				// get # officials
				$readgovtoff_sql = sprintf("SELECT * FROM %s WHERE Government_ID = '%s'", escape($tableID), escape($Government_ID));
				$result_readgovtoff = @PowerAlmanac\PDb::query($readgovtoff_sql);
				if (!$result_readgovtoff) {
					die("Error reading from $tableID database: $readgovtoff_sql" . PowerAlmanac\PDb::error());
				}
				$num_officials = PowerAlmanac\PDb::num_rows($result_readgovtoff);
				// get additonal government info
				$govtrow = PowerAlmanac\PDb::fetch_array($result_readgovtoff);
				$Government_Place_Name = $govtrow['Government_Place_Name'];
				$Government_Type_Name = $govtrow['Government_Type_Name'];
				$Mailing_City = $govtrow['Mailing_City'];
				$Mailing_State = $govtrow['Mailing_State'];
				if ($recordState == 'PENDING') {
					if ($primary_role == 'ProcessInitiator') {
						echo("<tr><td><a href='supervisor-remove-govts.php?gid=$Government_ID&jid=$job_id'>Delete</a></td><td>$Government_Place_Name ($Government_Type_Name) - $Mailing_City, $Mailing_State</td><td>$num_officials</td><td>$recordState</td></tr>");
					} else {
						echo("<tr><td>Pending</td><td>$Government_Place_Name ($Government_Type_Name) - $Mailing_City, $Mailing_State</td><td>$num_officials</td><td>$recordState</td></tr>");
					}
				} else {
					if (($recordState == 'CONFIRMED (QC)') && ($primary_role == 'QualityChecker')){
						echo("<tr><td><a href='supervisor-editgovt.php?gid=$Government_ID&jid=$job_id'>Edit</a></td><td>$Government_Place_Name ($Government_Type_Name) - $Mailing_City, $Mailing_State</td><td>$num_officials</td><td>$recordState</td></tr>");
					} else {
						if (($recordState == 'CONFIRMED (FA)') && ($primary_role == 'FinalApprover')){
							echo("<tr><td><a href='supervisor-editgovt.php?gid=$Government_ID&jid=$job_id'>Edit</a></td><td>$Government_Place_Name ($Government_Type_Name) - $Mailing_City, $Mailing_State</td><td>$num_officials</td><td>$recordState</td></tr>");
						} else {
							echo("<tr><td>DONE</td><td>$Government_Place_Name ($Government_Type_Name) - $Mailing_City, $Mailing_State</td><td>$num_officials</td><td>$recordState</td></tr>");
						}
					}
				}
			}
		}
		?>
		</tbody>
	</table>
    <?php
	// can only add governments if in NULL state
	if ($job_state == '-') {
		echo("<div class='zoom'><a href='supervisor-addgovt.php?jid=$job_id'>Add a government</a></div>");
	}
	?>
</div>

</center>

</body>
</html>