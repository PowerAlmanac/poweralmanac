<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$govtsArray = $_REQUEST['govts']; // array
$voiceagent = $_REQUEST['voiceagent'];
$qualitychecker = $_REQUEST['qualitychecker'];
$finalapprover = $_REQUEST['finalapprover'];

if (!isset($_REQUEST['govts'])) {
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

//print_r($_REQUEST); exit;

// create a new job and then add a single govt. finally, make job available.

// initiator_create_job(IN id BIGINT,OUT job_id BIGINT,out rc boolean)
// initiator_add_gov(in g_id varchar(14),in job_id bigint,out rc boolean)
// initiator_job_ready(in id BIGINT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// loop thru govts
foreach ($govtsArray as $gid) 
{
	$pdo->query(sprintf("CALL initiator_create_job(%s,%s,%s,@job_id,@rc)", escape($voiceagent), escape($qualitychecker), escape($finalapprover)));
	$pdoObject = $pdo->query("SELECT @job_id,@rc");
	$rsArray = $pdoObject->fetchAll();
	$job_id = $rsArray[0]['@job_id'];
	
	$pdo->query(sprintf("CALL initiator_add_gov('%s',%s,@rc3)", escape($gid), escape($job_id)));
	$pdoObject2 = $pdo->query("SELECT @rc3");
	$rsArray2 = $pdoObject2->fetchAll();
	$rc3 = $rsArray2[0]['@rc3'];
	
	if ($rc3 == '0') {
		$pdo->query(sprintf("CALL initiator_job_ready(%s)", escape($job_id)));
	}
}

header("Location: dashboard-supervisor.php");
flush();
exit;

?>