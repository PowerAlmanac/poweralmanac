<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$gid = $_REQUEST['gid'];
$jid = $_REQUEST['jid'];
// initiator_remove_gov(in g_id varchar(14),in job_id bigint)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL initiator_remove_gov('%s',%s)", escape($gid), escape($jid)));

header("Location: admin-editjob.php?jid=$jid");
flush();
exit;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Remove Govts</title>
<meta name="robots" content="noindex">
</head>

<body>

</body>
</html>
