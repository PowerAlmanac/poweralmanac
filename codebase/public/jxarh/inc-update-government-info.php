<?php

$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

$phone = str_replace("-","",$_REQUEST['phone']);
$webaddress = $_REQUEST['webaddress'];
$updatedweb = str_replace('"','',str_replace("'","\'",$_REQUEST['updatedweb']));
$address = str_replace('"','',str_replace("'","\'",$_REQUEST['address']));
$city = str_replace("'","\'",$_REQUEST['city']);
$zip = $_REQUEST['zip'];
$takenfrom = str_replace('"','',str_replace("'","\'",$_REQUEST['takenfrom']));
$electionmonth = $_REQUEST['electionmonth'];

$p_phone = str_replace("-","",$_REQUEST['p_phone']);
$p_webaddress = $_REQUEST['p_webaddress'];
$p_updatedweb = str_replace('"','',str_replace("'","\'",$_REQUEST['p_updatedweb']));
$p_address = str_replace('"','',str_replace("'","\'",$_REQUEST['p_address']));
$p_city = str_replace("'","\'",$_REQUEST['p_city']);
$p_zip = $_REQUEST['p_zip'];
$p_takenfrom = str_replace('"','',str_replace("'","\'",$_REQUEST['p_takenfrom']));
$p_electionmonth = $_REQUEST['p_electionmonth'];

// using Official_ID = Government_ID for government info
// read changes from before if any
$readGovtNotes_sql = sprintf("SELECT * FROM notes WHERE Government_ID = '%s' AND Job_ID = '%s' AND Official_ID = '%s'
	LIMIT 1
	", escape($gid), escape($jid), escape($gid));
$result_readGovtNotes = @PowerAlmanac\PDb::query($readGovtNotes_sql);
if (!$result_readGovtNotes) {
	die("Error reading from notes database: $readGovtNotes_sql" . PowerAlmanac\PDb::error());
}
$numNotes = PowerAlmanac\PDb::num_rows($result_readGovtNotes);
$numGovtFields = 8;
if ($numNotes == 0) { // first time
	$notesArray = array(0,0,0,0,0,0,0,0);
} else {
	$onerow = PowerAlmanac\PDb::fetch_array($result_readGovtNotes);
	$notes = $onerow['txt'];
	$notesArray = explode(",", $notes);
			$numNotesArray = count($notesArray);
			if ($numNotesArray < $numGovtFields) {
				array_push($notesArray,"0");
			}
}

// compare and tag additional changes
$government_updates = '';

if ($p_phone != $phone) {
	$notesArray[0] = 1;
}
if ($p_webaddress != $webaddress) {
	$notesArray[1] = 1;
}
if ($p_updatedweb != $updatedweb) {
	$notesArray[2] = 1;
}
if ($p_address != $address) {
	$notesArray[3] = 1;
}
if ($p_city != $city) {
	$notesArray[4] = 1;
}
if ($p_zip != $zip) {
	$notesArray[5] = 1;
}
if ($p_takenfrom != $takenfrom) {
	$notesArray[6] = 1;
}
if ($p_electionmonth != $electionmonth) {
	$notesArray[7] = 1;
}

// generate notes into CSV

for ($i = 0; $i < $numGovtFields; $i++) {
	if ($i != ($numGovtFields - 1)) {
		$government_updates = $government_updates . $notesArray[$i] . ',';
	} else {
		$government_updates = $government_updates . $notesArray[$i];
	}
}

//print_r($government_updates); exit;

if ($numNotes == 0) {
	// insert
	$insertGovtNotes_sql = sprintf("INSERT INTO notes (Government_ID,Job_ID,Official_ID,timestamp,txt,Writer_ID)
		VALUES ('%s','%s','%s',now(),'%s','0')
	", escape($gid), escape($jid), escape($gid), escape($government_updates));
	$result_insertGovtNotes = @PowerAlmanac\PDb::query($insertGovtNotes_sql);
	if (!$result_insertGovtNotes) {
		die('Error writing to notes database:' . PowerAlmanac\PDb::error());
	}
} else {
	// update
	$updateGovtNotes_sql = sprintf("UPDATE notes SET txt = '%s'
		WHERE Government_ID = '%s' AND Job_ID = '%s' AND Official_ID = '%s'
		", escape($government_updates), escape($gid), escape($jid), escape($gid));
	$result_updateGovtNotes = @PowerAlmanac\PDb::query($updateGovtNotes_sql);
	if (!$result_updateGovtNotes) {
		die('<p>Error updating notes database: '. PowerAlmanac\PDb::error().'</p>');
	}
}

//echo("[CALL all_update_government('$gid',$jid,'$webaddress','$takenfrom','$updatedweb','$phone','$address','$city','$zip',@rc)]"); exit;
//print_r($_REQUEST); exit;

// all_update_government(in g_id varchar(100),in job_id bigint,in v_Government_Web_Address varchar(255),in v_Taken_from varchar(500),in v_Updated_website varchar(10),in v_Government_PhoneNumber varchar(30),in v_Address_Street_Box varchar(255),in v_City varchar(255),in v_Zip_Code varchar(5),out rc boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL all_update_government('%s',%s,'%s','%s','%s','%s','%s','%s','%s','%s',@rc)",
    escape($gid), escape($jid), escape($webaddress), escape($takenfrom), escape($updatedweb), escape($phone), escape($address), escape($city), escape($zip), escape($electionmonth)));

?>