<?php

?>

	<script>
	$(function() {
		$( ".voiceagent_nav a:first" ).button({
            icons: {
                primary: "ui-icon-folder-collapsed"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-folder-collapsed"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-info"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-power"
            }
        });
	});
	</script>
    
<div class="voiceagent_nav">

<a href="dashboard-voiceagent.php">My Active Tasks</a>
<a href="voiceagent-productivity.php">View Productivity</a>
<a href="help.php" target="_blank">Help</a>
<a href="logout.php">Log Out</a>

</div>