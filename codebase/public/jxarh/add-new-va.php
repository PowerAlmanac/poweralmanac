<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$f_name = str_replace("'","\'",$_REQUEST['f_name']);
$l_name = str_replace("'","\'",$_REQUEST['l_name']);
$userid = $_REQUEST['userid'];
$password = $_REQUEST['password'];
$access_level = $_REQUEST['access_level'];
$primary_role = $_REQUEST['primary_role'];

// add_new_employee(IN FiratName varchar(100),IN LastName varchar(100),IN UserID varchar(100),IN Pass varchar(100),IN AccessLevel varchar(100),IN PrimaryRole varchar(100),OUT id BIGINT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL add_new_employee('%s','%s','%s','%s','%s','%s',@uniqueid)", escape($f_name), escape($l_name), escape($userid), escape($password), escape($access_level), escape($primary_role)));
$pdoObject = $pdo->query("SELECT @uniqueid");
$rsArray = $pdoObject->fetchAll();
//print_r($rsArray);
$uniqueid = $rsArray[0]['@uniqueid'];
	
?>
