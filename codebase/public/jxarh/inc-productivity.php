<?php

?>
<table id="productivity" class="ui-widget ui-widget-content" width="1000">
    <thead>
        <tr class="ui-widget-header ">
            <th>Productivity</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td valign="top" align="left">
            <b>Applies to Voice Agent Roles only</b>.
            <br />
            Productivity is defined to be the <b>AVERAGE</b> per first confirm (of any Government official record). Thsi is calculated according to the deltas (elapsed time) from <b>confirm</b> to <b>confirm</b> in SECONDS.
            </td>
        </tr>
    </tbody>
</table>
