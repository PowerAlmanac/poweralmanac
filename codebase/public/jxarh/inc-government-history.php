	<a name="history"></a><b>Government History</b>
    <?php
	// Job_ID  timestamp  txt  Writer_ID
	$readgovtHistory_sql = sprintf("SELECT * FROM notes
		WHERE Government_ID = '%s' AND Writer_ID != '0'
		ORDER BY ID DESC
		", escape($gid));
	$result_readgovtHistory = @PowerAlmanac\PDb::query($readgovtHistory_sql);
	if (!$result_readgovtHistory) {
		die("Error reading from notes database: $readgovtHistory_sql" . PowerAlmanac\PDb::error());
	}
	$numNotes = PowerAlmanac\PDb::num_rows($result_readgovtHistory);
	?>
	<table id="govtnotes" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
           		<!--
                <th width="30">Task#</th>
                -->
                <th width="100">Date/Time</th>
				<th width="300">Notes</th>
                <!--
                <th width="100">Written By (ID)</th>
                -->
                <th width="100">Written By</th>
			</tr>
		</thead>
		<tbody>
        <?php
		//echo("DEBUG: numNotes = $numNotes<br>");
		if ($numNotes > 0) {
			while ($nrow = PowerAlmanac\PDb::fetch_array($result_readgovtHistory))
			{
				$Job_ID = $nrow['Job_ID'];
				$timestamp = date("M j, Y G:i",strtotime($nrow['timestamp']));
				$txt = $nrow['txt'];
				$Writer_ID = $nrow['Writer_ID'];
				if ($Writer_ID != '0') {
					// look up writer
					$readWriter_sql = sprintf("SELECT * FROM employees
						WHERE emp_id = '%s'
						LIMIT 1
						", escape($Writer_ID));
					$result_readWriter = @PowerAlmanac\PDb::query($readWriter_sql);
					if (!$result_readWriter) {
						die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
					}
					$wonerow = PowerAlmanac\PDb::fetch_array($result_readWriter);
					$Writerfirst_name = $wonerow['first_name'];
					$Writerlast_name = $wonerow['last_name'];
					if ($Writerfirst_name == '') $Writerfirst_name = 'Not';
					if ($Writerlast_name == '') $Writerlast_name = 'Available';
					//echo("<tr><td>$Job_ID</td><td>$timestamp</td><td>$txt</td><td>$Writer_ID</td><td>$Writerfirst_name $Writerlast_name</td></tr>");
					echo("<tr><td align='center'>$timestamp</td><td align='left'>$txt</td><td align='center'>$Writerfirst_name $Writerlast_name</td></tr>");
				}
			}
		} else {
			echo("<tr><td colspan='6' align='center'>No notes available</td></tr>");
		}
		?>
		</tbody>
	</table>