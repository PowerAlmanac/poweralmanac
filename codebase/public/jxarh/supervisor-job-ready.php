<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$jid = $_REQUEST['jid'];
// initiator_job_ready(in id BIGINT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
//$pdo->query("CALL initiator_job_ready(5)");

$pdo->query(sprintf("CALL initiator_job_ready(%s)", escape($jid)));

header("Location: dashboard-supervisor.php");
flush();
exit;

?>