<?php
include("inc-config.php");
include("inc-protected-supervisor.php");

// get all jobs
$read_sql = "SELECT * FROM jobs
		ORDER BY state
	";
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}

$primary_role = $_SESSION['primary_role'];
$emp_id = $_SESSION['emp_id'];

// get total outstanding jobs
$numJobsALL = PowerAlmanac\PDb::num_rows($result_read);

// get individual counts
$readVAJobs_sql = "SELECT * FROM jobs
		WHERE state = 'va'
	";
$result_readVAJobs = @PowerAlmanac\PDb::query($readVAJobs_sql);
if (!$result_readVAJobs) {
	die("Error reading from $dbname database: $readVAJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsVA = PowerAlmanac\PDb::num_rows($result_readVAJobs);
$numJobsVAPCt = number_format(($numJobsVA/$numJobsALL)*100,2);

$readQCJobs_sql = "SELECT * FROM jobs
		WHERE state = 'qc'
	";
$result_readQCJobs = @PowerAlmanac\PDb::query($readQCJobs_sql);
if (!$result_readQCJobs) {
	die("Error reading from $dbname database: $readQCJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsQC = PowerAlmanac\PDb::num_rows($result_readQCJobs);
$numJobsQCPCt = number_format(($numJobsQC/$numJobsALL)*100,2);

$readFAJobs_sql = "SELECT * FROM jobs
		WHERE state = 'fa'
	";
$result_readFAJobs = @PowerAlmanac\PDb::query($readFAJobs_sql);
if (!$result_readFAJobs) {
	die("Error reading from $dbname database: $readFAJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsFA = PowerAlmanac\PDb::num_rows($result_readFAJobs);
$numJobsFAPCt = number_format(($numJobsFA/$numJobsALL)*100,2);

$readDUJobs_sql = "SELECT * FROM jobs
		WHERE state = 'done'
	";
$result_readDUJobs = @PowerAlmanac\PDb::query($readDUJobs_sql);
if (!$result_readDUJobs) {
	die("Error reading from $dbname database: $readDUJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsDU = PowerAlmanac\PDb::num_rows($result_readDUJobs);
$numJobsDUPCt = number_format(($numJobsDU/$numJobsALL)*100,2);

// overall

$readGovernments_sql = "SELECT DISTINCT Government_ID FROM full_data
	";
$result_readGovernments = @PowerAlmanac\PDb::query($readGovernments_sql);
if (!$result_readGovernments) {
	die("Error reading from $dbname database: $readGovernments_sql" . PowerAlmanac\PDb::error());
}
$numAllGovernments = PowerAlmanac\PDb::num_rows($result_readGovernments);

$readAllocatedJobs_sql = "SELECT DISTINCT Government_ID FROM full_data
		WHERE r_state = 'allocated'
	";
$result_readAllocatedJobs = @PowerAlmanac\PDb::query($readAllocatedJobs_sql);
if (!$result_readAllocatedJobs) {
	die("Error reading from $dbname database: $readAllocatedJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsAllocated = PowerAlmanac\PDb::num_rows($result_readAllocatedJobs);
$numJobsAllocatedPCt = number_format(($numJobsAllocated/$numAllGovernments)*100,2);

$readUNAllocatedJobs_sql = "SELECT DISTINCT Government_ID FROM full_data
		WHERE r_state = 'unallocated'
	";
$result_readUNAllocatedJobs = @PowerAlmanac\PDb::query($readUNAllocatedJobs_sql);
if (!$result_readUNAllocatedJobs) {
	die("Error reading from $dbname database: $readUNAllocatedJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsUNAllocated = PowerAlmanac\PDb::num_rows($result_readUNAllocatedJobs);
$numJobsUNAllocatedPCt = number_format(($numJobsUNAllocated/$numAllGovernments)*100,2);

function decodeTimeZone($tz) {
	/*
	4	Atlantic (GMT -04:00)
	5	Eastern (GMT -05:00)
	6	Central (GMT -06:00)
	7	Mountain (GMT -07:00)
	8	Pacific (GMT -08:00)
	9	Alaska (GMT -09:00)
	10	Hawaii-Aleutian Islands (GMT -10:00)
	11	American Samoa (GMT -11:00)
	14	Guam (GMT +10:00)
	15	Palau (GMT +9:00)
	*/
	switch($tz) {
		case '4':
			$timezone = 'Atlantic';
			break;
		case '5':
			$timezone = 'Eastern';
			break;
		case '6':
			$timezone = 'Central';
			break;
		case '7':
			$timezone = 'Mountain';
			break;
		case '8':
			$timezone = 'Pacific';
			break;
		case '9':
			$timezone = 'Alaska';
			break;
		case '10':
			$timezone = 'Hawaii-Aleutian Islands';
			break;
		case '11':
			$timezone = 'American Samoa';
			break;
		case '14':
			$timezone = 'Guam';
			break;
		case '15':
			$timezone = 'Palau';
			break;
		default: // unknown
			$timezone = $tz;
			break;
	}
	return $timezone;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Supervisor</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var voiceagent = $( "#voiceagent" );
		var qualitychecker = $( "#qualitychecker" );
		var finalapprover = $( "#finalapprover" );
		
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 250,
			width: 400,
			modal: true,
			buttons: {
				"Create a new Task": function() {
					//alert('submit');
					$.ajax({
						type: "POST",
						url: "supervisor-create-job.php",
						data: "voiceagent=" + voiceagent.val() + "&qualitychecker=" + qualitychecker.val() + "&finalapprover=" + finalapprover.val(),
						success: function(msg){
							//alert('ajax completed' + msg);
							//$( "#dialog-form" ).dialog( "close" );
							// refresh
							window.location = 'dashboard-supervisor.php';
						}
					});
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});
		
		$( "#create-job" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});

	});
	</script>

	<script>
	$(function() {
		$( ".task a" ).button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
	});
	</script>
    
<script type="text/javascript">

function selectAllQC()
{
	//alert('selectAll');
	checkAll(document.qcform.govts);
}

function unselectAllQC()
{
	//alert('unselectAll');
	uncheckAll(document.qcform.govts);
}

function selectAll()
{
	//alert('selectAll');
	checkAll(document.faform.govts);
}

function unselectAll()
{
	//alert('unselectAll');
	uncheckAll(document.faform.govts);
}

function checkAll(field)
{
	for (i = 0; i < field.length; i++)
		//alert(field[i].checked);
		field[i].checked = true ;
		//field[i].className = "NFCheck NFh";
}

function uncheckAll(field)
{
	for (i = 0; i < field.length; i++)
		field[i].checked = false ;
}

</script>   
<script>
$(function() {
	$( "input:submit" ).button();
});
</script>
<meta name="robots" content="noindex">
</head>

<body>

<center>
<?php
// navigation
include("nav-supervisor.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Supervisor Dashboard</h1>
    <h2>Tasks In Progress</h2>
    <div class="task">
    <?php
	if ($primary_role == 'ProcessInitiator') {
		echo('<a href="supervisor-createtask.php">Create new Task</a>');
	}
	?>
    </div>
    
	<table id="jobssummary" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Active Task Status</th>
				<th>Total # Governments</th>
				<th>% of Total Active</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$numJobsVAFMT = number_format($numJobsVA);
		$numJobsQCFMT = number_format($numJobsQC);
		$numJobsFAFMT = number_format($numJobsFA);
		$numJobsDUFMT = number_format($numJobsDU);
		echo("<tr><td>Voice Agent</td><td>$numJobsVAFMT</td><td>$numJobsVAPCt%</td></tr>");	
		echo("<tr><td>Quality Checker</td><td>$numJobsQCFMT</td><td>$numJobsQCPCt%</td></tr>");
		echo("<tr><td>Final Approver</td><td>$numJobsFAFMT</td><td>$numJobsFAPCt%</td></tr>");
		echo("<tr><td>Done Updating</td><td>$numJobsDUFMT</td><td>$numJobsDUPCt%</td></tr>");
		?>
 		</tbody>
		<thead>
			<tr class="ui-widget-header ">
            	<th>Government Database</th>
				<th>Total # Governments</th>
				<th>% of Total</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$numJobsAllocatedFMT = number_format($numJobsAllocated);
		$nnumJobsUNAllocatedFMT = number_format($numJobsUNAllocated);
		echo("<tr><td>Allocated Governments</td><td>$numJobsAllocatedFMT</td><td>$numJobsAllocatedPCt%</td></tr>");	
		echo("<tr><td>UnAllocated Governments</td><td>$nnumJobsUNAllocatedFMT</td><td>$numJobsUNAllocatedPCt%</td></tr>");
		?>
		</tbody>
	</table>
    
    <?php
	if ($primary_role != 'ProcessInitiator') {
		switch($primary_role) {
			case 'QualityChecker':
				$readQC_sql = "SELECT * FROM jobs
						WHERE state = 'qc'
						ORDER BY job_id
					";
				$result_read = @PowerAlmanac\PDb::query($readQC_sql);
				if (!$result_read) {
					die("Error reading from $dbname database: $readQC_sql" . PowerAlmanac\PDb::error());
				}
				$readQCDone_sql = "SELECT * FROM jobs
						WHERE state = 'fa'
						ORDER BY job_id
					";
				$result_readDone = @PowerAlmanac\PDb::query($readQCDone_sql);
				if (!$result_readDone) {
					die("Error reading from $dbname database: $readQCDone_sql" . PowerAlmanac\PDb::error());
				}
				$subHeaderCurrent = 'Tasks in QC Status';
				$subHeaderComplete = 'Tasks in FA Status';
				break;
			case 'FinalApprover':
				$readFA_sql = "SELECT * FROM jobs
						WHERE state = 'fa'
						ORDER BY job_id
					";
				$result_read = @PowerAlmanac\PDb::query($readFA_sql);
				if (!$result_read) {
					die("Error reading from $dbname database: $readFA_sql" . PowerAlmanac\PDb::error());
				}
				$readFADone_sql = "SELECT * FROM jobs
						WHERE state = 'done'
						ORDER BY job_id
					";
				$result_readDone = @PowerAlmanac\PDb::query($readFADone_sql);
				if (!$result_readDone) {
					die("Error reading from $dbname database: $readFADone_sql" . PowerAlmanac\PDb::error());
				}
				$subHeaderCurrent = 'Tasks in FA Status';
				$subHeaderComplete = 'Tasks in DU Status';
				break;
			default: // none for ProcessInitiator and others
				break;
		}
	?>
 	<b><?php echo($subHeaderCurrent); ?></b>
    <?php
    if ($primary_role == 'FinalApprover') {
		echo('<form action="supervisor-fa-multipleconfirm.php" method="post" name="faform" id="faform">');
    	echo('<input type="submit" name="submit" id="submit" value="Confirm SELECTED Tasks" />');
	}
    if ($primary_role == 'QualityChecker') {
		echo('<form action="supervisor-qc-multipleconfirm.php" method="post" name="qcform" id="qcform">');
    	echo('<input type="submit" name="submit" id="submit" value="Confirm SELECTED Tasks" />');
	}
    ?>
	<table id="jobs" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Actions</th>
				<th>Task#</th>
				<th>Govt ID</th>
				<th>Govt Name</th>
				<th>Govt Type</th>
				<th>Govt Loc</th>
				<th>Time Zone</th>
				<th># Ofcls</th>
				<th># Edits (%)</th>
                <th>Agent</th>
				<?php
                if ($primary_role == 'FinalApprover') {
                    echo('<th align="center">&nbsp;Select<br /><a href="javascript: void(0)" onclick="selectAll();">ALL</a><br /><a href="javascript: void(0)" onclick="unselectAll();">NONE</a>&nbsp;</th>');
				}
                if ($primary_role == 'QualityChecker') {
                    echo('<th align="center">&nbsp;Select<br /><a href="javascript: void(0)" onclick="selectAllQC();">ALL</a><br /><a href="javascript: void(0)" onclick="unselectAllQC();">NONE</a>&nbsp;</th><th align="center">DL<br />CSV</th>');
				}
                ?>
                <!--
				<th><img src='img/blockstates.png' alt='state' width="125" height="20" border="1" title='state' /></th>
                -->
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			$job_id = $row['job_id'];
			$va_id = $row['va_id'];
			$qc_id = $row['qc_id'];
			$fa_id = $row['fa_id'];
			$num_officials = $row['num_officials'];
			$num_govs = $row['num_govs'];
			$job_state = $row['state'];
			if ($job_state != 'done') {
				$tableID = $job_state . '_' .  $job_id;
				// look up Govt Info
				$readgovt_sql = sprintf("SELECT DISTINCT Government_ID,state,Government_Place_Name,Government_Type_Name,Government_Type,County_Name,g_timezone FROM %s", escape($tableID));
				$result_readgovt = @PowerAlmanac\PDb::query($readgovt_sql);
				if (!$result_readgovt) {
				    // Ignore jobs where we don't have full info (data is bad - potentially due to only a partial data import)
					continue;
				}
				$onerow = PowerAlmanac\PDb::fetch_array($result_readgovt);
				$Government_ID = $onerow['Government_ID'];
				$state = $onerow['state'];
				$Government_Place_Name = $onerow['Government_Place_Name'];
				$Government_Type_Name = $onerow['Government_Type_Name'];
				$Government_Type = $onerow['Government_Type'];
				$County_Name = $onerow['County_Name'];
				$g_timezone = $onerow['g_timezone'];
				$TZ = decodeTimeZone($g_timezone);
				if ($County_Name != '') {
					$location = "$County_Name, $state";
				} else {
					$location = "$state";
				}
				// look up agent
				$readAgent_sql = sprintf("SELECT * FROM employees
					WHERE emp_id = '%s'
					LIMIT 1
					", escape($va_id));
				$result_readAgent = @PowerAlmanac\PDb::query($readAgent_sql);
				if (!$result_readAgent) {
                    continue;
				}
				$aonerow = PowerAlmanac\PDb::fetch_array($result_readAgent);
				$Agentfirst_name = $aonerow['first_name'];
				$Agentlast_name = $aonerow['last_name'];
				// calculate changes
				/*
				all = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0'
				government = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = '$Government_ID'
				officials = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  != '$Government_ID' AND Official_ID  != ''
				disposition = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = 'DISPOSITION'
				notes = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` != '0'
				*/
				$readOfficialsEdits_sql = sprintf("SELECT * FROM notes
						WHERE Government_ID = '%s' AND Writer_ID = '0' AND Official_ID  != '%s' AND Official_ID  != ''
					", escape($Government_ID), escape($Government_ID));
				$result_readOfficialEdits = @PowerAlmanac\PDb::query($readOfficialsEdits_sql);
				if (!$result_readOfficialEdits) {
					continue;
				}
				$numOfficialEdits = PowerAlmanac\PDb::num_rows($result_readOfficialEdits);
				$numOfficialEditsPCT = number_format(($numOfficialEdits/$num_officials)*100,2);
				// rework
				$readRework_sql = sprintf("SELECT * FROM notes
						WHERE Government_ID = '%s' AND Official_ID = 'REWORK'
						ORDER BY id DESC
						LIMIT 1
					", escape($Government_ID));
				$result_readRework = @PowerAlmanac\PDb::query($readRework_sql);
				if (!$result_readRework) {
                    continue;
				}
				$numRework = PowerAlmanac\PDb::num_rows($result_readRework);
				if ($numRework != 0) {
					$rework = "<img src='img/redflag.png' width='10' height='10' alt='rework' title='rework'>";
				} else {
					$rework = '';
				}
			}
			switch($primary_role) {
				case 'QualityChecker':
					if ($emp_id == $qc_id) {
						echo("<tr><td><a href='supervisor-editgovt.php?gid=$Government_ID&jid=$job_id'>Edit</a> $rework</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$TZ</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$Agentfirst_name $Agentlast_name</td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID,$job_id' /></td><td align='center'><a href='qc-generate-csv.php?gid=$Government_ID&jid=$job_id' title='Download CSV for $Government_Place_Name' target='_blank'><img src='img/button-download.png' border='0'></a></td></tr>");
					} else {
						echo("<tr><td>N/A</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$g_timezone</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$Agentfirst_name $Agentlast_name</td></tr>");
					}
					break;
				case 'FinalApprover':
					if ($emp_id == $fa_id) {
						echo("<tr><td><a href='supervisor-editgovt.php?gid=$Government_ID&jid=$job_id'>Edit</a> $rework</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$TZ</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$Agentfirst_name $Agentlast_name</td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID,$job_id' /></td></tr>");
					} else {
						echo("<tr><td>N/A</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$g_timezone</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$Agentfirst_name $Agentlast_name</td></tr>");
					}
					break;
				default: // none for ProcessInitiator and others
					/*
					if ($job_state != 'done') {
						echo("<tr><td>N/A</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$g_timezone</td><td>$num_officials</td><td><img src='img/blockstates-$job_state.png' alt='$job_state' title='$job_state' border='1'></td></tr>");
					} else {
						echo("<tr><td>Completed</td><td>$job_id</td><td></td><td></td><td></td><td></td><td></td><td>$num_officials</td><td><img src='img/blockstates-$job_state.png' alt='$job_state' title='$job_state' border='1'></td></tr>");
					}
					*/
					break;
			}
		}
		?>
		</tbody>
	</table>
    <h2>Recently Completed Tasks</h2>
    <b><?php echo($subHeaderComplete); ?></b>
	<table id="completedjobs" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Actions</th>
				<th>Task#</th>
				<th>Govt ID</th>
				<th>Govt Name</th>
				<th>Govt Type</th>
				<th>Govt Loc</th>
				<th>Time Zone</th>
				<th># Ofcls</th>
				<th># Edits (%)</th>
                <th>Agent</th>
			</tr>
		</thead>
		<tbody>
        <?php
		$numDone = PowerAlmanac\PDb::num_rows($result_readDone);

		if ($numDone != 0) {
			while ($row = PowerAlmanac\PDb::fetch_array($result_readDone))
			{
				$job_id = $row['job_id'];
				$va_id = $row['va_id'];
				$qc_id = $row['qc_id'];
				$fa_id = $row['fa_id'];
				$num_officials = $row['num_officials'];
				$num_govs = $row['num_govs'];
				$job_state = $row['state'];
				if ($job_state != 'done') {
					$tableID = $job_state . '_' . $job_id;
	
					// look up Govt Info
					$readgovt_sql = sprintf("SELECT DISTINCT Government_ID,state,Government_Place_Name,Government_Type_Name,Government_Type,County_Name,g_timezone  FROM %s", escape($tableID));
					$result_readgovt = @PowerAlmanac\PDb::query($readgovt_sql);
					if (!$result_readgovt) {
                        continue;
					}
					$onerow = PowerAlmanac\PDb::fetch_array($result_readgovt);
					$Government_ID = $onerow['Government_ID'];
					$state = $onerow['state'];
					$Government_Place_Name = $onerow['Government_Place_Name'];
					$Government_Type_Name = $onerow['Government_Type_Name'];
					$Government_Type = $onerow['Government_Type'];
					$County_Name = $onerow['County_Name'];
					$g_timezone = $onerow['g_timezone'];
					$TZ = decodeTimeZone($g_timezone);
					if ($County_Name != '') {
						$location = "$County_Name, $state";
					} else {
						$location = "$state";
					}
					// look up agent
					$readAgent_sql = sprintf("SELECT * FROM employees
						WHERE emp_id = '%s'
						LIMIT 1
						", escape($va_id));
					$result_readAgent = @PowerAlmanac\PDb::query($readAgent_sql);
					if (!$result_readAgent) {
                        continue;
					}
					$aonerow = PowerAlmanac\PDb::fetch_array($result_readAgent);
					$Agentfirst_name = $aonerow['first_name'];
					$Agentlast_name = $aonerow['last_name'];
					
					// calculate changes
					/*
					all = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0'
					government = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = '$Government_ID'
					officials = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  != '$Government_ID' AND Official_ID  != ''
					disposition = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` = '0' AND Official_ID  = 'DISPOSITION'
					notes = SELECT * FROM notes WHERE Government_ID = '$Government_ID' AND `Writer_ID` != '0'
					*/
					$readOfficialsEdits_sql = sprintf("SELECT * FROM notes
							WHERE Government_ID = '%s' AND Writer_ID = '0' AND Official_ID  != '%s' AND Official_ID  != ''
						", escape($Government_ID), escape($Government_ID));
					$result_readOfficialEdits = @PowerAlmanac\PDb::query($readOfficialsEdits_sql);
					if (!$result_readOfficialEdits) {
                        continue;
					}
					$numOfficialEdits = PowerAlmanac\PDb::num_rows($result_readOfficialEdits);
					$numOfficialEditsPCT = number_format(($numOfficialEdits/$num_officials)*100,2);
				}
				switch($primary_role) {
					case 'QualityChecker':
						if ($emp_id == $qc_id) {
							$readAgent_sql = sprintf("SELECT * FROM employees
								WHERE emp_id = '%s'
								LIMIT 1
								", escape($va_id));
							$result_readAgent = @PowerAlmanac\PDb::query($readAgent_sql);
							if (!$result_readAgent) {
                                continue;
							}
							$aonerow = PowerAlmanac\PDb::fetch_array($result_readAgent);
							$Agentfirst_name = $aonerow['first_name'];
							$Agentlast_name = $aonerow['last_name'];
							echo("<tr><td>Completed</td><td>$job_id</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name ($Government_Type)</td><td>$location</td><td>$TZ</td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$Agentfirst_name $Agentlast_name</td></tr>");
						}
						break;
					case 'FinalApprover':
						if ($emp_id == $fa_id) {
							//echo("<tr><td>Completed</td><td>$job_id</td><td></td><td></td><td></td><td></td><td></td><td>$num_officials</td><td>$numOfficialEdits ($numOfficialEditsPCT%)</td><td>$Agentfirst_name $Agentlast_name</td></tr>");
							// look up agent
							$readAgent_sql = sprintf("SELECT * FROM employees
								WHERE emp_id = '%s'
								LIMIT 1
								", escape($va_id));
							$result_readAgent = @PowerAlmanac\PDb::query($readAgent_sql);
							if (!$result_readAgent) {
                                continue;
							}
							$aonerow = PowerAlmanac\PDb::fetch_array($result_readAgent);
							$Agentfirst_name = $aonerow['first_name'];
							$Agentlast_name = $aonerow['last_name'];
							echo("<tr><td>Completed</td><td>$job_id</td><td colspan='3'>Information Not Available Yet - Task is in Data Uploader</td><!--<td></td><td></td>--><td></td><td></td><td>$num_officials</td><td>NA</td><td>$Agentfirst_name $Agentlast_name</td></tr>");
						}
						break;
					default: // none for ProcessInitiator and others
						break;
				}
			}
		} else {
			echo("<tr><td colspan='9'>You have no completed tasks</td></tr>");
		}
		?>
		</tbody>
	</table>
	<?php
  	} else {
		// voice agent summary for PI
		
		// get VAa
		$readVA_sql = "SELECT * FROM employees
				WHERE access_level = 'VoiceAgent'
			";
			
		$result_readVA = @PowerAlmanac\PDb::query($readVA_sql);
		if (!$result_readVA) {
			die("Error reading from employees database: $readVA_sql" . PowerAlmanac\PDb::error());
		}
	?>
    <h2>Voice Agents Snapshot</h2>
	<table id="voiceagents" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Voice Agent</th>
				<th># of Governments in VA Status</th>
				<th>% of Total in VA Status</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($varow = PowerAlmanac\PDb::fetch_array($result_readVA))
		{
			$first_name = $varow['first_name'];
			$last_name = $varow['last_name'];
			$emp_id = $varow['emp_id'];
			// get tasks count
			$readVAJobsActive_sql = sprintf("SELECT * FROM jobs
					WHERE state = 'va' AND va_id = '%s'
				", escape($emp_id));
			$result_readVAJobsActive = @PowerAlmanac\PDb::query($readVAJobsActive_sql);
			if (!$result_readVAJobsActive) {
				die("Error reading from $dbname database: $readVAJobsActive_sql" . PowerAlmanac\PDb::error());
			}
			$numJobsVAActive = PowerAlmanac\PDb::num_rows($result_readVAJobsActive);
			$numJobsVAActiveFMT = number_format($numJobsVAActive);
			$PCTofTotal = number_format(($numJobsVAActive/$numJobsVA)*100,2);
			echo("<tr><td>$first_name $last_name ($emp_id)</td><td>$numJobsVAActiveFMT</td><td>$PCTofTotal</td></tr>");	
		}
		?>
		</tbody>
	</table>
    <?php
	}
	?>
    </div>

</center>

</div>

</body>
</html>