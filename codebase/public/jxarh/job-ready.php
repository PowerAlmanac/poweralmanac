<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$jid = $_REQUEST['jid'];
// initiator_job_ready(in id BIGINT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
//$pdo->query("CALL initiator_job_ready(5)");

$pdo->query(sprintf("CALL initiator_job_ready(%s)", escape($jid)));

header("Location: admin-editjob.php?jid=$jid");
flush();
exit;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Job ready</title>
<meta name="robots" content="noindex">
</head>

<body>

</body>
</html>
