<?php

?>
<table id="instructions" class="ui-widget ui-widget-content" width="1000">
    <thead>
        <tr class="ui-widget-header ">
            <th width="25%">Process Initiator Role</th>
            <th width="25%">Voice Agent Role</th>
            <th width="25%">Quality Checker Role</th>
            <th width="25%">Final Approver Role</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td valign="top" align="left">
            <ol>
            <li>Log in.</li>
            <li>Create a <b>new</b> TASK.</li>
            <li>Select a Voice Agent, Quality Checker, and Final Approver for the TASK.</li>
            <li>Select Government IDs (up to 25 at a time) for the Voice Agent to work on.</li>
            <li><b>Activate</b> by making the TASK <b>available</b> to the Voice Agent.</li>
            </ol>
            </td>
            <td valign="top" align="left">
            <ol>
            <li>Log in.</li>
            <li>Start by <b>editing</b> a TASK assigned to you by the Process Initator.</li>
            <li><b>Update</b> or <b>confirm</b> each Government Officials for the selected Government ID.</li>
            <li>When you are done with all the Government Officials for a specific Government ID, it will <b>automatically</b> be moved to the Quality Checker for verification.</li>
            </ol>
            The Quality Checker or Final Approver may send the Government ID (and all associated Government Officials) back for more edits/updates. Repeat the process but be sure to read the <b>Notes</b> section for additional instructions.
            </td>
            <td valign="top" align="left">
            <ol>
            <li>Log in.</li>
            <li>Start by <b>editing</b> a TASK completed by the Voice Agent.</li>
            <li><b>Confirm</b> the Government ID and pass it to the Final Approver if clean - it will <b>automatically</b> be moved to the Final Approver for verification.</li>
            <li>Otherwise send it back to the Voice Agent for further updates - remember to add instructions to the <b>Notes</b> section for the Voice Agent.</li>
            <!--
            <li>Otherwise <b>edit</b> or <b>update</b> the Government Officials or send it back to the Voice Agent for further updates - remember to add instructions to the <b>Notes</b> section for the Voice Agent.</li>
            <li>If you chose to <b>edit</b> individual Government Officials, you have to confirm or update the <b>full list</b> in order to complete the Government ID and pass it to the Final Approver.</li>
            -->
            </ol>
            </td>
            <td valign="top" align="left">
            <ol>
            <li>Log in.</li>
            <li>Start by <b>editing</b> a TASK completed by the Quality Checker.</li>
            <li><b>Confirm</b> the Government ID and pass it to the Data Uploader if clean - it will <b>automatically</b> be moved to the Data Uploader for verification.</li>
            <li>Otherwise send it back to the Voice Agent or Quality Checker for further updates - remember to add instructions to the <b>Notes</b> section for the Voice Agent or Quality Checker.</li>
            <!--
            <li>Otherwise <b>edit</b> or <b>update</b> the Government Officials or send it back to the Voice Agent or Quality Checker for further updates - remember to add instructions to the <b>Notes</b> section for the Voice Agent or Quality Checker.</li>
            <li>If you chose to <b>edit</b> individual Government Officials, you have to confirm or update the <b>full list</b> in order to complete the Government ID and pass it to the Data Uploader.</li>
            -->
            </ol>
            </td>
        </tr>
    </tbody>
</table>
