<?php
include("inc-config.php");
include("inc-protected-admin.php");

$gid = $_REQUEST['gid'];
$jid = $_REQUEST['jid'];

// get job detail
$read_sql = sprintf("SELECT * FROM jobs
		WHERE job_id = '%s'
		LIMIT 1
	", escape($jid));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_read);
$job_id = $onerow['job_id'];
$va_id = $onerow['va_id'];
$num_officials = $onerow['num_officials'];
$num_govs = $onerow['num_govs'];
$job_state = $onerow['state'];

$job_table = $job_state . '_' . $job_id;

$checkSQL = sprintf("show tables from $dbname like '%s'", escape($job_table));
$result_checkTable = @PowerAlmanac\PDb::query($checkSQL);
if (!$result_checkTable) {
	die('Error reading from $dbname database:' . PowerAlmanac\PDb::error());
}
$numTables = number_format(PowerAlmanac\PDb::num_rows($result_checkTable));
if ($numTables > 0) {
	// found
	$errorMsg = '';
	
	// govt info
	$readgovtInfo_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		LIMIT 1
		", escape($job_table), escape($gid));
	$result_readgovtInfo = @PowerAlmanac\PDb::query($readgovtInfo_sql);
	if (!$result_readgovtInfo) {
		die("Error reading from $job_table database: $read_sql" . PowerAlmanac\PDb::error());
	}
	$govtinforow = PowerAlmanac\PDb::fetch_array($result_readgovtInfo);
	$Government_Type = $govtinforow['Government_Type'];
	$Government_ID = $govtinforow['Government_ID'];
	$Government_Place_Name = $govtinforow['Government_Place_Name'];
	$Government_Type_Name = $govtinforow['Government_Type_Name'];
	$County_Name = $govtinforow['County_Name'];
	$state = $govtinforow['state'];
	$Population = $govtinforow['Population'];
	$Government_PhoneNumber = $govtinforow['Government_PhoneNumber'];
	$Government_Web_Address = $govtinforow['Government_Web_Address'];
	$Address_Street_Box = $govtinforow['Address_Street_Box'];
	$City = $govtinforow['City'];
	$Zip_Code = $govtinforow['Zip_Code'];
	
	$Taken_from = $govtinforow['Taken_from'];
	$Updated_website = $govtinforow['Updated_website'];
			
	// govt officials
	$readgovtOfficials_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		", escape($job_table), escape($gid));
	$result_readgovtOfficials = @PowerAlmanac\PDb::query($readgovtOfficials_sql);
	if (!$result_readgovtOfficials) {
		die("Error reading from $job_table database: $read_sql" . PowerAlmanac\PDb::error());
	}
} else {
	// does not exist
	//$errorMsg = 'Tables not found.';
	$errorMsg = '';
	// check full_data? TBD
	
}

/*
[Government_Type] => township
[Government_ID] => 20300203800000

[Government_Place_Name] => MAPLETON
[Government_Type_Name] => TOWN
[County_Name] => AROOSTOOK
[state] => ME
[Population] => 1840
		
[Government_PhoneNumber] => 2077643754
[Government_Web_Address] => Website Not Found
[Address_Street_Box] => 103 Pulcifur Road
[City] => Mapleton
[Zip_Code] => 04757

[Taken_from] => 
[Updated_website] => 
[Form_of_Government] => 7
[Governing_Body_Name] => 0


[Role] => Clerk

[Role_exists] => Yes
[Government_Title] => Clerk
[First_Name] => Suzan
[Last_Name] => Bragdon
[Email_Address] => sueb@mapletontownoffice.com
[Phone_Number] => 2077643754
[Phone_Number_Ext] => 
[Mailing_Street_Box] => 103 Pulcifur Road
[Mailing_Suite_Number] => 
[Mailing_City] => Mapleton
[Mailing_State] => ME          <--- Missing
[Mailng_Zip_Code] => 04757
[Part_of_Governing_Board] => 
[Official_ID] => 220300203800000-109-8718E8

[Email_Type] => Individual <- IGNORE

[Date_Confirmed] => 1970-01-01
[g_timezone] => 5
[o_timezone] => 5
[va_date] => 
[va_time] => 
[r_state] => allocated

[notes] => 
*/

// text
switch($job_state) {
	case 'va':
		$job_state_description = 'Voice Agent';
		break;
	case 'qc':
		$job_state_description = 'Quality Check';
		break;
	case 'fa':
		$job_state_description = 'Final Approval';
		break;
	case 'dome':
		$job_state_description = 'Ready To Upload';
		break;
	default:
		$job_state_description = 'Unknown';
		break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Access - Edit Government</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />
<script>
	$(function() {
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		$( ".ready a" ).button({
            icons: {
                primary: "ui-icon-plus"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-minus"
			}
        }).next().button({
            icons: {
                primary: "ui-icon-minus"
			}
        });
		
	$( "input:submit" ).button();

	$( ".hidebtn a" ).button({
            icons: {
                primary: "ui-icon-locked"
            },
            text: false
        });
	
	var gid1 = $( "#gid" ),
		jid1 = $( "#jid" ),
		note1 = $( "#note" );
			
	$( "#dialog-form1" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Send Note": function() {
				//alert(gid.val());
				//alert(jid.val());
				//alert(note.val());
				
				$.ajax({
					type: "POST",
					url: "admin-qc-sendback2va.php",
					data: "gid=" + gid1.val() + "&jid=" + jid1.val() + "&note=" + note1.val(),
					success: function(msg){
						window.location = 'admin-editgovt.php?gid=' + gid1.val() + '&jid=' + jid1.val();
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});

	var gid2 = $( "#gid" ),
		jid2 = $( "#jid" ),
		note2 = $( "#note" );
		
	$( "#dialog-form2" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Send Note": function() {
				//alert(gid.val());
				//alert(jid.val());
				//alert(note.val());
				
				$.ajax({
					type: "POST",
					url: "admin-fa-sendback2va.php",
					data: "gid=" + gid2.val() + "&jid=" + jid2.val() + "&note=" + note2.val(),
					success: function(msg){
						window.location = 'admin-editgovt.php?gid=' + gid2.val() + '&jid=' + jid2.val();
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});

	var gid3 = $( "#gid" ),
		jid3 = $( "#jid" ),
		note3 = $( "#note" );
		
	$( "#dialog-form3" ).dialog({
		autoOpen: false,
		height: 400,
		width: 300,
		modal: true,
		buttons: {
			"Send Note": function() {
				//alert(gid3.val());
				//alert(jid3.val());
				//alert(note3.val());
				
				$.ajax({
					type: "POST",
					url: "admin-fa-sendback2qc.php",
					data: "gid=" + gid3.val() + "&jid=" + jid3.val() + "&note=" + note3.val(),
					success: function(msg){
						window.location = 'admin-editgovt.php?gid=' + gid3.val() + '&jid=' + jid3.val();
					}
				});
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});
	
	$( "#open-qcNote1" )
	  .button({
            icons: {
                primary: "ui-icon-minus"
            }
        })
	  .click(function() {
		  $( "#dialog-form1" ).dialog( "open" );
	  });
	  
	$( "#open-faNote1" )
	  .button({
            icons: {
                primary: "ui-icon-minus"
            }
        })
	  .click(function() {
		  $( "#dialog-form2" ).dialog( "open" );
	  });
	  
	$( "#open-faNote2" )
	  .button({
            icons: {
                primary: "ui-icon-minus"
            }
        })
	  .click(function() {
		  $( "#dialog-form3" ).dialog( "open" );
	  });
	  
	});
	  
	$( "#dialog-zipmessage" ).dialog({
		modal: true,
		autoOpen: false,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
	});
		
</script>

<script type="text/javascript">
function showEdit(id) {
	//alert('showEdit start');
	var e = document.getElementById(id);
	e.style.display = 'block';
	//alert('showEdit end');
}
function closeEdit(id) {
	var e = document.getElementById(id);
	e.style.display = 'none';
}
function validateForm() {
	// if needed...
	//alert('validating form...');
}

function validateZip(inputString) {
	if(inputString.length == 5) {
	$('#foo').addClass('load');
		$.post("validate-zipcode.php", {queryString: ""+inputString}, function(data){
			if(data.length >0) {
				//alert(data);
				if (data == '0') {
					//$( "#dialog-zipmessage" ).dialog( "open" );
					alert('Invalid Zip Code entered. Please try again.');
				}
			}
		});
	}
	if(inputString.length > 5) {
		alert('Invalid Zip Code entered. Please try again.');
	}
}
		
</script>

<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">
<!--
<div id="dialog-zipmessage" title="Zipcode Error">
	<p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		Invalid Zip Code entered. Please try again.
	</p>
</div>
-->
<div id="dialog-form1" title="Add Notes">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to Voice Agent</label>
        <textarea name="note" id="note" cols="30" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

<div id="dialog-form2" title="Add Notes">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to Voice Agent</label>
        <textarea name="note" id="note" cols="30" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

<div id="dialog-form3" title="Add Notes">
	<form>
    <input type="hidden" name="gid" id="gid" value="<?php echo($gid); ?>" />
    <input type="hidden" name="jid" id="jid" value="<?php echo($jid); ?>" />
	<fieldset>
		<label for="note">Add Notes to Quality Checker</label>
        <textarea name="note" id="note" cols="30" rows="10">
        </textarea>
	</fieldset>
	</form>
</div>

<center>
<?php
// navigation
include("nav-admin.php");
include("userinfo.php");
?>
</center>

<center>
<div id="wide-contain" class="ui-widget">
	<h1>EDIT: Government #<?php echo($gid); ?> Job ID #<?php echo($jid); ?></h1>
    Government ID Status: <button><?php echo($job_state_description); ?></button>
    <?php
	if (($job_state == 'qc')) {
		// add ready submission
		$confirmURL = "admin-qc-confirm.php?gid=$Government_ID&jid=$job_id";
		$sendback2vaURL = "admin-qc-sendback2va.php?gid=$Government_ID&jid=$job_id";
		echo("<div class='ready'>");
		echo("<a href='$confirmURL'>Confirm Government</a> ");
		echo('<button id="open-qcNote1">Send Back To VA</button>');
		echo("</div>");
	}
	if (($job_state == 'fa')) {
		// add ready submission
		$confirmURL = "admin-fa-confirm.php?gid=$Government_ID&jid=$job_id";
		$sendback2vaURL = "admin-fa-sendback2va.php?gid=$Government_ID&jid=$job_id";
		$sendback2qcURL = "admin-fa-sendback2qc.php?gid=$Government_ID&jid=$job_id";
		echo("<div class='ready'>");
		echo("<a href='$confirmURL'>Confirm Government</a> ");
		echo('<button id="open-faNote1">Send Back To VA</button> ');
		echo('<button id="open-faNote2">Send Back To QC</button>');
		echo("</div>");
	}
	?>
    <br />
	<b>Government Information</b>
	<table id="users" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
           		<th>Action</th>
                <th>Government Type</th>
				<th>Government ID</th>
                <th>Government Place Name</th>
                <th>Government Type Name</th>
				<th>County Name</th>
				<th>State</th>
				<th>Population</th> 
                
				<th>Government Phone Number</th>
                <th>Government Web Address</th>
                <th>Updated</th>
				<th>Street Box</th>
				<th>City</th>
                <th>Zip Code</th>
                <th>Taken From</th>
			</tr>
		</thead>
		<tbody>
        <?php
		// list government info - 7
		echo("<tr><td><a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Government_ID');\">Edit</a></td><td>$Government_Type</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$Government_Type_Name</td><td>$County_Name</td><td>$state</td><td>$Population</td><td>$Government_PhoneNumber</td><td><a href='$Government_Web_Address' target='_blank'>$Government_Web_Address</a></td><td>$Updated_website</td><td>$Address_Street_Box</td><td>$City</td><td>$Zip_Code</td><td>$Taken_from</td></tr>");
		switch($Updated_website) {
			case 'Yes':
				$updated_yes = ' selected';
				$updated_no = ''; 
				break;
			case 'No':
				$updated_yes = ''; 
				$updated_no = ' selected'; 
				break;
			default:
				$updated_yes = ''; 
				$updated_no = ' selected'; 
				break;
		}
		echo("<tr bgcolor='#ffffff'><td colspan='20' align='left'><div id='$Government_ID' style='display:none;'>&nbsp;&nbsp;<a href='JavaScript:void(0);' onclick=\"javascript:closeEdit('$Government_ID');\" style='color:#ff0000;'>Hide/Cancel</a><form action='all-government-edit.php' name='edit' method='post' onSubmit=\"return validateForm()\"><input type='hidden' name='gid' value='$gid' /><input type='hidden' name='jid' value='$jid' /><table id='$Government_ID' width='75%' align='center'><thead><tr class='ui-edit-header '><th>Govt Phone Number</th><th>Government Web Address</th><th>Updated</th><th>Street Box</th><th>City</th><th>Zip Code</th><th>Taken From</th></tr></thead><tbody><tr><td><input type='text' name='phone' value='$Government_PhoneNumber'></td><td><input type='text' name='webaddress' value='$Government_Web_Address' size='30'></td><td><select name='updatedweb'><option value='Yes' $updated_yes>Yes</option><option value='No' $updated_no>No</option></select></td><td><input type='text' name='address' value=\"$Address_Street_Box\"></td><td><input type='text' name='city' value='$City' size='30'></td><td><input type='text' name='zip' value='$Zip_Code'></td><td><input type='text' name='takenfrom' value=\"$Taken_from\"></td></tr></tbody><tr><td colspan='7' align='center'><input type='submit' value='submit updates'></td></tr></table></form></div></td></tr>");
		?>
		</tbody>
	</table>
    <br />
    <b>Government Officials</b>
	<table id="users" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
           		<th>Actions</th>
				<th>Role</th>
                <th>Role Exist</th>
                <th>Government Title</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th> 
				<th>Phone Number</th>
                <th>Phone Ext</th>
				<th>Mailing Street Box</th>
				<th>Mailing Suite Number</th>
				<th>Mailing City</th>
                <th>Mailing State</th>
                <th>Mailing Zip Code</th>
                <th>Governing Board?</th>
                <th>Official ID</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_readgovtOfficials))
		{
			// list officials - 12 to 13

			$Role = str_replace("/"," / ",$row['Role']);
			$Role_exists = $row['Role_exists'];
			$Government_Title = $row['Government_Title'];
			$First_Name = $row['First_Name'];
			$Last_Name = $row['Last_Name'];
			$Email_Address = $row['Email_Address'];
			$Phone_Number = $row['Phone_Number'];

			$Phone_Number_Ext = $row['Phone_Number_Ext'];
			$Mailing_Street_Box = $row['Mailing_Street_Box'];
			$Mailing_Suite_Number = $row['Mailing_Suite_Number'];
			$Mailing_City = $row['Mailing_City'];
			$Mailing_State = $row['Mailing_State'];
			$Mailng_Zip_Code = $row['Mailng_Zip_Code'];
			$Part_of_Governing_Board = $row['Part_of_Governing_Board'];
			$Official_ID = $row['Official_ID'];
			
			if ($job_state == 'va') {
				$confirmURL = "admin-$job_state-confirm.php?oid=$Official_ID&gid=$gid&jid=$job_id";
				$editURL = "admin-$job_state-edit.php?oid=$Official_ID&gid=$gid&jid=$job_id";
				//echo("<tr><td><a href='$confirmURL'>Confirm</a> | <a href='$editURL'>Edit</a> | Delete</td><td>$Role</td><td>$Role_exists</td><td>$Government_Title</td><td>$First_Name</td><td>$Last_Name</td><td>$Email_Address</td><td>$Phone_Number</td><td>$Phone_Number_Ext</td><td>$Mailing_Street_Box</td><td>$Mailing_Suite_Number</td><td>$Mailing_City</td><td>$Mailing_State</td><td>$Mailng_Zip_Code</td><td>$Part_of_Governing_Board</td><td>$Official_ID</td></tr>");
				echo("<tr><td><a href='$confirmURL'>Confirm</a> | <a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Official_ID');\">Edit</a> | Delete</td><td>$Role</td><td>$Role_exists</td><td>$Government_Title</td><td>$First_Name</td><td>$Last_Name</td><td>$Email_Address</td><td>$Phone_Number</td><td>$Phone_Number_Ext</td><td>$Mailing_Street_Box</td><td>$Mailing_Suite_Number</td><td>$Mailing_City</td><td>$Mailing_State</td><td>$Mailng_Zip_Code</td><td>$Part_of_Governing_Board</td><td>$Official_ID</td></tr>");
				switch($Role_exists) {
					case 'Yes':
						$selected_role_yes = ' selected';
						$selected_role_no = ''; 
						break;
					case 'No':
						$selected_role_yes = ''; 
						$selected_role_no = ' selected'; 
						break;
					default:
						$selected_role_yes = ''; 
						$selected_role_no = ''; 
						break;
				}
				switch($Part_of_Governing_Board) {
					case 'Yes':
						$selected_board_yes = ' selected';
						$selected_board_no = ''; 
						break;
					case 'No':
						$selected_board_yes = ''; 
						$selected_board_no = ' selected'; 
						break;
					default:
						$selected_board_yes = ''; 
						$selected_board_no = ' selected'; 
						break;
				}
				echo("<tr bgcolor='#ffffff'><td colspan='20' align='left'><div id='$Official_ID' style='display:none;'>&nbsp;&nbsp;<a href='JavaScript:void(0);' onclick=\"javascript:closeEdit('$Official_ID');\" style='color:#ff0000;'>Hide/Cancel</a><form action='admin-va-edit.php' name='edit' method='post' onSubmit=\"return validateForm()\"><input type='hidden' name='gid' value='$gid' /><input type='hidden' name='jid' value='$jid' /><input type='hidden' name='oid' value='$Official_ID' /><table id='$Official_ID' width='75%' align='center'><thead><tr class='ui-edit-header '><th>Role Exist</th><th>Government Title</th><th>First Name</th><th>Last Name</th><th>Email</th> <th>Phone Number</th><th>Phone Ext</th></tr></thead><tbody><tr><td><select name='roleexist'><option value='Yes' $selected_role_yes>Yes</option><option value='No' $selected_role_no>No</option></select></td><td><input type='text' name='title' value='$Government_Title' size='30'></td><td><input type='text' name='fname' value=\"$First_Name\"></td><td><input type='text' name='lname' value=\"$Last_Name\"></td><td><input type='text' name='email' value='$Email_Address' size='30'></td><td><input type='text' name='phone' value='$Phone_Number'></td><td><input type='text' name='phoneext' value='$Phone_Number_Ext'></td></tr></tbody><thead><tr class='ui-edit-header '><th>Mailing Street Box</th><th>Mailing Suite Number</th><th>Mailing City</th><th>Mailing State</th><th>Mailing Zip Code</th><th>Governing Board?</th></tr></thead><tbody><tr><td><input type='text' name='mail_box' value=\"$Mailing_Street_Box\" size='30'></td><td><input type='text' name='mail_suite' value=\"$Mailing_Suite_Number\"></td><td><input type='text' name='mail_city' value='$Mailing_City'></td><td><input type='text' name='mail_state' value='$Mailing_State' readonly='readonly'></td><td><input type='text' name='mail_zip' value='$Mailng_Zip_Code' onkeyup='validateZip(this.value);'></td><td><select name='partofboard'><option value='Yes' $selected_board_yes>Yes</option><option value='' $selected_board_no>No</option></select></td></tr></tbody><tr><td colspan='7' align='center'><input type='submit' value='submit updates'></td></tr></table></form></div></td></tr>");
			} else {
				// qc or fa confirm all officials in government, NOT individual officials
				$editURL = "admin-$job_state-edit.php?oid=$Official_ID&gid=$gid&jid=$job_id";
				echo("<tr><td><a href='JavaScript:void(0);' onclick=\"javascript:showEdit('$Official_ID');\">Edit</a>  | Delete</td><td>$Role</td><td>$Role_exists</td><td>$Government_Title</td><td>$First_Name</td><td>$Last_Name</td><td>$Email_Address</td><td>$Phone_Number</td><td>$Phone_Number_Ext</td><td>$Mailing_Street_Box</td><td>$Mailing_Suite_Number</td><td>$Mailing_City</td><td>$Mailing_State</td><td>$Mailng_Zip_Code</td><td>$Part_of_Governing_Board</td><td>$Official_ID</td></tr>");
				switch($Role_exists) {
					case 'Yes':
						$selected_role_yes = ' selected';
						$selected_role_no = ''; 
						break;
					case 'No':
						$selected_role_yes = ''; 
						$selected_role_no = ' selected'; 
						break;
					default:
						$selected_role_yes = ''; 
						$selected_role_no = ''; 
						break;
				}
				switch($Part_of_Governing_Board) {
					case 'Yes':
						$selected_board_yes = ' selected';
						$selected_board_no = ''; 
						break;
					case 'No':
						$selected_board_yes = ''; 
						$selected_board_no = ' selected'; 
						break;
					default:
						$selected_board_yes = ''; 
						$selected_board_no = ' selected'; 
						break;
				}
				echo("<tr bgcolor='#ffffff'><td colspan='20' align='left'><div id='$Official_ID' style='display:none;'>&nbsp;&nbsp;<a href='JavaScript:void(0);' onclick=\"javascript:closeEdit('$Official_ID');\" style='color:#ff0000;'>Hide/Cancel</a><form action='admin-$job_state-edit.php' name='edit' method='post' onSubmit=\"return validateForm()\"><input type='hidden' name='gid' value='$gid' /><input type='hidden' name='jid' value='$jid' /><input type='hidden' name='oid' value='$Official_ID' /><table id='$Official_ID' width='75%' align='center'><thead><tr class='ui-edit-header '><th>Role Exist</th><th>Government Title</th><th>First Name</th><th>Last Name</th><th>Email</th> <th>Phone Number</th><th>Phone Ext</th></tr></thead><tbody><tr><td><select name='roleexist'><option value='Yes' $selected_role_yes>Yes</option><option value='No' $selected_role_no>No</option></select></td><td><input type='text' name='title' value='$Government_Title' size='30'></td><td><input type='text' name='fname' value=\"$First_Name\"></td><td><input type='text' name='lname' value=\"$Last_Name\"></td><td><input type='text' name='email' value='$Email_Address' size='30'></td><td><input type='text' name='phone' value='$Phone_Number'></td><td><input type='text' name='phoneext' value='$Phone_Number_Ext'></td></tr></tbody><thead><tr class='ui-edit-header '><th>Mailing Street Box</th><th>Mailing Suite Number</th><th>Mailing City</th><th>Mailing State</th><th>Mailing Zip Code</th><th>Governing Board?</th></tr></thead><tbody><tr><td><input type='text' name='mail_box' value=\"$Mailing_Street_Box\" size='30'></td><td><input type='text' name='mail_suite' value=\"$Mailing_Suite_Number\"></td><td><input type='text' name='mail_city' value='$Mailing_City'></td><td><input type='text' name='mail_state' value='$Mailing_State' readonly='readonly'></td><td><input type='text' name='mail_zip' value='$Mailng_Zip_Code' onkeyup='validateZip(this.value);'></td><td><select name='partofboard'><option value='Yes' $selected_board_yes>Yes</option><option value='' $selected_board_no>No</option></select></td></tr></tbody><tr><td colspan='7' align='center'><input type='submit' value='submit updates'></td></tr></table></form></div></td></tr>");
			}
		}
		?>
		</tbody>
	</table>
    <br />
	<b>Government History</b>
    <?php
	// Job_ID  timestamp  txt  Writer_ID
	$readgovtHistory_sql = sprintf("SELECT * FROM notes
		WHERE Government_ID = '%s'
		ORDER BY ID DESC
		", escape($gid));
	$result_readgovtHistory = @PowerAlmanac\PDb::query($readgovtHistory_sql);
	if (!$result_readgovtHistory) {
		die("Error reading from notes database: $readgovtHistory_sql" . PowerAlmanac\PDb::error());
	}
	$numNotes = PowerAlmanac\PDb::num_rows($result_readgovtHistory);
	?>
	<table id="users" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
           		<th>Job ID</th>
                <th>Date/Time</th>
				<th>Notes</th>
                <th>Written By (ID)</th>
                <th>Written By (Name)</th>
			</tr>
		</thead>
		<tbody>
        <?php
		if ($numNotes > 0) {
			while ($nrow = PowerAlmanac\PDb::fetch_array($result_readgovtHistory))
			{
				$Job_ID = $nrow['Job_ID'];
				$timestamp = $nrow['timestamp'];
				$txt = $nrow['txt'];
				$Writer_ID = $nrow['Writer_ID'];
				// look up writer
				$readWriter_sql = sprintf("SELECT * FROM employees
					WHERE emp_id = '%s'
					LIMIT 1
					", escape($Writer_ID));
				$result_readWriter = @PowerAlmanac\PDb::query($readWriter_sql);
				if (!$result_readWriter) {
					die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
				}
				$wonerow = PowerAlmanac\PDb::fetch_array($result_readWriter);
				$Writerfirst_name = $wonerow['first_name'];
				$Writerlast_name = $wonerow['last_name'];
				if ($Writerfirst_name == '') $Writerfirst_name = 'Not';
				if ($Writerlast_name == '') $Writerlast_name = 'Available';
				echo("<tr><td>$Job_ID</td><td>$timestamp</td><td>$txt</td><td>$Writer_ID</td><td>$Writerfirst_name $Writerlast_name</td></tr>");
			}
		} else {
			echo("<tr><td colspan='6'>No notes available</td></tr>");
		}
		?>
		</tbody>
	</table>
</div>

</center>

</div>


</body>
</html>