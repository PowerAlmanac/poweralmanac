<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

// qc_refresh(in job_id bigint,in StartFrom int, in numRecords2Show int,OUT Result MEDIUMTEXT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query("CALL qc_refresh(3,0,25,@Result1)");
$pdoObject = $pdo->query("SELECT @Result1");
$rsArray = $pdoObject->fetchAll();
$Result1 = $rsArray[0]['@Result1'];
//print_r($Result1);

/*
$pdo->query("CALL va_refresh(5,0,25,@Result2)");
$pdoObject = $pdo->query("SELECT @Result2");
$rsArray = $pdoObject->fetchAll();
$Result2 = $rsArray[0]['@Result2'];
print_r($Result2);
*/

$Result_Array = json_decode($Result1, TRUE);
switch(json_last_error())
{
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded<br>';
	break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found<br>';
	break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON_SUMMARY<br>';
		print_r($JSON_SUMMARY);
	break;
	case JSON_ERROR_NONE:
		//echo ' - No errors<br>';
	break;
}
//print_r($Result_Array);
//exit;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QC Refresh</title>
<meta name="robots" content="noindex">
</head>

<body>
<?php
	//
	foreach ($Result_Array as $result) {
		//echo("<p>");
		$Government_ID = $result['Government_ID'];
		$Official_ID = $result['Official_ID'];
		$Government_Title = $result['Government_Title'];
		$First_Name = $result['First_Name'];
		$Last_Name = $result['Last_Name'];
		echo("$Government_Title - $First_Name $Last_Name ($Official_ID [$Government_ID]) <a href='qc-confirm.php?gid=$Official_ID&jid=3'>confirm</a> | <a href='qc-sendback-notes.php?gid=$Official_ID&jid=3'>send back</a> | edit <br>");
		//echo("</p>");
	}
	
?>
</body>
</html>
