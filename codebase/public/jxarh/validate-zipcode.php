<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$queryString = $_REQUEST['queryString'];

// zip_exists(IN zip_code varchar(5),OUT exist boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL zip_exists('%s',@rc)", escape($queryString)));
$pdoObject = $pdo->query("SELECT @rc");
$rsArray = $pdoObject->fetchAll();
$rc = $rsArray[0]['@rc'];
echo($rc);

?>
