<?php
include("inc-config.php");

$oid = $_REQUEST['oid'];
$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

// va_confirm(in o_id varchar(100),in job_id bigint,out rc boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL va_confirm('%s',%s,@rc)", escape($oid), escape($jid)));
$pdoObject = $pdo->query("SELECT @rc");
$rsArray = $pdoObject->fetchAll();
$rc = $rsArray[0]['@rc'];

header("Location: voiceagent-editgovt.php?gid=$gid&jid=$jid");
flush();
exit;

?>