<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');


// va_refresh(in job_id bigint,in StartFrom int, in numRecords2Show int,OUT Result MEDIUMTEXT)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query("CALL va_refresh(3,0,25,@Result1)");
$pdoObject = $pdo->query("SELECT @Result1");
$rsArray = $pdoObject->fetchAll();
$Result1 = $rsArray[0]['@Result1'];
//print_r($Result1);

/*
$pdo->query("CALL va_refresh(5,0,25,@Result2)");
$pdoObject = $pdo->query("SELECT @Result2");
$rsArray = $pdoObject->fetchAll();
$Result2 = $rsArray[0]['@Result2'];
print_r($Result2);
*/

$Result_Array = json_decode($Result1, TRUE);
switch(json_last_error())
{
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded<br>';
	break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found<br>';
	break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON_SUMMARY<br>';
		print_r($JSON_SUMMARY);
	break;
	case JSON_ERROR_NONE:
		echo ' - No errors<br>';
	break;
}
//print_r($Result_Array);

// for those already confirmed
$pdo->query("CALL qc_refresh(3,0,25,@Result2)");
$pdoObject = $pdo->query("SELECT @Result2");
$rsArray = $pdoObject->fetchAll();
$Result2 = $rsArray[0]['@Result2'];

$ResultConfirmed_Array = json_decode($Result2, TRUE);
switch(json_last_error())
{
	case JSON_ERROR_DEPTH:
		echo ' - Maximum stack depth exceeded<br>';
	break;
	case JSON_ERROR_CTRL_CHAR:
		echo ' - Unexpected control character found<br>';
	break;
	case JSON_ERROR_SYNTAX:
		echo ' - Syntax error, malformed JSON_SUMMARY<br>';
		print_r($JSON_SUMMARY);
	break;
	case JSON_ERROR_NONE:
		//echo ' - No errors<br>';
	break;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VA Refresh</title>
<meta name="robots" content="noindex">
</head>

<body>
<?php
	// va refresh
	echo("<p>");
	echo("<b>Unconfirmed</b><br>");
	foreach ($Result_Array as $result) {
		//
		$Government_ID = $result['Government_ID'];
		$Official_ID = $result['Official_ID'];
		$Government_Title = $result['Government_Title'];
		$First_Name = $result['First_Name'];
		$Last_Name = $result['Last_Name'];
		echo("$Government_Title - $First_Name $Last_Name ($Official_ID [$Government_ID]) <a href='va-confirm.php?oid=$Official_ID&jid=3'>confirm</a> | edit<br>");
	}
	echo("</p>");
	// get from qc refresh
	// ONLY IF STILL 'VA' state
	$pdo->query("CALL show_state(3,@status)");
	$pdoObject = $pdo->query("SELECT @status");
	$rsArray = $pdoObject->fetchAll();
	$status = $rsArray[0]['@status'];
	echo("<p>");
	echo("<b>Confirmed</b><br>");
	if ($status == 'va') {
		foreach ($ResultConfirmed_Array as $resultConfirm) {
			//
			$Government_ID = $resultConfirm['Government_ID'];
			$Official_ID = $resultConfirm['Official_ID'];
			$Government_Title = $resultConfirm['Government_Title'];
			$First_Name = $resultConfirm['First_Name'];
			$Last_Name = $resultConfirm['Last_Name'];
			echo("$Government_Title - $First_Name $Last_Name ($Official_ID [$Government_ID]) <a href='va-changemind.php?oid=$Official_ID&jid=3'>unconfirm</a><br>");
		}
	}
	echo("</p>");
	
?>
</body>
</html>
