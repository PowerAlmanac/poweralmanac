<?php
include("inc-config.php");

$gid = '31300761400000';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Government Notes for DUPLICATES VACANT Problem</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />


<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">

<center>
<div id="wide-contain" class="ui-widget">
	<h1>Government Notes - Vacant Officials</h1>
    
	<?php
    $filename = "Vacant_Officials.csv";
    //echo("Reading $filename...<br />");
    if (!$handle = fopen($filename, 'r')) {
         print "Cannot open file ($filename)";
         exit;
    }
    $handle = fopen ($filename,"r");
    while ($data = fgetcsv ($handle, 1000, ",")) {
        $gidTemp = $data[0];
		// strip first digit
		$gid = substr($gidTemp,1);
		echo("<b>Government #$gid History </b>");
		$readgovtHistory_sql = sprintf("SELECT * FROM notes
			WHERE Government_ID = '%s'
			ORDER BY ID DESC
			", escape($gid));
		$result_readgovtHistory = @PowerAlmanac\PDb::query($readgovtHistory_sql);
		if (!$result_readgovtHistory) {
			die("Error reading from notes database: $readgovtHistory_sql" . PowerAlmanac\PDb::error());
		}
		$numNotes = PowerAlmanac\PDb::num_rows($result_readgovtHistory);
		?>
		<table id="users" class="ui-widget ui-widget-content">
			<thead>
				<tr class="ui-widget-header ">
					<th>Job ID</th>
					<th>Official ID</th>
					<th>Date/Time</th>
					<th>Notes</th>
					<th>Written By (ID)</th>
					<th>Written By (Name)</th>
				</tr>
			</thead>
			<tbody>
			<?php
			if ($numNotes > 0) {
				while ($nrow = PowerAlmanac\PDb::fetch_array($result_readgovtHistory))
				{
					$Job_ID = $nrow['Job_ID'];
					$Official_ID = $nrow['Official_ID'];
					$timestamp = $nrow['timestamp'];
					$txt = $nrow['txt'];
					$Writer_ID = $nrow['Writer_ID'];
					// look up writer
					$readWriter_sql = sprintf("SELECT * FROM employees
						WHERE emp_id = '%s'
						LIMIT 1
						", escape($Writer_ID));
					$result_readWriter = @PowerAlmanac\PDb::query($readWriter_sql);
					if (!$result_readWriter) {
						die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
					}
					$wonerow = PowerAlmanac\PDb::fetch_array($result_readWriter);
					$Writerfirst_name = $wonerow['first_name'];
					$Writerlast_name = $wonerow['last_name'];
					if ($Writerfirst_name == '') $Writerfirst_name = 'Not';
					if ($Writerlast_name == '') $Writerlast_name = 'Available';
					echo("<tr><td>$Job_ID</td><td>$Official_ID</td><td>$timestamp</td><td>$txt</td><td>$Writer_ID</td><td>$Writerfirst_name $Writerlast_name</td></tr>");
				}
			} else {
				echo("<tr><td colspan='6'>No notes available</td></tr>");
			}
			?>
			</tbody>
		</table>
        <br />
    <?php
    }
    ?>
    
</div>

</center>

</div>


</body>
</html>