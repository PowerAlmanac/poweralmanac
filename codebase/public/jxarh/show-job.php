<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');


// show_state(in job_id bigint,out st varchar(50))

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query("CALL show_state(4,@statusDel)");
$pdoObject = $pdo->query("SELECT @statusDel");
$rsArray = $pdoObject->fetchAll();
$statusDel = $rsArray[0]['@statusDel'];
echo("Job State (deleted): $statusDel");

$pdo->query("CALL show_state(5,@status)");
$pdoObject = $pdo->query("SELECT @status");
$rsArray = $pdoObject->fetchAll();
$status = $rsArray[0]['@status'];
echo("Job State (active): $status");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Status Job</title>
<meta name="robots" content="noindex">
</head>

<body>

</body>
</html>
