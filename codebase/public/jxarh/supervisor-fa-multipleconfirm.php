<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

$govtsArray = $_REQUEST['govts']; // array

if (!isset($_REQUEST['govts'])) {
	header("Location: dashboard-supervisor.php");
	flush();
	exit;
}

//print_r($_REQUEST); exit;

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// loop thru govts
foreach ($govtsArray as $gid_jid) 
{
	// explode
	list($gid, $jid) = explode(",", $gid_jid);

	// remove changes in notes for writerID = 0, JID, GID
	$delete_sql =sprintf("DELETE FROM notes
				WHERE Government_ID = '%s' AND Job_ID = '%s' AND Writer_ID = '0' AND Official_ID != 'REWORK'
				", escape($gid), escape($jid));
	$result_delete = @PowerAlmanac\PDb::query($delete_sql);
	if (!$result_delete) {
		die('Error deleting to notes database:' . PowerAlmanac\PDb::error());
	}
	$pdo->query(sprintf("CALL fa_confirm(%s,'%s',@rc)", escape($jid), escape($gid)));
	$pdoObject = $pdo->query("SELECT @rc");
	$rsArray = $pdoObject->fetchAll();
	$rc = $rsArray[0]['@rc'];
}

header("Location: dashboard-supervisor.php");
flush();
exit;

?>