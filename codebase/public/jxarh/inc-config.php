<?php
require_once __DIR__ . '/../../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath());
$dotenv->load();

session_start();

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

// init db
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connectIcube();
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
	exit();
}

if (!@PowerAlmanac\PDb::query($sql)){
	die("Unable to connect to $dbname database: " . PowerAlmanac\PDb::error());
}

function escape($string)
{
    return PowerAlmanac\PDb::escape($string);
}
$escapeFunc = 'escape';

?>
