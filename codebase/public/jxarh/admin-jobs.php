<?php
include("inc-config.php");
include("inc-protected-admin.php");

// get all jobs
$read_sql = "SELECT * FROM jobs
		ORDER BY state DESC
	";
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Access - Jobs</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

	<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var voiceagent = $( "#voiceagent" );
		var qualitychecker = $( "#qualitychecker" );
		var finalapprover = $( "#finalapprover" );
		
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 250,
			width: 400,
			modal: true,
			buttons: {
				"Create a new job": function() {
					//alert('submit');
					$.ajax({
						type: "POST",
						url: "create-job.php",
						data: "voiceagent=" + voiceagent.val() + "&qualitychecker=" + qualitychecker.val() + "&finalapprover=" + finalapprover.val(),
						success: function(msg){
							//alert('ajax completed' + msg);
							//$( "#dialog-form" ).dialog( "close" );
							// refresh
							window.location = 'admin-jobs.php';
						}
					});
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});
		
		$( "#create-job" )
			.button()
			.click(function() {
				$( "#dialog-form" ).dialog( "open" );
			});

	});
	</script>

<meta name="robots" content="noindex">
</head>

<body>

<div id="dialog-form" title="Create new job">
	<form>
	<fieldset>
		<label for="voiceagent">Assign To Voice Agent</label>
		<select id="voiceagent">
        <?php
		// drop down list for VA
		$readVA_sql = "SELECT * FROM employees
			WHERE primary_role = 'VoiceAgent'
			";
		$result_readVA = @PowerAlmanac\PDb::query($readVA_sql);
		if (!$result_readVA) {
			die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
		}
		while ($row = PowerAlmanac\PDb::fetch_array($result_readVA))
		{
			$emp_id = $row['emp_id'];
			$first_name = $row['first_name'];
			$last_name = $row['last_name'];
			echo("<option value='$emp_id'>$first_name $last_name</option>");
		}
		?>
        </select>
        
		<label for="qualitychecker">Assign To Quality Checker</label>
		<select id="qualitychecker">
        <?php
		// drop down list for QC
		$readQC_sql = "SELECT * FROM employees
			WHERE primary_role = 'QualityChecker'
			";
		$result_readQC = @PowerAlmanac\PDb::query($readQC_sql);
		if (!$result_readQC) {
			die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
		}
		while ($qcrow = PowerAlmanac\PDb::fetch_array($result_readQC))
		{
			$emp_id = $qcrow['emp_id'];
			$first_name = $qcrow['first_name'];
			$last_name = $qcrow['last_name'];
			echo("<option value='$emp_id'>$first_name $last_name</option>");
		}
		?>
        </select>

		<label for="finalapprover">Assign To Final Approver</label>
		<select id="finalapprover">
        <?php
		// drop down list for FA
		$readFA_sql = "SELECT * FROM employees
			WHERE primary_role = 'FinalApprover'
			";
		$result_readFA = @PowerAlmanac\PDb::query($readFA_sql);
		if (!$result_readFA) {
			die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
		}
		while ($farow = PowerAlmanac\PDb::fetch_array($result_readFA))
		{
			$emp_id = $farow['emp_id'];
			$first_name = $farow['first_name'];
			$last_name = $farow['last_name'];
			echo("<option value='$emp_id'>$first_name $last_name</option>");
		}
		?>
        </select>
	</fieldset>
	</form>
</div>

<center>
<?php
// navigation
include("nav-admin.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Manage Active Jobs</h1>
	<table id="jobs" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
            	<th>Actions</th>
				<th>Job ID</th>
				<th>Voice Agent ID</th>
				<th>VA Name</th>
				<th>QC Name</th>
				<th>FA Name</th>
				<th>Number of Officials</th> 
				<th>Number of Governments</th>
				<th><img src='img/blockstates.png' alt='state' width="125" height="20" border="1" title='state' /></th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			$job_id = $row['job_id'];
			$va_id = $row['va_id'];
			$qc_id = $row['qc_id'];
			$fa_id = $row['fa_id'];
			$num_officials = $row['num_officials'];
			$num_govs = $row['num_govs'];
			$job_state = $row['state'];
			
			// look up VA
			$readVAname_sql = sprintf("SELECT * FROM employees
				WHERE emp_id = '%s'
				LIMIT 1
				", escape($va_id));
			$result_readVAname = @PowerAlmanac\PDb::query($readVAname_sql);
			if (!$result_readVAname) {
				die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
			}
			$onerow = PowerAlmanac\PDb::fetch_array($result_readVAname);
			$first_name = $onerow['first_name'];
			$last_name = $onerow['last_name'];
			
			// look up QC
			$readQCname_sql = sprintf("SELECT * FROM employees
				WHERE emp_id = '%s'
				LIMIT 1
				", escape($qc_id));
			$result_readQCname = @PowerAlmanac\PDb::query($readQCname_sql);
			if (!$result_readQCname) {
				die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
			}
			$qconerow = PowerAlmanac\PDb::fetch_array($result_readQCname);
			$QCfirst_name = $qconerow['first_name'];
			$QClast_name = $qconerow['last_name'];
			
			if ($QCfirst_name == '') $QCfirst_name = 'Not';
			if ($QClast_name == '') $QClast_name = 'Available';
			
			// look up FA
			$readFAname_sql = sprintf("SELECT * FROM employees
				WHERE emp_id = '%s'
				LIMIT 1
				", escape($fa_id));
			$result_readFAname = @PowerAlmanac\PDb::query($readFAname_sql);
			if (!$result_readFAname) {
				die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
			}
			$faonerow = PowerAlmanac\PDb::fetch_array($result_readFAname);
			$FAfirst_name = $faonerow['first_name'];
			$FAlast_name = $faonerow['last_name'];
			
			if ($FAfirst_name == '') $FAfirst_name = 'Not';
			if ($FAlast_name == '') $FAlast_name = 'Available';
			
			if ($job_state == '') $job_state = '-';
			
			echo("<tr><td><a href='admin-editjob.php?jid=$job_id'>Edit</a> | <a href='remove-job.php?jid=$job_id'>Delete</a></td><td>$job_id</td><td>$va_id</td><td>$first_name $last_name</td><td>$QCfirst_name $QClast_name</td><td>$FAfirst_name $FAlast_name</td><td>$num_officials</td><td>$num_govs</td><td><img src='img/blockstates-$job_state.png' alt='$job_state' title='$job_state' border='1'></td></tr>");
		}
		?>
		</tbody>
	</table>
    </div>
<button id="create-job">Create new job</button>
</center>

</body>
</html>