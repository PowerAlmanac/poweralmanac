<?php
include("inc-config.php");
include("inc-protected-supervisor.php");

// get all jobs
$read_sql = "SELECT * FROM jobs
		ORDER BY state DESC
	";
// ORDER BY job_id DESC
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}

// get total outstanding jobs
$numJobsALL = PowerAlmanac\PDb::num_rows($result_read);

echo("numJobsALL,$numJobsALL,\n");

// overall

$readGovernments_sql = "SELECT DISTINCT Government_ID FROM full_data
	";
$result_readGovernments = @PowerAlmanac\PDb::query($readGovernments_sql);
if (!$result_readGovernments) {
	die("Error reading from $dbname database: $readGovernments_sql" . PowerAlmanac\PDb::error());
}
$numAllGovernments = PowerAlmanac\PDb::num_rows($result_readGovernments);

$readAllocatedJobs_sql = "SELECT DISTINCT Government_ID FROM full_data
		WHERE r_state = 'allocated'
	";
$result_readAllocatedJobs = @PowerAlmanac\PDb::query($readAllocatedJobs_sql);
if (!$result_readAllocatedJobs) {
	die("Error reading from $dbname database: $readAllocatedJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsAllocated = PowerAlmanac\PDb::num_rows($result_readAllocatedJobs);

echo("numJobsAllocated,$numJobsAllocated,\n");

$numJobsAllocatedPCt = number_format(($numJobsAllocated/$numAllGovernments)*100,2);

$readUNAllocatedJobs_sql = "SELECT DISTINCT Government_ID FROM full_data
		WHERE r_state = 'unallocated'
	";
$result_readUNAllocatedJobs = @PowerAlmanac\PDb::query($readUNAllocatedJobs_sql);
if (!$result_readUNAllocatedJobs) {
	die("Error reading from $dbname database: $readUNAllocatedJobs_sql" . PowerAlmanac\PDb::error());
}
$numJobsUNAllocated = PowerAlmanac\PDb::num_rows($result_readUNAllocatedJobs);
$numJobsUNAllocatedPCt = number_format(($numJobsUNAllocated/$numAllGovernments)*100,2);

while ($row = PowerAlmanac\PDb::fetch_array($result_read))
{
	$job_id = $row['job_id'];
	$job_state = $row['state'];
	$databaseID = $job_state . '_' . $job_id;
	// look up Govt Info
	$readgovt_sql = sprintf("SELECT DISTINCT Government_ID,state,Government_Place_Name,Government_Type_Name,Government_Type,County_Name,g_timezone,Date_Confirmed FROM %s", escape($databaseID));
	$result_readgovt = @PowerAlmanac\PDb::query($readgovt_sql);
	if (!$result_readgovt) {
		die("Error reading from va_$job_id database: $readgovt_sql" . PowerAlmanac\PDb::error());
	}
	$onerow = PowerAlmanac\PDb::fetch_array($result_readgovt);
	$Government_ID = $onerow['Government_ID'];

	// get r_state
	$readGovernmentState_sql = sprintf("SELECT * FROM full_data
			WHERE Government_ID = '%s'
			LIMIT 1
		", escape($Government_ID));
	$result_readGovernmentState = @PowerAlmanac\PDb::query($readGovernmentState_sql);
	if (!$result_readGovernmentState) {
		die("Error reading from $dbname database: $readGovernmentState_sql" . PowerAlmanac\PDb::error());
	}
	$onestaterow = PowerAlmanac\PDb::fetch_array($result_readGovernmentState);
	$r_state = $onestaterow['r_state'];
	
	echo("$job_id,$job_state,$Government_ID,$r_state\n");
}

?>