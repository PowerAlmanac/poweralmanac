<?php
include("inc-config.php");
include("inc-protected-voiceagent.php");

$emp_id = $_SESSION['emp_id'];

// get all users
$read_sql = sprintf("SELECT * FROM employees
		WHERE emp_id = '%s'
	", escape($emp_id));
	
$result_read = @PowerAlmanac\PDb::query($read_sql);
if (!$result_read) {
	die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Voice Agent - Productivity</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">

<center>
<?php
// navigation
include("nav-voiceagent.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Voice Agents Productivity</h1>
	<table id="users" class="ui-widget ui-widget-content">
		<thead>
			<tr class="ui-widget-header ">
				<th>Employee ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>User ID</th>
				<th>Job ID</th>
                <th>Job Status</th>
                <th># Govts</th>
                <th># Officials</th>
				<th>Productivity</th>
			</tr>
		</thead>
		<tbody>
        <?php
		while ($row = PowerAlmanac\PDb::fetch_array($result_read))
		{
			$emp_id = $row['emp_id'];
			$first_name = $row['first_name'];
			$last_name = $row['last_name'];
			$user_id = $row['user_id'];
			// get total productivity
			$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
			$pdo->query(sprintf("CALL calulate_productivity(%s,@total_prod)", escape($emp_id)));
			$pdoObject = $pdo->query("SELECT @total_prod");
			$rsArray = $pdoObject->fetchAll();
			$total_prod = $rsArray[0]['@total_prod'];
			echo("<tr bgcolor='#42D0FF'><td><b>$emp_id</b></td><td><b>$first_name</b></td><td><b>$last_name</b></td><td><b>$user_id</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>$total_prod</b></td></tr>");
			// get jobs and productivity related to VA
			$readJobs_sql = sprintf("SELECT * FROM jobs
					WHERE va_id = '%s'
				", escape($emp_id));
			$result_readJobs = @PowerAlmanac\PDb::query($readJobs_sql);
			if (!$result_readJobs) {
				die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
			}
			while ($jobrow = PowerAlmanac\PDb::fetch_array($result_readJobs))
			{
				$job_id = $jobrow['job_id'];
				$num_officials = $jobrow['num_officials'];
				$num_govs = $jobrow['num_govs'];
				$state = $jobrow['state'];
				switch($state) {
					case 'va':
						$jobstate = 'In VA queue';
						break;
					case '':
						$jobstate = 'In PI queue';
						break;
					case 'qc':
						$jobstate = 'In QC queue';
						break;
					case 'fa':
						$jobstate = 'In FA queue';
						break;
					case 'done':
						$jobstate = 'In DONE queue';
						break;
					default:
						$jobstate = $state;
						break;
				}
				$prod = $jobrow['prod'];
				echo("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>$job_id</td><td>$jobstate</td><td>$num_govs</td><td>$num_officials</td><td>$prod</td></tr>");
			}
		}
		?>
		</tbody>
	</table>
</div>

</center>

</div>

</body>
</html>