<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$oid = $_REQUEST['oid'];
$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

$roleexist = $_REQUEST['roleexist'];
$title = str_replace('"','',str_replace("'","\'",$_REQUEST['title']));
$fname = str_replace('"','',str_replace("'","\'",$_REQUEST['fname']));
$lname = str_replace('"','',str_replace("'","\'",$_REQUEST['lname']));
$email = $_REQUEST['email'];
$phone = $_REQUEST['phone'];
$phoneext = $_REQUEST['phoneext'];
$mail_box = str_replace('"','',str_replace("'","\'",$_REQUEST['mail_box']));
$mail_suite = str_replace('"','',str_replace("'","\'",$_REQUEST['mail_suite']));
$mail_city = $_REQUEST['mail_city'];
$mail_state = $_REQUEST['mail_state'];
$mail_zip = $_REQUEST['mail_zip'];
$partofboard = $_REQUEST['partofboard'];

//print_r($_REQUEST); exit;

// qc_update_official(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255))

// qc_update_official(in o_id varchar(100),in job_id bigint,in v_Role_exists varchar(3),in v_Government_Title varchar(150),in v_First_Name varchar(255),in v_Last_Name varchar(255),in v_Email_Address varchar(100),in v_Part_of_Governing_Board varchar(20),in v_Mailing_Street_Box varchar(255),in v_Mailing_Suite_Number varchar(30),in v_Mailing_City varchar(100),in v_Mailng_Zip_Code varchar(5),in v_Phone_Number varchar(30),in v_Phone_Number_Ext varchar(255))


$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL qc_update_official('%s',%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
    escape($oid), escape($jid), escape($roleexist), escape($title), escape($fname), escape($lname), escape($email), escape($partofboard), escape($mail_box), escape($mail_suite), escape($mail_city), escape($mail_zip), escape($phone), escape($phoneext)));

header("Location: admin-editgovt.php?gid=$gid&jid=$jid");
flush();
exit;

?>
