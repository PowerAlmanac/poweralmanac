<?php
include("inc-config.php");

$dbhost = PowerAlmanac\Config::env('mysql_host');
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = PowerAlmanac\Config::env('mysql_pass');
$dbname = PowerAlmanac\Config::env('mysql_name_icube');

$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

// qc_confirm(in job_id bigint,in g_id varchar(20),out rc boolean)

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query(sprintf("CALL qc_confirm(%s,'%s',@rc)", escape($jid), escape($gid)));
$pdoObject = $pdo->query("SELECT @rc");
$rsArray = $pdoObject->fetchAll();
$rc = $rsArray[0]['@rc'];
//echo("rc = $rc"); exit;

//header("Location: supervisor-editgovt.php?gid=$gid&jid=$jid");
header("Location: dashboard-supervisor.php");
flush();
exit;

?>
