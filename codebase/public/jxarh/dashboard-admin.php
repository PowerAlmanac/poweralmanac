<?php
include("inc-config.php");
include("inc-protected-admin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard - Admin</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />


<meta name="robots" content="noindex">
</head>

<body>

<div class="demo">


<center>
<?php
// navigation
include("nav-admin.php");
include("userinfo.php");
?>
</center>

<center>
<div id="users-contain" class="ui-widget">
	<h1>Administrator Dashboard</h1>
    <h2><font color="#FF0000">Use with caution!</font></h2>
    <br />
    <h2>Add &amp; Modify User accounts</h2>
    <h2>Edit/Delete/Modify any Job.</h2>
    <h2>Act as Process Initiator, Voice Agent, Quality Checker, &amp; Final Approver</h2>
</div>


</center>

</div>

</body>
</html>