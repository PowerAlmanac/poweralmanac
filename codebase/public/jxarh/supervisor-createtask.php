<?php

include("inc-config.php");
include("inc-protected-supervisor.php");

if (isset($_REQUEST['p'])) {
	$p = $_REQUEST['p'];
} else {
	$p = 0;
}

if (isset($_REQUEST['q'])) {
	$queryIs = $_REQUEST['q'];
} else {
	$queryIs = '';
}

//echo("DEBUG: queryIs = $queryIs<br>");
$numRecords2Show = 100;

$pageOffset = $p * $numRecords2Show;

if ($queryIs == "") {
	//echo("DEBUG: Query is NULL<br>");
	// initiator_refresh(in StartFrom int, in numRecords2Show int, OUT Result MEDIUMTEXT)
	$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
	$pdo->query(sprintf("CALL initiator_refresh(%s,%s,@Result1)", escape($pageOffset), escape($numRecords2Show)));
	$pdoObject = $pdo->query("SELECT @Result1");
	$rsArray = $pdoObject->fetchAll();
	$Result1 = $rsArray[0]['@Result1'];
	
	$Result_Array = json_decode($Result1, TRUE);
	switch(json_last_error())
	{
		case JSON_ERROR_DEPTH:
			echo ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_CTRL_CHAR:
			echo ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			echo ' - Syntax error, malformed JSON_SUMMARY';
			print_r($JSON_SUMMARY);
		break;
		case JSON_ERROR_NONE:
			//echo ' - No errors';
		break;
	}
} else {
	//echo("DEBUG: Query is NOT NULL<br>");
	// search
	$read_sql = sprintf("SELECT * FROM full_data
		WHERE Government_ID  = '%s'
	", escape($queryIs));
	$result_read = @PowerAlmanac\PDb::query($read_sql);
	if (!$result_read) {
		die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
	}
	$number_of_officials = PowerAlmanac\PDb::num_rows($result_read);
	$onerow = PowerAlmanac\PDb::fetch_array($result_read);
	//print_r($onerow);
	$r_state = $onerow['r_state'];
	if ($r_state == 'unallocated') {
		// OK
		//echo("Clean - display info<br>");
		$Government_ID = $onerow['Government_ID'];
		$Government_Place_Name = $onerow['Government_Place_Name'];
		$Government_Type = $onerow['Government_Type'];
		$State = $onerow['state'];
		$Population = $onerow['Population'];
		//$number_of_officials = $onerow['number_of_officials'];
		$County_Name = $onerow['County_Name'];
		$Government_Type_Name = $onerow['Government_Type_Name'];
		$g_timezone = $onerow['g_timezone'];
		$TZ = decodeTimeZone($g_timezone);
		$date_confirmed = $onerow['Date_Confirmed'];
		$confirmed = date("F",strtotime($date_confirmed));
	} else {
		if ($number_of_officials == 0) {
			$erroMessage = 'ERROR: No Governments matched the ID you entered. Please try search again.';
		} else {
			$erroMessage = 'ERROR: Government ID is already allocated. Please try your search again.';
		}
	}
}

function decodeTimeZone($tz) {
	/*
	4	Atlantic (GMT -04:00)
	5	Eastern (GMT -05:00)
	6	Central (GMT -06:00)
	7	Mountain (GMT -07:00)
	8	Pacific (GMT -08:00)
	9	Alaska (GMT -09:00)
	10	Hawaii-Aleutian Islands (GMT -10:00)
	11	American Samoa (GMT -11:00)
	14	Guam (GMT +10:00)
	15	Palau (GMT +9:00)
	*/
	switch($tz) {
		case '4':
			$timezone = 'Atlantic';
			break;
		case '5':
			$timezone = 'Eastern';
			break;
		case '6':
			$timezone = 'Central';
			break;
		case '7':
			$timezone = 'Mountain';
			break;
		case '8':
			$timezone = 'Pacific';
			break;
		case '9':
			$timezone = 'Alaska';
			break;
		case '10':
			$timezone = 'Hawaii-Aleutian Islands';
			break;
		case '11':
			$timezone = 'American Samoa';
			break;
		case '14':
			$timezone = 'Guam';
			break;
		case '15':
			$timezone = 'Palau';
			break;
		default: // unknown
			$timezone = $tz;
			break;
	}
	return $timezone;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Create new Task</title>

<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>

<script language="javascript" type="text/javascript" src="niceforms.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="niceforms-default.css" />
<link rel="stylesheet" type="text/css" media="all" href="icube.css" />

<script type="text/javascript">

function selectAll()
{
	//alert('selectAll');
	checkAll(document.govtform.govts);
}

function unselectAll()
{
	//alert('unselectAll');
	uncheckAll(document.govtform.govts);
}

function checkAll(field)
{
	for (i = 0; i < field.length; i++)
		//alert(field[i].checked);
		field[i].checked = true ;
		//field[i].className = "NFCheck NFh";
}

function uncheckAll(field)
{
	for (i = 0; i < field.length; i++)
		field[i].checked = false ;
}

</script>

<script>
$(function() {
	$( "input:submit" ).button();
});
</script>
    
<meta name="robots" content="noindex">
</head>

<body>

<center>
<?php
// navigation
include("nav-supervisor.php");
include("userinfo.php");
?>
</center>
<center>
<div id="users-contain" class="ui-widget">
    	<h1>Create new Task</h1>
        <center>
        <h2>Search By Government ID</h2>
        <p align="center">
        <form action="?" method="get" name="searchgovtID">
        <table cellpadding="0" cellspacing="0" width="300" align="center" border="2" style="width:300px;">
        <tr>
            <td align="center">
            <center>Government ID:
            <br />
            <input type="text" name="q" value="" size="50"/>
            <input type="submit" name="submit" value="Search by Government ID" />
            </center>
            </td>
        </tr>
        </table>
        </form>
        </p>
        OR
        <br />
        <h2>Standard Form</h2>
        <a href="?p=0">CLICK HERE to use Standard Create Task (up to <?php echo($numRecords2Show); ?> Government IDs) form</a>
        </center>
        <form action="supervisor-add-tasks.php" method="post" name="govtform" id="govtform">
	<fieldset>
   		<legend>Create Tasks</legend>
        <dl>
        	<dt><label for="voiceagent">Assign To Voice Agent: </label></dt>
            <dd>
            	<select size="1" name="voiceagent" id="access_level">
				<?php
                // drop down list for VA
                $readVA_sql = "SELECT * FROM employees
                    WHERE primary_role = 'VoiceAgent'
                    ";
                $result_readVA = @PowerAlmanac\PDb::query($readVA_sql);
                if (!$result_readVA) {
                    die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
                }
                while ($row = PowerAlmanac\PDb::fetch_array($result_readVA))
                {
                    $va_emp_id = $row['emp_id'];
                    $first_name = $row['first_name'];
                    $last_name = $row['last_name'];
                    echo("<option value='$va_emp_id'>$first_name $last_name </option>");
                }
                ?>
            	</select>
            </dd>
        </dl>
        <dl>
        	<dt><label for="qualitychecker">Assign To Quality Checker: </label></dt>
            <dd>
            	<select size="1" name="qualitychecker" id="qualitychecker">
				<?php
                // drop down list for QC
                $readQC_sql = "SELECT * FROM employees
                    WHERE primary_role = 'QualityChecker'
                    ";
                $result_readQC = @PowerAlmanac\PDb::query($readQC_sql);
                if (!$result_readQC) {
                    die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
                }
                while ($qcrow = PowerAlmanac\PDb::fetch_array($result_readQC))
                {
                    $qc_emp_id = $qcrow['emp_id'];
                    $first_name = $qcrow['first_name'];
                    $last_name = $qcrow['last_name'];
                    echo("<option value='$qc_emp_id'>$first_name $last_name </option>");
                }
                ?>
            	</select>
            </dd>
        </dl>

        
        <dl>
        	<dt><label for="finalapprover">Assign To Final Approver: </label></dt>
            <dd>
            	<select size="1" name="finalapprover" id="finalapprover">
				<?php
                // drop down list for FA
                $readFA_sql = "SELECT * FROM employees
                    WHERE primary_role = 'FinalApprover'
                    ";
                $result_readFA = @PowerAlmanac\PDb::query($readFA_sql);
                if (!$result_readFA) {
                    die("Error reading from $dbname database: $read_sql" . PowerAlmanac\PDb::error());
                }
                while ($farow = PowerAlmanac\PDb::fetch_array($result_readFA))
                {
                    $fa_emp_id = $farow['emp_id'];
                    $first_name = $farow['first_name'];
                    $last_name = $farow['last_name'];
                    echo("<option value='$fa_emp_id'>$first_name $last_name </option>");
                }
                ?>
            	</select>
            </dd>
        </dl>

        <p><b>You can add up to <a href="?p=0"><?php echo($numRecords2Show); ?> Government IDs</a> at a time OR search by a single Government ID above</b></p>
        <dl>
        <input type="submit" name="submit" id="submit" value="Create Tasks" />
        </dl>
		<?php
			// paging
			if ($queryIs == "") {
				$nextPage = $p + 1;
				if ($p != 0) {
					$prevPage = $p - 1;
					echo("&lt; <a href='?p=$prevPage&jid=$jid'>See PREVIOUS $numRecords2Show Governments</a> | ");
					echo("<a href='?p=$nextPage&jid=$jid'>See NEXT $numRecords2Show Governments</a> &gt;");
				} else {
					echo("<a href='?p=$nextPage&jid=$jid'>See NEXT $numRecords2Show Governments</a> &gt;");
				}
			}
        ?>
        <table id="govts" class="ui-widget ui-widget-content">
            <thead>
                <tr class="ui-widget-header ">
                 	<th>&nbsp;Confirmed&nbsp;</th>
                	<th>&nbsp;Government ID&nbsp;</th>
                    <th>&nbsp;Government Name&nbsp;(County)</th>
                    <th>&nbsp;State&nbsp;</th>
                    <th>&nbsp;Government Type&nbsp;</th>
                    <th>&nbsp;Time Zone&nbsp;</th>
                    <th>&nbsp;# Officials&nbsp;</th>
                    <th align="center">&nbsp;Select<br /><a href="javascript: void(0)" onclick="selectAll();">ALL</a><br /><a href="javascript: void(0)" onclick="unselectAll();">NONE</a>&nbsp;</th> 
                </tr>
            </thead>
            <tbody>
			<?php
			if ($queryIs == "") {
				foreach ($Result_Array as $resultItem) {
					//print_r($resultItem);
					$Government_ID = $resultItem['Government_ID'];
					$Government_Place_Name = $resultItem['Government_Place_Name'];
					$Government_Type = $resultItem['Government_Type'];
					$State = $resultItem['State'];
					$Population = $resultItem['Population'];
					$number_of_officials = $resultItem['number_of_officials'];
					$County_Name = $resultItem['County_Name'];
					$Government_Type_Name = $resultItem['Government_Type_Name'];
					$g_timezone = $resultItem['g_timezone'];
					$TZ = decodeTimeZone($g_timezone);
					$date_confirmed = $resultItem['date_confirmed'];
					$confirmed = date("F",strtotime($date_confirmed));
					/*
					[Government_ID] => 05201570100000
					[Government_Type] => municipality
					[State] => CA
					[Government_Place_Name] => RIDGECREST
					[Population] => 26430
					[County_Name] => KERN
					[Government_Type_Name] => CITY
					[Government_Web_Address] => http://www.ci.ridgecrest.ca.us/
					[number_of_officials] => 12
					[g_timezone] => 8
					[date_confirmed] => 2011-03-03
					*/
					if ($County_Name != '') {
						echo("<tr><td>$confirmed</td><td>$Government_ID</td><td>$Government_Place_Name ($County_Name)</td><td>$State</td><td>$Government_Type_Name ($Government_Type)</td><td>$TZ</td><td>$number_of_officials</td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID' /></td></tr>");
					} else {
						echo("<tr><td>$confirmed</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$State</td><td>$Government_Type_Name ($Government_Type)</td><td>$TZ</td><td>$number_of_officials</td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID' /></td></tr>");
					}
				}
			} else {
				if ($r_state == 'unallocated') {
					if ($County_Name != '') {
						echo("<tr><td>$confirmed</td><td>$Government_ID</td><td>$Government_Place_Name ($County_Name)</td><td>$State</td><td>$Government_Type_Name ($Government_Type)</td><td>$TZ</td><td>$number_of_officials</td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID' checked /></td></tr>");
					} else {
						echo("<tr><td>$confirmed</td><td>$Government_ID</td><td>$Government_Place_Name</td><td>$State</td><td>$Government_Type_Name ($Government_Type)</td><td>$TZ</td><td>$number_of_officials</td><td align='center'><input type='checkbox' name='govts[]' id='govts' value='$Government_ID' checked /></td></tr>");
					}
				} else {
					// error
					echo("<tr><td colspan='8' align='center'><font color='#FF0000'>$erroMessage</font></td></tr>");
				}
			}
            ?>
            </tbody>
        </table>
        <input type="submit" name="submit" id="submit" value="Create Tasks" />
        <br /><br />
<?php
// paging
if ($queryIs == "") {
	$nextPage = $p + 1;
	if ($p != 0) {
	  $prevPage = $p - 1;
	  echo("&lt; <a href='?p=$prevPage&jid=$jid'>See PREVIOUS $numRecords2Show Governments</a> | ");
	  echo("<a href='?p=$nextPage&jid=$jid'>See NEXT $numRecords2Show Governments</a> &gt;");
	} else {
	  echo("<a href='?p=$nextPage&jid=$jid'>See NEXT $numRecords2Show Governments</a> &gt;");
	}
	// add first 100 pages
	echo("<br />");
	if ($p <= 100) {
		for ($i = 0; $i <= 99; $i++) {
			$i_display = $i + 1;
			if ($i != $p) {
				echo("<a href='?p=$i&jid=$jid'>$i_display</a> ");
			} else {
				echo("<b><font size='+1'>$i_display</font></b> ");
			}
			if ($i_display % 50 == 0) {
				echo("<br />");
			}
		}

	}
	
}
?>
	</fieldset>
</form>
</div>

</center>
</body>
</html>