<?php
$oid = $_REQUEST['oid'];
$jid = $_REQUEST['jid'];
$gid = $_REQUEST['gid'];

$roleexist = $_REQUEST['roleexist'];
$title = str_replace('"','',str_replace("'","\'",$_REQUEST['title']));
$fname = str_replace('"','',str_replace("'","\'",$_REQUEST['fname']));
$lname = str_replace('"','',str_replace("'","\'",$_REQUEST['lname']));
$email = str_replace("'","\'",$_REQUEST['email']);
$phone = str_replace("-","",$_REQUEST['phone']);
$phoneext = $_REQUEST['phoneext'];
$mail_box = str_replace('"','',str_replace("'","\'",$_REQUEST['mail_box']));
$mail_suite = str_replace('"','',str_replace("'","\'",$_REQUEST['mail_suite']));
$mail_city = str_replace("'","\'",$_REQUEST['mail_city']);
$mail_state = $_REQUEST['mail_state'];
$mail_zip = $_REQUEST['mail_zip'];
$partofboard = $_REQUEST['partofboard'];

$role = $_REQUEST['role'];
$p_roleexist = $_REQUEST['p_roleexist'];
$p_title = str_replace('"','',str_replace("'","\'",$_REQUEST['p_title']));
$p_fname = str_replace('"','',str_replace("'","\'",$_REQUEST['p_fname']));
$p_lname = str_replace('"','',str_replace("'","\'",$_REQUEST['p_lname']));
$p_email = str_replace("'","\'",$_REQUEST['p_email']);
$p_phone = str_replace("-","",$_REQUEST['p_phone']);
$p_phoneext = $_REQUEST['p_phoneext'];
$p_mail_box = str_replace('"','',str_replace("'","\'",$_REQUEST['p_mail_box']));
$p_mail_suite = str_replace('"','',str_replace("'","\'",$_REQUEST['p_mail_suite']));
$p_mail_city = str_replace("'","\'",$_REQUEST['p_mail_city']);
$p_mail_state = $_REQUEST['p_mail_state'];
$p_mail_zip = $_REQUEST['p_mail_zip'];
$p_partofboard = $_REQUEST['p_partofboard'];

// read changes from before if any
$readOfficialNotes_sql = sprintf("SELECT * FROM notes WHERE Government_ID = '%s' AND Official_ID = '%s'
	LIMIT 1
	", escape($gid), escape($oid));
$result_readOfficialNotes = @PowerAlmanac\PDb::query($readOfficialNotes_sql);
if (!$result_readOfficialNotes) {
	die("Error reading from notes database: $readOfficialNotes_sql" . PowerAlmanac\PDb::error());
}
$numNotes = PowerAlmanac\PDb::num_rows($result_readOfficialNotes);
if ($numNotes == 0) { // first time
	$notesArray = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
} else {
	$onerow = PowerAlmanac\PDb::fetch_array($result_readOfficialNotes);
	$notes = $onerow['txt'];
	$notesArray = explode(",", $notes);
}

// compare and tag additional changes
$official_updates = '';

if ($p_roleexist != $roleexist) {
	$notesArray[0] = 1;
}
if ($p_title != $title) {
	$notesArray[1] = 1;
}
if ($p_fname != $fname) {
	$notesArray[2] = 1;
}
if ($p_lname != $lname) {
	$notesArray[3] = 1;
}
if ($p_email != $email) {
	$notesArray[4] = 1;
}
if ($p_phone != $phone) {
	$notesArray[5] = 1;
}
if ($p_phoneext != $phoneext) {
	$notesArray[6] = 1;
}
if ($p_mail_box != $mail_box) {
	$notesArray[7] = 1;
}
if ($p_mail_suite != $mail_suite) {
	$notesArray[8] = 1;
}
if ($p_mail_city != $mail_city) {
	$notesArray[9] = 1;
}
// state cannot change
/*
if ($p_mail_state != $mail_state) {
	$notesArray[10] = 1;
}
*/
if ($p_mail_zip != $mail_zip) {
	$notesArray[11] = 1;
}

// only if Top Elected Official
if ($role == 'Top Elected Official') {
	if ($p_partofboard != $partofboard) {
		$notesArray[12] = 1;
	}
}

// generate notes into CSV
$numFields = 13;

for ($i = 0; $i < $numFields; $i++) {
	if ($i != ($numFields - 1)) {
		$official_updates = $official_updates . $notesArray[$i] . ',';
	} else {
		$official_updates = $official_updates . $notesArray[$i];
	}
}

if ($numNotes == 0) {
	// insert
	$insertOfficialNotes_sql = sprintf("INSERT INTO notes (Government_ID,Job_ID,Official_ID,timestamp,txt,Writer_ID)
		VALUES ('%s','%s','%s',now(),'%s','0')
	", escape($gid), escape($jid), escape($oid), escape($official_updates));
	$result_insertOfficialNotes = @PowerAlmanac\PDb::query($insertOfficialNotes_sql);
	if (!$result_insertOfficialNotes) {
		die('Error writing to notes database:' . PowerAlmanac\PDb::error());
	}
} else {
	// update
	$updateOfficialNotes_sql = sprintf("UPDATE notes SET txt = '%s'
		WHERE Government_ID = '%s' AND Official_ID = '%s'
		", escape($official_updates), escape($gid), escape($oid));
	$result_updateOfficialNotes = @PowerAlmanac\PDb::query($updateOfficialNotes_sql);
	if (!$result_updateOfficialNotes) {
		die('<p>Error updating notes database: '. PowerAlmanac\PDb::error().'</p>');
	}
}
?>