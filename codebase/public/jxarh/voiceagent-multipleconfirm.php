<?php

include("inc-config.php");

$govtsArray = $_REQUEST['govts']; // array

if (!isset($_REQUEST['govts'])) {
	header("Location: dashboard-voiceagent.php");
	flush();
	exit;
}

//print_r($_REQUEST); exit;

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// loop thru govts
foreach ($govtsArray as $gid_jid) 
{
	// explode
	list($gid, $jid) = explode(",", $gid_jid);
	$VAjob_table = 'va_' . $jid;

	// get all unconfirmed agents in each government
	$readgovtOfficialsVA_sql = sprintf("SELECT * FROM %s
		WHERE Government_ID = '%s'
		ORDER BY Official_ID
		", escape($VAjob_table), escape($gid));
	$result_readgovtOfficialsVA = @PowerAlmanac\PDb::query($readgovtOfficialsVA_sql);
	if (!$result_readgovtOfficialsVA) {
		die("Error reading from $VAjob_table database: $readgovtOfficialsVA_sql" . PowerAlmanac\PDb::error());
	}
	
	while ($row = PowerAlmanac\PDb::fetch_array($result_readgovtOfficialsVA))
	{
		$oid = $row['Official_ID'];
		// va_confirm(in o_id varchar(100),in job_id bigint,out rc boolean)
		$pdo->query(sprintf("CALL va_confirm('%s',%s,@rc)", escape($oid), escape($jid)));
		$pdoObject = $pdo->query("SELECT @rc");
		$rsArray = $pdoObject->fetchAll();
		$rc = $rsArray[0]['@rc'];
	}
	
}

header("Location: dashboard-voiceagent.php");
flush();
exit;

?>