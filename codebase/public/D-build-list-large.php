<?php
include("inc/config.php");
$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$filename = 'DL1409114735.csv';

if (!$handle = fopen($filename, 'r')) {
	 print "Cannot open file ($filename)";
	 exit;
}

$handle = fopen ($filename,"r");
$govtOfficalsArray = array();
while ($data = fgetcsv ($handle, 1000, ",")) {
	$governmentOfficialID = $data[0];
	array_push($govtOfficalsArray,$governmentOfficialID);
}

echo("Building Government Official IDs Test - LARGE...<br />");

$startTime = time();

echo("Start Time: $startTime<br />");

foreach ($govtOfficalsArray as $govtOfficalsID) {
	$pdo->query(sprintf("CALL SaveIDForDeltekTable('%s','deltekLargeTable')", escape($govtOfficalsID)));
}

$endTime = time();

echo("End Time: $endTime<br />");

echo("Finished build.<br />");

?>