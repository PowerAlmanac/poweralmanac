<?php

include("inc/config.php");
include("inc/protect-register.php");

$record_id = $_REQUEST['id'];

$RegUser_ID = $_SESSION['RegUser_ID'];
$User_Email = $_SESSION['user_email'];
$User_FirstName = $_SESSION['user_firstname'];
$User_LastName = $_SESSION['user_lastname'];
$User_Subscription = $_SESSION['user_subscription'];


// Sanitize
$record_id = Sanitize::number($record_id);

$title = 'Download FULL SET Records';

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

// get searchname
$pdo->query(sprintf("CALL GetFileName(%s,@filename)", escape($record_id))); // IN Saved_ID BIGINT, OUT FileName VARCHAR(255)
$pdoObject = $pdo->query("SELECT @filename");
$rsArray = $pdoObject->fetchAll();
$filename = $rsArray[0]['@filename'];
// strip extension
$savedname = str_replace(".zip","",$filename);

// number of records to download
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}
$readDownloads_sql = sprintf("SELECT * FROM SavedDownloads
	WHERE RegUser_ID = '%s' AND SavedDownloads_ID = '%s'
	LIMIT 1
", escape($RegUser_ID), escape($record_id));
$result_readDownloads = @PowerAlmanac\PDb::query($readDownloads_sql);
if (!$result_readDownloads) {
	die('Error reading from SavedDownloads database:' . PowerAlmanac\PDb::error());
}
$onerow = PowerAlmanac\PDb::fetch_array($result_readDownloads);
$Download_NumRecords = $onerow['Download_NumRecords'];
$Download_NewRecords = $onerow['Download_NewRecords'];

$searchParams = $_SESSION['lastSearchParams'];
//$numRecords2DL = $_SESSION['lastSearchNumMatched'];

$numRecords2DL = $Download_NumRecords + $Download_NewRecords;

$pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
$pdoObject2 = $pdo->query("SELECT @JSON_String");
$rsArray2 = $pdoObject2->fetchAll();
$json2a = $rsArray2[0]['@JSON_String'];
$jsonArray2a = json_decode($json2a, TRUE);
$User_Password = $jsonArray2a['User_Password'];
$User_Parent = $jsonArray2a['User_Parent'];
$User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
$User_ActivationCode = $jsonArray2a['User_ActivationCode'];

$pdo->query(sprintf("CALL GetSubscriptionInfo(%s,@JSON_String)", escape($User_Subscription)));
$pdoObject3 = $pdo->query("SELECT @JSON_String");
$rsArray3 = $pdoObject3->fetchAll();
$json3 = $rsArray3[0]['@JSON_String'];
$jsonArray3 = json_decode($json3, TRUE);
$Sub_Name = $jsonArray3['Sub_Name'];
$Sub_CostPerDL = $jsonArray3['Sub_CostPerDL'];

// call rountine to check for payment if not unlimited

if ($User_Subscription != '10') {
	//  PROCEDURE CheckPaymentForFullDownload(IN RegID BIGINT,IN SearchName VARCHAR(255),OUT paid BIGINT,OUT unpaid BIGINT)
	$currentTableID = $_SESSION['currentTableID'];
	
	$pdo->query(sprintf("CALL CheckPaymentForFullDownload(%s,'%s',@paid,@unpaid)", escape($RegUser_ID), escape($savedname)));
	$pdoObject = $pdo->query("SELECT @paid,@unpaid");
	$rsArray = $pdoObject->fetchAll();
	//echo("CALL CheckPaymentForFullDownload($RegUser_ID,'$savedname',@paid,@unpaid)");print_r($rsArray);exit;
	$paid = $rsArray[0]['@paid'];
	$unpaid = $rsArray[0]['@unpaid'];
} else {
	$paid = 0;
	$unpaid = 0;
}

$previousDLRecords = $paid;
$record2Charge = $unpaid;
$numRecords2DLAdj = $record2Charge;

if ($numRecords2DL == '') $numRecords2DL = 0;

if ($record2Charge <= $User_DL_Reserves) {
	$passThru = '1';
	$deltaRecords = $numRecords2DLAdj - $User_DL_Reserves;
	$amount2Charge = '0.00';
} else {
	$passThru = '0';
	$deltaRecords = $numRecords2DLAdj - $User_DL_Reserves;
	$downloadCost = $deltaRecords * $Sub_CostPerDL;
	$amount2Charge = number_format($downloadCost,2);
}

$_SESSION['record2Charge'] = $record2Charge;

?>
<html>
<head>
<title><?php echo($title); ?> | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
	Custom.checkAll();
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
	Custom.clear();
}
//  End -->
</script>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'mydownloads';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstrip2.png" valign="top">
            <br>
            <table cellpadding="5" cellspacing="5" border="0" width="95%" align="center" bgcolor="#FFFFFF">
            <tr>
                <td colspan="2">
                <h3>Download Records</h3>
                </td>
            </tr>
            <tr>
                <td width="50%">
                  <table cellpadding="2" cellspacing="3" width="100%" border="0" align="center">
                  <tr>
                      <td width="75%" align="center">
                      <p><b># of records to download</b></p>
                      </td>
                      <td width="25%" align="right">
                      <p><b><?php $nr = number_format($numRecords2DL); echo("$nr"); ?></b></p>
                      </td>
                  </tr>
					  <?php
                      if ($passThru == '0') {
                          // check if sub-user
                          if ($User_Parent != '0') {
                              // sub-user
							  // get user parent account info
							  $sql ="USE " . escape($dbname);
							  $dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
							  if (!$dbcnx) {
								  print("Unable to connect to the database server at this time.\n");
							  exit();
							  }
							  if (!@PowerAlmanac\PDb::query($sql)){
								  die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
							  }
							  $read_sql = sprintf("SELECT * FROM registeredusers
								  WHERE RegUser_ID = '%s'
								  LIMIT 1
							  ", escape($User_Parent));
							  $result_read = @PowerAlmanac\PDb::query($read_sql);
							  if (!$result_read) {
								  die('Error reading from registeredusers database:' . PowerAlmanac\PDb::error());
							  }
							  $row = PowerAlmanac\PDb::fetch_array($result_read);
							  $Parent_Email = $row['User_Email'];
							  echo('<tr><td colspan="2" align="center">');
							  echo("<p><b>You need to have more credits to download this file.</b></p>");
							  echo("<p><a rel='shadowbox;height=200;width=625' href='PA-CreditRequest.php?e=$Parent_Email&c=$deltaRecords' class='downloadAction'>Request More Credits Now</a><br>");
                              echo("(send an email request to this account's master user)</p>");
							  echo('</td></tr>');
                          } else {
							  echo('<tr><td align="center" valign="top"><p>You need to buy more credits to download this file.</td><td></td></tr>');
							  echo('<tr><td align="center" valign="top">');
							  echo("<p><b>The total cost for these additional credits is:</b></p>");  
                      ?>
                              <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                              <input type='hidden' name="custom" value="<?php echo("$User_ActivationCode-$User_Email-PADL"); ?>">
                              <input type="hidden" name="cmd" value="_xclick">
                              <input type="hidden" name="amount" value="<?php echo("$$amount2Charge"); ?>">
                              <input type="hidden" name="item_name" value="Power Almanac Download Credits Purchase">
                              <input type="hidden" name="item_number" value="PADL">
                              <input type="hidden" name="quantity" value="1">
                              <input type="hidden" name="business" value="ron.mester@ltbl.com">
                              <input type="hidden" name="email" value="<?php echo($User_Email); ?>">
                              <input type="hidden" name="first_name" value="<?php echo($User_FirstName); ?>">
                              <input type="hidden" name="last_name" value="<?php echo($User_LastName); ?>">
                              <input type="hidden" name="address1" value="9 Elm Street">
                              <input type="hidden" name="address2" value="Apt 5">
                              <input type="hidden" name="city" value="Berwyn">
                              <input type="hidden" name="state" value="PA">
                              <input type="hidden" name="zip" value="19312">
                              <input type="hidden" name="night_phone_a" value="610">
                              <input type="hidden" name="night_phone_b" value="555">
                              <input type="hidden" name="night_phone_c" value="1234">
                              <input type="hidden" name="no_shipping" value="1">
                              <input type="hidden" name="return" value="<?php echo($serverURL); ?>/PA-dlauth-success.php">
                              <input type="hidden" name="rm" value="2">
                              <input type="hidden" name="cbt" value="Return to Power Almanac">
                              <input type="hidden" name="cancel_return" value="<?php echo($serverURL); ?>/PA-dlauth-cancel.php">
                              <input type="image" src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                              <img alt="" border="0" src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/scr/pixel.gif" width="1" height="1">
                              </form>
                      <?php
					 		echo('</td><td align="right" valign="top">');
                            echo("<p><b>$$amount2Charge</b></p>");
					  		echo('</td></tr>');
                          }
                      } else {
						  echo('<tr><td colspan="2" align="center">');
                          echo("<p><b>Good News! You have enough credits for this download.</b></p>");
                          echo("<p><a href='PA-DownloadFull.php?id=$record_id' class='downloadAction'>Proceed to Start the Download</a><br>(you will <u>NOT</u> be charged any fees)</p>");
						  echo('</td></tr>');
                      }
                      ?>
                  </table>
                </td>
                <td width="50%">&nbsp;
                	
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <hr align="center" width="100%" size="2" color="#333333">
                <br>
                <b class="downloadAction"><u>EXPLANATION</u></b>
                </td>
            </tr>
            <tr>
            	<td colspan="2">
                <table cellpadding="5" cellspacing="5" border="0" width="100%" align="center">
                <tr>
                	<td valign="top" width="40%">
                    <p><b>You Need This Many Credits To Download This File:</b></p>
                    </td>
                	<td valign="top" width="10%" align="right">
                    <p><b><?php $nrAdj = number_format($numRecords2DLAdj); echo("$nrAdj"); ?></b></p>
                    </td>
                	<td valign="top" width="50%">
                    <p><b><i>Here's How This Was Calculated</i></b></p>
                    <table cellpadding="1" cellspacing="1" border="0" width="90%" align="center">
                    <tr>
                    	<td width="80%">
                        # of records in this download file
                        </td>
                        <td width="20%" align="right">
                        <?php $nr = number_format($numRecords2DL); echo("$nr"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td >
                        Less the # of these records you've downloaded in previous files
                        </td>
                        <td align="right">
                        <?php $pr = number_format($previousDLRecords); echo("$pr"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Equals the # credits required to download this file
                        </td>
                        <td align="right">
                        <p><b><?php echo("$nrAdj"); ?></b></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                	<td valign="top">
                    <p><b>Your Current Credit Balance is:</b></p>
                    </td>
                	<td valign="top" align="right">
                    <p><b><?php $nres = number_format($User_DL_Reserves); echo("$nres"); ?></b></p>
                    </td>
                	<td valign="top">
                    
                    </td>
                </tr>
                <?php
				// 3 scenarios here
				if ($passThru == '1')  {
				?>
                <tr>
                	<td valign="top">
                    <p><b>After this download, your credit balance will be:</b></p>
                    </td>
                	<td valign="top" align="right">
                    <p><b><?php $nrem = number_format($User_DL_Reserves-$numRecords2DLAdj); echo("$nrem"); ?></b></p>
                    </td>
                	<td valign="top">
                    <p><b><i>Here's How This Was Calculated</i></b></p>
                    <table cellpadding="1" cellspacing="1" border="0" width="90%" align="center">
                    <tr>
                    	<td width="80%">
                        Your current credit balance
                        </td>
                        <td width="20%" align="right">
                        <?php echo("$nres"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Less the # credits required to download this file
                        </td>
                        <td align="right">
                        <?php echo("$nrAdj"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Equals your credit balance AFTER you download this file
                        </td>
                        <td align="right">
                        <p><b><?php echo("$nrem"); ?></b></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
             
            <?php
				} else {
					if ($User_Parent == '0') {
						// main account
			?>
                <tr>
                	<td valign="top" width="40%">
                    <p><b>To download this file, you need to buy this many credits:</b></p>
                    </td>
                	<td valign="top" width="10%" align="right">
                    <p><b><?php $dr = number_format($deltaRecords); echo("$dr"); ?></b></p>
                    </td>
                	<td valign="top" width="50%">
                    <p><b><i>Here's How This Was Calculated</i></b></p>
                    <table cellpadding="1" cellspacing="1" border="0" width="90%" align="center">
                    <tr>
                    	<td width="80%">
                        The # of credits required to download this file
                        </td>
                        <td width="20%" align="right">
                        <?php echo("$nrAdj"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Less your current credit balance
                        </td>
                        <td align="right">
                        <?php echo("$nres"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Equals the # credits you need to buy to download this file
                        </td>
                        <td align="right">
                        <p><b><?php echo("$dr"); ?></b></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                	<td valign="top" width="40%">
                    <p><b>The cost to purchase the required credits:</b></p>
                    </td>
                	<td valign="top" width="10%" align="right">
                    <p><b><?php echo("$$amount2Charge"); ?></b></p>
                    </td>
                	<td valign="top" width="50%">
                    <p><b><i>Here's How This Was Calculated</i></b></p>
                    <table cellpadding="1" cellspacing="1" border="0" width="90%" align="center">
                    <tr>
                    	<td width="80%">
                        The # of credits you need to buy to download this file
                        </td>
                        <td width="20%" align="right">
                        <?php echo("$dr"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        multipled by the cost per download credit under your current subscription plan
                        </td>
                        <td align="right">
                        <?php echo("$$Sub_CostPerDL"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Equals the amount you need to pay to download this file
                        </td>
                        <td align="right">
                        <p><b><?php echo("$$amount2Charge"); ?></b></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
            <?php
					} else {
						// sub user
			?>
                <tr>
                	<td valign="top" width="40%">
                    <p><b>To download this file, you need this many additional credits:</b></p>
                    </td>
                	<td valign="top" width="10%" align="right">
                    <p><b><?php $dr = number_format($deltaRecords); echo("$dr"); ?></b></p>
                    </td>
                	<td valign="top" width="50%">
                    <p><b><i>Here's How This Was Calculated</i></b></p>
                    <table cellpadding="1" cellspacing="1" border="0" width="90%" align="center">
                    <tr>
                    	<td width="80%">
                        The # of credits required to download this file
                        </td>
                        <td width="20%" align="right">
                        <?php echo("$nrAdj"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Less your current credit balance
                        </td>
                        <td align="right">
                        <?php echo("$nres"); ?>
                        </td>
                    </tr>
                    <tr>
                    	<td>
                        Equals the # of additional credits you need from the master user
                        </td>
                        <td align="right">
                        <p><b><?php echo("$dr"); ?></b></p>
                        </td>
                    </tr>
                    </table>
                    </td>
                </tr>
            <?php
					}
				}
			?>
                </table>
                </td>
            </tr>
            <!--
            <tr>
                <td colspan="2">
                <p>
                <b>Current Account</b><br />
                #Records To Download: <b><?php echo("$numRecords2DL"); ?></b><br />
                #Download Units Left: <b><?php echo("$User_DL_Reserves"); ?></b><br />
                <br />
                <b>Additional Costs</b><br />
                #Records To Charge: <b><?php echo("$deltaRecords"); ?></b><br />
                #Cost Per Record: <b><?php echo("$$Sub_CostPerDL"); ?></b><br />
                #Amount To Charge: <b><?php echo("$$amount2Charge"); ?></b><br />
                </p>
                </td>
            </tr>
            -->
            </table>
            <br>
			</td>
		</tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstrip2.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" align="center" bgcolor="#ebebeb">
    <?php
	include("inc/oldfooter.php");
	?>
    </td>
</tr>
</table>
<br />
</body>
</html>
