<?php
include("inc/config.php");

//redirect auth'd testers to test version
if(!$_SESSION['user_email'] == "jon@blendimc.com" || !$_SESSION['user_email'] == "tim@blendimc.com"){
  header("Location: /checkout.php");
  die();
}

$_SESSION['redirect'] = 'checkout.php';
include("inc/protect-register.php");
$title = 'Terms of Service | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");

$bestPricing = getSubList($dbname,$dbhost,$dbuser,$dbpass);

if($_SESSION['user_subscription'] > 0){
    $User_Email = $_SESSION['user_email'];
    $pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
    $pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)"), escape($User_Email));
    $pdoObject2 = $pdo->query("SELECT @JSON_String");
    $rsArray2 = $pdoObject2->fetchAll();
    $json2a = $rsArray2[0]['@JSON_String'];
    $jsonArray2a = json_decode($json2a, TRUE);
    $User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
    $User_ActivationCode = $jsonArray2a['User_ActivationCode'];
}

$user_subscription = $_SESSION['user_subscription'];
$User_LastName = $_SESSION['user_lastname'];
$User_FirstName = $_SESSION['user_firstname'];


if(isset($_GET['subscription']))
{
  //for link clicked subscription upgrades
  $newSub = $_GET['subscription'];

  //automatically assign all grandfathered-in sub types to power 20 (level six, which is the second item in our sub array)
  $select = $bestPricing[1];

  //grab the passed through subscription
  for($i = 0; $i < count($bestPricing); $i++){
    if($bestPricing[$i]['Sub_Level'] == $newSub)
    {
      $select = $bestPricing[$i];
    }
  }

  //go ahead and set quantity vars to zero, these are only used for a checkout from the search page
  $adjustedQuantity = 0;
  $quantity = 0;
  $subscriptionLevel = $select['Sub_Level'];

  //current power unlimited subscribers do not get to keep their million credits, but any lower subscription is able to keep them.
  if($user_subscription == 10){
    $User_DL_Reserves = 0;
  }


}
else
{
  //for search page stuff
  //let power unlimited users jump straight to download page
  if($_SESSION['user_subscription'] == 10){
      header("Location: ./PA-DownloadNow.php");
  }

  $checkout_data = json_decode($_SESSION['checkout'],TRUE);
  $quantity = $checkout_data['Num_Matched_Officials'];

  $_SESSION['lastSearchNumMatched'] = $quantity;

  //temporary test vars
  //$quantity = 551; 
  //$User_DL_Reserves = 500;

  //get the amount of credits that must be paid for
  //if user is power + subscriber, skip this check
  if($_SESSION['user_subscription'] != 10){
      $adjustedQuantity = $quantity - $User_DL_Reserves;
  }
  else{
      $adjustedQuantity = 0;
  }


  //if they have enough credits, assign the credit amount to the amount they need to deduct
  //else, remove all of their credits
  if($adjustedQuantity < 0){
      $deduct = $quantity;
      $adjustedQuantity = 0;
  }
  else{
      $deduct = $User_DL_Reserves;
  }

//calculate the best value for user
//calculate best value based on number of records. Each record holds the pricing break for itself.


    $level = 0;
    $old_sub = true;
    for($i = 0; $i < count($bestPricing); $i++){
        //assign user's current download costs per record if they already have a subscription
        if($_SESSION['user_subscription'] == $bestPricing[$i]['Sub_Level']){
            $currentPrice = $bestPricing[$i]['cost'];
            $old_sub = false;
        }

        if($adjustedQuantity >= $bestPricing[$i]['pricingBreak']){
            $level = $i;
            //assign the db subscription number so we can compare upgrade levels
            $upgrade = $bestPricing[$i]['Sub_Level'];
        }
    }
    $select = $bestPricing[$level];
    if($old_sub == true){
       if($_SESSION['user_subscription'] == 6)
       {
           $currentPrice = 0.098;
       }
       else if($_SESSION['user_subscription'] == 5)
       {
          $currentPrice = 0.098;
       }
       else{
        $currentPrice = 0.15;
       }
    }


  // show current subscription upgrade options for search based purchases

  if(isset($_SESSION['user_subscription']) == true && $_SESSION['user_subscription'] > 0){
      if($upgrade > $_SESSION['user_subscription']){
          $bundledRecords = $select['included'];
          $upgradeText = 'UPGRADE TO:';
          $subscriptionLevel = $upgrade;
      }
      else{
          $select['cost'] = $currentPrice;
          $select['included'] = 0;
          $subscriptionLevel = $_SESSION['user_subscription'];
      }
  }else{
      $bundledRecords = $select['included'];
  }

  // set subscription level for subscribe based purchases
  if(isset($newSub)){


  }


  if($adjustedQuantity > $bundledRecords){
      $payForCreditsSeparately = $adjustedQuantity - $bundledRecords;
  }else{
      $payForCreditsSeparately = 0;
  }

  //end search page specific stuff

}


//calculate pricing
$totalPrice = ($payForCreditsSeparately * $select['cost']) + $select['price'];
$_SESSION['total_price'] = $totalPrice;
$costPerRecord = round($totalPrice / $adjustedQuantity, 2);

// The "included" amount that is currently listed is written in a confusing manner. This should ultimately
// be revised, but for now, I'm assigning a new variable to make it more user-friendly.
$actualIncluded = $select['included'] + $User_DL_Reserves;

$leftover = ($select['included'] + $User_DL_Reserves >= $quantity ? ($select['included'] + $User_DL_Reserves) - $quantity : 0);

$totalNewCredits = $select['included'] + $User_DL_Reserves + $payForCreditsSeparately;

//adding a line for just new credits to update new amount
$newCredits = $select['included'] + $payForCreditsSeparately;

// store session so user is charged- update 3-20-14: variable should be the $quantity, as the number removed is fixed regardless of how many
// credits they have or don't have.
$_SESSION['record2Charge'] = $quantity;

//hack for unlimited plans to display different info
if(($user_subscription == "10" && !isset($newSub)) || $upgrade == "10"){
    $actualIncluded = "Unlimited";
    $leftover = "N/A";
}
else{
    $actualIncluded = number_format($actualIncluded);
    $leftover = number_format($leftover);
}

// The ($subscriptionLevel == 2 ? 3 : $subscriptionLevel) is there because I had to hack this part to change level 2 subscriptions
// to level 3. The database doesn't recognize level 2, and the Power 3 values are literally set as "3" in the database, bypassing
// 2 completely. Therefore, users that registered with a very specific amount would get set to 2 (and have no account associated
// with them). This hack fixes it.
$custom = $User_ActivationCode . ',' . $_SESSION['RegUser_ID'] . ',' . ($subscriptionLevel == 2 ? 3 : $subscriptionLevel) . ',' . $newCredits;
?>

            <div class="intro">
                <div class="intro-holder">
                    <h1>Checkout</h1>
                </div>
            </div>
            <div class="main-holder layout-n checkout">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame searchgov">
							<?php							
							//if($_GET['scrvr'] == $_SESSION['checkout_secure']) :
							?>
                            <div class="section">
                                <h1>Review and Checkout</h1>
                                <div class="checkouthalf">
                                    <h4>Cost Breakdown</h4>
                                    <div class="lineitem">Quantity<span><?php echo number_format($quantity); ?></span></div>
                                    <div class="lineitem">Cost/Record<span>$<?php echo number_format($costPerRecord,2); ?></span></div>
                                    <div class="lineitem total">Grand Total:<span>$<?php echo number_format($totalPrice,2); ?></span></div>
                                </div>
                                <div class="checkouthalf">
                                    <h4>Included in purchase</h4>
                                    <div class="lineitem"><?php echo $select['Sub_Name']; ?></div>
                                    <div class="lineitem">Included Credits<span><?php echo number_format($select['included']) ?></span></div>
                                    <div class="lineitem">Remaining Credits<span><?= $leftover ?></span></div>
                                </div>
                            </div>
                            <div class="section last">


                                <h1>How do you want to pay?</h1>
                                <div style="float: left; text-align: center">
                                <?php if($adjustedQuantity !== 0 || isset($newSub)): ?>
                                <img src="images/securepayments.jpg" alt="buy government contact information securely" />
                                <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                  <input type='hidden' name="custom" value="<?php echo $custom ?>">
                                  <input type="hidden" name="cmd" value="_xclick">
                                  <input type="hidden" name="amount" value="<?php echo number_format($totalPrice,2); ?>">
                                  <input type="hidden" name="item_name" value="Power Almanac Download Credits Purchase">
                                  <input type="hidden" name="item_number" value="PADL">
                                  <input type="hidden" name="quantity" value="1">
                                  <input type="hidden" name="business" value="jon@blendimc.com">
                                  <input type="hidden" name="email" value="<?php echo($User_Email); ?>">
                                  <input type="hidden" name="first_name" value="<?php echo($User_FirstName); ?>">
                                  <input type="hidden" name="last_name" value="<?php echo($User_LastName); ?>">
                                  <input type="hidden" name="address1" value="9 Elm Street">
                                  <input type="hidden" name="address2" value="Apt 5">
                                  <input type="hidden" name="city" value="Berwyn">
                                  <input type="hidden" name="state" value="PA">
                                  <input type="hidden" name="zip" value="19312">
                                  <input type="hidden" name="night_phone_a" value="610">
                                  <input type="hidden" name="night_phone_b" value="555">
                                  <input type="hidden" name="night_phone_c" value="1234">
                                  <input type="hidden" name="no_shipping" value="1">
                                  <input type="hidden" name="notify_url" value="http://www.poweralmanac.com/PA-ipn-new.php">
                                  <input type="hidden" name="return" value="<?php echo($serverURL); ?>/PA-DownloadNow.php">
                                  <input type="hidden" name="rm" value="2">
                                  <input type="hidden" name="cbt" value="Return to Power Almanac">
                                  <input type="hidden" name="cancel_return" value="<?php echo($serverURL); ?>/PA-dlauth-cancel.php">
                                  <input type="image" style="margin-top: 50px;" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                  <img alt="" border="0" src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/scr/pixel.gif" width="1" height="1">
                              </form>
                                <?php else: ?>
                                    <a href="PA-DownloadNow.php" class="btn btn-warning" style="color: white;text-decoration:none;">Download Records</a>
                                <?php endif; ?>
                          </div>
                              <!--<?php echo '<div style="float: right; margin-right: 100px; background-color: gray; text-align: center;" class="checkout-ad"><img style="margin: auto" src="'.$select['img'].'"></div>'; ?>-->
                            </div>
							
                            <?php //endif;
                            ?>

							<div class="clearfix">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php include("inc/footer.php"); ?>
<?php unset($_SESSION['redirect']); ?>