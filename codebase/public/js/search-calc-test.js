
    var filterGovt = false;
    var filterEmail = false;
    var quartile = Array();
    var globalshare = '';
    var globalscrvr = '';
    var searchParams = {
		byText: '',
		populationMoreThan: 0,
		populationThan: 100000000,
		criteria: 'expenseBudgetQuartile',
		'off_role': 'govtOff_role'
	}

	var stateString = '';
	

	$(document).ready(function() {

		// integer key/value matchups for the slider
		var popRangeArray = {
			0: 0,
			10: 1000,
			20: 2500,
			30: 5000,
			40: 10000,
			50: 25000,
			60: 50000,
			70: 100000,
			80: 500000,
			90: 100000000
		}

		//expand and show divs
		$('.expand-btn').on('click',function(){
			$(this).parents('.tab').find('.expand').hide();
			$($(this).attr('data-expand')).show();
		})

		//tabs for faq page
		if($('.tabbed').length>0){
			var tabs = $('#main').find('.tab');
			$(tabs).hide();
			if(window.location.hash){
				var re1 = window.location.hash.match('[^\\?]*')[0];
				$(re1).show();
				$('.top-tabs').find('[href='+re1+']').addClass('selected-tab');
			}
			else{
				$(tabs[0]).show();
				$($('.top-tabs').find('.tablink')[0]).addClass('selected-tab');
			}
			
			$(tabs).find('.answer').hide();


		//open the first option for each expanding set.
		$('.expand').each(function(){
			$(this).parents('section').find('.expand').first().show();
		});

			//handle click between tabs
			$('.top-tabs').find('.tablink').on('click',function(e){
				e.preventDefault();
				$('.top-tabs').find('.tablink').removeClass('selected-tab');
				$(this).addClass('selected-tab');
				var tabId = $(this).attr('href');
				window.location.hash = tabId;
				$(tabs).hide();
				$(tabId).show();
			});



			//handle open / close answers
			$('.tabbed.faq').on('click','li:not(.answer)', function(){
				var answer = $(this).next('.answer');
				if(answer.is(':hidden')){
					answer.fadeIn(350);
					$(this).addClass('hide-up');
				}
				else{
					answer.fadeOut(350);
					$(this).removeClass('hide-up');
				}
			})
		}

		//jquery for the search page specific (not tabs)
		if($('.searchgov').length>0){
			var steps = $('#step-amounts').find('.step-slider');
			steps.each(function(){
				$(this).css('width', ((steps.length) / 1)+'%');
			})
			//initialize jquery plugin for range slider (not dependent on jquery ui)
			var steps = 10;
			$('#rangeslider').noUiSlider({
				connect: true,
				orientation: 'horizontal',
				handles: 2,
				margin: 10,
				range: [0,90],
				start: [30,50],
				step: 10,
				slide: function(){
					var leftpos = $(this).val()[0];
					var rightpos = $(this).val()[1];
					searchParams['populationMoreThan'] = popRangeArray[leftpos];
					searchParams['populationThan'] = popRangeArray[rightpos];
					getDetailedBody(searchParams);
				}
			});

			//handle next buttons
			$('.step-holder').find('.next').on('click', function(){
				var tabId = $(this).attr('href');
				window.location.hash = tabId;
				$(tabs).hide();
				$(tabId).show();
				$('.top-tabs').find('.tablink').removeClass('selected-tab'); 
				$('.top-tabs').find('[href='+window.location.hash+']').addClass('selected-tab');
			});

			//handle final button, only go if search is completed
			$('#checkout-final').on('click', function(e){
				if($(this).hasClass('ready')){
				}
				else{
					e.preventDefault();i
				}
			});

			//tie preview button
			$('#previewResults').colorbox({
				iframe: true,
				width: '70%',
				height: '80%'
			});

			//handle open and close sections on pricing bar
			$('#switches').hide();
			$('#shareResults').hide();
			$('#priceBar .grayinset').on('click', function(){
				$(this).next('.blackinset').toggle();
			});

			//handle toggle switch filters
			$('#onePerGov > .switch').on('switch-change', function (e, data) {
			    if(data.value){
			    	searchParams['top_elected'] = 1
			    }
			    else{
			    	delete searchParams['top_elected'];
			    }
			    getDetailedBody(searchParams);
			});

			$('#includeEmails > .switch').on('switch-change', function (e, data) {
			    if(data.value){
			    	searchParams['off_email'] = 'govtOff_email';
			    }
			    else{
			    	delete searchParams['off_email'];
			    }
			    getDetailedBody(searchParams);
			});

			//handle sharing stuff
			$('#firstEmail').one('focus', function(){
				$('#moreEmail').show();
			})
			var numberEmails = 1;
			$('#moreEmail').on('click', function(){
				if(numberEmails < 3){
					$('#firstEmail').before('<input type="text" id="email'+numberEmails+'" placeholder="Email" type="email">');
					numberEmails++;
					if(numberEmails == 3){
						$('#moreEmail').hide();
					}
				}
			});
			
			$("#priceBar").find('.tooltipLink').tooltip();

		}
















		//REVERSE SET EVERYTHING TO LOAD A SAVED SEARCH FOR EXAMPLE, PULL IT FROM A GET URL STRING AND POSSIBLY A SESSION VARIABLE
			var queryString = {};
			var queryCount = 0;
			var quartile = [];

			//restore saved search from link
			if(getParameter('restoreSaved') == 'true'){
				lastSearch = window.location.search;
			}

			if(lastSearch.length > 1){
				lastSearch = lastSearch.replace(/%25/g,'%');

				//create an object out of the search parameters
				lastSearch.replace(
				    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
				    function($0, $1, $2, $3) {
				    	queryCount++;
				    	if(queryCount > 1){
					    	if(queryString[$1]){
					    		if($.isArray(queryString[$1])){
					    			queryString[$1].push($3);
					    		}
					    		else{
					    			var tempstate = queryString[$1];
					    			queryString[$1] = new Array();
					    			queryString[$1][0] = tempstate;
					    			queryString[$1][1] = $3;
					    		}
					    	}else{
					    		queryString[$1] = $3;
					    	}
					    }
				    }
				);

				//roles
				if(queryString['role_1'] == 'Top+Elected+Official'){
					$('#mayor').prop('checked', true);
				}
				if(queryString['role_3'] == 'Governing+Board+Member'){
					$('#alderman').prop('checked', true);
				}
				if(queryString['role_8'] == 'Head+of+Fire+Protection+Services'){
					$('#fire').prop('checked', true);
				}
				if(queryString['role_4'] == 'Head+of+Finance%2FBudgeting'){
					$('#finance').prop('checked', true);
				}
				if(queryString['role_2'] == 'Top+Appointed+Official'){
					$('#manager').prop('checked', true);
				}
				if(queryString['role_6'] == 'Head+of+Public+Works'){
					$('#publicworks').prop('checked', true);
				}
				if(queryString['role_7'] == 'Head+of+Law+Enforcement'){
					$('#police').prop('checked', true);
				}
				if(queryString['role_5'] == 'Head+of+Purchasing%2FProcurement'){
					$('#purchasing').prop('checked', true);
				}
				if(queryString['role_10'] == 'Head+of+IT'){
					$('#infotech').prop('checked', true);
				}
				if(queryString['role_9'] == 'Clerk'){
					$('#clerk').prop('checked', true);
				}

				//saved searches use integers for population checkboxes, so we need to support that
				if(queryString['population%5B%5D']){
					if( Object.prototype.toString.call(queryString['population%5B%5D']) === '[object Array]' ){
						var popBoxes = queryString['population%5B%5D'];
						queryString['populationThan'] = popRangeArray[(Math.max.apply(Math, popBoxes) * 10)];
						queryString['populationMoreThan'] = popRangeArray[(Math.min.apply(Math, popBoxes) * 10)];
					}
					else{
						//our parameters changed a little bit, so we have to make sure things line up.
						var popBoxes = parseInt(queryString['population%5B%5D']);
						if(popBoxes == 9){
							queryString['populationMoreThan'] = popRangeArray[(8 * 10)];
							queryString['populationThan'] = popRangeArray[(9 * 10)];
						}else{
							queryString['populationMoreThan'] = popRangeArray[(popBoxes * 10)];
							queryString['populationThan'] = popRangeArray[((popBoxes+1) * 10)];
						}
					}
					delete queryString['population%5B%5D'];
				}
				//population range
				if(queryString['populationMoreThan'] || queryString['populationThan']){
					var positions = Array(getIndexByValue(popRangeArray,queryString['populationMoreThan']), getIndexByValue(popRangeArray,queryString['populationThan']));
					$('#rangeslider').val(positions);
				}else{
					var positions = Array(0,90);
					$('#rangeslider').val(positions);
				}

				//govt type
				var govtypeChecked = false;
				if(queryString['govType_municipalities'] == 'municipality'){
					$('#cities').prop('checked', true);
					govtypeChecked = true;
				}
				if(queryString['govType_counties'] == 'county'){
					$('#counties').prop('checked', true);
					govtypeChecked = true;
				}
				if(queryString['govType_townships'] == 'township'){
					$('#towns').prop('checked', true);
					govtypeChecked = true;
				}

				//add all three to search object if none are sent by default
				if(govtypeChecked == false){
					$('#cities').prop('checked',true);
					$('#counties').prop('checked',true);
					$('#towns').prop('checked',true);
					queryString['govType_municipalities'] = 'municipality';
					queryString['govType_townships'] = 'township';
					queryString['govType_counties'] = 'county';
				}


				checkedStates = queryString['govLoc_byState%5B%5D'];

				//location
				if(queryString['byZipcode']){
					$('#r-location').find('.expand').hide();	
					$('#r-location').find('.expand-btn').removeClass('active');
					$('#search-zip').show();
					$('#search-zip').find('input').val(queryString['byZipcode']);
					$('#r-location').find('button[data-expand="#search-zip"]').addClass('active');
				}
				else if(queryString['region']){
					$('#search-region').show();
					$('#search-national').hide();
					$('#r-location').find('button').removeClass('active');
					$('#r-location').find('button[data-expand="#search-region"]').addClass('active');
					var regionArray = queryString['region'].split("_");
					for(region in regionArray){
						$('#'+regionArray[region]).prop('checked',true);
					}
				}
				else{
					$('#search-state').show();
					$('#search-national').hide();

					$('#r-location').find('button').removeClass('active');
					$('#r-location').find('button[data-expand="#search-state"]').addClass('active');
				}

				for(i in checkedStates){
					var modState = checkedStates[i].replace(/\+/g, '').toLowerCase();
					var modState = modState.replace(/%2b/g, '');
					$('#'+modState).prop('checked', true);
				}


				//spending
				if(queryString['criteria'] == 'expenseBudget'){
					searchParams['criteria'] = 'expenseBudget';
					$('#r-spending').find('button[data-val="expenseBudget"]').addClass('active');
					$('#annual-range').show();
					$('#more-than-spending').val(addCommas(queryString['expenseBudgetMoreThan'] * 1000));
					$('#less-than-spending').val(addCommas(queryString['expenseBudgetLessThan'] * 1000));
				}
				else if(queryString['criteria'] == 'expenseBudgetQuartile'){
					searchParams['criteria'] = 'expenseBudgetQuartile';
					$('#r-spending').find('button').removeClass('active');
					$('#annual-range').hide();
					$('#r-spending').find('button[data-val="expenseBudgetQuartile"]').addClass('active');
					$('#quartile').show();
					var i = 0;
					for(i in queryString['expenseBudgetQuartileSel%5B%5D']){
						$('#quartile').find('input[value='+queryString['expenseBudgetQuartileSel%5B%5D'][i]+']').prop('checked', true);
						quartile.push(queryString['expenseBudgetQuartileSel%5B%5D'][i]);
					}
				}
				else if(queryString['criteria'] == 'perCapitalBudgetQuartile'){
					searchParams['criteria'] = 'perCapitalBudgetQuartile';
					$('#r-spending').find('button').removeClass('active');
					$('#annual-range').hide();
					$('#r-spending').find('button[data-val="perCapitalBudgetQuartile"]').addClass('active');
					$('#quartile').show();
					var i = 0;
					for(i in queryString['perCapitalBudgetQuartileSel%5B%5D']){
						$('#quartile').find('input[value='+queryString['perCapitalBudgetQuartileSel%5B%5D'][i]+']').prop('checked', true);
						quartile.push(queryString['expenseBudgetQuartileSel%5B%5D'][i]);
					}
				}
				
				// expense types
				//alert(queryString['finH%5B%5D']);
				//alert(JSON.stringify(queryString));
				//searchParams['finU[]'];
				//alert(JSON.stringify(searchParams));

				//assign financial categories to the right value, show dropdown beneath
				var catTypes =[
					{catName: 'expType', cvalue: 'Special Categories'},
					{catName: 'finPS', cvalue: 'Public Safety'},
					{catName: 'finPW', cvalue: 'Public Welfare'},
					{catName: 'finH', cvalue: 'Health'},
					{catName: 'finU', cvalue: 'Utilities'},
					{catName: 'finT', cvalue: 'Transportation'},
					{catName: 'finF', cvalue: 'Finance'},
					{catName: 'finL', cvalue: 'Leisure'},
					{catName: 'finM', cvalue: 'Miscellaneous'}
				];

				//loop through each financial type to see if it exists
				for(i in catTypes){
					if(queryString[catTypes[i].catName + '%5B%5D'] && queryString[catTypes[i].catName + '%5B%5D'] != "1"){
						$('#categories').val(catTypes[i].cvalue);

						//show dropdown below for subcategories
						$('#'+catTypes[i].catName).show();

						//assign subvalue if exists
						if(queryString[catTypes[i].catName + '%5B%5D'] != "All"){
							$( '#'+catTypes[i].catName ).find('select').val( decodeURIComponent( queryString[ catTypes[i].catName+'%5B%5D' ].replace(/\+/g, ' ') ) );
						}
					}
				}
				
				//filters
				if(queryString['top_elected']){
					$('#onePerGov > .switch').bootstrapSwitch('toggleState');
				}

				if(queryString['off_email']){
					$('#includeEmails > .switch').bootstrapSwitch('toggleState');
				}
				searchParams = queryString;
				if(typeof(searchParams['byText']) !== "undefined"){}else{searchParams['byText'] = ''}
				for(key in searchParams){
					var oldVal = searchParams[key];
					var newKey = unescape(key);
					delete searchParams[key];
					searchParams[newKey] = unescape(oldVal).replace(/\+/g,' ');
				}

				//budget end

				if(queryString['fiscalYearEnd_Month[]']){
					var n = queryString['fiscalYearEnd_Month[]'].split(',');
					searchParams['fiscalYearEnd_Month[]'] = new Array();
					for(i in n){
						searchParams['fiscalYearEnd_Month[]'].push(n[i]);
						$('#endBudgetYear').find('option[value='+n[i]+']').attr('selected','selected');
					}
					if(n.length == 12){
						$('#manipulateMonths').text('Clear All');
						$('#manipulateMonths').attr('data-all',"0");
					}
				}

				//refresh styled selects
				$('.selectpicker').selectpicker('refresh');

				//make sure expenseBudgetQuartileSel[] is an array, otherwise it returns comma sep values which messes up the results
				if(queryString['expenseBudgetQuartileSel[]']){
					searchParams['expenseBudgetQuartileSel[]'] = queryString['expenseBudgetQuartileSel[]'].split(",");
				}

				if(typeof(searchParams['criteria']) == 'undefined'){
					searchParams['criteria'] = 'expenseBudget';
				}

				delete searchParams['restoreSaved'];
				delete searchParams['selLocation'];
				delete searchParams['govLoc_byRegion[]'];
				getDetailedBody(searchParams);
			}else{
				//if we're not restoring a search, set default values
				checkAllIfEmpty($('#r-roles').find('input'));
				checkAllIfEmpty($('#r-government-type').find('input'));
				$('#selectall0').prop('checked',true);
				$('#statelist input').each(function(){
					this.checked = true;
					stateString += 'govLoc_byState%5B%5D='+ spaceToPlus($(this).siblings('label').text()) + '&';
				});
				searchParams['byText'] = '';

				//set default search to be expensebudget and set no max and min
				searchParams['criteria'] = 'expenseBudget';
				searchParams.expenseBudgetLessThan = '';
				searchParams.expenseBudgetMoreThan = '';

				//set population range slider
				positions = Array(0,90)
				$('#rangeslider').val(positions);

				getDetailedBody(searchParams);
			}










		// THIS PAGE SENDS A URL STRING TO THE SERVER AND THEN USES AJAX TO PULL THE DATA BACK - FOR DETAILS SEE THE STORED SQL PROCEDURE 'SEARCHSUMMARY2' IN THE DATABASE
		//bind data refresh to applicable elements ===============================================================
		//choose roles tab
			$('#r-roles').on('change', 'input', function(){
				if(this.checked){searchParams[$(this).val()] = $(this).attr('data-val')}
				else{delete searchParams[$(this).val()]};

				checkAllIfEmpty($('#r-roles').find('input'));
				
				getDetailedBody(searchParams);
			});

			$('#r-government-type').on('change', 'input', function(){
				if(this.checked){searchParams[$(this).val()] = $(this).attr('data-val')}
				else{delete searchParams[$(this).val()]};

				checkAllIfEmpty($('#r-government-type input'));
				getDetailedBody(searchParams);
			});

			//location tab
			$('#r-location-buttons').on('click', '> .btn', function(){
				var option = $(this).text();
				if(option == 'National'){
					//get rid of state and zip info
					delete searchParams['govLoc_byState[]'];
					delete searchParams['byZipCodeDistance'];
					delete searchParams['byZipcode'];
					getDetailedBody(searchParams);
				}else if(option == 'Region'){
					delete searchParams['govLoc_byState[]'];
					delete searchParams['byZipCodeDistance'];
					delete searchParams['byZipcode'];
					if($('#midwest').prop('checked')){
						stateParams(Midwest);
					}
					if($('#northeast').prop('checked')){
						stateParams(Northeast);
					}
					if($('#south').prop('checked')){
						stateParams(South);
					}
					if($('#west').prop('checked')){
						stateParams(West);
					}
					getDetailedBody(searchParams);
				}else if(option == 'State'){
					delete searchParams['byZipCodeDistance'];
					delete searchParams['byZipcode'];
					var stateCount = 0;
					$('#statelist input').each(function(){
						if($(this).prop('checked')){
							if(stateCount == 0){
								searchParams['govLoc_byState[]'] = spaceToPlus($(this).siblings('label').text());
								stateCount = 1;
							}
							else{
								searchParams['govLoc_byState[]'] += '%2C' + spaceToPlus($(this).siblings('label').text());
							}
						}
					});
					getDetailedBody(searchParams);
				}else if(option == 'Zipcode Radius'){
					var input = $('#search-zip input');
					if(input.val().length == 5){
						searchParams['byZipcode'] = input.val();
						searchParams['byZipCodeDistance'] = $('#radius-select option:selected').val();
						getDetailedBody(searchParams);
					}
				}
			});

			//select all handle for the states
			$('#selectall0').on('change', function(){
				if(this.checked){
					$('#statelist input').each(function(){
						if(this.checked != true){
							this.checked = true;
						}
					});
				}
				else{
					$('#statelist input').each(function(){
						if(this.checked == true){
							this.checked = false;
						}
					});
				}

				var stateCount = 0;
				$('#statelist input').each(function(){
					if($(this).prop('checked')){
						if(stateCount == 0){
							searchParams['govLoc_byState[]'] = spaceToPlus($(this).siblings('label').text());
							stateCount = 1;
						}
						else{
							searchParams['govLoc_byState[]'] += '%2C' + spaceToPlus($(this).siblings('label').text());
						}
					}
				});
				getDetailedBody(searchParams);
			});

			//this is one of the confusing parts, sending a statelist. it helps to see what the stored procedure actually accepts.
			$('#r-location').on('change','#statelist input', function(){
				delete searchParams['govLoc_byState%5B%5D'];
				delete searchParams['govLoc_byState[]'];
				delete searchParams['region'];
				///&govLoc_byState%5B%5D=[^&]*/	
				var states = $('#statelist input');
				var checkAllStates = false;
				if(states.filter(':checked').length === 0){
					checkAllStates = true;
				}

				var stateCount = 0;
				states.each(function(){
					if(checkAllStates == true){
						this.checked = true;
					}

					if($(this).prop('checked')){
						if(stateCount == 0){
							searchParams['govLoc_byState[]'] = spaceToPlus($(this).siblings('label').text());
							stateCount = 1;
						}
						else{
							searchParams['govLoc_byState[]'] += '%2C' + spaceToPlus($(this).siblings('label').text());
						}
					}
					
				});
				getDetailedBody(searchParams);
			});

			$('#r-location').on('change','#search-region input', function(){
				var regionChecked = false;
				searchParams['region'] = '';
				searchParams['govLoc_byState[]'] = '';
				if($('#midwest').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(Midwest);
					searchParams['region'] += 'midwest_';
					getDetailedBody(searchParams);
					regionChecked = true;
				}
				if($('#northeast').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(Northeast);
					searchParams['region'] += 'northeast_';
					getDetailedBody(searchParams);
					regionChecked = true;
				}
				if($('#south').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(South);
					searchParams['region'] += 'south_';
					getDetailedBody(searchParams);
					regionChecked = true;
				}
				if($('#west').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(West);
					searchParams['region'] += 'west_';
					getDetailedBody(searchParams);
					regionChecked = true;
				}
				if(regionChecked == false){
					$('#midwest').prop('checked',true);
					$('#northeast').prop('checked',true);
					$('#south').prop('checked',true);
					$('#west').prop('checked',true);
					searchParams['govLoc_byState[]'] = stateParams(West) + '%2C' + stateParams(South) + '%2C' + stateParams(Northeast) + '%2C' + stateParams(Midwest);
					getDetailedBody(searchParams);
				}
			});

			$('#r-location').on('keyup','#search-zip input', function(){
				if (/\D/g.test(this.value)){
			        // Filter non-digits from input value.
			        this.value = this.value.replace(/\D/g, '');
				}
				if($(this).val().length == 5){
					delete searchParams['govLoc_byState[]'];
					delete searchParams['region'];
					searchParams['byZipcode'] = $(this).val();
					searchParams['byZipCodeDistance'] = $('#radius-select option:selected').val();
					getDetailedBody(searchParams);
				}
			})

			$('#radius-select select').change(function(){
				searchParams['byZipCodeDistance'] = $('#radius-select option:selected').val();
				getDetailedBody(searchParams);
			});


			//government spending tab
			$('#r-spending-type').on('click', '> .btn', function(){
				var option = $(this).attr('data-val');
				searchParams['criteria'] = option;
				getDetailedBody(searchParams);
			});

			$('#r-spending').on('keyup','input', function(){
				//get commas out initially...
				this.value = this.value.replace(/,/g,'')
				if (/\D/g.test(this.value)){
			        // Filter non-digits from input value.
			        this.value = this.value.replace(/\D/g, '');
				}
				else{
					if(this.value.length > 3){
						var position = -3;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if(this.value.length > 7){
						var position = -7;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if(this.value.length > 11){
						var position = -11;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if(this.value.length > 15){
						var position = -15;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if($('#less-than-spending').length > 0 && $('#more-than-spending').length > 0){
						searchParams['expenseBudgetLessThan'] = parseInt(parseInt($('#less-than-spending').val().replace(/,/g,'')) / 1000);
						searchParams['expenseBudgetMoreThan'] = parseInt(parseInt($('#more-than-spending').val().replace(/,/g,'')) / 1000);
						//replace with empty string if less than one to avoid the NaN string sending out.
						if(isNaN(searchParams['expenseBudgetLessThan']) || parseInt(searchParams['expenseBudgetLessThan']) < 1){
							searchParams['expenseBudgetLessThan'] = 0;
						}

						if(isNaN(searchParams['expenseBudgetMoreThan']) || parseInt(searchParams['expenseBudgetMoreThan']) < 1){
							searchParams['expenseBudgetMoreThan'] = 0;
						}
						getDetailedBody(searchParams);
					}
					else{
						delete searchParams['expenseBudgetLessThan'];
						delete searchParams['expenseBudgetMoreThan'];
					};
					
				}

			});

			$('#quartile').on('change','input', function(){
				//add or remove if already exists
				var n = quartile.indexOf($(this).val().toString());
				if(n == -1){
					quartile.push($(this).val());
				}
				else{
					quartile.splice(n,1);
				}
				if(searchParams['criteria'] == 'expenseBudgetQuartile'){
					delete searchParams['perCapitalBudgetQuartileSel'];
					searchParams['expenseBudgetQuartileSel[]'] = quartile;
				}
				else if(searchParams['criteria'] == 'perCapitalBudgetQuartile'){
					delete searchParams['expenseBudgetQuartileSel'];
					searchParams['perCapitalBudgetQuartileSel[]'] = quartile;
				}
				getDetailedBody(searchParams);
			});

			$('#following-category').on('change', 'select', function(){
				delete searchParams['finPS[]'];
				delete searchParams['finPW[]'];
				delete searchParams['finH[]'];
				delete searchParams['finU[]'];
				delete searchParams['finT[]'];
				delete searchParams['finL[]'];
				delete searchParams['finF[]'];
				delete searchParams['finM[]'];
				delete searchParams['expType[]'];

				if($(this).attr('id') == 'categories'){
					$('#following-category').find('.expandSelect').hide();
					var expandedSelect = $(this).find(':selected').attr('data-val');
					var jq_expandedSelect = $('#'+expandedSelect).show();
					searchParams[expandedSelect+'[]'] = jq_expandedSelect.find('select :selected').val();
					getDetailedBody(searchParams);
				}
				else{
					//parameters are stored in the html ids here
					searchParams[$(this).parent('div').attr('id')+'[]'] = $(this).find(':selected').val();
					getDetailedBody(searchParams);
				}
			});

		//handle changes on budget year end

		//handle click all or unclick all
		$('#selectall1').on('change', function(){
			if(this.checked){
				$('#endBudgetYear input').each(function(){
					if(this.checked != true){
						this.checked = true;
					}
				});
			}
			else{
				$('#endBudgetYear input').each(function(){
					if(this.checked == true){
						this.checked = false;
					}
				});
			}

			searchParams['fiscalYearEnd_Month[]'] = new Array();
			$('#endBudgetYear input').each(function(){
				if($(this).prop('checked')){
					searchParams['fiscalYearEnd_Month[]'].push($(this).val());
				}
			});
			getDetailedBody(searchParams);
		});

		//handle each input's check
		$('#months').change(function(){
			searchParams['fiscalYearEnd_Month[]'] = new Array();
			$('#months option:selected').each(function(){
				searchParams['fiscalYearEnd_Month[]'].push($(this).val());
			});
			
			getDetailedBody(searchParams);
		});

		$('#manipulateMonths').on('click',function(e){
			e.stopPropagation();
			searchParams['fiscalYearEnd_Month[]'] = new Array();
			var select = $('#months');
			if($(this).attr('data-all') == "1")
			{
				select.selectpicker('selectAll');
				$(this).text('Clear All');
				$(this).attr('data-all',"0");
			}
			else{
				$(this).text('Select All');
				$(this).attr('data-all',"1");
				select.selectpicker('deselectAll');
			}

			$('#months option:selected').each(function(){
				searchParams['fiscalYearEnd_Month[]'].push($(this).val());
			});
			getDetailedBody(searchParams);
		});

	});
	














	var checkAllIfEmpty = function(roles){
		//roles is a jquery object
		if(roles.filter(':checked').length === 0){
			roles.prop('checked', true);
			roles.each(function(){
				if($(this).prop('checked')){
					searchParams[$(this).val()] = $(this).attr('data-val');
				}
			});
		}
	}

	var spaceToPlus = function(str){
		var str = str.split(' ').join('+');
		return str;
	}

	var stateParams = function(regionArray){
		for(var i in regionArray){
			if(i == 0){
				var statelist = states[regionArray[i]];
			}else{
				//loop through predefined state list and add each state in the region to the search parameters
				statelist += '%2C' + states[regionArray[i]];
			}
		}
		return statelist;
	}

	var updateHeader = function(data){
		var data = JSON.parse(data);
		$('#total-officials-fill-in').text(addCommas(data['Total_Gov_Officials']));
		$('#total-gov').text(addCommas(data['Total_Governments']));
		$('#total-email').text(addCommas(data['Total_Available_Emails']));
		$('#total-phone').text(addCommas(data['Total_Phone_Number']));
		$('#total-mail').text(addCommas(data['Total_Mailing_Street_Box']));
	}


	//this function sends a parameter string to an ajax  which returns results from a mysql procedure
	var getDetailedBody = function(params){
		console.log(params);
		//throw new Error("Trace function");
		var tabs = $('#btn-chooseroles, #btn-location, #btn-spending');
		$('#loadingOverlay, #loadingOverlayTwo').show();
		$('#checkout-final').removeClass('ready').html('Please Wait...');
		delete searchParams['byText'];		

		var params = $.param(params,true);

		//convert series into form acceptable by database
		params = params.replace(/%2B/g, '+');
		params = params.replace(/%252C|%2C/g,'&govLoc_byState%5B%5D=');

		//have to split the parameters apart and sort it alpabetically ebcause that's the way the database procedure needs the values
		var paramArray = params.split("&");
		paramArray.sort();
		params = paramArray.join('&');

		var zip = params.match(/&byZipcode=[0-9]*/i);
		var zipDist = params.match(/&?byZipCodeDistance=[0-9]*/i);
		if(zip){
			params = params.replace(zip, '');
			params = params.replace(zipDist, '');
			params += zip+'&';
			params += zipDist;
		}

		//make sure that bytext is always true by manually adding it 
		params = 'byText=&'+params;
		console.log(params);
		globalshare = params;
		if(window.ajx){
			window.ajx.abort();
		}
		window.ajx = $.ajax({
			dataType: 'jsonp',
			url: 'search-summary.php',
			data: {
				params: params
			}
		}).done(function(data){

			$('#loadingOverlay, #loadingOverlayTwo').hide();
			$('#checkout-final').addClass('ready').html('DOWNLOAD SEARCH RESULTS <span class="bump-up">»</span>');
			//remove ajax loader from tabs
			tabs.removeClass('spinning');

			//do a cool glow thing on the header number
			$('#total-officials-fill-in').addClass('searchGlow');
			setTimeout(function(){
				$('#total-officials-fill-in').removeClass('searchGlow');
			}, 1000)
			
			
			if(data){header_data = JSON.parse(data[0]['@JSON_SUMMARY'])};
			if(header_data){
				//update link to include get var that ensures checkout data is the same as checkout page
				$('#submitsearch').attr('href','checkout.php?scrvr='+data[0]['@scrvr']);
				$('#smallcheckout').attr('href','checkout.php?scrvr='+data[0]['@scrvr']);

				$('#total-officials-fill-in').text(addCommas(header_data['Num_Matched_Officials']));
				$('#total-gov').text(addCommas(header_data['Num_Matched_Governments']));
				$('#total-email').text(addCommas(header_data['Num_Matched_Emails']));
				$('#total-phone').text(addCommas(header_data['Phone_Numbers']));
				$('#total-mail').text(addCommas(header_data['Mailing_Addresses']));
				updateBody(data[0]['@JSON_OFFICIALS']);				
				updatePrice(header_data);
			}
			else{
				$('#total-officials-fill-in').text('0');
				$('#total-gov').text(0);
				$('#total-email').text(0);
				$('#total-phone').text(0);
				$('#total-mail').text(0);
			}
		});

	}


	//updates all the ajax-able areas inside the content area
	var updateBody = function(data){
		var data = JSON.parse(data);

		//update roles
		$('#r-roles').find('input').each(function(i){
			if($(this).attr('data-val')){
				if(data[$(this).attr('data-val')] === undefined){
					$(this).siblings('.query-fill').text('0');
				}else{
					$(this).siblings('.query-fill').text(addCommas(data[$(this).attr('data-val')]));
				}
			}else{
				$(this).siblings('.query-fill').text('N/A');
			}	
		});

		//update type of government
		$('#r-government-type').find('input').each(function(i){
			if($(this).attr('data-val')){
				if(data[$(this).attr('data-val')] === undefined){
					$(this).siblings('.query-fill').text('0');
				}
				else{$(this).siblings('.query-fill').text(addCommas(data[$(this).attr('data-val')]));}
			}else{
				$(this).siblings('.query-fill').text('N/A');
			}	
		});

		//update region numbers =============================================
		if($('#r-rgn').hasClass('active')){
			// if northeast is checked, sum up the states in it and get total
			if($('#northeast').prop('checked')){
				var amount = 0;
				for(var i in Northeast){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[Northeast[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#northeast').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#northeast').siblings('.query-fill').text('0')}

			if($('#south').prop('checked')){
				var amount = 0;
				for(var i in South){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[South[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#south').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#south').siblings('.query-fill').text('0')}

			if($('#midwest').prop('checked')){
				var amount = 0;
				for(var i in Midwest){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[Midwest[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#midwest').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#midwest').siblings('.query-fill').text('0')}

			if($('#west').prop('checked')){
				var amount = 0;
				for(var i in West){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[West[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#west').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#west').siblings('.query-fill').text('0')}
		}

		//update state numbers ==============================================================
		$('#statelist label').each(function(){
			var amount = data[ statesAbbrev[spaceToPlus($(this).text())] ];
			if(amount !== undefined){
				$(this).siblings('.query-fill').text( addCommas( amount ) );
			}
			else{
				$(this).siblings('.query-fill').text('0');
			}
		});

		//update month numbers
		$('#endBudgetYear').find('.inner .text').each(function(){
			var amount = data['Fiscal_' + $(this).justtext()];
			if(amount !== undefined){
				$(this).find('.muted').text(addCommas(amount));
			}
			else{
				$(this).find('.muted').text('0');
			}
		});
		
	}



	var updatePrice = function(data){
		var quantity = data['Num_Matched_Officials'];

		if(creditsRemaining > 0){
			//get the amont of credits that must be paid for beyond the subscription (used to calculate best value for buyer)
			var adjustedQuantity = quantity - creditsRemaining;
			//if the user has enough credits to pay for the records, charge nothing and log the credits to be deducted.
			if(adjustedQuantity <= 0){
				var deduct = Math.abs(quantity);
				var adjustedQuantity = 0;
			}
		}else{
			adjustedQuantity = quantity;
		}

		$('#quantityRecords').text(addCommas(quantity));

		//calculate best value based on number of records. Each record holds the pricing break it is still the best value.
		var level = 0;
		for(var i = 0; i < bestPricing.length; i++){
			//assign user's current download costs per record if they already have a subscription
			if(subscriptionType == bestPricing[i].Sub_Level){
				var currentPrice = bestPricing[i].cost;
			}

			if(adjustedQuantity >= bestPricing[i].pricingBreak){
				var level = i;
				//assign the db subscription number so we can compare upgrade levels
				var upgrade = bestPricing[i].Sub_Level;
			}
		}

		select = bestPricing[level];

		var bundledRecords = 0;
		if(upgrade > subscriptionType){
			//if we are upgrading, assign the number of bundled records
			bundledRecords = parseInt(select.included);
		}else{
			select = bestPricing[level];
		}

		//if the user currently has a subscription, only show subscription if upgrade. If not currently a user, show the included subscription
		if(upgrade > subscriptionType){
			//this is only to show an available offer
			$('#purchaseOrUpgrade').html('UPGRADED PLAN');
			$('#subscriptionType').html(select['Sub_Name']);
			$('#includedCredits').html(addCommas(select.included));
		}
		else{
			$('#purchaseOrUpgrade').html('Included in Purchase');
			$('#subscriptionType').html('&nbsp;<span>N/A</span>');
			$('#includedCredits').html('&nbsp;<span>N/A</span>');
		}

		//update number of user's existed used:
		if(creditsRemaining >= quantity){
			$('#usedRecords').text(addCommas(quantity));
		}else{
			$('#usedRecords').text(addCommas(creditsRemaining));
		}


		if(adjustedQuantity > bundledRecords){
			var payForCreditsSeparately = adjustedQuantity - bundledRecords
		}
		else{
			var payForCreditsSeparately = 0;
		}

		var totalPrice = (payForCreditsSeparately * parseFloat(select.cost)) + parseFloat(select.price);

		// show as free if included in subscription
		if(costPerRecord <= 0){
			costPerRecord = 0;
			totalPrice = 0;
		}

		$('#joinpowerplus > img').attr('src',select.img);
		$('#costPerRecord').text('$'+parseFloat(select.cost).toFixed(2));
		$('#totalPricing').text('$'+addCommas(totalPrice.toFixed(2)));
		$('#pricingTop').text('$'+addCommas(totalPrice.toFixed(2)));

		//calculate the remaining credits a user would have if he upgraded 
		if((bundledRecords + creditsRemaining) >= quantity){
			var leftover = (bundledRecords + parseInt(creditsRemaining)) - quantity;
		}else{
			var leftover = 0;
		}

		//var leftover = (bundledRecords + creditsRemaining) >= quantity ? (bundledRecords + creditsRemaining) - quantity : 0;
		$('#remainingCredits').html(addCommas(leftover));

		//hack to hide credits for unlimited upgrade or current
		if(upgrade == "10" || subscriptionType == "10"){
			$('#includedCredits').html('&nbsp;<span>Unlimited</span>');
			$('#remainingCredits').html('&nbsp;<span>N/A</span>');
		}
	};


	//add commas to thousand markers
	function addCommas(nStr) {
	    nStr += '';
	    var x = nStr.split('.');
	    var x1 = x[0];
	    var x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + ',' + '$2');
	    }
	    return x1 + x2;
	};

	//will get the value of one query string
	function getParameter(paramName) {
	  var searchString = window.location.search.substring(1),
	      i, val, params = searchString.split("&");

	  for (i=0;i<params.length;i++) {
	    val = params[i].split("=");
	    if (val[0] == paramName) {
	      return unescape(val[1]);
	    }
	  }
	  return null;
	};

	jQuery.fn.justtext = function(){
	    return $(this).clone().children().remove().end().text();
	};

	function getIndexByValue(arrayName, value){
		  for(var key in arrayName){
		    if(arrayName[key] == value){
		      return key;
		    }
		  }
		  return null;
		};

	//note that the state count is actually 51 (or 50 for 0 based) because DC is included
	var Northeast = Array(6,19,21,29,39,45,30,32,38);
	var South = Array(0,7,8,9,10,20,33,40,46,48,17,24,42,3,18,36,43);
	var Midwest = Array(14,13,22,34,35,49,15,16,23,25,27,41);
	var West = Array(2,5,12,31,26,44,28,50,1,4,11,37,47);

	var states = Array(
		"Alabama", 
		"Alaska", 
		"Arizona", 
		"Arkansas", 
		"California", 
		"Colorado", 
		"Connecticut", 
		"Delaware", 
		"District Of Columbia",
		"Florida", 
		"Georgia", 
		"Hawaii", 
		"Idaho", 
		"Illinois", 
		"Indiana", 
		"Iowa", 
		"Kansas", 
		"Kentucky", 
		"Louisiana", 
		"Maine", 
		"Maryland", 
		"Massachusetts", 
		"Michigan", 
		"Minnesota", 
		"Mississippi", 
		"Missouri", 
		"Montana",
		"Nebraska",
		"Nevada",
		"New Hampshire",
		"New Jersey",
		"New Mexico",
		"New York",
		"North Carolina",
		"North Dakota",
		"Ohio", 
		"Oklahoma", 
		"Oregon", 
		"Pennsylvania", 
		"Rhode Island", 
		"South Carolina", 
		"South Dakota",
		"Tennessee", 
		"Texas", 
		"Utah", 
		"Vermont", 
		"Virginia", 
		"Washington", 
		"West Virginia", 
		"Wisconsin", 
		"Wyoming"
	);

	var statesAbbrev = {
		Alabama: 'AL',
		Alaska: 'AK', 
		Arizona: 'AZ', 
		Arkansas: 'AR', 
		California: 'CA', 
		Colorado: 'CO', 
		Connecticut: 'CT', 
		Delaware: 'DE', 
		'District+Of+Columbia': 'DC', 
		Florida: 'FL', 
		Georgia: 'GA', 
		Hawaii: 'HI', 
		Idaho: 'ID', 
		Illinois: 'IL', 
		Indiana: 'IN', 
		Iowa: 'IA', 
		Kansas: 'KS', 
		Kentucky: 'KY', 
		Louisiana: 'LA', 
		Maine: 'ME', 
		Maryland: 'MD', 
		Massachusetts: 'MA', 
		Michigan: 'MI', 
		Minnesota: 'MN', 
		Mississippi: 'MS', 
		Missouri: 'MO', 
		Montana: 'MT',
		Nebraska: 'NE',
		Nevada: 'NV',
		'New+Hampshire': 'NH',
		'New+Jersey': 'NJ',
		'New+Mexico': 'NM',
		'New+York': 'NY',
		'North+Carolina': 'NC',
		'North+Dakota': 'ND',
		Ohio: 'OH', 
		Oklahoma: 'OK', 
		Oregon: 'OR', 
		Pennsylvania: 'PA', 
		'Rhode+Island': 'RI', 
		'South+Carolina': 'SC', 
		'South+Dakota': 'SD',
		Tennessee: 'TN', 
		Texas: 'TX', 
		Utah: 'UT', 
		Vermont: 'VT', 
		Virginia: 'VA', 
		Washington: 'WA', 
		'West+Virginia': 'WV', 
		Wisconsin: 'WI', 
		Wyoming: 'WY'
	};
	

