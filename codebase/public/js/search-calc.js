/* add every support to script */	
	if (!Array.prototype.every) {
	  Array.prototype.every = function(fun /*, thisp */) {
	    'use strict';
	    var t, len, i, thisp;

	    if (this == null) {
	      throw new TypeError();
	    }

	    t = Object(this);
	    len = t.length >>> 0;
	    if (typeof fun !== 'function') {
	        throw new TypeError();
	    }

	    thisp = arguments[1];
	    for (i = 0; i < len; i++) {
	      if (i in t && !fun.call(thisp, t[i], i, t)) {
	        return false;
	      }
	    }

	    return true;
	  };
	}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


    var filterGovt = false;
    var filterEmail = false;
    var quartile = Array();
    var globalshare = '';
    var globalscrvr = '';
    var searchParams = {
		byText: '',
		populationMoreThan: 0,
		populationThan: 100000000,
		criteria: 'expenseBudget',
		'off_role': 'govtOff_role',
		expType: '1'
	}

	var stateString = '';

    // Check if zip is valid, if it's not make sure we let them know
    function validateZipCode(zipCode) {
		// Cancel any already running zip code checks (if the server is slow, or searches happen in quick succession)
        if (window.ajxZipCode) {
            window.ajxZipCode.abort();
        }

        window.ajxZipCode = $.ajax({
            url: '/json/is-zip-valid.php',
			dataType: 'jsonp',
			data: {
            	'zip': zipCode
			}
        }).done(function(data) {
    		if (data.result == "UNKNOWN_ZIPCODE") {
                $('#zipWarning').html('We couldn\'t find this zip code in our database, please try again. <br />If you\'re sure it\'s correct please contact our support team').show();
			} else if(data.result == "BAD_ZIPCODE") {
    			$('#zipWarning').html('This doesn\'t seem to be a valid zip code').hide();
			} else {
                $('#zipWarning').hide();
            }
		});
	}

	function resetAndZeroSearch() {
        if (window.ajx) {
            window.ajx.abort();
        }

        $('#mobileLoadingOverlay, #loadingOverlay, #loadingOverlayTwo').hide();
        //remove ajax loader from tabs
        var tabs = $('#main').find('.tab');
        tabs.removeClass('spinning');
        $('.total-officials-fill-in').text('0');
        $('#total-gov').text(0);
        $('#total-email').text(0);
        $('#total-phone').text(0);
        $('#total-mail').text(0);
        $('#quantityRecords').text('0');
        $('#quantityUnpaid').text('0');
        $('#totalPricing').text('0');
        $('#pricingTop').text('0');
	}

    function resetZipCode() {
    	$('#searchzip').val('');
		$('#zipWarning').hide();
        delete searchParams['byZipCode'];
        delete searchParams['byZipCodeDistance'];
        getDetailedBody(searchParams);
	}

	function setSearchingByTitle(title) {
    	$('#r-location h3').html('Searching by: ' + title);
	}

	function getCheckedRegionCount() {
        return $('.region-checkbox:checked').length;
	}

	function getCheckedStateCount() {
        return $('#statelist input:checked').length;
	}

	function checkAllStates() {
        $('#selectall0').prop('checked',true);
        $('#statelist input').each(function() {
            this.checked = true;
            stateString += 'govLoc_byState%5B%5D='+ spaceToPlus($(this).siblings('label').text()) + '&';
        });
        checkAllIfEmpty($('#search-region').find('input')); // Check all regions
    }

	$(document).ready(function() {

		// integer key/value matchups for the slider
		var popRangeArray = {
			0: 0,
			10: 1000,
			20: 2500,
			30: 5000,
			40: 10000,
			50: 25000,
			60: 50000,
			70: 100000,
			80: 500000,
			90: 100000000
		}

        //Redo Search on Submit
        $("#smallcheckout").on('click',function(e){
            e.preventDefault();
            var finalUrl = $(this).attr("href");
            //Submit Most Recent Params
            var resendParams = globalshare;
            $("#processingResultsModal").appendTo("body").modal();
            //On Final Submit, Redirect to new page
		    window.ajx = $.ajax({
                dataType: 'jsonp',
                url: 'search-summary.php',
                data: {
                    params: resendParams
                    }
            }).done(function(data){
                window.location = finalUrl;
            });
        });

		//expand and show divs
		$('.expand-btn').on('click',function(){
			$(this).parents('.tab').find('.expand').hide();
			$($(this).attr('data-expand')).show();
		})

		//tabs for faq page
		if($('.tabbed').length>0){
			var tabs = $('#main').find('.tab');
			$(tabs).hide();
			if(window.location.hash){
				var re1 = window.location.hash.match('[^\\?]*')[0];
				$(re1).show();
				$('.top-tabs').find('[href='+re1+']').addClass('selected-tab');
			}
			else{
				$(tabs[0]).show();
				$($('.top-tabs').find('.tablink')[0]).addClass('selected-tab');
			}
			
			$(tabs).find('.answer').hide();


		//open the first option for each expanding set.
		$('.expand').each(function(){
			$(this).parents('section').find('.expand').first().show();
		});

			//handle click between tabs
			$('.top-tabs').find('.tablink').on('click',function(e){
				e.preventDefault();
				$('.top-tabs').find('.tablink').removeClass('selected-tab');
				$(this).addClass('selected-tab');
				var tabId = $(this).attr('href');
				window.location.hash = tabId;
				$(tabs).hide();
				$(tabId).show();
			});



			//handle open / close answers
			$('.tabbed.faq').on('click','li:not(.answer)', function(){
				var answer = $(this).next('.answer');
				if(answer.is(':hidden')){
					answer.fadeIn(350);
					$(this).addClass('hide-up');
				}
				else{
					answer.fadeOut(350);
					$(this).removeClass('hide-up');
				}
			})
		}

		//jquery for the search page specific (not tabs)
		if($('.searchgov').length>0) {
            var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            var mobile = (viewportWidth < 600);

            var populationRangeValuesDesktop = {
                'min': 0,
                11: 1000,
		22: 2500,
                33: 5000,
                44: 10000,
                55: 25000,
                66: 50000,
                77: 100000,
                88: 500000,
                'max': 10000000
            };

            var populationRangeValuesMobile = {
                'min': 0,
                20: 2500,
                40: 10000,
                60: 25000,
                80: 50000,
                'max': 10000000
            };

            var populationRangeValues = (mobile) ? populationRangeValuesMobile : populationRangeValuesDesktop;

            function filterPips(value, type) {
                if (Object.values(populationRangeValues).indexOf(value) === -1) {
                    return -1;
                }

                // if (mobile) {
                //     return 2;
                // }

                return 1;
            }

			//initialize jquery plugin for range slider (not dependent on jquery ui)
            var slider = noUiSlider.create(document.getElementById('rangeslider'), {
				connect: true,
				orientation: 'horizontal',
				handles: 2,
				range: populationRangeValues,
				pips: {
					mode: 'count',
                    values: Object.size(populationRangeValues),
                    density: Object.size(populationRangeValues),
					stepped: true,
					filter: filterPips,
                                        format: {
                                                to: function (value) {
							if (value == 10000000 && mobile) {
								return '10 mil';
							}
                                                        return value.toLocaleString(undefined, {minimumFractionDigits: 0});
                                                }
                                        }
				},
				snap: true,
				start: [0, 10000000],
			});

            document.getElementById('rangeslider').noUiSlider.on('update',
				function(values, handle, unencoded, tap, positions) {
					searchParams['populationMoreThan'] = Math.floor(values[0]);
					searchParams['populationThan'] = Math.floor(values[1]);
					var plusSymbol = (searchParams['populationThan'] == 1000000) ? '+' : '';
					$('#populationRangeResult').html(
						searchParams['populationMoreThan'].toLocaleString(undefined, {minimumFractionDigits: 0}) + ' - ' + searchParams['populationThan'].toLocaleString(undefined, {minimumFractionDigits: 0})
						+ plusSymbol
					);
					getDetailedBody(searchParams);
				}
			);

			//handle next buttons
			$('.step-holder').find('.next').on('click', function(){
				var tabId = $(this).attr('href');
				window.location.hash = tabId;
				$(tabs).hide();
				$(tabId).show();
				$('.top-tabs').find('.tablink').removeClass('selected-tab'); 
				$('.top-tabs').find('[href='+window.location.hash+']').addClass('selected-tab');
			});

			//handle final button, only go if search is completed
			$('#checkout-final').on('click', function(e){
				if($(this).hasClass('ready')){
				}
				else{
					e.preventDefault();i
				}
			});

			//tie preview button
			$('#previewResults').colorbox({
				iframe: true,
				width: '70%',
				height: '80%'
			});

			//handle open and close sections on pricing bar (we disabled this because we removed sharing and left filters open)
			$('#shareResults').hide();
			
			/*$('#priceBar .grayinset').on('click', function(){
				$(this).next('.blackinset').toggle();
			});*/ 

			//handle toggle switch filters
			$('#onePerGov > .switch').on('switch-change', function (e, data) {
			    if(data.value){
			    	searchParams['top_elected'] = 1
			    }
			    else{
			    	delete searchParams['top_elected'];
			    }
			    getDetailedBody(searchParams);
			});

			$('#includeEmails > .switch').on('switch-change', function (e, data) {
			    if(data.value){
			    	searchParams['off_email'] = 'govtOff_email';
			    }
			    else{
			    	delete searchParams['off_email'];
			    }
			    getDetailedBody(searchParams);
			});

			//handle sharing stuff
			$('#firstEmail').one('focus', function(){
				$('#moreEmail').show();
			})
			var numberEmails = 1;
			$('#moreEmail').on('click', function(){
				if(numberEmails < 3){
					$('#firstEmail').before('<input type="text" id="email'+numberEmails+'" placeholder="Email" type="email">');
					numberEmails++;
					if(numberEmails == 3){
						$('#moreEmail').hide();
					}
				}
			});

			//start tooltips			
			$("#priceBar").find('.tooltipLink').tooltip();
			$("#r-roles").find('.circle-tip').tooltip();

		}
















		//REVERSE SET EVERYTHING TO LOAD A SAVED SEARCH FOR EXAMPLE, PULL IT FROM A GET URL STRING AND POSSIBLY A SESSION VARIABLE
			var queryString = {};
			var queryCount = 0;
			var quartile = [];
			var resetSearch = false;

			//restore saved search from link
			if(getParameter('restoreSaved') == 'true'){
				lastSearch = window.location.search;
			}

			if (getParameter('resetSearch') == 'true') {
				lastSearch = '';
			}

			if(lastSearch.length > 1){
				lastSearch = lastSearch.replace(/%25/g,'%');

				//create an object out of the search parameters
				lastSearch.replace(
				    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
				    function($0, $1, $2, $3) {
				    	queryCount++;
				    	if(queryCount > 1){
					    	if(queryString[$1]){
					    		if($.isArray(queryString[$1])){
					    			queryString[$1].push($3);
					    		}
					    		else{
					    			var tempstate = queryString[$1];
					    			queryString[$1] = new Array();
					    			queryString[$1][0] = tempstate;
					    			queryString[$1][1] = $3;
					    		}
					    	}else{
					    		queryString[$1] = $3;
					    	}
					    }
				    }
				);

				//roles
				var noRoles = true;
				if(queryString['role_1'] == 'Top+Elected+Official'){
					$('#mayor').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_3'] == 'Governing+Board+Member'){
					$('#alderman').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_8'] == 'Head+of+Fire+Protection+Services'){
					$('#fire').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_4'] == 'Head+of+Finance%2FBudgeting'){
					$('#finance').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_2'] == 'Top+Appointed+Official'){
					$('#manager').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_6'] == 'Head+of+Public+Works'){
					$('#publicworks').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_7'] == 'Head+of+Law+Enforcement'){
					$('#police').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_5'] == 'Head+of+Purchasing%2FProcurement'){
					$('#purchasing').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_10'] == 'Head+of+IT'){
					$('#infotech').prop('checked', true);
					noRoles = false;
				}
				if(queryString['role_9'] == 'Clerk'){
					$('#clerk').prop('checked', true);
					noRoles = false;
				}

				//check all roles if none are checked
				if(noRoles){
					$('#r-roles input').each(function(){
						$(this).prop('checked', true);
					});
				}

				//saved searches use integers for population checkboxes, so we need to support that
				if(queryString['population%5B%5D']){
					if( Object.prototype.toString.call(queryString['population%5B%5D']) === '[object Array]' ){
						var popBoxes = queryString['population%5B%5D'];
						queryString['populationThan'] = popRangeArray[(Math.max.apply(Math, popBoxes) * 10)];
						queryString['populationMoreThan'] = popRangeArray[(Math.min.apply(Math, popBoxes) * 10)];
					}
					else{
						//our parameters changed a little bit, so we have to make sure things line up.
						var popBoxes = parseInt(queryString['population%5B%5D']);
						if(popBoxes == 9){
							queryString['populationMoreThan'] = popRangeArray[(8 * 10)];
							queryString['populationThan'] = popRangeArray[(9 * 10)];
						}else{
							queryString['populationMoreThan'] = popRangeArray[(popBoxes * 10)];
							queryString['populationThan'] = popRangeArray[((popBoxes+1) * 10)];
						}
					}
					delete queryString['population%5B%5D'];
				}
				//population range
				if(queryString['populationMoreThan'] || queryString['populationThan']) {
					if (queryString['populationMoreThan'] < 100) {
                        var positions = Array(getIndexByValue(popRangeArray, queryString['populationMoreThan']), getIndexByValue(popRangeArray, queryString['populationThan']));
                    } else {
                        var positions = Array(queryString['populationMoreThan'], queryString['populationThan']);
					}
					$('#rangeslider').val(positions);
                    document.getElementById('rangeslider').noUiSlider.set(positions);
                } else {
					var positions = Array(0, 1000000);
                    document.getElementById('rangeslider').noUiSlider.set(positions);
				}

				//govt type
				var govtypeChecked = false;
				if(queryString['govType_municipalities'] == 'municipality'){
					$('#cities').prop('checked', true);
					govtypeChecked = true;
				}
				if(queryString['govType_counties'] == 'county'){
					$('#counties').prop('checked', true);
					govtypeChecked = true;
				}
				if(queryString['govType_townships'] == 'township'){
					$('#towns').prop('checked', true);
					govtypeChecked = true;
				}

				//add all three to search object if none are sent by default
				if(govtypeChecked == false){
					$('#cities').prop('checked',true);
					$('#counties').prop('checked',true);
					$('#towns').prop('checked',true);
					queryString['govType_municipalities'] = 'municipality';
					queryString['govType_townships'] = 'township';
					queryString['govType_counties'] = 'county';
				}


				checkedStates = queryString['govLoc_byState%5B%5D'];
				var searchingByState = false;
				var searchingByRegion = false;
				//location
				// We are searching by zip code
				if (queryString['byZipcode']) {
					$('#search-zip').addClass('usingLocation');
					$('#search-zip').find('input').val(queryString['byZipcode']);
					$('#r-location').find('button[data-expand="#search-zip"]').addClass('active');
                    validateZipCode(queryString['byZipcode']);
                    setSearchingByTitle('zip code ' + queryString['byZipcode']);
				} else if(queryString['region']) { // We are searching by region (North West, North East, etc)
					$('#search-region').addClass('usingLocation');
					var regionArray = queryString['region'].split("_");
					for(region in regionArray){
						$('#'+regionArray[region]).prop('checked',true);
					}

                    resetZipCode();
                    searchingByRegion = true;
				} else { // We are searching by a list of states (Alabama, Colorado, Oregon)
					$('#search-state').addClass('usingLocation').removeClass('cutoff');
                    resetZipCode();
                    searchingByState = true;
				}

				if (typeof checkedStates === "string" && checkedStates.length > 0) {
					var modState = checkedStates.replace(/\+/g, '').toLowerCase();
					var modState = modState.replace(/%2b/g, '');
					$('#'+modState).prop('checked', true);
				} else if (checkedStates && typeof checkedStates === 'object' && checkedStates.constructor === Array) {
					for(i in checkedStates) {
						var modState = checkedStates[i].replace(/\+/g, '').toLowerCase();
						var modState = modState.replace(/%2b/g, '');
						$('#'+modState).prop('checked', true);
					}

					//check appropriate region boxes
					if(checkedStates){
						if(regionSelected(Midwest,checkedStates)){
							$('#midwest').prop('checked', true);
						}
						if(regionSelected(Northeast,checkedStates)){
							$('#northeast').prop('checked', true);
						}
						if(regionSelected(West,checkedStates)){
							$('#west').prop('checked', true);
						}
						if(regionSelected(South,checkedStates)){
							$('#south').prop('checked', true);
						}
					}
				} else { // No checked states, we should check them all
					checkAllStates();
				}

				if (searchingByState) {
                    setSearchingByTitle(getCheckedStateCount().toString() + ' states');
				} else if(searchingByRegion) {
                    setSearchingByTitle(getCheckedStateCount().toString() + ' states in ' + getCheckedRegionCount() + ' regions');
				}


				//spending
				if(queryString['criteria'] == 'expenseBudget'){
					searchParams['criteria'] = 'expenseBudget';
                    $('#r-spending').find('button').removeClass('active');
					$('#r-spending').find('button[data-val="expenseBudget"]').addClass('active');
					$('#annual-range').show();
					var budgetMoreThan = isNaN(queryString['expenseBudgetMoreThan']) ? 0 : queryString['expenseBudgetMoreThan'];
					var budgetLessThan = isNaN(queryString['expenseBudgetLessThan']) ? 0 : queryString['expenseBudgetLessThan'];
					$('#more-than-spending').val(addCommas(budgetMoreThan * 1000));
					$('#less-than-spending').val(addCommas(budgetLessThan * 1000));
				}
				else if(queryString['criteria'] == 'expenseBudgetQuartile'){
					searchParams['criteria'] = 'expenseBudgetQuartile';
					$('#r-spending').find('button').removeClass('active');
					$('#annual-range').hide();
					$('#r-spending').find('button[data-val="expenseBudgetQuartile"]').addClass('active');
					$('#quartile').show();
					var i = 0;
					for(i in queryString['expenseBudgetQuartileSel%5B%5D']){
						$('#quartile').find('input[value='+queryString['expenseBudgetQuartileSel%5B%5D'][i]+']').prop('checked', true);
						quartile.push(queryString['expenseBudgetQuartileSel%5B%5D'][i]);
					}
				}
				else if(queryString['criteria'] == 'perCapitalBudgetQuartile'){
					searchParams['criteria'] = 'perCapitalBudgetQuartile';
					$('#r-spending').find('button').removeClass('active');
					$('#annual-range').hide();
					$('#r-spending').find('button[data-val="perCapitalBudgetQuartile"]').addClass('active');
					$('#quartile').show();
					var i = 0;
					for(i in queryString['perCapitalBudgetQuartileSel%5B%5D']){
						$('#quartile').find('input[value='+queryString['perCapitalBudgetQuartileSel%5B%5D'][i]+']').prop('checked', true);
						quartile.push(queryString['expenseBudgetQuartileSel%5B%5D'][i]);
					}
				}
				
				// expense types
				//alert(queryString['finH%5B%5D']);
				//alert(JSON.stringify(queryString));
				//searchParams['finU[]'];
				//alert(JSON.stringify(searchParams));

				//assign financial categories to the right value, show dropdown beneath
				var catTypes =[
					{catName: 'expType', cvalue: 'Special Categories'},
					{catName: 'finPS', cvalue: 'Public Safety'},
					{catName: 'finPW', cvalue: 'Public Welfare'},
					{catName: 'finH', cvalue: 'Health'},
					{catName: 'finU', cvalue: 'Utilities'},
					{catName: 'finT', cvalue: 'Transportation'},
					{catName: 'finF', cvalue: 'Finance'},
					{catName: 'finL', cvalue: 'Leisure'},
					{catName: 'finM', cvalue: 'Miscellaneous'}
				];

				//loop through each financial type to see if it exists
				for(i in catTypes){
					if(queryString[catTypes[i].catName + '%5B%5D'] && queryString[catTypes[i].catName + '%5B%5D'] != "1"){
						$('#categories').val(catTypes[i].cvalue);

						//show dropdown below for subcategories
						$('#'+catTypes[i].catName).show();

						//assign subvalue if exists
						if(queryString[catTypes[i].catName + '%5B%5D'] != "All"){
							$( '#'+catTypes[i].catName ).find('select').val( decodeURIComponent( queryString[ catTypes[i].catName+'%5B%5D' ].replace(/\+/g, ' ') ) );
						}
					}
				}
				
				//filters
				if(queryString['top_elected']){
					$('#onePerGov > .switch').bootstrapSwitch('toggleState');
				}

				if(queryString['off_email']){
					$('#includeEmails > .switch').bootstrapSwitch('toggleState');
				}
				searchParams = queryString;
				if(typeof(searchParams['byText']) !== "undefined"){}else{searchParams['byText'] = ''}
				for(key in searchParams){
					var oldVal = searchParams[key];
					var newKey = unescape(key);
					delete searchParams[key];
					searchParams[newKey] = unescape(oldVal).replace(/\+/g,' ');
				}

				//budget end

				if(queryString['fiscalYearEnd_Month[]']){
					var n = queryString['fiscalYearEnd_Month[]'].split(',');
					searchParams['fiscalYearEnd_Month[]'] = new Array();
					for(i in n){
						searchParams['fiscalYearEnd_Month[]'].push(n[i]);
						$('#endBudgetYear').find('option[value='+n[i]+']').attr('selected','selected');
					}
					if(n.length == 12 || n.length == 0){
						$('#manipulateMonths').text('Clear All');
						$('#manipulateMonths').attr('data-all',"0");
						$('#months').selectpicker('selectAll');
					}
				}else{
					$('#months').selectpicker('selectAll');
				}

				//refresh styled selects
				$('.selectpicker').selectpicker('refresh');

				//make sure expenseBudgetQuartileSel[] is an array, otherwise it returns comma sep values which messes up the results
				if(queryString['expenseBudgetQuartileSel[]']){
					searchParams['expenseBudgetQuartileSel[]'] = queryString['expenseBudgetQuartileSel[]'].split(",");
				}

				if(typeof(searchParams['criteria']) == 'undefined'){
					searchParams['criteria'] = 'expenseBudget';
				}

				if(typeof(searchParams['expType']) == 'undefined'){
					searchParams['expType'] = '1';
				}

				delete searchParams['restoreSaved'];
				delete searchParams['selLocation'];
				delete searchParams['govLoc_byRegion[]'];
				getDetailedBody(searchParams);
			}else{
				//if we're not restoring a search, set default values
				checkAllIfEmpty($('#r-roles').find('input'));
				checkAllIfEmpty($('#r-government-type').find('input'));
				checkAllIfEmpty($('#search-region').find('input'));

				//select all fiscal years visually (they are selected in the background already);
				$('#months').selectpicker('selectAll');
				$('#manipulateMonths').text('Clear All');
				$('#manipulateMonths').attr('data-all',"0");

				checkAllStates();
				searchParams['byText'] = '';

				//set default search to be expensebudget and set no max and min
				searchParams['criteria'] = 'expenseBudget';
				searchParams['expType'] = 1;
				searchParams.expenseBudgetLessThan = '';
				searchParams.expenseBudgetMoreThan = '';
				//set population range slider
				positions = Array(0,90)
				$('#rangeslider').val(positions);
				getDetailedBody(searchParams);

                setSearchingByTitle(getCheckedStateCount().toString() + ' states');
                $('#search-state').addClass('usingLocation').removeClass('cutoff');
                $('#search-region, #search-zip').removeClass('usingLocation');
            }










		// THIS PAGE SENDS A URL STRING TO THE SERVER AND THEN USES AJAX TO PULL THE DATA BACK - FOR DETAILS SEE THE STORED SQL PROCEDURE 'SEARCHSUMMARY2' IN THE DATABASE
		//bind data refresh to applicable elements ===============================================================
		//choose roles tab
			$('#r-roles').on('change', 'input', function(){
				$('#r-roles').find('input').each(function(){
					if(this.checked){
						searchParams[$(this).val()] = $(this).attr('data-val');
					}
					else{
						delete searchParams[$(this).val()];
					}
				});
				
				checkAllIfEmpty($('#r-roles').find('input'));

				getDetailedBody(searchParams);
			});

			$('#r-government-type').on('change', 'input', function(){
				$('#r-government-type').find('input').each(function(){
				if(this.checked){searchParams[$(this).val()] = $(this).attr('data-val')}
					else{delete searchParams[$(this).val()]};
				});

				checkAllIfEmpty($('#r-government-type input'));
				getDetailedBody(searchParams);
			});

			//select all handle for the states
			$('#selectall0').on('change', function(){
				resetZipCode();
				$('#search-state').addClass('usingLocation').removeClass('cutoff');
				$('#search-region, #search-zip').removeClass('usingLocation');
				if(this.checked){
					$('#statelist input').each(function(){
						if(this.checked != true){
							this.checked = true;
						}
					});

					//check the region boxes
					$('#midwest').prop('checked', true);
					$('#northeast').prop('checked', true);
					$('#west').prop('checked', true);
					$('#south').prop('checked', true);
				}	
				else{
					$('#statelist input').each(function(){
						if(this.checked == true){
							this.checked = false;
						}
					});
					
                    //clear the region checkboxes
                    $('#search-region').find('input').prop('checked',false);
				}

				var stateCount = 0;
				$('#statelist input').each(function(){
					if($(this).prop('checked')){
						if(stateCount == 0){
							searchParams['govLoc_byState[]'] = spaceToPlus($(this).siblings('label').text());
							stateCount = 1;
						}
						else{
							searchParams['govLoc_byState[]'] += '%2C' + spaceToPlus($(this).siblings('label').text());
						}
					}
				});

                setSearchingByTitle(getCheckedStateCount().toString() + ' states');
				getDetailedBody(searchParams);
			});

			$('#toggle-states').on('click', function(e) {
				e.preventDefault();
				e.stopPropagation();
				$('#search-state').toggleClass('cutoff');
			});

			//just trigger state box to drop down
			$('#search-state').on('click', function(){
				$('#search-state').removeClass('cutoff');
			});

			//this is one of the confusing parts, sending a statelist. it helps to see what the stored procedure actually accepts.
			$('#r-location').on('change','#statelist input', function() {
				var selectedStates = new Array();
				//remove highlights from other options
				$('#search-state').addClass('usingLocation').removeClass('cutoff');
				$('#search-region, #search-zip').removeClass('usingLocation');
				resetZipCode();

				//reset affected parameters
				if(searchParams['byZipcode']){delete searchParams['byZipcode']};
				if(searchParams['region']){delete searchParams['region']};
				delete searchParams['govLoc_byState%5B%5D'];
				delete searchParams['govLoc_byState[]'];

				//clear the region checkboxes
				$('#search-region').find('input').prop('checked',false);

				var states = $('#statelist input');
				var checkAllStates = false;
				if(states.filter(':checked').length === 0){
					checkAllStates = true;
				}

				var stateCount = 0;
				
				states.each(function(){
					if(checkAllStates == true){
						this.checked = true;
					}

					if($(this).prop('checked')){
						var stateText = $(this).siblings('label').text();
						selectedStates[selectedStates.length] = stateText;
						if(stateCount == 0){
							searchParams['govLoc_byState[]'] = spaceToPlus(stateText);
							stateCount = 1;
						}
						else{
							searchParams['govLoc_byState[]'] += '%2C' + spaceToPlus(stateText);
						}
					}
					
				});

				//check region boxes if the amount of states match up
				if(regionSelected(Midwest,selectedStates)){
					$('#midwest').prop('checked', true);
				}
				if(regionSelected(Northeast,selectedStates)){
					$('#northeast').prop('checked', true);
				}
				if(regionSelected(West,selectedStates)){
					$('#west').prop('checked', true);
				}
				if(regionSelected(South,selectedStates)){
					$('#south').prop('checked', true);
				}

                setSearchingByTitle(getCheckedStateCount().toString() + ' states');

				getDetailedBody(searchParams);
			});

			//region checkboxes - apply the checks to the matching states
			$('#r-location').on('change','#search-region input', function(){
				//update visual appearance to show we're using that section
				$('#search-region').addClass('usingLocation');
				$('#search-state').removeClass('usingLocation').addClass('cutoff');
				$('#search-zip').removeClass('usingLocation');
				searchParams['region'] = '';
				searchParams['govLoc_byState[]'] = '';
				if(searchParams['byZipcode']){delete searchParams['byZipcode']};
				resetZipCode();
				$('#statelist').find('input').prop('checked',false);

				// if no regions are checked, don't trigger a refresh search
				regionChecked = false;
				if($('#midwest').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(Midwest);
					checkStatesByRegion(Midwest);
					searchParams['region'] += 'midwest_';
					regionChecked = true;
				}
				if($('#northeast').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(Northeast);
					checkStatesByRegion(Northeast);
					searchParams['region'] += 'northeast_';
					regionChecked = true;
				}
				if($('#south').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(South);
					checkStatesByRegion(South);
					searchParams['region'] += 'south_';
					regionChecked = true;
				}
				if($('#west').prop('checked')){
					searchParams['govLoc_byState[]'] += (searchParams['govLoc_byState[]'].length > 2 ? '%2C':'') + stateParams(West);
					checkStatesByRegion(West);
					searchParams['region'] += 'west_';
					regionChecked = true;
				}

                setSearchingByTitle(getCheckedStateCount().toString() + ' states in ' + getCheckedRegionCount() + ' regions');

				if(regionChecked){
					getDetailedBody(searchParams);
				}
			});

			//code for the search by zip functionality
			$('#r-location').on('keyup','#search-zip input', function(){
				if (/\D/g.test(this.value)){
			        // Filter non-digits from input value.
			        this.value = this.value.replace(/\D/g, '');
				}
                this.value = this.value.substr(0, 5);

				// If the zip code is valid
				if($(this).val().length == 5) {
					//update visual highlight to show we're using the zip
					$('#search-zip').addClass('usingLocation');
					$('#search-state').removeClass('usingLocation').addClass('cutoff');
					$('#search-region').removeClass('usingLocation');

					//clear the region checkboxes
                    $('#search-region').find('input').prop('checked',false);
                    $('#statelist').find('input').prop('checked',false);
                    $('#selectall0').prop('checked', false);

                    validateZipCode($(this).val());
                    setSearchingByTitle('zip code ' + $(this).val());

					delete searchParams['govLoc_byState[]'];
					delete searchParams['region'];
                    searchParams['byZipCodeDistance'] = $('#radius-select option:selected').val();
                    searchParams['byZipcode'] = $(this).val();
					getDetailedBody(searchParams);
				}
			})

			$('#search-zip input, #search-zip select').on('focus', function(){
				if($('#search-zip input').val().length == 5){
					//update visual highlight to show we're using the zip
					$('#search-zip').addClass('usingLocation');
					$('#search-state').removeClass('usingLocation').addClass('cutoff');
					$('#search-region').removeClass('usingLocation');

                    validateZipCode($('#search-zip input').val());
                    setSearchingByTitle('zip code ' + $('#search-zip input').val());

					//rerun search if we're not already on zip
					if(!searchParams['byZipcode']) {
						delete searchParams['govLoc_byState[]'];
						delete searchParams['region'];
						searchParams['byZipcode'] = $('#search-zip input').val();
						searchParams['byZipCodeDistance'] = $('#radius-select option:selected').val();
						getDetailedBody(searchParams);
					}
				}
			})

			$('#radius-select select').change(function(){
				searchParams['byZipCodeDistance'] = $('#radius-select option:selected').val();
				getDetailedBody(searchParams);
			});


			//government spending tab
			$('#r-spending-type').on('click', '> .btn', function(){
				var option = $(this).attr('data-val');
				searchParams['criteria'] = option;
                $('#r-spending').find('button').removeClass('active');
                $('#r-spending').find('button[data-val="' + option + '"]').addClass('active');
				getDetailedBody(searchParams);
			});

			$('#r-spending').on('keyup','input', function(){
				//get commas out initially...
				this.value = this.value.replace(/,/g,'')
				if (/\D/g.test(this.value)){
			        // Filter non-digits from input value.
			        this.value = this.value.replace(/\D/g, '');
				}
				else{
					//this.value.replace(/,/g,'');
					if(this.value.length > 3){
						var position = -3;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if(this.value.length > 7){
						var position = -7;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if(this.value.length > 11){
						var position = -11;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if(this.value.length > 15){
						var position = -15;
						this.value = this.value.slice(0,position)  + ','+ this.value.slice(position);
					}
					if($('#less-than-spending').length > 0 && $('#more-than-spending').length > 0){
						//check to make sure more than is less than less than haha
						var lessThan = parseInt($('#less-than-spending').val().replace(/,/g,''));
						var moreThan = parseInt($('#more-than-spending').val().replace(/,/g,''));

						if(moreThan >= lessThan && lessThan != 0){
							$('#amount-error-msg').text("The 'Less than' field must be greater than the 'More than' field. We return results between these values.");
						}else{
							$('#amount-error-msg').text('');
							searchParams['expenseBudgetLessThan'] = lessThan / 1000;
							searchParams['expenseBudgetMoreThan'] = moreThan / 1000;
							//replace with empty string if less than one to avoid the NaN string sending out.
							if(isNaN(searchParams['expenseBudgetLessThan']) || searchParams['expenseBudgetLessThan'] == 0){
								searchParams['expenseBudgetLessThan'] = 100000000000;
							}

							if(isNaN(searchParams['expenseBudgetMoreThan'])){
								searchParams['expenseBudgetMoreThan'] = 0;
							}

							getDetailedBody(searchParams);
						}
					}
					else{
						delete searchParams['expenseBudgetLessThan'];
						delete searchParams['expenseBudgetMoreThan'];
					};
					
				}

			});

			$('#quartile').on('change','input', function(){
				//add or remove if already exists
				var n = quartile.indexOf($(this).val().toString());
				if(n == -1){
					quartile.push($(this).val());
				}
				else{
					quartile.splice(n,1);
				}
				if(searchParams['criteria'] == 'expenseBudgetQuartile'){
					delete searchParams['perCapitalBudgetQuartileSel'];
					searchParams['expenseBudgetQuartileSel[]'] = quartile;
				}
				else if(searchParams['criteria'] == 'perCapitalBudgetQuartile'){
					delete searchParams['expenseBudgetQuartileSel'];
					searchParams['perCapitalBudgetQuartileSel[]'] = quartile;
				}
				getDetailedBody(searchParams);
			});

			$('#following-category').on('change', 'select', function(){
				delete searchParams['finPS'];
				delete searchParams['finPW'];
				delete searchParams['finH'];
				delete searchParams['finU'];
				delete searchParams['finT'];
				delete searchParams['finL'];
				delete searchParams['finF'];
				delete searchParams['finM'];
				delete searchParams['expType'];

				//okay - exptype = 1 only for total expenditures. exptype > 1 is the other special categories.
				//for all the others, remove extype and add the parameter of fin[X] for the type, which is stored in the data-val. then store the selected value in the category. 
				if($(this).attr('id') == 'categories'){
					$('#following-category').find('.expandSelect').hide();
					var expandedSelect = $(this).find(':selected').attr('data-val');
					if($(this).val() != "expType"){
						//everything but the 'Total Expenditure'
						var jq_expandedSelect = $('#'+expandedSelect).show();
						searchParams[expandedSelect] = jq_expandedSelect.find('select :selected').val();
					}else{
						//total expenditure
						searchParams[expandedSelect] = 1;
					}

					if(expandedSelect != 'expType'){
						//searchParams['expType'] = 1;
					}
					getDetailedBody(searchParams);
				}
				else{
					//parameters are stored in the html ids here
					searchParams[$(this).parent('div').attr('id')] = $(this).find(':selected').val();
					if($(this).parent('div').attr('id') != 'expType'){
						//searchParams['expType'] = 1;
					}
					getDetailedBody(searchParams);
				}
			});

		//handle changes on budget year end

		//handle click all or unclick all
		$('#selectall1').on('change', function(){
			if(this.checked){
				$('#endBudgetYear input').each(function(){
					if(this.checked != true){
						this.checked = true;
					}
				});
			}
			else{
				$('#endBudgetYear input').each(function(){
					if(this.checked == true){
						this.checked = false;
					}
				});
			}

			searchParams['fiscalYearEnd_Month[]'] = new Array();
			$('#endBudgetYear input').each(function(){
				if($(this).prop('checked')){
					searchParams['fiscalYearEnd_Month[]'].push($(this).val());
				}
			});
			getDetailedBody(searchParams);
		});

		//handle each input's check
		$('#months').change(function(){
			searchParams['fiscalYearEnd_Month[]'] = new Array();
			$('#months option:selected').each(function(){
				searchParams['fiscalYearEnd_Month[]'].push($(this).val());
			});
			
			getDetailedBody(searchParams);
		});

		$('#manipulateMonths').on('click',function(e){
			e.stopPropagation();
			searchParams['fiscalYearEnd_Month[]'] = new Array();
			var select = $('#months');
			if($(this).attr('data-all') == "1")
			{
				select.selectpicker('selectAll');
				$(this).text('Clear All');
				$(this).attr('data-all',"0");
			}
			else{
				$(this).text('Select All');
				$(this).attr('data-all',"1");
				select.selectpicker('deselectAll');
			}

			$('#months option:selected').each(function(){
				searchParams['fiscalYearEnd_Month[]'].push($(this).val());
			});
			getDetailedBody(searchParams);
		});

	});
	


	var checkAllIfEmpty = function(roles){
		//roles is a jquery object
		if(roles.filter(':checked').length === 0){
			roles.prop('checked', true);
			roles.each(function(){
				if($(this).prop('checked')){
					searchParams[$(this).val()] = $(this).attr('data-val');
				}
			});
		}
	}

	var spaceToPlus = function(str){
		var str = str.split(' ').join('+');
		return str;
	}

	var stateParams = function(regionArray){
		for(var i in regionArray){
			if(i == 0){
				var statelist = states[regionArray[i]];
			}else{
				//loop through predefined state list and add each state in the region to the search parameters
				statelist += '%2C' + states[regionArray[i]];
			}
		}
		return statelist;
	}

	var updateHeader = function(data) {
		if (data != null && data.hasOwnProperty('Total_Gov_Officials')) {
            var data = JSON.parse(data);

            $('.total-officials-fill-in').text(addCommas(data['Total_Gov_Officials']));
            $('#total-gov').text(addCommas(data['Total_Governments']));
            $('#total-email').text(addCommas(data['Total_Available_Emails']));
            $('#total-phone').text(addCommas(data['Total_Phone_Number']));
            $('#total-mail').text(addCommas(data['Total_Mailing_Street_Box']));
		}
	}


	//this function sends a parameter string to an ajax  which returns results from a mysql procedure
	var getDetailedBody = function(params) {
		//throw new Error("Trace function");
		var tabs = $('#btn-chooseroles, #btn-location, #btn-spending');
		$('#mobileLoadingOverlay, #loadingOverlay, #loadingOverlayTwo').show();
		$('#checkout-final').removeClass('ready').html('Please Wait...');
		delete searchParams['byText'];

		var params = $.param(params,true);

		//convert series into form acceptable by database
		params = params.replace(/%2B/g, '+');
		params = params.replace(/%252C|%2C/g,'&govLoc_byState%5B%5D=');

		//have to split the parameters apart and sort it alpabetically ebcause that's the way the database procedure needs the values
		var paramArray = params.split("&");
		paramArray.sort();
		params = paramArray.join('&');

		var zip = params.match(/&byZipcode=[0-9]*/i);
		var zipDist = params.match(/&?byZipCodeDistance=[0-9]*/i);
		if(zip){
			params = params.replace(zip, '');
			params = params.replace(zipDist, '');
			params += zip+'&';
			params += zipDist;
		}

		//make sure that bytext is always true by manually adding it 
		params = 'byText=&'+params;
		globalshare = params;
		if(window.ajx){
			window.ajx.abort();
		}
		window.ajx = $.ajax({
			dataType: 'jsonp',
			url: 'search-summary.php',
			data: {
				params: params
			}
		}).done(function(data){

			$('#mobileLoadingOverlay, #loadingOverlay, #loadingOverlayTwo').hide();
			$('#checkout-final').addClass('ready').html('DOWNLOAD SEARCH RESULTS <span class="bump-up">»</span>');
			//remove ajax loader from tabs
			tabs.removeClass('spinning');

			//do a cool glow thing on the header number
			$('.total-officials-fill-in').addClass('searchGlow');
			setTimeout(function(){
				$('.total-officials-fill-in').removeClass('searchGlow');
			}, 1000)
			
			if(data){
				header_data = JSON.parse(data[0]['@JSON_SUMMARY'])
				paid = data[0]['paid_records'];
				searchid = data[0]['@scrvr'];
			}
			if(header_data){
				//update link to include get var that ensures checkout data is the same as checkout page
				$('#submitsearch').attr('href','checkout.php?scrvr='+searchid);
				$('#smallcheckout').attr('href','checkout.php?scrvr='+searchid);

				$('.total-officials-fill-in').text(addCommas(header_data['Num_Matched_Officials']));
				$('#total-gov').text(addCommas(header_data['Num_Matched_Governments']));
				$('#total-email').text(addCommas(header_data['Num_Matched_Emails']));
				$('#total-phone').text(addCommas(header_data['Phone_Numbers']));
				$('#total-mail').text(addCommas(header_data['Mailing_Addresses']));
				updateBody(data[0]['@JSON_OFFICIALS']);				
				updatePrice(header_data, paid);
			}
			else{
				$('.total-officials-fill-in').text('0');
				$('#total-gov').text(0);
				$('#total-email').text(0);
				$('#total-phone').text(0);
				$('#total-mail').text(0);
			}
		});

	}


	//updates all the ajax-able areas inside the content area
	var updateBody = function(data){
		var data = JSON.parse(data);

		//update roles
		$('#r-roles').find('input').each(function(i){
			if($(this).attr('data-val')){
				if(data[$(this).attr('data-val')] === undefined){
					$(this).siblings('.query-fill').text('0');
				}else{
					$(this).siblings('.query-fill').text(addCommas(data[$(this).attr('data-val')]));
				}
			}else{
				$(this).siblings('.query-fill').text('N/A');
			}	
		});

		//update type of government
		$('#r-government-type').find('input').each(function(i){
			if($(this).attr('data-val')){
				if(data[$(this).attr('data-val')] === undefined){
					$(this).siblings('.query-fill').text('0');
				}
				else{$(this).siblings('.query-fill').text(addCommas(data[$(this).attr('data-val')]));}
			}else{
				$(this).siblings('.query-fill').text('N/A');
			}	
		});

		//update region numbers =============================================
		if($('#r-rgn').hasClass('active')){
			// if northeast is checked, sum up the states in it and get total
			if($('#northeast').prop('checked')){
				var amount = 0;
				for(var i in Northeast){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[Northeast[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#northeast').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#northeast').siblings('.query-fill').text('0')}

			if($('#south').prop('checked')){
				var amount = 0;
				for(var i in South){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[South[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#south').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#south').siblings('.query-fill').text('0')}

			if($('#midwest').prop('checked')){
				var amount = 0;
				for(var i in Midwest){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[Midwest[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#midwest').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#midwest').siblings('.query-fill').text('0')}

			if($('#west').prop('checked')){
				var amount = 0;
				for(var i in West){
					var stateExist = data[ statesAbbrev[spaceToPlus(states[West[i]])] ];
					if(stateExist !== undefined){
						amount += parseInt(stateExist);
					}
				}
				$('#west').siblings('.query-fill').text(addCommas(amount));
			}
			else{$('#west').siblings('.query-fill').text('0')}
		}

		//update state numbers ==============================================================
		$('#statelist label').each(function(){
			var amount = data[ statesAbbrev[spaceToPlus($(this).text())] ];
			if(amount !== undefined){
				$(this).siblings('.query-fill').text( addCommas( amount ) );
			}
			else{
				$(this).siblings('.query-fill').text('0');
			}
		});

		//update month numbers
		$('#endBudgetYear').find('.inner .text').each(function(){
			var amount = data['Fiscal_' + $(this).justtext()];
			if(amount !== undefined){
				$(this).find('.muted').text(addCommas(amount));
			}
			else{
				$(this).find('.muted').text('0');
			}
		});
	}



	var updatePrice = function(data, paid){
		paid = typeof paid !== 'undefined' ? paid : 0;
		var quantity = data['Num_Matched_Officials'];

		if(creditsRemaining > 0){
			//get the amont of credits that must be paid for beyond the subscription (used to calculate best value for buyer)
			var adjustedQuantity = quantity - creditsRemaining;
			//if the user has enough credits to pay for the records, charge nothing and log the credits to be deducted.
			if(adjustedQuantity <= 0){
				var deduct = Math.abs(quantity);
				var adjustedQuantity = 0;
			}
		}else{
			adjustedQuantity = quantity;
		}

		$('#quantityRecords').text(addCommas(quantity));

		var unpurchasedQuantity = quantity - paid;
		adjustedQuantity = adjustedQuantity - paid;
		$('#quantityUnpaid').text(addCommas(unpurchasedQuantity));

		//calculate best value based on number of records. Each record holds the pricing break it is still the best value.
		var level = 0;
		for(var i = 0; i < bestPricing.length; i++){
			//assign user's current download costs per record if they already have a subscription
			if(subscriptionType == bestPricing[i].Sub_Level){
				var currentPrice = bestPricing[i].cost;
			}

			if(adjustedQuantity >= bestPricing[i].pricingBreak && bestPricing[i].Purchasable){
				var level = i;
				//assign the db subscription number so we can compare upgrade levels
				var upgrade = bestPricing[i].Sub_Level;
			}
		}

		select = bestPricing[level];

		var bundledRecords = 0;
		if(upgrade > subscriptionType){
			//if we are upgrading, assign the number of bundled records
			bundledRecords = parseInt(select.included);
		}else{
			select = bestPricing[level];
		}

		//if the user currently has a subscription, only show subscription if upgrade. If not currently a user, show the included subscription
		if(upgrade > subscriptionType){
			//this is only to show an available offer
			$('#purchaseOrUpgrade').html('UPGRADED PLAN');
			$('#subscriptionType').html(select['Sub_Name']);
			$('#includedCredits').html(addCommas(select.included));
		}
		else{
			$('#purchaseOrUpgrade').html('Included in Purchase');
			$('#subscriptionType').html('&nbsp;<span>N/A</span>');
			$('#includedCredits').html('&nbsp;<span>N/A</span>');
		}

		//update number of user's existed used:
		if(creditsRemaining >= unpurchasedQuantity){
			$('#usedRecords').text(addCommas(unpurchasedQuantity));
		}else{
			$('#usedRecords').text(addCommas(creditsRemaining));

			//show alert for subuser extra credits
			if(subuser_credits_left){
				console.log('test');
				$('#subusercredits').show();
			}
			else{
				$('#subusercredits').hide();
			}
		}


		if(adjustedQuantity > bundledRecords){
			var payForCreditsSeparately = adjustedQuantity - bundledRecords;
		}
		else{
			var payForCreditsSeparately = 0;
		}

		var totalPrice = (payForCreditsSeparately * parseFloat(select.cost)) + parseFloat(select.price);

		// show as free if included in subscription
		if(costPerRecord <= 0){
			costPerRecord = 0;
			totalPrice = 0;
		}

		$('#joinpowerplus > img').attr('src',select.img);
		$('#costPerRecord').text('$'+parseFloat(select.cost).toFixed(2));
		$('#totalPricing').text('$'+addCommas(totalPrice.toFixed(2)));
		$('#pricingTop').text('$'+addCommas(totalPrice.toFixed(2)));

		//calculate the remaining credits a user would have if he upgraded 
		if((bundledRecords + creditsRemaining) >= unpurchasedQuantity){
			var leftover = (bundledRecords + parseInt(creditsRemaining)) - unpurchasedQuantity;
		}else{
			var leftover = 0;
		}

		//var leftover = (bundledRecords + creditsRemaining) >= unpurchasedQuantity ? (bundledRecords + creditsRemaining) - unpurchasedQuantity : 0;
		$('#remainingCredits').html(addCommas(leftover));

		//hack to hide credits for unlimited upgrade or current
		if(upgrade == "10" || subscriptionType == "10"){
			$('#includedCredits').html('&nbsp;<span>Unlimited</span>');
			$('#remainingCredits').html('&nbsp;<span>N/A</span>');
		}
	};


	//add commas to thousand markers
	function addCommas(nStr) {
	    nStr += '';
	    var x = nStr.split('.');
	    var x1 = x[0];
	    var x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + ',' + '$2');
	    }
	    return x1 + x2;
	};

	//will get the value of one query string
	function getParameter(paramName) {
	  var searchString = window.location.search.substring(1),
	      i, val, params = searchString.split("&");

	  for (i=0;i<params.length;i++) {
	    val = params[i].split("=");
	    if (val[0] == paramName) {
	      return unescape(val[1]);
	    }
	  }
	  return null;
	};

	jQuery.fn.justtext = function(){
	    return $(this).clone().children().remove().end().text();
	};

	function getIndexByValue(arrayName, value){
		  for(var key in arrayName){
		    if(arrayName[key] == value){
		      return key;
		    }
		  }
		  return null;
		};


	function regionSelected(region, arrayToCheck){
		var result = true;

		function isInArray(element, index, array){
			var state = states[element];
			state = state.replace(/\+/g, '');
			state = state.replace(/%2b/g, '');
			if(arrayToCheck.indexOf(state) == -1){
				result = false;
				return false;
			}
			else{
				return true;
			}
		}
		
		region.every(isInArray);
		return result;
	}

	//note that the state count is actually 51 (or 50 for 0 based) because DC is included
	var Northeast = Array(6,19,21,29,39,45,30,32,38);
	var South = Array(0,7,8,9,10,20,33,40,46,48,17,24,42,3,18,36,43);
	var Midwest = Array(14,13,22,34,35,49,15,16,23,25,27,41);
	var West = Array(2,5,12,31,26,44,28,50,1,4,11,37,47);

	function checkStatesByRegion(region){
		var statelist = $('#statelist');
		for(i = 0; i < region.length; i++){
			var stateNum = region[i];
			var state = states[stateNum].toLowerCase();
			state = state.replace(/\s/g, "");
			//rhode island doesn't match the rest i think...
			//state = (state == "rhodeisland") ? 'rhode' : state;
			statelist.find('#' + state).prop('checked',true);
		}
	}

	var states = Array(
		"Alabama", 
		"Alaska", 
		"Arizona", 
		"Arkansas", 
		"California", 
		"Colorado", 
		"Connecticut", 
		"Delaware", 
		"District Of Columbia",
		"Florida", 
		"Georgia", 
		"Hawaii", 
		"Idaho", 
		"Illinois", 
		"Indiana", 
		"Iowa", 
		"Kansas", 
		"Kentucky", 
		"Louisiana", 
		"Maine", 
		"Maryland", 
		"Massachusetts", 
		"Michigan", 
		"Minnesota", 
		"Mississippi", 
		"Missouri", 
		"Montana",
		"Nebraska",
		"Nevada",
		"New Hampshire",
		"New Jersey",
		"New Mexico",
		"New York",
		"North Carolina",
		"North Dakota",
		"Ohio", 
		"Oklahoma", 
		"Oregon", 
		"Pennsylvania", 
		"Rhode Island", 
		"South Carolina", 
		"South Dakota",
		"Tennessee", 
		"Texas", 
		"Utah", 
		"Vermont", 
		"Virginia", 
		"Washington", 
		"West Virginia", 
		"Wisconsin", 
		"Wyoming"
	);

	var statesAbbrev = {
		Alabama: 'AL',
		Alaska: 'AK', 
		Arizona: 'AZ', 
		Arkansas: 'AR', 
		California: 'CA', 
		Colorado: 'CO', 
		Connecticut: 'CT', 
		Delaware: 'DE', 
		'District+Of+Columbia': 'DC', 
		Florida: 'FL', 
		Georgia: 'GA', 
		Hawaii: 'HI', 
		Idaho: 'ID', 
		Illinois: 'IL', 
		Indiana: 'IN', 
		Iowa: 'IA', 
		Kansas: 'KS', 
		Kentucky: 'KY', 
		Louisiana: 'LA', 
		Maine: 'ME', 
		Maryland: 'MD', 
		Massachusetts: 'MA', 
		Michigan: 'MI', 
		Minnesota: 'MN', 
		Mississippi: 'MS', 
		Missouri: 'MO', 
		Montana: 'MT',
		Nebraska: 'NE',
		Nevada: 'NV',
		'New+Hampshire': 'NH',
		'New+Jersey': 'NJ',
		'New+Mexico': 'NM',
		'New+York': 'NY',
		'North+Carolina': 'NC',
		'North+Dakota': 'ND',
		Ohio: 'OH', 
		Oklahoma: 'OK', 
		Oregon: 'OR', 
		Pennsylvania: 'PA', 
		'Rhode+Island': 'RI', 
		'South+Carolina': 'SC', 
		'South+Dakota': 'SD',
		Tennessee: 'TN', 
		Texas: 'TX', 
		Utah: 'UT', 
		Vermont: 'VT', 
		Virginia: 'VA', 
		Washington: 'WA', 
		'West+Virginia': 'WV', 
		Wisconsin: 'WI', 
		Wyoming: 'WY'
	};
	

