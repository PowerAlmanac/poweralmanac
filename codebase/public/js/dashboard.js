$(document).ready(function(){
                    $('#shareModal').appendTo($('body'));
                    $('#shareTableModal').appendTo($('body'));
                    $('#passModal').appendTo($('body'));
                    $('#subUserModal').appendTo($('body'));

                    var spinner = "<a class='spinner'><img src='images/ajax-loader.gif'></a>";

                    var deleteAlert = '<div class="alert alert-danger" id="removeuseralert" style="float: left">\
                                        <a class="close" data-dismiss="alert">×</a>\
                                        <strong>Are you sure you want to remove this user?</strong>\
                                        <button class="btn btn-danger" data-dismiss="alert" href="#">Cancel</button>\
                                        <button class="btn" id="confirmdelete" href="#">Confirm</button>\
                                    </div>';

                    var creditAlert = '<div class="alert alert-danger" id="creditalert" style="float: left">\
                        <a class="close" data-dismiss="alert">×</a>\
                        <strong>Update User Credits</strong>\
                        <span style="width: 100%; display: block"><input type="number" min="0" max="" id="newCreditAmount" style="height: 28px"></span>\
                        <button class="btn btn-danger" data-dismiss="alert" href="#">Cancel</button>\
                        <button class="btn" id="confirmcredits" href="#">Update</button>\
                    </div>';

                    var shareString = '';

                    $('#mydownloadstable .download-info').each(function(){
                        $(this).popover({
                            title: 'Download Information',
                            trigger: 'click',
                            container: $(this),
                            placement: 'top',
                            content: '<br><br><br><br>'+spinner+'<br><br><br><br><br>',
                            html: true
                        });
                    })

                    //delete & edit subuser
                    $('#subusers').on('click','a', function(e){

                        //delete subuser
                        if($(this).hasClass('deletesubuser')){
                            e.preventDefault();
                            var deleteEmail = $(this).attr('data-val');
                            var link = $(this);
                            $(this).parents('.subuser').append(deleteAlert);
                            $('#removeuseralert').alert();
                            $('#confirmdelete').one('click', function(){
                                $.ajax({
                                    url: 'ajax/deletesub.php',
                                    type: 'POST',
                                    data: {
                                        e: deleteEmail
                                    },
                                    success: function(){
                                        link.parents('.subuser').remove();
                                        $('#removeuseralert').alert('close');

                                        //add credits back in front end
                                        var removedCredits = parseInt(link.parents('.subuser').find('.availableCredits').text().replace(',',''));
                                        userCredits = userCredits + removedCredits;
                                        $('#availableCredits').text(userCredits);
                                    }
                                })
                            })
                            
                        }

                        //edit subuser credit amount
                        else if($(this).hasClass('editsubuser')){
                            e.preventDefault();
                            var userDiv = $(this).parents('.subuser');
                            var email = $(this).attr('data-val');
                            userDiv.after(creditAlert);
                            var currentAmount = parseInt(userDiv.find('.availableCredits').text().replace(',',''));
                            console.log(currentAmount);
                            $('#creditalert').alert();
                            $('#newCreditAmount').val(currentAmount).attr('max', currentAmount + userCredits);
                            

                            $('#confirmcredits').on('click', function(e){
                                e.preventDefault();
                                var newCredits = $('#newCreditAmount').val();

                                if(newCredits > (currentAmount + userCredits)){
                                    $('#newCreditAmount').after('<p class="hinted">You can only allocate up to ' + (currentAmount + userCredits) + ' credits to this user</p>')
                                }else{
                                    $.ajax({
                                        url: 'ajax/editsub.php',
                                        data: {
                                            eMail: email,
                                            newDLLimits: newCredits.replace(',','')
                                        },
                                        type: 'POST',
                                         statusCode: {
                                            406: function(data){
                                                $('#newCreditAmount').after('<p class="hinted">' + data.responseText + '</p>')
                                            },
                                            200: function(){
                                                $('#creditalert').alert('close');
                                                userDiv.find('.availableCredits').text(newCredits);
                                                userCredits = (userCredits + currentAmount) - newCredits;
                                                $('#availableCredits').text(userCredits);
                                            }
                                        }
                                    })
                                }
                            });
                        }

                        //popup subuser modal
                        else if($(this).hasClass('addSubuser')){
                            e.preventDefault();
                            //restore defaults for second time
                            $('#credits').val(userCredits);
                            $('#subUserError').text('');
                            $('#subPass').val('');
                            $('#subPassConfirm').val('');
                            $('#subUserModal').modal('show');
                        }
                    })

                    //handle create subuser submission
                    $('#subUserForm').on('submit', function(e){
                        e.preventDefault();

                        var userDomain = userEmail.match(/@.*/);
                        var userDomain = userDomain[0].toLowerCase();
                        var subuserDomain = $('#subuserEmail').val().match(/@.*/);
                        var subuserDomain = subuserDomain[0].toLowerCase();

                        //validate passwords matching
                        if(subuserDomain != userDomain){
                            $('#subUserError').text('The subuser must have the same domain name (e.g. mycompany.com) email address as yours');
                        }
                        else if($('#subPass').val() != $('#subPassConfirm').val()){
                            $('#subUserError').text('Your subuser\'s password and confirm password need to be the same');
                        }else if((userCredits < $('#credits').val()) && $User_Subscription != 10 ){
                            $('#subUserError').text('You can\'t assign your user more credits than you have left');
                        }else{
                            $('#savesub').text('Saving User...')
                            $.ajax({
                                url: 'ajax/createsub.php',
                                type: 'POST',
                                data: {
                                    eMail: $('#subuserEmail').val(),
                                    passWord: $('#subPass').val(),
                                    confirmPass: $('#subPassConfirm').val(),
                                    firstName: $('#subFirst').val(),
                                    lastName: $('#subLast').val(),
                                    dlLimits: $('#creditLimit').val()
                                },
                                statusCode: {
                                    406: function(data){
                                        $('#savesub').text('Create User');
                                        $('#subUserError').text(data.responseText);
                                    },
                                    200: function(){
                                            var subuserHtml = "<div class='subuser'><p>" + $('#subFirst').val() + " " + $('#subLast').val() + " (SUB-USER)<a class='editsubuser' href='' data-val='"+$('#subuserEmail').val()+"'><img src='images/edit.png' alt='edit credits' title='edit credit' border='0'></a> <a class='deletesubuser' href='' data-val='"+$('#subuserEmail').val()+"'><img src='images/delete.png' alt='delete' title='delete' border='0'></a></p>";
                                                subuserHtml += "<p>"+$('#subuserEmail').val()+"</p>";
                                                subuserHtml += "<p>Download credits used: <span>0</span></p>";
                                                subuserHtml += "<p>Available download credits: <span class='availableCredits'>"+$('#creditLimit').val()+"</span></p>";
                                                subuserHtml += "</div>";
                                        $('#addsubbefore').before(subuserHtml);
                                        $('#subUserModal').modal('hide');
                                        $('#savesub').text('Create User');

                                        //update remaining credits
                                        userCredits = userCredits - parseInt($('#creditLimit').val());
                                        $('#availableCredits').text(userCredits);
                                    }
                                }
                            })
                        }
                    });


                    //change password modal
                    $('#changePass').on('click', function(e){
                        e.preventDefault();
                        $('#passerror').text('');
                        $('#ajaxpasssave').text('Save').show();
                        $('#passModal').modal('show');
                    });

                    $('#passShareForm').on('submit', function(e){
                        e.preventDefault();
                       
                        if($('#confirmpass').val() != $('#newpass').val()){
                            $('#passerror').text('Your new passwords don\'t match - please check them');
                        }else{
                             $('#ajaxpasssave').text('Saving...');
                            $.ajax({
                                url: 'ajax/changepass.php',
                                type: 'POST',
                                data: {
                                    oldPassWord: $('#oldpass').val(),
                                    newPassWord: $('#newpass').val(),
                                    confirmPassWord: $('#confirmpass').val()
                                },
                                statusCode: {
                                    405: function(){
                                        $('#passerror').text('Your new passwords don\'t match - please check them. Your new password must also be longer than 4 characters');
                                        $('#ajaxpasssave').text('Save');
                                    },
                                    406: function(){
                                        $('#passerror').text('Sorry, your old password didnt match the one we have on file. Could you try again?');
                                        $('#ajaxpasssave').text('Save');
                                    },
                                    200: function(){
                                        $('#passerror').text('Password change complete');
                                        $('#ajaxpasssave').hide();
                                    }
                                }
                            });
                        }
                    });

                    //ajax email subscription changes
                    $('#updateCom').on('click', function(){
                        $(this).text('Saving...');
                        if($('#getalerts').is(":checked")){
                            var alerts = 'true';
                        }
                        if($('#getnews').is(":checked")){
                            var newsletter = 'true';
                        }
                        $.ajax({
                            url: 'ajax/updateemail.php',
                            type: 'POST',
                            data: {
                                email: $(this).siblings('input[name=email]').val(),
                                alerts: alerts,
                                newsletter: newsletter
                            }
                        }).done(function(){
                            $('#updateCom').text('Save Changes');
                        })
                    });

                    
                    //handle search table actions
                    $('#mysearchestable').on('click', 'a', function(e){
                        //my searches ajax delete
                        if($(this).hasClass('delete')){
                            e.preventDefault();
                            var savedId = $(this).attr('data-val');
                            $(this).hide()
                            $(this).after(spinner);
                            var parent = $(this).parent();
                             $.ajax({
                                url: 'ajax/deletesavedsearch.php',
                                type: 'POST',
                                data: {
                                   id: savedId
                                }
                            }).done(function(){
                                parent.parents('tr').remove();

                            });

                            //if edit button pressed in the search table...
                        }else if($(this).hasClass('edit')){
                            e.preventDefault();
                            e.stopPropagation();
                            ajaxEdit($(this),'ajax/renamesearch.php');

                            //if share button pressed in the search table...
                        }else if($(this).hasClass('share')){
                            e.preventDefault();
                            e.stopPropagation();
                            $('#shareModal').modal('show');
                            shareString = $(this).parents('tr').find('.restoreLink').attr('href');
                        }
                    });
                    
                    //ajax shared search out
                    $('#shareForm').on('submit', function(){
                         $('#ajaxshare').text('Sending...');
                         $.ajax({
                            url: 'ajax/sharesearch.php',
                            type: 'POST',
                            data: {
                                sender_Name: $("#shareName").val(),
                                sender_Email: $('#shareEmail').val(),
                                recipient_Email: $('#recipientEmail').val(),
                                message: $('#shareMessage').val() + '\n' + '<a href="http://poweralmanac.com/'+shareString+'">View the Search</a>'
                            }
                         }).done(function(){
                            $('#shareModal').modal('hide');
                            $('#ajaxshare').text('Share');
                         })
                         return false;
                    });

                    //ajax shared table out
                    $('#shareTableForm').on('submit', function(){
                         $('#ajaxtableshare').text('Sending...');
                         $.ajax({
                            url: 'ajax/sharesearch.php',
                            type: 'POST',
                            data: {
                                sender_Name: $("#shareTableName").val(),
                                sender_Email: $('#shareTableEmail').val(),
                                recipient_Email: $('#recipientTableEmail').val(),
                                message: $('#shareTableMessage').val() + '\n' + '<a href="http://poweralmanac.com/'+shareString+'">View the Table</a>'
                            }
                         }).done(function(){
                            $('#shareTableModal').modal('hide');
                            $('#ajaxtableshare').text('Share');
                         })
                         return false;
                    });





                    //my searches ajax delete
                    $('#mysavedtable').on('click', 'a', function(e){
                        if($(this).hasClass('delete')){
                            e.preventDefault();
                            var savedId = $(this).attr('data-val');
                            $(this).hide()
                            $(this).after(spinner);
                            var parent = $(this).parent();
                             $.ajax({
                                url: 'ajax/deletesavedtable.php',
                                type: 'POST',
                                data: {
                                   id: savedId
                                }
                            }).done(function(){
                                parent.parents('tr').remove();
                            });
                        }else if($(this).hasClass('edit')){
                            e.preventDefault();
                            e.stopPropagation();
                            ajaxEdit($(this),'ajax/renametable.php');
                        }else if($(this).hasClass('share')){
                            e.preventDefault();
                            e.stopPropagation();
                            $('#shareTableModal').modal('show');
                            shareString = $(this).parents('tr').find('.restoreLink').attr('href');
                        }
                    });



                    //my downloads delete
                    $('#mydownloadstable').on('click', '.delete', function(e){
                        e.preventDefault();
                        var savedId = $(this).attr('data-val');
                        $(this).hide()
                        $(this).after(spinner);
                        var parent = $(this).parent();
                         $.ajax({
                            url: 'ajax/deletedownload.php',
                            type: 'POST',
                            data: {
                               id: savedId
                            }
                        }).done(function(){
                            parent.parents('tr').remove();

                        })
                    });


                    //if a user has just downloaded something, popup a tip
                    if(getParameterByName('downloadID')){
                        $('#'+getParameterByName('downloadID')).popover({
                            content: 'This is your latest download. Either click on it to begin downloading, or right click (ctrl+click) and choose the "save as" option'
                        });
                        $('#'+getParameterByName('downloadID')).popover('show');
                    }

                    // hide popup
                     $('html').on('click.popover.data-api', function() {
                        $('#'+getParameterByName('downloadID')).popover('hide');
                    });


                    //pull description of a saved download
                    $('#mydownloadstable').on('click', '.download-info', function(e){
                        var _this = $(this);
                        var savedId = $(this).attr('data-val');
                        _this.parents('tr').siblings('tr').find('.download-info').popover('hide');
                       
                       $.ajax({
                            url: '../ajax/downloadinfo.php',
                            type: 'POST',
                            data: {
                                downloadName: savedId + '.zip'
                            },
                            success: function(data){
                                var data = data + "<br><a href='./search-government.php?"+_this.attr('data-search') + "&restoreSaved=true'>Rerun Search</a>";
                                _this.find('.popover-content').html(data);
                            }
                        });
                    });


                });
            
            //function to check a query string variable

           function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            //reusable function for each rename button, allows for a different url each time
            var ajaxEdit = function(edit, url){
                    var newInput = "<span class='newholder' id='newholder'><input id='newInput' type='text' value=''><button id='saveNew'>SAVE</button></span>";
                //hide original link, add an input field to update
                    var restore = edit.parents('tr').find('.restoreLink');
                    restore.hide();
                    restore.after(newInput);
                    $('#newInput').val(restore.text()).focus().select();

                    //grab save click and disable document off-click
                    $('#saveNew').one('click', function(){
                        //unbind all bound events during this function
                        $(document).off('click');
                        edit.off('click');
                        $('#newholder').off('click');

                        $(this).text('SAVING...');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                id: edit.attr('data-val'),
                                new_label: $('#newInput').val()
                            }
                        }).done(function(){
                            restore.text($('#newInput').val());
                            $("#newholder").remove();
                            restore.show();
                        })
                    })

                    //make sure any clicks on save area don't trigger document hide
                    $("#newholder").on('click',function(e) {
                        e.stopPropagation();
                    });

                    //cancel a second click of the edit button
                    edit.on('click', function(e) {
                        return false;
                    });

                    //restore old name click off
                    $(document).on('click', function() {
                        edit.off('click');
                      $("#newholder").hide();
                      restore.show();
                    });
            }
