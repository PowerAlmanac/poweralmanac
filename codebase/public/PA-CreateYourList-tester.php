<?php

//keeping this as a reference to the old search pages' functionality

include("inc/config.php");

// added for browser back problem, first time search, and history - PRG Fix
$newSearch = $_REQUEST['ns']; //history or new search
//echo("DEBUG: newSearch = $newSearch<br>");

if (!isset($_SESSION['firstTimeSearch'])) {
	$firstTimeSearch = 1;
} else {
	$firstTimeSearch = $_SESSION['firstTimeSearch'];
}
//echo("DEBUG: firstTimeSearch = $firstTimeSearch<br>");

if (!isset($_SESSION['searchRequestArrayJSON'])) {
	$searchRequestArray = array();
	$usePASummary = 1; // first time
	//echo("<!-- FIRST TIME -->");
} else {
	//echo("<p>redirected</p>");
	$searchRequestArrayJSON = $_SESSION['searchRequestArrayJSON'];
	if ($searchRequestArrayJSON != '') {
		$searchRequestArray = json_decode($searchRequestArrayJSON, TRUE);
		switch(json_last_error())
		{
			case JSON_ERROR_DEPTH:
				echo ' - Maximum stack depth exceeded';
			break;
			case JSON_ERROR_CTRL_CHAR:
				echo ' - Unexpected control character found';
			break;
			case JSON_ERROR_SYNTAX:
				echo ' - Syntax error, malformed JSON';
			break;
			case JSON_ERROR_NONE:
				//echo ' - No errors';
			break;
		}
		//print_r($searchRequestArray);
		$usePASummary = 0;
	} else {
		$searchRequestArray = array();
		$usePASummary = 1; // first time
	}
}

// start
if (!isset($_SESSION['results_GovtOffMustHaveEmails'])) {
	$_SESSION['results_GovtOffMustHaveEmails'] = '0';
}
if (!isset($_SESSION['results_GovtOffMustMatchRoles'])) {
	$_SESSION['results_GovtOffMustMatchRoles'] = '1';
}

/*
Build the equivalent of $_SERVER['QUERY_STRING'] via GET method from $_REQUEST (works with both POST and GET methods)
*/

if (isset($_SESSION['useSavedSearchParams'])) {
	$useSavedSearchParams = $_SESSION['useSavedSearchParams'];
} else {
	$_SESSION['useSavedSearchParams'] = '0';
	$useSavedSearchParams = '0';
}

//echo("<!-- searchParams = $searchParams -->");
//echo("useSavedSearchParams = $useSavedSearchParams<br>");
if ($useSavedSearchParams == '1') {
	$_SESSION['useSavedSearchParams'] = '0';
	$searchParams = $_SESSION['savedSearchParams'];
	$_SESSION['lastSearchParams'] = $searchParams;
} else {
	//echo("<!--"); print_r($_REQUEST); echo("-->");
	$govtType = '';
	$govLoc_byState = '';
	$role = '';
	$population = '';
	$off_email = '';
	$searchParamsConstructed = '';
	// PRG Fix
	$requestArray = $searchRequestArray;
	//$requestArray = $_REQUEST;
	//print_r($requestArray);
	$location = '';
	$selLocation = 'locationByZipCode';
	$selGovtExp = 'TotalExp';
	$selGovtExpScreen = 'SpendingBuckets';
	//echo("selGovtExpScreen = $selGovtExpScreen<br>");
	foreach ($requestArray as $key => $value) {
		//echo "<br>DEBUG: Key: $key, Value: $value";
		switch($key) {
			// basic search start
			case 'govType_counties':
			case 'govType_townships':
			case 'govType_municipalities':
			case 'role_1':
			case 'role_2':
			case 'role_3':
			case 'role_4':
			case 'role_5':
			case 'role_6':
			case 'role_7':
			case 'role_8':
			case 'role_9':
			case 'off_email':
				$valueURLENCODED = urlencode($value);
				$searchParamsConstructed = $searchParamsConstructed . "$key=$valueURLENCODED&";
				break;
			case 'selLocation':
				$selLocation = urlencode($value);
				$valueURLENCODED = urlencode($value);
				$searchParamsConstructed = $searchParamsConstructed . "$key=$valueURLENCODED&";
				break;
			case 'byZipcode':
			case 'byZipcodeDistance':
				if ($selLocation == 'locationByZipCode') {
					if ($value != "") {
						$valueURLENCODED = urlencode($value);
						$searchParamsConstructed = $searchParamsConstructed . "$key=$valueURLENCODED&";
					}
				}
				break;
			case 'govLoc_byState': // array
				if ($selLocation == 'locationByStates') {
					foreach ($value as $item) {
						if ($item != "") {
						  $itemURLENCODED = urlencode($item);
						  $searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
						}
					}
				}
				break;
			case 'govLoc_byRegion': // array
				if ($selLocation == 'locationByRegions') {
					foreach ($value as $item) {
						if ($item != "") {
							$itemURLENCODED = urlencode($item);
							$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
							// construct the associated states
							switch($item) {
								case 'Northeast':
									$searchParamsConstructed = $searchParamsConstructed . "govLoc_byState%5B%5D=Connecticut&govLoc_byState%5B%5D=Maine&govLoc_byState%5B%5D=Massachusetts&govLoc_byState%5B%5D=New+Hampshire&govLoc_byState%5B%5D=Rhode+Island&govLoc_byState%5B%5D=Vermont&govLoc_byState%5B%5D=New+Jersey&govLoc_byState%5B%5D=New+York&govLoc_byState%5B%5D=Pennsylvania&";
									break;
								case 'South':
									$searchParamsConstructed = $searchParamsConstructed . "govLoc_byState%5B%5D=Delaware&govLoc_byState%5B%5D=District+of+Columbia&govLoc_byState%5B%5D=Florida&govLoc_byState%5B%5D=Georgia&govLoc_byState%5B%5D=Maryland&govLoc_byState%5B%5D=North+Carolina&govLoc_byState%5B%5D=South+Carolina&govLoc_byState%5B%5D=Virginia&govLoc_byState%5B%5D=West+Virginia&govLoc_byState%5B%5D=Alabama&govLoc_byState%5B%5D=Kentucky&govLoc_byState%5B%5D=Mississippi&govLoc_byState%5B%5D=Tennessee&govLoc_byState%5B%5D=Arkansas&govLoc_byState%5B%5D=Louisiana&govLoc_byState%5B%5D=Oklahoma&govLoc_byState%5B%5D=Texas&";
									break;
								case 'Midwest':
									$searchParamsConstructed = $searchParamsConstructed . "govLoc_byState%5B%5D=Indiana&govLoc_byState%5B%5D=Illinois&govLoc_byState%5B%5D=Michigan&govLoc_byState%5B%5D=Ohio&govLoc_byState%5B%5D=Wisconsin&govLoc_byState%5B%5D=Iowa&govLoc_byState%5B%5D=Kansas&govLoc_byState%5B%5D=Minnesota&govLoc_byState%5B%5D=Missouri&govLoc_byState%5B%5D=Nebraska&govLoc_byState%5B%5D=North+Dakota&govLoc_byState%5B%5D=South+Dakota&";
									break;
								case 'West':
									$searchParamsConstructed = $searchParamsConstructed . "govLoc_byState%5B%5D=Arizona&govLoc_byState%5B%5D=Colorado&govLoc_byState%5B%5D=Idaho&govLoc_byState%5B%5D=New+Mexico&govLoc_byState%5B%5D=Montana&govLoc_byState%5B%5D=Utah&govLoc_byState%5B%5D=Nevada&govLoc_byState%5B%5D=Wyoming&govLoc_byState%5B%5D=Alaska&govLoc_byState%5B%5D=California&govLoc_byState%5B%5D=Hawaii&govLoc_byState%5B%5D=Oregon&govLoc_byState%5B%5D=Washington&";
									break;
								default:
									break;
							}
						}
					}
				}
				break;
			case 'population': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 10) {
					foreach ($value as $item) {
						if ($item != "") {
						  $searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$item&";
						}
					}
				}
				break;
			// basic search end
			
			// power search start
			case 'selGovtExp':
				$selGovtExp = urlencode($value);
				if ($selGovtExp == 'gsTotal') {
					$searchParamsConstructed = $searchParamsConstructed . "expType%5B%5D=1&";
				}
				break;
			case 'selGovtExpScreen':
				$selGovtExpScreen = urlencode($value);
				//echo("selGovtExpScreen = $selGovtExpScreen<br>");
				switch($value) {
					case 'DefinedRange':
						$searchParamsConstructed = $searchParamsConstructed . "criteria=expenseBudget&";
						break;
					case 'SpendingBuckets':
						$searchParamsConstructed = $searchParamsConstructed . "criteria=expenseBudgetQuartile&";
						break;
					case 'PerCapitaSpending':
						$searchParamsConstructed = $searchParamsConstructed . "criteria=perCapitalBudgetQuartile&";
						break;
					default:
						break;
				}
				//$searchParamsConstructed = $searchParamsConstructed . "criteria=$value&";
				break;
			case 'gsPS': // array but only one value allowed
				if ($selGovtExp == 'gsPS') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finPS%5B%5D=$valueURLENCODED&";
				}
				break;
			case 'gsPW': // array but only one value allowed
				if ($selGovtExp == 'gsPW') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finPW%5B%5D=$valueURLENCODED&";
				}
				break;
			case 'gsH': // array but only one value allowed
				if ($selGovtExp == 'gsH') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finH%5B%5D=$valueURLENCODED&";
				}
				break;
			case 'gsU': // array but only one value allowed
				if ($selGovtExp == 'gsU') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finU%5B%5D=$valueURLENCODED&";
				}
				break;
			case 'gsT': // array but only one value allowed
				if ($selGovtExp == 'gsT') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finT%5B%5D=$valueURLENCODED&";
				}
				break;
			case 'gsL': // array but only one value allowed
				if ($selGovtExp == 'gsL') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finL%5B%5D=$valueURLENCODED&";
				}
				break;
			case 'gsF': // array but only one value allowed
				if ($selGovtExp == 'gsF') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finF%5B%5D=$valueURLENCODED&";
				}
				break;	
			case 'gsM': // array but only one value allowed
				if ($selGovtExp == 'gsM') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "finM%5B%5D=$valueURLENCODED&";
				}
				break;	
			case 'gsSC': // array but only one value allowed
				if ($selGovtExp == 'gsSC') {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "expType%5B%5D=$valueURLENCODED&";
				}
				break;	
			case 'expenseBudgetQuartileSel': // array
				if ($selGovtExpScreen == 'SpendingBuckets') {
					// could shut off if all selected?
					$numItems = count($value);
					if ($numItems != 5) {
						foreach ($value as $item) {
							if ($item != "") {
							  $itemURLENCODED = urlencode($item);
							  $searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
							}
						}
					}
				}
				break;	
			case 'perCapitalBudgetQuartileSel': // array
				if ($selGovtExpScreen == 'PerCapitaSpending') {
					// could shut off if all selected?
					$numItems = count($value);
					if ($numItems != 5) {
					  foreach ($value as $item) {
						  if ($item != "") {
							$itemURLENCODED = urlencode($item);
							$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
						  }
					  }
					}
				}
				break;	
			case 'expenseBudgetMoreThan':
				if ($selGovtExpScreen == 'DefinedRange') {
					// check if null then fill in 0
					if ($value == "") {
						$value = 0;
					}
					$valueURLENCODED = urlencode($value)/1000;
					$searchParamsConstructed = $searchParamsConstructed . "$key=$valueURLENCODED&";
				}
				break;
			case 'expenseBudgetLessThan':	
				if ($selGovtExpScreen == 'DefinedRange') {
					// check if null then fill in 1 Trillion
					if ($value == "") {
						$value = 1000000000000;
					}
					$valueURLENCODED = urlencode($value)/1000;
					$searchParamsConstructed = $searchParamsConstructed . "$key=$valueURLENCODED&";
				}
				break;
			case 'populationMoreThan':
			case 'populationLessThan':
				if ($value != "") {
					$valueURLENCODED = urlencode($value);
					$searchParamsConstructed = $searchParamsConstructed . "$key=$valueURLENCODED&";
				}
				break;
			case 'demoGQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 10) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;	
			case 'demoRQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 25) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;	
			case 'demoAQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 15) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoEdQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 10) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoIQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 25) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoEmQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 20) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoHQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 20) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoCQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 15) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoQQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 15) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'demoPQ': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 10) {
				  foreach ($value as $item) {
					  if ($item != "") {
						$itemURLENCODED = urlencode($item);
						$searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$itemURLENCODED&";
					  }
				  }
				}
				break;
			case 'fiscalYearEnd_Month': // array
				// could shut off if all selected?
				$numItems = count($value);
				if ($numItems != 13) {
					foreach ($value as $item) {
						if ($item != "") {
						  $searchParamsConstructed = $searchParamsConstructed . "$key%5B%5D=$item&";
						}
					}
				}
				break;
			// power search end
			
			default:
				break;
		}
	}
	//$searchParams = "govtType=$govtType&govLoc_byState=$govLoc_byState&population=$population&role=$role&off_email=$off_email";
	//echo("<br>DEBUG: searchParamsConstructed = $searchParamsConstructed<br>");
	//exit;
	
	$searchParams = 'byText=&' . $searchParamsConstructed;
	$_SESSION['lastSearchParams'] = $searchParams;
}

/********************************
 SEARCH PARAMETERS USED
********************************/
//$searchParamsFromSession = $_SESSION['lastSearchParams'];
//echo("DEBUG: searchParams = $searchParams <br>");
//echo("<!--DEBUG: searchParams = $searchParams -->");
//echo("<!--"); print_r($searchParams); echo("-->");


/**************************************
 POWER ALMANAC DATABASE SUMMARY
***************************************/
/*
Local Government Officials $Total_Gov_Officials
$Percentage_PAEmails found with Email Addresses $Total_Available_Emails
$Percentage_PAPhoneNumbers found with Phone Numbers $Total_Phone_Number
$Percentage_PAMailingAddresses found with Mailing Addresses $Total_Mailing_Street_Box
Unique Local Governments $Total_Governments
*/

/**************************************
 MATCHED SEARCH SUMMARY
***************************************/
/*
Local Government Officials $Num_Matched_OfficialsFORMATTED
$Percentage_Matched_Email found with Email Addresses $Num_Matched_EmailsFORMATTED
$Percentage_Matched_Phone_Numbers found with Phone Numbers $Phone_NumbersFORMATTED
$Percentage_Matched_Mailing_Addresses found with Mailing Addresses $Mailing_AddressesFORMATTED
Unique Local Governments $Num_Matched_GovernmentsFORMATTED
*/

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo->query("CALL DatabaseSummary(@Result)"); 
$pdoObject = $pdo->query("SELECT @Result");
$rsArray = $pdoObject->fetchAll();
$json = $rsArray[0]['@Result'];
$jsonArray = json_decode($json, TRUE);

$Total_No_Of_Records = number_format($jsonArray['Total_No_Of_Records']);
$Total_No_Of_Changed_Rec = number_format($jsonArray['Total_No_Of_Changed_Rec']);
$Total_Gov_Officials = number_format($jsonArray['Total_Gov_Officials']);
$Total_Governments = number_format($jsonArray['Total_Governments']);
$Total_Gov_With_No_Offices = number_format( $jsonArray['Total_Gov_With_No_Offices']);
$Total_Available_Emails = number_format($jsonArray['Total_Available_Emails']);
$Total_Phone_Numbers = number_format( $jsonArray['Total_Phone_Number']);
$Total_Mailing_Addresses = number_format($jsonArray['Total_Mailing_Street_Box']);
$Total_Phone_Number = number_format( $jsonArray['Total_Phone_Number']);
$Total_Mailing_Street_Box = number_format($jsonArray['Total_Mailing_Street_Box']);

$PAEmails = $jsonArray['Total_Available_Emails'];
$PAOfficials = $jsonArray['Total_Gov_Officials'];
$PAPhoneNumbers = $jsonArray['Total_Phone_Number'];
$PAMailingAddresses = $jsonArray['Total_Mailing_Street_Box'];

if ($Total_Gov_Officials != 0) {
	$Percentage_PAEmails = sprintf("%01.0f",($PAEmails/$PAOfficials)*100);
	$Percentage_PAPhoneNumbers = sprintf("%01.0f",($PAPhoneNumbers/$PAOfficials)*100);
	$Percentage_PAMailingAddresses = sprintf("%01.0f",($PAMailingAddresses/$PAOfficials)*100);
} else {
	$Percentage_PAEmails = "0.00%";
	$Percentage_PAPhoneNumbers = "0.00%";
	$Percentage_PAMailingAddresses = "0.00%";
}

// need to also get JSON_OFFICIALS and JSON_GOVERNMENTS from database summary
$sql ="USE " . escape($dbname);
$dbcnx = @PowerAlmanac\PDb::connect("$dbhost", "$dbuser", "$dbpass");
if (!$dbcnx) {
	print("Unable to connect to the database server at this time.\n");
exit();
}
if (!@PowerAlmanac\PDb::query($sql)){
	die('Unable to connect to government database: '. PowerAlmanac\PDb::error());
}

if ($_SESSION['logged_in'] != '1') {
	$User_Subscription = '0';
	$_SESSION['user_subscription'] = '0';
	$_SESSION['RegUser_ID'] = '0';
	$RegUser_ID = 0;
	$timeIs = time();
	$tableID = $RegUser_ID . '_' . substr($timeIs,-6);
} else {
	$User_Subscription = $_SESSION['user_subscription'];
	$RegUser_ID = $_SESSION['RegUser_ID'];
	$timeIs = time();
	$tableID = $RegUser_ID . '_' . substr($timeIs,-6);
}

/* 
   DECIDE HOW TO GET SEARCH RESULTS

   1. Use Previous Results - stored in Session variables ($newSearch != 1)
   2. Use PA Database summary - (($usePASummary == 1) || ($firstTimeSearch == 1))
   3. New search ($newSearch == 1)
*/

if (($newSearch == 1) || ($useSavedSearchParams == 1)) {
	//echo("DEBUG: Results - Search Summary request ($newSearch:$useSavedSearchParams)<br>");
	$useCache = 0;
	// handling search result tables
	if (isset($_SESSION['previousTableID'])) {
		// delete previous table
		$previoustableID = $_SESSION['previousTableID'];
		$pdo->query(sprintf("CALL Delete_Research_Tables('%s')", escape($previoustableID)));
		// now
		$_SESSION['previousTableID'] = $tableID;
		$_SESSION['currentTableID'] = $tableID;
	} else {
		// first time
		$_SESSION['previousTableID'] = $tableID;
		$_SESSION['currentTableID'] = $tableID;
	}
	
	if ($_SESSION['results_GovtOffMustMatchRoles'] == '1') {
		$searchParams = $searchParams . "&top_elected=0&";
		$pdo->query(sprintf("CALL SearchSummary('%s','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($tableID), escape($searchParams)));
	} else {
		$searchParams = $searchParams . "&top_elected=1&";
        $pdo->query(sprintf("CALL SearchSummary('%s','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($tableID), escape($searchParams)));
	}
	
	$pdoObject = $pdo->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists");
	$rsArray = $pdoObject->fetchAll();
	//print_r($rsArray);
	$JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
	$JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
	$JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];
	$SearchSummaryFLAG = $rsArray[0]['@flag'];
	$table_existsFLAG = $rsArray[0]['@table_exists'];
	//print_r($JSON_SUMMARY);
	
	$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
	switch(json_last_error())
	{
		case JSON_ERROR_DEPTH:
			echo ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_CTRL_CHAR:
			echo ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			echo ' - Syntax error, malformed JSON_SUMMARY';
			print_r($JSON_SUMMARY);
		break;
		case JSON_ERROR_NONE:
			//echo ' - No errors';
		break;
	}
	
	$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
	switch(json_last_error())
	{
		case JSON_ERROR_DEPTH:
			echo ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_CTRL_CHAR:
			echo ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			echo ' - Syntax error, malformed JSON_GOVERNMENTS';
			print_r($JSON_GOVERNMENTS);
		break;
		case JSON_ERROR_NONE:
			//echo ' - No errors';
		break;
	}
	//print_r($JSON_GOVERNMENTS_Array);
	
	$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);
	switch(json_last_error())
	{
		case JSON_ERROR_DEPTH:
			echo ' - Maximum stack depth exceeded';
		break;
		case JSON_ERROR_CTRL_CHAR:
			echo ' - Unexpected control character found';
		break;
		case JSON_ERROR_SYNTAX:
			echo ' - Syntax error, malformed JSON_OFFICIALS';
			print_r($JSON_OFFICIALS);
			//print_r($JSON_OFFICIALS);
		break;
		case JSON_ERROR_NONE:
			//echo ' - No errors';
		break;
	}
	//print_r($JSON_OFFICIALS_Array);
	
	$Num_Matched_GovernmentsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Governments']);
	$Num_Matched_OfficialsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Officials']);
	//$Num_Matched_RecordsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Records']);
	$Num_Matched_EmailsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Emails']);
	
	// new
	$Phone_NumbersFORMATTED = number_format($JSON_SUMMARY_Array['Phone_Numbers']);
	$Mailing_AddressesFORMATTED = number_format($JSON_SUMMARY_Array['Mailing_Addresses']);
	
	// percentage calculations
	$Num_Matched_Emails = $JSON_SUMMARY_Array['Num_Matched_Emails'];
	$Num_Matched_Officials = $JSON_SUMMARY_Array['Num_Matched_Officials'];
	$Phone_Numbers = $JSON_SUMMARY_Array['Phone_Numbers'];
	$Mailing_Addresses = $JSON_SUMMARY_Array['Mailing_Addresses'];
	
	if ($Num_Matched_Officials != 0) {
		$Percentage_Matched_Emails = sprintf("%01.0f",($Num_Matched_Emails/$Num_Matched_Officials)*100);
		$Percentage_Matched_Phone_Numbers = sprintf("%01.0f",($Phone_Numbers/$Num_Matched_Officials)*100);
		$Percentage_Matched_Mailing_Addresses = sprintf("%01.0f",($Mailing_Addresses/$Num_Matched_Officials)*100);
	} else {
		$Percentage_Matched_Emails = "0";
		$Percentage_Matched_Phone_Numbers = "0";
		$Percentage_Matched_Mailing_Addresses = "0";
	}
	
	$_SESSION['lastSearchNumMatched'] = $JSON_SUMMARY_Array['Num_Matched_Officials']; // cannot be formatted
	
	// SAVE PREVIOUS RESULTS IN SESSION JSON_SUMMARY, JSON_GOVERNMENTS, JSON_OFFICIALS
	$_SESSION['upr_JSON_SUMMARY'] = $JSON_SUMMARY;
	$_SESSION['upr_JSON_GOVERNMENTS'] = $JSON_GOVERNMENTS;
	$_SESSION['upr_JSON_OFFICIALS'] = $JSON_OFFICIALS;
} else {
	if (($usePASummary == 1) || ($firstTimeSearch == 1)) {
		//echo("DEBUG: Results - Using PA Database Summary cache<br>");
		// use PA database summary
		// 6/13/2011 fill in all zeroes
		$tableID = $RegUser_ID . '_TEMPORARY';
		$_SESSION['previousTableID'] = $tableID;
		$_SESSION['currentTableID'] = $tableID;
		
		$Num_Matched_OfficialsFORMATTED = 0;
		$Percentage_Matched_Emails = 0;
		$Num_Matched_EmailsFORMATTED = 0;
		$Percentage_Matched_Phone_Numbers = 0;
		$Phone_NumbersFORMATTED = 0;
		$Percentage_Matched_Mailing_Addresses = 0;
		$Mailing_AddressesFORMATTED = 0;
		$Num_Matched_GovernmentsFORMATTED = 0;
		
		$JSON_GOVERNMENTS_Array = array();
		$JSON_OFFICIALS_Array = array();
		$useCache = 0;
		$_SESSION['lastSearchNumMatched'] = 0;
		
		/*
		$Num_Matched_OfficialsFORMATTED = $Total_Gov_Officials;
		$Percentage_Matched_Emails = $Percentage_PAEmails;
		$Num_Matched_EmailsFORMATTED = $Total_Available_Emails;
		$Percentage_Matched_Phone_Numbers = $Percentage_PAPhoneNumbers;
		$Phone_NumbersFORMATTED = $Total_Phone_Number;
		$Percentage_Matched_Mailing_Addresses = $Percentage_PAMailingAddresses;
		$Mailing_AddressesFORMATTED = $Total_Mailing_Street_Box;
		$Num_Matched_GovernmentsFORMATTED = $Total_Governments;
		
		$read_sql = "SELECT * FROM database_summary
			LIMIT 1
			";
		$result_read = @PowerAlmanac\PDb::query($read_sql);
		if (!$result_read) {
			die("Error reading from database_summary database: $read_sql" . PowerAlmanac\PDb::error());
		}
		$onerow = PowerAlmanac\PDb::fetch_array($result_read);
		$JSON_GOVERNMENTS = $onerow['JSON_GOVERNMENTS'];
		$JSON_OFFICIALS = $onerow['JSON_OFFICIALS'];
		$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
		$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);
		$useCache = 1;
		$_SESSION['lastSearchNumMatched'] = str_replace(",","",$Total_Gov_Officials); // cannot be formatted
		*/
		
	} else {
		//echo("DEBUG: Results - Using Previous Results<br>");
		
		$JSON_SUMMARY = $_SESSION['upr_JSON_SUMMARY'];
		$JSON_GOVERNMENTS = $_SESSION['upr_JSON_GOVERNMENTS'];
		$JSON_OFFICIALS = $_SESSION['upr_JSON_OFFICIALS'];
	
		$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
		switch(json_last_error())
		{
			case JSON_ERROR_DEPTH:
				echo ' - Maximum stack depth exceeded';
			break;
			case JSON_ERROR_CTRL_CHAR:
				echo ' - Unexpected control character found';
			break;
			case JSON_ERROR_SYNTAX:
				echo ' - Syntax error, malformed JSON_SUMMARY';
				//print_r($JSON_SUMMARY);
			break;
			case JSON_ERROR_NONE:
				//echo ' - No errors';
			break;
		}
		
		$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
		switch(json_last_error())
		{
			case JSON_ERROR_DEPTH:
				echo ' - Maximum stack depth exceeded';
			break;
			case JSON_ERROR_CTRL_CHAR:
				echo ' - Unexpected control character found';
			break;
			case JSON_ERROR_SYNTAX:
				echo ' - Syntax error, malformed JSON_GOVERNMENTS';
				//print_r($JSON_GOVERNMENTS);
			break;
			case JSON_ERROR_NONE:
				//echo ' - No errors';
			break;
		}
		//print_r($JSON_GOVERNMENTS_Array);
		
		$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);
		switch(json_last_error())
		{
			case JSON_ERROR_DEPTH:
				echo ' - Maximum stack depth exceeded';
			break;
			case JSON_ERROR_CTRL_CHAR:
				echo ' - Unexpected control character found';
			break;
			case JSON_ERROR_SYNTAX:
				echo ' - Syntax error, malformed JSON_OFFICIALS';
				//print_r($JSON_OFFICIALS);
			break;
			case JSON_ERROR_NONE:
				//echo ' - No errors';
			break;
		}
		//print_r($JSON_OFFICIALS_Array);
		
		$Num_Matched_GovernmentsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Governments']);
		$Num_Matched_OfficialsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Officials']);
		//$Num_Matched_RecordsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Records']);
		$Num_Matched_EmailsFORMATTED = number_format($JSON_SUMMARY_Array['Num_Matched_Emails']);
		
		// new
		$Phone_NumbersFORMATTED = number_format($JSON_SUMMARY_Array['Phone_Numbers']);
		$Mailing_AddressesFORMATTED = number_format($JSON_SUMMARY_Array['Mailing_Addresses']);
		
		// percentage calculations
		$Num_Matched_Emails = $JSON_SUMMARY_Array['Num_Matched_Emails'];
		$Num_Matched_Officials = $JSON_SUMMARY_Array['Num_Matched_Officials'];
		$Phone_Numbers = $JSON_SUMMARY_Array['Phone_Numbers'];
		$Mailing_Addresses = $JSON_SUMMARY_Array['Mailing_Addresses'];
		
		if ($Num_Matched_Officials != 0) {
			$Percentage_Matched_Emails = sprintf("%01.0f",($Num_Matched_Emails/$Num_Matched_Officials)*100);
			$Percentage_Matched_Phone_Numbers = sprintf("%01.0f",($Phone_Numbers/$Num_Matched_Officials)*100);
			$Percentage_Matched_Mailing_Addresses = sprintf("%01.0f",($Mailing_Addresses/$Num_Matched_Officials)*100);
		} else {
			$Percentage_Matched_Emails = "0.00%";
			$Percentage_Matched_Phone_Numbers = "0.00%";
			$Percentage_Matched_Mailing_Addresses = "0.00%";
		}
		
		$_SESSION['lastSearchNumMatched'] = $JSON_SUMMARY_Array['Num_Matched_Officials']; // cannot be formatted
	}
}

?>
<html>
<head>
<title>Search for Local Gov Officials | Power Almanac</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="description" content="Online directory of city and county officials for all U.S.  Easy to use, comprehensive, and up-to-date.  Download names, titles, emails, phone numbers, and more." />
<meta name="Keywords" content="Cities,Counties,Government officials,Government emails,Government phone numbers" /> 

<link rel="stylesheet" href="popup.css" type="text/css">
<link rel="stylesheet" type="text/css" href="shadowbox-3.0.3/shadowbox.css">
<link rel="stylesheet" type="text/css" href="css/accordion.css" />
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />

<script type="text/javascript" src="shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript" src="custom-form-elements.js"></script>
<script type="text/javascript" src="showhint.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>

<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.msAccordion.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.8.10.custom.min.js"></script>

<!--<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />-->
<link rel="Stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/redmond/jquery-ui.css" type="text/css" />
<!--
<link rel="Stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css" type="text/css" />
<link rel="Stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-darkness/jquery-ui.css" type="text/css" /> 
-->

<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function checkAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = true ;
	Custom.checkAll();
}

function uncheckAll(field)
{
for (i = 0; i < field.length; i++)
	field[i].checked = false ;
	Custom.clear();
}
function toggleAllButton(name,field,CheckBoxIMG)
{
	var g = document.getElementById(CheckBoxIMG);
	var currentState = g.src;
	//alert(currentState);
	var checkState = currentState.indexOf('uncheckedAll');
	//alert(checkState);
	if (checkState != '-1') {
		// currently unchecked
		//alert('Currently UNCHECKED');
		var s = '1';
		g.src = 'images/checkedAll.png';
		for (i = 0; i < field.length; i++)
		{
			field[i].checked = true ;
			Custom.checkAll();
		}
	} else {
		//alert('Currently CHECKED');
		// currently checked
		var s = '0';
		g.src = 'images/uncheckedAll.png';
		for (i = 0; i < field.length; i++)
		{
			field[i].checked = false ;
			Custom.clear();
		}
	}
	$.ajax({
	   type: "POST",
	   url: "ajax-checkboxsettings.php",
	   data: "id=" + name + "&s=" + s,
	   success: function(msg){
		 //alert( "Data Saved: " + msg );
	   }
	 });
}
function toggleAllLocationButton(CheckBoxIMG)
{
	var g = document.getElementById(CheckBoxIMG);
	var currentState = g.src;
	//alert(currentState);
	var checkState = currentState.indexOf('uncheckedAll');
	//alert(checkState);
	if (checkState != '-1') {
		// currently unchecked
		//alert('Currently UNCHECKED');
		g.src = 'images/checkedAll.png';
		checkAll(document.search.location);
		checkAll(document.search.region);
		var s = '1';
	} else {
		//alert('Currently CHECKED');
		// currently checked
		g.src = 'images/uncheckedAll.png';
		uncheckAll(document.search.location);
		uncheckAll(document.search.region);
		var s = '0';
	}
	$.ajax({
	   type: "POST",
	   url: "ajax-checkboxsettings.php",
	   data: "id=states&s=" + s,
	   success: function(msg){
		 //alert( "Data Saved: " + msg );
	   }
	 });
}
function toggleAllDemographicsButton(name,field,CheckBoxIMG)
{
	var g = document.getElementById(CheckBoxIMG);
	var currentState = g.src;
	//alert(currentState);
	var checkState = currentState.indexOf('uncheckedAll');
	//alert(checkState);
	if (checkState != '-1') {
		// currently unchecked
		//alert('Currently UNCHECKED');
		var s = '1';
		g.src = 'images/checkedAll.png';
		checkAll(document.search.demographics);
	} else {
		//alert('Currently CHECKED');
		// currently checked
		var s = '0';
		g.src = 'images/uncheckedAll.png';
		uncheckAll(document.search.demographics);
	}
	$.ajax({
	   type: "POST",
	   url: "ajax-checkboxsettings.php",
	   data: "id=" + name + "&s=" + s,
	   success: function(msg){
		 //alert( "Data Saved: " + msg );
	   }
	 });
}
function resetDemographics()
{
	uncheckAll(document.search.demographics);
}
//  End -->
</script>

<script type="text/javascript">
<!--
function toggle_powersearch(id) {
var e = document.getElementById(id);
//alert(img_e);
if(e.style.display == 'none')
{
   e.style.display = 'block';
}
else {	
   e.style.display = 'none';
   }
}
//-->
</script>

<SCRIPT LANGUAGE="JavaScript">
function resetSpendingCriteria()
{
	// set defaults
	document.search.selGovtExp[0].checked = true;
	document.search.selGovtExpScreen[1].checked = true;
	var a = document.getElementById('SpendingBuckets');
	a.style.display = 'block';
	uncheckAll(document.search.perCapitalBudgetQuartileSel);
	uncheckAll(document.search.expenseBudgetQuartileSel);
	document.search.expenseBudgetMoreThan.value = 0;
	document.search.expenseBudgetLessThan.value = 0;
	
	// reset others
	document.search.selGovtExp[1].checked = false;
	document.search.selGovtExp[2].checked = false;
	document.search.selGovtExp[3].checked = false;
	document.search.selGovtExp[4].checked = false;
	document.search.selGovtExp[5].checked = false;
	document.search.selGovtExp[6].checked = false;
	document.search.selGovtExp[7].checked = false;
	document.search.selGovtExp[8].checked = false;
	document.search.selGovtExpScreen[0].checked = false;
	document.search.selGovtExpScreen[2].checked = false;
	
	var b = document.getElementById('gsPS');
	b.style.display = 'none';
	var c = document.getElementById('gsPW');
	c.style.display = 'none';
	var d = document.getElementById('gsH');
	d.style.display = 'none';
	var e = document.getElementById('gsU');
	e.style.display = 'none';
	var f = document.getElementById('gsT');
	f.style.display = 'none';
	var g = document.getElementById('gsL');
	g.style.display = 'none';
	var h = document.getElementById('gsF');
	h.style.display = 'none';
	var i = document.getElementById('gsM');
	i.style.display = 'none';
	var j = document.getElementById('gsSC');
	j.style.display = 'none';

	var k = document.getElementById('DefinedRange');
	k.style.display = 'none';
	var l = document.getElementById('PerCapitaSpending');
	l.style.display = 'none';

}

function resetCheckDemographics()
{
	checkAll(document.search.demographics);
}

function resetClearDemographics()
{
	uncheckAll(document.search.demographics);
}

function checkAllStatesRegions()
{
	checkAll(document.search.location);
	checkAll(document.search.region);
}

function uncheckAllStatesRegions()
{
	uncheckAll(document.search.location);
	uncheckAll(document.search.region);
}


function selLocationNational()
{
	//alert('NATIONAL selected');
	// hide others
	var s = document.getElementById('selREGIONS');
	s.style.display = 'none';
	var t = document.getElementById('selSTATES');
	t.style.display = 'none';
	var u = document.getElementById('locationByZipCode');
	u.style.display = 'none';
	checkAll(document.search.location);
	checkAll(document.search.region);
	var g = document.getElementById('locationCheckBoxIMG');
	g.src = 'images/checkedAll.png';
	$.ajax({
	   type: "POST",
	   url: "ajax-checkboxsettings.php",
	   data: "id=states&s=1",
	   success: function(msg){
		 //alert( "Data Saved: " + msg );
	   }
	 });
}

function selLocationRegions()
{
	//alert('REGIONS selected');
	var z = document.getElementById('selREGIONS');
	z.style.display = 'block';
	// hide others
	var s = document.getElementById('selSTATES');
	s.style.display = 'none';
	var t = document.getElementById('locationByZipCode');
	t.style.display = 'none';
}

function selLocationStates()
{
	//alert('STATES selected');
	var z = document.getElementById('selSTATES');
	z.style.display = 'block';
	// hide others
	var s = document.getElementById('selREGIONS');
	s.style.display = 'none';
	var t = document.getElementById('locationByZipCode');
	t.style.display = 'none';
}

function selLocationZipCode()
{
	//alert('ZIPCODE selected');
	var z = document.getElementById('locationByZipCode');
	z.style.display = 'block';
	// hide others
	var s = document.getElementById('selREGIONS');
	s.style.display = 'none';
	var t = document.getElementById('selSTATES');
	t.style.display = 'none';
	// reset all state and region choices
	uncheckAll(document.search.location);
	uncheckAll(document.search.region);
	var g = document.getElementById('locationCheckBoxIMG');
	g.src = 'images/uncheckedAll.png';
	$.ajax({
	   type: "POST",
	   url: "ajax-checkboxsettings.php",
	   data: "id=states&s=0",
	   success: function(msg){
		 //alert( "Data Saved: " + msg );
	   }
	 });
}

function checkBoxChecked(element) 
{
	var x = 'checkBoxChecked ' + element.value + ' ' + element.name + ' ' + element.checked;
	//alert(x);
	//alert(element.name);
	
	// reset demographics checkall if any changes
	/*
	if ((element.name == 'demoGQ[]') || (element.name == 'demoRQ[]') || (element.name == 'demoAQ[]') || (element.name == 'demoEdQ[]') || (element.name == 'demoIQ[]') || (element.name == 'demoEmQ[]') || (element.name == 'demoHQ[]') || (element.name == 'demoCQ[]') || (element.name == 'demoQQ[]') || (element.name == 'demoPQ[]')){
		var g = document.getElementById('demographicsCheckBoxIMG');
		g.src = 'images/uncheckedAll.png';
		$.ajax({
		   type: "POST",
		   url: "ajax-checkboxsettings.php",
		   data: "id=demographics&s=0",
		   success: function(msg){
			 //alert( "Data Saved: " + msg );
		   }
		 });
	}
	*/
	
	// check for 4 or less
	/*
	if ((element.name == 'demoGQ[]') || (element.name == 'demoRQ[]') || (element.name == 'demoAQ[]') || (element.name == 'demoEdQ[]') || (element.name == 'demoIQ[]') || (element.name == 'demoEmQ[]') || (element.name == 'demoHQ[]') || (element.name == 'demoCQ[]') || (element.name == 'demoQQ[]') || (element.name == 'demoPQ[]')){
			alert(element.name);
			var field = document.getElementById('demoGQ[]');
			alert(field);
			alert(field.length);
			if (element.length == 4) {
				alert("Please choose no more than 4 selections for any single demographic criteria.");
			}
	}
	*/
	if (element.name == 'population[]') {
		// reset checkall flag
		var g = document.getElementById('populationCheckBoxIMG');
		g.src = 'images/uncheckedAll.png';
		$.ajax({
		   type: "POST",
		   url: "ajax-checkboxsettings.php",
		   data: "id=pop&s=0",
		   success: function(msg){
			 //alert( "Data Saved: " + msg );
		   }
		 });
	}

	if ((element.name == 'role_1') || (element.name == 'role_2') || (element.name == 'role_3') || (element.name == 'role_4') || (element.name == 'role_5') || (element.name == 'role_6') || (element.name == 'role_7') || (element.name == 'role_8') || (element.name == 'role_9')){
		// reset checkall flag
		var g = document.getElementById('rolesCheckBoxIMG');
		g.src = 'images/uncheckedAll.png';
		$.ajax({
		   type: "POST",
		   url: "ajax-checkboxsettings.php",
		   data: "id=role&s=0",
		   success: function(msg){
			 //alert( "Data Saved: " + msg );
		   }
		 });
	}
	
	if ((element.name == 'govType_counties') || (element.name == 'govType_townships') || (element.name == 'govType_municipalities')) {
		// reset checkall flag
		var g = document.getElementById('typesCheckBoxIMG');
		g.src = 'images/uncheckedAll.png';
		$.ajax({
		   type: "POST",
		   url: "ajax-checkboxsettings.php",
		   data: "id=govType&s=0",
		   success: function(msg){
			 //alert( "Data Saved: " + msg );
		   }
		 });
	}
	
	if (element.name == 'fiscalYearEnd_Month[]') {
		// reset checkall flag
		var g = document.getElementById('fiscalYearCheckBoxIMG');
		g.src = 'images/uncheckedAll.png';
		$.ajax({
		   type: "POST",
		   url: "ajax-checkboxsettings.php",
		   data: "id=fiscalyear&s=0",
		   success: function(msg){
			 //alert( "Data Saved: " + msg );
		   }
		 });
	}
	
	if (element.name == 'govLoc_byState[]') {
		// change in states result in unchecking all regions
		uncheckAll(document.search.region);
		// reset states checkall flag
		var g = document.getElementById('locationCheckBoxIMG');
		g.src = 'images/uncheckedAll.png';
		$.ajax({
		   type: "POST",
		   url: "ajax-checkboxsettings.php",
		   data: "id=states&s=0",
		   success: function(msg){
			 //alert( "Data Saved: " + msg );
		   }
		 });
	}
	if (element.name == 'govLoc_byRegion[]') {
		// check and uncheck appropriate states based regions action
		switch(element.value) {
			case 'Northeast':
				// reset states checkall flag
				var g = document.getElementById('locationCheckBoxIMG');
				g.src = 'images/uncheckedAll.png';
				$.ajax({
				   type: "POST",
				   url: "ajax-checkboxsettings.php",
				   data: "id=states&s=0",
				   success: function(msg){
					 //alert( "Data Saved: " + msg );
				   }
				 });
				if (element.checked == false) {
					// deselecting now
					//alert('reset Northeast');
					var field = document.search.location;
					//alert(field.length);
					for (i = 0; i < field.length; i++)
					{
						//alert(field[i].value);
						switch(field[i].value) {
							case 'Connecticut':
							case 'Maine':
							case 'Massachusetts':
							case 'New Hampshire':
							case 'Rhode Island':
							case 'Vermont':
							case 'New Jersey':
							case 'New York':
							case 'Pennsylvania':
								field[i].checked = false ;
								break;
							default:
								break;
						}
					}
				} else {
					// selecting now
					//alert('setting Northeast');
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Connecticut':
							case 'Maine':
							case 'Massachusetts':
							case 'New Hampshire':
							case 'Rhode Island':
							case 'Vermont':
							case 'New Jersey':
							case 'New York':
							case 'Pennsylvania':
								field[i].checked = true ;
								break;
							default:
								break;
						}
					}
				}
				break;
			case 'South':
				// reset states checkall flag
				var g = document.getElementById('locationCheckBoxIMG');
				g.src = 'images/uncheckedAll.png';
				$.ajax({
				   type: "POST",
				   url: "ajax-checkboxsettings.php",
				   data: "id=states&s=0",
				   success: function(msg){
					 //alert( "Data Saved: " + msg );
				   }
				 });
				if (element.checked == false) {
					// deselecting now
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Delaware':
							case 'District of Columbia':
							case 'Florida':
							case 'Georgia':
							case 'Maryland':
							case 'North Carolina':
							case 'South Carolina':
							case 'Virginia':
							case 'West Virginia':
							case 'Alabama':
							case 'Kentucky':
							case 'Mississippi':
							case 'Tennessee':
							case 'Arkansas':
							case 'Louisiana':
							case 'Oklahoma':
							case 'Texas':
								field[i].checked = false ;
								break;
							default:
								break;
						}
					}
				} else {
					// selecting now
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Delaware':
							case 'District of Columbia':
							case 'Florida':
							case 'Georgia':
							case 'Maryland':
							case 'North Carolina':
							case 'South Carolina':
							case 'Virginia':
							case 'West Virginia':
							case 'Alabama':
							case 'Kentucky':
							case 'Mississippi':
							case 'Tennessee':
							case 'Arkansas':
							case 'Louisiana':
							case 'Oklahoma':
							case 'Texas':
								field[i].checked = true ;
								break;
							default:
								break;
						}
					}
				}
				break;
			case 'Midwest':
				// reset states checkall flag
				var g = document.getElementById('locationCheckBoxIMG');
				g.src = 'images/uncheckedAll.png';
				$.ajax({
				   type: "POST",
				   url: "ajax-checkboxsettings.php",
				   data: "id=states&s=0",
				   success: function(msg){
					 //alert( "Data Saved: " + msg );
				   }
				 });
				if (element.checked == false) {
					// deselecting now
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Indiana':
							case 'Illinois':
							case 'Michigan':
							case 'Ohio':
							case 'Wisconsin':
							case 'Iowa':
							case 'Kansas':
							case 'Minnesota':
							case 'Missouri':
							case 'Nebraska':
							case 'South Dakota':
							case 'North Dakota':
								field[i].checked = false ;
								break;
							default:
								break;
						}
					}
				} else {
					// selecting now
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Indiana':
							case 'Illinois':
							case 'Michigan':
							case 'Ohio':
							case 'Wisconsin':
							case 'Iowa':
							case 'Kansas':
							case 'Minnesota':
							case 'Missouri':
							case 'Nebraska':
							case 'South Dakota':
							case 'North Dakota':
								field[i].checked = true ;
								break;
							default:
								break;
						}
					}
				}
				break;
			case 'West':
				// reset states checkall flag
				var g = document.getElementById('locationCheckBoxIMG');
				g.src = 'images/uncheckedAll.png';
				$.ajax({
				   type: "POST",
				   url: "ajax-checkboxsettings.php",
				   data: "id=states&s=0",
				   success: function(msg){
					 //alert( "Data Saved: " + msg );
				   }
				 });
				if (element.checked == false) {
					// deselecting now
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Arizona':
							case 'Colorado':
							case 'Idaho':
							case 'New Mexico':
							case 'Montana':
							case 'Utah':
							case 'Nevada':
							case 'Wyoming':
							case 'Alaska':
							case 'California':
							case 'Hawaii':
							case 'Oregon':
							case 'Washington':
								field[i].checked = false ;
								break;
							default:
								break;
						}
					}
				} else {
					// selecting now
					var field = document.search.location;
					for (i = 0; i < field.length; i++)
					{
						switch(field[i].value) {
							case 'Arizona':
							case 'Colorado':
							case 'Idaho':
							case 'New Mexico':
							case 'Montana':
							case 'Utah':
							case 'Nevada':
							case 'Wyoming':
							case 'Alaska':
							case 'California':
							case 'Hawaii':
							case 'Oregon':
							case 'Washington':
								field[i].checked = true ;
								break;
							default:
								break;
						}
					}
				}
				break;
			default:
				alert('error');
			 break;
			}
	}
}

function validateSearchForm()
{
	var x = document.forms["search"]["govType_counties"].checked
	var y = document.forms["search"]["govType_townships"].checked
	var z = document.forms["search"]["govType_municipalities"].checked
	var types = x + y + z;
	if (types == 0)
	{
	  alert("Please make at least one selection from the TYPES section.");
	  return false;
	}
	
	var roles1 = document.forms["search"]["role_1"].checked
	var roles2 = document.forms["search"]["role_2"].checked
	var roles3 = document.forms["search"]["role_3"].checked
	var roles4 = document.forms["search"]["role_4"].checked
	var roles5 = document.forms["search"]["role_5"].checked
	var roles6 = document.forms["search"]["role_6"].checked
	var roles7 = document.forms["search"]["role_7"].checked
	var roles8 = document.forms["search"]["role_8"].checked
	var roles9 = document.forms["search"]["role_9"].checked
	var roles = roles1 + roles2 + roles3 + roles4 + roles5 + roles6 + roles7 + roles8 + roles9;
	if (roles == 0)
	{
	  alert("Please make at least one selection from the ROLES section.");
	  return false;
	} 
	
	var pop = 0;
	for (i = 0; i < document.forms["search"]["population[]"].length; i++)
	{
		pop = pop + document.forms["search"]["population[]"][i].checked
	}
	if (pop == 0)
	{
	  alert("Please make at least one selection from the POPULATION section.");
	  return false;
	}
	
	var selectRegion = document.forms["search"]["selLocation"][1].checked
	var selectState = document.forms["search"]["selLocation"][2].checked
	var selectZipCode = document.forms["search"]["selLocation"][3].checked
	
	//alert(selectRegion);alert(selectState);alert(selectZipCode);
	
	if (selectRegion) {
		var region1 = document.forms["search"]["govLoc_byRegion[]"][0].checked
		var region2 = document.forms["search"]["govLoc_byRegion[]"][1].checked
		var region3 = document.forms["search"]["govLoc_byRegion[]"][2].checked
		var region4 = document.forms["search"]["govLoc_byRegion[]"][3].checked
		var region = region1 + region2 + region3 + region4;
		if (region == 0)
		{
		  alert("Please make at least one selection from the REGIONS section.");
		  return false;
		}
	}
	if (selectState) {
		var states = 0;
		for (s = 0; s < document.forms["search"]["govLoc_byState[]"].length; s++)
		{
			states = states + document.forms["search"]["govLoc_byState[]"][s].checked
		}
		if (states == 0)
		{
		  alert("Please make at least one selection from the STATES section.");
		  return false;
		}
	}
	if (selectZipCode) {
		var byZipcode = document.forms["search"]["byZipcode"].value
		var byZipcodeDistance = document.forms["search"]["byZipcodeDistance"].value
		
		if ((byZipcode == '') || (byZipcodeDistance == ''))
		{
		  alert("Please enter a ZIPCODE and a DISTANCE.");
		  return false;
		}
		// check if valid zipcode
		//alert('checking zipcode...');
		/*
		$.post("ajax-checkzipcode.php", {zip: byZipcode},
		   function(data) {
			 alert("Data Loaded: " + data);
		   });
		 */
		var zipcodestatus = 1;
		$.ajax({
		   type: "GET",
		   url: "ajax-checkzipcode.php",
		   async:false,
		   data: "zip=" + byZipcode,
		   success: function(msg){
			 //alert( "Zip results: " + msg );
			 if (msg == 0) {
			  alert("Zipcode entered is INVALID. Please enter a valid zipcode and try again.");
			  zipcodestatus = 0;
			 }
		   }
		 });
		 //alert(zipcodestatus);
		 if (zipcodestatus == 0) {
			 return false;
		 }
	}
}
function validatePowerSearchForm()
{
	var rc = validateSearchForm();
	if (rc == false) {
		return false;
	}
	
	var fiscalyear = 0;
	for (f = 0; f < document.forms["search"]["fiscalYearEnd_Month[]"].length; f++)
	{
		fiscalyear = fiscalyear + document.forms["search"]["fiscalYearEnd_Month[]"][f].checked
	}
	if (fiscalyear == 0)
	{
	  alert("Please make at least one selection from the FISCAL YEAR section.");
	  return false;
	}
	
	var selectRange = document.forms["search"]["selGovtExpScreen"][0].checked
	//var selectQuartile = document.forms["search"]["selGovtExpScreen"][1].checked
	//var selectPerCapita = document.forms["search"]["selGovtExpScreen"][2].checked
	
	if (selectRange) {
		var expenseBudgetMoreThan = document.forms["search"]["expenseBudgetMoreThan"].value
		var expenseBudgetLessThan = document.forms["search"]["expenseBudgetLessThan"].value
		
		if ((expenseBudgetMoreThan == '') || (expenseBudgetLessThan == ''))
		{
		  alert("Please enter a Spending MORE THAN and a Spending LESS THAN value.");
		  return false;
		}
	}
	// 6/14/2011 default checked but don't display
	/*
	if (selectQuartile) {
		var squartile = 0;
		for (sq = 0; sq < document.forms["search"]["expenseBudgetQuartileSel[]"].length; sq++)
		{
			squartile = squartile + document.forms["search"]["expenseBudgetQuartileSel[]"][sq].checked
		}
		if (squartile == 0)
		{
		  alert("Please make at least one selection from the SPEND BY QUARTILE section.");
		  return false;
		}
	}
	if (selectPerCapita) {
		var pcquartile = 0;
		for (pcq = 0; pcq < document.forms["search"]["perCapitalBudgetQuartileSel[]"].length; pcq++)
		{
			pcquartile = pcquartile + document.forms["search"]["perCapitalBudgetQuartileSel[]"][pcq].checked
		}
		if (pcquartile == 0)
		{
		  alert("Please make at least one selection from the PER CAPITA SPEND QUARTILE section.");
		  return false;
		}
	}
	*/
}
</script>

<script type="text/javascript">
		
	$(function() {
		$("buttonMailCheck").button(
		{
			icons:{primary: "ui-icon-check",secondary: "ui-icon-mail-closed"}
		});
		$("buttonMailUnCheck").button(
		{
			icons:{primary: "ui-icon-minus",secondary: "ui-icon-mail-closed"}
		});
		$("buttonRoleCheck").button(
		{
			icons:{primary: "ui-icon-check",secondary: "ui-icon-person"}
		});
		$("buttonRoleUnCheck").button(
		{
			icons:{primary: "ui-icon-minus",secondary: "ui-icon-person"}
		})
	});
		
</script>

<SCRIPT LANGUAGE="JavaScript">

function selGovtExpJS(id)
{
	//alert('selGovtExpTEST called');
	// hide all
	var a = document.getElementById('gsTotal');
	a.style.display = 'none';
	var b = document.getElementById('gsPS');
	b.style.display = 'none';
	var c = document.getElementById('gsPW');
	c.style.display = 'none';
	var d = document.getElementById('gsH');
	d.style.display = 'none';
	var e = document.getElementById('gsU');
	e.style.display = 'none';
	var f = document.getElementById('gsT');
	f.style.display = 'none';
	var g = document.getElementById('gsL');
	g.style.display = 'none';
	var h = document.getElementById('gsF');
	h.style.display = 'none';
	var i = document.getElementById('gsM');
	i.style.display = 'none';
	var j = document.getElementById('gsSC');
	j.style.display = 'none';
	
	// show selected
	switch(id)
	{
		case 'gsTotal':
		  //alert('gsTotal selected');
		  var z = document.getElementById('gsTotal');
		  z.style.display = 'block';
		  break;
		case 'gsPS':
		  //alert('gsPS selected');
		  var z = document.getElementById('gsPS');
		  z.style.display = 'block';
		  var y = document.getElementById('gsPSTotal');
		  y.checked = true;
		  break;
		case 'gsPW':
		  //alert('gsPW selected');
		  var z = document.getElementById('gsPW');
		  z.style.display = 'block';
		  var y = document.getElementById('gsPWTotal');
		  y.checked = true;
		  break;
		case 'gsH':
		  //alert('gsH selected');
		  var z = document.getElementById('gsH');
		  z.style.display = 'block';
		  var y = document.getElementById('gsHTotal');
		  y.checked = true;
		  break;
		case 'gsU':
		  //alert('gsU selected');
		  var z = document.getElementById('gsU');
		  z.style.display = 'block';
		  var y = document.getElementById('gsUTotal');
		  y.checked = true;
		  break;
		case 'gsT':
		  //alert('gsT selected');
		  var z = document.getElementById('gsT');
		  z.style.display = 'block';
		  var y = document.getElementById('gsTTotal');
		  y.checked = true;
		  break;
		case 'gsL':
		  //alert('gsL selected');
		  var z = document.getElementById('gsL');
		  z.style.display = 'block';
		  var y = document.getElementById('gsLTotal');
		  y.checked = true;
		  break;
		case 'gsF':
		  //alert('gsF selected');
		  var z = document.getElementById('gsF');
		  z.style.display = 'block';
		  var y = document.getElementById('gsFTotal');
		  y.checked = true;
		  break;
		case 'gsM':
		  //alert('gsM selected');
		  var z = document.getElementById('gsM');
		  z.style.display = 'block';
		  var y = document.getElementById('gsMTotal');
		  y.checked = true;
		  break;
		case 'gsSC':
		 // alert('gsSC selected');
		  var z = document.getElementById('gsSC');
		  z.style.display = 'block';
		  break;
		default:
		// alert('default selected');
		 break;
	}
}

function selGovtExpScreenJS(id)
{
	//alert('selGovtExpTEST called');
	// hide all
	var a = document.getElementById('DefinedRange');
	a.style.display = 'none';
	var b = document.getElementById('SpendingBuckets');
	b.style.display = 'none';
	var c = document.getElementById('PerCapitaSpending');
	c.style.display = 'none';
	// show selected
	switch(id)
	{
		case 'DefinedRange':
		  var z = document.getElementById('DefinedRange');
		  z.style.display = 'block';
		  break;
		case 'SpendingBuckets':
		  var z = document.getElementById('SpendingBuckets');
		  z.style.display = 'block';
		  break;
		case 'PerCapitaSpending':
		  var z = document.getElementById('PerCapitaSpending');
		  z.style.display = 'block';
		  break;
		default:
		// alert('default selected');
		 break;
	}
}

function selDemographicsJS(id,idImg)
{
	// hide all
	var a = document.getElementById('demoG');
	var aImg = document.getElementById('demoGimg');
	a.style.display = 'none';
	aImg.src = 'images/demoG.jpg';
	var b = document.getElementById('demoR');
	var bImg = document.getElementById('demoRimg');
	b.style.display = 'none';
	bImg.src = 'images/demoR.jpg';
	var c = document.getElementById('demoA');
	var cImg = document.getElementById('demoAimg');
	c.style.display = 'none';
	cImg.src = 'images/demoA.jpg';
	var d = document.getElementById('demoEd');
	var dImg = document.getElementById('demoEdimg');
	d.style.display = 'none';
	dImg.src = 'images/demoEd.jpg';
	var e = document.getElementById('demoI');
	var eImg = document.getElementById('demoIimg');
	e.style.display = 'none';
	eImg.src = 'images/demoI.jpg';
	var f = document.getElementById('demoEm');
	var fImg = document.getElementById('demoEmimg');
	f.style.display = 'none';
	fImg.src = 'images/demoEm.jpg';
	var g = document.getElementById('demoH');
	var gImg = document.getElementById('demoHimg');
	g.style.display = 'none';
	gImg.src = 'images/demoH.jpg';
	var h = document.getElementById('demoC');
	var hImg = document.getElementById('demoCimg');
	h.style.display = 'none';
	hImg.src = 'images/demoC.jpg';
	var i = document.getElementById('demoQ');
	var iImg = document.getElementById('demoQimg');
	i.style.display = 'none';
	iImg.src = 'images/demoQ.jpg';
	var j = document.getElementById('demoPO');
	var jImg = document.getElementById('demoPOimg');
	j.style.display = 'none';
	jImg.src = 'images/demoPO.jpg';
	//var k = document.getElementById('demoP');
	//var kImg = document.getElementById('demoPimg');
	//k.style.display = 'none';
	//kImg.src = 'images/demoP.jpg';
	
	// show selected
	var z = document.getElementById(id);
	var zImg = document.getElementById(idImg);
	z.style.display = 'block';
	zImg.src = 'images/' + id + '-sel.jpg';

}

</script>

<script>
	$(function() {
		<?php
		require_once('Browser.php');
		$browser = new Browser();
		$detectedBrowser = $browser->getBrowser();
		if( $detectedBrowser == 'Internet Explorer') {
		?>
			$( "#dialog" ).dialog({ width: 460, height: 350, modal: true });
		<?php
		} else {
		?>
			$( "#dialog" ).dialog({ width: 460, height: 200, modal: true });
		<?php
		}
		?>
	});
</script>

<link rel="shortcut icon" href="favicon.ico" /> 

</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<br />
<table width="1190" border="0" align="center" cellpadding="0"  cellspacing="0" bgcolor="#afafaf" style="border: 1px solid #000000;">
<tr>
	<?php
	$affiliateID = $_SESSION['affiliateID'];
    if ($affiliateID == '2') {
        // GovEvents
        echo('<td align="right" valign="middle" colspan="2" background="images/topheader-govevents.jpg" height="65">');
    } else {
		// default
        echo('<td align="right" valign="middle" colspan="2" background="images/topheader2.png" height="65">');
	}
    ?>
    <?php
	include("inc/oldheader.php");
	?>
	</td>
</tr>
<tr align="left" valign="top">
	<td colspan="2" b align="right" valign="middle">
    <?php
	include("inc/oldnav.php");
	?>
	</td>
</tr>
<tr>
<td align="center" colspan="2">
		<table cellpadding="0" cellspacing="0" align="center" width="1125">
		<tr>
			<td>
			<?php
			$selBTN = 'createyourlist';
            include("inc/oldtabs.php");
            ?>
			</td>
		</tr>
		<tr>
			<td width="1125" background="images/canvasbgstrip2.png" valign="top">
			<table cellpadding="0" cellspacing="0" border="0" align="center" width="99%">
			<tr>
				<td width="310" valign="top" bgcolor="#94adc9">
                    <table cellpadding="0" cellspacing="0" width="100%" border="0" align="center">
                    <tr>
                        <td align="center" valign="top">
                        <br><img src="images/step1.png" width="25" height="25" hspace="0" vspace="0" border="0" align="absmiddle"><br>
                        <?php
						if (($_SESSION['logged_in'] == '1') && ($User_Subscription > 1)) {
							// power user
						?>
                            <a style="font-size:10px; font-style:normal;" href="#" class="hintanchor" onMouseOver="showhint('<p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #072263;\'><b><font style=\'color: #b34c2b;\'>Step 1:</font> Selected Desired Officials</b><p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;color: #072263;\'>Select one or more ROLES that you\'re interested in, and then click SUBMIT.<br><b><font style=\'color: #b34c2b;\'>HINT:</font></b> <i style=\'color: #333333;\'>Try modifying your search results by also using the other 5 categories (TYPES, POPULATION, LOCATION, FISCAL YEAR ENDS, and GOVERNMENT SPENDING).  Just remember to click SUBMIT again when you\'re done making your selections.</i></p></p>', this, event, '400px')">more</a>
						<?php
                        } else {
						?>
                            <a style="font-size:10px; font-style:normal;" href="#" class="hintanchor" onMouseOver="showhint('<p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #072263;\'><b><font style=\'color: #b34c2b;\'>Step 1:</font> Selected Desired Officials</b><p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;color: #072263;\'>Select one or more ROLES that you\'re interested in, and then click SUBMIT.<br><b><font style=\'color: #b34c2b;\'>HINT:</font></b> <i style=\'color: #333333;\'>Try modifying your search results by also using the TYPES, POPULATION, and LOCATION categories.  Just remember to click SUBMIT again when you\'re done making your selections.</i></p></p>', this, event, '400px')">more</a>
						<?php
                        }
						?>
                        </td>
                        <td align="left" valign="top">
                        <?php
                        //echo('<form action="search-government.php" name="search" method="post">');
						if ($User_Subscription > 1) {
							echo('<form action="PA-PRG.php" name="search" method="post" onSubmit="return validatePowerSearchForm()">');
						} else {
                        	echo('<form action="PA-PRG.php" name="search" method="post" onSubmit="return validateSearchForm()">');
						}
                        // handle email and roles checkboxes
                        if ($_SESSION['results_GovtOffMustHaveEmails'] == '1') {
                            echo('<input type="hidden" name="off_email" value="govtOff_email">');
                        }
                        if ($_SESSION['results_GovtOffMustMatchRoles'] == '1') {
                            echo('<input type="hidden" name="off_role" value="govtOff_role ">');
                        }
                        include("PA-inc-basicsearch2.php");
                        
                        // if ($_SESSION['logged_in'] != '1') {
                        //     // not logged in
                        //     echo('</form>');	
                        // } else {
                        //     // check if non-power user
                        //     $User_Subscription = $_SESSION['user_subscription'];
                        //     if ($User_Subscription < 2) {
                        //         // registered but not subscriber
                        //         echo('</form>');
                        //     } else {
                        //         include("PA-inc-searchfiscalyear2.php");
                        //     }
                        // }
                        include("PA-inc-searchfiscalyear2.php");
                        include("PA-inc-searchnotes.php");
                        ?>
                        </td>
                    </tr>
                    </table>
				</td>
                <td width="1" bgcolor="#FFFFFF">
                <img src="images/space.gif" width="1" height="1">
                </td>
				<td valign="top" align="left">
                <table cellpadding="0" cellspacing="0" width="97%" border="0" align="center">
                <tr>
                	<td align="center" valign="top">
                    <br><img src="images/step2.png" width="25" height="25" hspace="0" vspace="0" border="0" align="absmiddle"><br>
                    <a style="font-size:10px; font-style:normal;" href="#" class="hintanchor" onMouseOver="showhint('<p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #072263;\'><b><font style=\'color: #b34c2b;\'>Step 2:</font> Review Results</b><p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;color: #072263;\'>Look in the white YOUR SEARCH RESULTS box to see how many officials fit your criteria.<br><b><font style=\'color: #b34c2b;\'>HINT:</font></b> <i style=\'color: #333333;\'>Also look next to the criteria to see the number of officials that match each of your selections (these numbers change after each time you click the SUBMIT button).</i></p></p>', this, event, '400px')">more</a>
                    </td>
                    <td align="left" valign="middle">
      				   <p style="padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #FFFFFF;font-weight: bold;">
                      YOUR SEARCH RESULTS
                      </p>
                    </td>
                  </tr>
                  <tr>
                  	<td align="center" valign="top">
                    <img src="images/space.gif" width="1" height="170" hspace="0" vspace="0" border="0" align="top">
                    <br><img src="images/step3.png" width="25" height="25" hspace="0" vspace="0" border="0" align="absmiddle"><br>
                    <a style="font-size:10px; font-style:normal;" href="#" class="hintanchor" onMouseOver="showhint('<p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #072263;\'><b><font style=\'color: #b34c2b;\'>Step 3:</font> Download Your List</b><p style=\'padding-left:5px;font-family: Arial, Helvetica, sans-serif;color: #072263;\'>When you\'re satisfied with your results, click DOWNLOAD THE RECORDS.<br><b><font style=\'color: #b34c2b;\'>HINT:</font></b> <i style=\'color: #333333;\'>If you plan to download more than 1,000 records, you\'ll save by subscribing to a <a href=PA-Pricing.php>Power Plan</a>.</i></p></p>', this, event, '400px')">more</a>
                    </td>
                    <td align="center" valign="top"> 
                      <table cellpadding="0" cellspacing="0" border="0" align="center" width="98%" bgcolor="#FFFFFF">
                      <tr>
                          <td align="right" valign="top">
                          
                          </td>
                          <td>&nbsp;
                          
                          </td>
                          <td align="right" valign="top">
                          <p class="PAHdr">
                          Compare to entire Power Almanac
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                       </tr>
                       <tr>
                          <td align="right" valign="top">
                          <p class="matchedLarge">
                          Local Government Officials <?php echo($Num_Matched_OfficialsFORMATTED); ?>
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                          <td align="right" valign="top" bgcolor="#dde7f2">
                          <p class="PAMedium">
                          <?php echo($Total_Gov_Officials); ?>
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                       </tr> 
                       <tr>
                          <td align="right" valign="top">
                          <p class="matchedMedium">
                          <b><?php echo($Percentage_Matched_Emails); ?>%</b> have email addresses <b><?php echo($Num_Matched_EmailsFORMATTED); ?></b><br />
                          <b><?php echo($Percentage_Matched_Phone_Numbers); ?>%</b> have phone numbers <b><?php echo($Phone_NumbersFORMATTED); ?></b><br />
                          <b><?php echo($Percentage_Matched_Mailing_Addresses); ?>%</b> have mailing addresses <b><?php echo($Mailing_AddressesFORMATTED); ?></b><br />
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                          <td align="right" valign="top" bgcolor="#dde7f2">
                          <p class="PAMedium">
                          <?php echo($Percentage_PAEmails); ?>%: <?php echo($Total_Available_Emails); ?><br />
                          <?php echo($Percentage_PAPhoneNumbers); ?>%: <?php echo($Total_Phone_Number); ?><br />
                          <?php echo($Percentage_PAMailingAddresses); ?>%: <?php echo($Total_Mailing_Street_Box); ?><br />
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                      </tr>
                      <tr>
                          <td align="right" valign="top">
                          <p class="matchedMedium">
                          <b>Unique Local Governments <?php echo($Num_Matched_GovernmentsFORMATTED); ?></b>
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                          <td align="right" valign="top" bgcolor="#dde7f2">
                          <p class="PAMedium">
                          <?php echo($Total_Governments); ?>
                          </p>
                          </td>
                          <td>&nbsp;
                          
                          </td>
                      </tr>
                      <?php
                      // determine width for preview
                      $userAgent = $_SERVER['HTTP_USER_AGENT'];
                      $pos2 = strpos($userAgent, 'MSIE');
                      if ($pos2 === false) {
                          $selDimensions = 'height=910;width=1100';
                      } else {
                          // MSIE
                          $selDimensions = 'height=910;width=1200';
                      }
                      ?>
                      <tr>
                          <td colspan="4" align="right" valign="baseline">
                          <br>
                          <table cellpadding="3" cellspacing="0" width="100%" border="0" align="center">
                          <?php
						  if ($firstTimeSearch == '1') {
						  ?>
                          <tr>
                              <td width="235" align="left" valign="top">
                              <a href="javascript:alert('Please SUBMIT a search before choosing DOWNLOAD, SAVE, SHARE, ANALYZE, or PREVIEW.');"><img src="images/downloadrecords-btn.png" width="234" height="38" hspace="0" vspace="0" border="0" align="absmiddle"></a>
                              </td>
                              <td align="right" valign="top">
                              <a href="javascript:alert('Please SUBMIT a search before choosing DOWNLOAD, SAVE, SHARE, ANALYZE, or PREVIEW.');"><img src="images/savesearch-btn.png" width="78" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a> <a href="javascript:alert('Please SUBMIT a search before choosing DOWNLOAD, SAVE, SHARE, ANALYZE, or PREVIEW.');"><img src="images/sharesearch-btn.png" width="79" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a> <a href="javascript:alert('Please SUBMIT a search before choosing DOWNLOAD, SAVE, SHARE, ANALYZE, or PREVIEW.');"><img src="images/analyzesearch-btn.png" width="87" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a> <a href="javascript:alert('Please SUBMIT a search before choosing DOWNLOAD, SAVE, SHARE, ANALYZE, or PREVIEW.');"><img src="images/previewsearch-btn.png" width="213" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a>
                              </td>
                          </tr>
                          <?php
						  } else {
						  ?>
                          <tr>
                              <td width="235" align="left" valign="top">
                              <a href="PA-Download.php"><img src="images/downloadrecords-btn.png" width="234" height="38" hspace="0" vspace="0" border="0" align="absmiddle"></a>
                              </td>
                              <td align="right" valign="top">
                              <a href="PA-SaveSearch.php"><img src="images/savesearch-btn.png" width="78" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a> <a href="PA-ShareSearch.php"><img src="images/sharesearch-btn.png" width="79" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a> <a href="PA-Analyze.php"><img src="images/analyzesearch-btn.png" width="87" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a> <a rel="shadowbox;<?php echo($selDimensions); ?>" href="PA-PreviewData.php?uc=<?php echo($useCache); ?>" title=""><img src="images/previewsearch-btn.png" width="213" height="29" hspace="0" vspace="0" border="0" align="absmiddle"></a>
                              </td>
                          </tr>
                          <?php
						  }
						  ?>
                          <tr>
                              <td valign="top" align="left">
                              <?php
                              $numRecords2DL = $_SESSION['lastSearchNumMatched'];
                              if ($_SESSION['logged_in'] != '1') {
                                  // not logged in
                                  $Sub_CostPerDL = 0.95;
                                  $downloadCost = $numRecords2DL * $Sub_CostPerDL;
                                  $amount2Charge = number_format($downloadCost,2);
                              } else {
                                  $User_Email = $_SESSION['user_email'];
                                  $pdo->query(sprintf("CALL GetUserInfo('%s',@JSON_String)", escape($User_Email)));
                                  $pdoObject2 = $pdo->query("SELECT @JSON_String");
                                  $rsArray2 = $pdoObject2->fetchAll();
                                  $json2a = $rsArray2[0]['@JSON_String'];
                                  $jsonArray2a = json_decode($json2a, TRUE);
                                  $User_DL_Reserves = $jsonArray2a['User_DL_Reserves'];
                                  
                                  $User_Subscription = $_SESSION['user_subscription'];
                                  $pdo->query(sprintf("CALL GetSubscriptionInfo(%s,@JSON_String)", escape($User_Subscription)));
                                  $pdoObject3 = $pdo->query("SELECT @JSON_String");
                                  $rsArray3 = $pdoObject3->fetchAll();
                                  $json3 = $rsArray3[0]['@JSON_String'];
                                  $jsonArray3 = json_decode($json3, TRUE);
                                  $Sub_CostPerDL = $jsonArray3['Sub_CostPerDL'];
                                  if ($numRecords2DL <= $User_DL_Reserves) {
                                      $amount2Charge = '0.00';
                                  } else {
                                      $deltaRecords = $numRecords2DL - $User_DL_Reserves;
                                      $downloadCost = $deltaRecords * $Sub_CostPerDL;
                                      $amount2Charge = number_format($downloadCost,2);
                                  }
                              }
                              ?>
                              <?php
                              // IE does not set searchbtn
                              

								$User_Subscription = $_SESSION['user_subscription'];
                              	//Show for all users
                              	 echo("<p><a href='#' class='hintanchor' onMouseOver=\"showhint('If you have enough download credits in your account to download the full set of search results, then the fee shown will be $0.00.    Otherwise, the fee is the cost of purchasing the additional download credits you need to download these records.  Alternatively, you can upgrade your account to add download credits (and to reduce your cost <u>per</u> download credit).', this, event, '500px')\"><img src='images/help-blue.png' alt='' width='12' height='12' border='0' title=''></a><a href='PA-Download.php' class='textUL'>Fee to download the records?</a><br>");
                                      if ($amount2Charge > 0.00) {
                                          echo("<a href='PA-Upgrade.php?m=$User_Subscription'>Upgrade now and save 20%+</a></p>");
                                      }



                              ?>
                              </td>
                              <td valign="top" align="left">
                              <a href="#" class="hintanchor" onMouseOver="showhint('&nbsp;Toggle this to &quot;ON&quot; if you want your search results to only include government officials with an email address.', this, event, '500px')"><img src="images/help-blue.png" alt="" width="12" height="12" border="0" title=""></a>
                              <?php
                              if ($_SESSION['results_GovtOffMustHaveEmails'] == '0') {
                                  //echo('Government Officials without EMails (click button to toggle) <a href="PA-filteremails.php?off_email=1"><buttonMailCheck>INCLUDED</buttonMailCheck></a>');
                                  //echo('<a href="PA-filteremails.php?off_email=1"><b>INCLUDED</b></a>: government officials without emails');
                                  echo('<a href="PA-filteremails.php?off_email=1"><b>OFF</b></a>: only include government officials with an email address');
                              } else {
                                  //echo('Government Officials without EMails (click button to toggle) <a href="PA-filteremails.php?off_email=0"><buttonMailUnCheck>EXCLUDED</buttonMailUnCheck></a>');
                                  //echo('<a href="PA-filteremails.php?off_email=0"><b>EXCLUDED</b></a>: government officials without emails');
                                  echo('<a href="PA-filteremails.php?off_email=0"><b>ON</b></a>: only include government officials with an email address');
                              }
                              ?>
                              <br><br>
                              <a href="#" class="hintanchor" onMouseOver="showhint('&nbsp;Toggle this to &quot;ON&quot; if you want at least one official for every government that matches your search criteria.   Let\'s say you search for Clerks for all governments in Alabama.  With this &quot;OFF&quot;, that\'s what you\'ll get.  For the Alabama governments with no Clerk, you\'ll get no records.  If you turn this &quot;ON&quot;, then we\'ll add to your results the Top Elected Official for the governments in Alabama <u>without</u> a Clerk.  This is a way to fill in the &quot;gaps&quot; so every local government you want is covered.', this, event, '500px')"><img src="images/help-blue.png" alt="" width="12" height="12" border="0" title=""></a>
                              <?php
                              // show this only when SearchSummary returns OUT top_elected_flag to true
                              
                              if ($_SESSION['results_GovtOffMustMatchRoles'] == '0') {
                                  //echo('Government without matching Roles (click button to toggle) <a href="PA-filterroles.php?off_role=1"><buttonRoleCheck>INCLUDED</buttonRoleCheck></a>');
                                  //echo('<a href="PA-filterroles.php?off_role=1"><b>INCLUDED</b></a>: "Top Elected Official" for governments with no other matching role');
                                  echo('<a href="PA-filterroles.php?off_role=1"><b>ON</b></a>: include at least one government official per matched government');
                              } else {
                                  //echo('Government without matching Roles (click button to toggle) <a href="PA-filterroles.php?off_role=0"><buttonRoleUnCheck>EXCLUDED</buttonRoleUnCheck></a>');
                                  //echo('<a href="PA-filterroles.php?off_role=0"><b>EXCLUDED</b></a>: "Top Elected Official" for governments with no other matching role');
                                  echo('<a href="PA-filterroles.php?off_role=0"><b>OFF</b></a>: include at least one government official per matched government');
                              }
                              //echo("<p>Results: SearchSummaryFLAG = $SearchSummaryFLAG</p>");
                              ?>
                              </td> 
                          </tr>
                          </table>
                          <br>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="4" align="left" valign="top">
                          
                          </td>
                      </tr>
                      </table>
                    </td>
                </tr>
                </table>
                
                <?php
				
				// determine width for states
				$userAgent = $_SERVER['HTTP_USER_AGENT'];
				$pos = strpos($userAgent, 'MSIE');
				if ($pos === false) {
					$selWidth = '650';
					$positioning = 'absolute';
				} else {
					// MSIE
					$selWidth = '700';
					$positioning = 'relative';
				}

				
					include("PA-inc-powersearch.php");
						echo("</form>");	
                ?>
				</td>
			</tr>
			</table>
			</td>
		</tr>
 		<tr>
			<td width="1125" background="images/canvasbgbotstrip2.png" valign="top">&nbsp;
            
            </td>
        </tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
    <?php
    echo("DEBUG: searchParams = $searchParams<br>");
    var_dump($_SESSION);
    ?>
    </td>
</tr>
<tr>
	<td colspan="2">
    <br />
    <?php
	include("PA-inc-specificsearch.php");
	?>
	</td>
</tr>
<tr>
	<td colspan="2" align="center" bgcolor="#ebebeb">
    <?php
	include("inc/oldfooter.php");
	?>
    </td>
</tr>
</table>
<br />
</body>
</html>
