<?php
include("inc/config.php");
$title = 'Local Gov Directory | Power Almanac';
include("inc/officialsArray.php");
include("inc/header-search.php");
?>
<!-- welcome -->
            <div class="welcome deltek">
                <div class="welcome-holder">                    
                    <div class="intro-holder">
                        <h1>We Specialize in Local.</h1>
                    </div>
                    <p>Download all the contact information you need<br />
                        to market to <strong><em>Local Government Officials</em></strong>.</p>
                    <form action="PA-PRG.php" method="post" class="form-search">
                        <fieldset>
                            <strong class="title">Who Are You Looking For? <img src="images/ico01.png" width="29" height="30" alt="" /></strong>
                          <?=$officialDrop ?>
                            <div class="btn-blue">
                                <div class="btn-frame">
                                    <input type="submit" value="Search &#187;" class="submit" />
                                </div>
                            </div>
                            <span class="pointer">Start Now</span>
                        </fieldset>
                    </form>
                </div>
            </div>

            <div class="main-holder">
                <!-- white box -->
                <div class="white-box">
                    <div class="holder">
                        <div class="frame">
                            <div class="heading">
                                <div class="holder">
                                    <h2>How do you easily market to the right local government officials?</h2>
                                </div>
                                <div class="form-holder">
                                    <strong class="title">Try the Search</strong>
                                    <form action="PA-PRG.php" method="post" class="form-search">
                                        <fieldset>
                                            <?=$officialDrop ?>
                                            <div class="btn-blue">
                                                <div class="btn-frame">
                                                    <input type="submit" value="Go" class="submit" />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>                           
                                </div>
                            </div>
                            <div class="navbar">
                              <div class="menu-holder">
                                <div class="container">                                  
                                    <ul class="content-menu nav">
                                        <li><a href="#searchTarget"><span>Search and Target</span></a></li>
                                        <li><a href="#contactInfo"><span>Download Contact Info</span></a></li>
                                        <li><a href="#analyzeResults"><span>Analyze Results</span></a></li>                                    
                                        <li><a href="#solveProblems"><span>Solve Your Problems</span></a></li>
                                    </ul>
                                    <!-- <a href="#" class="btn-to-top btn-gray"><span><em>Back to Top</em></span></a> -->
                                </div>
                              </div>
                            </div>
                           <!--  <div class="navbar" id="hoverMenu">                                
                                <div class="menu-holder">
                                    <div class="container">                                                                
                                       
                                    </div>
                                </div>
                            </div> -->
                            <!-- post -->                            
                            <div class="post" id="searchTarget">
                                <img src="images/img02.jpg" width="484" height="410" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>Role-Based Search</h3>
                                    <strong class="subheading">Find the right officials</strong>
                                    <h4>9 Different Local Government Official Roles</h4>
                                    <div class="columns-list">
                                        <ul>
                                            <li>City/County Manager</li>
                                            <li>Head of Purchasing</li>
                                            <li>Head of Public Works</li>
                                        </ul>
                                        <ul>
                                            <li>Fire Chief</li>
                                            <li>Head Clerk</li>
                                            <li>Mayor</li>
                                        </ul>
                                        <ul>
                                            <li>Head of Finance</li>
                                            <li>Police Chief/Sheriff</li>
                                            <li>Council Member</li>
                                        </ul>
                                    </div>
                                    <p>For example, you want to reach the person in charge of it all, but every municipality calls him something different. Is it the Mayor, Village President, Town President, Council President, Town Supervisor or the Chairperson of the Board? Who knows? We do.</p>
                                    <p>We categorize <strong>each official</strong> into roles, so if you search for Mayor, you will get everyone in that role, no matter what their title may be. We also give you their <strong>exact title</strong> so you can personalize your outreach.</p>
                                    <form action="PA-PRG.php" method="post" class="form-search">
                                        <fieldset>
                                            <strong class="title">Try it! It’s that easy.</strong>
                                           <?=$officialDrop ?>
                                            <div class="btn-blue">
                                                <div class="btn-frame">
                                                    <input type="submit" value="Go"  class="submit"  />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post">
                                <img src="images/img03.jpg" width="484" height="327" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>Precise Targeting</h3>
                                    <strong class="subheading">Get an exact match to your target market.</strong>
                                    <h4>Zoom in. Tight.</h4>
                                    <ul>
                                        <li>Narrow <strong>location</strong> by state, region,  or zipcode+distance.</li>
                                        <li>Filter by Population</li>
                                        <li>Choose city, county, or township. </li>
                                        <li>
                                            Target specific government <strong>spending demographics</strong> like
                                            <ul>
                                                <li>Total and per capita spending,</li>
                                            
                                                <li>Fiscal year endings, and</li>
                                                <li>Percentages spent on categories like public safety, health, welfare, utilities, transportation, leisure, finance, etc...</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post" id="contactInfo">
                                <img src="images/img04.jpg" width="484" height="426" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>Download Contact Info</h3>
                                    <strong class="subheading">Quickly get the results you need.</strong>
                                    <h4>Phone Numbers, Emails, and Addresses</h4>
                                    <p>It’s time to throw some numbers out. Our database spans <strong>21,000 unique local governments</strong>, giving you access to local government decision-makers from <strong>97%</strong> of cities, counties, and townships with a population greater than 1,000. With over <strong>217,000 officials</strong> listed, there’s probably no one you can’t find.</p>
                                    <h4>Not Your Grandma’s Attic</h4>
                                    <p>This is NOT a compilation of information from other, inadequate sources. We've built - from scratch - and continuously update our own comprehensive list of local government officials and their contact info.<br /> <a href="/PA-Features.php" class="link-more">Find Out More</a></p>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post last-post" id="analyzeResults">
                                <img src="images/img05.jpg" width="484" height="386" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>Make Good Decisions</h3>
                                    <strong class="subheading">The intelligence you need to make good decisions.</strong>
                                    <ul>
                                        <li>Ascertain <strong>exactly</strong> who is most likely to buy your product.</li>
                                        <li>Customize your search to produce the results you’re looking for.</li>
                                        <li>Save and Export your data to <strong>Excel instantly</strong>.</li>
                                        <li>
                                            Narrow your campaigns by filtering the data by
                                            <ul>
                                                <li>Roles of the officials</li>
                                                <li>Population</li>
                                                <li>Location</li>
                                                <li>Types of local government</li>
                                                <li>Spending categories</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- subfooter -->
                            <div class="subfooter">
                                <strong class="title">Need more convincing? Try Power Almanac</strong>
                                <a href="/search-government.php?ns=1" class="btn-search btn-gray"><span>Search for Officials</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- white box -->
                <div class="white-box" id="solveProblems">
                    <div class="holder">
                        <div class="frame">
                            <div class="heading">
                                <h2>Solve Your Marketing List Problems</h2>
                                <span class="note">Don’t waste your time. Get in contact with <em><strong>the right local government officials</strong></em> using highly targeted search criteria and accurate contact information.</span>
                            </div>
                            <!-- post -->
                            <div class="post">
                                <img src="images/img06.jpg" width="417" height="313" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>1. I don’t want to waste time contacting the wrong people.</h3>
                                    <h4>Freakishly Accurate</h4>
                                    <p>Don’t worry. We <strong>triple verify all records</strong> before they go into the Power Almanac, so you can be confident that the phone numbers, email addresses, names, titles, roles, governments, websites, and more are all correct.</p>
                                    <h4>Obsessively Up-to-Date</h4>
                                    <p>Government officials often change because they quit, get fired, or don’t get re-elected. They may also change because of elections. We rigorously verify every record in the Power Almanac <strong>twice per year</strong>, updating and adding new records weekly.</p>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post">
                                <img src="images/img07.jpg" width="415" height="419" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>2. I don’t have a million dollars.</h3>
                                    <strong class="subheading">And if I did, I would just buy a Superbowl ad.</strong>
                                    <h4>Priced for Everyone</h4>
                                    <p>Get your first 50 records FREE. Additional records start at 95¢ and go as low as <strong>4¢ per record</strong> when you purchase the <a href="/PA-Pricing.php">Power +</a> unlimited plan. Spend the rest of your money on something you love, like improving your product.</p>
                                    <h4>Free Updates for Subscribers</h4>
                                    <p>When we update our records, you don’t have to pay again to download! Just <strong>pay once</strong> and have access to constantly updated information. </p>
                                    <h4>Unlimited Usage - Like a boss!</h4>
                                    <p>Other sources make you pay each time you use their list. Some won't even give you emails at all - you have to pay them extra to send the emails to you. Once you download a record from us, you can use the contact info as many times as you'd like for as long as you have your subscription.</p>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post">
                                <img src="images/img08.jpg" width="416" height="397" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>3. I don’t have all day to<br /> search for contacts.</h3>
                                    <strong class="subheading">Who do you think I am? Indiana Jones?</strong>
                                    <h4>So Easy a Caveman Could Do It</h4>
                                    <p>Custom search based on the types of officials you need and <strong>get an Excel spreadsheet</strong> that you can easily use to contact officials however you choose.</p>
                                    <h4>Stupid Fast</h4>
                                    <p>Get records instantly at the click of a button.<br /> Adjust parameters on the fly and <strong>receive results fast</strong>.</p>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post last-post">
                                <img src="images/img09.jpg" width="413" height="379" alt="" class="alignleft" />
                                <div class="txt">
                                    <h3>4. I want to make my own list the way I want it!</h3>
                                    <h4>DIY Customizable</h4>
                                    <p>Search only for the officials you need. The Power Almanac allows you to focus your seach to <em><strong>market to your exact audience</strong></em>. You don’t pay for records you don’t need.</p>
                                    <h4>Chock Full Comprehensive</h4>
                                    <p>We have contact info for over <em><strong>217,000</strong></em> local government officials including the Mayor, Council Members, Public Works Director, Police Chief, Fire Chief, Head of Finance, Head of Purchasing, City/County Manager &amp; Head Clerk for <em><strong>97%</strong></em> of cities over 1000 people.</p>
                                </div>
                            </div>
                            <!-- subfooter -->
                            <div class="subfooter">
                                <strong class="title">Need more convincing? Try Power Almanac</strong>
                                <a href="/search-government.php?ns=1" class="btn-search btn-gray"><span>Search for Officials</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




<?php include("inc/footer.php"); ?>