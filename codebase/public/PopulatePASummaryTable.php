<?php
error_reporting(E_ALL);

$dbhost = 'localhost';
$dbuser = PowerAlmanac\Config::env('mysql_user');
$dbpass = 'powerAlmanac2011';
$dbname = PowerAlmanac\Config::env('mysql_name');
		
PowerAlmanac\PDb::connect($dbhost,$dbuser,$dbpass) or die(PowerAlmanac\PDb::error());
PowerAlmanac\PDb::select_db($dbname) or die(PowerAlmanac\PDb::error());

$timeIs = time();
$tableID = 'NickPA_' . substr($timeIs,-6);
$searchParams = 'byText=&';

$pdo2 = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);
$pdo2->query(sprintf("CALL SearchSummary('%s','%s',@JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@table_exists)", escape($tableID), escape($searchParams)));
$pdoObject = $pdo2->query("SELECT @JSON_SUMMARY,@JSON_GOVERNMENTS,@JSON_OFFICIALS,@flag,@table_exists");
$rsArray = $pdoObject->fetchAll();
//print_r($rsArray); exit;
$JSON_SUMMARY = $rsArray[0]['@JSON_SUMMARY'];
$JSON_GOVERNMENTS = $rsArray[0]['@JSON_GOVERNMENTS'];
$JSON_OFFICIALS = $rsArray[0]['@JSON_OFFICIALS'];
$flag = $rsArray[0]['@flag'];
$table_exists = $rsArray[0]['@table_exists'];
$JSON_SUMMARY_Array = json_decode($JSON_SUMMARY, TRUE);
$JSON_GOVERNMENTS_Array = json_decode($JSON_GOVERNMENTS, TRUE);
$JSON_OFFICIALS_Array = json_decode($JSON_OFFICIALS, TRUE);

$Num_Matched_Officials = $JSON_SUMMARY_Array['Num_Matched_Officials'];
$Num_Matched_Governments = $JSON_SUMMARY_Array['Num_Matched_Governments'];
$Num_Matched_Emails = $JSON_SUMMARY_Array['Num_Matched_Emails'];
$Phone_Numbers = $JSON_SUMMARY_Array['Phone_Numbers'];
$Mailing_Addresses = $JSON_SUMMARY_Array['Mailing_Addresses'];

//print_r($JSON_GOVERNMENTS);
//print_r($JSON_OFFICIALS);

// get preview data
$pdo2->query(sprintf("CALL SearchDetails(25,'%s',@Result)", escape($tableID)));
$pdoObject = $pdo2->query("SELECT @Result");
$rsArray = $pdoObject->fetchAll();
$JSON_PREVIEW = addslashes($rsArray[0]['@Result']);


// update database
$update_sql = sprintf("UPDATE database_summary SET Total_Available_Emails = '%s', Total_Gov_Officials = '%s', Total_Phone_Number = '%s', Total_Mailing_Street_Box = '%s', Total_Governments = '%s',  JSON_GOVERNMENTS = '%s', JSON_OFFICIALS = '%s', JSON_PREVIEW = '%s'
", escape($Num_Matched_Emails), escape($Num_Matched_Officials), escape($Phone_Numbers), escape($Mailing_Addresses), escape($Num_Matched_Governments), escape($JSON_GOVERNMENTS), escape($JSON_OFFICIALS), escape($JSON_PREVIEW));
$result_update = @PowerAlmanac\PDb::query($update_sql);
if (!$result_update) {
	die('<p>Error updating database_summary: '. PowerAlmanac\PDb::error().'</p>');
}

// remove temp tables
$pdo2->query(sprintf("CALL Delete_Research_Tables('%s')", escape($tableID)));

echo("DONE!");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">