<?php include('inc/config.php') ?>
<?php include('inc/officialsArray.php') ?>
<!DOCTYPE html>
<html>
	<head>
		<title>404: Couldn't find it! - Power Almanac</title>
		<meta name="robots" content="noindex">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/404.css">
	</head>
	<body>
		<div id="main404">
			<h1>404: The page you're looking for is broken.</h1>
			<h3 class="ital">We are so sorry for the inconvenience.</h3>
			<aside>
				<h3>Need to search again?<br>
				Go right ahead.</h3>
				<form action="PA-PRG.php" method="post" id="selectRole">
					<?=$officialDrop ?>
					<button>GO</button>
				</form>
			</aside>
			<h4>or try one of these pages:</h4>
			<ul>
				<li><a href="/">home page</a></li>
				<li><a href="/dashboard">member account</a></li>
				<li><a href="/search-government">search government officials</a></li>
			</ul>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				$("#selectRole").submit(function(){
					var sel = $("[name='official_role'] option:selected",this);
					var value = sel.attr("value");
					var name = sel.attr("data-name");
					 var input = '<input type="hidden" name="' + name + '" value="' + value + '" />';
					 $(this).append(input);
				});
			})
		</script>
	</body>
</html>