<?php

include("inc/config.php");

$pdo = new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpass);

$email = "marc@blendimc.com,jon@rhinedesign.biz";
$header = "";
$emailtext = "";

$txn_id = 'NA';
$mc_gross = 0.00;
$dl_Credits = 0;
$user_id = 0;

// STEP 1: read POST data
 
// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
// Instead, read raw POST data from the input stream. 
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);

$myPost = array();
foreach ($raw_post_array as $keyval) {
  $keyval = explode ('=', $keyval);
  if (count($keyval) == 2)
     $myPost[$keyval[0]] = urldecode($keyval[1]);
}

mail('jon@rhinedesign.biz', "DEBUG IPN", serialize($raw_post_array), null, '-fpostmaster@poweralmanac.com');

$sandbox = true;

// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
   $get_magic_quotes_exists = true;
} 
foreach ($myPost as $key => $value) {        
   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
        $value = urlencode(stripslashes($value)); 
   } else {
        $value = urlencode($value);
   }
   $req .= "&$key=$value";
}

if ($sandbox) {
	// sandbox
	$paypalProcessor = 'https://www.sandbox.paypal.com';
	$verifiedMessageHdr = 'Sandbox-VERIFIED IPN';
	$invalidMessageHdr = 'Sandbox-INVALID IPN';
	$receiverEmail = 'jon@blendimc.com';

} else {

 	// live
 	$paypalProcessor = 'https://www.paypal.com';
 	$verifiedMessageHdr = 'Live-VERIFIED IPN';
 	$invalidMessageHdr = 'Live-INVALID IPN';
 	$receiverEmail = 'ron.mester@ltbl.com';
 }
 
// Step 2: POST IPN data back to PayPal to validate
 
$ch = curl_init($paypalProcessor . '/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
 
// In wamp-like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set 
// the directory path of the certificate as shown below:

curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');

$res = curl_exec($ch);
if( !$res ) {
	$msg = "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) . "\n" . 'response: ' . $res;
	 mail('jon@rhinedesign.biz', "DEBUG IPN", $msg , null, '-fpostmaster@poweralmanac.com');
	mail('jon@rhinedesign.biz', "DEBUG IPN", 'no response from curl', null, '-fpostmaster@poweralmanac.com');
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
curl_close($ch);

//check localhost so we can post data to test transactions

// Process validation from PayPal
// TODO: This sample does not test the HTTP response code. All
// HTTP response codes must be handles or you should use an HTTP
// library, such as cUrl
mail('jon@rhinedesign.biz', "DEBUG IPN", 'return: ' . $res, null, '-fpostmaster@poweralmanac.com');
if (strcmp ($res, "VERIFIED") == 0) {

// TODO:
// Check the payment_status is Completed
// Check that txn_id has not been previously processed
// Check that receiver_email is your Primary PayPal email
// Check that payment_amount/payment_currency are correct
// Process payment
// If 'VERIFIED', send an email of IPN variables and values to the
// specified email address

foreach ($_POST as $key => $value)
{
	$emailtext .= $key . " = " .$value ."\n\n";
	switch($key) {
		case 'txn_id':
			$txn_id = $value;
			break;
		case 'receiver_email':
			$receiver_email = $value;
			break;
		case 'payment_status':
			$payment_status = $value;
			break;
		case 'txn_type':
			$txn_type = $value;
			break;
		case 'mc_gross':
			$mc_gross = $value;
			break;
		case 'mc_currency':
			$mc_currency = $value;
			break;
		case 'custom':
			$custom = $value;
			// $User_ActivationCode . ',' . $_SESSION['RegUser_ID'] . ',' . $subscriptionLevel . ',' . $totalNewCredits;
			list($User_ActivationCode,$user_id,$subscription,$dls) = explode(",",$custom);
			break;
		default:
			break;
	}
}
	//only run if payment went through, not for refunds or reverses
	if($payment_status == "Completed")
	{
		// process
		mail($email, "DEBUG: All IPN", "$User_ActivationCode,$user_id,$subscription,$dls \n", null, '-fpostmaster@poweralmanac.com');

		//set how much to extend subscription date if a user has upgraded or renewed, set new session variables
		$sql = "SELECT User_Subscription, DateTime_SubscriptionEnd FROM registeredusers WHERE RegUser_ID = :user_id LIMIT 1";
		$vars = array(
			"user_id" => $user_id
			);

		$stm = $pdo->prepare($sql);
		$stm->execute($vars);

		while ($row = $stm->fetch()) {
		    $old_sub = $row['User_Subscription'];
		    $old_end = $row['DateTime_SubscriptionEnd'];
		}

		//if upgrade or downgrade, extend subscription date by a year
		if($old_sub != $subscription)
		{	
			// all subscriptions are 6 month length from time of renewal except for unlimited
			if($subscription == 10){
				$months = 12;
			}
			else{
				$months = 6;
			}

			//right now we're setting subscription time to today's date plus the number of months on subscription
			$date = new DateTime();
			$date->add(new DateInterval("P" . $months ."M"));
			$dateend = $date->format('Y-m-d H:i:s');
		}
		else
		{
			// if they haven't upgraded, no new date for renewal
			$dateend = $old_end;
		}
		// we add the set amount to their timefrane (adding value instead of overwriting new value because of paypal ipn processing time)
		$sql = "UPDATE registeredusers SET
			User_Subscription = :subscription,
			User_DL_Reserves = User_DL_Reserves + :dls,
			DateTime_SubscriptionEnd = :dateend
			WHERE RegUser_ID = :user_id LIMIT 1";
		$vars = array(
			"subscription" => $subscription,
			"dls" => $dls,
			"user_id" => $user_id,
			"dateend" => $dateend
		); 

		$stm = $pdo->prepare($sql);
		$stm->execute($vars);

		if($user_id && $dateend){ // make sure data is passed in so we don't assign all 0 parent users a nonexistent date
			// update any subusers' subscription end time
			$sqlSubUsers = "UPDATE registeredusers SET DateTime_SubscriptionEnd = :dateend, User_Subscription = :subscription WHERE User_Parent = :parentId";
		    $vars = array(
				"parentId" => $user_id,
				"subscription" => $subscription,
				"dateend" => $dateend
			);
			$stm = $pdo->prepare($sqlSubUsers);
		    $stm->execute($vars);
		}
	}
} else if (strcmp ($res, "INVALID") == 0) {			
	// If 'INVALID', send an email. TODO: Log for manual investigation.
	foreach ($_POST as $key => $value) 
	{
		$emailtext .= $key . " = " .$value ."\n\n";
	}
	mail($email, "$invalidMessageHdr", $emailtext . "\n\n" . $req);
}
?>