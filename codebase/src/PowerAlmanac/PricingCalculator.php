<?php

namespace PowerAlmanac;

class PricingCalculator
{
    /**
     * @param array $subscription - must be a full select from the Subscriptions table
     * @param int $totalQuantity
     * @param int $existingCredits
     * @param int $currentSubscriptionId - Exclude the subscription cost and reserves for this particular subscription, this is the subscription they're on
     *
     * @return int
     */
    public static function calculateCost(array $subscription, $totalQuantity=0, $existingCredits=0, $currentSubscriptionId=null)
    {
        $owned = ($subscription['Subscriptions_ID'] == $currentSubscriptionId) ? true : false;
        $quantityFromSubscription = ($owned) ? 0 : (int) $subscription['Sub_NumReserves'];
        $quantityToPurchase = $totalQuantity - ($quantityFromSubscription + $existingCredits);

        $totalCost = ($owned) ? 0 : (int)$subscription['Sub_AnnualFees'];
        if ($quantityToPurchase > 0) {
            $totalCost += ($quantityToPurchase * (float) $subscription['Sub_CostPerDL']);
        }

        return $totalCost;
    }

    /**
     * @param array $subscriptions
     * @param int $totalQuantity
     * @param int $existingCredits
     * @param null $currentSubscriptionId - Exclude the subscription cost and reserves for this particular subscription, this is the subscription they're on
     * @return array
     */
    public static function lowestCostSubscription(array $subscriptions, $totalQuantity=0, $existingCredits=0, $currentSubscriptionId=null)
    {
        $lowestCost = false;
        $lowestCostSubscription = [];

        foreach ($subscriptions as $subscriptionId => $subscription) {
            $cost = self::calculateCost($subscription, $totalQuantity, $existingCredits, $currentSubscriptionId);
            if ($lowestCost === false || $cost < $lowestCost) {
                $lowestCost = $cost;
                $lowestCostSubscription = $subscription;
            }
        }

        return [$lowestCost, $lowestCostSubscription];
    }

    public static function bestPrice($quantity=0, $currentSubscriptionId=0, $existingCredits=0)
    {
        $currentSubscription = PDb::query_assoc('SELECT * FROM Subscriptions WHERE Subscriptions_ID=' . PDb::escape($currentSubscriptionId));

        $conditionExcludeSubscription = ($currentSubscriptionId) ? ' OR Sub_Level >= ' . $currentSubscription['Sub_Level'] : '';
        $allSubscriptions = PDb::query_assoc_all('SELECT * FROM Subscriptions WHERE Active=1 ORDER BY Sub_NumReserves ASC', 'Subscriptions_ID');
        $purchasableSubscriptions = PDb::query_assoc_all('SELECT * FROM Subscriptions WHERE Active=1 AND Purchasable=1 AND (Sub_Level=10' . $conditionExcludeSubscription . ') ORDER BY Sub_NumReserves ASC', 'Subscriptions_ID');

        if ($currentSubscription['Sub_Level'] == 10) {
            return [
                'upgradingSubscription' => 0,
                'newSubscriptionCost' => 0,
                'bestSubscription' => $currentSubscription,
                'extraCreditsCost' => 0,
                'totalPrice' => 0,
                'costPerRecord' => 0,
                'leftover' => 'N/A',
                'totalNewCredits' => 0,
                'newCredits' => 0,
                'payForCreditsSeparately' => 0,
                'existingCreditsBeforePurchase' => $existingCredits,
            ];
        }

        list($lowestCost, $lowestCostSubscription) = self::lowestCostSubscription($purchasableSubscriptions, $quantity, $existingCredits);
        list($lowestCostUpgrade, $lowestCostSubscriptionUpgrade) = self::lowestCostSubscription($purchasableSubscriptions, $quantity, $existingCredits, $currentSubscriptionId);

        $upgrading = false;
        // Pay as you go or Power Entry subscription, so they have to upgrade to a subscription, or pay per record
        if ($currentSubscription['Sub_NumReserves'] == 0) {
            // Use Lowest cost and lowest cost subscription
            $upgrading = ($lowestCostSubscription['Subscriptions_ID'] != $currentSubscriptionId);
        } else {
            //Use lowest cost upgrade
            $upgrading = ($lowestCostSubscriptionUpgrade['Subscriptions_ID'] != $currentSubscriptionId);
            $lowestCostSubscription = $lowestCostSubscriptionUpgrade;
            $lowestCost = $lowestCostUpgrade;
        }

        $bundledRecords = ($upgrading) ? $lowestCostSubscription['Sub_NumReserves'] + $existingCredits : $existingCredits;
        $payForCreditsSeparately = ($quantity > $bundledRecords) ? $quantity - $bundledRecords : 0;

        //calculate pricing
        $costPerRecord = round($lowestCost / $quantity, 2);
        $leftover = $bundledRecords >= $quantity ? $bundledRecords - $quantity : 0;
        $totalNewCredits = $lowestCostSubscription['Sub_NumReserves'] + $existingCredits + $payForCreditsSeparately;
        $newCredits = ($upgrading) ? $lowestCostSubscription['Sub_NumReserves'] + $payForCreditsSeparately : $payForCreditsSeparately;

        return [
            'upgradingSubscription' => $upgrading,
            'bestSubscription' => $lowestCostSubscription,
            'newSubscriptionCost' => $upgrading ? $lowestCostSubscription['Sub_AnnualFees'] : 0,
            'extraCreditsCost' => $payForCreditsSeparately * $costPerRecord,
            'totalPrice' => $lowestCost,
            'costPerRecord' => $costPerRecord,
            'leftover' => $leftover,
            'totalNewCredits' => $totalNewCredits,
            'newCredits' => $newCredits,
            'payForCreditsSeparately' => $payForCreditsSeparately,
            'existingCreditsBeforePurchase' => $existingCredits,
        ];
    }
}

if (php_sapi_name() == 'cli') {
    require_once __DIR__ . '/../../public/inc/config.php';
    print_r(PricingCalculator::bestPrice(18356, 103, 11166));
}
