<?php

namespace PowerAlmanac;

/**
 * Class PDb
 * @package PowerAlmanac
 */
class PDb
{
    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @var null
     */
    private static $instanceIcube = null;

    /**
     * @return \mysqli|null
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            $host = Config::env('mysql_host');
            $user = Config::env('mysql_user');
            $pass = Config::env('mysql_pass');
            $name = Config::env('mysql_name');

            self::$instance = mysqli_connect($host, $user, $pass, $name);
            if (!self::$instance) {
                die('Failed to connect to database');
            }
        }

        return self::$instance;
    }

    /**
     * @return \mysqli|null
     */
    public static function getInstanceIcube()
    {
        if (is_null(self::$instanceIcube)) {
            $host = Config::env('mysql_host');
            $user = Config::env('mysql_user');
            $pass = Config::env('mysql_pass');
            $name = Config::env('mysql_name_icube');

            self::$instanceIcube = mysqli_connect($host, $user, $pass, $name);
            if (!self::$instanceIcube) {
                die('Failed to connect to database');
            }
        }

        return self::$instanceIcube;
    }

    /**
     * @param $result
     * @return int
     */
    public static function num_rows($result)
    {
        return mysqli_num_rows($result);
    }

    /**
     * @return \mysqli|null
     */
    public static function connect()
    {
        return self::getInstance();
    }

    /**
     * @return \mysqli|null
     */
    public static function connectIcube()
    {
        return self::getInstanceIcube();
    }

    /**
     * @return string
     */
    public static function error()
    {
        return mysqli_error(self::getInstance());
    }

    /**
     * @param $query
     * @return bool|\mysqli_result
     */
    public static function query($query)
    {
        return mysqli_query(self::getInstance(), $query);
    }

    /**
     * @param $result
     * @return array|null
     */
    public static function fetch_array($result)
    {
        return mysqli_fetch_array($result);
    }

    /**
     * @param $result
     * @return array
     */
    public static function fetch_assoc_all($result)
    {
        $results = [];

        while ($row = mysqli_fetch_assoc($result)) {
            $results[] = $row;
        }

        return $results;
    }

    /**
     * @param $query
     * @return array|bool
     */
    public static function query_assoc($query)
    {
        $result = self::query($query);
        if (!$result) {
            return false;
        }

        return mysqli_fetch_assoc($result);
    }

    /**
     * @param $query
     * @param $keyBy - which array key should we key by?
     * @return array|bool
     */
    public static function query_assoc_all($query, $keyBy=null)
    {
        $results = [];
        $result = self::query($query);
        if (!$result) {
            return false;
        }

        $results = self::fetch_assoc_all($result);
        if (!$keyBy || empty($results)) {
            return $results;
        }

        if (!array_key_exists($keyBy, $results[0])) {
            throw new \Exception('Please key by a key that exists');
        }

        return array_column($results, null, $keyBy);
    }

    /**
     * @param $result
     * @return array|null
     */
    public static function fetch_assoc($result)
    {
        return mysqli_fetch_assoc($result);
    }

    /**
     * @param $name
     * @return bool
     */
    public static function select_db($name)
    {
        return mysqli_select_db(self::getInstance(), $name);
    }

    /**
     * @param $string
     * @return string
     */
    public static function real_escape_string($string)
    {
        return mysqli_real_escape_string(self::getInstance(), $string);
    }

    /**
     * @param $string
     * @return string
     */
    public static function escape($string)
    {
        return self::real_escape_string($string);
    }
}
