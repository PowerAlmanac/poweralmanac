<?php

namespace PowerAlmanac;

use GuzzleHttp\Client;

class Recaptcha
{
    /**
     * @param string $recaptchaSecret
     * @param string $recaptchaResponse
     * @param string $ipAddress
     * @return bool
     */
    public static function validate($recaptchaSecret, $recaptchaResponse, $ipAddress)
    {
        $valid = false;

        $formParams = [
            'secret' => $recaptchaSecret,
            'response' => $recaptchaResponse,
            'remoteip' => $ipAddress,
        ];

        try {
            $client = new Client();
            $validationResponse = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', ['form_params' => $formParams]);

            if ($validationResponse->getStatusCode() == 200 && !empty($validationResponse->getBody())) {
                $jsonResult = $validationResponse->getBody();
                $result = json_decode($jsonResult, true);

                if ($result['success'] === true) {
                    $valid = true;
                }
            } else {
                trigger_error('Failed to verify recaptcha: ' . $validationResponse->getStatusCode() . ' - ' . $validationResponse->getBody());
            }
        } catch (\Exception $e) {
            // fail validation if the service is unavailable
            trigger_error('Failed to call recaptcha site verify: ' . $e->getMessage());
            $valid = false;
        }

        return $valid;
    }
}
