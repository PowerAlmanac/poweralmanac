<?php

namespace PowerAlmanac;

class Config
{
    /**
     * Finds the path of the .env file
     *
     * @param  integer  $level
     * @param  integer  $maxLevels
     * @return string|boolean
     */
    public static function findDotEnvPath($level = 1, $maxLevels = 10)
    {
        while ($level++ < $maxLevels) {
            $envFile = __DIR__ . str_repeat('/..', $level) . '/.env';
            if (file_exists($envFile)) {
                return __DIR__ . str_repeat('/..', $level);
            }
        }
        return false;
    }

    /**
     * Gets the value of an environment variable. Supports boolean, empty and null
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    public static function env($key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            return self::value($default);
        }
        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return null;
        }
        if (strlen($value) > 1 && substr($value, 0, 1) == '"' && substr($value, -1) == '"') {
            return substr($value, 1, -1);
        }
        return $value;
    }

    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    public static function value($value)
    {
        return $value instanceof \Closure ? $value() : $value;
    }
}
