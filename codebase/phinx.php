<?php
require_once __DIR__ . '/vendor/autoload.php';
(new Dotenv\Dotenv(PowerAlmanac\Config::findDotEnvPath()))->load();

return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds',
        ],
        'environments' =>
            [
                'default_database' => 'dev',
                'default_migration_table' => 'phinxlog',
                'dev'      =>
                    [
                        'adapter' => 'mysql',
                        'host' => $_ENV['mysql_host'],
                        'name' => $_ENV['mysql_name'],
                        'user' => $_ENV['mysql_user'],
                        'pass' => $_ENV['mysql_pass'],
                        'port' => 3306,
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                    ],
                'test' =>
                    [
                        'adapter' => 'mysql',
                        'host' => $_ENV['mysql_host'],
                        'name' => $_ENV['mysql_name'],
                        'user' => $_ENV['mysql_user'],
                        'pass' => $_ENV['mysql_pass'],
                        'port' => 3306,
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                    ],
                'prod' =>
                    [
                        'adapter' => 'mysql',
                        'host' => $_ENV['mysql_host'],
                        'name' => $_ENV['mysql_name'],
                        'user' => $_ENV['mysql_user'],
                        'pass' => $_ENV['mysql_pass'],
                        'port' => 3306,
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                    ],
            ],
    ];
