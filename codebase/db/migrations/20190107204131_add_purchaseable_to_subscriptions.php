<?php


use Phinx\Migration\AbstractMigration;

class AddPurchaseableToSubscriptions extends AbstractMigration
{
    public function change()
    {
        $this->table('subscriptions')->addColumn('Purchasable', 'boolean', ['default' => false])->update();
    }
}
