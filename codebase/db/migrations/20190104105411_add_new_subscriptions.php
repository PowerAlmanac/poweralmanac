<?php

use Phinx\Migration\AbstractMigration;

class AddNewSubscriptions extends AbstractMigration
{
    public function up()
    {
        // inserting multiple rows
        $subscriptions = [
            [
                'Subscriptions_ID' => 102,
                'Sub_Level'  => 11,
                'Sub_Name' => 'Power Entry',
                'Sub_NumReserves' => 0,
                'Sub_AnnualFees' => 0,
                'Sub_CostPerDL' => 0.90,
                'Sub_PayPalCode_Sandbox' => null,
                'Sub_PayPalCode_Production' => null,
                'Sub_SKU' => 'PASUB_POWER_ENTRY_2019',
                'RegUser_ID' => 0,
                'Active' => 1
            ],
            [
                'Subscriptions_ID' => 103,
                'Sub_Level'  => 12,
                'Sub_Name' => 'Power 15',
                'Sub_NumReserves' => 15000,
                'Sub_AnnualFees' => 4500,
                'Sub_CostPerDL' => 0.30,
                'Sub_PayPalCode_Sandbox' => null,
                'Sub_PayPalCode_Production' => null,
                'Sub_SKU' => 'PASUB_POWER_15_2019',
                'RegUser_ID' => 0,
                'Active' => 1
            ],
            [
                'Subscriptions_ID' => 104,
                'Sub_Level'  => 13,
                'Sub_Name' => 'Power 75',
                'Sub_NumReserves' => 75000,
                'Sub_AnnualFees' => 10500,
                'Sub_CostPerDL' => 0.14,
                'Sub_PayPalCode_Sandbox' => null,
                'Sub_PayPalCode_Production' => null,
                'Sub_SKU' => 'PASUB_POWER_75_2019',
                'RegUser_ID' => 0,
                'Active' => 1
            ],
            [
                'Subscriptions_ID' => 105,
                'Sub_Level'  => 10,
                'Sub_Name' => 'Power Max',
                'Sub_NumReserves' => 1000000,
                'Sub_AnnualFees' => 14700,
                'Sub_CostPerDL' => 0.00,
                'Sub_PayPalCode_Sandbox' => null,
                'Sub_PayPalCode_Production' => null,
                'Sub_SKU' => 'PASUB_POWER_MAX_2019',
                'RegUser_ID' => 0,
                'Active' => 1
            ],
        ];

        $this->table('subscriptions')->insert($subscriptions)->save();
    }

    public function down()
    {
        $this->execute('DELETE FROM subscriptions WHERE Sub_SKU like "PASUB_POWER_%_2019"');
    }
}
