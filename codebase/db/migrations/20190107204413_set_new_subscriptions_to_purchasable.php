<?php


use Phinx\Migration\AbstractMigration;

class SetNewSubscriptionsToPurchasable extends AbstractMigration
{
    public function up()
    {
        $this->execute('UPDATE subscriptions SET Purchasable=0 WHERE Subscriptions_ID IN (2, 8, 34)');
        $this->execute('UPDATE subscriptions SET Purchasable=1 WHERE Sub_SKU like "PASUB_POWER_%_2019"');
    }

    public function down()
    {
        $this->execute('UPDATE subscriptions SET Purchasable=1 WHERE Subscriptions_ID IN (2, 8, 34)');
        $this->execute('UPDATE subscriptions SET Purchasable=0 WHERE Sub_SKU like "PASUB_POWER_%_2019"');
    }
}
